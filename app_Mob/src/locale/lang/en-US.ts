import dynachart_en_US from '@locale/lanres/dyna-chart/dyna-chart_en_US';
import crm_lost_reason_en_US from '@locale/lanres/crm-lost-reason/crm-lost-reason_en_US';
import product_pricelist_en_US from '@locale/lanres/product-pricelist/product-pricelist_en_US';
import product_price_list_en_US from '@locale/lanres/product-price-list/product-price-list_en_US';
import crm_lead2opportunity_partner_en_US from '@locale/lanres/crm-lead2opportunity-partner/crm-lead2opportunity-partner_en_US';
import mail_activity_en_US from '@locale/lanres/mail-activity/mail-activity_en_US';
import crm_lead2opportunity_partner_mass_en_US from '@locale/lanres/crm-lead2opportunity-partner-mass/crm-lead2opportunity-partner-mass_en_US';
import res_users_en_US from '@locale/lanres/res-users/res-users_en_US';
import crm_stage_en_US from '@locale/lanres/crm-stage/crm-stage_en_US';
import mail_message_en_US from '@locale/lanres/mail-message/mail-message_en_US';
import res_partner_en_US from '@locale/lanres/res-partner/res-partner_en_US';
import mail_activity_type_en_US from '@locale/lanres/mail-activity-type/mail-activity-type_en_US';
import crm_partner_binding_en_US from '@locale/lanres/crm-partner-binding/crm-partner-binding_en_US';
import crm_merge_opportunity_en_US from '@locale/lanres/crm-merge-opportunity/crm-merge-opportunity_en_US';
import calendar_event_en_US from '@locale/lanres/calendar-event/calendar-event_en_US';
import account_invoice_line_en_US from '@locale/lanres/account-invoice-line/account-invoice-line_en_US';
import account_invoice_en_US from '@locale/lanres/account-invoice/account-invoice_en_US';
import crm_lead_lost_en_US from '@locale/lanres/crm-lead-lost/crm-lead-lost_en_US';
import uom_uom_en_US from '@locale/lanres/uom-uom/uom-uom_en_US';
import crm_team_en_US from '@locale/lanres/crm-team/crm-team_en_US';
import account_journal_en_US from '@locale/lanres/account-journal/account-journal_en_US';
import account_register_payments_en_US from '@locale/lanres/account-register-payments/account-register-payments_en_US';
import dynadashboard_en_US from '@locale/lanres/dyna-dashboard/dyna-dashboard_en_US';
import product_category_en_US from '@locale/lanres/product-category/product-category_en_US';
import crm_lead_tag_en_US from '@locale/lanres/crm-lead-tag/crm-lead-tag_en_US';
import crm_lead_en_US from '@locale/lanres/crm-lead/crm-lead_en_US';
import crm_activity_report_en_US from '@locale/lanres/crm-activity-report/crm-activity-report_en_US';
import sale_order_line_en_US from '@locale/lanres/sale-order-line/sale-order-line_en_US';
import product_pricelist_item_en_US from '@locale/lanres/product-pricelist-item/product-pricelist-item_en_US';
import res_company_en_US from '@locale/lanres/res-company/res-company_en_US';
import product_product_en_US from '@locale/lanres/product-product/product-product_en_US';
import sale_order_en_US from '@locale/lanres/sale-order/sale-order_en_US';
import account_payment_en_US from '@locale/lanres/account-payment/account-payment_en_US';
import userCustom_en_US from '@locale/lanres/userCustom/userCustom_en_US';
import codelist_en_US from '@locale/lanres/codelist/codelist_en_US';

export default {
    app: {
        gridpage: {
            choicecolumns: 'Choice columns',
            refresh: 'refresh',
            show: 'Show',
            records: 'records',
            totle: 'totle',
        },
        tabpage: {
            sureclosetip: {
                title: 'Close warning',
                content: 'Form data Changed, are sure close?',
            },
            closeall: 'Close all',
            closeother: 'Close other',
        },
        fileUpload: {
            caption: 'Upload',
        },
        searchButton: {
            search: 'Search',
            reset: 'Reset',
        },
        // 非实体视图
        views: {
            appportalview: {
                caption: '工作台',
            },
            appindexview: {
                caption: 'CRM标准系统',
            },
        },
        menus: {
            appindexview: {
                menuitem3: '工作台',
                menuitem4: '活动',
                menuitem5: '我的',
            },
            quickmenu: {
                menuitem1: '客户信息',
                menuitem2: '商机',
                menuitem4: '账单',
                menuitem3: '开票单',
                menuitem7: '合同',
                menuitem5: '付款登记',
                menuitem6: '产品种类',
            },
        },
        components: {
            app_icon_menu: {
                statusValue_open: 'open',
                statusValue_close: 'close',
            }
        },
        button: {
            cancel: 'Cancel',
            confirm: 'Confirm',
            back: 'Back',
            loadmore: 'Load more'
        },
        loadding: 'Loadding',
        fastsearch: 'Quick search',
        pulling_text: 'Pull down to refresh',
        statusMessage:{
            200: 'The server successfully returned the requested data',
            201: 'New or modified data is successful.',
            202: 'The request has been queued in the background (asynchronous task).',
            204: 'The data was deleted successfully.',
            400: 'There was an error in the request issued, and the server did not create or modify data.',
            401: 'The user does not have permission (token, user name, wrong password).',
            403: 'The user is authorized, but access is prohibited.',
            404: 'The request issued was for a non-existent record, and the server did not operate.',
            405: 'Method not allowed',
            406: 'The requested format is not available.',
            410: 'The requested resource is permanently deleted and will no longer be available.',
            422: 'When creating an object, a validation error occurred.',
            500: 'An error occurred on the server, please check the server.',
            502: 'Gateway error.',
            503: 'The service is unavailable, the server is temporarily overloaded or maintained.',
            504: 'Gateway timeout.',
        },
        errorMessage: {
            100: 'unknown',
            101: 'Request error',
            5001: 'Data does not exist',
            5002: 'Data already exists and cannot be recreated',
            5003: 'New creation failed',
            5004: 'The data does not exist and cannot be saved',
            5005: 'Data deletion failed'
        }
    },
    dynachart: dynachart_en_US,
    crm_lost_reason: crm_lost_reason_en_US,
    product_pricelist: product_pricelist_en_US,
    product_price_list: product_price_list_en_US,
    crm_lead2opportunity_partner: crm_lead2opportunity_partner_en_US,
    mail_activity: mail_activity_en_US,
    crm_lead2opportunity_partner_mass: crm_lead2opportunity_partner_mass_en_US,
    res_users: res_users_en_US,
    crm_stage: crm_stage_en_US,
    mail_message: mail_message_en_US,
    res_partner: res_partner_en_US,
    mail_activity_type: mail_activity_type_en_US,
    crm_partner_binding: crm_partner_binding_en_US,
    crm_merge_opportunity: crm_merge_opportunity_en_US,
    calendar_event: calendar_event_en_US,
    account_invoice_line: account_invoice_line_en_US,
    account_invoice: account_invoice_en_US,
    crm_lead_lost: crm_lead_lost_en_US,
    uom_uom: uom_uom_en_US,
    crm_team: crm_team_en_US,
    account_journal: account_journal_en_US,
    account_register_payments: account_register_payments_en_US,
    dynadashboard: dynadashboard_en_US,
    product_category: product_category_en_US,
    crm_lead_tag: crm_lead_tag_en_US,
    crm_lead: crm_lead_en_US,
    crm_activity_report: crm_activity_report_en_US,
    sale_order_line: sale_order_line_en_US,
    product_pricelist_item: product_pricelist_item_en_US,
    res_company: res_company_en_US,
    product_product: product_product_en_US,
    sale_order: sale_order_en_US,
    account_payment: account_payment_en_US,
    codelist: codelist_en_US,
    userCustom: userCustom_en_US,
};