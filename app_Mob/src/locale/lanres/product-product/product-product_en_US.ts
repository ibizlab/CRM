
export default {
	views: {
		mobmpickupview: {
			caption: '产品',
		},
		mobeditview: {
			caption: '产品',
		},
		mobmdview: {
			caption: '产品',
		},
		mobpickupview: {
			caption: '产品',
		},
		mobpickupmdview: {
			caption: '产品',
		},
	},
	mobmain_form: {
		details: {
			group1: '产品基本信息', 
			group2: '操作信息', 
			formpage1: '基本信息', 
			srfupdatedate: '最后更新时间', 
			srforikey: '', 
			srfkey: 'ID', 
			srfmajortext: '名称', 
			srftempmode: '', 
			srfuf: '', 
			srfdeid: '', 
			srfsourcekey: '', 
			name: '名称', 
			id: 'ID', 
			product_tmpl_id: '产品模板', 
		},
		uiactions: {
		},
	},
	mobeditviewrighttoolbar_toolbar: {
		tbitem1: {
			caption: 'Save And Close',
			tip: 'tbitem1',
		},
	},
	mobmdviewrighttoolbar_toolbar: {
		tbitem1: {
			caption: 'New',
			tip: 'tbitem1',
		},
	},
};