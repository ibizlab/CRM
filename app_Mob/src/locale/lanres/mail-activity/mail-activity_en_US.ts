
export default {
	views: {
		mobmdview: {
			caption: '活动',
		},
		mobcalendarview: {
			caption: '活动',
		},
		mobmdview9: {
			caption: '活动',
		},
		mobeditview: {
			caption: '活动',
		},
	},
	mobmain_form: {
		details: {
			group1: '活动基本信息', 
			group2: '操作信息', 
			formpage1: '基本信息', 
			srfupdatedate: '最后更新时间', 
			srforikey: '', 
			srfkey: 'ID', 
			srfmajortext: '显示名称', 
			srftempmode: '', 
			srfuf: '', 
			srfdeid: '', 
			srfsourcekey: '', 
			display_name: '显示名称', 
			id: 'ID', 
		},
		uiactions: {
		},
	},
	mobeditviewrighttoolbar_toolbar: {
		tbitem1: {
			caption: 'Save And Close',
			tip: 'tbitem1',
		},
	},
	mobcalendarviewtoolbar_toolbar: {
		tbitem1: {
			caption: 'New',
			tip: 'tbitem1',
		},
	},
	mobmdviewrighttoolbar_toolbar: {
		tbitem1: {
			caption: 'New',
			tip: 'tbitem1',
		},
	},
};