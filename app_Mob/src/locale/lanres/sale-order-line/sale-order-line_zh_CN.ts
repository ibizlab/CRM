export default {
	views: {
		mobmdview9: {
			caption: '销售订单行',
		},
		mobeditview: {
			caption: '销售订单行',
		},
	},
	mobmain_form: {
		details: {
			grouppanel1: '说明', 
			group1: '销售订单行基本信息', 
			formpage1: '基本信息', 
			srfupdatedate: '最后更新时间', 
			srforikey: '', 
			srfkey: 'ID', 
			srfmajortext: '说明', 
			srftempmode: '', 
			srfuf: '', 
			srfdeid: '', 
			srfsourcekey: '', 
			product_id_text: '产品', 
			price_unit: '单价', 
			product_uom_qty: '订购数量', 
			product_uom: '计量单位', 
			product_uom_text: '计量单位', 
			price_total: '总计', 
			customer_lead: '交货提前时间', 
			name: '说明', 
			id: 'ID', 
			product_id: '产品', 
		},
		uiactions: {
		},
	},
	mobmdview9righttoolbar_toolbar: {
		tbitem1: {
			caption: '新建',
			tip: '新建',
		},
	},
	mobeditviewrighttoolbar_toolbar: {
		tbitem1: {
			caption: '保存',
			tip: '保存',
		},
	},
};