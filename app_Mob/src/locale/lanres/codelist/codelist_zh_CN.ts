export default {
    SALE_ORDER__STATE: {
        'draft': '报价单',
        'sent': '报价已发送',
        'sale': '销售订单',
        'done': '已锁定',
        'cancel': '已取消',
        empty: '',
    },
    CRM_LEAD__PRIORITY: {
        '0': '低',
        '1': '媒介',
        '2': '高',
        '3': '非常高',
        empty: '',
    },
    CodeList: {
        'New': '新增',
        'Qualified': '验证',
        'Proposition': '建议',
        'Won': '赢得',
        empty: '',
    },
};