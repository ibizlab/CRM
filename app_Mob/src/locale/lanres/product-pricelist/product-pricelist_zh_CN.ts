export default {
	views: {
		mobeditview: {
			caption: '价格表',
		},
		mobmdview: {
			caption: '价格表',
		},
	},
	mobmain_form: {
		details: {
			group1: '价格表基本信息', 
			group2: '操作信息', 
			formpage1: '基本信息', 
			srfupdatedate: '最后更新时间', 
			srforikey: '', 
			srfkey: 'ID', 
			srfmajortext: '价格表名称', 
			srftempmode: '', 
			srfuf: '', 
			srfdeid: '', 
			srfsourcekey: '', 
			name: '价格表名称', 
			id: 'ID', 
		},
		uiactions: {
		},
	},
	mobmdviewrighttoolbar_toolbar: {
		tbitem1: {
			caption: '新建',
			tip: '新建',
		},
	},
	mobeditviewrighttoolbar_toolbar: {
		tbitem1: {
			caption: '保存',
			tip: '保存',
		},
	},
};