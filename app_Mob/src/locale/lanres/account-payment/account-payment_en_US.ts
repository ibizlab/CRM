
export default {
	views: {
		mobeditview: {
			caption: '付款',
		},
		mobmdview: {
			caption: '付款',
		},
	},
	mobmain_form: {
		details: {
			group1: '付款基本信息', 
			group2: '操作信息', 
			formpage1: '基本信息', 
			srfupdatedate: '最后更新时间', 
			srforikey: '', 
			srfkey: 'ID', 
			srfmajortext: '名称', 
			srftempmode: '', 
			srfuf: '', 
			srfdeid: '', 
			srfsourcekey: '', 
			name: '名称', 
			id: 'ID', 
		},
		uiactions: {
		},
	},
	mobeditviewrighttoolbar_toolbar: {
		tbitem1: {
			caption: 'Save And Close',
			tip: 'tbitem1',
		},
	},
	mobmdviewrighttoolbar_toolbar: {
		tbitem1: {
			caption: 'New',
			tip: 'tbitem1',
		},
	},
};