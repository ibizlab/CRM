import { CodeListBase } from '@/ibiz-core/code-list/code-list-base';
import { HttpResponse } from '@/ibiz-core/utils';

/**
 * 代码表--SALE_ORDER__STATE
 *
 * @export
 * @class SALE_ORDER__STATE
 * @extends {CodeListBase}
 */
export class SALE_ORDER__STATE extends CodeListBase { 
}