import { CodeListBase } from '@/ibiz-core/code-list/code-list-base';
import { HttpResponse } from '@/ibiz-core/utils';

/**
 * 代码表--CRM_LEAD__PRIORITY
 *
 * @export
 * @class CRM_LEAD__PRIORITY
 * @extends {CodeListBase}
 */
export class CRM_LEAD__PRIORITY extends CodeListBase { 
}