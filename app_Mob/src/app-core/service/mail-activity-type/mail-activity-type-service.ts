import { Mail_activity_typeServiceBase } from './mail-activity-type-service-base';

/**
 * 活动类型服务对象
 *
 * @export
 * @class Mail_activity_typeService
 * @extends {Mail_activity_typeServiceBase}
 */
export class Mail_activity_typeService extends Mail_activity_typeServiceBase { }
// 默认导出
export default Mail_activity_typeService;