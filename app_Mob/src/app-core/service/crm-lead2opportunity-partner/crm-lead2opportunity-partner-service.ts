import { Crm_lead2opportunity_partnerServiceBase } from './crm-lead2opportunity-partner-service-base';

/**
 * 转化线索为商机（单个）服务对象
 *
 * @export
 * @class Crm_lead2opportunity_partnerService
 * @extends {Crm_lead2opportunity_partnerServiceBase}
 */
export class Crm_lead2opportunity_partnerService extends Crm_lead2opportunity_partnerServiceBase { }
// 默认导出
export default Crm_lead2opportunity_partnerService;