import { Account_journalServiceBase } from './account-journal-service-base';

/**
 * 日记账服务对象
 *
 * @export
 * @class Account_journalService
 * @extends {Account_journalServiceBase}
 */
export class Account_journalService extends Account_journalServiceBase { }
// 默认导出
export default Account_journalService;