import { Crm_partner_bindingServiceBase } from './crm-partner-binding-service-base';

/**
 * 在CRM向导中处理业务伙伴的绑定或生成。服务对象
 *
 * @export
 * @class Crm_partner_bindingService
 * @extends {Crm_partner_bindingServiceBase}
 */
export class Crm_partner_bindingService extends Crm_partner_bindingServiceBase { }
// 默认导出
export default Crm_partner_bindingService;