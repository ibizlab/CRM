import { Uom_uomServiceBase } from './uom-uom-service-base';

/**
 * 产品计量单位服务对象
 *
 * @export
 * @class Uom_uomService
 * @extends {Uom_uomServiceBase}
 */
export class Uom_uomService extends Uom_uomServiceBase { }
// 默认导出
export default Uom_uomService;