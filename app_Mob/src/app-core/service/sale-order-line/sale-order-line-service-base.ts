import { EntityServiceBase } from '@/ibiz-core';
import { HttpResponse } from '@/ibiz-core/utils';

/**
 * 销售订单行服务对象基类
 *
 * @export
 * @class Sale_order_lineServiceBase
 * @extends {EntityServiceBase}
 */
export class Sale_order_lineServiceBase extends EntityServiceBase {

    /**
     * 当前实体主键标识
     * 
     * @protected
     * @type {(string)}
     * @memberof Sale_order_lineServiceBase
     */
    protected readonly key: string = 'id';

    /**
     * 当前实体名称
     * 
     * @protected
     * @type {(string)}
     * @memberof Sale_order_lineServiceBase
     */
    protected readonly dePath: string = 'sale_order_lines';

    /**
     * 当前实体主信息标识
     * 
     * @protected
     * @type {(string)}
     * @memberof Sale_order_lineServiceBase
     */
    protected readonly text: string = 'name';

    /**
     * 请求根路径
     *
     * @protected
     * @type {string}
     * @memberof Sale_order_lineServiceBase
     */
    protected readonly rootUrl: string = '';

    /**
     * 所有从实体
     *
     * @protected
     * @type {*}
     * @memberof Sale_order_lineServiceBase
     */
    protected allMinorAppEntity: any = {
    };

    /**
     * Creates an instance of Crm_leadServiceBase.
     * @memberof Crm_leadServiceBase
     */
    constructor() {
        super('sale_order_line');
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Sale_order_lineServiceBase
     */
    public async FetchDefault(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            // FetchDefault ---FETCH
            const result: any = await this.getLocalCache(context);
            if (result) {
                return new HttpResponse(200, result, null, { 'x-page': 1, 'x-per-page': 1000, 'x-total': result.length });
            }
            return new HttpResponse(200, []);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * FetchTempDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Sale_order_lineServiceBase
     */
    public async FetchTempDefault(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            // FetchTempDefault ---FETCHTEMP
                    const result: any = await this.getLocalCache(context);
                    if (result) {
                        return new HttpResponse(200, result, null, { 'x-page': 1, 'x-per-page': 1000, 'x-total': result.length });
                    }
                    return new HttpResponse(200, []);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }
}