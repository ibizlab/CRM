import { EntityServiceBase } from '@/ibiz-core';
import { HttpResponse } from '@/ibiz-core/utils';

/**
 * 线索/商机服务对象基类
 *
 * @export
 * @class Crm_leadServiceBase
 * @extends {EntityServiceBase}
 */
export class Crm_leadServiceBase extends EntityServiceBase {

    /**
     * 当前实体主键标识
     * 
     * @protected
     * @type {(string)}
     * @memberof Crm_leadServiceBase
     */
    protected readonly key: string = 'id';

    /**
     * 当前实体名称
     * 
     * @protected
     * @type {(string)}
     * @memberof Crm_leadServiceBase
     */
    protected readonly dePath: string = 'crm_leads';

    /**
     * 当前实体主信息标识
     * 
     * @protected
     * @type {(string)}
     * @memberof Crm_leadServiceBase
     */
    protected readonly text: string = 'name';

    /**
     * 请求根路径
     *
     * @protected
     * @type {string}
     * @memberof Crm_leadServiceBase
     */
    protected readonly rootUrl: string = '';

    /**
     * 所有从实体
     *
     * @protected
     * @type {*}
     * @memberof Crm_leadServiceBase
     */
    protected allMinorAppEntity: any = {
    };

    /**
     * Creates an instance of Crm_leadServiceBase.
     * @memberof Crm_leadServiceBase
     */
    constructor() {
        super('crm_lead');
    }

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Crm_leadServiceBase
     */
    public async Select(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.crm_lead) {
                return await this.http.post(`/res_partners/${context.res_partner}/crm_leads/${context.crm_lead}/select`, data);
            }
            return await this.http.post(`/crm_leads/${context.crm_lead}/select`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Crm_leadServiceBase
     */
    public async GetDraft(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner) {
                return await this.http.get(`/res_partners/${context.res_partner}/crm_leads/getdraft`);
            }
            const res: any = await this.http.get(`/crm_leads/getdraft`);
            res.data.crm_lead = context.crm_lead;
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Crm_leadServiceBase
     */
    public async Remove(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.crm_lead) {
                return await this.http.delete(`/res_partners/${context.res_partner}/crm_leads/${context.crm_lead}`);
            }
            return await this.http.delete(`/crm_leads/${context.crm_lead}`);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Crm_leadServiceBase
     */
    public async Save(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.crm_lead) {
                return await this.http.post(`/res_partners/${context.res_partner}/crm_leads/${context.crm_lead}/save`, data);
            }
            Object.assign(data, await this.getMinorLocalCache(context));
            const res: any = await this.http.post(`/crm_leads/${context.crm_lead}/save`, data);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * UpdateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Crm_leadServiceBase
     */
    public async UpdateBatch(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.crm_lead) {
                return await this.http.put(`/res_partners/${context.res_partner}/crm_leads/${context.crm_lead}/updatebatch`, data);
            }
            return await this.http.put(`/crm_leads/${context.crm_lead}/updatebatch`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Crm_leadServiceBase
     */
    public async Get(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.crm_lead) {
                return await this.http.get(`/res_partners/${context.res_partner}/crm_leads/${context.crm_lead}`);
            }
            const res: any = await this.http.get(`/crm_leads/${context.crm_lead}`);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * CreateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Crm_leadServiceBase
     */
    public async CreateBatch(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.crm_lead) {
                return await this.http.post(`/res_partners/${context.res_partner}/crm_leads/${context.crm_lead}/createbatch`, data);
            }
            return await this.http.post(`/crm_leads/${context.crm_lead}/createbatch`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * RemoveBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Crm_leadServiceBase
     */
    public async RemoveBatch(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.crm_lead) {
                return await this.http.delete(`/res_partners/${context.res_partner}/crm_leads/${context.crm_lead}/removebatch`);
            }
            return await this.http.delete(`/crm_leads/${context.crm_lead}/removebatch`);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Crm_leadServiceBase
     */
    public async Create(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner) {
                return await this.http.post(`/res_partners/${context.res_partner}/crm_leads`, data);
            }
            Object.assign(data, await this.getMinorLocalCache(context));
            data.crm_lead = null;
            const res: any = await this.http.post(`/crm_leads`, data);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Crm_leadServiceBase
     */
    public async CheckKey(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.crm_lead) {
                return await this.http.post(`/res_partners/${context.res_partner}/crm_leads/${context.crm_lead}/checkkey`, data);
            }
            return await this.http.post(`/crm_leads/${context.crm_lead}/checkkey`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Crm_leadServiceBase
     */
    public async Update(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner && context.crm_lead) {
                return await this.http.put(`/res_partners/${context.res_partner}/crm_leads/${context.crm_lead}`, data);
            }
            Object.assign(data, await this.getMinorLocalCache(context));
            const res: any = await this.http.put(`/crm_leads/${context.crm_lead}`, data);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Crm_leadServiceBase
     */
    public async FetchDefault(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner) {
                return await this.http.get(`/res_partners/${context.res_partner}/crm_leads/fetchdefault`, data);
            }
            return await this.http.get(`/crm_leads/fetchdefault`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * FetchDefault2接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Crm_leadServiceBase
     */
    public async FetchDefault2(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.res_partner) {
                return await this.http.get(`/res_partners/${context.res_partner}/crm_leads/fetchdefault2`, data);
            }
            return await this.http.get(`/crm_leads/fetchdefault2`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }
}