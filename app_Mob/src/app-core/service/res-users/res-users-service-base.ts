import { EntityServiceBase } from '@/ibiz-core';
import { HttpResponse } from '@/ibiz-core/utils';

/**
 * 用户服务对象基类
 *
 * @export
 * @class Res_usersServiceBase
 * @extends {EntityServiceBase}
 */
export class Res_usersServiceBase extends EntityServiceBase {

    /**
     * 当前实体主键标识
     * 
     * @protected
     * @type {(string)}
     * @memberof Res_usersServiceBase
     */
    protected readonly key: string = 'id';

    /**
     * 当前实体名称
     * 
     * @protected
     * @type {(string)}
     * @memberof Res_usersServiceBase
     */
    protected readonly dePath: string = 'res_users';

    /**
     * 当前实体主信息标识
     * 
     * @protected
     * @type {(string)}
     * @memberof Res_usersServiceBase
     */
    protected readonly text: string = 'name';

    /**
     * 请求根路径
     *
     * @protected
     * @type {string}
     * @memberof Res_usersServiceBase
     */
    protected readonly rootUrl: string = '';

    /**
     * 所有从实体
     *
     * @protected
     * @type {*}
     * @memberof Res_usersServiceBase
     */
    protected allMinorAppEntity: any = {
    };

    /**
     * Creates an instance of Crm_leadServiceBase.
     * @memberof Crm_leadServiceBase
     */
    constructor() {
        super('res_users');
    }

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_usersServiceBase
     */
    public async Select(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.crm_team && context.res_users) {
                return await this.http.post(`/crm_teams/${context.crm_team}/res_users/${context.res_users}/select`, data);
            }
            return await this.http.post(`/res_users/${context.res_users}/select`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_usersServiceBase
     */
    public async Update(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.crm_team && context.res_users) {
                return await this.http.put(`/crm_teams/${context.crm_team}/res_users/${context.res_users}`, data);
            }
            Object.assign(data, await this.getMinorLocalCache(context));
            const res: any = await this.http.put(`/res_users/${context.res_users}`, data);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_usersServiceBase
     */
    public async Save(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.crm_team && context.res_users) {
                return await this.http.post(`/crm_teams/${context.crm_team}/res_users/${context.res_users}/save`, data);
            }
            Object.assign(data, await this.getMinorLocalCache(context));
            const res: any = await this.http.post(`/res_users/${context.res_users}/save`, data);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_usersServiceBase
     */
    public async Create(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.crm_team) {
                return await this.http.post(`/crm_teams/${context.crm_team}/res_users`, data);
            }
            Object.assign(data, await this.getMinorLocalCache(context));
            data.res_users = null;
            const res: any = await this.http.post(`/res_users`, data);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_usersServiceBase
     */
    public async CheckKey(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.crm_team && context.res_users) {
                return await this.http.post(`/crm_teams/${context.crm_team}/res_users/${context.res_users}/checkkey`, data);
            }
            return await this.http.post(`/res_users/${context.res_users}/checkkey`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_usersServiceBase
     */
    public async Get(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.crm_team && context.res_users) {
                return await this.http.get(`/crm_teams/${context.crm_team}/res_users/${context.res_users}`);
            }
            const res: any = await this.http.get(`/res_users/${context.res_users}`);
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * UpdateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_usersServiceBase
     */
    public async UpdateBatch(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.crm_team && context.res_users) {
                return await this.http.put(`/crm_teams/${context.crm_team}/res_users/${context.res_users}/updatebatch`, data);
            }
            return await this.http.put(`/res_users/${context.res_users}/updatebatch`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * CreateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_usersServiceBase
     */
    public async CreateBatch(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.crm_team && context.res_users) {
                return await this.http.post(`/crm_teams/${context.crm_team}/res_users/${context.res_users}/createbatch`, data);
            }
            return await this.http.post(`/res_users/${context.res_users}/createbatch`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * RemoveBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_usersServiceBase
     */
    public async RemoveBatch(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.crm_team && context.res_users) {
                return await this.http.delete(`/crm_teams/${context.crm_team}/res_users/${context.res_users}/removebatch`);
            }
            return await this.http.delete(`/res_users/${context.res_users}/removebatch`);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_usersServiceBase
     */
    public async Remove(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.crm_team && context.res_users) {
                return await this.http.delete(`/crm_teams/${context.crm_team}/res_users/${context.res_users}`);
            }
            return await this.http.delete(`/res_users/${context.res_users}`);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_usersServiceBase
     */
    public async GetDraft(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.crm_team) {
                return await this.http.get(`/crm_teams/${context.crm_team}/res_users/getdraft`);
            }
            const res: any = await this.http.get(`/res_users/getdraft`);
            res.data.res_users = context.res_users;
            await this.setMinorLocalCache(context, res.data);
            return res;
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @returns {Promise<HttpResponse>}
     * @memberof Res_usersServiceBase
     */
    public async FetchDefault(context: any = {}, data: any = {}): Promise<HttpResponse> {
        try {
            if (context.crm_team) {
                return await this.http.get(`/crm_teams/${context.crm_team}/res_users/fetchdefault`, data);
            }
            return await this.http.get(`/res_users/fetchdefault`, data);
        } catch (res) {
            return new HttpResponse(res.status, null);
        }
    }
}