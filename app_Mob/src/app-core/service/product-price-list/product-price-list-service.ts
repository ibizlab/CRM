import { Product_price_listServiceBase } from './product-price-list-service-base';

/**
 * 基于价格列表版本的单位产品价格服务对象
 *
 * @export
 * @class Product_price_listService
 * @extends {Product_price_listServiceBase}
 */
export class Product_price_listService extends Product_price_listServiceBase { }
// 默认导出
export default Product_price_listService;