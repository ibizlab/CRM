import { Crm_teamServiceBase } from './crm-team-service-base';

/**
 * 销售渠道服务对象
 *
 * @export
 * @class Crm_teamService
 * @extends {Crm_teamServiceBase}
 */
export class Crm_teamService extends Crm_teamServiceBase { }
// 默认导出
export default Crm_teamService;