import { Sale_orderServiceBase } from './sale-order-service-base';

/**
 * 销售订单服务对象
 *
 * @export
 * @class Sale_orderService
 * @extends {Sale_orderServiceBase}
 */
export class Sale_orderService extends Sale_orderServiceBase { }
// 默认导出
export default Sale_orderService;