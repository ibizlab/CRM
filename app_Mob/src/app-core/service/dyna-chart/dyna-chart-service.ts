import { DynaChartServiceBase } from './dyna-chart-service-base';

/**
 * 动态图表服务对象
 *
 * @export
 * @class DynaChartService
 * @extends {DynaChartServiceBase}
 */
export class DynaChartService extends DynaChartServiceBase { }
// 默认导出
export default DynaChartService;