/**
 * Mob 部件模型
 *
 * @export
 * @class MobModel
 */
export class MobModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MobMdctrlModel
	 */
	public getDataItems(): any[] {
		return [
			{
				name: 'stage_id_text',
			},
			{
				name: 'planned_revenue',
			},
			{
				name: 'priority',
			},
			{
				name: 'partner_name',
			},
			{
				name: 'srfkey',
				prop: 'id',
				dataType: 'ACID',
			},
			{
				name: 'srfmajortext',
				prop: 'name',
				dataType: 'TEXT',
			},
			{
				name: 'lost_reason',
				prop: 'lost_reason',
				dataType: 'PICKUP',
			},
			{
				name: 'create_uid',
				prop: 'create_uid',
				dataType: 'PICKUP',
			},
			{
				name: 'write_uid',
				prop: 'write_uid',
				dataType: 'PICKUP',
			},
			{
				name: 'company_id',
				prop: 'company_id',
				dataType: 'PICKUP',
			},
			{
				name: 'user_id',
				prop: 'user_id',
				dataType: 'PICKUP',
			},
			{
				name: 'state_id',
				prop: 'state_id',
				dataType: 'PICKUP',
			},
			{
				name: 'medium_id',
				prop: 'medium_id',
				dataType: 'PICKUP',
			},
			{
				name: 'stage_id',
				prop: 'stage_id',
				dataType: 'PICKUP',
			},
			{
				name: 'source_id',
				prop: 'source_id',
				dataType: 'PICKUP',
			},
			{
				name: 'country_id',
				prop: 'country_id',
				dataType: 'PICKUP',
			},
			{
				name: 'campaign_id',
				prop: 'campaign_id',
				dataType: 'PICKUP',
			},
			{
				name: 'partner_id',
				prop: 'partner_id',
				dataType: 'PICKUP',
			},
			{
				name: 'team_id',
				prop: 'team_id',
				dataType: 'PICKUP',
			},
			{
				name: 'title',
				prop: 'title',
				dataType: 'PICKUP',
			},
			{
				name: 'crm_lead',
				prop: 'id',
				dataType: 'FONTKEY',
			},

            {
                name:'size',
                prop:'size'
            },
            {
                name:'query',
                prop:'query'
            },
            {
                name:'page',
                prop:'page'
            },
            {
                name:'sort',
                prop:'sort'
            },
            {
                name:'srfparentdata',
                prop:'srfparentdata'
            }
		]
	}

}
// 默认导出
export default MobModel;