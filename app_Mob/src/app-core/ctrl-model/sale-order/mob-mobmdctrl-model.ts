/**
 * Mob 部件模型
 *
 * @export
 * @class MobModel
 */
export class MobModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MobMdctrlModel
	 */
	public getDataItems(): any[] {
		return [
			{
				name: 'srfkey',
				prop: 'id',
				dataType: 'ACID',
			},
			{
				name: 'srfmajortext',
				prop: 'name',
				dataType: 'TEXT',
			},
			{
				name: 'write_uid',
				prop: 'write_uid',
				dataType: 'PICKUP',
			},
			{
				name: 'team_id',
				prop: 'team_id',
				dataType: 'PICKUP',
			},
			{
				name: 'partner_invoice_id',
				prop: 'partner_invoice_id',
				dataType: 'PICKUP',
			},
			{
				name: 'fiscal_position_id',
				prop: 'fiscal_position_id',
				dataType: 'PICKUP',
			},
			{
				name: 'partner_id',
				prop: 'partner_id',
				dataType: 'PICKUP',
			},
			{
				name: 'campaign_id',
				prop: 'campaign_id',
				dataType: 'PICKUP',
			},
			{
				name: 'source_id',
				prop: 'source_id',
				dataType: 'PICKUP',
			},
			{
				name: 'sale_order_template_id',
				prop: 'sale_order_template_id',
				dataType: 'PICKUP',
			},
			{
				name: 'user_id',
				prop: 'user_id',
				dataType: 'PICKUP',
			},
			{
				name: 'company_id',
				prop: 'company_id',
				dataType: 'PICKUP',
			},
			{
				name: 'medium_id',
				prop: 'medium_id',
				dataType: 'PICKUP',
			},
			{
				name: 'analytic_account_id',
				prop: 'analytic_account_id',
				dataType: 'PICKUP',
			},
			{
				name: 'payment_term_id',
				prop: 'payment_term_id',
				dataType: 'PICKUP',
			},
			{
				name: 'partner_shipping_id',
				prop: 'partner_shipping_id',
				dataType: 'PICKUP',
			},
			{
				name: 'opportunity_id',
				prop: 'opportunity_id',
				dataType: 'PICKUP',
			},
			{
				name: 'incoterm',
				prop: 'incoterm',
				dataType: 'PICKUP',
			},
			{
				name: 'pricelist_id',
				prop: 'pricelist_id',
				dataType: 'PICKUP',
			},
			{
				name: 'create_uid',
				prop: 'create_uid',
				dataType: 'PICKUP',
			},
			{
				name: 'warehouse_id',
				prop: 'warehouse_id',
				dataType: 'PICKUP',
			},
			{
				name: 'sale_order',
				prop: 'id',
				dataType: 'FONTKEY',
			},

            {
                name:'size',
                prop:'size'
            },
            {
                name:'query',
                prop:'query'
            },
            {
                name:'page',
                prop:'page'
            },
            {
                name:'sort',
                prop:'sort'
            },
            {
                name:'srfparentdata',
                prop:'srfparentdata'
            }
		]
	}

}
// 默认导出
export default MobModel;