/**
 * QuickMenu 部件模型
 *
 * @export
 * @class QuickMenuModel
 */
export class QuickMenuModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof QuickMenuModel
    */
  public getDataItems(): any[] {
    return [
    ]
  }

}
// 默认导出
export default QuickMenuModel;