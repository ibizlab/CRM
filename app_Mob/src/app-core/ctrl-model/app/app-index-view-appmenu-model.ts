/**
 * AppIndexView 部件模型
 *
 * @export
 * @class AppIndexViewModel
 */
export class AppIndexViewModel {

    /**
     * 菜单项集合
     *
     * @private
     * @type {any[]}
     * @memberof AppIndexViewModel
     */
    private items: any[] = [
        {
	id: '0cfef223143b6805dcf2d802b6e79ea0',
	name: 'menuitem3',
	text: '工作台',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '工作台',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'home',
	icon: '',
	textcls: '',
	appfunctag: 'Auto19',
	resourcetag: '',
},
        {
	id: '1ebe52baa4573adb6c5d44146d0da6a3',
	name: 'menuitem4',
	text: '活动',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '活动',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'chatbubble-ellipses',
	icon: '',
	textcls: '',
	appfunctag: 'Auto4',
	resourcetag: '',
},
        {
	id: '6221be19414c86117e91659f4a19154b',
	name: 'menuitem5',
	text: '我的',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '我的',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'person-circle',
	icon: '',
	textcls: '',
	appfunctag: '',
	resourcetag: '',
},
    ];

	/**
	 * 应用功能集合
	 *
	 * @private
	 * @type {any[]}
	 * @memberof AppIndexViewModel
	 */
	private funcs: any[] = [
        {
            appfunctag: 'Auto19',
            appfuncyype: 'APPVIEW',
            openmode: '', 
            componentname: 'app-portal-view', 
            codename: 'appportalview',
            deResParameters: [],
            routepath: '/appindexview/:appindexview?/appportalview/:appportalview?',
            parameters: [
                { pathName: 'appportalview', parameterName: 'appportalview' },
            ],
        },
        {
            appfunctag: 'Auto4',
            appfuncyype: 'APPVIEW',
            openmode: '', 
            componentname: 'mail-activity-mob-calendar-view', 
            codename: 'mail_activitymobcalendarview',
            deResParameters: [],
            routepath: '/appindexview/:appindexview?/mail_activities/:mail_activity?/mobcalendarview/:mobcalendarview?',
            parameters: [
                { pathName: 'mail_activities', parameterName: 'mail_activity' },
                { pathName: 'mobcalendarview', parameterName: 'mobcalendarview' },
            ],
        },
	];

    /**
     * 获取所有菜单项集合
     *
     * @returns {any[]}
     * @memberof AppIndexViewModel
     */
    public getAppMenuItems(): any[] {
        return this.items;
    }

    /**
     * 获取所有应用功能集合
     *
     * @returns {any[]}
     * @memberof AppIndexViewModel
     */
    public getAppFuncs(): any[] {
        return this.funcs;
    }
}
// 默认导出
export default AppIndexViewModel;