/**
 * Trend 部件模型
 *
 * @export
 * @class TrendModel
 */
export class TrendModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof TrendDb_sysportlet3_chartMode
	 */
	public getDataItems(): any[] {
		return [
			{
				name:'query',
				prop:'query'
			},
		]
	}

}
// 默认导出
export default TrendModel;