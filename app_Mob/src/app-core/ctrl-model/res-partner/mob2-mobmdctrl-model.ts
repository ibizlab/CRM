/**
 * Mob2 部件模型
 *
 * @export
 * @class Mob2Model
 */
export class Mob2Model {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Mob2MdctrlModel
	 */
	public getDataItems(): any[] {
		return [
			{
				name: 'is_company',
			},
			{
				name: 'user_id_text',
			},
			{
				name: 'contact_address',
			},
			{
				name: 'srfkey',
				prop: 'id',
				dataType: 'ACID',
			},
			{
				name: 'srfmajortext',
				prop: 'name',
				dataType: 'TEXT',
			},
			{
				name: 'team_id',
				prop: 'team_id',
				dataType: 'PICKUP',
			},
			{
				name: 'state_id',
				prop: 'state_id',
				dataType: 'PICKUP',
			},
			{
				name: 'user_id',
				prop: 'user_id',
				dataType: 'PICKUP',
			},
			{
				name: 'create_uid',
				prop: 'create_uid',
				dataType: 'PICKUP',
			},
			{
				name: 'parent_id',
				prop: 'parent_id',
				dataType: 'PICKUP',
			},
			{
				name: 'title',
				prop: 'title',
				dataType: 'PICKUP',
			},
			{
				name: 'write_uid',
				prop: 'write_uid',
				dataType: 'PICKUP',
			},
			{
				name: 'commercial_partner_id',
				prop: 'commercial_partner_id',
				dataType: 'PICKUP',
			},
			{
				name: 'industry_id',
				prop: 'industry_id',
				dataType: 'PICKUP',
			},
			{
				name: 'company_id',
				prop: 'company_id',
				dataType: 'PICKUP',
			},
			{
				name: 'country_id',
				prop: 'country_id',
				dataType: 'PICKUP',
			},
			{
				name: 'res_partner',
				prop: 'id',
				dataType: 'FONTKEY',
			},

            {
                name:'size',
                prop:'size'
            },
            {
                name:'query',
                prop:'query'
            },
            {
                name:'page',
                prop:'page'
            },
            {
                name:'sort',
                prop:'sort'
            },
            {
                name:'srfparentdata',
                prop:'srfparentdata'
            }
		]
	}

}
// 默认导出
export default Mob2Model;