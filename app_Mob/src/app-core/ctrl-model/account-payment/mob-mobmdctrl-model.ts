/**
 * Mob 部件模型
 *
 * @export
 * @class MobModel
 */
export class MobModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MobMdctrlModel
	 */
	public getDataItems(): any[] {
		return [
			{
				name: 'srfkey',
				prop: 'id',
				dataType: 'ACID',
			},
			{
				name: 'srfmajortext',
				prop: 'name',
				dataType: 'TEXT',
			},
			{
				name: 'write_uid',
				prop: 'write_uid',
				dataType: 'PICKUP',
			},
			{
				name: 'payment_token_id',
				prop: 'payment_token_id',
				dataType: 'PICKUP',
			},
			{
				name: 'payment_transaction_id',
				prop: 'payment_transaction_id',
				dataType: 'PICKUP',
			},
			{
				name: 'writeoff_account_id',
				prop: 'writeoff_account_id',
				dataType: 'PICKUP',
			},
			{
				name: 'partner_bank_account_id',
				prop: 'partner_bank_account_id',
				dataType: 'PICKUP',
			},
			{
				name: 'create_uid',
				prop: 'create_uid',
				dataType: 'PICKUP',
			},
			{
				name: 'payment_method_id',
				prop: 'payment_method_id',
				dataType: 'PICKUP',
			},
			{
				name: 'destination_journal_id',
				prop: 'destination_journal_id',
				dataType: 'PICKUP',
			},
			{
				name: 'partner_id',
				prop: 'partner_id',
				dataType: 'PICKUP',
			},
			{
				name: 'journal_id',
				prop: 'journal_id',
				dataType: 'PICKUP',
			},
			{
				name: 'currency_id',
				prop: 'currency_id',
				dataType: 'PICKUP',
			},
			{
				name: 'account_payment',
				prop: 'id',
				dataType: 'FONTKEY',
			},

            {
                name:'size',
                prop:'size'
            },
            {
                name:'query',
                prop:'query'
            },
            {
                name:'page',
                prop:'page'
            },
            {
                name:'sort',
                prop:'sort'
            },
            {
                name:'srfparentdata',
                prop:'srfparentdata'
            }
		]
	}

}
// 默认导出
export default MobModel;