/**
 * Mob 部件模型
 *
 * @export
 * @class MobModel
 */
export class MobModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MobMdctrlModel
	 */
	public getDataItems(): any[] {
		return [
			{
				name: 'srfkey',
				prop: 'id',
				dataType: 'ACID',
			},
			{
				name: 'srfmajortext',
				prop: 'display_name',
				dataType: 'TEXT',
			},
			{
				name: 'recommended_activity_type_id',
				prop: 'recommended_activity_type_id',
				dataType: 'PICKUP',
			},
			{
				name: 'activity_type_id',
				prop: 'activity_type_id',
				dataType: 'PICKUP',
			},
			{
				name: 'create_user_id',
				prop: 'create_user_id',
				dataType: 'PICKUP',
			},
			{
				name: 'create_uid',
				prop: 'create_uid',
				dataType: 'PICKUP',
			},
			{
				name: 'user_id',
				prop: 'user_id',
				dataType: 'PICKUP',
			},
			{
				name: 'note_id',
				prop: 'note_id',
				dataType: 'PICKUP',
			},
			{
				name: 'previous_activity_type_id',
				prop: 'previous_activity_type_id',
				dataType: 'PICKUP',
			},
			{
				name: 'calendar_event_id',
				prop: 'calendar_event_id',
				dataType: 'PICKUP',
			},
			{
				name: 'write_uid',
				prop: 'write_uid',
				dataType: 'PICKUP',
			},
			{
				name: 'mail_activity',
				prop: 'id',
				dataType: 'FONTKEY',
			},

            {
                name:'size',
                prop:'size'
            },
            {
                name:'query',
                prop:'query'
            },
            {
                name:'page',
                prop:'page'
            },
            {
                name:'sort',
                prop:'sort'
            },
            {
                name:'srfparentdata',
                prop:'srfparentdata'
            }
		]
	}

}
// 默认导出
export default MobModel;