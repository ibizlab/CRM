/**
 * MobBriefReport 部件模型
 *
 * @export
 * @class MobBriefReportModel
 */
export class MobBriefReportModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof MobBriefReportModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'is_moderator',
      },
      {
        name: 'resource_ids',
      },
      {
        name: 'create_date',
      },
      {
        name: 'category_id',
      },
      {
        name: 'resource_calendar_id',
      },
      {
        name: 'log_ids',
      },
      {
        name: 'message_ids',
      },
      {
        name: 'pos_security_pin',
      },
      {
        name: 'badge_ids',
      },
      {
        name: 'write_date',
      },
      {
        name: 'res_users',
        prop: 'id',
      },
      {
        name: 'company_ids',
      },
      {
        name: 'child_ids',
      },
      {
        name: 'tz_offset',
      },
      {
        name: 'target_sales_done',
      },
      {
        name: 'message_partner_ids',
      },
      {
        name: 'notification_type',
      },
      {
        name: 'active',
      },
      {
        name: 'im_status',
      },
      {
        name: 'karma',
      },
      {
        name: 'website_id',
      },
      {
        name: 'activity_ids',
      },
      {
        name: 'gold_badge',
      },
      {
        name: 'employee_ids',
      },
      {
        name: 'state',
      },
      {
        name: 'moderation_channel_ids',
      },
      {
        name: 'message_channel_ids',
      },
      {
        name: 'silver_badge',
      },
      {
        name: 'payment_token_ids',
      },
      {
        name: 'display_name',
      },
      {
        name: 'companies_count',
      },
      {
        name: 'target_sales_invoiced',
      },
      {
        name: 'message_follower_ids',
      },
      {
        name: 'login_date',
      },
      {
        name: 'channel_ids',
      },
      {
        name: 'groups_id',
      },
      {
        name: 'share',
      },
      {
        name: 'bank_ids',
      },
      {
        name: 'sale_order_ids',
      },
      {
        name: 'new_password',
      },
      {
        name: 'odoobot_state',
      },
      {
        name: 'ref_company_ids',
      },
      {
        name: 'password',
      },
      {
        name: 'bronze_badge',
      },
      {
        name: 'meeting_ids',
      },
      {
        name: '__last_update',
      },
      {
        name: 'forum_waiting_posts_count',
      },
      {
        name: 'goal_ids',
      },
      {
        name: 'target_sales_won',
      },
      {
        name: 'website_message_ids',
      },
      {
        name: 'action_id',
      },
      {
        name: 'login',
      },
      {
        name: 'contract_ids',
      },
      {
        name: 'moderation_counter',
      },
      {
        name: 'signature',
      },
      {
        name: 'user_ids',
      },
      {
        name: 'opportunity_ids',
      },
      {
        name: 'task_ids',
      },
      {
        name: 'invoice_ids',
      },
      {
        name: 'ref',
      },
      {
        name: 'message_has_error_counter',
      },
      {
        name: 'last_website_so_id',
      },
      {
        name: 'date',
      },
      {
        name: 'message_is_follower',
      },
      {
        name: 'property_stock_customer',
      },
      {
        name: 'last_time_entries_checked',
      },
      {
        name: 'lang',
      },
      {
        name: 'sale_warn',
      },
      {
        name: 'meeting_count',
      },
      {
        name: 'street',
      },
      {
        name: 'invoice_warn',
      },
      {
        name: 'signup_token',
      },
      {
        name: 'task_count',
      },
      {
        name: 'signup_valid',
      },
      {
        name: 'message_unread_counter',
      },
      {
        name: 'signup_type',
      },
      {
        name: 'property_account_receivable_id',
      },
      {
        name: 'website_meta_og_img',
      },
      {
        name: 'event_count',
      },
      {
        name: 'journal_item_count',
      },
      {
        name: 'parent_name',
      },
      {
        name: 'sale_team_id_text',
      },
      {
        name: 'credit',
      },
      {
        name: 'opportunity_count',
      },
      {
        name: 'signup_url',
      },
      {
        name: 'property_account_payable_id',
      },
      {
        name: 'state_id',
      },
      {
        name: 'message_has_error',
      },
      {
        name: 'sale_warn_msg',
      },
      {
        name: 'website_published',
      },
      {
        name: 'total_invoiced',
      },
      {
        name: 'debit',
      },
      {
        name: 'pos_order_count',
      },
      {
        name: 'write_uid_text',
      },
      {
        name: 'title',
      },
      {
        name: 'message_unread',
      },
      {
        name: 'supplier_invoice_count',
      },
      {
        name: 'city',
      },
      {
        name: 'picking_warn',
      },
      {
        name: 'message_main_attachment_id',
      },
      {
        name: 'activity_user_id',
      },
      {
        name: 'additional_info',
      },
      {
        name: 'ibizfunction',
      },
      {
        name: 'website_url',
      },
      {
        name: 'calendar_last_notif_ack',
      },
      {
        name: 'message_needaction_counter',
      },
      {
        name: 'activity_date_deadline',
      },
      {
        name: 'employee',
      },
      {
        name: 'activity_state',
      },
      {
        name: 'barcode',
      },
      {
        name: 'partner_gid',
      },
      {
        name: 'type',
      },
      {
        name: 'vat',
      },
      {
        name: 'purchase_warn_msg',
      },
      {
        name: 'comment',
      },
      {
        name: 'supplier',
      },
      {
        name: 'website_meta_keywords',
      },
      {
        name: 'parent_id',
      },
      {
        name: 'purchase_warn',
      },
      {
        name: 'active_partner',
      },
      {
        name: 'currency_id',
      },
      {
        name: 'industry_id',
      },
      {
        name: 'property_stock_supplier',
      },
      {
        name: 'payment_token_count',
      },
      {
        name: 'message_needaction',
      },
      {
        name: 'user_id',
      },
      {
        name: 'customer',
      },
      {
        name: 'property_payment_term_id',
      },
      {
        name: 'activity_type_id',
      },
      {
        name: 'contracts_count',
      },
      {
        name: 'self',
      },
      {
        name: 'website_meta_description',
      },
      {
        name: 'image',
      },
      {
        name: 'email',
      },
      {
        name: 'image_medium',
      },
      {
        name: 'activity_summary',
      },
      {
        name: 'debit_limit',
      },
      {
        name: 'country_id',
      },
      {
        name: 'credit_limit',
      },
      {
        name: 'commercial_company_name',
      },
      {
        name: 'invoice_warn_msg',
      },
      {
        name: 'is_published',
      },
      {
        name: 'trust',
      },
      {
        name: 'mobile',
      },
      {
        name: 'email_formatted',
      },
      {
        name: 'is_company',
      },
      {
        name: 'create_uid_text',
      },
      {
        name: 'team_id',
      },
      {
        name: 'is_blacklisted',
      },
      {
        name: 'bank_account_count',
      },
      {
        name: 'property_product_pricelist',
      },
      {
        name: 'name',
      },
      {
        name: 'picking_warn_msg',
      },
      {
        name: 'message_attachment_count',
      },
      {
        name: 'website',
      },
      {
        name: 'phone',
      },
      {
        name: 'street2',
      },
      {
        name: 'has_unreconciled_entries',
      },
      {
        name: 'signup_expiration',
      },
      {
        name: 'tz',
      },
      {
        name: 'contact_address',
      },
      {
        name: 'website_short_description',
      },
      {
        name: 'partner_share',
      },
      {
        name: 'company_id_text',
      },
      {
        name: 'color',
      },
      {
        name: 'zip',
      },
      {
        name: 'sale_order_count',
      },
      {
        name: 'company_type',
      },
      {
        name: 'property_account_position_id',
      },
      {
        name: 'is_seo_optimized',
      },
      {
        name: 'message_bounce',
      },
      {
        name: 'website_meta_title',
      },
      {
        name: 'image_small',
      },
      {
        name: 'property_purchase_currency_id',
      },
      {
        name: 'purchase_order_count',
      },
      {
        name: 'website_description',
      },
      {
        name: 'property_supplier_payment_term_id',
      },
      {
        name: 'commercial_partner_id',
      },
      {
        name: 'alias_contact',
      },
      {
        name: 'company_name',
      },
      {
        name: 'company_id',
      },
      {
        name: 'partner_id',
      },
      {
        name: 'alias_id',
      },
      {
        name: 'sale_team_id',
      },
      {
        name: 'write_uid',
      },
      {
        name: 'create_uid',
      },
    ]
  }

}
// 默认导出
export default MobBriefReportModel;