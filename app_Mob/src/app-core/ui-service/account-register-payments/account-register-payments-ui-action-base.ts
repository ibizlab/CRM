import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * 登记付款UI服务对象基类
 *
 * @export
 * @class Account_register_paymentsUIActionBase
 * @extends {UIActionBase}
 */
export class Account_register_paymentsUIActionBase extends UIActionBase {

}