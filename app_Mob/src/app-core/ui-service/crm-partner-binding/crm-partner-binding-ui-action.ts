import { Crm_partner_bindingUIActionBase } from './crm-partner-binding-ui-action-base';

/**
 * 在CRM向导中处理业务伙伴的绑定或生成。UI服务对象
 *
 * @export
 * @class Crm_partner_bindingUIAction
 * @extends {Crm_partner_bindingUIActionBase}
 */
export class Crm_partner_bindingUIAction extends Crm_partner_bindingUIActionBase { }
// 默认导出
export default Crm_partner_bindingUIAction;