import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * CRM活动分析UI服务对象基类
 *
 * @export
 * @class Crm_activity_reportUIActionBase
 * @extends {UIActionBase}
 */
export class Crm_activity_reportUIActionBase extends UIActionBase {

}