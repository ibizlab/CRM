import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * 销售订单行UI服务对象基类
 *
 * @export
 * @class Sale_order_lineUIActionBase
 * @extends {UIActionBase}
 */
export class Sale_order_lineUIActionBase extends UIActionBase {

}