import { Sale_order_lineUIActionBase } from './sale-order-line-ui-action-base';

/**
 * 销售订单行UI服务对象
 *
 * @export
 * @class Sale_order_lineUIAction
 * @extends {Sale_order_lineUIActionBase}
 */
export class Sale_order_lineUIAction extends Sale_order_lineUIActionBase { }
// 默认导出
export default Sale_order_lineUIAction;