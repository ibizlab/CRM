import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * 产品种类UI服务对象基类
 *
 * @export
 * @class Product_categoryUIActionBase
 * @extends {UIActionBase}
 */
export class Product_categoryUIActionBase extends UIActionBase {

}