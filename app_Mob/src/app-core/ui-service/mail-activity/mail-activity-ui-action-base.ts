import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * 活动UI服务对象基类
 *
 * @export
 * @class Mail_activityUIActionBase
 * @extends {UIActionBase}
 */
export class Mail_activityUIActionBase extends UIActionBase {

}