import { Mail_activityUIActionBase } from './mail-activity-ui-action-base';

/**
 * 活动UI服务对象
 *
 * @export
 * @class Mail_activityUIAction
 * @extends {Mail_activityUIActionBase}
 */
export class Mail_activityUIAction extends Mail_activityUIActionBase { }
// 默认导出
export default Mail_activityUIAction;