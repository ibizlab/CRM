import { Sale_orderUIActionBase } from './sale-order-ui-action-base';

/**
 * 销售订单UI服务对象
 *
 * @export
 * @class Sale_orderUIAction
 * @extends {Sale_orderUIActionBase}
 */
export class Sale_orderUIAction extends Sale_orderUIActionBase { }
// 默认导出
export default Sale_orderUIAction;