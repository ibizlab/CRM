import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * 销售订单UI服务对象基类
 *
 * @export
 * @class Sale_orderUIActionBase
 * @extends {UIActionBase}
 */
export class Sale_orderUIActionBase extends UIActionBase {

}