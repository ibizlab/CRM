import { Mail_messageUIActionBase } from './mail-message-ui-action-base';

/**
 * 消息UI服务对象
 *
 * @export
 * @class Mail_messageUIAction
 * @extends {Mail_messageUIActionBase}
 */
export class Mail_messageUIAction extends Mail_messageUIActionBase { }
// 默认导出
export default Mail_messageUIAction;