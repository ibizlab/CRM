import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * 基于价格列表版本的单位产品价格UI服务对象基类
 *
 * @export
 * @class Product_price_listUIActionBase
 * @extends {UIActionBase}
 */
export class Product_price_listUIActionBase extends UIActionBase {

}