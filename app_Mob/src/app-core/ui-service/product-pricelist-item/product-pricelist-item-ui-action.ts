import { Product_pricelist_itemUIActionBase } from './product-pricelist-item-ui-action-base';

/**
 * 价格表明细UI服务对象
 *
 * @export
 * @class Product_pricelist_itemUIAction
 * @extends {Product_pricelist_itemUIActionBase}
 */
export class Product_pricelist_itemUIAction extends Product_pricelist_itemUIActionBase { }
// 默认导出
export default Product_pricelist_itemUIAction;