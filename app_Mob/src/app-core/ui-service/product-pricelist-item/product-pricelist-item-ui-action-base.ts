import { UIActionBase } from '@/ibiz-core';
import { Util, UIActionTool } from '@/ibiz-core/utils';

/**
 * 价格表明细UI服务对象基类
 *
 * @export
 * @class Product_pricelist_itemUIActionBase
 * @extends {UIActionBase}
 */
export class Product_pricelist_itemUIActionBase extends UIActionBase {

}