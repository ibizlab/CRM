import { Crm_merge_opportunityUIActionBase } from './crm-merge-opportunity-ui-action-base';

/**
 * 合并商机UI服务对象
 *
 * @export
 * @class Crm_merge_opportunityUIAction
 * @extends {Crm_merge_opportunityUIActionBase}
 */
export class Crm_merge_opportunityUIAction extends Crm_merge_opportunityUIActionBase { }
// 默认导出
export default Crm_merge_opportunityUIAction;