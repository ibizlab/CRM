import { TabViewPanelServiceBase } from '@/ibiz-core';
import { Util, HttpResponse } from '@/ibiz-core/utils';
import { MobTabExpView9tabviewpanelModel } from '@/app-core/ctrl-model/res-partner/mob-tab-exp-view9tabviewpanel-tabviewpanel-model';


/**
 * MobTabExpView9tabviewpanel 部件服务对象
 *
 * @export
 * @class MobTabExpView9tabviewpanelService
 * @extends {TabViewPanelServiceBase}
 */
export class MobTabExpView9tabviewpanelService extends TabViewPanelServiceBase {

    /**
     * 部件模型
     *
     * @protected
     * @type {MobTabExpView9tabviewpanelModel}
     * @memberof ControlServiceBase
     */
    protected model: MobTabExpView9tabviewpanelModel = new MobTabExpView9tabviewpanelModel();

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MobTabExpView9tabviewpanelService
     */
    protected appDEName: string = 'res_partner';

    /**
     * 当前应用实体主键标识
     *
     * @protected
     * @type {string}
     * @memberof MobTabExpView9tabviewpanelService
     */
    protected appDeKey: string = 'id';
}
// 默认导出
export default MobTabExpView9tabviewpanelService;