import { PortletServiceBase } from '@/ibiz-core';
import { Util, HttpResponse } from '@/ibiz-core/utils';
import { MobLeadChartModel } from '@/app-core/ctrl-model/crm-lead/mob-lead-chart-portlet-model';


/**
 * MobLeadChart 部件服务对象
 *
 * @export
 * @class MobLeadChartService
 * @extends {PortletServiceBase}
 */
export class MobLeadChartService extends PortletServiceBase {

    /**
     * 部件模型
     *
     * @protected
     * @type {MobLeadChartModel}
     * @memberof ControlServiceBase
     */
    protected model: MobLeadChartModel = new MobLeadChartModel();

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MobLeadChartService
     */
    protected appDEName: string = 'crm_lead';

    /**
     * 当前应用实体主键标识
     *
     * @protected
     * @type {string}
     * @memberof MobLeadChartService
     */
    protected appDeKey: string = 'id';
}
// 默认导出
export default MobLeadChartService;