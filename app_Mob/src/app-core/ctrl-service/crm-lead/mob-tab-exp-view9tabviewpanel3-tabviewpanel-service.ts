import { TabViewPanelServiceBase } from '@/ibiz-core';
import { Util, HttpResponse } from '@/ibiz-core/utils';
import { MobTabExpView9tabviewpanel3Model } from '@/app-core/ctrl-model/crm-lead/mob-tab-exp-view9tabviewpanel3-tabviewpanel-model';


/**
 * MobTabExpView9tabviewpanel3 部件服务对象
 *
 * @export
 * @class MobTabExpView9tabviewpanel3Service
 * @extends {TabViewPanelServiceBase}
 */
export class MobTabExpView9tabviewpanel3Service extends TabViewPanelServiceBase {

    /**
     * 部件模型
     *
     * @protected
     * @type {MobTabExpView9tabviewpanel3Model}
     * @memberof ControlServiceBase
     */
    protected model: MobTabExpView9tabviewpanel3Model = new MobTabExpView9tabviewpanel3Model();

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MobTabExpView9tabviewpanel3Service
     */
    protected appDEName: string = 'crm_lead';

    /**
     * 当前应用实体主键标识
     *
     * @protected
     * @type {string}
     * @memberof MobTabExpView9tabviewpanel3Service
     */
    protected appDeKey: string = 'id';
}
// 默认导出
export default MobTabExpView9tabviewpanel3Service;