import { ChartServiceBase } from '@/ibiz-core';
import { Util, HttpResponse } from '@/ibiz-core/utils';
import { OrderBarModel } from '@/app-core/ctrl-model/sale-order/order-bar-chart-model';


/**
 * OrderBar 部件服务对象
 *
 * @export
 * @class OrderBarService
 * @extends {ChartServiceBase}
 */
export class OrderBarService extends ChartServiceBase {

    /**
     * 部件模型
     *
     * @protected
     * @type {OrderBarModel}
     * @memberof ControlServiceBase
     */
    protected model: OrderBarModel = new OrderBarModel();

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof OrderBarService
     */
    protected appDEName: string = 'sale_order';

    /**
     * 当前应用实体主键标识
     *
     * @protected
     * @type {string}
     * @memberof OrderBarService
     */
    protected appDeKey: string = 'id';

    /**
     * 生成图表数据
     *
     * @param {string} action
     * @param {*} response
     * @memberof OrderBarService
     */
    public handleResponse(action: string, response: any): any {
        const chartOption: any = {};
        const catalogFields: any = ["user_id_text",];
        const valueFields: any = [[ "amount_total", "总计" ],];
        // 数据按分类属性分组处理
        const xFields:any = [];
        const yFields:any = [];
        valueFields.forEach((field: any, index: number) => {
            yFields[index] = [];
        });
        response.data.forEach((item:any) => {
            if(xFields.indexOf(item[catalogFields[0]]) > -1){
                const num = xFields.indexOf(item[catalogFields[0]]);
                valueFields.forEach((field: any,index: number) => {
                    yFields[index][num] += item[field[0]];
                });
            }else{
                xFields.push(item[catalogFields[0]]);
                valueFields.forEach((field: any,index: number) => {
                    yFields[index].push(item[field[0]]);
                });
            }
        });
        chartOption.xAxis = { data: xFields };
        const series: any = [];
        valueFields.forEach((field: any,index: number) => {
            const yData: any = [];
            xFields.forEach((item:any, num: number) => {
                yData.push(yFields[index][num]);
            });
            yData.sort(function (a:any, b:any) { return a.value - b.value; });
            series.push({
              name: field[1],
              type: "bar",
              data: yData,
            });
        });
        chartOption.series = series;
        return new HttpResponse(response.status, chartOption);
    }

}
// 默认导出
export default OrderBarService;