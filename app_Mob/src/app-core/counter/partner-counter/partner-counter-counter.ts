import { PartnerCounterCounterServiceBase } from './partner-counter-counter-base';

/**
 * 客户统计摘要计数器计数器服务对象
 *
 * @export
 * @class PartnerCounterCounterService
 * @extends {PartnerCounterCounterServiceBase}
 */
export class PartnerCounterCounterService extends PartnerCounterCounterServiceBase { }
// 默认导出
export default PartnerCounterCounterService;