import Vue from 'vue';
import Router from 'vue-router';
import { AuthGuard } from '@/utils';
import qs from 'qs';
import store from '@/store';

Vue.use(Router);

const router = new Router({
    routes: [
                {
            path: '/appindexview/:appindexview?',
            beforeEnter: (to: any, from: any, next: any) => {
                const routerParamsName = 'appindexview';
                const params: any = {};
                if (to.params && to.params[routerParamsName]) {
                    Object.assign(params, qs.parse(to.params[routerParamsName], { delimiter: ';' }));
                }
                const url: string = 'v7/appdata';
                const auth: Promise<any> = AuthGuard.getInstance().authGuard(url, params, store);
                auth.then(() => {
                    next();
                }).catch(() => {
                    next();
                });
            },
            meta: {  
                caption: 'CRM标准系统',
                viewType: 'APPINDEX',
                parameters: [
                    { pathName: 'appindexview', parameterName: 'appindexview' },
                ],
                requireAuth: true,
            },
            component: () => import('@pages/ungroup/app-index-view/app-index-view.vue'),
        },
        {
            path: '/viewshell/:viewshell?',
            beforeEnter: (to: any, from: any, next: any) => {
                const routerParamsName = 'appindexview';
                const params: any = {};
                if (to.params && to.params[routerParamsName]) {
                    Object.assign(params, qs.parse(to.params[routerParamsName], { delimiter: ';' }));
                }
                const url: string = 'v7/appdata';
                const auth: Promise<any> = AuthGuard.getInstance().authGuard(url, params, store);
                auth.then(() => {
                    next();
                }).catch(() => {
                    next();
                });
            },
            meta: {
                parameters: [
                    { pathName: 'viewshell', parameterName: 'viewshell' },
                ],
            },
            component: () => import('@/components/view-shell/view-shell.vue'),
            children: [
                {
                    path: 'res_partners/:res_partner?/mobpickupmdview/:mobpickupmdview?',
                    meta: {
                        caption: 'res_partner.views.mobpickupmdview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'mobpickupmdview', parameterName: 'mobpickupmdview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-mob-pickup-mdview/res-partner-mob-pickup-mdview.vue'),
                },
                {
                    path: 'product_pricelists/:product_pricelist?/mobmdview/:mobmdview?',
                    meta: {
                        caption: 'product_pricelist.views.mobmdview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'product_pricelists', parameterName: 'product_pricelist' },
                            { pathName: 'mobmdview', parameterName: 'mobmdview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-pricelist-mob-mdview/product-pricelist-mob-mdview.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/crm_leads/:crm_lead?/mobeditview/:mobeditview?',
                    meta: {
                        caption: 'crm_lead.views.mobeditview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'mobeditview', parameterName: 'mobeditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-mob-edit-view/crm-lead-mob-edit-view.vue'),
                },
                {
                    path: 'crm_leads/:crm_lead?/mobeditview/:mobeditview?',
                    meta: {
                        caption: 'crm_lead.views.mobeditview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'mobeditview', parameterName: 'mobeditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-mob-edit-view/crm-lead-mob-edit-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/mobmainview9/:mobmainview9?',
                    meta: {
                        caption: 'res_partner.views.mobmainview9.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'mobmainview9', parameterName: 'mobmainview9' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-mob-main-view9/res-partner-mob-main-view9.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/mobtabexpview9/:mobtabexpview9?',
                    meta: {
                        caption: 'res_partner.views.mobtabexpview9.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'mobtabexpview9', parameterName: 'mobtabexpview9' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-mob-tab-exp-view9/res-partner-mob-tab-exp-view9.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/account_invoices/:account_invoice?/mobmdview/:mobmdview?',
                    meta: {
                        caption: 'account_invoice.views.mobmdview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'account_invoices', parameterName: 'account_invoice' },
                            { pathName: 'mobmdview', parameterName: 'mobmdview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-account/account-invoice-mob-mdview/account-invoice-mob-mdview.vue'),
                },
                {
                    path: 'account_invoices/:account_invoice?/mobmdview/:mobmdview?',
                    meta: {
                        caption: 'account_invoice.views.mobmdview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'account_invoices', parameterName: 'account_invoice' },
                            { pathName: 'mobmdview', parameterName: 'mobmdview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-account/account-invoice-mob-mdview/account-invoice-mob-mdview.vue'),
                },
                {
                    path: 'mail_activities/:mail_activity?/mobcalendarview/:mobcalendarview?',
                    meta: {
                        caption: 'mail_activity.views.mobcalendarview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'mail_activities', parameterName: 'mail_activity' },
                            { pathName: 'mobcalendarview', parameterName: 'mobcalendarview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-mail/mail-activity-mob-calendar-view/mail-activity-mob-calendar-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/crm_leads/:crm_lead?/mobtabexpview9/:mobtabexpview9?',
                    meta: {
                        caption: 'crm_lead.views.mobtabexpview9.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'mobtabexpview9', parameterName: 'mobtabexpview9' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-mob-tab-exp-view9/crm-lead-mob-tab-exp-view9.vue'),
                },
                {
                    path: 'crm_leads/:crm_lead?/mobtabexpview9/:mobtabexpview9?',
                    meta: {
                        caption: 'crm_lead.views.mobtabexpview9.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'mobtabexpview9', parameterName: 'mobtabexpview9' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-mob-tab-exp-view9/crm-lead-mob-tab-exp-view9.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/crm_leads/:crm_lead?/mobmainview9/:mobmainview9?',
                    meta: {
                        caption: 'crm_lead.views.mobmainview9.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'mobmainview9', parameterName: 'mobmainview9' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-mob-main-view9/crm-lead-mob-main-view9.vue'),
                },
                {
                    path: 'crm_leads/:crm_lead?/mobmainview9/:mobmainview9?',
                    meta: {
                        caption: 'crm_lead.views.mobmainview9.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'mobmainview9', parameterName: 'mobmainview9' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-mob-main-view9/crm-lead-mob-main-view9.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/mobpickupview/:mobpickupview?',
                    meta: {
                        caption: 'res_partner.views.mobpickupview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'mobpickupview', parameterName: 'mobpickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-mob-pickup-view/res-partner-mob-pickup-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/sale_orders/:sale_order?/mobmdview/:mobmdview?',
                    meta: {
                        caption: 'sale_order.views.mobmdview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'sale_orders', parameterName: 'sale_order' },
                            { pathName: 'mobmdview', parameterName: 'mobmdview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-sale/sale-order-mob-mdview/sale-order-mob-mdview.vue'),
                },
                {
                    path: 'sale_orders/:sale_order?/mobmdview/:mobmdview?',
                    meta: {
                        caption: 'sale_order.views.mobmdview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'sale_orders', parameterName: 'sale_order' },
                            { pathName: 'mobmdview', parameterName: 'mobmdview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-sale/sale-order-mob-mdview/sale-order-mob-mdview.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/crm_leads/:crm_lead?/mobmaindashboardview/:mobmaindashboardview?',
                    meta: {
                        caption: 'crm_lead.views.mobmaindashboardview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'mobmaindashboardview', parameterName: 'mobmaindashboardview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-mob-main-dashboard-view/crm-lead-mob-main-dashboard-view.vue'),
                },
                {
                    path: 'crm_leads/:crm_lead?/mobmaindashboardview/:mobmaindashboardview?',
                    meta: {
                        caption: 'crm_lead.views.mobmaindashboardview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'mobmaindashboardview', parameterName: 'mobmaindashboardview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-mob-main-dashboard-view/crm-lead-mob-main-dashboard-view.vue'),
                },
                {
                    path: 'mail_activities/:mail_activity?/mobmdview9/:mobmdview9?',
                    meta: {
                        caption: 'mail_activity.views.mobmdview9.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'mail_activities', parameterName: 'mail_activity' },
                            { pathName: 'mobmdview9', parameterName: 'mobmdview9' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-mail/mail-activity-mob-mdview9/mail-activity-mob-mdview9.vue'),
                },
                {
                    path: 'product_categories/:product_category?/mobmdview/:mobmdview?',
                    meta: {
                        caption: 'product_category.views.mobmdview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'product_categories', parameterName: 'product_category' },
                            { pathName: 'mobmdview', parameterName: 'mobmdview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-category-mob-mdview/product-category-mob-mdview.vue'),
                },
                {
                    path: 'product_categories/:product_category?/mobeditview/:mobeditview?',
                    meta: {
                        caption: 'product_category.views.mobeditview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'product_categories', parameterName: 'product_category' },
                            { pathName: 'mobeditview', parameterName: 'mobeditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-category-mob-edit-view/product-category-mob-edit-view.vue'),
                },
                {
                    path: 'mail_messages/:mail_message?/mobeditview/:mobeditview?',
                    meta: {
                        caption: 'mail_message.views.mobeditview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'mail_messages', parameterName: 'mail_message' },
                            { pathName: 'mobeditview', parameterName: 'mobeditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-mail/mail-message-mob-edit-view/mail-message-mob-edit-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/mobmdview/:mobmdview?',
                    meta: {
                        caption: 'res_partner.views.mobmdview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'mobmdview', parameterName: 'mobmdview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-mob-mdview/res-partner-mob-mdview.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/account_invoices/:account_invoice?/mobeditview/:mobeditview?',
                    meta: {
                        caption: 'account_invoice.views.mobeditview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'account_invoices', parameterName: 'account_invoice' },
                            { pathName: 'mobeditview', parameterName: 'mobeditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-account/account-invoice-mob-edit-view/account-invoice-mob-edit-view.vue'),
                },
                {
                    path: 'account_invoices/:account_invoice?/mobeditview/:mobeditview?',
                    meta: {
                        caption: 'account_invoice.views.mobeditview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'account_invoices', parameterName: 'account_invoice' },
                            { pathName: 'mobeditview', parameterName: 'mobeditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-account/account-invoice-mob-edit-view/account-invoice-mob-edit-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/crm_leads/:crm_lead?/mobpanelview9/:mobpanelview9?',
                    meta: {
                        caption: 'crm_lead.views.mobpanelview9.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'mobpanelview9', parameterName: 'mobpanelview9' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-mob-panel-view9/crm-lead-mob-panel-view9.vue'),
                },
                {
                    path: 'crm_leads/:crm_lead?/mobpanelview9/:mobpanelview9?',
                    meta: {
                        caption: 'crm_lead.views.mobpanelview9.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'mobpanelview9', parameterName: 'mobpanelview9' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-mob-panel-view9/crm-lead-mob-panel-view9.vue'),
                },
                {
                    path: 'mail_activities/:mail_activity?/mobeditview/:mobeditview?',
                    meta: {
                        caption: 'mail_activity.views.mobeditview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'mail_activities', parameterName: 'mail_activity' },
                            { pathName: 'mobeditview', parameterName: 'mobeditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-mail/mail-activity-mob-edit-view/mail-activity-mob-edit-view.vue'),
                },
                {
                    path: 'product_pricelists/:product_pricelist?/mobeditview/:mobeditview?',
                    meta: {
                        caption: 'product_pricelist.views.mobeditview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'product_pricelists', parameterName: 'product_pricelist' },
                            { pathName: 'mobeditview', parameterName: 'mobeditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-pricelist-mob-edit-view/product-pricelist-mob-edit-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/sale_orders/:sale_order?/mobeditview/:mobeditview?',
                    meta: {
                        caption: 'sale_order.views.mobeditview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'sale_orders', parameterName: 'sale_order' },
                            { pathName: 'mobeditview', parameterName: 'mobeditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-sale/sale-order-mob-edit-view/sale-order-mob-edit-view.vue'),
                },
                {
                    path: 'sale_orders/:sale_order?/mobeditview/:mobeditview?',
                    meta: {
                        caption: 'sale_order.views.mobeditview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'sale_orders', parameterName: 'sale_order' },
                            { pathName: 'mobeditview', parameterName: 'mobeditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-sale/sale-order-mob-edit-view/sale-order-mob-edit-view.vue'),
                },
                {
                    path: 'product_products/:product_product?/mobpickupview/:mobpickupview?',
                    meta: {
                        caption: 'product_product.views.mobpickupview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'mobpickupview', parameterName: 'mobpickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-product-mob-pickup-view/product-product-mob-pickup-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/account_payments/:account_payment?/mobeditview/:mobeditview?',
                    meta: {
                        caption: 'account_payment.views.mobeditview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'account_payments', parameterName: 'account_payment' },
                            { pathName: 'mobeditview', parameterName: 'mobeditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-account/account-payment-mob-edit-view/account-payment-mob-edit-view.vue'),
                },
                {
                    path: 'account_payments/:account_payment?/mobeditview/:mobeditview?',
                    meta: {
                        caption: 'account_payment.views.mobeditview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'account_payments', parameterName: 'account_payment' },
                            { pathName: 'mobeditview', parameterName: 'mobeditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-account/account-payment-mob-edit-view/account-payment-mob-edit-view.vue'),
                },
                {
                    path: 'product_products/:product_product?/mobpickupmdview/:mobpickupmdview?',
                    meta: {
                        caption: 'product_product.views.mobpickupmdview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'product_products', parameterName: 'product_product' },
                            { pathName: 'mobpickupmdview', parameterName: 'mobpickupmdview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-product/product-product-mob-pickup-mdview/product-product-mob-pickup-mdview.vue'),
                },
                {
                    path: 'mail_messages/:mail_message?/mobmdview9/:mobmdview9?',
                    meta: {
                        caption: 'mail_message.views.mobmdview9.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'mail_messages', parameterName: 'mail_message' },
                            { pathName: 'mobmdview9', parameterName: 'mobmdview9' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-mail/mail-message-mob-mdview9/mail-message-mob-mdview9.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/mobmaindashboardview/:mobmaindashboardview?',
                    meta: {
                        caption: 'res_partner.views.mobmaindashboardview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'mobmaindashboardview', parameterName: 'mobmaindashboardview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-mob-main-dashboard-view/res-partner-mob-main-dashboard-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/mobmainview9_editmode/:mobmainview9_editmode?',
                    meta: {
                        caption: 'res_partner.views.mobmainview9_editmode.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'mobmainview9_editmode', parameterName: 'mobmainview9_editmode' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-mob-main-view9-edit-mode/res-partner-mob-main-view9-edit-mode.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/crm_leads/:crm_lead?/mobmdview/:mobmdview?',
                    meta: {
                        caption: 'crm_lead.views.mobmdview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'mobmdview', parameterName: 'mobmdview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-mob-mdview/crm-lead-mob-mdview.vue'),
                },
                {
                    path: 'crm_leads/:crm_lead?/mobmdview/:mobmdview?',
                    meta: {
                        caption: 'crm_lead.views.mobmdview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'mobmdview', parameterName: 'mobmdview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-mob-mdview/crm-lead-mob-mdview.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/sale_orders/:sale_order?/sale_order_lines/:sale_order_line?/mobmdview9/:mobmdview9?',
                    meta: {
                        caption: 'sale_order_line.views.mobmdview9.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'sale_orders', parameterName: 'sale_order' },
                            { pathName: 'sale_order_lines', parameterName: 'sale_order_line' },
                            { pathName: 'mobmdview9', parameterName: 'mobmdview9' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-sale/sale-order-line-mob-mdview9/sale-order-line-mob-mdview9.vue'),
                },
                {
                    path: 'sale_orders/:sale_order?/sale_order_lines/:sale_order_line?/mobmdview9/:mobmdview9?',
                    meta: {
                        caption: 'sale_order_line.views.mobmdview9.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'sale_orders', parameterName: 'sale_order' },
                            { pathName: 'sale_order_lines', parameterName: 'sale_order_line' },
                            { pathName: 'mobmdview9', parameterName: 'mobmdview9' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-sale/sale-order-line-mob-mdview9/sale-order-line-mob-mdview9.vue'),
                },
                {
                    path: 'sale_order_lines/:sale_order_line?/mobmdview9/:mobmdview9?',
                    meta: {
                        caption: 'sale_order_line.views.mobmdview9.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'sale_order_lines', parameterName: 'sale_order_line' },
                            { pathName: 'mobmdview9', parameterName: 'mobmdview9' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-sale/sale-order-line-mob-mdview9/sale-order-line-mob-mdview9.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/account_payments/:account_payment?/mobmdview/:mobmdview?',
                    meta: {
                        caption: 'account_payment.views.mobmdview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'account_payments', parameterName: 'account_payment' },
                            { pathName: 'mobmdview', parameterName: 'mobmdview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-account/account-payment-mob-mdview/account-payment-mob-mdview.vue'),
                },
                {
                    path: 'account_payments/:account_payment?/mobmdview/:mobmdview?',
                    meta: {
                        caption: 'account_payment.views.mobmdview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'account_payments', parameterName: 'account_payment' },
                            { pathName: 'mobmdview', parameterName: 'mobmdview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-account/account-payment-mob-mdview/account-payment-mob-mdview.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/crm_leads/:crm_lead?/mobmdview9/:mobmdview9?',
                    meta: {
                        caption: 'crm_lead.views.mobmdview9.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'mobmdview9', parameterName: 'mobmdview9' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-mob-mdview9/crm-lead-mob-mdview9.vue'),
                },
                {
                    path: 'crm_leads/:crm_lead?/mobmdview9/:mobmdview9?',
                    meta: {
                        caption: 'crm_lead.views.mobmdview9.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'crm_leads', parameterName: 'crm_lead' },
                            { pathName: 'mobmdview9', parameterName: 'mobmdview9' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-crm/crm-lead-mob-mdview9/crm-lead-mob-mdview9.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/sale_orders/:sale_order?/sale_order_lines/:sale_order_line?/mobeditview/:mobeditview?',
                    meta: {
                        caption: 'sale_order_line.views.mobeditview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'sale_orders', parameterName: 'sale_order' },
                            { pathName: 'sale_order_lines', parameterName: 'sale_order_line' },
                            { pathName: 'mobeditview', parameterName: 'mobeditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-sale/sale-order-line-mob-edit-view/sale-order-line-mob-edit-view.vue'),
                },
                {
                    path: 'sale_orders/:sale_order?/sale_order_lines/:sale_order_line?/mobeditview/:mobeditview?',
                    meta: {
                        caption: 'sale_order_line.views.mobeditview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'sale_orders', parameterName: 'sale_order' },
                            { pathName: 'sale_order_lines', parameterName: 'sale_order_line' },
                            { pathName: 'mobeditview', parameterName: 'mobeditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-sale/sale-order-line-mob-edit-view/sale-order-line-mob-edit-view.vue'),
                },
                {
                    path: 'sale_order_lines/:sale_order_line?/mobeditview/:mobeditview?',
                    meta: {
                        caption: 'sale_order_line.views.mobeditview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'sale_order_lines', parameterName: 'sale_order_line' },
                            { pathName: 'mobeditview', parameterName: 'mobeditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-sale/sale-order-line-mob-edit-view/sale-order-line-mob-edit-view.vue'),
                },
                {
                    path: 'appportalview/:appportalview?',
                    meta: {
                        caption: 'app.views.appportalview.caption',
                        parameters: [
                            { pathName: 'viewshell', parameterName: 'viewshell' },
                            { pathName: 'appportalview', parameterName: 'appportalview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/ungroup/app-portal-view/app-portal-view.vue'),
                },
            ]
        },
        {
            path: '/login/:login?',
            name: 'login',
            meta: {  
                caption: '登录',
                viewType: 'login',
                requireAuth: false,
                ignoreAddPage: true,
            },
            beforeEnter: (to: any, from: any, next: any) => {
                router.app.$store.commit('resetRootStateData');
                next();
            },
            component: () => import('@components/login/login.vue'),
        },
        {
            path: '/404',
            component: () => import('@components/404/404.vue')
        },
        {
            path: '/500',
            component: () => import('@components/500/500.vue')
        },
        {
            path: '*',
            redirect: 'appindexview'
        },
    ],
});

export default router;
