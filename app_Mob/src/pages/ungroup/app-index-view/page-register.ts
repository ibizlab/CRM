export const PageComponents = {
    install(Vue: any, opt: any) {
                Vue.component('res-partner-mob-pickup-mdview', () => import('@pages/odoo-base/res-partner-mob-pickup-mdview/res-partner-mob-pickup-mdview.vue'));
        Vue.component('product-pricelist-mob-mdview', () => import('@pages/odoo-product/product-pricelist-mob-mdview/product-pricelist-mob-mdview.vue'));
        Vue.component('crm-lead-mob-edit-view', () => import('@pages/odoo-crm/crm-lead-mob-edit-view/crm-lead-mob-edit-view.vue'));
        Vue.component('res-partner-mob-main-view9', () => import('@pages/odoo-base/res-partner-mob-main-view9/res-partner-mob-main-view9.vue'));
        Vue.component('res-partner-mob-tab-exp-view9', () => import('@pages/odoo-base/res-partner-mob-tab-exp-view9/res-partner-mob-tab-exp-view9.vue'));
        Vue.component('account-invoice-mob-mdview', () => import('@pages/odoo-account/account-invoice-mob-mdview/account-invoice-mob-mdview.vue'));
        Vue.component('mail-activity-mob-calendar-view', () => import('@pages/odoo-mail/mail-activity-mob-calendar-view/mail-activity-mob-calendar-view.vue'));
        Vue.component('crm-lead-mob-tab-exp-view9', () => import('@pages/odoo-crm/crm-lead-mob-tab-exp-view9/crm-lead-mob-tab-exp-view9.vue'));
        Vue.component('crm-lead-mob-main-view9', () => import('@pages/odoo-crm/crm-lead-mob-main-view9/crm-lead-mob-main-view9.vue'));
        Vue.component('res-partner-mob-pickup-view', () => import('@pages/odoo-base/res-partner-mob-pickup-view/res-partner-mob-pickup-view.vue'));
        Vue.component('sale-order-mob-mdview', () => import('@pages/odoo-sale/sale-order-mob-mdview/sale-order-mob-mdview.vue'));
        Vue.component('crm-lead-mob-main-dashboard-view', () => import('@pages/odoo-crm/crm-lead-mob-main-dashboard-view/crm-lead-mob-main-dashboard-view.vue'));
        Vue.component('mail-activity-mob-mdview9', () => import('@pages/odoo-mail/mail-activity-mob-mdview9/mail-activity-mob-mdview9.vue'));
        Vue.component('product-category-mob-mdview', () => import('@pages/odoo-product/product-category-mob-mdview/product-category-mob-mdview.vue'));
        Vue.component('product-category-mob-edit-view', () => import('@pages/odoo-product/product-category-mob-edit-view/product-category-mob-edit-view.vue'));
        Vue.component('mail-message-mob-edit-view', () => import('@pages/odoo-mail/mail-message-mob-edit-view/mail-message-mob-edit-view.vue'));
        Vue.component('res-partner-mob-mdview', () => import('@pages/odoo-base/res-partner-mob-mdview/res-partner-mob-mdview.vue'));
        Vue.component('account-invoice-mob-edit-view', () => import('@pages/odoo-account/account-invoice-mob-edit-view/account-invoice-mob-edit-view.vue'));
        Vue.component('crm-lead-mob-panel-view9', () => import('@pages/odoo-crm/crm-lead-mob-panel-view9/crm-lead-mob-panel-view9.vue'));
        Vue.component('mail-activity-mob-edit-view', () => import('@pages/odoo-mail/mail-activity-mob-edit-view/mail-activity-mob-edit-view.vue'));
        Vue.component('product-pricelist-mob-edit-view', () => import('@pages/odoo-product/product-pricelist-mob-edit-view/product-pricelist-mob-edit-view.vue'));
        Vue.component('sale-order-mob-edit-view', () => import('@pages/odoo-sale/sale-order-mob-edit-view/sale-order-mob-edit-view.vue'));
        Vue.component('product-product-mob-pickup-view', () => import('@pages/odoo-product/product-product-mob-pickup-view/product-product-mob-pickup-view.vue'));
        Vue.component('account-payment-mob-edit-view', () => import('@pages/odoo-account/account-payment-mob-edit-view/account-payment-mob-edit-view.vue'));
        Vue.component('product-product-mob-pickup-mdview', () => import('@pages/odoo-product/product-product-mob-pickup-mdview/product-product-mob-pickup-mdview.vue'));
        Vue.component('mail-message-mob-mdview9', () => import('@pages/odoo-mail/mail-message-mob-mdview9/mail-message-mob-mdview9.vue'));
        Vue.component('res-partner-mob-main-dashboard-view', () => import('@pages/odoo-base/res-partner-mob-main-dashboard-view/res-partner-mob-main-dashboard-view.vue'));
        Vue.component('res-partner-mob-main-view9-edit-mode', () => import('@pages/odoo-base/res-partner-mob-main-view9-edit-mode/res-partner-mob-main-view9-edit-mode.vue'));
        Vue.component('crm-lead-mob-mdview', () => import('@pages/odoo-crm/crm-lead-mob-mdview/crm-lead-mob-mdview.vue'));
        Vue.component('sale-order-line-mob-mdview9', () => import('@pages/odoo-sale/sale-order-line-mob-mdview9/sale-order-line-mob-mdview9.vue'));
        Vue.component('account-payment-mob-mdview', () => import('@pages/odoo-account/account-payment-mob-mdview/account-payment-mob-mdview.vue'));
        Vue.component('crm-lead-mob-mdview9', () => import('@pages/odoo-crm/crm-lead-mob-mdview9/crm-lead-mob-mdview9.vue'));
        Vue.component('sale-order-line-mob-edit-view', () => import('@pages/odoo-sale/sale-order-line-mob-edit-view/sale-order-line-mob-edit-view.vue'));
        Vue.component('app-portal-view', () => import('@pages/ungroup/app-portal-view/app-portal-view.vue'));
    }
};