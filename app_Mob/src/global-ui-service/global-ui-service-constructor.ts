import { ServiceConstructorBase } from '@/ibiz-core/service/service-constructor-base';

/**
 * 应用实体服务
 *
 * @export
 * @class GlobalUiServiceConstructor
 * @extends {ServiceConstructorBase}
 */
export default class GlobalUiServiceConstructor extends ServiceConstructorBase {

    /**
     * 初始化
     *
     * @protected
     * @memberof GlobalUiServiceConstructor
     */
    protected init(): void {
        this.allService.set('res_users_ui_action', () => import('@/ui-service/res-users/res-users-ui-action'));
        this.allService.set('res_partner_ui_action', () => import('@/ui-service/res-partner/res-partner-ui-action'));
    }

}