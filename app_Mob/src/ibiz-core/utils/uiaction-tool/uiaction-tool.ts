/**
 * 界面行为工具类
 */
export class UIActionTool {


    /**
     * 处理视图上下文
     *
     * @static
     * @param {*} actionTarget
     * @param {*} _args
     * @param {*} [context={}]
     * @returns {*}
     * @memberof UIActionTool
     */
    public static handleContextParam(actionTarget: any, _args: any, context: any = {}): any {
        return context
    }

    /**
     * 处理行为参数
     *
     * @static
     * @param {*} actionTarget
     * @param {*} _args
     * @param {*} [params={}]
     * @returns {*}
     * @memberof UIActionTool
     */
    public static handleActionParam(actionTarget: any, _args: any, params: any = {}): any {
        return params
    }


    /**
     * 处理界面行为参数
     * 
     * @param args  传入数据参数
     * @param context 传入行为附加上下文
     * @param params  传入行为附加参数
     * @param data 处理之后的数据
     */
    public static handleActionParams(args: any, context: any, params: any, data: any) {
        this.handleSingleParam(args, context, data);
        this.handleSingleParam(args, params, data);
    }

    /**
     * 处理单个参数
     * 
     * @param args 传入数据参数
     * @param param 传入行为附加参数
     */
    public static handleSingleParam(args: any, params: any, data: any) {
        if (params && Object.keys(params).length > 0) {
            const _params: any = {};
            const arg: any = args[0];
            Object.keys(params).forEach((name: string) => {
                if (!name) {
                    return;
                }
                let value: string | null = params[name];
                if (value && value.startsWith('%') && value.endsWith('%')) {
                    const key = value.substring(1, value.length - 1);
                    if (arg && arg.hasOwnProperty(key)) {
                        value = (arg[key] !== null && arg[key] !== undefined) ? arg[key] : null;
                    } else {
                        value = null;
                    }
                }
                Object.assign(_params, { [name]: value });
            });
            Object.assign(data, _params);
        }
    }
}