/**
 * 所有应用视图
 */
export const viewstate: any = {
    appviews: [
        {
            viewtag: '018dccda3c6c920bfb2980ddb03fb4cb',
            viewmodule: 'odoo_base',
            viewname: 'res_partnerMobPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'ffee6f12f0d08a52de068e28cce48c01',
            ],
        },
        {
            viewtag: '0243f3e2be8dbfd83a730ebb33668c48',
            viewmodule: 'odoo_product',
            viewname: 'product_productMobMPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'e67f14c03e5efe8789620215d3bc6efa',
            ],
        },
        {
            viewtag: '05957341115cd06f6a966c6a67324dc9',
            viewmodule: 'odoo_base',
            viewname: 'res_partnerMobMainDashboardView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'c40e08b7aa071cc9cf2555c6cf3d37bb',
                'a63e27336dcfbcbfdaef51b4851aa053',
                'ab496e8f263aea88b64ecda1fb55a6dd',
                '64019dbfceb8f82774a1138ddff34525',
                'da1c259a853c2b25c40c18065bf071c9',
            ],
        },
        {
            viewtag: '05d22d26347c0bd08a1c3a753edb83e0',
            viewmodule: 'odoo_sale',
            viewname: 'sale_orderMobEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '018dccda3c6c920bfb2980ddb03fb4cb',
                'd238df7368e482aa3fdab8ce5cae83fe',
            ],
        },
        {
            viewtag: '0a3f699f366f0e3d8f018b8ca4947298',
            viewmodule: 'odoo_product',
            viewname: 'product_productMobEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '0c98ee2d0e604f1cf7df51989bd5d2cf',
            viewmodule: 'odoo_crm',
            viewname: 'crm_leadMobTabExpView9',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'a6414c1332620971c8cb0e224cf1de53',
                '91b13cefe06f3ee2c1a4485d980900b1',
                'c63ce8b701c59d50f9aa6af4f87e1c3b',
            ],
        },
        {
            viewtag: '245aebf9efbc646ec4a5965b1cb74f0e',
            viewmodule: 'odoo_account',
            viewname: 'account_paymentMobEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '2d302b4fb2b19d7d7d61efe57f74af31',
            viewmodule: 'odoo_product',
            viewname: 'product_productMobMDView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '0a3f699f366f0e3d8f018b8ca4947298',
            ],
        },
        {
            viewtag: '40be893a0844bae5eb7c68a66ec3c0aa',
            viewmodule: 'odoo_product',
            viewname: 'product_productMobPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'e67f14c03e5efe8789620215d3bc6efa',
            ],
        },
        {
            viewtag: '438a2301b07a1f9c33f9b8ba380eb8a8',
            viewmodule: 'odoo_account',
            viewname: 'account_invoiceMobEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '5584c8108a110b8e5b12e8f075f4f5ec',
            viewmodule: 'odoo_account',
            viewname: 'account_paymentMobMDView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '245aebf9efbc646ec4a5965b1cb74f0e',
            ],
        },
        {
            viewtag: '57fa709c88c2c3f67d526db0bc5aee87',
            viewmodule: 'Ungroup',
            viewname: 'AppPortalView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'c8ff7ae245c052f9c670b610981a2f5b',
                'd0bb89a0b2ad5136f5e94397271adc54',
                '980630b2d965ba5074733e56a5a3ecd7',
                'f4f82e8bba6a57bde7b9615ef8539be7',
                '64019dbfceb8f82774a1138ddff34525',
                '5584c8108a110b8e5b12e8f075f4f5ec',
                'da1c259a853c2b25c40c18065bf071c9',
            ],
        },
        {
            viewtag: '5d26fe75e3416a9d4df2cc8a0ff5ff2c',
            viewmodule: 'odoo_mail',
            viewname: 'mail_activityMobMDView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'cf6552c1f5732dcac2eff5fb25052f01',
            ],
        },
        {
            viewtag: '5e255d910cf70469737eba86437dc2f8',
            viewmodule: 'odoo_product',
            viewname: 'product_categoryMobEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '64019dbfceb8f82774a1138ddff34525',
            viewmodule: 'odoo_sale',
            viewname: 'sale_orderMobMDView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '05d22d26347c0bd08a1c3a753edb83e0',
            ],
        },
        {
            viewtag: '6a3de46ef6f2a7beb2388d5efbd9d65b',
            viewmodule: 'odoo_crm',
            viewname: 'crm_leadMobMainDashboardView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '0c98ee2d0e604f1cf7df51989bd5d2cf',
                '74ca5263d23667d6bc0bda8eccb6f8ad',
            ],
        },
        {
            viewtag: '74ca5263d23667d6bc0bda8eccb6f8ad',
            viewmodule: 'odoo_crm',
            viewname: 'crm_leadMobPanelView9',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '76814c7b0cea389974c389a5df084588',
            viewmodule: 'odoo_mail',
            viewname: 'mail_activityMobCalendarView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'cf6552c1f5732dcac2eff5fb25052f01',
            ],
        },
        {
            viewtag: '7816ed6c2da367374ab22979e1083ea4',
            viewmodule: 'odoo_product',
            viewname: 'product_pricelistMobEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '7f305e69fef591271653071c641ce82c',
            viewmodule: 'Ungroup',
            viewname: 'AppIndexView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '76814c7b0cea389974c389a5df084588',
                '57fa709c88c2c3f67d526db0bc5aee87',
            ],
        },
        {
            viewtag: '91b13cefe06f3ee2c1a4485d980900b1',
            viewmodule: 'odoo_mail',
            viewname: 'mail_messageMobMDView9',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'ea8a08606126b1ca3430fa910eb68ad8',
            ],
        },
        {
            viewtag: '980630b2d965ba5074733e56a5a3ecd7',
            viewmodule: 'odoo_base',
            viewname: 'res_partnerMobMDView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '05957341115cd06f6a966c6a67324dc9',
                'a63e27336dcfbcbfdaef51b4851aa053',
            ],
        },
        {
            viewtag: '9aa97dd968513874afb11efb363ca5b0',
            viewmodule: 'odoo_base',
            viewname: 'res_partnerMobEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'a63e27336dcfbcbfdaef51b4851aa053',
            viewmodule: 'odoo_base',
            viewname: 'res_partnerMobMainView9_EditMode',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'a6414c1332620971c8cb0e224cf1de53',
            viewmodule: 'odoo_mail',
            viewname: 'mail_activityMobMDView9',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'cf6552c1f5732dcac2eff5fb25052f01',
            ],
        },
        {
            viewtag: 'ab496e8f263aea88b64ecda1fb55a6dd',
            viewmodule: 'odoo_base',
            viewname: 'res_partnerMobTabExpView9',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'a6414c1332620971c8cb0e224cf1de53',
                'be15e70cb42dbcc17654630a2e6f8306',
            ],
        },
        {
            viewtag: 'be15e70cb42dbcc17654630a2e6f8306',
            viewmodule: 'odoo_crm',
            viewname: 'crm_leadMobMDView9',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'd8d40138a03da822dae8ae9f0fcee04c',
                '6a3de46ef6f2a7beb2388d5efbd9d65b',
            ],
        },
        {
            viewtag: 'c40e08b7aa071cc9cf2555c6cf3d37bb',
            viewmodule: 'odoo_base',
            viewname: 'res_partnerMobMainView9',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'c63ce8b701c59d50f9aa6af4f87e1c3b',
            viewmodule: 'odoo_crm',
            viewname: 'crm_leadMobMainView9',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'c8ff7ae245c052f9c670b610981a2f5b',
            viewmodule: 'odoo_product',
            viewname: 'product_pricelistMobMDView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '7816ed6c2da367374ab22979e1083ea4',
            ],
        },
        {
            viewtag: 'cf6552c1f5732dcac2eff5fb25052f01',
            viewmodule: 'odoo_mail',
            viewname: 'mail_activityMobEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'd0bb89a0b2ad5136f5e94397271adc54',
            viewmodule: 'odoo_product',
            viewname: 'product_categoryMobMDView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '5e255d910cf70469737eba86437dc2f8',
            ],
        },
        {
            viewtag: 'd238df7368e482aa3fdab8ce5cae83fe',
            viewmodule: 'odoo_sale',
            viewname: 'Sale_order_lineMobMDView9',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'dcd67e75868c6686f357ecaa1e6379b7',
            ],
        },
        {
            viewtag: 'd8d40138a03da822dae8ae9f0fcee04c',
            viewmodule: 'odoo_crm',
            viewname: 'crm_leadMobEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'd9e3e62df800a6c17189ecaa12c0a37d',
            viewmodule: 'odoo_base',
            viewname: 'res_partnerMobTabExpView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'a6414c1332620971c8cb0e224cf1de53',
                'be15e70cb42dbcc17654630a2e6f8306',
            ],
        },
        {
            viewtag: 'da1c259a853c2b25c40c18065bf071c9',
            viewmodule: 'odoo_account',
            viewname: 'account_invoiceMobMDView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '438a2301b07a1f9c33f9b8ba380eb8a8',
            ],
        },
        {
            viewtag: 'dcd67e75868c6686f357ecaa1e6379b7',
            viewmodule: 'odoo_sale',
            viewname: 'Sale_order_lineMobEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '40be893a0844bae5eb7c68a66ec3c0aa',
            ],
        },
        {
            viewtag: 'e67f14c03e5efe8789620215d3bc6efa',
            viewmodule: 'odoo_product',
            viewname: 'product_productMobPickupMDView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'ea8a08606126b1ca3430fa910eb68ad8',
            viewmodule: 'odoo_mail',
            viewname: 'mail_messageMobEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'f4f82e8bba6a57bde7b9615ef8539be7',
            viewmodule: 'odoo_crm',
            viewname: 'crm_leadMobMDView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'd8d40138a03da822dae8ae9f0fcee04c',
                '6a3de46ef6f2a7beb2388d5efbd9d65b',
            ],
        },
        {
            viewtag: 'ffee6f12f0d08a52de068e28cce48c01',
            viewmodule: 'odoo_base',
            viewname: 'res_partnerMobPickupMDView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
    ],
    createdviews: [],
}