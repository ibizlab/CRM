package cn.ibizlab.odoo.mob.odoo_mail.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.mob.odoo_mail.service.Mail_activityService;
import cn.ibizlab.odoo.mob.odoo_mail.domain.Mail_activity;
import cn.ibizlab.odoo.mob.odoo_mail.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Mail_activityController {
	@Autowired
    Mail_activityService mail_activityservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/mail_activities/getdraft")
    @PreAuthorize("@mail_activity_pms.check('CREATE')")
    public ResponseEntity<Mail_activity> getDraft() {
        //Mail_activity mail_activity = mail_activityservice.getDraft( mail_activity_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Mail_activity());
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_activities/{mail_activity_id}/removebatch")
    @PreAuthorize("@mail_activity_pms.check(#mail_activity_id,'DELETE')")
    public ResponseEntity<Mail_activity> removeBatch(@PathVariable("mail_activity_id") Integer mail_activity_id, @RequestBody Mail_activity mail_activity) {
        Mail_activity mail_activity2 = mail_activityservice.removeBatch(mail_activity_id, mail_activity);
        return ResponseEntity.status(HttpStatus.OK).body(mail_activity2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_activities/{mail_activity_id}")
    @PreAuthorize("@mail_activity_pms.check(#mail_activity_id,'UPDATE')")
    public ResponseEntity<Mail_activity> update(@PathVariable("mail_activity_id") Integer mail_activity_id, @RequestBody Mail_activity mail_activity) {
        Mail_activity mail_activity2 = mail_activityservice.update(mail_activity_id, mail_activity);
        return ResponseEntity.status(HttpStatus.OK).body(mail_activity2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/mail_activities/{mail_activity_id}")
    @PreAuthorize("@mail_activity_pms.check(#mail_activity_id,'UPDATE')")
    public ResponseEntity<Mail_activity> api_update(@PathVariable("mail_activity_id") Integer mail_activity_id, @RequestBody Mail_activity mail_activity) {
        Mail_activity mail_activity2 = mail_activityservice.update(mail_activity_id, mail_activity);
        return ResponseEntity.status(HttpStatus.OK).body(mail_activity2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_activities/{mail_activity_id}/updatebatch")
    @PreAuthorize("@mail_activity_pms.check(#mail_activity_id,'UPDATE')")
    public ResponseEntity<Mail_activity> updateBatch(@PathVariable("mail_activity_id") Integer mail_activity_id, @RequestBody Mail_activity mail_activity) {
        Mail_activity mail_activity2 = mail_activityservice.updateBatch(mail_activity_id, mail_activity);
        return ResponseEntity.status(HttpStatus.OK).body(mail_activity2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activities/checkkey")
    @PreAuthorize("@mail_activity_pms.check('')")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_activity mail_activity) {
        boolean b = mail_activityservice.checkKey(mail_activity);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activities/{mail_activity_id}/save")
    @PreAuthorize("@mail_activity_pms.check(#mail_activity_id,'')")
    public ResponseEntity<Mail_activity> save(@PathVariable("mail_activity_id") Integer mail_activity_id, @RequestBody Mail_activity mail_activity) {
        Mail_activity mail_activity2 = mail_activityservice.save(mail_activity_id, mail_activity);
        return ResponseEntity.status(HttpStatus.OK).body(mail_activity2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_activities/{mail_activity_id}")
    @PreAuthorize("@mail_activity_pms.check(#mail_activity_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_activity_id") Integer mail_activity_id) {
        boolean b = mail_activityservice.remove( mail_activity_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activities/{mail_activity_id}/createbatch")
    @PreAuthorize("@mail_activity_pms.check(#mail_activity_id,'CREATE')")
    public ResponseEntity<Mail_activity> createBatch(@PathVariable("mail_activity_id") Integer mail_activity_id, @RequestBody Mail_activity mail_activity) {
        Mail_activity mail_activity2 = mail_activityservice.createBatch(mail_activity_id, mail_activity);
        return ResponseEntity.status(HttpStatus.OK).body(mail_activity2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/mail_activities/{mail_activity_id}")
    @PreAuthorize("@mail_activity_pms.check(#mail_activity_id,'READ')")
    public ResponseEntity<Mail_activity> get(@PathVariable("mail_activity_id") Integer mail_activity_id) {
        Mail_activity mail_activity = mail_activityservice.get( mail_activity_id);
        return ResponseEntity.status(HttpStatus.OK).body(mail_activity);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activities")
    @PreAuthorize("@mail_activity_pms.check('CREATE')")
    public ResponseEntity<Mail_activity> create(@RequestBody Mail_activity mail_activity) {
        Mail_activity mail_activity2 = mail_activityservice.create(mail_activity);
        return ResponseEntity.status(HttpStatus.OK).body(mail_activity2);
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/mail_activities/fetchdefault")
    @PreAuthorize("@mail_activity_pms.check('READ')")
	public ResponseEntity<List<Mail_activity>> fetchDefault(Mail_activitySearchContext searchContext,Pageable pageable) {
        
        Page<Mail_activity> page = mail_activityservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}
