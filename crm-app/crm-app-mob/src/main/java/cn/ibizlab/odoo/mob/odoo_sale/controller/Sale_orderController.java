package cn.ibizlab.odoo.mob.odoo_sale.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.mob.odoo_sale.service.Sale_orderService;
import cn.ibizlab.odoo.mob.odoo_sale.domain.Sale_order;
import cn.ibizlab.odoo.mob.odoo_sale.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Sale_orderController {
	@Autowired
    Sale_orderService sale_orderservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/sale_orders/{sale_order_id}/updatebatch")
    @PreAuthorize("@sale_order_pms.check(#sale_order_id,'UPDATE')")
    public ResponseEntity<Sale_order> updateBatch(@PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_order sale_order) {
        Sale_order sale_order2 = sale_orderservice.updateBatch(sale_order_id, sale_order);
        return ResponseEntity.status(HttpStatus.OK).body(sale_order2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/sale_orders/{sale_order_id}/removebatch")
    @PreAuthorize("@sale_order_pms.check(#sale_order_id,'DELETE')")
    public ResponseEntity<Sale_order> removeBatch(@PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_order sale_order) {
        Sale_order sale_order2 = sale_orderservice.removeBatch(sale_order_id, sale_order);
        return ResponseEntity.status(HttpStatus.OK).body(sale_order2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/sale_orders/{sale_order_id}")
    @PreAuthorize("@sale_order_pms.check(#sale_order_id,'UPDATE')")
    public ResponseEntity<Sale_order> update(@PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_order sale_order) {
        Sale_order sale_order2 = sale_orderservice.update(sale_order_id, sale_order);
        return ResponseEntity.status(HttpStatus.OK).body(sale_order2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/sale_orders/{sale_order_id}")
    @PreAuthorize("@sale_order_pms.check(#sale_order_id,'UPDATE')")
    public ResponseEntity<Sale_order> api_update(@PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_order sale_order) {
        Sale_order sale_order2 = sale_orderservice.update(sale_order_id, sale_order);
        return ResponseEntity.status(HttpStatus.OK).body(sale_order2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/sale_orders/{sale_order_id}")
    @PreAuthorize("@sale_order_pms.check(#sale_order_id,'READ')")
    public ResponseEntity<Sale_order> get(@PathVariable("sale_order_id") Integer sale_order_id) {
        Sale_order sale_order = sale_orderservice.get( sale_order_id);
        return ResponseEntity.status(HttpStatus.OK).body(sale_order);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/sale_orders/{sale_order_id}/save")
    @PreAuthorize("@sale_order_pms.check(#sale_order_id,'')")
    public ResponseEntity<Sale_order> save(@PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_order sale_order) {
        Sale_order sale_order2 = sale_orderservice.save(sale_order_id, sale_order);
        return ResponseEntity.status(HttpStatus.OK).body(sale_order2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/sale_orders/checkkey")
    @PreAuthorize("@sale_order_pms.check('')")
    public ResponseEntity<Boolean> checkKey(@RequestBody Sale_order sale_order) {
        boolean b = sale_orderservice.checkKey(sale_order);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/sale_orders/{sale_order_id}")
    @PreAuthorize("@sale_order_pms.check(#sale_order_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("sale_order_id") Integer sale_order_id) {
        boolean b = sale_orderservice.remove( sale_order_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/sale_orders/getdraft")
    @PreAuthorize("@sale_order_pms.check('CREATE')")
    public ResponseEntity<Sale_order> getDraft() {
        //Sale_order sale_order = sale_orderservice.getDraft( sale_order_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Sale_order());
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/sale_orders/{sale_order_id}/createbatch")
    @PreAuthorize("@sale_order_pms.check(#sale_order_id,'CREATE')")
    public ResponseEntity<Sale_order> createBatch(@PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_order sale_order) {
        Sale_order sale_order2 = sale_orderservice.createBatch(sale_order_id, sale_order);
        return ResponseEntity.status(HttpStatus.OK).body(sale_order2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/sale_orders")
    @PreAuthorize("@sale_order_pms.check('CREATE')")
    public ResponseEntity<Sale_order> create(@RequestBody Sale_order sale_order) {
        Sale_order sale_order2 = sale_orderservice.create(sale_order);
        return ResponseEntity.status(HttpStatus.OK).body(sale_order2);
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/sale_orders/fetchdefault")
    @PreAuthorize("@sale_order_pms.check('READ')")
	public ResponseEntity<List<Sale_order>> fetchDefault(Sale_orderSearchContext searchContext,Pageable pageable) {
        
        Page<Sale_order> page = sale_orderservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


	@RequestMapping(method = RequestMethod.PUT, value = "/res_partners/{res_partner_id}/sale_orders/{sale_order_id}/updatebatch")
    @PreAuthorize("@sale_order_pms.checkByRes_partner(#res_partner_id,#sale_order_id,'UPDATE')")
    public ResponseEntity<Sale_order> updateBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_order sale_order) {
        Sale_order sale_order2 = sale_orderservice.updateBatchByRes_partner(res_partner_id, sale_order_id, sale_order);
        return ResponseEntity.status(HttpStatus.OK).body(sale_order2);
    }

	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partners/{res_partner_id}/sale_orders/{sale_order_id}/removebatch")
    @PreAuthorize("@sale_order_pms.checkByRes_partner(#res_partner_id,#sale_order_id,'DELETE')")
    public ResponseEntity<Sale_order> removeBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_order sale_order) {
        Sale_order sale_order2 = sale_orderservice.removeBatchByRes_partner(res_partner_id, sale_order_id, sale_order);
        return ResponseEntity.status(HttpStatus.OK).body(sale_order2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/res_partners/{res_partner_id}/sale_orders/{sale_order_id}")
    @PreAuthorize("@sale_order_pms.checkByRes_partner(#res_partner_id,#sale_order_id,'UPDATE')")
    public ResponseEntity<Sale_order> updateByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_order sale_order) {
        Sale_order sale_order2 = sale_orderservice.updateByRes_partner(res_partner_id, sale_order_id, sale_order);
        return ResponseEntity.status(HttpStatus.OK).body(sale_order2);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/api/res_partners/{res_partner_id}/sale_orders/{sale_order_id}")
    @PreAuthorize("@sale_order_pms.checkByRes_partner(#res_partner_id,#sale_order_id,'UPDATE')")
    public ResponseEntity<Sale_order> api_updateByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_order sale_order) {
        Sale_order sale_order2 = sale_orderservice.updateByRes_partner(res_partner_id, sale_order_id, sale_order);
        return ResponseEntity.status(HttpStatus.OK).body(sale_order2);
    }

	@RequestMapping(method = RequestMethod.GET, value = "/res_partners/{res_partner_id}/sale_orders/{sale_order_id}")
    @PreAuthorize("@sale_order_pms.checkByRes_partner(#res_partner_id,#sale_order_id,'READ')")
    public ResponseEntity<Sale_order> getByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id) {
        Sale_order sale_order = sale_orderservice.getByRes_partner(res_partner_id,  sale_order_id);
        return ResponseEntity.status(HttpStatus.OK).body(sale_order);
    }

	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/sale_orders/{sale_order_id}/save")
    @PreAuthorize("@sale_order_pms.checkByRes_partner(#res_partner_id,#sale_order_id,'')")
    public ResponseEntity<Sale_order> saveByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_order sale_order) {
        Sale_order sale_order2 = sale_orderservice.saveByRes_partner(res_partner_id, sale_order_id, sale_order);
        return ResponseEntity.status(HttpStatus.OK).body(sale_order2);
    }

	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/sale_orders/checkkey")
    @PreAuthorize("@sale_order_pms.checkByRes_partner(#res_partner_id,'')")
    public ResponseEntity<Boolean> checkKeyByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Sale_order sale_order) {
        boolean b = sale_orderservice.checkKeyByRes_partner(res_partner_id, sale_order);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partners/{res_partner_id}/sale_orders/{sale_order_id}")
    @PreAuthorize("@sale_order_pms.checkByRes_partner(#res_partner_id,#sale_order_id,'DELETE')")
    public ResponseEntity<Boolean> removeByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id) {
        boolean b = sale_orderservice.removeByRes_partner(res_partner_id,  sale_order_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

	@RequestMapping(method = RequestMethod.GET, value = "/res_partners/{res_partner_id}/sale_orders/getdraft")
    @PreAuthorize("@sale_order_pms.checkByRes_partner(#res_partner_id,'CREATE')")
    public ResponseEntity<Sale_order> getDraftByRes_partner() {
        //Sale_order sale_order = sale_orderservice.getDraftByRes_partner(res_partner_id,  sale_order_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Sale_order());
    }

	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/sale_orders/{sale_order_id}/createbatch")
    @PreAuthorize("@sale_order_pms.checkByRes_partner(#res_partner_id,#sale_order_id,'CREATE')")
    public ResponseEntity<Sale_order> createBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_order sale_order) {
        Sale_order sale_order2 = sale_orderservice.createBatchByRes_partner(res_partner_id, sale_order_id, sale_order);
        return ResponseEntity.status(HttpStatus.OK).body(sale_order2);
    }

	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/sale_orders")
    @PreAuthorize("@sale_order_pms.checkByRes_partner(#res_partner_id,'CREATE')")
    public ResponseEntity<Sale_order> createByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Sale_order sale_order) {
        Sale_order sale_order2 = sale_orderservice.createByRes_partner(res_partner_id, sale_order);
        return ResponseEntity.status(HttpStatus.OK).body(sale_order2);
    }

    @RequestMapping(method= RequestMethod.GET , value="/res_partners/{res_partner_id}/sale_orders/fetchdefault")
    @PreAuthorize("@sale_order_pms.checkByRes_partner(#res_partner_id,'READ')")
	public ResponseEntity<List<Sale_order>> fetchDefaultByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id,Sale_orderSearchContext searchContext,Pageable pageable) {
        searchContext.setN_partner_id_eq(res_partner_id);
        Page<Sale_order> page = sale_orderservice.fetchDefaultByRes_partner(res_partner_id , searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}
