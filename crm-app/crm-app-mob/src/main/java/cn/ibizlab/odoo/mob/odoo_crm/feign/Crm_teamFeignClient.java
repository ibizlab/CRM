package cn.ibizlab.odoo.mob.odoo_crm.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_team;
import cn.ibizlab.odoo.mob.odoo_crm.filter.*;


public interface Crm_teamFeignClient {



	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_teams/{crm_team_id}")
    public Crm_team get(@PathVariable("crm_team_id") Integer crm_team_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_teams/checkkey")
    public Boolean checkKey(@RequestBody Crm_team crm_team) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_teams/{crm_team_id}/getdraft")
    public Crm_team getDraft(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody Crm_team crm_team) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_teams/{crm_team_id}/removebatch")
    public Crm_team removeBatch(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody Crm_team crm_team) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_teams/{crm_team_id}/createbatch")
    public Crm_team createBatch(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody Crm_team crm_team) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_teams/{crm_team_id}")
    public Crm_team update(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody Crm_team crm_team) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_teams/{crm_team_id}")
    public Boolean remove(@PathVariable("crm_team_id") Integer crm_team_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_teams")
    public Crm_team create(@RequestBody Crm_team crm_team) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_teams/{crm_team_id}/updatebatch")
    public Crm_team updateBatch(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody Crm_team crm_team) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_teams/{crm_team_id}/save")
    public Crm_team save(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody Crm_team crm_team) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/crm_teams/fetchdefault")
	public Page<Crm_team> fetchDefault(Crm_teamSearchContext searchContext) ;
}
