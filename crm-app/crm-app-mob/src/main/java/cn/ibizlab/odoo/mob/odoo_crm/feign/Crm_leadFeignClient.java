package cn.ibizlab.odoo.mob.odoo_crm.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_lead;
import cn.ibizlab.odoo.mob.odoo_crm.filter.*;


public interface Crm_leadFeignClient {



	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_leads/{crm_lead_id}/getdraft")
    public Crm_lead getDraft(@PathVariable("crm_lead_id") Integer crm_lead_id, @RequestBody Crm_lead crm_lead) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_leads/{crm_lead_id}")
    public Boolean remove(@PathVariable("crm_lead_id") Integer crm_lead_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_leads/{crm_lead_id}/save")
    public Crm_lead save(@PathVariable("crm_lead_id") Integer crm_lead_id, @RequestBody Crm_lead crm_lead) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_leads/{crm_lead_id}/updatebatch")
    public Crm_lead updateBatch(@PathVariable("crm_lead_id") Integer crm_lead_id, @RequestBody Crm_lead crm_lead) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_leads/{crm_lead_id}")
    public Crm_lead get(@PathVariable("crm_lead_id") Integer crm_lead_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_leads/{crm_lead_id}/createbatch")
    public Crm_lead createBatch(@PathVariable("crm_lead_id") Integer crm_lead_id, @RequestBody Crm_lead crm_lead) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_leads/{crm_lead_id}/removebatch")
    public Crm_lead removeBatch(@PathVariable("crm_lead_id") Integer crm_lead_id, @RequestBody Crm_lead crm_lead) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_leads")
    public Crm_lead create(@RequestBody Crm_lead crm_lead) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_leads/checkkey")
    public Boolean checkKey(@RequestBody Crm_lead crm_lead) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_leads/{crm_lead_id}")
    public Crm_lead update(@PathVariable("crm_lead_id") Integer crm_lead_id, @RequestBody Crm_lead crm_lead) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/crm_leads/fetchdefault")
	public Page<Crm_lead> fetchDefault(Crm_leadSearchContext searchContext) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/crm_leads/fetchdefault2")
	public Page<Crm_lead> fetchDefault2(Crm_leadSearchContext searchContext) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/res_partners/{res_partner_id}/crm_leads/{crm_lead_id}/getdraft")
    public Crm_lead getDraftByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("crm_lead_id") Integer crm_lead_id, @RequestBody Crm_lead crm_lead) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_partners/{res_partner_id}/crm_leads/{crm_lead_id}")
    public Boolean removeByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("crm_lead_id") Integer crm_lead_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/crm_leads/{crm_lead_id}/save")
    public Crm_lead saveByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("crm_lead_id") Integer crm_lead_id, @RequestBody Crm_lead crm_lead) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_partners/{res_partner_id}/crm_leads/{crm_lead_id}/updatebatch")
    public Crm_lead updateBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("crm_lead_id") Integer crm_lead_id, @RequestBody Crm_lead crm_lead) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/res_partners/{res_partner_id}/crm_leads/{crm_lead_id}")
    public Crm_lead getByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("crm_lead_id") Integer crm_lead_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/crm_leads/{crm_lead_id}/createbatch")
    public Crm_lead createBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("crm_lead_id") Integer crm_lead_id, @RequestBody Crm_lead crm_lead) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_partners/{res_partner_id}/crm_leads/{crm_lead_id}/removebatch")
    public Crm_lead removeBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("crm_lead_id") Integer crm_lead_id, @RequestBody Crm_lead crm_lead) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/crm_leads")
    public Crm_lead createByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Crm_lead crm_lead) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/crm_leads/checkkey")
    public Boolean checkKeyByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Crm_lead crm_lead) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_partners/{res_partner_id}/crm_leads/{crm_lead_id}")
    public Crm_lead updateByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("crm_lead_id") Integer crm_lead_id, @RequestBody Crm_lead crm_lead) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/res_partners/{res_partner_id}/crm_leads/fetchdefault")
	public Page<Crm_lead> fetchDefaultByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id , Crm_leadSearchContext searchContext) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/res_partners/{res_partner_id}/crm_leads/fetchdefault2")
	public Page<Crm_lead> fetchDefault2ByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id , Crm_leadSearchContext searchContext) ;
}
