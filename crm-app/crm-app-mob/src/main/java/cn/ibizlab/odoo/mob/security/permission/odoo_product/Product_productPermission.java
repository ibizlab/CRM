package cn.ibizlab.odoo.mob.security.permission.odoo_product;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;

import cn.ibizlab.odoo.mob.odoo_product.service.Product_productService;
import cn.ibizlab.odoo.mob.odoo_product.domain.Product_product;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Component("product_product_pms")
public class Product_productPermission {

    @Autowired
    Product_productService product_productservice;

    public boolean check(String action){
        return true ;
    }

    public boolean check(Integer product_product_id, String action){
        return true ;
    }


}
