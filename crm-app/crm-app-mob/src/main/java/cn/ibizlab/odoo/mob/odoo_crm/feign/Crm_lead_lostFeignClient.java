package cn.ibizlab.odoo.mob.odoo_crm.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_lead_lost;
import cn.ibizlab.odoo.mob.odoo_crm.filter.*;


public interface Crm_lead_lostFeignClient {



	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_lead_losts/{crm_lead_lost_id}/updatebatch")
    public Crm_lead_lost updateBatch(@PathVariable("crm_lead_lost_id") Integer crm_lead_lost_id, @RequestBody Crm_lead_lost crm_lead_lost) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_lead_losts/{crm_lead_lost_id}")
    public Boolean remove(@PathVariable("crm_lead_lost_id") Integer crm_lead_lost_id) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_lead_losts/{crm_lead_lost_id}")
    public Crm_lead_lost update(@PathVariable("crm_lead_lost_id") Integer crm_lead_lost_id, @RequestBody Crm_lead_lost crm_lead_lost) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_lead_losts/{crm_lead_lost_id}/getdraft")
    public Crm_lead_lost getDraft(@PathVariable("crm_lead_lost_id") Integer crm_lead_lost_id, @RequestBody Crm_lead_lost crm_lead_lost) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_lead_losts/{crm_lead_lost_id}")
    public Crm_lead_lost get(@PathVariable("crm_lead_lost_id") Integer crm_lead_lost_id) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_lead_losts/{crm_lead_lost_id}/removebatch")
    public Crm_lead_lost removeBatch(@PathVariable("crm_lead_lost_id") Integer crm_lead_lost_id, @RequestBody Crm_lead_lost crm_lead_lost) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_lead_losts")
    public Crm_lead_lost create(@RequestBody Crm_lead_lost crm_lead_lost) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_lead_losts/{crm_lead_lost_id}/createbatch")
    public Crm_lead_lost createBatch(@PathVariable("crm_lead_lost_id") Integer crm_lead_lost_id, @RequestBody Crm_lead_lost crm_lead_lost) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/crm_lead_losts/fetchdefault")
	public Page<Crm_lead_lost> fetchDefault(Crm_lead_lostSearchContext searchContext) ;
}
