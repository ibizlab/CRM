package cn.ibizlab.odoo.mob.odoo_product.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.mob.odoo_product.service.Product_pricelist_itemService;
import cn.ibizlab.odoo.mob.odoo_product.domain.Product_pricelist_item;
import cn.ibizlab.odoo.mob.odoo_product.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Product_pricelist_itemController {
	@Autowired
    Product_pricelist_itemService product_pricelist_itemservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/product_pricelist_items/{product_pricelist_item_id}")
    @PreAuthorize("@product_pricelist_item_pms.check(#product_pricelist_item_id,'READ')")
    public ResponseEntity<Product_pricelist_item> get(@PathVariable("product_pricelist_item_id") Integer product_pricelist_item_id) {
        Product_pricelist_item product_pricelist_item = product_pricelist_itemservice.get( product_pricelist_item_id);
        return ResponseEntity.status(HttpStatus.OK).body(product_pricelist_item);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/product_pricelist_items")
    @PreAuthorize("@product_pricelist_item_pms.check('CREATE')")
    public ResponseEntity<Product_pricelist_item> create(@RequestBody Product_pricelist_item product_pricelist_item) {
        Product_pricelist_item product_pricelist_item2 = product_pricelist_itemservice.create(product_pricelist_item);
        return ResponseEntity.status(HttpStatus.OK).body(product_pricelist_item2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_pricelist_items/{product_pricelist_item_id}")
    @PreAuthorize("@product_pricelist_item_pms.check(#product_pricelist_item_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("product_pricelist_item_id") Integer product_pricelist_item_id) {
        boolean b = product_pricelist_itemservice.remove( product_pricelist_item_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/product_pricelist_items/getdraft")
    @PreAuthorize("@product_pricelist_item_pms.check('CREATE')")
    public ResponseEntity<Product_pricelist_item> getDraft() {
        //Product_pricelist_item product_pricelist_item = product_pricelist_itemservice.getDraft( product_pricelist_item_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Product_pricelist_item());
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/product_pricelist_items/{product_pricelist_item_id}/createbatch")
    @PreAuthorize("@product_pricelist_item_pms.check(#product_pricelist_item_id,'CREATE')")
    public ResponseEntity<Product_pricelist_item> createBatch(@PathVariable("product_pricelist_item_id") Integer product_pricelist_item_id, @RequestBody Product_pricelist_item product_pricelist_item) {
        Product_pricelist_item product_pricelist_item2 = product_pricelist_itemservice.createBatch(product_pricelist_item_id, product_pricelist_item);
        return ResponseEntity.status(HttpStatus.OK).body(product_pricelist_item2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/product_pricelist_items/{product_pricelist_item_id}/updatebatch")
    @PreAuthorize("@product_pricelist_item_pms.check(#product_pricelist_item_id,'UPDATE')")
    public ResponseEntity<Product_pricelist_item> updateBatch(@PathVariable("product_pricelist_item_id") Integer product_pricelist_item_id, @RequestBody Product_pricelist_item product_pricelist_item) {
        Product_pricelist_item product_pricelist_item2 = product_pricelist_itemservice.updateBatch(product_pricelist_item_id, product_pricelist_item);
        return ResponseEntity.status(HttpStatus.OK).body(product_pricelist_item2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_pricelist_items/{product_pricelist_item_id}/removebatch")
    @PreAuthorize("@product_pricelist_item_pms.check(#product_pricelist_item_id,'DELETE')")
    public ResponseEntity<Product_pricelist_item> removeBatch(@PathVariable("product_pricelist_item_id") Integer product_pricelist_item_id, @RequestBody Product_pricelist_item product_pricelist_item) {
        Product_pricelist_item product_pricelist_item2 = product_pricelist_itemservice.removeBatch(product_pricelist_item_id, product_pricelist_item);
        return ResponseEntity.status(HttpStatus.OK).body(product_pricelist_item2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/product_pricelist_items/{product_pricelist_item_id}")
    @PreAuthorize("@product_pricelist_item_pms.check(#product_pricelist_item_id,'UPDATE')")
    public ResponseEntity<Product_pricelist_item> update(@PathVariable("product_pricelist_item_id") Integer product_pricelist_item_id, @RequestBody Product_pricelist_item product_pricelist_item) {
        Product_pricelist_item product_pricelist_item2 = product_pricelist_itemservice.update(product_pricelist_item_id, product_pricelist_item);
        return ResponseEntity.status(HttpStatus.OK).body(product_pricelist_item2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/product_pricelist_items/{product_pricelist_item_id}")
    @PreAuthorize("@product_pricelist_item_pms.check(#product_pricelist_item_id,'UPDATE')")
    public ResponseEntity<Product_pricelist_item> api_update(@PathVariable("product_pricelist_item_id") Integer product_pricelist_item_id, @RequestBody Product_pricelist_item product_pricelist_item) {
        Product_pricelist_item product_pricelist_item2 = product_pricelist_itemservice.update(product_pricelist_item_id, product_pricelist_item);
        return ResponseEntity.status(HttpStatus.OK).body(product_pricelist_item2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/product_pricelist_items/checkkey")
    @PreAuthorize("@product_pricelist_item_pms.check('')")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_pricelist_item product_pricelist_item) {
        boolean b = product_pricelist_itemservice.checkKey(product_pricelist_item);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/product_pricelist_items/{product_pricelist_item_id}/save")
    @PreAuthorize("@product_pricelist_item_pms.check(#product_pricelist_item_id,'')")
    public ResponseEntity<Product_pricelist_item> save(@PathVariable("product_pricelist_item_id") Integer product_pricelist_item_id, @RequestBody Product_pricelist_item product_pricelist_item) {
        Product_pricelist_item product_pricelist_item2 = product_pricelist_itemservice.save(product_pricelist_item_id, product_pricelist_item);
        return ResponseEntity.status(HttpStatus.OK).body(product_pricelist_item2);
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/product_pricelist_items/fetchdefault")
    @PreAuthorize("@product_pricelist_item_pms.check('READ')")
	public ResponseEntity<List<Product_pricelist_item>> fetchDefault(Product_pricelist_itemSearchContext searchContext,Pageable pageable) {
        
        Page<Product_pricelist_item> page = product_pricelist_itemservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}
