package cn.ibizlab.odoo.mob.odoo_crm.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_lead_tag;
import cn.ibizlab.odoo.mob.odoo_crm.filter.*;
import cn.ibizlab.odoo.mob.odoo_crm.feign.Crm_lead_tagFeignClient;
import cn.ibizlab.odoo.mob.MobApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Crm_lead_tagService {

    Crm_lead_tagFeignClient client;

    @Autowired
    public Crm_lead_tagService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Crm_lead_tagFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Crm_lead_tagFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public Crm_lead_tag updateBatch(Integer crm_lead_tag_id, Crm_lead_tag crm_lead_tag) {
        return client.updateBatch(crm_lead_tag_id, crm_lead_tag);
    }

    public Crm_lead_tag getDraft(Integer crm_lead_tag_id, Crm_lead_tag crm_lead_tag) {
        return client.getDraft(crm_lead_tag_id, crm_lead_tag);
    }

    public Crm_lead_tag removeBatch(Integer crm_lead_tag_id, Crm_lead_tag crm_lead_tag) {
        return client.removeBatch(crm_lead_tag_id, crm_lead_tag);
    }

    public Crm_lead_tag createBatch(Integer crm_lead_tag_id, Crm_lead_tag crm_lead_tag) {
        return client.createBatch(crm_lead_tag_id, crm_lead_tag);
    }

    public boolean remove( Integer crm_lead_tag_id) {
        return client.remove( crm_lead_tag_id);
    }

    public Crm_lead_tag update(Integer crm_lead_tag_id, Crm_lead_tag crm_lead_tag) {
        return client.update(crm_lead_tag_id, crm_lead_tag);
    }

    public Crm_lead_tag get( Integer crm_lead_tag_id) {
        return client.get( crm_lead_tag_id);
    }

	public Crm_lead_tag create(Crm_lead_tag crm_lead_tag) {
        return client.create(crm_lead_tag);
    }

	public Page<Crm_lead_tag> fetchDefault(Crm_lead_tagSearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }

}
