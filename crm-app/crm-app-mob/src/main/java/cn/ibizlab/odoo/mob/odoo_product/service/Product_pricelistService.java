package cn.ibizlab.odoo.mob.odoo_product.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.mob.odoo_product.domain.Product_pricelist;
import cn.ibizlab.odoo.mob.odoo_product.filter.*;
import cn.ibizlab.odoo.mob.odoo_product.feign.Product_pricelistFeignClient;
import cn.ibizlab.odoo.mob.MobApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Product_pricelistService {

    Product_pricelistFeignClient client;

    @Autowired
    public Product_pricelistService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Product_pricelistFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Product_pricelistFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public Product_pricelist get( Integer product_pricelist_id) {
        return client.get( product_pricelist_id);
    }

    public Product_pricelist save(Integer product_pricelist_id, Product_pricelist product_pricelist) {
        return client.save(product_pricelist_id, product_pricelist);
    }

    public Product_pricelist removeBatch(Integer product_pricelist_id, Product_pricelist product_pricelist) {
        return client.removeBatch(product_pricelist_id, product_pricelist);
    }

    public Product_pricelist getDraft(Integer product_pricelist_id, Product_pricelist product_pricelist) {
        return client.getDraft(product_pricelist_id, product_pricelist);
    }

    public Product_pricelist update(Integer product_pricelist_id, Product_pricelist product_pricelist) {
        return client.update(product_pricelist_id, product_pricelist);
    }

    public boolean remove( Integer product_pricelist_id) {
        return client.remove( product_pricelist_id);
    }

    public Product_pricelist updateBatch(Integer product_pricelist_id, Product_pricelist product_pricelist) {
        return client.updateBatch(product_pricelist_id, product_pricelist);
    }

    public boolean checkKey(Product_pricelist product_pricelist) {
        return client.checkKey(product_pricelist);
    }

    public Product_pricelist createBatch(Integer product_pricelist_id, Product_pricelist product_pricelist) {
        return client.createBatch(product_pricelist_id, product_pricelist);
    }

	public Product_pricelist create(Product_pricelist product_pricelist) {
        return client.create(product_pricelist);
    }

	public Page<Product_pricelist> fetchDefault(Product_pricelistSearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }

}
