package cn.ibizlab.odoo.mob.odoo_product.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.mob.odoo_product.domain.Product_product;
import cn.ibizlab.odoo.mob.odoo_product.filter.*;
import cn.ibizlab.odoo.mob.odoo_product.feign.Product_productFeignClient;
import cn.ibizlab.odoo.mob.MobApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Product_productService {

    Product_productFeignClient client;

    @Autowired
    public Product_productService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Product_productFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Product_productFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public Product_product updateBatch(Integer product_product_id, Product_product product_product) {
        return client.updateBatch(product_product_id, product_product);
    }

    public Product_product get( Integer product_product_id) {
        return client.get( product_product_id);
    }

    public Product_product save(Integer product_product_id, Product_product product_product) {
        return client.save(product_product_id, product_product);
    }

	public Product_product create(Product_product product_product) {
        return client.create(product_product);
    }

    public boolean checkKey(Product_product product_product) {
        return client.checkKey(product_product);
    }

    public Product_product update(Integer product_product_id, Product_product product_product) {
        return client.update(product_product_id, product_product);
    }

    public Product_product removeBatch(Integer product_product_id, Product_product product_product) {
        return client.removeBatch(product_product_id, product_product);
    }

    public Product_product getDraft(Integer product_product_id, Product_product product_product) {
        return client.getDraft(product_product_id, product_product);
    }

    public boolean remove( Integer product_product_id) {
        return client.remove( product_product_id);
    }

    public Product_product createBatch(Integer product_product_id, Product_product product_product) {
        return client.createBatch(product_product_id, product_product);
    }

	public Page<Product_product> fetchDefault(Product_productSearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }

}
