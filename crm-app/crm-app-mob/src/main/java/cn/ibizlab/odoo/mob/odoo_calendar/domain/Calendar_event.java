package cn.ibizlab.odoo.mob.odoo_calendar.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 应用实体[活动]
 */
public class Calendar_event implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 地点
     */
    private String location;

    @JsonIgnore
    private boolean locationDirtyFlag;
    
    /**
     * 附件
     */
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 说明
     */
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;
    
    /**
     * 循环ID日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp recurrent_id_date;

    @JsonIgnore
    private boolean recurrent_id_dateDirtyFlag;
    
    /**
     * 周六
     */
    private String sa;

    @JsonIgnore
    private boolean saDirtyFlag;
    
    /**
     * 显示名称
     */
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;
    
    /**
     * 循环
     */
    private String recurrency;

    @JsonIgnore
    private boolean recurrencyDirtyFlag;
    
    /**
     * 重复
     */
    private Integer count;

    @JsonIgnore
    private boolean countDirtyFlag;
    
    /**
     * 周五
     */
    private String fr;

    @JsonIgnore
    private boolean frDirtyFlag;
    
    /**
     * 循环规则
     */
    private String rrule;

    @JsonIgnore
    private boolean rruleDirtyFlag;
    
    /**
     * 选项
     */
    private String month_by;

    @JsonIgnore
    private boolean month_byDirtyFlag;
    
    /**
     * 工作日
     */
    private String week_list;

    @JsonIgnore
    private boolean week_listDirtyFlag;
    
    /**
     * 未读消息
     */
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;
    
    /**
     * 停止
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp stop;

    @JsonIgnore
    private boolean stopDirtyFlag;
    
    /**
     * 文档ID
     */
    private Integer res_id;

    @JsonIgnore
    private boolean res_idDirtyFlag;
    
    /**
     * 错误数
     */
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;
    
    /**
     * 循环ID
     */
    private Integer recurrent_id;

    @JsonIgnore
    private boolean recurrent_idDirtyFlag;
    
    /**
     * 消息递送错误
     */
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;
    
    /**
     * 重复终止
     */
    private String end_type;

    @JsonIgnore
    private boolean end_typeDirtyFlag;
    
    /**
     * 参与者
     */
    private String attendee_ids;

    @JsonIgnore
    private boolean attendee_idsDirtyFlag;
    
    /**
     * 出席者状态
     */
    private String attendee_status;

    @JsonIgnore
    private boolean attendee_statusDirtyFlag;
    
    /**
     * 有效
     */
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;
    
    /**
     * 重新提起
     */
    private String rrule_type;

    @JsonIgnore
    private boolean rrule_typeDirtyFlag;
    
    /**
     * 重复
     */
    private Integer interval;

    @JsonIgnore
    private boolean intervalDirtyFlag;
    
    /**
     * 隐私
     */
    private String privacy;

    @JsonIgnore
    private boolean privacyDirtyFlag;
    
    /**
     * 持续时间
     */
    private Double duration;

    @JsonIgnore
    private boolean durationDirtyFlag;
    
    /**
     * 开始时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp start_datetime;

    @JsonIgnore
    private boolean start_datetimeDirtyFlag;
    
    /**
     * 出席者
     */
    private String is_attendee;

    @JsonIgnore
    private boolean is_attendeeDirtyFlag;
    
    /**
     * 开始日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp start_date;

    @JsonIgnore
    private boolean start_dateDirtyFlag;
    
    /**
     * 周一
     */
    private String mo;

    @JsonIgnore
    private boolean moDirtyFlag;
    
    /**
     * 状态
     */
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;
    
    /**
     * 周三
     */
    private String we;

    @JsonIgnore
    private boolean weDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;
    
    /**
     * 活动时间
     */
    private String display_time;

    @JsonIgnore
    private boolean display_timeDirtyFlag;
    
    /**
     * 日期
     */
    private String display_start;

    @JsonIgnore
    private boolean display_startDirtyFlag;
    
    /**
     * 结束日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp stop_date;

    @JsonIgnore
    private boolean stop_dateDirtyFlag;
    
    /**
     * 需要采取行动
     */
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;
    
    /**
     * 重复直到
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp final_date;

    @JsonIgnore
    private boolean final_dateDirtyFlag;
    
    /**
     * 文档模型
     */
    private Integer res_model_id;

    @JsonIgnore
    private boolean res_model_idDirtyFlag;
    
    /**
     * 事件是否突出显示
     */
    private String is_highlighted;

    @JsonIgnore
    private boolean is_highlightedDirtyFlag;
    
    /**
     * ID
     */
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;
    
    /**
     * 结束日期时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp stop_datetime;

    @JsonIgnore
    private boolean stop_datetimeDirtyFlag;
    
    /**
     * 活动
     */
    private String activity_ids;

    @JsonIgnore
    private boolean activity_idsDirtyFlag;
    
    /**
     * 文档模型名称
     */
    private String res_model;

    @JsonIgnore
    private boolean res_modelDirtyFlag;
    
    /**
     * 周二
     */
    private String tu;

    @JsonIgnore
    private boolean tuDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;
    
    /**
     * 显示时间为
     */
    private String show_as;

    @JsonIgnore
    private boolean show_asDirtyFlag;
    
    /**
     * 标签
     */
    private String categ_ids;

    @JsonIgnore
    private boolean categ_idsDirtyFlag;
    
    /**
     * 提醒
     */
    private String alarm_ids;

    @JsonIgnore
    private boolean alarm_idsDirtyFlag;
    
    /**
     * 周四
     */
    private String th;

    @JsonIgnore
    private boolean thDirtyFlag;
    
    /**
     * 行动数量
     */
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;
    
    /**
     * 按 天
     */
    private String byday;

    @JsonIgnore
    private boolean bydayDirtyFlag;
    
    /**
     * 日期
     */
    private Integer day;

    @JsonIgnore
    private boolean dayDirtyFlag;
    
    /**
     * 开始
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp start;

    @JsonIgnore
    private boolean startDirtyFlag;
    
    /**
     * 附件数量
     */
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;
    
    /**
     * 与会者
     */
    private String partner_ids;

    @JsonIgnore
    private boolean partner_idsDirtyFlag;
    
    /**
     * 全天
     */
    private String allday;

    @JsonIgnore
    private boolean alldayDirtyFlag;
    
    /**
     * 消息
     */
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;
    
    /**
     * 网站消息
     */
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;
    
    /**
     * 关注者
     */
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;
    
    /**
     * 周日
     */
    private String su;

    @JsonIgnore
    private boolean suDirtyFlag;
    
    /**
     * 是关注者
     */
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;
    
    /**
     * 会议主题
     */
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;
    
    /**
     * 商机
     */
    private String opportunity_id_text;

    @JsonIgnore
    private boolean opportunity_id_textDirtyFlag;
    
    /**
     * 创建人
     */
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;
    
    /**
     * 最后更新人
     */
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;
    
    /**
     * 所有者
     */
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;
    
    /**
     * 负责人
     */
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;
    
    /**
     * 申请人
     */
    private String applicant_id_text;

    @JsonIgnore
    private boolean applicant_id_textDirtyFlag;
    
    /**
     * 创建人
     */
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;
    
    /**
     * 申请人
     */
    private Integer applicant_id;

    @JsonIgnore
    private boolean applicant_idDirtyFlag;
    
    /**
     * 最后更新人
     */
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;
    
    /**
     * 所有者
     */
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;
    
    /**
     * 商机
     */
    private Integer opportunity_id;

    @JsonIgnore
    private boolean opportunity_idDirtyFlag;
    

    /**
     * 获取 [地点]
     */
    @JsonProperty("location")
    public String getLocation(){
        return this.location ;
    }

    /**
     * 设置 [地点]
     */
    @JsonProperty("location")
    public void setLocation(String  location){
        this.location = location ;
        this.locationDirtyFlag = true ;
    }

    /**
     * 获取 [地点]脏标记
     */
    @JsonIgnore
    public boolean getLocationDirtyFlag(){
        return this.locationDirtyFlag ;
    }

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [说明]
     */
    @JsonProperty("description")
    public String getDescription(){
        return this.description ;
    }

    /**
     * 设置 [说明]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [说明]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return this.descriptionDirtyFlag ;
    }

    /**
     * 获取 [循环ID日期]
     */
    @JsonProperty("recurrent_id_date")
    public Timestamp getRecurrent_id_date(){
        return this.recurrent_id_date ;
    }

    /**
     * 设置 [循环ID日期]
     */
    @JsonProperty("recurrent_id_date")
    public void setRecurrent_id_date(Timestamp  recurrent_id_date){
        this.recurrent_id_date = recurrent_id_date ;
        this.recurrent_id_dateDirtyFlag = true ;
    }

    /**
     * 获取 [循环ID日期]脏标记
     */
    @JsonIgnore
    public boolean getRecurrent_id_dateDirtyFlag(){
        return this.recurrent_id_dateDirtyFlag ;
    }

    /**
     * 获取 [周六]
     */
    @JsonProperty("sa")
    public String getSa(){
        return this.sa ;
    }

    /**
     * 设置 [周六]
     */
    @JsonProperty("sa")
    public void setSa(String  sa){
        this.sa = sa ;
        this.saDirtyFlag = true ;
    }

    /**
     * 获取 [周六]脏标记
     */
    @JsonIgnore
    public boolean getSaDirtyFlag(){
        return this.saDirtyFlag ;
    }

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }

    /**
     * 获取 [循环]
     */
    @JsonProperty("recurrency")
    public String getRecurrency(){
        return this.recurrency ;
    }

    /**
     * 设置 [循环]
     */
    @JsonProperty("recurrency")
    public void setRecurrency(String  recurrency){
        this.recurrency = recurrency ;
        this.recurrencyDirtyFlag = true ;
    }

    /**
     * 获取 [循环]脏标记
     */
    @JsonIgnore
    public boolean getRecurrencyDirtyFlag(){
        return this.recurrencyDirtyFlag ;
    }

    /**
     * 获取 [重复]
     */
    @JsonProperty("count")
    public Integer getCount(){
        return this.count ;
    }

    /**
     * 设置 [重复]
     */
    @JsonProperty("count")
    public void setCount(Integer  count){
        this.count = count ;
        this.countDirtyFlag = true ;
    }

    /**
     * 获取 [重复]脏标记
     */
    @JsonIgnore
    public boolean getCountDirtyFlag(){
        return this.countDirtyFlag ;
    }

    /**
     * 获取 [周五]
     */
    @JsonProperty("fr")
    public String getFr(){
        return this.fr ;
    }

    /**
     * 设置 [周五]
     */
    @JsonProperty("fr")
    public void setFr(String  fr){
        this.fr = fr ;
        this.frDirtyFlag = true ;
    }

    /**
     * 获取 [周五]脏标记
     */
    @JsonIgnore
    public boolean getFrDirtyFlag(){
        return this.frDirtyFlag ;
    }

    /**
     * 获取 [循环规则]
     */
    @JsonProperty("rrule")
    public String getRrule(){
        return this.rrule ;
    }

    /**
     * 设置 [循环规则]
     */
    @JsonProperty("rrule")
    public void setRrule(String  rrule){
        this.rrule = rrule ;
        this.rruleDirtyFlag = true ;
    }

    /**
     * 获取 [循环规则]脏标记
     */
    @JsonIgnore
    public boolean getRruleDirtyFlag(){
        return this.rruleDirtyFlag ;
    }

    /**
     * 获取 [选项]
     */
    @JsonProperty("month_by")
    public String getMonth_by(){
        return this.month_by ;
    }

    /**
     * 设置 [选项]
     */
    @JsonProperty("month_by")
    public void setMonth_by(String  month_by){
        this.month_by = month_by ;
        this.month_byDirtyFlag = true ;
    }

    /**
     * 获取 [选项]脏标记
     */
    @JsonIgnore
    public boolean getMonth_byDirtyFlag(){
        return this.month_byDirtyFlag ;
    }

    /**
     * 获取 [工作日]
     */
    @JsonProperty("week_list")
    public String getWeek_list(){
        return this.week_list ;
    }

    /**
     * 设置 [工作日]
     */
    @JsonProperty("week_list")
    public void setWeek_list(String  week_list){
        this.week_list = week_list ;
        this.week_listDirtyFlag = true ;
    }

    /**
     * 获取 [工作日]脏标记
     */
    @JsonIgnore
    public boolean getWeek_listDirtyFlag(){
        return this.week_listDirtyFlag ;
    }

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }

    /**
     * 获取 [停止]
     */
    @JsonProperty("stop")
    public Timestamp getStop(){
        return this.stop ;
    }

    /**
     * 设置 [停止]
     */
    @JsonProperty("stop")
    public void setStop(Timestamp  stop){
        this.stop = stop ;
        this.stopDirtyFlag = true ;
    }

    /**
     * 获取 [停止]脏标记
     */
    @JsonIgnore
    public boolean getStopDirtyFlag(){
        return this.stopDirtyFlag ;
    }

    /**
     * 获取 [文档ID]
     */
    @JsonProperty("res_id")
    public Integer getRes_id(){
        return this.res_id ;
    }

    /**
     * 设置 [文档ID]
     */
    @JsonProperty("res_id")
    public void setRes_id(Integer  res_id){
        this.res_id = res_id ;
        this.res_idDirtyFlag = true ;
    }

    /**
     * 获取 [文档ID]脏标记
     */
    @JsonIgnore
    public boolean getRes_idDirtyFlag(){
        return this.res_idDirtyFlag ;
    }

    /**
     * 获取 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [错误数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [循环ID]
     */
    @JsonProperty("recurrent_id")
    public Integer getRecurrent_id(){
        return this.recurrent_id ;
    }

    /**
     * 设置 [循环ID]
     */
    @JsonProperty("recurrent_id")
    public void setRecurrent_id(Integer  recurrent_id){
        this.recurrent_id = recurrent_id ;
        this.recurrent_idDirtyFlag = true ;
    }

    /**
     * 获取 [循环ID]脏标记
     */
    @JsonIgnore
    public boolean getRecurrent_idDirtyFlag(){
        return this.recurrent_idDirtyFlag ;
    }

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [重复终止]
     */
    @JsonProperty("end_type")
    public String getEnd_type(){
        return this.end_type ;
    }

    /**
     * 设置 [重复终止]
     */
    @JsonProperty("end_type")
    public void setEnd_type(String  end_type){
        this.end_type = end_type ;
        this.end_typeDirtyFlag = true ;
    }

    /**
     * 获取 [重复终止]脏标记
     */
    @JsonIgnore
    public boolean getEnd_typeDirtyFlag(){
        return this.end_typeDirtyFlag ;
    }

    /**
     * 获取 [参与者]
     */
    @JsonProperty("attendee_ids")
    public String getAttendee_ids(){
        return this.attendee_ids ;
    }

    /**
     * 设置 [参与者]
     */
    @JsonProperty("attendee_ids")
    public void setAttendee_ids(String  attendee_ids){
        this.attendee_ids = attendee_ids ;
        this.attendee_idsDirtyFlag = true ;
    }

    /**
     * 获取 [参与者]脏标记
     */
    @JsonIgnore
    public boolean getAttendee_idsDirtyFlag(){
        return this.attendee_idsDirtyFlag ;
    }

    /**
     * 获取 [出席者状态]
     */
    @JsonProperty("attendee_status")
    public String getAttendee_status(){
        return this.attendee_status ;
    }

    /**
     * 设置 [出席者状态]
     */
    @JsonProperty("attendee_status")
    public void setAttendee_status(String  attendee_status){
        this.attendee_status = attendee_status ;
        this.attendee_statusDirtyFlag = true ;
    }

    /**
     * 获取 [出席者状态]脏标记
     */
    @JsonIgnore
    public boolean getAttendee_statusDirtyFlag(){
        return this.attendee_statusDirtyFlag ;
    }

    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }

    /**
     * 获取 [重新提起]
     */
    @JsonProperty("rrule_type")
    public String getRrule_type(){
        return this.rrule_type ;
    }

    /**
     * 设置 [重新提起]
     */
    @JsonProperty("rrule_type")
    public void setRrule_type(String  rrule_type){
        this.rrule_type = rrule_type ;
        this.rrule_typeDirtyFlag = true ;
    }

    /**
     * 获取 [重新提起]脏标记
     */
    @JsonIgnore
    public boolean getRrule_typeDirtyFlag(){
        return this.rrule_typeDirtyFlag ;
    }

    /**
     * 获取 [重复]
     */
    @JsonProperty("interval")
    public Integer getInterval(){
        return this.interval ;
    }

    /**
     * 设置 [重复]
     */
    @JsonProperty("interval")
    public void setInterval(Integer  interval){
        this.interval = interval ;
        this.intervalDirtyFlag = true ;
    }

    /**
     * 获取 [重复]脏标记
     */
    @JsonIgnore
    public boolean getIntervalDirtyFlag(){
        return this.intervalDirtyFlag ;
    }

    /**
     * 获取 [隐私]
     */
    @JsonProperty("privacy")
    public String getPrivacy(){
        return this.privacy ;
    }

    /**
     * 设置 [隐私]
     */
    @JsonProperty("privacy")
    public void setPrivacy(String  privacy){
        this.privacy = privacy ;
        this.privacyDirtyFlag = true ;
    }

    /**
     * 获取 [隐私]脏标记
     */
    @JsonIgnore
    public boolean getPrivacyDirtyFlag(){
        return this.privacyDirtyFlag ;
    }

    /**
     * 获取 [持续时间]
     */
    @JsonProperty("duration")
    public Double getDuration(){
        return this.duration ;
    }

    /**
     * 设置 [持续时间]
     */
    @JsonProperty("duration")
    public void setDuration(Double  duration){
        this.duration = duration ;
        this.durationDirtyFlag = true ;
    }

    /**
     * 获取 [持续时间]脏标记
     */
    @JsonIgnore
    public boolean getDurationDirtyFlag(){
        return this.durationDirtyFlag ;
    }

    /**
     * 获取 [开始时间]
     */
    @JsonProperty("start_datetime")
    public Timestamp getStart_datetime(){
        return this.start_datetime ;
    }

    /**
     * 设置 [开始时间]
     */
    @JsonProperty("start_datetime")
    public void setStart_datetime(Timestamp  start_datetime){
        this.start_datetime = start_datetime ;
        this.start_datetimeDirtyFlag = true ;
    }

    /**
     * 获取 [开始时间]脏标记
     */
    @JsonIgnore
    public boolean getStart_datetimeDirtyFlag(){
        return this.start_datetimeDirtyFlag ;
    }

    /**
     * 获取 [出席者]
     */
    @JsonProperty("is_attendee")
    public String getIs_attendee(){
        return this.is_attendee ;
    }

    /**
     * 设置 [出席者]
     */
    @JsonProperty("is_attendee")
    public void setIs_attendee(String  is_attendee){
        this.is_attendee = is_attendee ;
        this.is_attendeeDirtyFlag = true ;
    }

    /**
     * 获取 [出席者]脏标记
     */
    @JsonIgnore
    public boolean getIs_attendeeDirtyFlag(){
        return this.is_attendeeDirtyFlag ;
    }

    /**
     * 获取 [开始日期]
     */
    @JsonProperty("start_date")
    public Timestamp getStart_date(){
        return this.start_date ;
    }

    /**
     * 设置 [开始日期]
     */
    @JsonProperty("start_date")
    public void setStart_date(Timestamp  start_date){
        this.start_date = start_date ;
        this.start_dateDirtyFlag = true ;
    }

    /**
     * 获取 [开始日期]脏标记
     */
    @JsonIgnore
    public boolean getStart_dateDirtyFlag(){
        return this.start_dateDirtyFlag ;
    }

    /**
     * 获取 [周一]
     */
    @JsonProperty("mo")
    public String getMo(){
        return this.mo ;
    }

    /**
     * 设置 [周一]
     */
    @JsonProperty("mo")
    public void setMo(String  mo){
        this.mo = mo ;
        this.moDirtyFlag = true ;
    }

    /**
     * 获取 [周一]脏标记
     */
    @JsonIgnore
    public boolean getMoDirtyFlag(){
        return this.moDirtyFlag ;
    }

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }

    /**
     * 获取 [周三]
     */
    @JsonProperty("we")
    public String getWe(){
        return this.we ;
    }

    /**
     * 设置 [周三]
     */
    @JsonProperty("we")
    public void setWe(String  we){
        this.we = we ;
        this.weDirtyFlag = true ;
    }

    /**
     * 获取 [周三]脏标记
     */
    @JsonIgnore
    public boolean getWeDirtyFlag(){
        return this.weDirtyFlag ;
    }

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [活动时间]
     */
    @JsonProperty("display_time")
    public String getDisplay_time(){
        return this.display_time ;
    }

    /**
     * 设置 [活动时间]
     */
    @JsonProperty("display_time")
    public void setDisplay_time(String  display_time){
        this.display_time = display_time ;
        this.display_timeDirtyFlag = true ;
    }

    /**
     * 获取 [活动时间]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_timeDirtyFlag(){
        return this.display_timeDirtyFlag ;
    }

    /**
     * 获取 [日期]
     */
    @JsonProperty("display_start")
    public String getDisplay_start(){
        return this.display_start ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("display_start")
    public void setDisplay_start(String  display_start){
        this.display_start = display_start ;
        this.display_startDirtyFlag = true ;
    }

    /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_startDirtyFlag(){
        return this.display_startDirtyFlag ;
    }

    /**
     * 获取 [结束日期]
     */
    @JsonProperty("stop_date")
    public Timestamp getStop_date(){
        return this.stop_date ;
    }

    /**
     * 设置 [结束日期]
     */
    @JsonProperty("stop_date")
    public void setStop_date(Timestamp  stop_date){
        this.stop_date = stop_date ;
        this.stop_dateDirtyFlag = true ;
    }

    /**
     * 获取 [结束日期]脏标记
     */
    @JsonIgnore
    public boolean getStop_dateDirtyFlag(){
        return this.stop_dateDirtyFlag ;
    }

    /**
     * 获取 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [需要采取行动]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [重复直到]
     */
    @JsonProperty("final_date")
    public Timestamp getFinal_date(){
        return this.final_date ;
    }

    /**
     * 设置 [重复直到]
     */
    @JsonProperty("final_date")
    public void setFinal_date(Timestamp  final_date){
        this.final_date = final_date ;
        this.final_dateDirtyFlag = true ;
    }

    /**
     * 获取 [重复直到]脏标记
     */
    @JsonIgnore
    public boolean getFinal_dateDirtyFlag(){
        return this.final_dateDirtyFlag ;
    }

    /**
     * 获取 [文档模型]
     */
    @JsonProperty("res_model_id")
    public Integer getRes_model_id(){
        return this.res_model_id ;
    }

    /**
     * 设置 [文档模型]
     */
    @JsonProperty("res_model_id")
    public void setRes_model_id(Integer  res_model_id){
        this.res_model_id = res_model_id ;
        this.res_model_idDirtyFlag = true ;
    }

    /**
     * 获取 [文档模型]脏标记
     */
    @JsonIgnore
    public boolean getRes_model_idDirtyFlag(){
        return this.res_model_idDirtyFlag ;
    }

    /**
     * 获取 [事件是否突出显示]
     */
    @JsonProperty("is_highlighted")
    public String getIs_highlighted(){
        return this.is_highlighted ;
    }

    /**
     * 设置 [事件是否突出显示]
     */
    @JsonProperty("is_highlighted")
    public void setIs_highlighted(String  is_highlighted){
        this.is_highlighted = is_highlighted ;
        this.is_highlightedDirtyFlag = true ;
    }

    /**
     * 获取 [事件是否突出显示]脏标记
     */
    @JsonIgnore
    public boolean getIs_highlightedDirtyFlag(){
        return this.is_highlightedDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }

    /**
     * 获取 [结束日期时间]
     */
    @JsonProperty("stop_datetime")
    public Timestamp getStop_datetime(){
        return this.stop_datetime ;
    }

    /**
     * 设置 [结束日期时间]
     */
    @JsonProperty("stop_datetime")
    public void setStop_datetime(Timestamp  stop_datetime){
        this.stop_datetime = stop_datetime ;
        this.stop_datetimeDirtyFlag = true ;
    }

    /**
     * 获取 [结束日期时间]脏标记
     */
    @JsonIgnore
    public boolean getStop_datetimeDirtyFlag(){
        return this.stop_datetimeDirtyFlag ;
    }

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return this.activity_ids ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return this.activity_idsDirtyFlag ;
    }

    /**
     * 获取 [文档模型名称]
     */
    @JsonProperty("res_model")
    public String getRes_model(){
        return this.res_model ;
    }

    /**
     * 设置 [文档模型名称]
     */
    @JsonProperty("res_model")
    public void setRes_model(String  res_model){
        this.res_model = res_model ;
        this.res_modelDirtyFlag = true ;
    }

    /**
     * 获取 [文档模型名称]脏标记
     */
    @JsonIgnore
    public boolean getRes_modelDirtyFlag(){
        return this.res_modelDirtyFlag ;
    }

    /**
     * 获取 [周二]
     */
    @JsonProperty("tu")
    public String getTu(){
        return this.tu ;
    }

    /**
     * 设置 [周二]
     */
    @JsonProperty("tu")
    public void setTu(String  tu){
        this.tu = tu ;
        this.tuDirtyFlag = true ;
    }

    /**
     * 获取 [周二]脏标记
     */
    @JsonIgnore
    public boolean getTuDirtyFlag(){
        return this.tuDirtyFlag ;
    }

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }

    /**
     * 获取 [显示时间为]
     */
    @JsonProperty("show_as")
    public String getShow_as(){
        return this.show_as ;
    }

    /**
     * 设置 [显示时间为]
     */
    @JsonProperty("show_as")
    public void setShow_as(String  show_as){
        this.show_as = show_as ;
        this.show_asDirtyFlag = true ;
    }

    /**
     * 获取 [显示时间为]脏标记
     */
    @JsonIgnore
    public boolean getShow_asDirtyFlag(){
        return this.show_asDirtyFlag ;
    }

    /**
     * 获取 [标签]
     */
    @JsonProperty("categ_ids")
    public String getCateg_ids(){
        return this.categ_ids ;
    }

    /**
     * 设置 [标签]
     */
    @JsonProperty("categ_ids")
    public void setCateg_ids(String  categ_ids){
        this.categ_ids = categ_ids ;
        this.categ_idsDirtyFlag = true ;
    }

    /**
     * 获取 [标签]脏标记
     */
    @JsonIgnore
    public boolean getCateg_idsDirtyFlag(){
        return this.categ_idsDirtyFlag ;
    }

    /**
     * 获取 [提醒]
     */
    @JsonProperty("alarm_ids")
    public String getAlarm_ids(){
        return this.alarm_ids ;
    }

    /**
     * 设置 [提醒]
     */
    @JsonProperty("alarm_ids")
    public void setAlarm_ids(String  alarm_ids){
        this.alarm_ids = alarm_ids ;
        this.alarm_idsDirtyFlag = true ;
    }

    /**
     * 获取 [提醒]脏标记
     */
    @JsonIgnore
    public boolean getAlarm_idsDirtyFlag(){
        return this.alarm_idsDirtyFlag ;
    }

    /**
     * 获取 [周四]
     */
    @JsonProperty("th")
    public String getTh(){
        return this.th ;
    }

    /**
     * 设置 [周四]
     */
    @JsonProperty("th")
    public void setTh(String  th){
        this.th = th ;
        this.thDirtyFlag = true ;
    }

    /**
     * 获取 [周四]脏标记
     */
    @JsonIgnore
    public boolean getThDirtyFlag(){
        return this.thDirtyFlag ;
    }

    /**
     * 获取 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [行动数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [按 天]
     */
    @JsonProperty("byday")
    public String getByday(){
        return this.byday ;
    }

    /**
     * 设置 [按 天]
     */
    @JsonProperty("byday")
    public void setByday(String  byday){
        this.byday = byday ;
        this.bydayDirtyFlag = true ;
    }

    /**
     * 获取 [按 天]脏标记
     */
    @JsonIgnore
    public boolean getBydayDirtyFlag(){
        return this.bydayDirtyFlag ;
    }

    /**
     * 获取 [日期]
     */
    @JsonProperty("day")
    public Integer getDay(){
        return this.day ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("day")
    public void setDay(Integer  day){
        this.day = day ;
        this.dayDirtyFlag = true ;
    }

    /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDayDirtyFlag(){
        return this.dayDirtyFlag ;
    }

    /**
     * 获取 [开始]
     */
    @JsonProperty("start")
    public Timestamp getStart(){
        return this.start ;
    }

    /**
     * 设置 [开始]
     */
    @JsonProperty("start")
    public void setStart(Timestamp  start){
        this.start = start ;
        this.startDirtyFlag = true ;
    }

    /**
     * 获取 [开始]脏标记
     */
    @JsonIgnore
    public boolean getStartDirtyFlag(){
        return this.startDirtyFlag ;
    }

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [与会者]
     */
    @JsonProperty("partner_ids")
    public String getPartner_ids(){
        return this.partner_ids ;
    }

    /**
     * 设置 [与会者]
     */
    @JsonProperty("partner_ids")
    public void setPartner_ids(String  partner_ids){
        this.partner_ids = partner_ids ;
        this.partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [与会者]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idsDirtyFlag(){
        return this.partner_idsDirtyFlag ;
    }

    /**
     * 获取 [全天]
     */
    @JsonProperty("allday")
    public String getAllday(){
        return this.allday ;
    }

    /**
     * 设置 [全天]
     */
    @JsonProperty("allday")
    public void setAllday(String  allday){
        this.allday = allday ;
        this.alldayDirtyFlag = true ;
    }

    /**
     * 获取 [全天]脏标记
     */
    @JsonIgnore
    public boolean getAlldayDirtyFlag(){
        return this.alldayDirtyFlag ;
    }

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }

    /**
     * 获取 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [网站消息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [周日]
     */
    @JsonProperty("su")
    public String getSu(){
        return this.su ;
    }

    /**
     * 设置 [周日]
     */
    @JsonProperty("su")
    public void setSu(String  su){
        this.su = su ;
        this.suDirtyFlag = true ;
    }

    /**
     * 获取 [周日]脏标记
     */
    @JsonIgnore
    public boolean getSuDirtyFlag(){
        return this.suDirtyFlag ;
    }

    /**
     * 获取 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [是关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [会议主题]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [会议主题]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [会议主题]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }

    /**
     * 获取 [商机]
     */
    @JsonProperty("opportunity_id_text")
    public String getOpportunity_id_text(){
        return this.opportunity_id_text ;
    }

    /**
     * 设置 [商机]
     */
    @JsonProperty("opportunity_id_text")
    public void setOpportunity_id_text(String  opportunity_id_text){
        this.opportunity_id_text = opportunity_id_text ;
        this.opportunity_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [商机]脏标记
     */
    @JsonIgnore
    public boolean getOpportunity_id_textDirtyFlag(){
        return this.opportunity_id_textDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [所有者]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [所有者]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [所有者]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }

    /**
     * 获取 [负责人]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [负责人]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [负责人]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }

    /**
     * 获取 [申请人]
     */
    @JsonProperty("applicant_id_text")
    public String getApplicant_id_text(){
        return this.applicant_id_text ;
    }

    /**
     * 设置 [申请人]
     */
    @JsonProperty("applicant_id_text")
    public void setApplicant_id_text(String  applicant_id_text){
        this.applicant_id_text = applicant_id_text ;
        this.applicant_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [申请人]脏标记
     */
    @JsonIgnore
    public boolean getApplicant_id_textDirtyFlag(){
        return this.applicant_id_textDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }

    /**
     * 获取 [申请人]
     */
    @JsonProperty("applicant_id")
    public Integer getApplicant_id(){
        return this.applicant_id ;
    }

    /**
     * 设置 [申请人]
     */
    @JsonProperty("applicant_id")
    public void setApplicant_id(Integer  applicant_id){
        this.applicant_id = applicant_id ;
        this.applicant_idDirtyFlag = true ;
    }

    /**
     * 获取 [申请人]脏标记
     */
    @JsonIgnore
    public boolean getApplicant_idDirtyFlag(){
        return this.applicant_idDirtyFlag ;
    }

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }

    /**
     * 获取 [所有者]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [所有者]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [所有者]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }

    /**
     * 获取 [商机]
     */
    @JsonProperty("opportunity_id")
    public Integer getOpportunity_id(){
        return this.opportunity_id ;
    }

    /**
     * 设置 [商机]
     */
    @JsonProperty("opportunity_id")
    public void setOpportunity_id(Integer  opportunity_id){
        this.opportunity_id = opportunity_id ;
        this.opportunity_idDirtyFlag = true ;
    }

    /**
     * 获取 [商机]脏标记
     */
    @JsonIgnore
    public boolean getOpportunity_idDirtyFlag(){
        return this.opportunity_idDirtyFlag ;
    }



}
