package cn.ibizlab.odoo.mob.odoo_product.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.mob.odoo_product.service.Product_price_listService;
import cn.ibizlab.odoo.mob.odoo_product.domain.Product_price_list;
import cn.ibizlab.odoo.mob.odoo_product.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Product_price_listController {
	@Autowired
    Product_price_listService product_price_listservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_price_lists/{product_price_list_id}")
    @PreAuthorize("@product_price_list_pms.check(#product_price_list_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("product_price_list_id") Integer product_price_list_id) {
        boolean b = product_price_listservice.remove( product_price_list_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/product_price_lists/{product_price_list_id}")
    @PreAuthorize("@product_price_list_pms.check(#product_price_list_id,'UPDATE')")
    public ResponseEntity<Product_price_list> update(@PathVariable("product_price_list_id") Integer product_price_list_id, @RequestBody Product_price_list product_price_list) {
        Product_price_list product_price_list2 = product_price_listservice.update(product_price_list_id, product_price_list);
        return ResponseEntity.status(HttpStatus.OK).body(product_price_list2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/product_price_lists/{product_price_list_id}")
    @PreAuthorize("@product_price_list_pms.check(#product_price_list_id,'UPDATE')")
    public ResponseEntity<Product_price_list> api_update(@PathVariable("product_price_list_id") Integer product_price_list_id, @RequestBody Product_price_list product_price_list) {
        Product_price_list product_price_list2 = product_price_listservice.update(product_price_list_id, product_price_list);
        return ResponseEntity.status(HttpStatus.OK).body(product_price_list2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/product_price_lists/{product_price_list_id}/updatebatch")
    @PreAuthorize("@product_price_list_pms.check(#product_price_list_id,'UPDATE')")
    public ResponseEntity<Product_price_list> updateBatch(@PathVariable("product_price_list_id") Integer product_price_list_id, @RequestBody Product_price_list product_price_list) {
        Product_price_list product_price_list2 = product_price_listservice.updateBatch(product_price_list_id, product_price_list);
        return ResponseEntity.status(HttpStatus.OK).body(product_price_list2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/product_price_lists")
    @PreAuthorize("@product_price_list_pms.check('CREATE')")
    public ResponseEntity<Product_price_list> create(@RequestBody Product_price_list product_price_list) {
        Product_price_list product_price_list2 = product_price_listservice.create(product_price_list);
        return ResponseEntity.status(HttpStatus.OK).body(product_price_list2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/product_price_lists/{product_price_list_id}")
    @PreAuthorize("@product_price_list_pms.check(#product_price_list_id,'READ')")
    public ResponseEntity<Product_price_list> get(@PathVariable("product_price_list_id") Integer product_price_list_id) {
        Product_price_list product_price_list = product_price_listservice.get( product_price_list_id);
        return ResponseEntity.status(HttpStatus.OK).body(product_price_list);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/product_price_lists/getdraft")
    @PreAuthorize("@product_price_list_pms.check('CREATE')")
    public ResponseEntity<Product_price_list> getDraft() {
        //Product_price_list product_price_list = product_price_listservice.getDraft( product_price_list_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Product_price_list());
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_price_lists/{product_price_list_id}/removebatch")
    @PreAuthorize("@product_price_list_pms.check(#product_price_list_id,'DELETE')")
    public ResponseEntity<Product_price_list> removeBatch(@PathVariable("product_price_list_id") Integer product_price_list_id, @RequestBody Product_price_list product_price_list) {
        Product_price_list product_price_list2 = product_price_listservice.removeBatch(product_price_list_id, product_price_list);
        return ResponseEntity.status(HttpStatus.OK).body(product_price_list2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/product_price_lists/{product_price_list_id}/createbatch")
    @PreAuthorize("@product_price_list_pms.check(#product_price_list_id,'CREATE')")
    public ResponseEntity<Product_price_list> createBatch(@PathVariable("product_price_list_id") Integer product_price_list_id, @RequestBody Product_price_list product_price_list) {
        Product_price_list product_price_list2 = product_price_listservice.createBatch(product_price_list_id, product_price_list);
        return ResponseEntity.status(HttpStatus.OK).body(product_price_list2);
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/product_price_lists/fetchdefault")
    @PreAuthorize("@product_price_list_pms.check('READ')")
	public ResponseEntity<List<Product_price_list>> fetchDefault(Product_price_listSearchContext searchContext,Pageable pageable) {
        
        Page<Product_price_list> page = product_price_listservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}
