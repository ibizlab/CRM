package cn.ibizlab.odoo.mob.odoo_crm.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_lead_tag;
import cn.ibizlab.odoo.mob.odoo_crm.filter.*;


public interface Crm_lead_tagFeignClient {



	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_lead_tags/{crm_lead_tag_id}/updatebatch")
    public Crm_lead_tag updateBatch(@PathVariable("crm_lead_tag_id") Integer crm_lead_tag_id, @RequestBody Crm_lead_tag crm_lead_tag) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_lead_tags/{crm_lead_tag_id}/getdraft")
    public Crm_lead_tag getDraft(@PathVariable("crm_lead_tag_id") Integer crm_lead_tag_id, @RequestBody Crm_lead_tag crm_lead_tag) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_lead_tags/{crm_lead_tag_id}/removebatch")
    public Crm_lead_tag removeBatch(@PathVariable("crm_lead_tag_id") Integer crm_lead_tag_id, @RequestBody Crm_lead_tag crm_lead_tag) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_lead_tags/{crm_lead_tag_id}/createbatch")
    public Crm_lead_tag createBatch(@PathVariable("crm_lead_tag_id") Integer crm_lead_tag_id, @RequestBody Crm_lead_tag crm_lead_tag) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_lead_tags/{crm_lead_tag_id}")
    public Boolean remove(@PathVariable("crm_lead_tag_id") Integer crm_lead_tag_id) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_lead_tags/{crm_lead_tag_id}")
    public Crm_lead_tag update(@PathVariable("crm_lead_tag_id") Integer crm_lead_tag_id, @RequestBody Crm_lead_tag crm_lead_tag) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_lead_tags/{crm_lead_tag_id}")
    public Crm_lead_tag get(@PathVariable("crm_lead_tag_id") Integer crm_lead_tag_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_lead_tags")
    public Crm_lead_tag create(@RequestBody Crm_lead_tag crm_lead_tag) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/crm_lead_tags/fetchdefault")
	public Page<Crm_lead_tag> fetchDefault(Crm_lead_tagSearchContext searchContext) ;
}
