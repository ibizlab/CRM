package cn.ibizlab.odoo.mob.odoo_crm.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.mob.odoo_crm.service.Crm_lead2opportunity_partner_massService;
import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_lead2opportunity_partner_mass;
import cn.ibizlab.odoo.mob.odoo_crm.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Crm_lead2opportunity_partner_massController {
	@Autowired
    Crm_lead2opportunity_partner_massService crm_lead2opportunity_partner_massservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/crm_lead2opportunity_partner_masses/getdraft")
    @PreAuthorize("@crm_lead2opportunity_partner_mass_pms.check('CREATE')")
    public ResponseEntity<Crm_lead2opportunity_partner_mass> getDraft() {
        //Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass = crm_lead2opportunity_partner_massservice.getDraft( crm_lead2opportunity_partner_mass_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Crm_lead2opportunity_partner_mass());
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead2opportunity_partner_masses/{crm_lead2opportunity_partner_mass_id}/removebatch")
    @PreAuthorize("@crm_lead2opportunity_partner_mass_pms.check(#crm_lead2opportunity_partner_mass_id,'DELETE')")
    public ResponseEntity<Crm_lead2opportunity_partner_mass> removeBatch(@PathVariable("crm_lead2opportunity_partner_mass_id") Integer crm_lead2opportunity_partner_mass_id, @RequestBody Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass) {
        Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass2 = crm_lead2opportunity_partner_massservice.removeBatch(crm_lead2opportunity_partner_mass_id, crm_lead2opportunity_partner_mass);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2opportunity_partner_mass2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead2opportunity_partner_masses/{crm_lead2opportunity_partner_mass_id}/createbatch")
    @PreAuthorize("@crm_lead2opportunity_partner_mass_pms.check(#crm_lead2opportunity_partner_mass_id,'CREATE')")
    public ResponseEntity<Crm_lead2opportunity_partner_mass> createBatch(@PathVariable("crm_lead2opportunity_partner_mass_id") Integer crm_lead2opportunity_partner_mass_id, @RequestBody Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass) {
        Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass2 = crm_lead2opportunity_partner_massservice.createBatch(crm_lead2opportunity_partner_mass_id, crm_lead2opportunity_partner_mass);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2opportunity_partner_mass2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/crm_lead2opportunity_partner_masses/{crm_lead2opportunity_partner_mass_id}")
    @PreAuthorize("@crm_lead2opportunity_partner_mass_pms.check(#crm_lead2opportunity_partner_mass_id,'READ')")
    public ResponseEntity<Crm_lead2opportunity_partner_mass> get(@PathVariable("crm_lead2opportunity_partner_mass_id") Integer crm_lead2opportunity_partner_mass_id) {
        Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass = crm_lead2opportunity_partner_massservice.get( crm_lead2opportunity_partner_mass_id);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2opportunity_partner_mass);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_lead2opportunity_partner_masses/{crm_lead2opportunity_partner_mass_id}/updatebatch")
    @PreAuthorize("@crm_lead2opportunity_partner_mass_pms.check(#crm_lead2opportunity_partner_mass_id,'UPDATE')")
    public ResponseEntity<Crm_lead2opportunity_partner_mass> updateBatch(@PathVariable("crm_lead2opportunity_partner_mass_id") Integer crm_lead2opportunity_partner_mass_id, @RequestBody Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass) {
        Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass2 = crm_lead2opportunity_partner_massservice.updateBatch(crm_lead2opportunity_partner_mass_id, crm_lead2opportunity_partner_mass);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2opportunity_partner_mass2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead2opportunity_partner_masses")
    @PreAuthorize("@crm_lead2opportunity_partner_mass_pms.check('CREATE')")
    public ResponseEntity<Crm_lead2opportunity_partner_mass> create(@RequestBody Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass) {
        Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass2 = crm_lead2opportunity_partner_massservice.create(crm_lead2opportunity_partner_mass);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2opportunity_partner_mass2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead2opportunity_partner_masses/{crm_lead2opportunity_partner_mass_id}")
    @PreAuthorize("@crm_lead2opportunity_partner_mass_pms.check(#crm_lead2opportunity_partner_mass_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("crm_lead2opportunity_partner_mass_id") Integer crm_lead2opportunity_partner_mass_id) {
        boolean b = crm_lead2opportunity_partner_massservice.remove( crm_lead2opportunity_partner_mass_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_lead2opportunity_partner_masses/{crm_lead2opportunity_partner_mass_id}")
    @PreAuthorize("@crm_lead2opportunity_partner_mass_pms.check(#crm_lead2opportunity_partner_mass_id,'UPDATE')")
    public ResponseEntity<Crm_lead2opportunity_partner_mass> update(@PathVariable("crm_lead2opportunity_partner_mass_id") Integer crm_lead2opportunity_partner_mass_id, @RequestBody Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass) {
        Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass2 = crm_lead2opportunity_partner_massservice.update(crm_lead2opportunity_partner_mass_id, crm_lead2opportunity_partner_mass);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2opportunity_partner_mass2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/crm_lead2opportunity_partner_masses/{crm_lead2opportunity_partner_mass_id}")
    @PreAuthorize("@crm_lead2opportunity_partner_mass_pms.check(#crm_lead2opportunity_partner_mass_id,'UPDATE')")
    public ResponseEntity<Crm_lead2opportunity_partner_mass> api_update(@PathVariable("crm_lead2opportunity_partner_mass_id") Integer crm_lead2opportunity_partner_mass_id, @RequestBody Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass) {
        Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass2 = crm_lead2opportunity_partner_massservice.update(crm_lead2opportunity_partner_mass_id, crm_lead2opportunity_partner_mass);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2opportunity_partner_mass2);
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/crm_lead2opportunity_partner_masses/fetchdefault")
    @PreAuthorize("@crm_lead2opportunity_partner_mass_pms.check('READ')")
	public ResponseEntity<List<Crm_lead2opportunity_partner_mass>> fetchDefault(Crm_lead2opportunity_partner_massSearchContext searchContext,Pageable pageable) {
        
        Page<Crm_lead2opportunity_partner_mass> page = crm_lead2opportunity_partner_massservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}
