package cn.ibizlab.odoo.mob.odoo_crm.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_partner_binding;
import cn.ibizlab.odoo.mob.odoo_crm.filter.*;


public interface Crm_partner_bindingFeignClient {



	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_partner_bindings/{crm_partner_binding_id}/updatebatch")
    public Crm_partner_binding updateBatch(@PathVariable("crm_partner_binding_id") Integer crm_partner_binding_id, @RequestBody Crm_partner_binding crm_partner_binding) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_partner_bindings/{crm_partner_binding_id}/getdraft")
    public Crm_partner_binding getDraft(@PathVariable("crm_partner_binding_id") Integer crm_partner_binding_id, @RequestBody Crm_partner_binding crm_partner_binding) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_partner_bindings/{crm_partner_binding_id}")
    public Crm_partner_binding get(@PathVariable("crm_partner_binding_id") Integer crm_partner_binding_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_partner_bindings/{crm_partner_binding_id}/createbatch")
    public Crm_partner_binding createBatch(@PathVariable("crm_partner_binding_id") Integer crm_partner_binding_id, @RequestBody Crm_partner_binding crm_partner_binding) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_partner_bindings/{crm_partner_binding_id}/removebatch")
    public Crm_partner_binding removeBatch(@PathVariable("crm_partner_binding_id") Integer crm_partner_binding_id, @RequestBody Crm_partner_binding crm_partner_binding) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_partner_bindings/{crm_partner_binding_id}")
    public Crm_partner_binding update(@PathVariable("crm_partner_binding_id") Integer crm_partner_binding_id, @RequestBody Crm_partner_binding crm_partner_binding) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_partner_bindings")
    public Crm_partner_binding create(@RequestBody Crm_partner_binding crm_partner_binding) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_partner_bindings/{crm_partner_binding_id}")
    public Boolean remove(@PathVariable("crm_partner_binding_id") Integer crm_partner_binding_id) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/crm_partner_bindings/fetchdefault")
	public Page<Crm_partner_binding> fetchDefault(Crm_partner_bindingSearchContext searchContext) ;
}
