package cn.ibizlab.odoo.mob.odoo_mail.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.mob.odoo_mail.domain.Mail_message;
import cn.ibizlab.odoo.mob.odoo_mail.filter.*;


public interface Mail_messageFeignClient {



	@RequestMapping(method = RequestMethod.POST, value = "/web/mail_messages")
    public Mail_message create(@RequestBody Mail_message mail_message) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/mail_messages/{mail_message_id}")
    public Mail_message get(@PathVariable("mail_message_id") Integer mail_message_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/mail_messages/checkkey")
    public Boolean checkKey(@RequestBody Mail_message mail_message) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/mail_messages/{mail_message_id}/updatebatch")
    public Mail_message updateBatch(@PathVariable("mail_message_id") Integer mail_message_id, @RequestBody Mail_message mail_message) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/mail_messages/{mail_message_id}/getdraft")
    public Mail_message getDraft(@PathVariable("mail_message_id") Integer mail_message_id, @RequestBody Mail_message mail_message) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/mail_messages/{mail_message_id}/createbatch")
    public Mail_message createBatch(@PathVariable("mail_message_id") Integer mail_message_id, @RequestBody Mail_message mail_message) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/mail_messages/{mail_message_id}/removebatch")
    public Mail_message removeBatch(@PathVariable("mail_message_id") Integer mail_message_id, @RequestBody Mail_message mail_message) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/mail_messages/{mail_message_id}")
    public Boolean remove(@PathVariable("mail_message_id") Integer mail_message_id) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/mail_messages/{mail_message_id}")
    public Mail_message update(@PathVariable("mail_message_id") Integer mail_message_id, @RequestBody Mail_message mail_message) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/mail_messages/{mail_message_id}/save")
    public Mail_message save(@PathVariable("mail_message_id") Integer mail_message_id, @RequestBody Mail_message mail_message) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/mail_messages/fetchdefault")
	public Page<Mail_message> fetchDefault(Mail_messageSearchContext searchContext) ;
}
