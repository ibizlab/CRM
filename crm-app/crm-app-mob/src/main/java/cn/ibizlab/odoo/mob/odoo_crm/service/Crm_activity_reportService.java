package cn.ibizlab.odoo.mob.odoo_crm.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_activity_report;
import cn.ibizlab.odoo.mob.odoo_crm.filter.*;
import cn.ibizlab.odoo.mob.odoo_crm.feign.Crm_activity_reportFeignClient;
import cn.ibizlab.odoo.mob.MobApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Crm_activity_reportService {

    Crm_activity_reportFeignClient client;

    @Autowired
    public Crm_activity_reportService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Crm_activity_reportFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Crm_activity_reportFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public Crm_activity_report updateBatch(Integer crm_activity_report_id, Crm_activity_report crm_activity_report) {
        return client.updateBatch(crm_activity_report_id, crm_activity_report);
    }

	public Crm_activity_report create(Crm_activity_report crm_activity_report) {
        return client.create(crm_activity_report);
    }

    public Crm_activity_report update(Integer crm_activity_report_id, Crm_activity_report crm_activity_report) {
        return client.update(crm_activity_report_id, crm_activity_report);
    }

    public Crm_activity_report removeBatch(Integer crm_activity_report_id, Crm_activity_report crm_activity_report) {
        return client.removeBatch(crm_activity_report_id, crm_activity_report);
    }

    public boolean remove( Integer crm_activity_report_id) {
        return client.remove( crm_activity_report_id);
    }

    public Crm_activity_report get( Integer crm_activity_report_id) {
        return client.get( crm_activity_report_id);
    }

    public Crm_activity_report createBatch(Integer crm_activity_report_id, Crm_activity_report crm_activity_report) {
        return client.createBatch(crm_activity_report_id, crm_activity_report);
    }

    public Crm_activity_report getDraft(Integer crm_activity_report_id, Crm_activity_report crm_activity_report) {
        return client.getDraft(crm_activity_report_id, crm_activity_report);
    }

	public Page<Crm_activity_report> fetchDefault(Crm_activity_reportSearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }

}
