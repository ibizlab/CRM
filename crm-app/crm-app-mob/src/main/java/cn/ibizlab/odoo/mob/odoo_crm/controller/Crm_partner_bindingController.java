package cn.ibizlab.odoo.mob.odoo_crm.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.mob.odoo_crm.service.Crm_partner_bindingService;
import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_partner_binding;
import cn.ibizlab.odoo.mob.odoo_crm.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Crm_partner_bindingController {
	@Autowired
    Crm_partner_bindingService crm_partner_bindingservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_partner_bindings/{crm_partner_binding_id}/updatebatch")
    @PreAuthorize("@crm_partner_binding_pms.check(#crm_partner_binding_id,'UPDATE')")
    public ResponseEntity<Crm_partner_binding> updateBatch(@PathVariable("crm_partner_binding_id") Integer crm_partner_binding_id, @RequestBody Crm_partner_binding crm_partner_binding) {
        Crm_partner_binding crm_partner_binding2 = crm_partner_bindingservice.updateBatch(crm_partner_binding_id, crm_partner_binding);
        return ResponseEntity.status(HttpStatus.OK).body(crm_partner_binding2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/crm_partner_bindings/getdraft")
    @PreAuthorize("@crm_partner_binding_pms.check('CREATE')")
    public ResponseEntity<Crm_partner_binding> getDraft() {
        //Crm_partner_binding crm_partner_binding = crm_partner_bindingservice.getDraft( crm_partner_binding_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Crm_partner_binding());
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/crm_partner_bindings/{crm_partner_binding_id}")
    @PreAuthorize("@crm_partner_binding_pms.check(#crm_partner_binding_id,'READ')")
    public ResponseEntity<Crm_partner_binding> get(@PathVariable("crm_partner_binding_id") Integer crm_partner_binding_id) {
        Crm_partner_binding crm_partner_binding = crm_partner_bindingservice.get( crm_partner_binding_id);
        return ResponseEntity.status(HttpStatus.OK).body(crm_partner_binding);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/crm_partner_bindings/{crm_partner_binding_id}/createbatch")
    @PreAuthorize("@crm_partner_binding_pms.check(#crm_partner_binding_id,'CREATE')")
    public ResponseEntity<Crm_partner_binding> createBatch(@PathVariable("crm_partner_binding_id") Integer crm_partner_binding_id, @RequestBody Crm_partner_binding crm_partner_binding) {
        Crm_partner_binding crm_partner_binding2 = crm_partner_bindingservice.createBatch(crm_partner_binding_id, crm_partner_binding);
        return ResponseEntity.status(HttpStatus.OK).body(crm_partner_binding2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_partner_bindings/{crm_partner_binding_id}/removebatch")
    @PreAuthorize("@crm_partner_binding_pms.check(#crm_partner_binding_id,'DELETE')")
    public ResponseEntity<Crm_partner_binding> removeBatch(@PathVariable("crm_partner_binding_id") Integer crm_partner_binding_id, @RequestBody Crm_partner_binding crm_partner_binding) {
        Crm_partner_binding crm_partner_binding2 = crm_partner_bindingservice.removeBatch(crm_partner_binding_id, crm_partner_binding);
        return ResponseEntity.status(HttpStatus.OK).body(crm_partner_binding2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_partner_bindings/{crm_partner_binding_id}")
    @PreAuthorize("@crm_partner_binding_pms.check(#crm_partner_binding_id,'UPDATE')")
    public ResponseEntity<Crm_partner_binding> update(@PathVariable("crm_partner_binding_id") Integer crm_partner_binding_id, @RequestBody Crm_partner_binding crm_partner_binding) {
        Crm_partner_binding crm_partner_binding2 = crm_partner_bindingservice.update(crm_partner_binding_id, crm_partner_binding);
        return ResponseEntity.status(HttpStatus.OK).body(crm_partner_binding2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/crm_partner_bindings/{crm_partner_binding_id}")
    @PreAuthorize("@crm_partner_binding_pms.check(#crm_partner_binding_id,'UPDATE')")
    public ResponseEntity<Crm_partner_binding> api_update(@PathVariable("crm_partner_binding_id") Integer crm_partner_binding_id, @RequestBody Crm_partner_binding crm_partner_binding) {
        Crm_partner_binding crm_partner_binding2 = crm_partner_bindingservice.update(crm_partner_binding_id, crm_partner_binding);
        return ResponseEntity.status(HttpStatus.OK).body(crm_partner_binding2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/crm_partner_bindings")
    @PreAuthorize("@crm_partner_binding_pms.check('CREATE')")
    public ResponseEntity<Crm_partner_binding> create(@RequestBody Crm_partner_binding crm_partner_binding) {
        Crm_partner_binding crm_partner_binding2 = crm_partner_bindingservice.create(crm_partner_binding);
        return ResponseEntity.status(HttpStatus.OK).body(crm_partner_binding2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_partner_bindings/{crm_partner_binding_id}")
    @PreAuthorize("@crm_partner_binding_pms.check(#crm_partner_binding_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("crm_partner_binding_id") Integer crm_partner_binding_id) {
        boolean b = crm_partner_bindingservice.remove( crm_partner_binding_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/crm_partner_bindings/fetchdefault")
    @PreAuthorize("@crm_partner_binding_pms.check('READ')")
	public ResponseEntity<List<Crm_partner_binding>> fetchDefault(Crm_partner_bindingSearchContext searchContext,Pageable pageable) {
        
        Page<Crm_partner_binding> page = crm_partner_bindingservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}
