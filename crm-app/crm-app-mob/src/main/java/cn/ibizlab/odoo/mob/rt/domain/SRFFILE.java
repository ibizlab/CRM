package cn.ibizlab.odoo.mob.rt.domain;

import java.sql.Timestamp;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;

/**
 * 实体[FILE]
 */
public class SRFFILE implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * FILE标识
     */
    private String sRFFILEId;

    @JsonIgnore
    @JSONField(serialize=false)
    private boolean sRFFILEIdDirtyFlag;

    /**
     * 更新人
     */
    private String updateMan;

    @JsonIgnore
    @JSONField(serialize=false)
    private boolean updateManDirtyFlag;

    /**
     * FILE名称
     */
    private String sRFFILEName;

    @JsonIgnore
    @JSONField(serialize=false)
    private boolean sRFFILENameDirtyFlag;

    /**
     * 建立时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp createDate;

    @JsonIgnore
    @JSONField(serialize=false)
    private boolean createDateDirtyFlag;

    /**
     * 更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp updateDate;

    @JsonIgnore
    @JSONField(serialize=false)
    private boolean updateDateDirtyFlag;

    /**
     * 建立人
     */
    private String createMan;

    @JsonIgnore
    @JSONField(serialize=false)
    private boolean createManDirtyFlag;

    /**
     * 文件路径
     */
    private String filepath;

    @JsonIgnore
    @JSONField(serialize=false)
    private boolean filepathDirtyFlag;

    /**
     * 文件夹
     */
    private String folder;

    @JsonIgnore
    @JSONField(serialize=false)
    private boolean folderDirtyFlag;

    /**
     * 文件类型
     */
    private String fileext;

    @JsonIgnore
    @JSONField(serialize=false)
    private boolean fileextDirtyFlag;

    /**
     * 文件大小
     */
    private Integer filesize;

    @JsonIgnore
    @JSONField(serialize=false)
    private boolean filesizeDirtyFlag;

    /**
     * 所属者
     */
    private String ownerid;

    @JsonIgnore
    @JSONField(serialize=false)
    private boolean owneridDirtyFlag;

    /**
     * 摘要数据
     */
    private String digestcode;

    @JsonIgnore
    @JSONField(serialize=false)
    private boolean digestcodeDirtyFlag;

    /**
     * 图片宽度
     */
    private Integer picwidth;

    @JsonIgnore
    @JSONField(serialize=false)
    private boolean picwidthDirtyFlag;

    /**
     * 图片高度
     */
    private Integer picheight;

    @JsonIgnore
    @JSONField(serialize=false)
    private boolean picheightDirtyFlag;


    /**
     * 获取 [FILE标识]
     */
    @JsonProperty("srffileid")
    @JSONField(name="srffileid")
    public String getSRFFILEId(){
        return this.sRFFILEId ;
    }

    /**
     * 设置 [FILE标识]
     */
    @JsonProperty("srffileid")
    @JSONField(name="srffileid")
    public void setSRFFILEId(String sRFFILEId){
        this.sRFFILEId = sRFFILEId ;
        this.sRFFILEIdDirtyFlag = true ;
    }

    /**
     * 获取 [FILE标识]脏标记
     */
    @JsonIgnore
    @JSONField(serialize=false)
    public boolean getSRFFILEIdDirtyFlag(){
        return this.sRFFILEIdDirtyFlag ;
    }
    /**
     * 获取 [更新人]
     */
    @JsonProperty("updateman")
    @JSONField(name="updateman")
    public String getUpdateMan(){
        return this.updateMan ;
    }

    /**
     * 设置 [更新人]
     */
    @JsonProperty("updateman")
    @JSONField(name="updateman")
    public void setUpdateMan(String updateMan){
        this.updateMan = updateMan ;
        this.updateManDirtyFlag = true ;
    }

    /**
     * 获取 [更新人]脏标记
     */
    @JsonIgnore
    @JSONField(serialize=false)
    public boolean getUpdateManDirtyFlag(){
        return this.updateManDirtyFlag ;
    }
    /**
     * 获取 [FILE名称]
     */
    @JsonProperty("srffilename")
    @JSONField(name="srffilename")
    public String getSRFFILEName(){
        return this.sRFFILEName ;
    }

    /**
     * 设置 [FILE名称]
     */
    @JsonProperty("srffilename")
    @JSONField(name="srffilename")
    public void setSRFFILEName(String sRFFILEName){
        this.sRFFILEName = sRFFILEName ;
        this.sRFFILENameDirtyFlag = true ;
    }

    /**
     * 获取 [FILE名称]脏标记
     */
    @JsonIgnore
    @JSONField(serialize=false)
    public boolean getSRFFILENameDirtyFlag(){
        return this.sRFFILENameDirtyFlag ;
    }
    /**
     * 获取 [建立时间]
     */
    @JsonProperty("createdate")
    @JSONField(name="createdate")
    public Timestamp getCreateDate(){
        return this.createDate ;
    }

    /**
     * 设置 [建立时间]
     */
    @JsonProperty("createdate")
    @JSONField(name="createdate")
    public void setCreateDate(Timestamp createDate){
        this.createDate = createDate ;
        this.createDateDirtyFlag = true ;
    }

    /**
     * 获取 [建立时间]脏标记
     */
    @JsonIgnore
    @JSONField(serialize=false)
    public boolean getCreateDateDirtyFlag(){
        return this.createDateDirtyFlag ;
    }
    /**
     * 获取 [更新时间]
     */
    @JsonProperty("updatedate")
    @JSONField(name="updatedate")
    public Timestamp getUpdateDate(){
        return this.updateDate ;
    }

    /**
     * 设置 [更新时间]
     */
    @JsonProperty("updatedate")
    @JSONField(name="updatedate")
    public void setUpdateDate(Timestamp updateDate){
        this.updateDate = updateDate ;
        this.updateDateDirtyFlag = true ;
    }

    /**
     * 获取 [更新时间]脏标记
     */
    @JsonIgnore
    @JSONField(serialize=false)
    public boolean getUpdateDateDirtyFlag(){
        return this.updateDateDirtyFlag ;
    }
    /**
     * 获取 [建立人]
     */
    @JsonProperty("createman")
    @JSONField(name="createman")
    public String getCreateMan(){
        return this.createMan ;
    }

    /**
     * 设置 [建立人]
     */
    @JsonProperty("createman")
    @JSONField(name="createman")
    public void setCreateMan(String createMan){
        this.createMan = createMan ;
        this.createManDirtyFlag = true ;
    }

    /**
     * 获取 [建立人]脏标记
     */
    @JsonIgnore
    @JSONField(serialize=false)
    public boolean getCreateManDirtyFlag(){
        return this.createManDirtyFlag ;
    }
    /**
     * 获取 [文件路径]
     */
    @JsonProperty("filepath")
    @JSONField(name="filepath")
    public String getFilepath(){
        return this.filepath ;
    }

    /**
     * 设置 [文件路径]
     */
    @JsonProperty("filepath")
    @JSONField(name="filepath")
    public void setFilepath(String filepath){
        this.filepath = filepath ;
        this.filepathDirtyFlag = true ;
    }

    /**
     * 获取 [文件路径]脏标记
     */
    @JsonIgnore
    @JSONField(serialize=false)
    public boolean getFilepathDirtyFlag(){
        return this.filepathDirtyFlag ;
    }
    /**
     * 获取 [文件夹]
     */
    @JsonProperty("folder")
    @JSONField(name="folder")
    public String getFolder(){
        return this.folder ;
    }

    /**
     * 设置 [文件夹]
     */
    @JsonProperty("folder")
    @JSONField(name="folder")
    public void setFolder(String folder){
        this.folder = folder ;
        this.folderDirtyFlag = true ;
    }

    /**
     * 获取 [文件夹]脏标记
     */
    @JsonIgnore
    @JSONField(serialize=false)
    public boolean getFolderDirtyFlag(){
        return this.folderDirtyFlag ;
    }
    /**
     * 获取 [文件类型]
     */
    @JsonProperty("fileext")
    @JSONField(name="fileext")
    public String getFileext(){
        return this.fileext ;
    }

    /**
     * 设置 [文件类型]
     */
    @JsonProperty("fileext")
    @JSONField(name="fileext")
    public void setFileext(String fileext){
        this.fileext = fileext ;
        this.fileextDirtyFlag = true ;
    }

    /**
     * 获取 [文件类型]脏标记
     */
    @JsonIgnore
    @JSONField(serialize=false)
    public boolean getFileextDirtyFlag(){
        return this.fileextDirtyFlag ;
    }
    /**
     * 获取 [文件大小]
     */
    @JsonProperty("filesize")
    @JSONField(name="filesize")
    public Integer getFilesize(){
        return this.filesize ;
    }

    /**
     * 设置 [文件大小]
     */
    @JsonProperty("filesize")
    @JSONField(name="filesize")
    public void setFilesize(Integer filesize){
        this.filesize = filesize ;
        this.filesizeDirtyFlag = true ;
    }

    /**
     * 获取 [文件大小]脏标记
     */
    @JsonIgnore
    @JSONField(serialize=false)
    public boolean getFilesizeDirtyFlag(){
        return this.filesizeDirtyFlag ;
    }
    /**
     * 获取 [所属者]
     */
    @JsonProperty("ownerid")
    @JSONField(name="ownerid")
    public String getOwnerid(){
        return this.ownerid ;
    }

    /**
     * 设置 [所属者]
     */
    @JsonProperty("ownerid")
    @JSONField(name="ownerid")
    public void setOwnerid(String ownerid){
        this.ownerid = ownerid ;
        this.owneridDirtyFlag = true ;
    }

    /**
     * 获取 [所属者]脏标记
     */
    @JsonIgnore
    @JSONField(serialize=false)
    public boolean getOwneridDirtyFlag(){
        return this.owneridDirtyFlag ;
    }
    /**
     * 获取 [摘要数据]
     */
    @JsonProperty("digestcode")
    @JSONField(name="digestcode")
    public String getDigestcode(){
        return this.digestcode ;
    }

    /**
     * 设置 [摘要数据]
     */
    @JsonProperty("digestcode")
    @JSONField(name="digestcode")
    public void setDigestcode(String digestcode){
        this.digestcode = digestcode ;
        this.digestcodeDirtyFlag = true ;
    }

    /**
     * 获取 [摘要数据]脏标记
     */
    @JsonIgnore
    @JSONField(serialize=false)
    public boolean getDigestcodeDirtyFlag(){
        return this.digestcodeDirtyFlag ;
    }
    /**
     * 获取 [图片宽度]
     */
    @JsonProperty("picwidth")
    @JSONField(name="picwidth")
    public Integer getPicwidth(){
        return this.picwidth ;
    }

    /**
     * 设置 [图片宽度]
     */
    @JsonProperty("picwidth")
    @JSONField(name="picwidth")
    public void setPicwidth(Integer picwidth){
        this.picwidth = picwidth ;
        this.picwidthDirtyFlag = true ;
    }

    /**
     * 获取 [图片宽度]脏标记
     */
    @JsonIgnore
    @JSONField(serialize=false)
    public boolean getPicwidthDirtyFlag(){
        return this.picwidthDirtyFlag ;
    }
    /**
     * 获取 [图片高度]
     */
    @JsonProperty("picheight")
    @JSONField(name="picheight")
    public Integer getPicheight(){
        return this.picheight ;
    }

    /**
     * 设置 [图片高度]
     */
    @JsonProperty("picheight")
    @JSONField(name="picheight")
    public void setPicheight(Integer picheight){
        this.picheight = picheight ;
        this.picheightDirtyFlag = true ;
    }

    /**
     * 获取 [图片高度]脏标记
     */
    @JsonIgnore
    @JSONField(serialize=false)
    public boolean getPicheightDirtyFlag(){
        return this.picheightDirtyFlag ;
    }

}

