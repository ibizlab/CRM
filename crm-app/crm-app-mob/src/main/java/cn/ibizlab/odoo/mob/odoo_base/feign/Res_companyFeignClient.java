package cn.ibizlab.odoo.mob.odoo_base.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.mob.odoo_base.domain.Res_company;
import cn.ibizlab.odoo.mob.odoo_base.filter.*;


public interface Res_companyFeignClient {



	@RequestMapping(method = RequestMethod.GET, value = "/web/res_companies/{res_company_id}")
    public Res_company get(@PathVariable("res_company_id") Integer res_company_id) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/res_companies/{res_company_id}/getdraft")
    public Res_company getDraft(@PathVariable("res_company_id") Integer res_company_id, @RequestBody Res_company res_company) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_companies/{res_company_id}")
    public Boolean remove(@PathVariable("res_company_id") Integer res_company_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_companies/{res_company_id}/createbatch")
    public Res_company createBatch(@PathVariable("res_company_id") Integer res_company_id, @RequestBody Res_company res_company) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_companies/{res_company_id}/updatebatch")
    public Res_company updateBatch(@PathVariable("res_company_id") Integer res_company_id, @RequestBody Res_company res_company) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/res_companies")
    public Res_company create(@RequestBody Res_company res_company) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_companies/{res_company_id}")
    public Res_company update(@PathVariable("res_company_id") Integer res_company_id, @RequestBody Res_company res_company) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_companies/{res_company_id}/removebatch")
    public Res_company removeBatch(@PathVariable("res_company_id") Integer res_company_id, @RequestBody Res_company res_company) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/res_companies/fetchdefault")
	public Page<Res_company> fetchDefault(Res_companySearchContext searchContext) ;
}
