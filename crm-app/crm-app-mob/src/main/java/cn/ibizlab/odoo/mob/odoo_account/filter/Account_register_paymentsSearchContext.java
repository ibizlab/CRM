package cn.ibizlab.odoo.mob.odoo_account.filter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Data
@IBIZLog
public class Account_register_paymentsSearchContext extends SearchContext implements Serializable {

	public String n_payment_type_eq;//[付款类型]
	public String n_partner_type_eq;//[业务伙伴类型]
	public String n_payment_difference_handling_eq;//[付款差异处理]
	public String n_currency_id_text_eq;//[币种]
	public String n_currency_id_text_like;//[币种]
	public String n_write_uid_text_eq;//[最后更新人]
	public String n_write_uid_text_like;//[最后更新人]
	public String n_payment_method_id_text_eq;//[付款方法类型]
	public String n_payment_method_id_text_like;//[付款方法类型]
	public String n_create_uid_text_eq;//[创建人]
	public String n_create_uid_text_like;//[创建人]
	public String n_journal_id_text_eq;//[付款日记账]
	public String n_journal_id_text_like;//[付款日记账]
	public String n_partner_id_text_eq;//[业务伙伴]
	public String n_partner_id_text_like;//[业务伙伴]
	public String n_writeoff_account_id_text_eq;//[差异科目]
	public String n_writeoff_account_id_text_like;//[差异科目]
	public Integer n_writeoff_account_id_eq;//[差异科目]
	public Integer n_partner_bank_account_id_eq;//[收款银行账号]
	public Integer n_write_uid_eq;//[最后更新人]
	public Integer n_create_uid_eq;//[创建人]
	public Integer n_partner_id_eq;//[业务伙伴]
	public Integer n_journal_id_eq;//[付款日记账]
	public Integer n_currency_id_eq;//[币种]
	public Integer n_payment_method_id_eq;//[付款方法类型]

}