package cn.ibizlab.odoo.mob.odoo_crm.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_lead2opportunity_partner_mass;
import cn.ibizlab.odoo.mob.odoo_crm.filter.*;


public interface Crm_lead2opportunity_partner_massFeignClient {



	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_lead2opportunity_partner_masses/{crm_lead2opportunity_partner_mass_id}/getdraft")
    public Crm_lead2opportunity_partner_mass getDraft(@PathVariable("crm_lead2opportunity_partner_mass_id") Integer crm_lead2opportunity_partner_mass_id, @RequestBody Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_lead2opportunity_partner_masses/{crm_lead2opportunity_partner_mass_id}/removebatch")
    public Crm_lead2opportunity_partner_mass removeBatch(@PathVariable("crm_lead2opportunity_partner_mass_id") Integer crm_lead2opportunity_partner_mass_id, @RequestBody Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_lead2opportunity_partner_masses/{crm_lead2opportunity_partner_mass_id}/createbatch")
    public Crm_lead2opportunity_partner_mass createBatch(@PathVariable("crm_lead2opportunity_partner_mass_id") Integer crm_lead2opportunity_partner_mass_id, @RequestBody Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_lead2opportunity_partner_masses/{crm_lead2opportunity_partner_mass_id}")
    public Crm_lead2opportunity_partner_mass get(@PathVariable("crm_lead2opportunity_partner_mass_id") Integer crm_lead2opportunity_partner_mass_id) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_lead2opportunity_partner_masses/{crm_lead2opportunity_partner_mass_id}/updatebatch")
    public Crm_lead2opportunity_partner_mass updateBatch(@PathVariable("crm_lead2opportunity_partner_mass_id") Integer crm_lead2opportunity_partner_mass_id, @RequestBody Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_lead2opportunity_partner_masses")
    public Crm_lead2opportunity_partner_mass create(@RequestBody Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_lead2opportunity_partner_masses/{crm_lead2opportunity_partner_mass_id}")
    public Boolean remove(@PathVariable("crm_lead2opportunity_partner_mass_id") Integer crm_lead2opportunity_partner_mass_id) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_lead2opportunity_partner_masses/{crm_lead2opportunity_partner_mass_id}")
    public Crm_lead2opportunity_partner_mass update(@PathVariable("crm_lead2opportunity_partner_mass_id") Integer crm_lead2opportunity_partner_mass_id, @RequestBody Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/crm_lead2opportunity_partner_masses/fetchdefault")
	public Page<Crm_lead2opportunity_partner_mass> fetchDefault(Crm_lead2opportunity_partner_massSearchContext searchContext) ;
}
