package cn.ibizlab.odoo.mob.odoo_account.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.mob.odoo_account.domain.Account_journal;
import cn.ibizlab.odoo.mob.odoo_account.filter.*;
import cn.ibizlab.odoo.mob.odoo_account.feign.Account_journalFeignClient;
import cn.ibizlab.odoo.mob.MobApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Account_journalService {

    Account_journalFeignClient client;

    @Autowired
    public Account_journalService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Account_journalFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Account_journalFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public Account_journal createBatch(Integer account_journal_id, Account_journal account_journal) {
        return client.createBatch(account_journal_id, account_journal);
    }

    public Account_journal get( Integer account_journal_id) {
        return client.get( account_journal_id);
    }

    public Account_journal getDraft(Integer account_journal_id, Account_journal account_journal) {
        return client.getDraft(account_journal_id, account_journal);
    }

	public Account_journal create(Account_journal account_journal) {
        return client.create(account_journal);
    }

    public Account_journal removeBatch(Integer account_journal_id, Account_journal account_journal) {
        return client.removeBatch(account_journal_id, account_journal);
    }

    public Account_journal update(Integer account_journal_id, Account_journal account_journal) {
        return client.update(account_journal_id, account_journal);
    }

    public boolean remove( Integer account_journal_id) {
        return client.remove( account_journal_id);
    }

    public Account_journal updateBatch(Integer account_journal_id, Account_journal account_journal) {
        return client.updateBatch(account_journal_id, account_journal);
    }

	public Page<Account_journal> fetchDefault(Account_journalSearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }

}
