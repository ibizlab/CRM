package cn.ibizlab.odoo.mob.odoo_base.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.mob.odoo_base.domain.Res_users;
import cn.ibizlab.odoo.mob.odoo_base.filter.*;
import cn.ibizlab.odoo.mob.odoo_base.feign.Res_usersFeignClient;
import cn.ibizlab.odoo.mob.MobApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Res_usersService {

    Res_usersFeignClient client;

    @Autowired
    public Res_usersService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Res_usersFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Res_usersFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public Res_users update(Integer res_users_id, Res_users res_users) {
        return client.update(res_users_id, res_users);
    }

    public Res_users save(Integer res_users_id, Res_users res_users) {
        return client.save(res_users_id, res_users);
    }

	public Res_users create(Res_users res_users) {
        return client.create(res_users);
    }

    public boolean checkKey(Res_users res_users) {
        return client.checkKey(res_users);
    }

    public Res_users get( Integer res_users_id) {
        return client.get( res_users_id);
    }

    public Res_users updateBatch(Integer res_users_id, Res_users res_users) {
        return client.updateBatch(res_users_id, res_users);
    }

    public Res_users createBatch(Integer res_users_id, Res_users res_users) {
        return client.createBatch(res_users_id, res_users);
    }

    public Res_users removeBatch(Integer res_users_id, Res_users res_users) {
        return client.removeBatch(res_users_id, res_users);
    }

    public boolean remove( Integer res_users_id) {
        return client.remove( res_users_id);
    }

    public Res_users getDraft(Integer res_users_id, Res_users res_users) {
        return client.getDraft(res_users_id, res_users);
    }

	public Page<Res_users> fetchDefault(Res_usersSearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }


    public Res_users updateByCrm_team(Integer crm_team_id, Integer res_users_id, Res_users res_users) {
        return client.updateByCrm_team(crm_team_id, res_users_id, res_users);
    }

    public Res_users saveByCrm_team(Integer crm_team_id, Integer res_users_id, Res_users res_users) {
        return client.saveByCrm_team(crm_team_id, res_users_id, res_users);
    }

	public Res_users createByCrm_team(Integer crm_team_id, Res_users res_users) {
        return client.createByCrm_team(crm_team_id, res_users);
    }

    public boolean checkKeyByCrm_team(Integer crm_team_id, Res_users res_users) {
        return client.checkKeyByCrm_team(crm_team_id, res_users);
    }

    public Res_users getByCrm_team(Integer crm_team_id,  Integer res_users_id) {
        return client.getByCrm_team(crm_team_id,  res_users_id);
    }

    public Res_users updateBatchByCrm_team(Integer crm_team_id, Integer res_users_id, Res_users res_users) {
        return client.updateBatchByCrm_team(crm_team_id, res_users_id, res_users);
    }

    public Res_users createBatchByCrm_team(Integer crm_team_id, Integer res_users_id, Res_users res_users) {
        return client.createBatchByCrm_team(crm_team_id, res_users_id, res_users);
    }

    public Res_users removeBatchByCrm_team(Integer crm_team_id, Integer res_users_id, Res_users res_users) {
        return client.removeBatchByCrm_team(crm_team_id, res_users_id, res_users);
    }

    public boolean removeByCrm_team(Integer crm_team_id,  Integer res_users_id) {
        return client.removeByCrm_team(crm_team_id,  res_users_id);
    }

    public Res_users getDraftByCrm_team(Integer crm_team_id, Integer res_users_id, Res_users res_users) {
        return client.getDraftByCrm_team(crm_team_id, res_users_id, res_users);
    }

	public Page<Res_users> fetchDefaultByCrm_team(Integer crm_team_id , Res_usersSearchContext searchContext) {
        return client.fetchDefaultByCrm_team(crm_team_id , searchContext);
    }

}
