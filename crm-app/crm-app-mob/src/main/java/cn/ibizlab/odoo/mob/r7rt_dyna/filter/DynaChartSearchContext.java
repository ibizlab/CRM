package cn.ibizlab.odoo.mob.r7rt_dyna.filter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Data
@IBIZLog
public class DynaChartSearchContext extends SearchContext implements Serializable {

	public String n_dynachartname_like;//[动态图表名称]

}