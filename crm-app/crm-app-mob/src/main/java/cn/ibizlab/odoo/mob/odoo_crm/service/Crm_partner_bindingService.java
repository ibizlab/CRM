package cn.ibizlab.odoo.mob.odoo_crm.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_partner_binding;
import cn.ibizlab.odoo.mob.odoo_crm.filter.*;
import cn.ibizlab.odoo.mob.odoo_crm.feign.Crm_partner_bindingFeignClient;
import cn.ibizlab.odoo.mob.MobApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Crm_partner_bindingService {

    Crm_partner_bindingFeignClient client;

    @Autowired
    public Crm_partner_bindingService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Crm_partner_bindingFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Crm_partner_bindingFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public Crm_partner_binding updateBatch(Integer crm_partner_binding_id, Crm_partner_binding crm_partner_binding) {
        return client.updateBatch(crm_partner_binding_id, crm_partner_binding);
    }

    public Crm_partner_binding getDraft(Integer crm_partner_binding_id, Crm_partner_binding crm_partner_binding) {
        return client.getDraft(crm_partner_binding_id, crm_partner_binding);
    }

    public Crm_partner_binding get( Integer crm_partner_binding_id) {
        return client.get( crm_partner_binding_id);
    }

    public Crm_partner_binding createBatch(Integer crm_partner_binding_id, Crm_partner_binding crm_partner_binding) {
        return client.createBatch(crm_partner_binding_id, crm_partner_binding);
    }

    public Crm_partner_binding removeBatch(Integer crm_partner_binding_id, Crm_partner_binding crm_partner_binding) {
        return client.removeBatch(crm_partner_binding_id, crm_partner_binding);
    }

    public Crm_partner_binding update(Integer crm_partner_binding_id, Crm_partner_binding crm_partner_binding) {
        return client.update(crm_partner_binding_id, crm_partner_binding);
    }

	public Crm_partner_binding create(Crm_partner_binding crm_partner_binding) {
        return client.create(crm_partner_binding);
    }

    public boolean remove( Integer crm_partner_binding_id) {
        return client.remove( crm_partner_binding_id);
    }

	public Page<Crm_partner_binding> fetchDefault(Crm_partner_bindingSearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }

}
