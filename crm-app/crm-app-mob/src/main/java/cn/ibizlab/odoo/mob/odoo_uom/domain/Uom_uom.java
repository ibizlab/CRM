package cn.ibizlab.odoo.mob.odoo_uom.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 应用实体[产品计量单位]
 */
public class Uom_uom implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 单位
     */
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;
    
    /**
     * 更大比率
     */
    private Double factor_inv;

    @JsonIgnore
    private boolean factor_invDirtyFlag;
    
    /**
     * 有效
     */
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;
    
    /**
     * 舍入精度
     */
    private Double rounding;

    @JsonIgnore
    private boolean roundingDirtyFlag;
    
    /**
     * 显示名称
     */
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;
    
    /**
     * 比例
     */
    private Double factor;

    @JsonIgnore
    private boolean factorDirtyFlag;
    
    /**
     * ID
     */
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;
    
    /**
     * 类型
     */
    private String uom_type;

    @JsonIgnore
    private boolean uom_typeDirtyFlag;
    
    /**
     * 最后更新者
     */
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;
    
    /**
     * 类别
     */
    private String category_id_text;

    @JsonIgnore
    private boolean category_id_textDirtyFlag;
    
    /**
     * 分组销售点中的产品
     */
    private String is_pos_groupable;

    @JsonIgnore
    private boolean is_pos_groupableDirtyFlag;
    
    /**
     * 创建人
     */
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;
    
    /**
     * 计量单位的类别
     */
    private String measure_type;

    @JsonIgnore
    private boolean measure_typeDirtyFlag;
    
    /**
     * 创建人
     */
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;
    
    /**
     * 类别
     */
    private Integer category_id;

    @JsonIgnore
    private boolean category_idDirtyFlag;
    
    /**
     * 最后更新者
     */
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;
    

    /**
     * 获取 [单位]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [单位]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [单位]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }

    /**
     * 获取 [更大比率]
     */
    @JsonProperty("factor_inv")
    public Double getFactor_inv(){
        return this.factor_inv ;
    }

    /**
     * 设置 [更大比率]
     */
    @JsonProperty("factor_inv")
    public void setFactor_inv(Double  factor_inv){
        this.factor_inv = factor_inv ;
        this.factor_invDirtyFlag = true ;
    }

    /**
     * 获取 [更大比率]脏标记
     */
    @JsonIgnore
    public boolean getFactor_invDirtyFlag(){
        return this.factor_invDirtyFlag ;
    }

    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }

    /**
     * 获取 [舍入精度]
     */
    @JsonProperty("rounding")
    public Double getRounding(){
        return this.rounding ;
    }

    /**
     * 设置 [舍入精度]
     */
    @JsonProperty("rounding")
    public void setRounding(Double  rounding){
        this.rounding = rounding ;
        this.roundingDirtyFlag = true ;
    }

    /**
     * 获取 [舍入精度]脏标记
     */
    @JsonIgnore
    public boolean getRoundingDirtyFlag(){
        return this.roundingDirtyFlag ;
    }

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }

    /**
     * 获取 [比例]
     */
    @JsonProperty("factor")
    public Double getFactor(){
        return this.factor ;
    }

    /**
     * 设置 [比例]
     */
    @JsonProperty("factor")
    public void setFactor(Double  factor){
        this.factor = factor ;
        this.factorDirtyFlag = true ;
    }

    /**
     * 获取 [比例]脏标记
     */
    @JsonIgnore
    public boolean getFactorDirtyFlag(){
        return this.factorDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }

    /**
     * 获取 [类型]
     */
    @JsonProperty("uom_type")
    public String getUom_type(){
        return this.uom_type ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("uom_type")
    public void setUom_type(String  uom_type){
        this.uom_type = uom_type ;
        this.uom_typeDirtyFlag = true ;
    }

    /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getUom_typeDirtyFlag(){
        return this.uom_typeDirtyFlag ;
    }

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [类别]
     */
    @JsonProperty("category_id_text")
    public String getCategory_id_text(){
        return this.category_id_text ;
    }

    /**
     * 设置 [类别]
     */
    @JsonProperty("category_id_text")
    public void setCategory_id_text(String  category_id_text){
        this.category_id_text = category_id_text ;
        this.category_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [类别]脏标记
     */
    @JsonIgnore
    public boolean getCategory_id_textDirtyFlag(){
        return this.category_id_textDirtyFlag ;
    }

    /**
     * 获取 [分组销售点中的产品]
     */
    @JsonProperty("is_pos_groupable")
    public String getIs_pos_groupable(){
        return this.is_pos_groupable ;
    }

    /**
     * 设置 [分组销售点中的产品]
     */
    @JsonProperty("is_pos_groupable")
    public void setIs_pos_groupable(String  is_pos_groupable){
        this.is_pos_groupable = is_pos_groupable ;
        this.is_pos_groupableDirtyFlag = true ;
    }

    /**
     * 获取 [分组销售点中的产品]脏标记
     */
    @JsonIgnore
    public boolean getIs_pos_groupableDirtyFlag(){
        return this.is_pos_groupableDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [计量单位的类别]
     */
    @JsonProperty("measure_type")
    public String getMeasure_type(){
        return this.measure_type ;
    }

    /**
     * 设置 [计量单位的类别]
     */
    @JsonProperty("measure_type")
    public void setMeasure_type(String  measure_type){
        this.measure_type = measure_type ;
        this.measure_typeDirtyFlag = true ;
    }

    /**
     * 获取 [计量单位的类别]脏标记
     */
    @JsonIgnore
    public boolean getMeasure_typeDirtyFlag(){
        return this.measure_typeDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }

    /**
     * 获取 [类别]
     */
    @JsonProperty("category_id")
    public Integer getCategory_id(){
        return this.category_id ;
    }

    /**
     * 设置 [类别]
     */
    @JsonProperty("category_id")
    public void setCategory_id(Integer  category_id){
        this.category_id = category_id ;
        this.category_idDirtyFlag = true ;
    }

    /**
     * 获取 [类别]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idDirtyFlag(){
        return this.category_idDirtyFlag ;
    }

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }



}
