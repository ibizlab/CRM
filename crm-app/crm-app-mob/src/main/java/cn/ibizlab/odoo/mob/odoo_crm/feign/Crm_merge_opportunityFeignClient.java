package cn.ibizlab.odoo.mob.odoo_crm.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_merge_opportunity;
import cn.ibizlab.odoo.mob.odoo_crm.filter.*;


public interface Crm_merge_opportunityFeignClient {



	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_merge_opportunities/{crm_merge_opportunity_id}/updatebatch")
    public Crm_merge_opportunity updateBatch(@PathVariable("crm_merge_opportunity_id") Integer crm_merge_opportunity_id, @RequestBody Crm_merge_opportunity crm_merge_opportunity) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_merge_opportunities/{crm_merge_opportunity_id}/getdraft")
    public Crm_merge_opportunity getDraft(@PathVariable("crm_merge_opportunity_id") Integer crm_merge_opportunity_id, @RequestBody Crm_merge_opportunity crm_merge_opportunity) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_merge_opportunities/{crm_merge_opportunity_id}")
    public Crm_merge_opportunity update(@PathVariable("crm_merge_opportunity_id") Integer crm_merge_opportunity_id, @RequestBody Crm_merge_opportunity crm_merge_opportunity) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_merge_opportunities")
    public Crm_merge_opportunity create(@RequestBody Crm_merge_opportunity crm_merge_opportunity) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_merge_opportunities/{crm_merge_opportunity_id}/removebatch")
    public Crm_merge_opportunity removeBatch(@PathVariable("crm_merge_opportunity_id") Integer crm_merge_opportunity_id, @RequestBody Crm_merge_opportunity crm_merge_opportunity) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_merge_opportunities/{crm_merge_opportunity_id}")
    public Boolean remove(@PathVariable("crm_merge_opportunity_id") Integer crm_merge_opportunity_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_merge_opportunities/{crm_merge_opportunity_id}/createbatch")
    public Crm_merge_opportunity createBatch(@PathVariable("crm_merge_opportunity_id") Integer crm_merge_opportunity_id, @RequestBody Crm_merge_opportunity crm_merge_opportunity) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_merge_opportunities/{crm_merge_opportunity_id}")
    public Crm_merge_opportunity get(@PathVariable("crm_merge_opportunity_id") Integer crm_merge_opportunity_id) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/crm_merge_opportunities/fetchdefault")
	public Page<Crm_merge_opportunity> fetchDefault(Crm_merge_opportunitySearchContext searchContext) ;
}
