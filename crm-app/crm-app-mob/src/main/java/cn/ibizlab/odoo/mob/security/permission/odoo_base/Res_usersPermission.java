package cn.ibizlab.odoo.mob.security.permission.odoo_base;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;

import cn.ibizlab.odoo.mob.odoo_base.service.Res_usersService;
import cn.ibizlab.odoo.mob.odoo_base.domain.Res_users;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Component("res_users_pms")
public class Res_usersPermission {

    @Autowired
    Res_usersService res_usersservice;

    public boolean check(String action){
        return true ;
    }

    public boolean check(Integer res_users_id, String action){
        return true ;
    }

    public boolean checkByCrm_team(Integer crm_team_id,  Integer res_users_id, String action){
        return true ;
    }

    public boolean checkByCrm_team(Integer crm_team_id,  String action){
        return true ;
    }


}
