package cn.ibizlab.odoo.mob.odoo_crm.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_lead2opportunity_partner_mass;
import cn.ibizlab.odoo.mob.odoo_crm.filter.*;
import cn.ibizlab.odoo.mob.odoo_crm.feign.Crm_lead2opportunity_partner_massFeignClient;
import cn.ibizlab.odoo.mob.MobApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Crm_lead2opportunity_partner_massService {

    Crm_lead2opportunity_partner_massFeignClient client;

    @Autowired
    public Crm_lead2opportunity_partner_massService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Crm_lead2opportunity_partner_massFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Crm_lead2opportunity_partner_massFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public Crm_lead2opportunity_partner_mass getDraft(Integer crm_lead2opportunity_partner_mass_id, Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass) {
        return client.getDraft(crm_lead2opportunity_partner_mass_id, crm_lead2opportunity_partner_mass);
    }

    public Crm_lead2opportunity_partner_mass removeBatch(Integer crm_lead2opportunity_partner_mass_id, Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass) {
        return client.removeBatch(crm_lead2opportunity_partner_mass_id, crm_lead2opportunity_partner_mass);
    }

    public Crm_lead2opportunity_partner_mass createBatch(Integer crm_lead2opportunity_partner_mass_id, Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass) {
        return client.createBatch(crm_lead2opportunity_partner_mass_id, crm_lead2opportunity_partner_mass);
    }

    public Crm_lead2opportunity_partner_mass get( Integer crm_lead2opportunity_partner_mass_id) {
        return client.get( crm_lead2opportunity_partner_mass_id);
    }

    public Crm_lead2opportunity_partner_mass updateBatch(Integer crm_lead2opportunity_partner_mass_id, Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass) {
        return client.updateBatch(crm_lead2opportunity_partner_mass_id, crm_lead2opportunity_partner_mass);
    }

	public Crm_lead2opportunity_partner_mass create(Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass) {
        return client.create(crm_lead2opportunity_partner_mass);
    }

    public boolean remove( Integer crm_lead2opportunity_partner_mass_id) {
        return client.remove( crm_lead2opportunity_partner_mass_id);
    }

    public Crm_lead2opportunity_partner_mass update(Integer crm_lead2opportunity_partner_mass_id, Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass) {
        return client.update(crm_lead2opportunity_partner_mass_id, crm_lead2opportunity_partner_mass);
    }

	public Page<Crm_lead2opportunity_partner_mass> fetchDefault(Crm_lead2opportunity_partner_massSearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }

}
