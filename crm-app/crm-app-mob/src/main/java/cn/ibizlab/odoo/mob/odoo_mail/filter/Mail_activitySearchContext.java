package cn.ibizlab.odoo.mob.odoo_mail.filter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Data
@IBIZLog
public class Mail_activitySearchContext extends SearchContext implements Serializable {

	public String n_display_name_like;//[显示名称]
	public String n_state_eq;//[状态]
	public String n_user_id_text_eq;//[分派给]
	public String n_user_id_text_like;//[分派给]
	public String n_activity_type_id_text_eq;//[活动]
	public String n_activity_type_id_text_like;//[活动]
	public String n_previous_activity_type_id_text_eq;//[前一活动类型]
	public String n_previous_activity_type_id_text_like;//[前一活动类型]
	public String n_note_id_text_eq;//[相关便签]
	public String n_note_id_text_like;//[相关便签]
	public String n_create_user_id_text_eq;//[建立者]
	public String n_create_user_id_text_like;//[建立者]
	public String n_create_uid_text_eq;//[创建人]
	public String n_create_uid_text_like;//[创建人]
	public String n_write_uid_text_eq;//[最后更新者]
	public String n_write_uid_text_like;//[最后更新者]
	public String n_recommended_activity_type_id_text_eq;//[推荐的活动类型]
	public String n_recommended_activity_type_id_text_like;//[推荐的活动类型]
	public String n_calendar_event_id_text_eq;//[日历会议]
	public String n_calendar_event_id_text_like;//[日历会议]
	public Integer n_recommended_activity_type_id_eq;//[推荐的活动类型]
	public Integer n_activity_type_id_eq;//[活动]
	public Integer n_create_user_id_eq;//[建立者]
	public Integer n_create_uid_eq;//[创建人]
	public Integer n_user_id_eq;//[分派给]
	public Integer n_note_id_eq;//[相关便签]
	public Integer n_previous_activity_type_id_eq;//[前一活动类型]
	public Integer n_calendar_event_id_eq;//[日历会议]
	public Integer n_write_uid_eq;//[最后更新者]

}