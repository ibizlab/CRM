package cn.ibizlab.odoo.mob.odoo_base.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.mob.odoo_base.service.Res_companyService;
import cn.ibizlab.odoo.mob.odoo_base.domain.Res_company;
import cn.ibizlab.odoo.mob.odoo_base.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Res_companyController {
	@Autowired
    Res_companyService res_companyservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/res_companies/{res_company_id}")
    @PreAuthorize("@res_company_pms.check(#res_company_id,'READ')")
    public ResponseEntity<Res_company> get(@PathVariable("res_company_id") Integer res_company_id) {
        Res_company res_company = res_companyservice.get( res_company_id);
        return ResponseEntity.status(HttpStatus.OK).body(res_company);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/res_companies/getdraft")
    @PreAuthorize("@res_company_pms.check('CREATE')")
    public ResponseEntity<Res_company> getDraft() {
        //Res_company res_company = res_companyservice.getDraft( res_company_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Res_company());
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_companies/{res_company_id}")
    @PreAuthorize("@res_company_pms.check(#res_company_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("res_company_id") Integer res_company_id) {
        boolean b = res_companyservice.remove( res_company_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/res_companies/{res_company_id}/createbatch")
    @PreAuthorize("@res_company_pms.check(#res_company_id,'CREATE')")
    public ResponseEntity<Res_company> createBatch(@PathVariable("res_company_id") Integer res_company_id, @RequestBody Res_company res_company) {
        Res_company res_company2 = res_companyservice.createBatch(res_company_id, res_company);
        return ResponseEntity.status(HttpStatus.OK).body(res_company2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/res_companies/{res_company_id}/updatebatch")
    @PreAuthorize("@res_company_pms.check(#res_company_id,'UPDATE')")
    public ResponseEntity<Res_company> updateBatch(@PathVariable("res_company_id") Integer res_company_id, @RequestBody Res_company res_company) {
        Res_company res_company2 = res_companyservice.updateBatch(res_company_id, res_company);
        return ResponseEntity.status(HttpStatus.OK).body(res_company2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/res_companies")
    @PreAuthorize("@res_company_pms.check('CREATE')")
    public ResponseEntity<Res_company> create(@RequestBody Res_company res_company) {
        Res_company res_company2 = res_companyservice.create(res_company);
        return ResponseEntity.status(HttpStatus.OK).body(res_company2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/res_companies/{res_company_id}")
    @PreAuthorize("@res_company_pms.check(#res_company_id,'UPDATE')")
    public ResponseEntity<Res_company> update(@PathVariable("res_company_id") Integer res_company_id, @RequestBody Res_company res_company) {
        Res_company res_company2 = res_companyservice.update(res_company_id, res_company);
        return ResponseEntity.status(HttpStatus.OK).body(res_company2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/res_companies/{res_company_id}")
    @PreAuthorize("@res_company_pms.check(#res_company_id,'UPDATE')")
    public ResponseEntity<Res_company> api_update(@PathVariable("res_company_id") Integer res_company_id, @RequestBody Res_company res_company) {
        Res_company res_company2 = res_companyservice.update(res_company_id, res_company);
        return ResponseEntity.status(HttpStatus.OK).body(res_company2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_companies/{res_company_id}/removebatch")
    @PreAuthorize("@res_company_pms.check(#res_company_id,'DELETE')")
    public ResponseEntity<Res_company> removeBatch(@PathVariable("res_company_id") Integer res_company_id, @RequestBody Res_company res_company) {
        Res_company res_company2 = res_companyservice.removeBatch(res_company_id, res_company);
        return ResponseEntity.status(HttpStatus.OK).body(res_company2);
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/res_companies/fetchdefault")
    @PreAuthorize("@res_company_pms.check('READ')")
	public ResponseEntity<List<Res_company>> fetchDefault(Res_companySearchContext searchContext,Pageable pageable) {
        
        Page<Res_company> page = res_companyservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}
