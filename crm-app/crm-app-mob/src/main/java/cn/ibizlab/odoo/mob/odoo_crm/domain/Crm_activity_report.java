package cn.ibizlab.odoo.mob.odoo_crm.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 应用实体[CRM活动分析]
 */
public class Crm_activity_report implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 有效
     */
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;
    
    /**
     * 概率
     */
    private Double probability;

    @JsonIgnore
    private boolean probabilityDirtyFlag;
    
    /**
     * 类型
     */
    private String lead_type;

    @JsonIgnore
    private boolean lead_typeDirtyFlag;
    
    /**
     * 显示名称
     */
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;
    
    /**
     * 日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;
    
    /**
     * ID
     */
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;
    
    /**
     * 摘要
     */
    private String subject;

    @JsonIgnore
    private boolean subjectDirtyFlag;
    
    /**
     * 公司
     */
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;
    
    /**
     * 销售团队
     */
    private String team_id_text;

    @JsonIgnore
    private boolean team_id_textDirtyFlag;
    
    /**
     * 活动类型
     */
    private String mail_activity_type_id_text;

    @JsonIgnore
    private boolean mail_activity_type_id_textDirtyFlag;
    
    /**
     * 线索
     */
    private String lead_id_text;

    @JsonIgnore
    private boolean lead_id_textDirtyFlag;
    
    /**
     * 销售员
     */
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;
    
    /**
     * 子类型
     */
    private String subtype_id_text;

    @JsonIgnore
    private boolean subtype_id_textDirtyFlag;
    
    /**
     * 国家
     */
    private String country_id_text;

    @JsonIgnore
    private boolean country_id_textDirtyFlag;
    
    /**
     * 业务合作伙伴/客户
     */
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;
    
    /**
     * 阶段
     */
    private String stage_id_text;

    @JsonIgnore
    private boolean stage_id_textDirtyFlag;
    
    /**
     * 创建人
     */
    private String author_id_text;

    @JsonIgnore
    private boolean author_id_textDirtyFlag;
    
    /**
     * 创建人
     */
    private Integer author_id;

    @JsonIgnore
    private boolean author_idDirtyFlag;
    
    /**
     * 国家
     */
    private Integer country_id;

    @JsonIgnore
    private boolean country_idDirtyFlag;
    
    /**
     * 阶段
     */
    private Integer stage_id;

    @JsonIgnore
    private boolean stage_idDirtyFlag;
    
    /**
     * 子类型
     */
    private Integer subtype_id;

    @JsonIgnore
    private boolean subtype_idDirtyFlag;
    
    /**
     * 公司
     */
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;
    
    /**
     * 线索
     */
    private Integer lead_id;

    @JsonIgnore
    private boolean lead_idDirtyFlag;
    
    /**
     * 活动类型
     */
    private Integer mail_activity_type_id;

    @JsonIgnore
    private boolean mail_activity_type_idDirtyFlag;
    
    /**
     * 销售员
     */
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;
    
    /**
     * 业务合作伙伴/客户
     */
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;
    
    /**
     * 销售团队
     */
    private Integer team_id;

    @JsonIgnore
    private boolean team_idDirtyFlag;
    

    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }

    /**
     * 获取 [概率]
     */
    @JsonProperty("probability")
    public Double getProbability(){
        return this.probability ;
    }

    /**
     * 设置 [概率]
     */
    @JsonProperty("probability")
    public void setProbability(Double  probability){
        this.probability = probability ;
        this.probabilityDirtyFlag = true ;
    }

    /**
     * 获取 [概率]脏标记
     */
    @JsonIgnore
    public boolean getProbabilityDirtyFlag(){
        return this.probabilityDirtyFlag ;
    }

    /**
     * 获取 [类型]
     */
    @JsonProperty("lead_type")
    public String getLead_type(){
        return this.lead_type ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("lead_type")
    public void setLead_type(String  lead_type){
        this.lead_type = lead_type ;
        this.lead_typeDirtyFlag = true ;
    }

    /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getLead_typeDirtyFlag(){
        return this.lead_typeDirtyFlag ;
    }

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }

    /**
     * 获取 [日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }

    /**
     * 获取 [摘要]
     */
    @JsonProperty("subject")
    public String getSubject(){
        return this.subject ;
    }

    /**
     * 设置 [摘要]
     */
    @JsonProperty("subject")
    public void setSubject(String  subject){
        this.subject = subject ;
        this.subjectDirtyFlag = true ;
    }

    /**
     * 获取 [摘要]脏标记
     */
    @JsonIgnore
    public boolean getSubjectDirtyFlag(){
        return this.subjectDirtyFlag ;
    }

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }

    /**
     * 获取 [销售团队]
     */
    @JsonProperty("team_id_text")
    public String getTeam_id_text(){
        return this.team_id_text ;
    }

    /**
     * 设置 [销售团队]
     */
    @JsonProperty("team_id_text")
    public void setTeam_id_text(String  team_id_text){
        this.team_id_text = team_id_text ;
        this.team_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [销售团队]脏标记
     */
    @JsonIgnore
    public boolean getTeam_id_textDirtyFlag(){
        return this.team_id_textDirtyFlag ;
    }

    /**
     * 获取 [活动类型]
     */
    @JsonProperty("mail_activity_type_id_text")
    public String getMail_activity_type_id_text(){
        return this.mail_activity_type_id_text ;
    }

    /**
     * 设置 [活动类型]
     */
    @JsonProperty("mail_activity_type_id_text")
    public void setMail_activity_type_id_text(String  mail_activity_type_id_text){
        this.mail_activity_type_id_text = mail_activity_type_id_text ;
        this.mail_activity_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [活动类型]脏标记
     */
    @JsonIgnore
    public boolean getMail_activity_type_id_textDirtyFlag(){
        return this.mail_activity_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [线索]
     */
    @JsonProperty("lead_id_text")
    public String getLead_id_text(){
        return this.lead_id_text ;
    }

    /**
     * 设置 [线索]
     */
    @JsonProperty("lead_id_text")
    public void setLead_id_text(String  lead_id_text){
        this.lead_id_text = lead_id_text ;
        this.lead_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [线索]脏标记
     */
    @JsonIgnore
    public boolean getLead_id_textDirtyFlag(){
        return this.lead_id_textDirtyFlag ;
    }

    /**
     * 获取 [销售员]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [销售员]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [销售员]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }

    /**
     * 获取 [子类型]
     */
    @JsonProperty("subtype_id_text")
    public String getSubtype_id_text(){
        return this.subtype_id_text ;
    }

    /**
     * 设置 [子类型]
     */
    @JsonProperty("subtype_id_text")
    public void setSubtype_id_text(String  subtype_id_text){
        this.subtype_id_text = subtype_id_text ;
        this.subtype_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [子类型]脏标记
     */
    @JsonIgnore
    public boolean getSubtype_id_textDirtyFlag(){
        return this.subtype_id_textDirtyFlag ;
    }

    /**
     * 获取 [国家]
     */
    @JsonProperty("country_id_text")
    public String getCountry_id_text(){
        return this.country_id_text ;
    }

    /**
     * 设置 [国家]
     */
    @JsonProperty("country_id_text")
    public void setCountry_id_text(String  country_id_text){
        this.country_id_text = country_id_text ;
        this.country_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [国家]脏标记
     */
    @JsonIgnore
    public boolean getCountry_id_textDirtyFlag(){
        return this.country_id_textDirtyFlag ;
    }

    /**
     * 获取 [业务合作伙伴/客户]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [业务合作伙伴/客户]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [业务合作伙伴/客户]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [阶段]
     */
    @JsonProperty("stage_id_text")
    public String getStage_id_text(){
        return this.stage_id_text ;
    }

    /**
     * 设置 [阶段]
     */
    @JsonProperty("stage_id_text")
    public void setStage_id_text(String  stage_id_text){
        this.stage_id_text = stage_id_text ;
        this.stage_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [阶段]脏标记
     */
    @JsonIgnore
    public boolean getStage_id_textDirtyFlag(){
        return this.stage_id_textDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("author_id_text")
    public String getAuthor_id_text(){
        return this.author_id_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("author_id_text")
    public void setAuthor_id_text(String  author_id_text){
        this.author_id_text = author_id_text ;
        this.author_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getAuthor_id_textDirtyFlag(){
        return this.author_id_textDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("author_id")
    public Integer getAuthor_id(){
        return this.author_id ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("author_id")
    public void setAuthor_id(Integer  author_id){
        this.author_id = author_id ;
        this.author_idDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getAuthor_idDirtyFlag(){
        return this.author_idDirtyFlag ;
    }

    /**
     * 获取 [国家]
     */
    @JsonProperty("country_id")
    public Integer getCountry_id(){
        return this.country_id ;
    }

    /**
     * 设置 [国家]
     */
    @JsonProperty("country_id")
    public void setCountry_id(Integer  country_id){
        this.country_id = country_id ;
        this.country_idDirtyFlag = true ;
    }

    /**
     * 获取 [国家]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idDirtyFlag(){
        return this.country_idDirtyFlag ;
    }

    /**
     * 获取 [阶段]
     */
    @JsonProperty("stage_id")
    public Integer getStage_id(){
        return this.stage_id ;
    }

    /**
     * 设置 [阶段]
     */
    @JsonProperty("stage_id")
    public void setStage_id(Integer  stage_id){
        this.stage_id = stage_id ;
        this.stage_idDirtyFlag = true ;
    }

    /**
     * 获取 [阶段]脏标记
     */
    @JsonIgnore
    public boolean getStage_idDirtyFlag(){
        return this.stage_idDirtyFlag ;
    }

    /**
     * 获取 [子类型]
     */
    @JsonProperty("subtype_id")
    public Integer getSubtype_id(){
        return this.subtype_id ;
    }

    /**
     * 设置 [子类型]
     */
    @JsonProperty("subtype_id")
    public void setSubtype_id(Integer  subtype_id){
        this.subtype_id = subtype_id ;
        this.subtype_idDirtyFlag = true ;
    }

    /**
     * 获取 [子类型]脏标记
     */
    @JsonIgnore
    public boolean getSubtype_idDirtyFlag(){
        return this.subtype_idDirtyFlag ;
    }

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }

    /**
     * 获取 [线索]
     */
    @JsonProperty("lead_id")
    public Integer getLead_id(){
        return this.lead_id ;
    }

    /**
     * 设置 [线索]
     */
    @JsonProperty("lead_id")
    public void setLead_id(Integer  lead_id){
        this.lead_id = lead_id ;
        this.lead_idDirtyFlag = true ;
    }

    /**
     * 获取 [线索]脏标记
     */
    @JsonIgnore
    public boolean getLead_idDirtyFlag(){
        return this.lead_idDirtyFlag ;
    }

    /**
     * 获取 [活动类型]
     */
    @JsonProperty("mail_activity_type_id")
    public Integer getMail_activity_type_id(){
        return this.mail_activity_type_id ;
    }

    /**
     * 设置 [活动类型]
     */
    @JsonProperty("mail_activity_type_id")
    public void setMail_activity_type_id(Integer  mail_activity_type_id){
        this.mail_activity_type_id = mail_activity_type_id ;
        this.mail_activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [活动类型]脏标记
     */
    @JsonIgnore
    public boolean getMail_activity_type_idDirtyFlag(){
        return this.mail_activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [销售员]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [销售员]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [销售员]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }

    /**
     * 获取 [业务合作伙伴/客户]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [业务合作伙伴/客户]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [业务合作伙伴/客户]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }

    /**
     * 获取 [销售团队]
     */
    @JsonProperty("team_id")
    public Integer getTeam_id(){
        return this.team_id ;
    }

    /**
     * 设置 [销售团队]
     */
    @JsonProperty("team_id")
    public void setTeam_id(Integer  team_id){
        this.team_id = team_id ;
        this.team_idDirtyFlag = true ;
    }

    /**
     * 获取 [销售团队]脏标记
     */
    @JsonIgnore
    public boolean getTeam_idDirtyFlag(){
        return this.team_idDirtyFlag ;
    }



}
