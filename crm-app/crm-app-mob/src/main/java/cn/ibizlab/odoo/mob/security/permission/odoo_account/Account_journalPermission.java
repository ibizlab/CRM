package cn.ibizlab.odoo.mob.security.permission.odoo_account;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;

import cn.ibizlab.odoo.mob.odoo_account.service.Account_journalService;
import cn.ibizlab.odoo.mob.odoo_account.domain.Account_journal;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Component("account_journal_pms")
public class Account_journalPermission {

    @Autowired
    Account_journalService account_journalservice;

    public boolean check(String action){
        return true ;
    }

    public boolean check(Integer account_journal_id, String action){
        return true ;
    }


}
