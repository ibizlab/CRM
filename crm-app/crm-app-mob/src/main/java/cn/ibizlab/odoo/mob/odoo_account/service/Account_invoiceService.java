package cn.ibizlab.odoo.mob.odoo_account.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.mob.odoo_account.domain.Account_invoice;
import cn.ibizlab.odoo.mob.odoo_account.filter.*;
import cn.ibizlab.odoo.mob.odoo_account.feign.Account_invoiceFeignClient;
import cn.ibizlab.odoo.mob.MobApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Account_invoiceService {

    Account_invoiceFeignClient client;

    @Autowired
    public Account_invoiceService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Account_invoiceFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Account_invoiceFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public boolean remove( Integer account_invoice_id) {
        return client.remove( account_invoice_id);
    }

    public boolean checkKey(Account_invoice account_invoice) {
        return client.checkKey(account_invoice);
    }

	public Account_invoice create(Account_invoice account_invoice) {
        return client.create(account_invoice);
    }

    public Account_invoice update(Integer account_invoice_id, Account_invoice account_invoice) {
        return client.update(account_invoice_id, account_invoice);
    }

    public Account_invoice updateBatch(Integer account_invoice_id, Account_invoice account_invoice) {
        return client.updateBatch(account_invoice_id, account_invoice);
    }

    public Account_invoice get( Integer account_invoice_id) {
        return client.get( account_invoice_id);
    }

    public Account_invoice createBatch(Integer account_invoice_id, Account_invoice account_invoice) {
        return client.createBatch(account_invoice_id, account_invoice);
    }

    public Account_invoice getDraft(Integer account_invoice_id, Account_invoice account_invoice) {
        return client.getDraft(account_invoice_id, account_invoice);
    }

    public Account_invoice save(Integer account_invoice_id, Account_invoice account_invoice) {
        return client.save(account_invoice_id, account_invoice);
    }

    public Account_invoice removeBatch(Integer account_invoice_id, Account_invoice account_invoice) {
        return client.removeBatch(account_invoice_id, account_invoice);
    }

	public Page<Account_invoice> fetchDefault(Account_invoiceSearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }


    public boolean removeByRes_partner(Integer res_partner_id,  Integer account_invoice_id) {
        return client.removeByRes_partner(res_partner_id,  account_invoice_id);
    }

    public boolean checkKeyByRes_partner(Integer res_partner_id, Account_invoice account_invoice) {
        return client.checkKeyByRes_partner(res_partner_id, account_invoice);
    }

	public Account_invoice createByRes_partner(Integer res_partner_id, Account_invoice account_invoice) {
        return client.createByRes_partner(res_partner_id, account_invoice);
    }

    public Account_invoice updateByRes_partner(Integer res_partner_id, Integer account_invoice_id, Account_invoice account_invoice) {
        return client.updateByRes_partner(res_partner_id, account_invoice_id, account_invoice);
    }

    public Account_invoice updateBatchByRes_partner(Integer res_partner_id, Integer account_invoice_id, Account_invoice account_invoice) {
        return client.updateBatchByRes_partner(res_partner_id, account_invoice_id, account_invoice);
    }

    public Account_invoice getByRes_partner(Integer res_partner_id,  Integer account_invoice_id) {
        return client.getByRes_partner(res_partner_id,  account_invoice_id);
    }

    public Account_invoice createBatchByRes_partner(Integer res_partner_id, Integer account_invoice_id, Account_invoice account_invoice) {
        return client.createBatchByRes_partner(res_partner_id, account_invoice_id, account_invoice);
    }

    public Account_invoice getDraftByRes_partner(Integer res_partner_id, Integer account_invoice_id, Account_invoice account_invoice) {
        return client.getDraftByRes_partner(res_partner_id, account_invoice_id, account_invoice);
    }

    public Account_invoice saveByRes_partner(Integer res_partner_id, Integer account_invoice_id, Account_invoice account_invoice) {
        return client.saveByRes_partner(res_partner_id, account_invoice_id, account_invoice);
    }

    public Account_invoice removeBatchByRes_partner(Integer res_partner_id, Integer account_invoice_id, Account_invoice account_invoice) {
        return client.removeBatchByRes_partner(res_partner_id, account_invoice_id, account_invoice);
    }

	public Page<Account_invoice> fetchDefaultByRes_partner(Integer res_partner_id , Account_invoiceSearchContext searchContext) {
        return client.fetchDefaultByRes_partner(res_partner_id , searchContext);
    }

}
