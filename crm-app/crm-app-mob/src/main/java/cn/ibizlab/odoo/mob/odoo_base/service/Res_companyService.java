package cn.ibizlab.odoo.mob.odoo_base.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.mob.odoo_base.domain.Res_company;
import cn.ibizlab.odoo.mob.odoo_base.filter.*;
import cn.ibizlab.odoo.mob.odoo_base.feign.Res_companyFeignClient;
import cn.ibizlab.odoo.mob.MobApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Res_companyService {

    Res_companyFeignClient client;

    @Autowired
    public Res_companyService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Res_companyFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Res_companyFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public Res_company get( Integer res_company_id) {
        return client.get( res_company_id);
    }

    public Res_company getDraft(Integer res_company_id, Res_company res_company) {
        return client.getDraft(res_company_id, res_company);
    }

    public boolean remove( Integer res_company_id) {
        return client.remove( res_company_id);
    }

    public Res_company createBatch(Integer res_company_id, Res_company res_company) {
        return client.createBatch(res_company_id, res_company);
    }

    public Res_company updateBatch(Integer res_company_id, Res_company res_company) {
        return client.updateBatch(res_company_id, res_company);
    }

	public Res_company create(Res_company res_company) {
        return client.create(res_company);
    }

    public Res_company update(Integer res_company_id, Res_company res_company) {
        return client.update(res_company_id, res_company);
    }

    public Res_company removeBatch(Integer res_company_id, Res_company res_company) {
        return client.removeBatch(res_company_id, res_company);
    }

	public Page<Res_company> fetchDefault(Res_companySearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }

}
