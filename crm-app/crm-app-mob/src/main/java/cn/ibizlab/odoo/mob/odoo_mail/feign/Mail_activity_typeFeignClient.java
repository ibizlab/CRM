package cn.ibizlab.odoo.mob.odoo_mail.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.mob.odoo_mail.domain.Mail_activity_type;
import cn.ibizlab.odoo.mob.odoo_mail.filter.*;


public interface Mail_activity_typeFeignClient {



	@RequestMapping(method = RequestMethod.DELETE, value = "/web/mail_activity_types/{mail_activity_type_id}/removebatch")
    public Mail_activity_type removeBatch(@PathVariable("mail_activity_type_id") Integer mail_activity_type_id, @RequestBody Mail_activity_type mail_activity_type) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/mail_activity_types/{mail_activity_type_id}/createbatch")
    public Mail_activity_type createBatch(@PathVariable("mail_activity_type_id") Integer mail_activity_type_id, @RequestBody Mail_activity_type mail_activity_type) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/mail_activity_types/{mail_activity_type_id}")
    public Boolean remove(@PathVariable("mail_activity_type_id") Integer mail_activity_type_id) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/mail_activity_types/{mail_activity_type_id}")
    public Mail_activity_type get(@PathVariable("mail_activity_type_id") Integer mail_activity_type_id) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/mail_activity_types/{mail_activity_type_id}/getdraft")
    public Mail_activity_type getDraft(@PathVariable("mail_activity_type_id") Integer mail_activity_type_id, @RequestBody Mail_activity_type mail_activity_type) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/mail_activity_types/checkkey")
    public Boolean checkKey(@RequestBody Mail_activity_type mail_activity_type) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/mail_activity_types")
    public Mail_activity_type create(@RequestBody Mail_activity_type mail_activity_type) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/mail_activity_types/{mail_activity_type_id}/updatebatch")
    public Mail_activity_type updateBatch(@PathVariable("mail_activity_type_id") Integer mail_activity_type_id, @RequestBody Mail_activity_type mail_activity_type) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/mail_activity_types/{mail_activity_type_id}")
    public Mail_activity_type update(@PathVariable("mail_activity_type_id") Integer mail_activity_type_id, @RequestBody Mail_activity_type mail_activity_type) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/mail_activity_types/{mail_activity_type_id}/save")
    public Mail_activity_type save(@PathVariable("mail_activity_type_id") Integer mail_activity_type_id, @RequestBody Mail_activity_type mail_activity_type) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/mail_activity_types/fetchdefault")
	public Page<Mail_activity_type> fetchDefault(Mail_activity_typeSearchContext searchContext) ;
}
