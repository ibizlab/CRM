package cn.ibizlab.odoo.mob.r7rt_dyna.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.mob.r7rt_dyna.domain.DynaChart;
import cn.ibizlab.odoo.mob.r7rt_dyna.filter.*;
import cn.ibizlab.odoo.mob.r7rt_dyna.feign.DynaChartFeignClient;
import cn.ibizlab.odoo.mob.MobApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class DynaChartService {

    DynaChartFeignClient client;

    @Autowired
    public DynaChartService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(DynaChartFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(DynaChartFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public DynaChart get( String dynachart_id) {
        return client.get( dynachart_id);
    }

    public DynaChart getDraft(String dynachart_id, DynaChart dynachart) {
        return client.getDraft(dynachart_id, dynachart);
    }

    public boolean checkKey(DynaChart dynachart) {
        return client.checkKey(dynachart);
    }

    public DynaChart update(String dynachart_id, DynaChart dynachart) {
        return client.update(dynachart_id, dynachart);
    }

    public boolean remove( String dynachart_id) {
        return client.remove( dynachart_id);
    }

	public DynaChart create(DynaChart dynachart) {
        return client.create(dynachart);
    }

    public DynaChart save(String dynachart_id, DynaChart dynachart) {
        return client.save(dynachart_id, dynachart);
    }

	public Page<DynaChart> fetchDefault(DynaChartSearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }

}
