package cn.ibizlab.odoo.mob.odoo_product.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.mob.odoo_product.domain.Product_pricelist;
import cn.ibizlab.odoo.mob.odoo_product.filter.*;


public interface Product_pricelistFeignClient {



	@RequestMapping(method = RequestMethod.GET, value = "/web/product_pricelists/{product_pricelist_id}")
    public Product_pricelist get(@PathVariable("product_pricelist_id") Integer product_pricelist_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/product_pricelists/{product_pricelist_id}/save")
    public Product_pricelist save(@PathVariable("product_pricelist_id") Integer product_pricelist_id, @RequestBody Product_pricelist product_pricelist) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/product_pricelists/{product_pricelist_id}/removebatch")
    public Product_pricelist removeBatch(@PathVariable("product_pricelist_id") Integer product_pricelist_id, @RequestBody Product_pricelist product_pricelist) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/product_pricelists/{product_pricelist_id}/getdraft")
    public Product_pricelist getDraft(@PathVariable("product_pricelist_id") Integer product_pricelist_id, @RequestBody Product_pricelist product_pricelist) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/product_pricelists/{product_pricelist_id}")
    public Product_pricelist update(@PathVariable("product_pricelist_id") Integer product_pricelist_id, @RequestBody Product_pricelist product_pricelist) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/product_pricelists/{product_pricelist_id}")
    public Boolean remove(@PathVariable("product_pricelist_id") Integer product_pricelist_id) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/product_pricelists/{product_pricelist_id}/updatebatch")
    public Product_pricelist updateBatch(@PathVariable("product_pricelist_id") Integer product_pricelist_id, @RequestBody Product_pricelist product_pricelist) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/product_pricelists/checkkey")
    public Boolean checkKey(@RequestBody Product_pricelist product_pricelist) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/product_pricelists/{product_pricelist_id}/createbatch")
    public Product_pricelist createBatch(@PathVariable("product_pricelist_id") Integer product_pricelist_id, @RequestBody Product_pricelist product_pricelist) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/product_pricelists")
    public Product_pricelist create(@RequestBody Product_pricelist product_pricelist) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/product_pricelists/fetchdefault")
	public Page<Product_pricelist> fetchDefault(Product_pricelistSearchContext searchContext) ;
}
