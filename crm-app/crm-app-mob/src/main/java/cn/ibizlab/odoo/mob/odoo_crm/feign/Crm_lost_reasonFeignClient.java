package cn.ibizlab.odoo.mob.odoo_crm.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.mob.odoo_crm.domain.Crm_lost_reason;
import cn.ibizlab.odoo.mob.odoo_crm.filter.*;


public interface Crm_lost_reasonFeignClient {



	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_lost_reasons/{crm_lost_reason_id}")
    public Crm_lost_reason get(@PathVariable("crm_lost_reason_id") Integer crm_lost_reason_id) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_lost_reasons/{crm_lost_reason_id}")
    public Boolean remove(@PathVariable("crm_lost_reason_id") Integer crm_lost_reason_id) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_lost_reasons/{crm_lost_reason_id}")
    public Crm_lost_reason update(@PathVariable("crm_lost_reason_id") Integer crm_lost_reason_id, @RequestBody Crm_lost_reason crm_lost_reason) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_lost_reasons/{crm_lost_reason_id}/removebatch")
    public Crm_lost_reason removeBatch(@PathVariable("crm_lost_reason_id") Integer crm_lost_reason_id, @RequestBody Crm_lost_reason crm_lost_reason) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_lost_reasons/{crm_lost_reason_id}/updatebatch")
    public Crm_lost_reason updateBatch(@PathVariable("crm_lost_reason_id") Integer crm_lost_reason_id, @RequestBody Crm_lost_reason crm_lost_reason) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_lost_reasons")
    public Crm_lost_reason create(@RequestBody Crm_lost_reason crm_lost_reason) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_lost_reasons/{crm_lost_reason_id}/getdraft")
    public Crm_lost_reason getDraft(@PathVariable("crm_lost_reason_id") Integer crm_lost_reason_id, @RequestBody Crm_lost_reason crm_lost_reason) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_lost_reasons/{crm_lost_reason_id}/createbatch")
    public Crm_lost_reason createBatch(@PathVariable("crm_lost_reason_id") Integer crm_lost_reason_id, @RequestBody Crm_lost_reason crm_lost_reason) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/crm_lost_reasons/fetchdefault")
	public Page<Crm_lost_reason> fetchDefault(Crm_lost_reasonSearchContext searchContext) ;
}
