package cn.ibizlab.odoo.web.odoo_product.filter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Data
@IBIZLog
public class Product_pricelistSearchContext extends SearchContext implements Serializable {

	public String n_name_like;//[价格表名称]
	public String n_discount_policy_eq;//[折扣政策]
	public String n_currency_id_text_eq;//[币种]
	public String n_currency_id_text_like;//[币种]
	public String n_write_uid_text_eq;//[最后更新人]
	public String n_write_uid_text_like;//[最后更新人]
	public String n_create_uid_text_eq;//[创建人]
	public String n_create_uid_text_like;//[创建人]
	public String n_company_id_text_eq;//[公司]
	public String n_company_id_text_like;//[公司]
	public Integer n_create_uid_eq;//[创建人]
	public Integer n_write_uid_eq;//[最后更新人]
	public Integer n_currency_id_eq;//[币种]
	public Integer n_company_id_eq;//[公司]

}