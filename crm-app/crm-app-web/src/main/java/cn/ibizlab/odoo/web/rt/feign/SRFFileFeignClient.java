package cn.ibizlab.odoo.web.rt.feign;

import cn.ibizlab.odoo.web.rt.domain.SRFFILE;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public interface SRFFileFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/web/srffiles/{srffile_id}/save")
    boolean save(@PathVariable("srffile_id") String fileId, @RequestBody SRFFILE srffile) ;

    @RequestMapping(method = RequestMethod.GET, value = "/web/srffiles/{srffile_id}")
    SRFFILE get(@PathVariable("srffile_id") String fileId) ;

}
