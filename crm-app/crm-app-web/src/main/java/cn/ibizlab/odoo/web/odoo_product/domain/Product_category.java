package cn.ibizlab.odoo.web.odoo_product.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 应用实体[产品种类]
 */
public class Product_category implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;
    
    /**
     * 库存计价科目
     */
    private Integer property_stock_valuation_account_id;

    @JsonIgnore
    private boolean property_stock_valuation_account_idDirtyFlag;
    
    /**
     * 下级类别
     */
    private String child_id;

    @JsonIgnore
    private boolean child_idDirtyFlag;
    
    /**
     * # 产品
     */
    private Integer product_count;

    @JsonIgnore
    private boolean product_countDirtyFlag;
    
    /**
     * 路线
     */
    private String route_ids;

    @JsonIgnore
    private boolean route_idsDirtyFlag;
    
    /**
     * 名称
     */
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;
    
    /**
     * 显示名称
     */
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;
    
    /**
     * 库存计价
     */
    private String property_valuation;

    @JsonIgnore
    private boolean property_valuationDirtyFlag;
    
    /**
     * 父级路径
     */
    private String parent_path;

    @JsonIgnore
    private boolean parent_pathDirtyFlag;
    
    /**
     * ID
     */
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;
    
    /**
     * 完整名称
     */
    private String complete_name;

    @JsonIgnore
    private boolean complete_nameDirtyFlag;
    
    /**
     * 价格差异科目
     */
    private Integer property_account_creditor_price_difference_categ;

    @JsonIgnore
    private boolean property_account_creditor_price_difference_categDirtyFlag;
    
    /**
     * 库存出货科目
     */
    private Integer property_stock_account_output_categ_id;

    @JsonIgnore
    private boolean property_stock_account_output_categ_idDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;
    
    /**
     * 路线合计
     */
    private String total_route_ids;

    @JsonIgnore
    private boolean total_route_idsDirtyFlag;
    
    /**
     * 成本方法
     */
    private String property_cost_method;

    @JsonIgnore
    private boolean property_cost_methodDirtyFlag;
    
    /**
     * 收入科目
     */
    private Integer property_account_income_categ_id;

    @JsonIgnore
    private boolean property_account_income_categ_idDirtyFlag;
    
    /**
     * 费用科目
     */
    private Integer property_account_expense_categ_id;

    @JsonIgnore
    private boolean property_account_expense_categ_idDirtyFlag;
    
    /**
     * 库存日记账
     */
    private Integer property_stock_journal;

    @JsonIgnore
    private boolean property_stock_journalDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;
    
    /**
     * 库存进货科目
     */
    private Integer property_stock_account_input_categ_id;

    @JsonIgnore
    private boolean property_stock_account_input_categ_idDirtyFlag;
    
    /**
     * 最后更新人
     */
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;
    
    /**
     * 上级类别
     */
    private String parent_id_text;

    @JsonIgnore
    private boolean parent_id_textDirtyFlag;
    
    /**
     * 创建人
     */
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;
    
    /**
     * 强制下架策略
     */
    private String removal_strategy_id_text;

    @JsonIgnore
    private boolean removal_strategy_id_textDirtyFlag;
    
    /**
     * 上级类别
     */
    private Integer parent_id;

    @JsonIgnore
    private boolean parent_idDirtyFlag;
    
    /**
     * 创建人
     */
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;
    
    /**
     * 强制下架策略
     */
    private Integer removal_strategy_id;

    @JsonIgnore
    private boolean removal_strategy_idDirtyFlag;
    
    /**
     * 最后更新人
     */
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;
    

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }

    /**
     * 获取 [库存计价科目]
     */
    @JsonProperty("property_stock_valuation_account_id")
    public Integer getProperty_stock_valuation_account_id(){
        return this.property_stock_valuation_account_id ;
    }

    /**
     * 设置 [库存计价科目]
     */
    @JsonProperty("property_stock_valuation_account_id")
    public void setProperty_stock_valuation_account_id(Integer  property_stock_valuation_account_id){
        this.property_stock_valuation_account_id = property_stock_valuation_account_id ;
        this.property_stock_valuation_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [库存计价科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_valuation_account_idDirtyFlag(){
        return this.property_stock_valuation_account_idDirtyFlag ;
    }

    /**
     * 获取 [下级类别]
     */
    @JsonProperty("child_id")
    public String getChild_id(){
        return this.child_id ;
    }

    /**
     * 设置 [下级类别]
     */
    @JsonProperty("child_id")
    public void setChild_id(String  child_id){
        this.child_id = child_id ;
        this.child_idDirtyFlag = true ;
    }

    /**
     * 获取 [下级类别]脏标记
     */
    @JsonIgnore
    public boolean getChild_idDirtyFlag(){
        return this.child_idDirtyFlag ;
    }

    /**
     * 获取 [# 产品]
     */
    @JsonProperty("product_count")
    public Integer getProduct_count(){
        return this.product_count ;
    }

    /**
     * 设置 [# 产品]
     */
    @JsonProperty("product_count")
    public void setProduct_count(Integer  product_count){
        this.product_count = product_count ;
        this.product_countDirtyFlag = true ;
    }

    /**
     * 获取 [# 产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_countDirtyFlag(){
        return this.product_countDirtyFlag ;
    }

    /**
     * 获取 [路线]
     */
    @JsonProperty("route_ids")
    public String getRoute_ids(){
        return this.route_ids ;
    }

    /**
     * 设置 [路线]
     */
    @JsonProperty("route_ids")
    public void setRoute_ids(String  route_ids){
        this.route_ids = route_ids ;
        this.route_idsDirtyFlag = true ;
    }

    /**
     * 获取 [路线]脏标记
     */
    @JsonIgnore
    public boolean getRoute_idsDirtyFlag(){
        return this.route_idsDirtyFlag ;
    }

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }

    /**
     * 获取 [库存计价]
     */
    @JsonProperty("property_valuation")
    public String getProperty_valuation(){
        return this.property_valuation ;
    }

    /**
     * 设置 [库存计价]
     */
    @JsonProperty("property_valuation")
    public void setProperty_valuation(String  property_valuation){
        this.property_valuation = property_valuation ;
        this.property_valuationDirtyFlag = true ;
    }

    /**
     * 获取 [库存计价]脏标记
     */
    @JsonIgnore
    public boolean getProperty_valuationDirtyFlag(){
        return this.property_valuationDirtyFlag ;
    }

    /**
     * 获取 [父级路径]
     */
    @JsonProperty("parent_path")
    public String getParent_path(){
        return this.parent_path ;
    }

    /**
     * 设置 [父级路径]
     */
    @JsonProperty("parent_path")
    public void setParent_path(String  parent_path){
        this.parent_path = parent_path ;
        this.parent_pathDirtyFlag = true ;
    }

    /**
     * 获取 [父级路径]脏标记
     */
    @JsonIgnore
    public boolean getParent_pathDirtyFlag(){
        return this.parent_pathDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }

    /**
     * 获取 [完整名称]
     */
    @JsonProperty("complete_name")
    public String getComplete_name(){
        return this.complete_name ;
    }

    /**
     * 设置 [完整名称]
     */
    @JsonProperty("complete_name")
    public void setComplete_name(String  complete_name){
        this.complete_name = complete_name ;
        this.complete_nameDirtyFlag = true ;
    }

    /**
     * 获取 [完整名称]脏标记
     */
    @JsonIgnore
    public boolean getComplete_nameDirtyFlag(){
        return this.complete_nameDirtyFlag ;
    }

    /**
     * 获取 [价格差异科目]
     */
    @JsonProperty("property_account_creditor_price_difference_categ")
    public Integer getProperty_account_creditor_price_difference_categ(){
        return this.property_account_creditor_price_difference_categ ;
    }

    /**
     * 设置 [价格差异科目]
     */
    @JsonProperty("property_account_creditor_price_difference_categ")
    public void setProperty_account_creditor_price_difference_categ(Integer  property_account_creditor_price_difference_categ){
        this.property_account_creditor_price_difference_categ = property_account_creditor_price_difference_categ ;
        this.property_account_creditor_price_difference_categDirtyFlag = true ;
    }

    /**
     * 获取 [价格差异科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_creditor_price_difference_categDirtyFlag(){
        return this.property_account_creditor_price_difference_categDirtyFlag ;
    }

    /**
     * 获取 [库存出货科目]
     */
    @JsonProperty("property_stock_account_output_categ_id")
    public Integer getProperty_stock_account_output_categ_id(){
        return this.property_stock_account_output_categ_id ;
    }

    /**
     * 设置 [库存出货科目]
     */
    @JsonProperty("property_stock_account_output_categ_id")
    public void setProperty_stock_account_output_categ_id(Integer  property_stock_account_output_categ_id){
        this.property_stock_account_output_categ_id = property_stock_account_output_categ_id ;
        this.property_stock_account_output_categ_idDirtyFlag = true ;
    }

    /**
     * 获取 [库存出货科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_account_output_categ_idDirtyFlag(){
        return this.property_stock_account_output_categ_idDirtyFlag ;
    }

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }

    /**
     * 获取 [路线合计]
     */
    @JsonProperty("total_route_ids")
    public String getTotal_route_ids(){
        return this.total_route_ids ;
    }

    /**
     * 设置 [路线合计]
     */
    @JsonProperty("total_route_ids")
    public void setTotal_route_ids(String  total_route_ids){
        this.total_route_ids = total_route_ids ;
        this.total_route_idsDirtyFlag = true ;
    }

    /**
     * 获取 [路线合计]脏标记
     */
    @JsonIgnore
    public boolean getTotal_route_idsDirtyFlag(){
        return this.total_route_idsDirtyFlag ;
    }

    /**
     * 获取 [成本方法]
     */
    @JsonProperty("property_cost_method")
    public String getProperty_cost_method(){
        return this.property_cost_method ;
    }

    /**
     * 设置 [成本方法]
     */
    @JsonProperty("property_cost_method")
    public void setProperty_cost_method(String  property_cost_method){
        this.property_cost_method = property_cost_method ;
        this.property_cost_methodDirtyFlag = true ;
    }

    /**
     * 获取 [成本方法]脏标记
     */
    @JsonIgnore
    public boolean getProperty_cost_methodDirtyFlag(){
        return this.property_cost_methodDirtyFlag ;
    }

    /**
     * 获取 [收入科目]
     */
    @JsonProperty("property_account_income_categ_id")
    public Integer getProperty_account_income_categ_id(){
        return this.property_account_income_categ_id ;
    }

    /**
     * 设置 [收入科目]
     */
    @JsonProperty("property_account_income_categ_id")
    public void setProperty_account_income_categ_id(Integer  property_account_income_categ_id){
        this.property_account_income_categ_id = property_account_income_categ_id ;
        this.property_account_income_categ_idDirtyFlag = true ;
    }

    /**
     * 获取 [收入科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_income_categ_idDirtyFlag(){
        return this.property_account_income_categ_idDirtyFlag ;
    }

    /**
     * 获取 [费用科目]
     */
    @JsonProperty("property_account_expense_categ_id")
    public Integer getProperty_account_expense_categ_id(){
        return this.property_account_expense_categ_id ;
    }

    /**
     * 设置 [费用科目]
     */
    @JsonProperty("property_account_expense_categ_id")
    public void setProperty_account_expense_categ_id(Integer  property_account_expense_categ_id){
        this.property_account_expense_categ_id = property_account_expense_categ_id ;
        this.property_account_expense_categ_idDirtyFlag = true ;
    }

    /**
     * 获取 [费用科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_expense_categ_idDirtyFlag(){
        return this.property_account_expense_categ_idDirtyFlag ;
    }

    /**
     * 获取 [库存日记账]
     */
    @JsonProperty("property_stock_journal")
    public Integer getProperty_stock_journal(){
        return this.property_stock_journal ;
    }

    /**
     * 设置 [库存日记账]
     */
    @JsonProperty("property_stock_journal")
    public void setProperty_stock_journal(Integer  property_stock_journal){
        this.property_stock_journal = property_stock_journal ;
        this.property_stock_journalDirtyFlag = true ;
    }

    /**
     * 获取 [库存日记账]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_journalDirtyFlag(){
        return this.property_stock_journalDirtyFlag ;
    }

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }

    /**
     * 获取 [库存进货科目]
     */
    @JsonProperty("property_stock_account_input_categ_id")
    public Integer getProperty_stock_account_input_categ_id(){
        return this.property_stock_account_input_categ_id ;
    }

    /**
     * 设置 [库存进货科目]
     */
    @JsonProperty("property_stock_account_input_categ_id")
    public void setProperty_stock_account_input_categ_id(Integer  property_stock_account_input_categ_id){
        this.property_stock_account_input_categ_id = property_stock_account_input_categ_id ;
        this.property_stock_account_input_categ_idDirtyFlag = true ;
    }

    /**
     * 获取 [库存进货科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_account_input_categ_idDirtyFlag(){
        return this.property_stock_account_input_categ_idDirtyFlag ;
    }

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [上级类别]
     */
    @JsonProperty("parent_id_text")
    public String getParent_id_text(){
        return this.parent_id_text ;
    }

    /**
     * 设置 [上级类别]
     */
    @JsonProperty("parent_id_text")
    public void setParent_id_text(String  parent_id_text){
        this.parent_id_text = parent_id_text ;
        this.parent_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [上级类别]脏标记
     */
    @JsonIgnore
    public boolean getParent_id_textDirtyFlag(){
        return this.parent_id_textDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [强制下架策略]
     */
    @JsonProperty("removal_strategy_id_text")
    public String getRemoval_strategy_id_text(){
        return this.removal_strategy_id_text ;
    }

    /**
     * 设置 [强制下架策略]
     */
    @JsonProperty("removal_strategy_id_text")
    public void setRemoval_strategy_id_text(String  removal_strategy_id_text){
        this.removal_strategy_id_text = removal_strategy_id_text ;
        this.removal_strategy_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [强制下架策略]脏标记
     */
    @JsonIgnore
    public boolean getRemoval_strategy_id_textDirtyFlag(){
        return this.removal_strategy_id_textDirtyFlag ;
    }

    /**
     * 获取 [上级类别]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return this.parent_id ;
    }

    /**
     * 设置 [上级类别]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

    /**
     * 获取 [上级类别]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return this.parent_idDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }

    /**
     * 获取 [强制下架策略]
     */
    @JsonProperty("removal_strategy_id")
    public Integer getRemoval_strategy_id(){
        return this.removal_strategy_id ;
    }

    /**
     * 设置 [强制下架策略]
     */
    @JsonProperty("removal_strategy_id")
    public void setRemoval_strategy_id(Integer  removal_strategy_id){
        this.removal_strategy_id = removal_strategy_id ;
        this.removal_strategy_idDirtyFlag = true ;
    }

    /**
     * 获取 [强制下架策略]脏标记
     */
    @JsonIgnore
    public boolean getRemoval_strategy_idDirtyFlag(){
        return this.removal_strategy_idDirtyFlag ;
    }

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }



}
