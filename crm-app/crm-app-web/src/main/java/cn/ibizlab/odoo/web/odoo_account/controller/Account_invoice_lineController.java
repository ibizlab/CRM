package cn.ibizlab.odoo.web.odoo_account.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.web.odoo_account.service.Account_invoice_lineService;
import cn.ibizlab.odoo.web.odoo_account.domain.Account_invoice_line;
import cn.ibizlab.odoo.web.odoo_account.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Account_invoice_lineController {
	@Autowired
    Account_invoice_lineService account_invoice_lineservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_lines/{account_invoice_line_id}")
    @PreAuthorize("@account_invoice_line_pms.check(#account_invoice_line_id,'UPDATE')")
    public ResponseEntity<Account_invoice_line> update(@PathVariable("account_invoice_line_id") Integer account_invoice_line_id, @RequestBody Account_invoice_line account_invoice_line) {
        Account_invoice_line account_invoice_line2 = account_invoice_lineservice.update(account_invoice_line_id, account_invoice_line);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice_line2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/account_invoice_lines/{account_invoice_line_id}")
    @PreAuthorize("@account_invoice_line_pms.check(#account_invoice_line_id,'UPDATE')")
    public ResponseEntity<Account_invoice_line> api_update(@PathVariable("account_invoice_line_id") Integer account_invoice_line_id, @RequestBody Account_invoice_line account_invoice_line) {
        Account_invoice_line account_invoice_line2 = account_invoice_lineservice.update(account_invoice_line_id, account_invoice_line);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice_line2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_lines/checkkey")
    @PreAuthorize("@account_invoice_line_pms.check('')")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_invoice_line account_invoice_line) {
        boolean b = account_invoice_lineservice.checkKey(account_invoice_line);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_lines/{account_invoice_line_id}/updatebatch")
    @PreAuthorize("@account_invoice_line_pms.check(#account_invoice_line_id,'UPDATE')")
    public ResponseEntity<Account_invoice_line> updateBatch(@PathVariable("account_invoice_line_id") Integer account_invoice_line_id, @RequestBody Account_invoice_line account_invoice_line) {
        Account_invoice_line account_invoice_line2 = account_invoice_lineservice.updateBatch(account_invoice_line_id, account_invoice_line);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice_line2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_lines/{account_invoice_line_id}/createbatch")
    @PreAuthorize("@account_invoice_line_pms.check(#account_invoice_line_id,'CREATE')")
    public ResponseEntity<Account_invoice_line> createBatch(@PathVariable("account_invoice_line_id") Integer account_invoice_line_id, @RequestBody Account_invoice_line account_invoice_line) {
        Account_invoice_line account_invoice_line2 = account_invoice_lineservice.createBatch(account_invoice_line_id, account_invoice_line);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice_line2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_lines/{account_invoice_line_id}/removebatch")
    @PreAuthorize("@account_invoice_line_pms.check(#account_invoice_line_id,'DELETE')")
    public ResponseEntity<Account_invoice_line> removeBatch(@PathVariable("account_invoice_line_id") Integer account_invoice_line_id, @RequestBody Account_invoice_line account_invoice_line) {
        Account_invoice_line account_invoice_line2 = account_invoice_lineservice.removeBatch(account_invoice_line_id, account_invoice_line);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice_line2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoice_lines/{account_invoice_line_id}")
    @PreAuthorize("@account_invoice_line_pms.check(#account_invoice_line_id,'READ')")
    public ResponseEntity<Account_invoice_line> get(@PathVariable("account_invoice_line_id") Integer account_invoice_line_id) {
        Account_invoice_line account_invoice_line = account_invoice_lineservice.get( account_invoice_line_id);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice_line);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoice_lines/getdraft")
    @PreAuthorize("@account_invoice_line_pms.check('CREATE')")
    public ResponseEntity<Account_invoice_line> getDraft() {
        //Account_invoice_line account_invoice_line = account_invoice_lineservice.getDraft( account_invoice_line_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Account_invoice_line());
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_lines/{account_invoice_line_id}/save")
    @PreAuthorize("@account_invoice_line_pms.check(#account_invoice_line_id,'')")
    public ResponseEntity<Account_invoice_line> save(@PathVariable("account_invoice_line_id") Integer account_invoice_line_id, @RequestBody Account_invoice_line account_invoice_line) {
        Account_invoice_line account_invoice_line2 = account_invoice_lineservice.save(account_invoice_line_id, account_invoice_line);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice_line2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_lines/{account_invoice_line_id}")
    @PreAuthorize("@account_invoice_line_pms.check(#account_invoice_line_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("account_invoice_line_id") Integer account_invoice_line_id) {
        boolean b = account_invoice_lineservice.remove( account_invoice_line_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_lines")
    @PreAuthorize("@account_invoice_line_pms.check('CREATE')")
    public ResponseEntity<Account_invoice_line> create(@RequestBody Account_invoice_line account_invoice_line) {
        Account_invoice_line account_invoice_line2 = account_invoice_lineservice.create(account_invoice_line);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice_line2);
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/account_invoice_lines/fetchdefault")
    @PreAuthorize("@account_invoice_line_pms.check('READ')")
	public ResponseEntity<List<Account_invoice_line>> fetchDefault(Account_invoice_lineSearchContext searchContext,Pageable pageable) {
        
        Page<Account_invoice_line> page = account_invoice_lineservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}
