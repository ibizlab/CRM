package cn.ibizlab.odoo.web.security.permission.odoo_mail;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;

import cn.ibizlab.odoo.web.odoo_mail.service.Mail_messageService;
import cn.ibizlab.odoo.web.odoo_mail.domain.Mail_message;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Component("mail_message_pms")
public class Mail_messagePermission {

    @Autowired
    Mail_messageService mail_messageservice;

    public boolean check(String action){
        return true ;
    }

    public boolean check(Integer mail_message_id, String action){
        return true ;
    }


}
