package cn.ibizlab.odoo.web.odoo_base.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 应用实体[用户]
 */
public class Res_users implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 管理员
     */
    private String is_moderator;

    @JsonIgnore
    private boolean is_moderatorDirtyFlag;
    
    /**
     * 资源
     */
    private String resource_ids;

    @JsonIgnore
    private boolean resource_idsDirtyFlag;
    
    /**
     * 创建日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;
    
    /**
     * 标签
     */
    private String category_id;

    @JsonIgnore
    private boolean category_idDirtyFlag;
    
    /**
     * 默认工作时间
     */
    private Integer resource_calendar_id;

    @JsonIgnore
    private boolean resource_calendar_idDirtyFlag;
    
    /**
     * 用户登录记录
     */
    private String log_ids;

    @JsonIgnore
    private boolean log_idsDirtyFlag;
    
    /**
     * 消息
     */
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;
    
    /**
     * 安全PIN
     */
    private String pos_security_pin;

    @JsonIgnore
    private boolean pos_security_pinDirtyFlag;
    
    /**
     * 徽章
     */
    private String badge_ids;

    @JsonIgnore
    private boolean badge_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;
    
    /**
     * ID
     */
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;
    
    /**
     * 公司
     */
    private String company_ids;

    @JsonIgnore
    private boolean company_idsDirtyFlag;
    
    /**
     * 联系人
     */
    private String child_ids;

    @JsonIgnore
    private boolean child_idsDirtyFlag;
    
    /**
     * 时区偏移
     */
    private String tz_offset;

    @JsonIgnore
    private boolean tz_offsetDirtyFlag;
    
    /**
     * 活动达成
     */
    private Integer target_sales_done;

    @JsonIgnore
    private boolean target_sales_doneDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;
    
    /**
     * 通知管理
     */
    private String notification_type;

    @JsonIgnore
    private boolean notification_typeDirtyFlag;
    
    /**
     * 有效
     */
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;
    
    /**
     * IM的状态
     */
    private String im_status;

    @JsonIgnore
    private boolean im_statusDirtyFlag;
    
    /**
     * 贡献值
     */
    private Integer karma;

    @JsonIgnore
    private boolean karmaDirtyFlag;
    
    /**
     * 登记网站
     */
    private Integer website_id;

    @JsonIgnore
    private boolean website_idDirtyFlag;
    
    /**
     * 活动
     */
    private String activity_ids;

    @JsonIgnore
    private boolean activity_idsDirtyFlag;
    
    /**
     * 金质徽章个数
     */
    private Integer gold_badge;

    @JsonIgnore
    private boolean gold_badgeDirtyFlag;
    
    /**
     * 相关的员工
     */
    private String employee_ids;

    @JsonIgnore
    private boolean employee_idsDirtyFlag;
    
    /**
     * 状态
     */
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;
    
    /**
     * 管理频道
     */
    private String moderation_channel_ids;

    @JsonIgnore
    private boolean moderation_channel_idsDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;
    
    /**
     * 银质徽章个数
     */
    private Integer silver_badge;

    @JsonIgnore
    private boolean silver_badgeDirtyFlag;
    
    /**
     * 付款令牌
     */
    private String payment_token_ids;

    @JsonIgnore
    private boolean payment_token_idsDirtyFlag;
    
    /**
     * 显示名称
     */
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;
    
    /**
     * 公司数量
     */
    private Integer companies_count;

    @JsonIgnore
    private boolean companies_countDirtyFlag;
    
    /**
     * 销售订单目标发票
     */
    private Integer target_sales_invoiced;

    @JsonIgnore
    private boolean target_sales_invoicedDirtyFlag;
    
    /**
     * 关注者
     */
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;
    
    /**
     * 最后连接
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp login_date;

    @JsonIgnore
    private boolean login_dateDirtyFlag;
    
    /**
     * 渠道
     */
    private String channel_ids;

    @JsonIgnore
    private boolean channel_idsDirtyFlag;
    
    /**
     * 群组
     */
    private String groups_id;

    @JsonIgnore
    private boolean groups_idDirtyFlag;
    
    /**
     * 共享用户
     */
    private String share;

    @JsonIgnore
    private boolean shareDirtyFlag;
    
    /**
     * 银行
     */
    private String bank_ids;

    @JsonIgnore
    private boolean bank_idsDirtyFlag;
    
    /**
     * 销售订单
     */
    private String sale_order_ids;

    @JsonIgnore
    private boolean sale_order_idsDirtyFlag;
    
    /**
     * 设置密码
     */
    private String new_password;

    @JsonIgnore
    private boolean new_passwordDirtyFlag;
    
    /**
     * OdooBot 状态
     */
    private String odoobot_state;

    @JsonIgnore
    private boolean odoobot_stateDirtyFlag;
    
    /**
     * 公司是指业务伙伴
     */
    private String ref_company_ids;

    @JsonIgnore
    private boolean ref_company_idsDirtyFlag;
    
    /**
     * 密码
     */
    private String password;

    @JsonIgnore
    private boolean passwordDirtyFlag;
    
    /**
     * 青铜徽章数目
     */
    private Integer bronze_badge;

    @JsonIgnore
    private boolean bronze_badgeDirtyFlag;
    
    /**
     * 会议
     */
    private String meeting_ids;

    @JsonIgnore
    private boolean meeting_idsDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;
    
    /**
     * 待发布的帖子
     */
    private Integer forum_waiting_posts_count;

    @JsonIgnore
    private boolean forum_waiting_posts_countDirtyFlag;
    
    /**
     * 目标
     */
    private String goal_ids;

    @JsonIgnore
    private boolean goal_idsDirtyFlag;
    
    /**
     * 签单是商机的最终目标
     */
    private Integer target_sales_won;

    @JsonIgnore
    private boolean target_sales_wonDirtyFlag;
    
    /**
     * 网站消息
     */
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;
    
    /**
     * 主页动作
     */
    private Integer action_id;

    @JsonIgnore
    private boolean action_idDirtyFlag;
    
    /**
     * 登录
     */
    private String login;

    @JsonIgnore
    private boolean loginDirtyFlag;
    
    /**
     * 客户合同
     */
    private String contract_ids;

    @JsonIgnore
    private boolean contract_idsDirtyFlag;
    
    /**
     * 审核数
     */
    private Integer moderation_counter;

    @JsonIgnore
    private boolean moderation_counterDirtyFlag;
    
    /**
     * 签名
     */
    private String signature;

    @JsonIgnore
    private boolean signatureDirtyFlag;
    
    /**
     * 用户
     */
    private String user_ids;

    @JsonIgnore
    private boolean user_idsDirtyFlag;
    
    /**
     * 商机
     */
    private String opportunity_ids;

    @JsonIgnore
    private boolean opportunity_idsDirtyFlag;
    
    /**
     * 任务
     */
    private String task_ids;

    @JsonIgnore
    private boolean task_idsDirtyFlag;
    
    /**
     * 发票
     */
    private String invoice_ids;

    @JsonIgnore
    private boolean invoice_idsDirtyFlag;
    
    /**
     * 内部参考
     */
    private String ref;

    @JsonIgnore
    private boolean refDirtyFlag;
    
    /**
     * 错误个数
     */
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;
    
    /**
     * 最近的在线销售订单
     */
    private Integer last_website_so_id;

    @JsonIgnore
    private boolean last_website_so_idDirtyFlag;
    
    /**
     * 日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;
    
    /**
     * 关注者
     */
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;
    
    /**
     * 客户位置
     */
    private Integer property_stock_customer;

    @JsonIgnore
    private boolean property_stock_customerDirtyFlag;
    
    /**
     * 最近的发票和付款匹配时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp last_time_entries_checked;

    @JsonIgnore
    private boolean last_time_entries_checkedDirtyFlag;
    
    /**
     * 语言
     */
    private String lang;

    @JsonIgnore
    private boolean langDirtyFlag;
    
    /**
     * 销售警告
     */
    private String sale_warn;

    @JsonIgnore
    private boolean sale_warnDirtyFlag;
    
    /**
     * #会议
     */
    private Integer meeting_count;

    @JsonIgnore
    private boolean meeting_countDirtyFlag;
    
    /**
     * 街道
     */
    private String street;

    @JsonIgnore
    private boolean streetDirtyFlag;
    
    /**
     * 发票
     */
    private String invoice_warn;

    @JsonIgnore
    private boolean invoice_warnDirtyFlag;
    
    /**
     * 注册令牌 Token
     */
    private String signup_token;

    @JsonIgnore
    private boolean signup_tokenDirtyFlag;
    
    /**
     * # 任务
     */
    private Integer task_count;

    @JsonIgnore
    private boolean task_countDirtyFlag;
    
    /**
     * 注册令牌（ Token  ）是有效的
     */
    private String signup_valid;

    @JsonIgnore
    private boolean signup_validDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;
    
    /**
     * 注册令牌（Token）类型
     */
    private String signup_type;

    @JsonIgnore
    private boolean signup_typeDirtyFlag;
    
    /**
     * 应收账款
     */
    private Integer property_account_receivable_id;

    @JsonIgnore
    private boolean property_account_receivable_idDirtyFlag;
    
    /**
     * 网站opengraph图像
     */
    private String website_meta_og_img;

    @JsonIgnore
    private boolean website_meta_og_imgDirtyFlag;
    
    /**
     * 活动
     */
    private Integer event_count;

    @JsonIgnore
    private boolean event_countDirtyFlag;
    
    /**
     * 日记账项目
     */
    private Integer journal_item_count;

    @JsonIgnore
    private boolean journal_item_countDirtyFlag;
    
    /**
     * 上级名称
     */
    private String parent_name;

    @JsonIgnore
    private boolean parent_nameDirtyFlag;
    
    /**
     * 用户的销售团队
     */
    private String sale_team_id_text;

    @JsonIgnore
    private boolean sale_team_id_textDirtyFlag;
    
    /**
     * 应收总计
     */
    private Double credit;

    @JsonIgnore
    private boolean creditDirtyFlag;
    
    /**
     * 商机
     */
    private Integer opportunity_count;

    @JsonIgnore
    private boolean opportunity_countDirtyFlag;
    
    /**
     * 注册网址
     */
    private String signup_url;

    @JsonIgnore
    private boolean signup_urlDirtyFlag;
    
    /**
     * 应付账款
     */
    private Integer property_account_payable_id;

    @JsonIgnore
    private boolean property_account_payable_idDirtyFlag;
    
    /**
     * 省/ 州
     */
    private Integer state_id;

    @JsonIgnore
    private boolean state_idDirtyFlag;
    
    /**
     * 消息递送错误
     */
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;
    
    /**
     * 销售订单消息
     */
    private String sale_warn_msg;

    @JsonIgnore
    private boolean sale_warn_msgDirtyFlag;
    
    /**
     * 在当前网站显示
     */
    private String website_published;

    @JsonIgnore
    private boolean website_publishedDirtyFlag;
    
    /**
     * 已开票总计
     */
    private Double total_invoiced;

    @JsonIgnore
    private boolean total_invoicedDirtyFlag;
    
    /**
     * 应付总计
     */
    private Double debit;

    @JsonIgnore
    private boolean debitDirtyFlag;
    
    /**
     * 销售点订单计数
     */
    private Integer pos_order_count;

    @JsonIgnore
    private boolean pos_order_countDirtyFlag;
    
    /**
     * 最后更新者
     */
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;
    
    /**
     * 称谓
     */
    private Integer title;

    @JsonIgnore
    private boolean titleDirtyFlag;
    
    /**
     * 未读消息
     */
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;
    
    /**
     * ＃供应商账单
     */
    private Integer supplier_invoice_count;

    @JsonIgnore
    private boolean supplier_invoice_countDirtyFlag;
    
    /**
     * 城市
     */
    private String city;

    @JsonIgnore
    private boolean cityDirtyFlag;
    
    /**
     * 库存拣货
     */
    private String picking_warn;

    @JsonIgnore
    private boolean picking_warnDirtyFlag;
    
    /**
     * 附件
     */
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 责任用户
     */
    private Integer activity_user_id;

    @JsonIgnore
    private boolean activity_user_idDirtyFlag;
    
    /**
     * 附加信息
     */
    private String additional_info;

    @JsonIgnore
    private boolean additional_infoDirtyFlag;
    
    /**
     * 工作岗位
     */
    private String ibizfunction;

    @JsonIgnore
    private boolean ibizfunctionDirtyFlag;
    
    /**
     * 网站网址
     */
    private String website_url;

    @JsonIgnore
    private boolean website_urlDirtyFlag;
    
    /**
     * 最后的提醒已经标志为已读
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp calendar_last_notif_ack;

    @JsonIgnore
    private boolean calendar_last_notif_ackDirtyFlag;
    
    /**
     * 操作次数
     */
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;
    
    /**
     * 下一活动截止日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp activity_date_deadline;

    @JsonIgnore
    private boolean activity_date_deadlineDirtyFlag;
    
    /**
     * 员工
     */
    private String employee;

    @JsonIgnore
    private boolean employeeDirtyFlag;
    
    /**
     * 活动状态
     */
    private String activity_state;

    @JsonIgnore
    private boolean activity_stateDirtyFlag;
    
    /**
     * 条码
     */
    private String barcode;

    @JsonIgnore
    private boolean barcodeDirtyFlag;
    
    /**
     * 公司数据库ID
     */
    private Integer partner_gid;

    @JsonIgnore
    private boolean partner_gidDirtyFlag;
    
    /**
     * 地址类型
     */
    private String type;

    @JsonIgnore
    private boolean typeDirtyFlag;
    
    /**
     * 税号
     */
    private String vat;

    @JsonIgnore
    private boolean vatDirtyFlag;
    
    /**
     * 采购订单消息
     */
    private String purchase_warn_msg;

    @JsonIgnore
    private boolean purchase_warn_msgDirtyFlag;
    
    /**
     * 便签
     */
    private String comment;

    @JsonIgnore
    private boolean commentDirtyFlag;
    
    /**
     * 供应商
     */
    private String supplier;

    @JsonIgnore
    private boolean supplierDirtyFlag;
    
    /**
     * 网站meta关键词
     */
    private String website_meta_keywords;

    @JsonIgnore
    private boolean website_meta_keywordsDirtyFlag;
    
    /**
     * 关联公司
     */
    private Integer parent_id;

    @JsonIgnore
    private boolean parent_idDirtyFlag;
    
    /**
     * 采购订单
     */
    private String purchase_warn;

    @JsonIgnore
    private boolean purchase_warnDirtyFlag;
    
    /**
     * 激活的合作伙伴
     */
    private String active_partner;

    @JsonIgnore
    private boolean active_partnerDirtyFlag;
    
    /**
     * 币种
     */
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;
    
    /**
     * 工业
     */
    private Integer industry_id;

    @JsonIgnore
    private boolean industry_idDirtyFlag;
    
    /**
     * 供应商位置
     */
    private Integer property_stock_supplier;

    @JsonIgnore
    private boolean property_stock_supplierDirtyFlag;
    
    /**
     * 付款令牌计数
     */
    private Integer payment_token_count;

    @JsonIgnore
    private boolean payment_token_countDirtyFlag;
    
    /**
     * 前置操作
     */
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;
    
    /**
     * 销售员
     */
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;
    
    /**
     * 客户
     */
    private String customer;

    @JsonIgnore
    private boolean customerDirtyFlag;
    
    /**
     * 客户付款条款
     */
    private Integer property_payment_term_id;

    @JsonIgnore
    private boolean property_payment_term_idDirtyFlag;
    
    /**
     * 下一活动类型
     */
    private Integer activity_type_id;

    @JsonIgnore
    private boolean activity_type_idDirtyFlag;
    
    /**
     * 合同统计
     */
    private Integer contracts_count;

    @JsonIgnore
    private boolean contracts_countDirtyFlag;
    
    /**
     * 自己
     */
    private Integer self;

    @JsonIgnore
    private boolean selfDirtyFlag;
    
    /**
     * 网站元说明
     */
    private String website_meta_description;

    @JsonIgnore
    private boolean website_meta_descriptionDirtyFlag;
    
    /**
     * 图像
     */
    private byte[] image;

    @JsonIgnore
    private boolean imageDirtyFlag;
    
    /**
     * EMail
     */
    private String email;

    @JsonIgnore
    private boolean emailDirtyFlag;
    
    /**
     * 中等尺寸图像
     */
    private byte[] image_medium;

    @JsonIgnore
    private boolean image_mediumDirtyFlag;
    
    /**
     * 下一个活动摘要
     */
    private String activity_summary;

    @JsonIgnore
    private boolean activity_summaryDirtyFlag;
    
    /**
     * 应付限额
     */
    private Double debit_limit;

    @JsonIgnore
    private boolean debit_limitDirtyFlag;
    
    /**
     * 国家/地区
     */
    private Integer country_id;

    @JsonIgnore
    private boolean country_idDirtyFlag;
    
    /**
     * 信用额度
     */
    private Double credit_limit;

    @JsonIgnore
    private boolean credit_limitDirtyFlag;
    
    /**
     * 公司名称实体
     */
    private String commercial_company_name;

    @JsonIgnore
    private boolean commercial_company_nameDirtyFlag;
    
    /**
     * 发票消息
     */
    private String invoice_warn_msg;

    @JsonIgnore
    private boolean invoice_warn_msgDirtyFlag;
    
    /**
     * 已发布
     */
    private String is_published;

    @JsonIgnore
    private boolean is_publishedDirtyFlag;
    
    /**
     * 对此债务人的信任度
     */
    private String trust;

    @JsonIgnore
    private boolean trustDirtyFlag;
    
    /**
     * 手机
     */
    private String mobile;

    @JsonIgnore
    private boolean mobileDirtyFlag;
    
    /**
     * 格式化的邮件
     */
    private String email_formatted;

    @JsonIgnore
    private boolean email_formattedDirtyFlag;
    
    /**
     * 公司
     */
    private String is_company;

    @JsonIgnore
    private boolean is_companyDirtyFlag;
    
    /**
     * 创建人
     */
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;
    
    /**
     * 销售团队
     */
    private Integer team_id;

    @JsonIgnore
    private boolean team_idDirtyFlag;
    
    /**
     * 黑名单
     */
    private String is_blacklisted;

    @JsonIgnore
    private boolean is_blacklistedDirtyFlag;
    
    /**
     * 银行
     */
    private Integer bank_account_count;

    @JsonIgnore
    private boolean bank_account_countDirtyFlag;
    
    /**
     * 价格表
     */
    private Integer property_product_pricelist;

    @JsonIgnore
    private boolean property_product_pricelistDirtyFlag;
    
    /**
     * 名称
     */
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;
    
    /**
     * 库存拣货单消息
     */
    private String picking_warn_msg;

    @JsonIgnore
    private boolean picking_warn_msgDirtyFlag;
    
    /**
     * 附件数量
     */
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;
    
    /**
     * 网站
     */
    private String website;

    @JsonIgnore
    private boolean websiteDirtyFlag;
    
    /**
     * 电话
     */
    private String phone;

    @JsonIgnore
    private boolean phoneDirtyFlag;
    
    /**
     * 街道 2
     */
    private String street2;

    @JsonIgnore
    private boolean street2DirtyFlag;
    
    /**
     * 有未核销的分录
     */
    private String has_unreconciled_entries;

    @JsonIgnore
    private boolean has_unreconciled_entriesDirtyFlag;
    
    /**
     * 注册到期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp signup_expiration;

    @JsonIgnore
    private boolean signup_expirationDirtyFlag;
    
    /**
     * 时区
     */
    private String tz;

    @JsonIgnore
    private boolean tzDirtyFlag;
    
    /**
     * 完整地址
     */
    private String contact_address;

    @JsonIgnore
    private boolean contact_addressDirtyFlag;
    
    /**
     * 网站业务伙伴简介
     */
    private String website_short_description;

    @JsonIgnore
    private boolean website_short_descriptionDirtyFlag;
    
    /**
     * 共享合作伙伴
     */
    private String partner_share;

    @JsonIgnore
    private boolean partner_shareDirtyFlag;
    
    /**
     * 公司
     */
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;
    
    /**
     * 颜色索引
     */
    private Integer color;

    @JsonIgnore
    private boolean colorDirtyFlag;
    
    /**
     * 邮政编码
     */
    private String zip;

    @JsonIgnore
    private boolean zipDirtyFlag;
    
    /**
     * 销售订单个数
     */
    private Integer sale_order_count;

    @JsonIgnore
    private boolean sale_order_countDirtyFlag;
    
    /**
     * 公司类别
     */
    private String company_type;

    @JsonIgnore
    private boolean company_typeDirtyFlag;
    
    /**
     * 税科目调整
     */
    private Integer property_account_position_id;

    @JsonIgnore
    private boolean property_account_position_idDirtyFlag;
    
    /**
     * SEO优化
     */
    private String is_seo_optimized;

    @JsonIgnore
    private boolean is_seo_optimizedDirtyFlag;
    
    /**
     * 退回
     */
    private Integer message_bounce;

    @JsonIgnore
    private boolean message_bounceDirtyFlag;
    
    /**
     * 网站meta标题
     */
    private String website_meta_title;

    @JsonIgnore
    private boolean website_meta_titleDirtyFlag;
    
    /**
     * 小尺寸图像
     */
    private byte[] image_small;

    @JsonIgnore
    private boolean image_smallDirtyFlag;
    
    /**
     * 供应商货币
     */
    private Integer property_purchase_currency_id;

    @JsonIgnore
    private boolean property_purchase_currency_idDirtyFlag;
    
    /**
     * 采购订单数
     */
    private Integer purchase_order_count;

    @JsonIgnore
    private boolean purchase_order_countDirtyFlag;
    
    /**
     * 网站业务伙伴的详细说明
     */
    private String website_description;

    @JsonIgnore
    private boolean website_descriptionDirtyFlag;
    
    /**
     * 供应商付款条款
     */
    private Integer property_supplier_payment_term_id;

    @JsonIgnore
    private boolean property_supplier_payment_term_idDirtyFlag;
    
    /**
     * 商业实体
     */
    private Integer commercial_partner_id;

    @JsonIgnore
    private boolean commercial_partner_idDirtyFlag;
    
    /**
     * 安全联系人别名
     */
    private String alias_contact;

    @JsonIgnore
    private boolean alias_contactDirtyFlag;
    
    /**
     * 公司名称
     */
    private String company_name;

    @JsonIgnore
    private boolean company_nameDirtyFlag;
    
    /**
     * 公司
     */
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;
    
    /**
     * 相关的业务伙伴
     */
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;
    
    /**
     * 别名
     */
    private Integer alias_id;

    @JsonIgnore
    private boolean alias_idDirtyFlag;
    
    /**
     * 用户的销售团队
     */
    private Integer sale_team_id;

    @JsonIgnore
    private boolean sale_team_idDirtyFlag;
    
    /**
     * 最后更新者
     */
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;
    
    /**
     * 创建人
     */
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;
    

    /**
     * 获取 [管理员]
     */
    @JsonProperty("is_moderator")
    public String getIs_moderator(){
        return this.is_moderator ;
    }

    /**
     * 设置 [管理员]
     */
    @JsonProperty("is_moderator")
    public void setIs_moderator(String  is_moderator){
        this.is_moderator = is_moderator ;
        this.is_moderatorDirtyFlag = true ;
    }

    /**
     * 获取 [管理员]脏标记
     */
    @JsonIgnore
    public boolean getIs_moderatorDirtyFlag(){
        return this.is_moderatorDirtyFlag ;
    }

    /**
     * 获取 [资源]
     */
    @JsonProperty("resource_ids")
    public String getResource_ids(){
        return this.resource_ids ;
    }

    /**
     * 设置 [资源]
     */
    @JsonProperty("resource_ids")
    public void setResource_ids(String  resource_ids){
        this.resource_ids = resource_ids ;
        this.resource_idsDirtyFlag = true ;
    }

    /**
     * 获取 [资源]脏标记
     */
    @JsonIgnore
    public boolean getResource_idsDirtyFlag(){
        return this.resource_idsDirtyFlag ;
    }

    /**
     * 获取 [创建日期]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建日期]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [创建日期]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }

    /**
     * 获取 [标签]
     */
    @JsonProperty("category_id")
    public String getCategory_id(){
        return this.category_id ;
    }

    /**
     * 设置 [标签]
     */
    @JsonProperty("category_id")
    public void setCategory_id(String  category_id){
        this.category_id = category_id ;
        this.category_idDirtyFlag = true ;
    }

    /**
     * 获取 [标签]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idDirtyFlag(){
        return this.category_idDirtyFlag ;
    }

    /**
     * 获取 [默认工作时间]
     */
    @JsonProperty("resource_calendar_id")
    public Integer getResource_calendar_id(){
        return this.resource_calendar_id ;
    }

    /**
     * 设置 [默认工作时间]
     */
    @JsonProperty("resource_calendar_id")
    public void setResource_calendar_id(Integer  resource_calendar_id){
        this.resource_calendar_id = resource_calendar_id ;
        this.resource_calendar_idDirtyFlag = true ;
    }

    /**
     * 获取 [默认工作时间]脏标记
     */
    @JsonIgnore
    public boolean getResource_calendar_idDirtyFlag(){
        return this.resource_calendar_idDirtyFlag ;
    }

    /**
     * 获取 [用户登录记录]
     */
    @JsonProperty("log_ids")
    public String getLog_ids(){
        return this.log_ids ;
    }

    /**
     * 设置 [用户登录记录]
     */
    @JsonProperty("log_ids")
    public void setLog_ids(String  log_ids){
        this.log_ids = log_ids ;
        this.log_idsDirtyFlag = true ;
    }

    /**
     * 获取 [用户登录记录]脏标记
     */
    @JsonIgnore
    public boolean getLog_idsDirtyFlag(){
        return this.log_idsDirtyFlag ;
    }

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }

    /**
     * 获取 [安全PIN]
     */
    @JsonProperty("pos_security_pin")
    public String getPos_security_pin(){
        return this.pos_security_pin ;
    }

    /**
     * 设置 [安全PIN]
     */
    @JsonProperty("pos_security_pin")
    public void setPos_security_pin(String  pos_security_pin){
        this.pos_security_pin = pos_security_pin ;
        this.pos_security_pinDirtyFlag = true ;
    }

    /**
     * 获取 [安全PIN]脏标记
     */
    @JsonIgnore
    public boolean getPos_security_pinDirtyFlag(){
        return this.pos_security_pinDirtyFlag ;
    }

    /**
     * 获取 [徽章]
     */
    @JsonProperty("badge_ids")
    public String getBadge_ids(){
        return this.badge_ids ;
    }

    /**
     * 设置 [徽章]
     */
    @JsonProperty("badge_ids")
    public void setBadge_ids(String  badge_ids){
        this.badge_ids = badge_ids ;
        this.badge_idsDirtyFlag = true ;
    }

    /**
     * 获取 [徽章]脏标记
     */
    @JsonIgnore
    public boolean getBadge_idsDirtyFlag(){
        return this.badge_idsDirtyFlag ;
    }

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_ids")
    public String getCompany_ids(){
        return this.company_ids ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_ids")
    public void setCompany_ids(String  company_ids){
        this.company_ids = company_ids ;
        this.company_idsDirtyFlag = true ;
    }

    /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idsDirtyFlag(){
        return this.company_idsDirtyFlag ;
    }

    /**
     * 获取 [联系人]
     */
    @JsonProperty("child_ids")
    public String getChild_ids(){
        return this.child_ids ;
    }

    /**
     * 设置 [联系人]
     */
    @JsonProperty("child_ids")
    public void setChild_ids(String  child_ids){
        this.child_ids = child_ids ;
        this.child_idsDirtyFlag = true ;
    }

    /**
     * 获取 [联系人]脏标记
     */
    @JsonIgnore
    public boolean getChild_idsDirtyFlag(){
        return this.child_idsDirtyFlag ;
    }

    /**
     * 获取 [时区偏移]
     */
    @JsonProperty("tz_offset")
    public String getTz_offset(){
        return this.tz_offset ;
    }

    /**
     * 设置 [时区偏移]
     */
    @JsonProperty("tz_offset")
    public void setTz_offset(String  tz_offset){
        this.tz_offset = tz_offset ;
        this.tz_offsetDirtyFlag = true ;
    }

    /**
     * 获取 [时区偏移]脏标记
     */
    @JsonIgnore
    public boolean getTz_offsetDirtyFlag(){
        return this.tz_offsetDirtyFlag ;
    }

    /**
     * 获取 [活动达成]
     */
    @JsonProperty("target_sales_done")
    public Integer getTarget_sales_done(){
        return this.target_sales_done ;
    }

    /**
     * 设置 [活动达成]
     */
    @JsonProperty("target_sales_done")
    public void setTarget_sales_done(Integer  target_sales_done){
        this.target_sales_done = target_sales_done ;
        this.target_sales_doneDirtyFlag = true ;
    }

    /**
     * 获取 [活动达成]脏标记
     */
    @JsonIgnore
    public boolean getTarget_sales_doneDirtyFlag(){
        return this.target_sales_doneDirtyFlag ;
    }

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [通知管理]
     */
    @JsonProperty("notification_type")
    public String getNotification_type(){
        return this.notification_type ;
    }

    /**
     * 设置 [通知管理]
     */
    @JsonProperty("notification_type")
    public void setNotification_type(String  notification_type){
        this.notification_type = notification_type ;
        this.notification_typeDirtyFlag = true ;
    }

    /**
     * 获取 [通知管理]脏标记
     */
    @JsonIgnore
    public boolean getNotification_typeDirtyFlag(){
        return this.notification_typeDirtyFlag ;
    }

    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }

    /**
     * 获取 [IM的状态]
     */
    @JsonProperty("im_status")
    public String getIm_status(){
        return this.im_status ;
    }

    /**
     * 设置 [IM的状态]
     */
    @JsonProperty("im_status")
    public void setIm_status(String  im_status){
        this.im_status = im_status ;
        this.im_statusDirtyFlag = true ;
    }

    /**
     * 获取 [IM的状态]脏标记
     */
    @JsonIgnore
    public boolean getIm_statusDirtyFlag(){
        return this.im_statusDirtyFlag ;
    }

    /**
     * 获取 [贡献值]
     */
    @JsonProperty("karma")
    public Integer getKarma(){
        return this.karma ;
    }

    /**
     * 设置 [贡献值]
     */
    @JsonProperty("karma")
    public void setKarma(Integer  karma){
        this.karma = karma ;
        this.karmaDirtyFlag = true ;
    }

    /**
     * 获取 [贡献值]脏标记
     */
    @JsonIgnore
    public boolean getKarmaDirtyFlag(){
        return this.karmaDirtyFlag ;
    }

    /**
     * 获取 [登记网站]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return this.website_id ;
    }

    /**
     * 设置 [登记网站]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

    /**
     * 获取 [登记网站]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return this.website_idDirtyFlag ;
    }

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return this.activity_ids ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return this.activity_idsDirtyFlag ;
    }

    /**
     * 获取 [金质徽章个数]
     */
    @JsonProperty("gold_badge")
    public Integer getGold_badge(){
        return this.gold_badge ;
    }

    /**
     * 设置 [金质徽章个数]
     */
    @JsonProperty("gold_badge")
    public void setGold_badge(Integer  gold_badge){
        this.gold_badge = gold_badge ;
        this.gold_badgeDirtyFlag = true ;
    }

    /**
     * 获取 [金质徽章个数]脏标记
     */
    @JsonIgnore
    public boolean getGold_badgeDirtyFlag(){
        return this.gold_badgeDirtyFlag ;
    }

    /**
     * 获取 [相关的员工]
     */
    @JsonProperty("employee_ids")
    public String getEmployee_ids(){
        return this.employee_ids ;
    }

    /**
     * 设置 [相关的员工]
     */
    @JsonProperty("employee_ids")
    public void setEmployee_ids(String  employee_ids){
        this.employee_ids = employee_ids ;
        this.employee_idsDirtyFlag = true ;
    }

    /**
     * 获取 [相关的员工]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_idsDirtyFlag(){
        return this.employee_idsDirtyFlag ;
    }

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }

    /**
     * 获取 [管理频道]
     */
    @JsonProperty("moderation_channel_ids")
    public String getModeration_channel_ids(){
        return this.moderation_channel_ids ;
    }

    /**
     * 设置 [管理频道]
     */
    @JsonProperty("moderation_channel_ids")
    public void setModeration_channel_ids(String  moderation_channel_ids){
        this.moderation_channel_ids = moderation_channel_ids ;
        this.moderation_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [管理频道]脏标记
     */
    @JsonIgnore
    public boolean getModeration_channel_idsDirtyFlag(){
        return this.moderation_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [银质徽章个数]
     */
    @JsonProperty("silver_badge")
    public Integer getSilver_badge(){
        return this.silver_badge ;
    }

    /**
     * 设置 [银质徽章个数]
     */
    @JsonProperty("silver_badge")
    public void setSilver_badge(Integer  silver_badge){
        this.silver_badge = silver_badge ;
        this.silver_badgeDirtyFlag = true ;
    }

    /**
     * 获取 [银质徽章个数]脏标记
     */
    @JsonIgnore
    public boolean getSilver_badgeDirtyFlag(){
        return this.silver_badgeDirtyFlag ;
    }

    /**
     * 获取 [付款令牌]
     */
    @JsonProperty("payment_token_ids")
    public String getPayment_token_ids(){
        return this.payment_token_ids ;
    }

    /**
     * 设置 [付款令牌]
     */
    @JsonProperty("payment_token_ids")
    public void setPayment_token_ids(String  payment_token_ids){
        this.payment_token_ids = payment_token_ids ;
        this.payment_token_idsDirtyFlag = true ;
    }

    /**
     * 获取 [付款令牌]脏标记
     */
    @JsonIgnore
    public boolean getPayment_token_idsDirtyFlag(){
        return this.payment_token_idsDirtyFlag ;
    }

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }

    /**
     * 获取 [公司数量]
     */
    @JsonProperty("companies_count")
    public Integer getCompanies_count(){
        return this.companies_count ;
    }

    /**
     * 设置 [公司数量]
     */
    @JsonProperty("companies_count")
    public void setCompanies_count(Integer  companies_count){
        this.companies_count = companies_count ;
        this.companies_countDirtyFlag = true ;
    }

    /**
     * 获取 [公司数量]脏标记
     */
    @JsonIgnore
    public boolean getCompanies_countDirtyFlag(){
        return this.companies_countDirtyFlag ;
    }

    /**
     * 获取 [销售订单目标发票]
     */
    @JsonProperty("target_sales_invoiced")
    public Integer getTarget_sales_invoiced(){
        return this.target_sales_invoiced ;
    }

    /**
     * 设置 [销售订单目标发票]
     */
    @JsonProperty("target_sales_invoiced")
    public void setTarget_sales_invoiced(Integer  target_sales_invoiced){
        this.target_sales_invoiced = target_sales_invoiced ;
        this.target_sales_invoicedDirtyFlag = true ;
    }

    /**
     * 获取 [销售订单目标发票]脏标记
     */
    @JsonIgnore
    public boolean getTarget_sales_invoicedDirtyFlag(){
        return this.target_sales_invoicedDirtyFlag ;
    }

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [最后连接]
     */
    @JsonProperty("login_date")
    public Timestamp getLogin_date(){
        return this.login_date ;
    }

    /**
     * 设置 [最后连接]
     */
    @JsonProperty("login_date")
    public void setLogin_date(Timestamp  login_date){
        this.login_date = login_date ;
        this.login_dateDirtyFlag = true ;
    }

    /**
     * 获取 [最后连接]脏标记
     */
    @JsonIgnore
    public boolean getLogin_dateDirtyFlag(){
        return this.login_dateDirtyFlag ;
    }

    /**
     * 获取 [渠道]
     */
    @JsonProperty("channel_ids")
    public String getChannel_ids(){
        return this.channel_ids ;
    }

    /**
     * 设置 [渠道]
     */
    @JsonProperty("channel_ids")
    public void setChannel_ids(String  channel_ids){
        this.channel_ids = channel_ids ;
        this.channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [渠道]脏标记
     */
    @JsonIgnore
    public boolean getChannel_idsDirtyFlag(){
        return this.channel_idsDirtyFlag ;
    }

    /**
     * 获取 [群组]
     */
    @JsonProperty("groups_id")
    public String getGroups_id(){
        return this.groups_id ;
    }

    /**
     * 设置 [群组]
     */
    @JsonProperty("groups_id")
    public void setGroups_id(String  groups_id){
        this.groups_id = groups_id ;
        this.groups_idDirtyFlag = true ;
    }

    /**
     * 获取 [群组]脏标记
     */
    @JsonIgnore
    public boolean getGroups_idDirtyFlag(){
        return this.groups_idDirtyFlag ;
    }

    /**
     * 获取 [共享用户]
     */
    @JsonProperty("share")
    public String getShare(){
        return this.share ;
    }

    /**
     * 设置 [共享用户]
     */
    @JsonProperty("share")
    public void setShare(String  share){
        this.share = share ;
        this.shareDirtyFlag = true ;
    }

    /**
     * 获取 [共享用户]脏标记
     */
    @JsonIgnore
    public boolean getShareDirtyFlag(){
        return this.shareDirtyFlag ;
    }

    /**
     * 获取 [银行]
     */
    @JsonProperty("bank_ids")
    public String getBank_ids(){
        return this.bank_ids ;
    }

    /**
     * 设置 [银行]
     */
    @JsonProperty("bank_ids")
    public void setBank_ids(String  bank_ids){
        this.bank_ids = bank_ids ;
        this.bank_idsDirtyFlag = true ;
    }

    /**
     * 获取 [银行]脏标记
     */
    @JsonIgnore
    public boolean getBank_idsDirtyFlag(){
        return this.bank_idsDirtyFlag ;
    }

    /**
     * 获取 [销售订单]
     */
    @JsonProperty("sale_order_ids")
    public String getSale_order_ids(){
        return this.sale_order_ids ;
    }

    /**
     * 设置 [销售订单]
     */
    @JsonProperty("sale_order_ids")
    public void setSale_order_ids(String  sale_order_ids){
        this.sale_order_ids = sale_order_ids ;
        this.sale_order_idsDirtyFlag = true ;
    }

    /**
     * 获取 [销售订单]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_idsDirtyFlag(){
        return this.sale_order_idsDirtyFlag ;
    }

    /**
     * 获取 [设置密码]
     */
    @JsonProperty("new_password")
    public String getNew_password(){
        return this.new_password ;
    }

    /**
     * 设置 [设置密码]
     */
    @JsonProperty("new_password")
    public void setNew_password(String  new_password){
        this.new_password = new_password ;
        this.new_passwordDirtyFlag = true ;
    }

    /**
     * 获取 [设置密码]脏标记
     */
    @JsonIgnore
    public boolean getNew_passwordDirtyFlag(){
        return this.new_passwordDirtyFlag ;
    }

    /**
     * 获取 [OdooBot 状态]
     */
    @JsonProperty("odoobot_state")
    public String getOdoobot_state(){
        return this.odoobot_state ;
    }

    /**
     * 设置 [OdooBot 状态]
     */
    @JsonProperty("odoobot_state")
    public void setOdoobot_state(String  odoobot_state){
        this.odoobot_state = odoobot_state ;
        this.odoobot_stateDirtyFlag = true ;
    }

    /**
     * 获取 [OdooBot 状态]脏标记
     */
    @JsonIgnore
    public boolean getOdoobot_stateDirtyFlag(){
        return this.odoobot_stateDirtyFlag ;
    }

    /**
     * 获取 [公司是指业务伙伴]
     */
    @JsonProperty("ref_company_ids")
    public String getRef_company_ids(){
        return this.ref_company_ids ;
    }

    /**
     * 设置 [公司是指业务伙伴]
     */
    @JsonProperty("ref_company_ids")
    public void setRef_company_ids(String  ref_company_ids){
        this.ref_company_ids = ref_company_ids ;
        this.ref_company_idsDirtyFlag = true ;
    }

    /**
     * 获取 [公司是指业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getRef_company_idsDirtyFlag(){
        return this.ref_company_idsDirtyFlag ;
    }

    /**
     * 获取 [密码]
     */
    @JsonProperty("password")
    public String getPassword(){
        return this.password ;
    }

    /**
     * 设置 [密码]
     */
    @JsonProperty("password")
    public void setPassword(String  password){
        this.password = password ;
        this.passwordDirtyFlag = true ;
    }

    /**
     * 获取 [密码]脏标记
     */
    @JsonIgnore
    public boolean getPasswordDirtyFlag(){
        return this.passwordDirtyFlag ;
    }

    /**
     * 获取 [青铜徽章数目]
     */
    @JsonProperty("bronze_badge")
    public Integer getBronze_badge(){
        return this.bronze_badge ;
    }

    /**
     * 设置 [青铜徽章数目]
     */
    @JsonProperty("bronze_badge")
    public void setBronze_badge(Integer  bronze_badge){
        this.bronze_badge = bronze_badge ;
        this.bronze_badgeDirtyFlag = true ;
    }

    /**
     * 获取 [青铜徽章数目]脏标记
     */
    @JsonIgnore
    public boolean getBronze_badgeDirtyFlag(){
        return this.bronze_badgeDirtyFlag ;
    }

    /**
     * 获取 [会议]
     */
    @JsonProperty("meeting_ids")
    public String getMeeting_ids(){
        return this.meeting_ids ;
    }

    /**
     * 设置 [会议]
     */
    @JsonProperty("meeting_ids")
    public void setMeeting_ids(String  meeting_ids){
        this.meeting_ids = meeting_ids ;
        this.meeting_idsDirtyFlag = true ;
    }

    /**
     * 获取 [会议]脏标记
     */
    @JsonIgnore
    public boolean getMeeting_idsDirtyFlag(){
        return this.meeting_idsDirtyFlag ;
    }

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }

    /**
     * 获取 [待发布的帖子]
     */
    @JsonProperty("forum_waiting_posts_count")
    public Integer getForum_waiting_posts_count(){
        return this.forum_waiting_posts_count ;
    }

    /**
     * 设置 [待发布的帖子]
     */
    @JsonProperty("forum_waiting_posts_count")
    public void setForum_waiting_posts_count(Integer  forum_waiting_posts_count){
        this.forum_waiting_posts_count = forum_waiting_posts_count ;
        this.forum_waiting_posts_countDirtyFlag = true ;
    }

    /**
     * 获取 [待发布的帖子]脏标记
     */
    @JsonIgnore
    public boolean getForum_waiting_posts_countDirtyFlag(){
        return this.forum_waiting_posts_countDirtyFlag ;
    }

    /**
     * 获取 [目标]
     */
    @JsonProperty("goal_ids")
    public String getGoal_ids(){
        return this.goal_ids ;
    }

    /**
     * 设置 [目标]
     */
    @JsonProperty("goal_ids")
    public void setGoal_ids(String  goal_ids){
        this.goal_ids = goal_ids ;
        this.goal_idsDirtyFlag = true ;
    }

    /**
     * 获取 [目标]脏标记
     */
    @JsonIgnore
    public boolean getGoal_idsDirtyFlag(){
        return this.goal_idsDirtyFlag ;
    }

    /**
     * 获取 [签单是商机的最终目标]
     */
    @JsonProperty("target_sales_won")
    public Integer getTarget_sales_won(){
        return this.target_sales_won ;
    }

    /**
     * 设置 [签单是商机的最终目标]
     */
    @JsonProperty("target_sales_won")
    public void setTarget_sales_won(Integer  target_sales_won){
        this.target_sales_won = target_sales_won ;
        this.target_sales_wonDirtyFlag = true ;
    }

    /**
     * 获取 [签单是商机的最终目标]脏标记
     */
    @JsonIgnore
    public boolean getTarget_sales_wonDirtyFlag(){
        return this.target_sales_wonDirtyFlag ;
    }

    /**
     * 获取 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [网站消息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [主页动作]
     */
    @JsonProperty("action_id")
    public Integer getAction_id(){
        return this.action_id ;
    }

    /**
     * 设置 [主页动作]
     */
    @JsonProperty("action_id")
    public void setAction_id(Integer  action_id){
        this.action_id = action_id ;
        this.action_idDirtyFlag = true ;
    }

    /**
     * 获取 [主页动作]脏标记
     */
    @JsonIgnore
    public boolean getAction_idDirtyFlag(){
        return this.action_idDirtyFlag ;
    }

    /**
     * 获取 [登录]
     */
    @JsonProperty("login")
    public String getLogin(){
        return this.login ;
    }

    /**
     * 设置 [登录]
     */
    @JsonProperty("login")
    public void setLogin(String  login){
        this.login = login ;
        this.loginDirtyFlag = true ;
    }

    /**
     * 获取 [登录]脏标记
     */
    @JsonIgnore
    public boolean getLoginDirtyFlag(){
        return this.loginDirtyFlag ;
    }

    /**
     * 获取 [客户合同]
     */
    @JsonProperty("contract_ids")
    public String getContract_ids(){
        return this.contract_ids ;
    }

    /**
     * 设置 [客户合同]
     */
    @JsonProperty("contract_ids")
    public void setContract_ids(String  contract_ids){
        this.contract_ids = contract_ids ;
        this.contract_idsDirtyFlag = true ;
    }

    /**
     * 获取 [客户合同]脏标记
     */
    @JsonIgnore
    public boolean getContract_idsDirtyFlag(){
        return this.contract_idsDirtyFlag ;
    }

    /**
     * 获取 [审核数]
     */
    @JsonProperty("moderation_counter")
    public Integer getModeration_counter(){
        return this.moderation_counter ;
    }

    /**
     * 设置 [审核数]
     */
    @JsonProperty("moderation_counter")
    public void setModeration_counter(Integer  moderation_counter){
        this.moderation_counter = moderation_counter ;
        this.moderation_counterDirtyFlag = true ;
    }

    /**
     * 获取 [审核数]脏标记
     */
    @JsonIgnore
    public boolean getModeration_counterDirtyFlag(){
        return this.moderation_counterDirtyFlag ;
    }

    /**
     * 获取 [签名]
     */
    @JsonProperty("signature")
    public String getSignature(){
        return this.signature ;
    }

    /**
     * 设置 [签名]
     */
    @JsonProperty("signature")
    public void setSignature(String  signature){
        this.signature = signature ;
        this.signatureDirtyFlag = true ;
    }

    /**
     * 获取 [签名]脏标记
     */
    @JsonIgnore
    public boolean getSignatureDirtyFlag(){
        return this.signatureDirtyFlag ;
    }

    /**
     * 获取 [用户]
     */
    @JsonProperty("user_ids")
    public String getUser_ids(){
        return this.user_ids ;
    }

    /**
     * 设置 [用户]
     */
    @JsonProperty("user_ids")
    public void setUser_ids(String  user_ids){
        this.user_ids = user_ids ;
        this.user_idsDirtyFlag = true ;
    }

    /**
     * 获取 [用户]脏标记
     */
    @JsonIgnore
    public boolean getUser_idsDirtyFlag(){
        return this.user_idsDirtyFlag ;
    }

    /**
     * 获取 [商机]
     */
    @JsonProperty("opportunity_ids")
    public String getOpportunity_ids(){
        return this.opportunity_ids ;
    }

    /**
     * 设置 [商机]
     */
    @JsonProperty("opportunity_ids")
    public void setOpportunity_ids(String  opportunity_ids){
        this.opportunity_ids = opportunity_ids ;
        this.opportunity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [商机]脏标记
     */
    @JsonIgnore
    public boolean getOpportunity_idsDirtyFlag(){
        return this.opportunity_idsDirtyFlag ;
    }

    /**
     * 获取 [任务]
     */
    @JsonProperty("task_ids")
    public String getTask_ids(){
        return this.task_ids ;
    }

    /**
     * 设置 [任务]
     */
    @JsonProperty("task_ids")
    public void setTask_ids(String  task_ids){
        this.task_ids = task_ids ;
        this.task_idsDirtyFlag = true ;
    }

    /**
     * 获取 [任务]脏标记
     */
    @JsonIgnore
    public boolean getTask_idsDirtyFlag(){
        return this.task_idsDirtyFlag ;
    }

    /**
     * 获取 [发票]
     */
    @JsonProperty("invoice_ids")
    public String getInvoice_ids(){
        return this.invoice_ids ;
    }

    /**
     * 设置 [发票]
     */
    @JsonProperty("invoice_ids")
    public void setInvoice_ids(String  invoice_ids){
        this.invoice_ids = invoice_ids ;
        this.invoice_idsDirtyFlag = true ;
    }

    /**
     * 获取 [发票]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_idsDirtyFlag(){
        return this.invoice_idsDirtyFlag ;
    }

    /**
     * 获取 [内部参考]
     */
    @JsonProperty("ref")
    public String getRef(){
        return this.ref ;
    }

    /**
     * 设置 [内部参考]
     */
    @JsonProperty("ref")
    public void setRef(String  ref){
        this.ref = ref ;
        this.refDirtyFlag = true ;
    }

    /**
     * 获取 [内部参考]脏标记
     */
    @JsonIgnore
    public boolean getRefDirtyFlag(){
        return this.refDirtyFlag ;
    }

    /**
     * 获取 [错误个数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误个数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [错误个数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [最近的在线销售订单]
     */
    @JsonProperty("last_website_so_id")
    public Integer getLast_website_so_id(){
        return this.last_website_so_id ;
    }

    /**
     * 设置 [最近的在线销售订单]
     */
    @JsonProperty("last_website_so_id")
    public void setLast_website_so_id(Integer  last_website_so_id){
        this.last_website_so_id = last_website_so_id ;
        this.last_website_so_idDirtyFlag = true ;
    }

    /**
     * 获取 [最近的在线销售订单]脏标记
     */
    @JsonIgnore
    public boolean getLast_website_so_idDirtyFlag(){
        return this.last_website_so_idDirtyFlag ;
    }

    /**
     * 获取 [日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [客户位置]
     */
    @JsonProperty("property_stock_customer")
    public Integer getProperty_stock_customer(){
        return this.property_stock_customer ;
    }

    /**
     * 设置 [客户位置]
     */
    @JsonProperty("property_stock_customer")
    public void setProperty_stock_customer(Integer  property_stock_customer){
        this.property_stock_customer = property_stock_customer ;
        this.property_stock_customerDirtyFlag = true ;
    }

    /**
     * 获取 [客户位置]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_customerDirtyFlag(){
        return this.property_stock_customerDirtyFlag ;
    }

    /**
     * 获取 [最近的发票和付款匹配时间]
     */
    @JsonProperty("last_time_entries_checked")
    public Timestamp getLast_time_entries_checked(){
        return this.last_time_entries_checked ;
    }

    /**
     * 设置 [最近的发票和付款匹配时间]
     */
    @JsonProperty("last_time_entries_checked")
    public void setLast_time_entries_checked(Timestamp  last_time_entries_checked){
        this.last_time_entries_checked = last_time_entries_checked ;
        this.last_time_entries_checkedDirtyFlag = true ;
    }

    /**
     * 获取 [最近的发票和付款匹配时间]脏标记
     */
    @JsonIgnore
    public boolean getLast_time_entries_checkedDirtyFlag(){
        return this.last_time_entries_checkedDirtyFlag ;
    }

    /**
     * 获取 [语言]
     */
    @JsonProperty("lang")
    public String getLang(){
        return this.lang ;
    }

    /**
     * 设置 [语言]
     */
    @JsonProperty("lang")
    public void setLang(String  lang){
        this.lang = lang ;
        this.langDirtyFlag = true ;
    }

    /**
     * 获取 [语言]脏标记
     */
    @JsonIgnore
    public boolean getLangDirtyFlag(){
        return this.langDirtyFlag ;
    }

    /**
     * 获取 [销售警告]
     */
    @JsonProperty("sale_warn")
    public String getSale_warn(){
        return this.sale_warn ;
    }

    /**
     * 设置 [销售警告]
     */
    @JsonProperty("sale_warn")
    public void setSale_warn(String  sale_warn){
        this.sale_warn = sale_warn ;
        this.sale_warnDirtyFlag = true ;
    }

    /**
     * 获取 [销售警告]脏标记
     */
    @JsonIgnore
    public boolean getSale_warnDirtyFlag(){
        return this.sale_warnDirtyFlag ;
    }

    /**
     * 获取 [#会议]
     */
    @JsonProperty("meeting_count")
    public Integer getMeeting_count(){
        return this.meeting_count ;
    }

    /**
     * 设置 [#会议]
     */
    @JsonProperty("meeting_count")
    public void setMeeting_count(Integer  meeting_count){
        this.meeting_count = meeting_count ;
        this.meeting_countDirtyFlag = true ;
    }

    /**
     * 获取 [#会议]脏标记
     */
    @JsonIgnore
    public boolean getMeeting_countDirtyFlag(){
        return this.meeting_countDirtyFlag ;
    }

    /**
     * 获取 [街道]
     */
    @JsonProperty("street")
    public String getStreet(){
        return this.street ;
    }

    /**
     * 设置 [街道]
     */
    @JsonProperty("street")
    public void setStreet(String  street){
        this.street = street ;
        this.streetDirtyFlag = true ;
    }

    /**
     * 获取 [街道]脏标记
     */
    @JsonIgnore
    public boolean getStreetDirtyFlag(){
        return this.streetDirtyFlag ;
    }

    /**
     * 获取 [发票]
     */
    @JsonProperty("invoice_warn")
    public String getInvoice_warn(){
        return this.invoice_warn ;
    }

    /**
     * 设置 [发票]
     */
    @JsonProperty("invoice_warn")
    public void setInvoice_warn(String  invoice_warn){
        this.invoice_warn = invoice_warn ;
        this.invoice_warnDirtyFlag = true ;
    }

    /**
     * 获取 [发票]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_warnDirtyFlag(){
        return this.invoice_warnDirtyFlag ;
    }

    /**
     * 获取 [注册令牌 Token]
     */
    @JsonProperty("signup_token")
    public String getSignup_token(){
        return this.signup_token ;
    }

    /**
     * 设置 [注册令牌 Token]
     */
    @JsonProperty("signup_token")
    public void setSignup_token(String  signup_token){
        this.signup_token = signup_token ;
        this.signup_tokenDirtyFlag = true ;
    }

    /**
     * 获取 [注册令牌 Token]脏标记
     */
    @JsonIgnore
    public boolean getSignup_tokenDirtyFlag(){
        return this.signup_tokenDirtyFlag ;
    }

    /**
     * 获取 [# 任务]
     */
    @JsonProperty("task_count")
    public Integer getTask_count(){
        return this.task_count ;
    }

    /**
     * 设置 [# 任务]
     */
    @JsonProperty("task_count")
    public void setTask_count(Integer  task_count){
        this.task_count = task_count ;
        this.task_countDirtyFlag = true ;
    }

    /**
     * 获取 [# 任务]脏标记
     */
    @JsonIgnore
    public boolean getTask_countDirtyFlag(){
        return this.task_countDirtyFlag ;
    }

    /**
     * 获取 [注册令牌（ Token  ）是有效的]
     */
    @JsonProperty("signup_valid")
    public String getSignup_valid(){
        return this.signup_valid ;
    }

    /**
     * 设置 [注册令牌（ Token  ）是有效的]
     */
    @JsonProperty("signup_valid")
    public void setSignup_valid(String  signup_valid){
        this.signup_valid = signup_valid ;
        this.signup_validDirtyFlag = true ;
    }

    /**
     * 获取 [注册令牌（ Token  ）是有效的]脏标记
     */
    @JsonIgnore
    public boolean getSignup_validDirtyFlag(){
        return this.signup_validDirtyFlag ;
    }

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [注册令牌（Token）类型]
     */
    @JsonProperty("signup_type")
    public String getSignup_type(){
        return this.signup_type ;
    }

    /**
     * 设置 [注册令牌（Token）类型]
     */
    @JsonProperty("signup_type")
    public void setSignup_type(String  signup_type){
        this.signup_type = signup_type ;
        this.signup_typeDirtyFlag = true ;
    }

    /**
     * 获取 [注册令牌（Token）类型]脏标记
     */
    @JsonIgnore
    public boolean getSignup_typeDirtyFlag(){
        return this.signup_typeDirtyFlag ;
    }

    /**
     * 获取 [应收账款]
     */
    @JsonProperty("property_account_receivable_id")
    public Integer getProperty_account_receivable_id(){
        return this.property_account_receivable_id ;
    }

    /**
     * 设置 [应收账款]
     */
    @JsonProperty("property_account_receivable_id")
    public void setProperty_account_receivable_id(Integer  property_account_receivable_id){
        this.property_account_receivable_id = property_account_receivable_id ;
        this.property_account_receivable_idDirtyFlag = true ;
    }

    /**
     * 获取 [应收账款]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_receivable_idDirtyFlag(){
        return this.property_account_receivable_idDirtyFlag ;
    }

    /**
     * 获取 [网站opengraph图像]
     */
    @JsonProperty("website_meta_og_img")
    public String getWebsite_meta_og_img(){
        return this.website_meta_og_img ;
    }

    /**
     * 设置 [网站opengraph图像]
     */
    @JsonProperty("website_meta_og_img")
    public void setWebsite_meta_og_img(String  website_meta_og_img){
        this.website_meta_og_img = website_meta_og_img ;
        this.website_meta_og_imgDirtyFlag = true ;
    }

    /**
     * 获取 [网站opengraph图像]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_og_imgDirtyFlag(){
        return this.website_meta_og_imgDirtyFlag ;
    }

    /**
     * 获取 [活动]
     */
    @JsonProperty("event_count")
    public Integer getEvent_count(){
        return this.event_count ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("event_count")
    public void setEvent_count(Integer  event_count){
        this.event_count = event_count ;
        this.event_countDirtyFlag = true ;
    }

    /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getEvent_countDirtyFlag(){
        return this.event_countDirtyFlag ;
    }

    /**
     * 获取 [日记账项目]
     */
    @JsonProperty("journal_item_count")
    public Integer getJournal_item_count(){
        return this.journal_item_count ;
    }

    /**
     * 设置 [日记账项目]
     */
    @JsonProperty("journal_item_count")
    public void setJournal_item_count(Integer  journal_item_count){
        this.journal_item_count = journal_item_count ;
        this.journal_item_countDirtyFlag = true ;
    }

    /**
     * 获取 [日记账项目]脏标记
     */
    @JsonIgnore
    public boolean getJournal_item_countDirtyFlag(){
        return this.journal_item_countDirtyFlag ;
    }

    /**
     * 获取 [上级名称]
     */
    @JsonProperty("parent_name")
    public String getParent_name(){
        return this.parent_name ;
    }

    /**
     * 设置 [上级名称]
     */
    @JsonProperty("parent_name")
    public void setParent_name(String  parent_name){
        this.parent_name = parent_name ;
        this.parent_nameDirtyFlag = true ;
    }

    /**
     * 获取 [上级名称]脏标记
     */
    @JsonIgnore
    public boolean getParent_nameDirtyFlag(){
        return this.parent_nameDirtyFlag ;
    }

    /**
     * 获取 [用户的销售团队]
     */
    @JsonProperty("sale_team_id_text")
    public String getSale_team_id_text(){
        return this.sale_team_id_text ;
    }

    /**
     * 设置 [用户的销售团队]
     */
    @JsonProperty("sale_team_id_text")
    public void setSale_team_id_text(String  sale_team_id_text){
        this.sale_team_id_text = sale_team_id_text ;
        this.sale_team_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [用户的销售团队]脏标记
     */
    @JsonIgnore
    public boolean getSale_team_id_textDirtyFlag(){
        return this.sale_team_id_textDirtyFlag ;
    }

    /**
     * 获取 [应收总计]
     */
    @JsonProperty("credit")
    public Double getCredit(){
        return this.credit ;
    }

    /**
     * 设置 [应收总计]
     */
    @JsonProperty("credit")
    public void setCredit(Double  credit){
        this.credit = credit ;
        this.creditDirtyFlag = true ;
    }

    /**
     * 获取 [应收总计]脏标记
     */
    @JsonIgnore
    public boolean getCreditDirtyFlag(){
        return this.creditDirtyFlag ;
    }

    /**
     * 获取 [商机]
     */
    @JsonProperty("opportunity_count")
    public Integer getOpportunity_count(){
        return this.opportunity_count ;
    }

    /**
     * 设置 [商机]
     */
    @JsonProperty("opportunity_count")
    public void setOpportunity_count(Integer  opportunity_count){
        this.opportunity_count = opportunity_count ;
        this.opportunity_countDirtyFlag = true ;
    }

    /**
     * 获取 [商机]脏标记
     */
    @JsonIgnore
    public boolean getOpportunity_countDirtyFlag(){
        return this.opportunity_countDirtyFlag ;
    }

    /**
     * 获取 [注册网址]
     */
    @JsonProperty("signup_url")
    public String getSignup_url(){
        return this.signup_url ;
    }

    /**
     * 设置 [注册网址]
     */
    @JsonProperty("signup_url")
    public void setSignup_url(String  signup_url){
        this.signup_url = signup_url ;
        this.signup_urlDirtyFlag = true ;
    }

    /**
     * 获取 [注册网址]脏标记
     */
    @JsonIgnore
    public boolean getSignup_urlDirtyFlag(){
        return this.signup_urlDirtyFlag ;
    }

    /**
     * 获取 [应付账款]
     */
    @JsonProperty("property_account_payable_id")
    public Integer getProperty_account_payable_id(){
        return this.property_account_payable_id ;
    }

    /**
     * 设置 [应付账款]
     */
    @JsonProperty("property_account_payable_id")
    public void setProperty_account_payable_id(Integer  property_account_payable_id){
        this.property_account_payable_id = property_account_payable_id ;
        this.property_account_payable_idDirtyFlag = true ;
    }

    /**
     * 获取 [应付账款]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_payable_idDirtyFlag(){
        return this.property_account_payable_idDirtyFlag ;
    }

    /**
     * 获取 [省/ 州]
     */
    @JsonProperty("state_id")
    public Integer getState_id(){
        return this.state_id ;
    }

    /**
     * 设置 [省/ 州]
     */
    @JsonProperty("state_id")
    public void setState_id(Integer  state_id){
        this.state_id = state_id ;
        this.state_idDirtyFlag = true ;
    }

    /**
     * 获取 [省/ 州]脏标记
     */
    @JsonIgnore
    public boolean getState_idDirtyFlag(){
        return this.state_idDirtyFlag ;
    }

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [销售订单消息]
     */
    @JsonProperty("sale_warn_msg")
    public String getSale_warn_msg(){
        return this.sale_warn_msg ;
    }

    /**
     * 设置 [销售订单消息]
     */
    @JsonProperty("sale_warn_msg")
    public void setSale_warn_msg(String  sale_warn_msg){
        this.sale_warn_msg = sale_warn_msg ;
        this.sale_warn_msgDirtyFlag = true ;
    }

    /**
     * 获取 [销售订单消息]脏标记
     */
    @JsonIgnore
    public boolean getSale_warn_msgDirtyFlag(){
        return this.sale_warn_msgDirtyFlag ;
    }

    /**
     * 获取 [在当前网站显示]
     */
    @JsonProperty("website_published")
    public String getWebsite_published(){
        return this.website_published ;
    }

    /**
     * 设置 [在当前网站显示]
     */
    @JsonProperty("website_published")
    public void setWebsite_published(String  website_published){
        this.website_published = website_published ;
        this.website_publishedDirtyFlag = true ;
    }

    /**
     * 获取 [在当前网站显示]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_publishedDirtyFlag(){
        return this.website_publishedDirtyFlag ;
    }

    /**
     * 获取 [已开票总计]
     */
    @JsonProperty("total_invoiced")
    public Double getTotal_invoiced(){
        return this.total_invoiced ;
    }

    /**
     * 设置 [已开票总计]
     */
    @JsonProperty("total_invoiced")
    public void setTotal_invoiced(Double  total_invoiced){
        this.total_invoiced = total_invoiced ;
        this.total_invoicedDirtyFlag = true ;
    }

    /**
     * 获取 [已开票总计]脏标记
     */
    @JsonIgnore
    public boolean getTotal_invoicedDirtyFlag(){
        return this.total_invoicedDirtyFlag ;
    }

    /**
     * 获取 [应付总计]
     */
    @JsonProperty("debit")
    public Double getDebit(){
        return this.debit ;
    }

    /**
     * 设置 [应付总计]
     */
    @JsonProperty("debit")
    public void setDebit(Double  debit){
        this.debit = debit ;
        this.debitDirtyFlag = true ;
    }

    /**
     * 获取 [应付总计]脏标记
     */
    @JsonIgnore
    public boolean getDebitDirtyFlag(){
        return this.debitDirtyFlag ;
    }

    /**
     * 获取 [销售点订单计数]
     */
    @JsonProperty("pos_order_count")
    public Integer getPos_order_count(){
        return this.pos_order_count ;
    }

    /**
     * 设置 [销售点订单计数]
     */
    @JsonProperty("pos_order_count")
    public void setPos_order_count(Integer  pos_order_count){
        this.pos_order_count = pos_order_count ;
        this.pos_order_countDirtyFlag = true ;
    }

    /**
     * 获取 [销售点订单计数]脏标记
     */
    @JsonIgnore
    public boolean getPos_order_countDirtyFlag(){
        return this.pos_order_countDirtyFlag ;
    }

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [称谓]
     */
    @JsonProperty("title")
    public Integer getTitle(){
        return this.title ;
    }

    /**
     * 设置 [称谓]
     */
    @JsonProperty("title")
    public void setTitle(Integer  title){
        this.title = title ;
        this.titleDirtyFlag = true ;
    }

    /**
     * 获取 [称谓]脏标记
     */
    @JsonIgnore
    public boolean getTitleDirtyFlag(){
        return this.titleDirtyFlag ;
    }

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }

    /**
     * 获取 [＃供应商账单]
     */
    @JsonProperty("supplier_invoice_count")
    public Integer getSupplier_invoice_count(){
        return this.supplier_invoice_count ;
    }

    /**
     * 设置 [＃供应商账单]
     */
    @JsonProperty("supplier_invoice_count")
    public void setSupplier_invoice_count(Integer  supplier_invoice_count){
        this.supplier_invoice_count = supplier_invoice_count ;
        this.supplier_invoice_countDirtyFlag = true ;
    }

    /**
     * 获取 [＃供应商账单]脏标记
     */
    @JsonIgnore
    public boolean getSupplier_invoice_countDirtyFlag(){
        return this.supplier_invoice_countDirtyFlag ;
    }

    /**
     * 获取 [城市]
     */
    @JsonProperty("city")
    public String getCity(){
        return this.city ;
    }

    /**
     * 设置 [城市]
     */
    @JsonProperty("city")
    public void setCity(String  city){
        this.city = city ;
        this.cityDirtyFlag = true ;
    }

    /**
     * 获取 [城市]脏标记
     */
    @JsonIgnore
    public boolean getCityDirtyFlag(){
        return this.cityDirtyFlag ;
    }

    /**
     * 获取 [库存拣货]
     */
    @JsonProperty("picking_warn")
    public String getPicking_warn(){
        return this.picking_warn ;
    }

    /**
     * 设置 [库存拣货]
     */
    @JsonProperty("picking_warn")
    public void setPicking_warn(String  picking_warn){
        this.picking_warn = picking_warn ;
        this.picking_warnDirtyFlag = true ;
    }

    /**
     * 获取 [库存拣货]脏标记
     */
    @JsonIgnore
    public boolean getPicking_warnDirtyFlag(){
        return this.picking_warnDirtyFlag ;
    }

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return this.activity_user_id ;
    }

    /**
     * 设置 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [责任用户]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return this.activity_user_idDirtyFlag ;
    }

    /**
     * 获取 [附加信息]
     */
    @JsonProperty("additional_info")
    public String getAdditional_info(){
        return this.additional_info ;
    }

    /**
     * 设置 [附加信息]
     */
    @JsonProperty("additional_info")
    public void setAdditional_info(String  additional_info){
        this.additional_info = additional_info ;
        this.additional_infoDirtyFlag = true ;
    }

    /**
     * 获取 [附加信息]脏标记
     */
    @JsonIgnore
    public boolean getAdditional_infoDirtyFlag(){
        return this.additional_infoDirtyFlag ;
    }

    /**
     * 获取 [工作岗位]
     */
    @JsonProperty("ibizfunction")
    public String getIbizfunction(){
        return this.ibizfunction ;
    }

    /**
     * 设置 [工作岗位]
     */
    @JsonProperty("ibizfunction")
    public void setIbizfunction(String  ibizfunction){
        this.ibizfunction = ibizfunction ;
        this.ibizfunctionDirtyFlag = true ;
    }

    /**
     * 获取 [工作岗位]脏标记
     */
    @JsonIgnore
    public boolean getIbizfunctionDirtyFlag(){
        return this.ibizfunctionDirtyFlag ;
    }

    /**
     * 获取 [网站网址]
     */
    @JsonProperty("website_url")
    public String getWebsite_url(){
        return this.website_url ;
    }

    /**
     * 设置 [网站网址]
     */
    @JsonProperty("website_url")
    public void setWebsite_url(String  website_url){
        this.website_url = website_url ;
        this.website_urlDirtyFlag = true ;
    }

    /**
     * 获取 [网站网址]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_urlDirtyFlag(){
        return this.website_urlDirtyFlag ;
    }

    /**
     * 获取 [最后的提醒已经标志为已读]
     */
    @JsonProperty("calendar_last_notif_ack")
    public Timestamp getCalendar_last_notif_ack(){
        return this.calendar_last_notif_ack ;
    }

    /**
     * 设置 [最后的提醒已经标志为已读]
     */
    @JsonProperty("calendar_last_notif_ack")
    public void setCalendar_last_notif_ack(Timestamp  calendar_last_notif_ack){
        this.calendar_last_notif_ack = calendar_last_notif_ack ;
        this.calendar_last_notif_ackDirtyFlag = true ;
    }

    /**
     * 获取 [最后的提醒已经标志为已读]脏标记
     */
    @JsonIgnore
    public boolean getCalendar_last_notif_ackDirtyFlag(){
        return this.calendar_last_notif_ackDirtyFlag ;
    }

    /**
     * 获取 [操作次数]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [操作次数]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [操作次数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return this.activity_date_deadline ;
    }

    /**
     * 设置 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return this.activity_date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [员工]
     */
    @JsonProperty("employee")
    public String getEmployee(){
        return this.employee ;
    }

    /**
     * 设置 [员工]
     */
    @JsonProperty("employee")
    public void setEmployee(String  employee){
        this.employee = employee ;
        this.employeeDirtyFlag = true ;
    }

    /**
     * 获取 [员工]脏标记
     */
    @JsonIgnore
    public boolean getEmployeeDirtyFlag(){
        return this.employeeDirtyFlag ;
    }

    /**
     * 获取 [活动状态]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return this.activity_state ;
    }

    /**
     * 设置 [活动状态]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

    /**
     * 获取 [活动状态]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return this.activity_stateDirtyFlag ;
    }

    /**
     * 获取 [条码]
     */
    @JsonProperty("barcode")
    public String getBarcode(){
        return this.barcode ;
    }

    /**
     * 设置 [条码]
     */
    @JsonProperty("barcode")
    public void setBarcode(String  barcode){
        this.barcode = barcode ;
        this.barcodeDirtyFlag = true ;
    }

    /**
     * 获取 [条码]脏标记
     */
    @JsonIgnore
    public boolean getBarcodeDirtyFlag(){
        return this.barcodeDirtyFlag ;
    }

    /**
     * 获取 [公司数据库ID]
     */
    @JsonProperty("partner_gid")
    public Integer getPartner_gid(){
        return this.partner_gid ;
    }

    /**
     * 设置 [公司数据库ID]
     */
    @JsonProperty("partner_gid")
    public void setPartner_gid(Integer  partner_gid){
        this.partner_gid = partner_gid ;
        this.partner_gidDirtyFlag = true ;
    }

    /**
     * 获取 [公司数据库ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_gidDirtyFlag(){
        return this.partner_gidDirtyFlag ;
    }

    /**
     * 获取 [地址类型]
     */
    @JsonProperty("type")
    public String getType(){
        return this.type ;
    }

    /**
     * 设置 [地址类型]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

    /**
     * 获取 [地址类型]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return this.typeDirtyFlag ;
    }

    /**
     * 获取 [税号]
     */
    @JsonProperty("vat")
    public String getVat(){
        return this.vat ;
    }

    /**
     * 设置 [税号]
     */
    @JsonProperty("vat")
    public void setVat(String  vat){
        this.vat = vat ;
        this.vatDirtyFlag = true ;
    }

    /**
     * 获取 [税号]脏标记
     */
    @JsonIgnore
    public boolean getVatDirtyFlag(){
        return this.vatDirtyFlag ;
    }

    /**
     * 获取 [采购订单消息]
     */
    @JsonProperty("purchase_warn_msg")
    public String getPurchase_warn_msg(){
        return this.purchase_warn_msg ;
    }

    /**
     * 设置 [采购订单消息]
     */
    @JsonProperty("purchase_warn_msg")
    public void setPurchase_warn_msg(String  purchase_warn_msg){
        this.purchase_warn_msg = purchase_warn_msg ;
        this.purchase_warn_msgDirtyFlag = true ;
    }

    /**
     * 获取 [采购订单消息]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_warn_msgDirtyFlag(){
        return this.purchase_warn_msgDirtyFlag ;
    }

    /**
     * 获取 [便签]
     */
    @JsonProperty("comment")
    public String getComment(){
        return this.comment ;
    }

    /**
     * 设置 [便签]
     */
    @JsonProperty("comment")
    public void setComment(String  comment){
        this.comment = comment ;
        this.commentDirtyFlag = true ;
    }

    /**
     * 获取 [便签]脏标记
     */
    @JsonIgnore
    public boolean getCommentDirtyFlag(){
        return this.commentDirtyFlag ;
    }

    /**
     * 获取 [供应商]
     */
    @JsonProperty("supplier")
    public String getSupplier(){
        return this.supplier ;
    }

    /**
     * 设置 [供应商]
     */
    @JsonProperty("supplier")
    public void setSupplier(String  supplier){
        this.supplier = supplier ;
        this.supplierDirtyFlag = true ;
    }

    /**
     * 获取 [供应商]脏标记
     */
    @JsonIgnore
    public boolean getSupplierDirtyFlag(){
        return this.supplierDirtyFlag ;
    }

    /**
     * 获取 [网站meta关键词]
     */
    @JsonProperty("website_meta_keywords")
    public String getWebsite_meta_keywords(){
        return this.website_meta_keywords ;
    }

    /**
     * 设置 [网站meta关键词]
     */
    @JsonProperty("website_meta_keywords")
    public void setWebsite_meta_keywords(String  website_meta_keywords){
        this.website_meta_keywords = website_meta_keywords ;
        this.website_meta_keywordsDirtyFlag = true ;
    }

    /**
     * 获取 [网站meta关键词]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_keywordsDirtyFlag(){
        return this.website_meta_keywordsDirtyFlag ;
    }

    /**
     * 获取 [关联公司]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return this.parent_id ;
    }

    /**
     * 设置 [关联公司]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

    /**
     * 获取 [关联公司]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return this.parent_idDirtyFlag ;
    }

    /**
     * 获取 [采购订单]
     */
    @JsonProperty("purchase_warn")
    public String getPurchase_warn(){
        return this.purchase_warn ;
    }

    /**
     * 设置 [采购订单]
     */
    @JsonProperty("purchase_warn")
    public void setPurchase_warn(String  purchase_warn){
        this.purchase_warn = purchase_warn ;
        this.purchase_warnDirtyFlag = true ;
    }

    /**
     * 获取 [采购订单]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_warnDirtyFlag(){
        return this.purchase_warnDirtyFlag ;
    }

    /**
     * 获取 [激活的合作伙伴]
     */
    @JsonProperty("active_partner")
    public String getActive_partner(){
        return this.active_partner ;
    }

    /**
     * 设置 [激活的合作伙伴]
     */
    @JsonProperty("active_partner")
    public void setActive_partner(String  active_partner){
        this.active_partner = active_partner ;
        this.active_partnerDirtyFlag = true ;
    }

    /**
     * 获取 [激活的合作伙伴]脏标记
     */
    @JsonIgnore
    public boolean getActive_partnerDirtyFlag(){
        return this.active_partnerDirtyFlag ;
    }

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }

    /**
     * 获取 [工业]
     */
    @JsonProperty("industry_id")
    public Integer getIndustry_id(){
        return this.industry_id ;
    }

    /**
     * 设置 [工业]
     */
    @JsonProperty("industry_id")
    public void setIndustry_id(Integer  industry_id){
        this.industry_id = industry_id ;
        this.industry_idDirtyFlag = true ;
    }

    /**
     * 获取 [工业]脏标记
     */
    @JsonIgnore
    public boolean getIndustry_idDirtyFlag(){
        return this.industry_idDirtyFlag ;
    }

    /**
     * 获取 [供应商位置]
     */
    @JsonProperty("property_stock_supplier")
    public Integer getProperty_stock_supplier(){
        return this.property_stock_supplier ;
    }

    /**
     * 设置 [供应商位置]
     */
    @JsonProperty("property_stock_supplier")
    public void setProperty_stock_supplier(Integer  property_stock_supplier){
        this.property_stock_supplier = property_stock_supplier ;
        this.property_stock_supplierDirtyFlag = true ;
    }

    /**
     * 获取 [供应商位置]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_supplierDirtyFlag(){
        return this.property_stock_supplierDirtyFlag ;
    }

    /**
     * 获取 [付款令牌计数]
     */
    @JsonProperty("payment_token_count")
    public Integer getPayment_token_count(){
        return this.payment_token_count ;
    }

    /**
     * 设置 [付款令牌计数]
     */
    @JsonProperty("payment_token_count")
    public void setPayment_token_count(Integer  payment_token_count){
        this.payment_token_count = payment_token_count ;
        this.payment_token_countDirtyFlag = true ;
    }

    /**
     * 获取 [付款令牌计数]脏标记
     */
    @JsonIgnore
    public boolean getPayment_token_countDirtyFlag(){
        return this.payment_token_countDirtyFlag ;
    }

    /**
     * 获取 [前置操作]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [前置操作]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [前置操作]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }

    /**
     * 获取 [销售员]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [销售员]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [销售员]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }

    /**
     * 获取 [客户]
     */
    @JsonProperty("customer")
    public String getCustomer(){
        return this.customer ;
    }

    /**
     * 设置 [客户]
     */
    @JsonProperty("customer")
    public void setCustomer(String  customer){
        this.customer = customer ;
        this.customerDirtyFlag = true ;
    }

    /**
     * 获取 [客户]脏标记
     */
    @JsonIgnore
    public boolean getCustomerDirtyFlag(){
        return this.customerDirtyFlag ;
    }

    /**
     * 获取 [客户付款条款]
     */
    @JsonProperty("property_payment_term_id")
    public Integer getProperty_payment_term_id(){
        return this.property_payment_term_id ;
    }

    /**
     * 设置 [客户付款条款]
     */
    @JsonProperty("property_payment_term_id")
    public void setProperty_payment_term_id(Integer  property_payment_term_id){
        this.property_payment_term_id = property_payment_term_id ;
        this.property_payment_term_idDirtyFlag = true ;
    }

    /**
     * 获取 [客户付款条款]脏标记
     */
    @JsonIgnore
    public boolean getProperty_payment_term_idDirtyFlag(){
        return this.property_payment_term_idDirtyFlag ;
    }

    /**
     * 获取 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return this.activity_type_id ;
    }

    /**
     * 设置 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [下一活动类型]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return this.activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [合同统计]
     */
    @JsonProperty("contracts_count")
    public Integer getContracts_count(){
        return this.contracts_count ;
    }

    /**
     * 设置 [合同统计]
     */
    @JsonProperty("contracts_count")
    public void setContracts_count(Integer  contracts_count){
        this.contracts_count = contracts_count ;
        this.contracts_countDirtyFlag = true ;
    }

    /**
     * 获取 [合同统计]脏标记
     */
    @JsonIgnore
    public boolean getContracts_countDirtyFlag(){
        return this.contracts_countDirtyFlag ;
    }

    /**
     * 获取 [自己]
     */
    @JsonProperty("self")
    public Integer getSelf(){
        return this.self ;
    }

    /**
     * 设置 [自己]
     */
    @JsonProperty("self")
    public void setSelf(Integer  self){
        this.self = self ;
        this.selfDirtyFlag = true ;
    }

    /**
     * 获取 [自己]脏标记
     */
    @JsonIgnore
    public boolean getSelfDirtyFlag(){
        return this.selfDirtyFlag ;
    }

    /**
     * 获取 [网站元说明]
     */
    @JsonProperty("website_meta_description")
    public String getWebsite_meta_description(){
        return this.website_meta_description ;
    }

    /**
     * 设置 [网站元说明]
     */
    @JsonProperty("website_meta_description")
    public void setWebsite_meta_description(String  website_meta_description){
        this.website_meta_description = website_meta_description ;
        this.website_meta_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [网站元说明]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_descriptionDirtyFlag(){
        return this.website_meta_descriptionDirtyFlag ;
    }

    /**
     * 获取 [图像]
     */
    @JsonProperty("image")
    public byte[] getImage(){
        return this.image ;
    }

    /**
     * 设置 [图像]
     */
    @JsonProperty("image")
    public void setImage(byte[]  image){
        this.image = image ;
        this.imageDirtyFlag = true ;
    }

    /**
     * 获取 [图像]脏标记
     */
    @JsonIgnore
    public boolean getImageDirtyFlag(){
        return this.imageDirtyFlag ;
    }

    /**
     * 获取 [EMail]
     */
    @JsonProperty("email")
    public String getEmail(){
        return this.email ;
    }

    /**
     * 设置 [EMail]
     */
    @JsonProperty("email")
    public void setEmail(String  email){
        this.email = email ;
        this.emailDirtyFlag = true ;
    }

    /**
     * 获取 [EMail]脏标记
     */
    @JsonIgnore
    public boolean getEmailDirtyFlag(){
        return this.emailDirtyFlag ;
    }

    /**
     * 获取 [中等尺寸图像]
     */
    @JsonProperty("image_medium")
    public byte[] getImage_medium(){
        return this.image_medium ;
    }

    /**
     * 设置 [中等尺寸图像]
     */
    @JsonProperty("image_medium")
    public void setImage_medium(byte[]  image_medium){
        this.image_medium = image_medium ;
        this.image_mediumDirtyFlag = true ;
    }

    /**
     * 获取 [中等尺寸图像]脏标记
     */
    @JsonIgnore
    public boolean getImage_mediumDirtyFlag(){
        return this.image_mediumDirtyFlag ;
    }

    /**
     * 获取 [下一个活动摘要]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return this.activity_summary ;
    }

    /**
     * 设置 [下一个活动摘要]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

    /**
     * 获取 [下一个活动摘要]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return this.activity_summaryDirtyFlag ;
    }

    /**
     * 获取 [应付限额]
     */
    @JsonProperty("debit_limit")
    public Double getDebit_limit(){
        return this.debit_limit ;
    }

    /**
     * 设置 [应付限额]
     */
    @JsonProperty("debit_limit")
    public void setDebit_limit(Double  debit_limit){
        this.debit_limit = debit_limit ;
        this.debit_limitDirtyFlag = true ;
    }

    /**
     * 获取 [应付限额]脏标记
     */
    @JsonIgnore
    public boolean getDebit_limitDirtyFlag(){
        return this.debit_limitDirtyFlag ;
    }

    /**
     * 获取 [国家/地区]
     */
    @JsonProperty("country_id")
    public Integer getCountry_id(){
        return this.country_id ;
    }

    /**
     * 设置 [国家/地区]
     */
    @JsonProperty("country_id")
    public void setCountry_id(Integer  country_id){
        this.country_id = country_id ;
        this.country_idDirtyFlag = true ;
    }

    /**
     * 获取 [国家/地区]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idDirtyFlag(){
        return this.country_idDirtyFlag ;
    }

    /**
     * 获取 [信用额度]
     */
    @JsonProperty("credit_limit")
    public Double getCredit_limit(){
        return this.credit_limit ;
    }

    /**
     * 设置 [信用额度]
     */
    @JsonProperty("credit_limit")
    public void setCredit_limit(Double  credit_limit){
        this.credit_limit = credit_limit ;
        this.credit_limitDirtyFlag = true ;
    }

    /**
     * 获取 [信用额度]脏标记
     */
    @JsonIgnore
    public boolean getCredit_limitDirtyFlag(){
        return this.credit_limitDirtyFlag ;
    }

    /**
     * 获取 [公司名称实体]
     */
    @JsonProperty("commercial_company_name")
    public String getCommercial_company_name(){
        return this.commercial_company_name ;
    }

    /**
     * 设置 [公司名称实体]
     */
    @JsonProperty("commercial_company_name")
    public void setCommercial_company_name(String  commercial_company_name){
        this.commercial_company_name = commercial_company_name ;
        this.commercial_company_nameDirtyFlag = true ;
    }

    /**
     * 获取 [公司名称实体]脏标记
     */
    @JsonIgnore
    public boolean getCommercial_company_nameDirtyFlag(){
        return this.commercial_company_nameDirtyFlag ;
    }

    /**
     * 获取 [发票消息]
     */
    @JsonProperty("invoice_warn_msg")
    public String getInvoice_warn_msg(){
        return this.invoice_warn_msg ;
    }

    /**
     * 设置 [发票消息]
     */
    @JsonProperty("invoice_warn_msg")
    public void setInvoice_warn_msg(String  invoice_warn_msg){
        this.invoice_warn_msg = invoice_warn_msg ;
        this.invoice_warn_msgDirtyFlag = true ;
    }

    /**
     * 获取 [发票消息]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_warn_msgDirtyFlag(){
        return this.invoice_warn_msgDirtyFlag ;
    }

    /**
     * 获取 [已发布]
     */
    @JsonProperty("is_published")
    public String getIs_published(){
        return this.is_published ;
    }

    /**
     * 设置 [已发布]
     */
    @JsonProperty("is_published")
    public void setIs_published(String  is_published){
        this.is_published = is_published ;
        this.is_publishedDirtyFlag = true ;
    }

    /**
     * 获取 [已发布]脏标记
     */
    @JsonIgnore
    public boolean getIs_publishedDirtyFlag(){
        return this.is_publishedDirtyFlag ;
    }

    /**
     * 获取 [对此债务人的信任度]
     */
    @JsonProperty("trust")
    public String getTrust(){
        return this.trust ;
    }

    /**
     * 设置 [对此债务人的信任度]
     */
    @JsonProperty("trust")
    public void setTrust(String  trust){
        this.trust = trust ;
        this.trustDirtyFlag = true ;
    }

    /**
     * 获取 [对此债务人的信任度]脏标记
     */
    @JsonIgnore
    public boolean getTrustDirtyFlag(){
        return this.trustDirtyFlag ;
    }

    /**
     * 获取 [手机]
     */
    @JsonProperty("mobile")
    public String getMobile(){
        return this.mobile ;
    }

    /**
     * 设置 [手机]
     */
    @JsonProperty("mobile")
    public void setMobile(String  mobile){
        this.mobile = mobile ;
        this.mobileDirtyFlag = true ;
    }

    /**
     * 获取 [手机]脏标记
     */
    @JsonIgnore
    public boolean getMobileDirtyFlag(){
        return this.mobileDirtyFlag ;
    }

    /**
     * 获取 [格式化的邮件]
     */
    @JsonProperty("email_formatted")
    public String getEmail_formatted(){
        return this.email_formatted ;
    }

    /**
     * 设置 [格式化的邮件]
     */
    @JsonProperty("email_formatted")
    public void setEmail_formatted(String  email_formatted){
        this.email_formatted = email_formatted ;
        this.email_formattedDirtyFlag = true ;
    }

    /**
     * 获取 [格式化的邮件]脏标记
     */
    @JsonIgnore
    public boolean getEmail_formattedDirtyFlag(){
        return this.email_formattedDirtyFlag ;
    }

    /**
     * 获取 [公司]
     */
    @JsonProperty("is_company")
    public String getIs_company(){
        return this.is_company ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("is_company")
    public void setIs_company(String  is_company){
        this.is_company = is_company ;
        this.is_companyDirtyFlag = true ;
    }

    /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getIs_companyDirtyFlag(){
        return this.is_companyDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [销售团队]
     */
    @JsonProperty("team_id")
    public Integer getTeam_id(){
        return this.team_id ;
    }

    /**
     * 设置 [销售团队]
     */
    @JsonProperty("team_id")
    public void setTeam_id(Integer  team_id){
        this.team_id = team_id ;
        this.team_idDirtyFlag = true ;
    }

    /**
     * 获取 [销售团队]脏标记
     */
    @JsonIgnore
    public boolean getTeam_idDirtyFlag(){
        return this.team_idDirtyFlag ;
    }

    /**
     * 获取 [黑名单]
     */
    @JsonProperty("is_blacklisted")
    public String getIs_blacklisted(){
        return this.is_blacklisted ;
    }

    /**
     * 设置 [黑名单]
     */
    @JsonProperty("is_blacklisted")
    public void setIs_blacklisted(String  is_blacklisted){
        this.is_blacklisted = is_blacklisted ;
        this.is_blacklistedDirtyFlag = true ;
    }

    /**
     * 获取 [黑名单]脏标记
     */
    @JsonIgnore
    public boolean getIs_blacklistedDirtyFlag(){
        return this.is_blacklistedDirtyFlag ;
    }

    /**
     * 获取 [银行]
     */
    @JsonProperty("bank_account_count")
    public Integer getBank_account_count(){
        return this.bank_account_count ;
    }

    /**
     * 设置 [银行]
     */
    @JsonProperty("bank_account_count")
    public void setBank_account_count(Integer  bank_account_count){
        this.bank_account_count = bank_account_count ;
        this.bank_account_countDirtyFlag = true ;
    }

    /**
     * 获取 [银行]脏标记
     */
    @JsonIgnore
    public boolean getBank_account_countDirtyFlag(){
        return this.bank_account_countDirtyFlag ;
    }

    /**
     * 获取 [价格表]
     */
    @JsonProperty("property_product_pricelist")
    public Integer getProperty_product_pricelist(){
        return this.property_product_pricelist ;
    }

    /**
     * 设置 [价格表]
     */
    @JsonProperty("property_product_pricelist")
    public void setProperty_product_pricelist(Integer  property_product_pricelist){
        this.property_product_pricelist = property_product_pricelist ;
        this.property_product_pricelistDirtyFlag = true ;
    }

    /**
     * 获取 [价格表]脏标记
     */
    @JsonIgnore
    public boolean getProperty_product_pricelistDirtyFlag(){
        return this.property_product_pricelistDirtyFlag ;
    }

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }

    /**
     * 获取 [库存拣货单消息]
     */
    @JsonProperty("picking_warn_msg")
    public String getPicking_warn_msg(){
        return this.picking_warn_msg ;
    }

    /**
     * 设置 [库存拣货单消息]
     */
    @JsonProperty("picking_warn_msg")
    public void setPicking_warn_msg(String  picking_warn_msg){
        this.picking_warn_msg = picking_warn_msg ;
        this.picking_warn_msgDirtyFlag = true ;
    }

    /**
     * 获取 [库存拣货单消息]脏标记
     */
    @JsonIgnore
    public boolean getPicking_warn_msgDirtyFlag(){
        return this.picking_warn_msgDirtyFlag ;
    }

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [网站]
     */
    @JsonProperty("website")
    public String getWebsite(){
        return this.website ;
    }

    /**
     * 设置 [网站]
     */
    @JsonProperty("website")
    public void setWebsite(String  website){
        this.website = website ;
        this.websiteDirtyFlag = true ;
    }

    /**
     * 获取 [网站]脏标记
     */
    @JsonIgnore
    public boolean getWebsiteDirtyFlag(){
        return this.websiteDirtyFlag ;
    }

    /**
     * 获取 [电话]
     */
    @JsonProperty("phone")
    public String getPhone(){
        return this.phone ;
    }

    /**
     * 设置 [电话]
     */
    @JsonProperty("phone")
    public void setPhone(String  phone){
        this.phone = phone ;
        this.phoneDirtyFlag = true ;
    }

    /**
     * 获取 [电话]脏标记
     */
    @JsonIgnore
    public boolean getPhoneDirtyFlag(){
        return this.phoneDirtyFlag ;
    }

    /**
     * 获取 [街道 2]
     */
    @JsonProperty("street2")
    public String getStreet2(){
        return this.street2 ;
    }

    /**
     * 设置 [街道 2]
     */
    @JsonProperty("street2")
    public void setStreet2(String  street2){
        this.street2 = street2 ;
        this.street2DirtyFlag = true ;
    }

    /**
     * 获取 [街道 2]脏标记
     */
    @JsonIgnore
    public boolean getStreet2DirtyFlag(){
        return this.street2DirtyFlag ;
    }

    /**
     * 获取 [有未核销的分录]
     */
    @JsonProperty("has_unreconciled_entries")
    public String getHas_unreconciled_entries(){
        return this.has_unreconciled_entries ;
    }

    /**
     * 设置 [有未核销的分录]
     */
    @JsonProperty("has_unreconciled_entries")
    public void setHas_unreconciled_entries(String  has_unreconciled_entries){
        this.has_unreconciled_entries = has_unreconciled_entries ;
        this.has_unreconciled_entriesDirtyFlag = true ;
    }

    /**
     * 获取 [有未核销的分录]脏标记
     */
    @JsonIgnore
    public boolean getHas_unreconciled_entriesDirtyFlag(){
        return this.has_unreconciled_entriesDirtyFlag ;
    }

    /**
     * 获取 [注册到期]
     */
    @JsonProperty("signup_expiration")
    public Timestamp getSignup_expiration(){
        return this.signup_expiration ;
    }

    /**
     * 设置 [注册到期]
     */
    @JsonProperty("signup_expiration")
    public void setSignup_expiration(Timestamp  signup_expiration){
        this.signup_expiration = signup_expiration ;
        this.signup_expirationDirtyFlag = true ;
    }

    /**
     * 获取 [注册到期]脏标记
     */
    @JsonIgnore
    public boolean getSignup_expirationDirtyFlag(){
        return this.signup_expirationDirtyFlag ;
    }

    /**
     * 获取 [时区]
     */
    @JsonProperty("tz")
    public String getTz(){
        return this.tz ;
    }

    /**
     * 设置 [时区]
     */
    @JsonProperty("tz")
    public void setTz(String  tz){
        this.tz = tz ;
        this.tzDirtyFlag = true ;
    }

    /**
     * 获取 [时区]脏标记
     */
    @JsonIgnore
    public boolean getTzDirtyFlag(){
        return this.tzDirtyFlag ;
    }

    /**
     * 获取 [完整地址]
     */
    @JsonProperty("contact_address")
    public String getContact_address(){
        return this.contact_address ;
    }

    /**
     * 设置 [完整地址]
     */
    @JsonProperty("contact_address")
    public void setContact_address(String  contact_address){
        this.contact_address = contact_address ;
        this.contact_addressDirtyFlag = true ;
    }

    /**
     * 获取 [完整地址]脏标记
     */
    @JsonIgnore
    public boolean getContact_addressDirtyFlag(){
        return this.contact_addressDirtyFlag ;
    }

    /**
     * 获取 [网站业务伙伴简介]
     */
    @JsonProperty("website_short_description")
    public String getWebsite_short_description(){
        return this.website_short_description ;
    }

    /**
     * 设置 [网站业务伙伴简介]
     */
    @JsonProperty("website_short_description")
    public void setWebsite_short_description(String  website_short_description){
        this.website_short_description = website_short_description ;
        this.website_short_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [网站业务伙伴简介]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_short_descriptionDirtyFlag(){
        return this.website_short_descriptionDirtyFlag ;
    }

    /**
     * 获取 [共享合作伙伴]
     */
    @JsonProperty("partner_share")
    public String getPartner_share(){
        return this.partner_share ;
    }

    /**
     * 设置 [共享合作伙伴]
     */
    @JsonProperty("partner_share")
    public void setPartner_share(String  partner_share){
        this.partner_share = partner_share ;
        this.partner_shareDirtyFlag = true ;
    }

    /**
     * 获取 [共享合作伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_shareDirtyFlag(){
        return this.partner_shareDirtyFlag ;
    }

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }

    /**
     * 获取 [颜色索引]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return this.color ;
    }

    /**
     * 设置 [颜色索引]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [颜色索引]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return this.colorDirtyFlag ;
    }

    /**
     * 获取 [邮政编码]
     */
    @JsonProperty("zip")
    public String getZip(){
        return this.zip ;
    }

    /**
     * 设置 [邮政编码]
     */
    @JsonProperty("zip")
    public void setZip(String  zip){
        this.zip = zip ;
        this.zipDirtyFlag = true ;
    }

    /**
     * 获取 [邮政编码]脏标记
     */
    @JsonIgnore
    public boolean getZipDirtyFlag(){
        return this.zipDirtyFlag ;
    }

    /**
     * 获取 [销售订单个数]
     */
    @JsonProperty("sale_order_count")
    public Integer getSale_order_count(){
        return this.sale_order_count ;
    }

    /**
     * 设置 [销售订单个数]
     */
    @JsonProperty("sale_order_count")
    public void setSale_order_count(Integer  sale_order_count){
        this.sale_order_count = sale_order_count ;
        this.sale_order_countDirtyFlag = true ;
    }

    /**
     * 获取 [销售订单个数]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_countDirtyFlag(){
        return this.sale_order_countDirtyFlag ;
    }

    /**
     * 获取 [公司类别]
     */
    @JsonProperty("company_type")
    public String getCompany_type(){
        return this.company_type ;
    }

    /**
     * 设置 [公司类别]
     */
    @JsonProperty("company_type")
    public void setCompany_type(String  company_type){
        this.company_type = company_type ;
        this.company_typeDirtyFlag = true ;
    }

    /**
     * 获取 [公司类别]脏标记
     */
    @JsonIgnore
    public boolean getCompany_typeDirtyFlag(){
        return this.company_typeDirtyFlag ;
    }

    /**
     * 获取 [税科目调整]
     */
    @JsonProperty("property_account_position_id")
    public Integer getProperty_account_position_id(){
        return this.property_account_position_id ;
    }

    /**
     * 设置 [税科目调整]
     */
    @JsonProperty("property_account_position_id")
    public void setProperty_account_position_id(Integer  property_account_position_id){
        this.property_account_position_id = property_account_position_id ;
        this.property_account_position_idDirtyFlag = true ;
    }

    /**
     * 获取 [税科目调整]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_position_idDirtyFlag(){
        return this.property_account_position_idDirtyFlag ;
    }

    /**
     * 获取 [SEO优化]
     */
    @JsonProperty("is_seo_optimized")
    public String getIs_seo_optimized(){
        return this.is_seo_optimized ;
    }

    /**
     * 设置 [SEO优化]
     */
    @JsonProperty("is_seo_optimized")
    public void setIs_seo_optimized(String  is_seo_optimized){
        this.is_seo_optimized = is_seo_optimized ;
        this.is_seo_optimizedDirtyFlag = true ;
    }

    /**
     * 获取 [SEO优化]脏标记
     */
    @JsonIgnore
    public boolean getIs_seo_optimizedDirtyFlag(){
        return this.is_seo_optimizedDirtyFlag ;
    }

    /**
     * 获取 [退回]
     */
    @JsonProperty("message_bounce")
    public Integer getMessage_bounce(){
        return this.message_bounce ;
    }

    /**
     * 设置 [退回]
     */
    @JsonProperty("message_bounce")
    public void setMessage_bounce(Integer  message_bounce){
        this.message_bounce = message_bounce ;
        this.message_bounceDirtyFlag = true ;
    }

    /**
     * 获取 [退回]脏标记
     */
    @JsonIgnore
    public boolean getMessage_bounceDirtyFlag(){
        return this.message_bounceDirtyFlag ;
    }

    /**
     * 获取 [网站meta标题]
     */
    @JsonProperty("website_meta_title")
    public String getWebsite_meta_title(){
        return this.website_meta_title ;
    }

    /**
     * 设置 [网站meta标题]
     */
    @JsonProperty("website_meta_title")
    public void setWebsite_meta_title(String  website_meta_title){
        this.website_meta_title = website_meta_title ;
        this.website_meta_titleDirtyFlag = true ;
    }

    /**
     * 获取 [网站meta标题]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_titleDirtyFlag(){
        return this.website_meta_titleDirtyFlag ;
    }

    /**
     * 获取 [小尺寸图像]
     */
    @JsonProperty("image_small")
    public byte[] getImage_small(){
        return this.image_small ;
    }

    /**
     * 设置 [小尺寸图像]
     */
    @JsonProperty("image_small")
    public void setImage_small(byte[]  image_small){
        this.image_small = image_small ;
        this.image_smallDirtyFlag = true ;
    }

    /**
     * 获取 [小尺寸图像]脏标记
     */
    @JsonIgnore
    public boolean getImage_smallDirtyFlag(){
        return this.image_smallDirtyFlag ;
    }

    /**
     * 获取 [供应商货币]
     */
    @JsonProperty("property_purchase_currency_id")
    public Integer getProperty_purchase_currency_id(){
        return this.property_purchase_currency_id ;
    }

    /**
     * 设置 [供应商货币]
     */
    @JsonProperty("property_purchase_currency_id")
    public void setProperty_purchase_currency_id(Integer  property_purchase_currency_id){
        this.property_purchase_currency_id = property_purchase_currency_id ;
        this.property_purchase_currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [供应商货币]脏标记
     */
    @JsonIgnore
    public boolean getProperty_purchase_currency_idDirtyFlag(){
        return this.property_purchase_currency_idDirtyFlag ;
    }

    /**
     * 获取 [采购订单数]
     */
    @JsonProperty("purchase_order_count")
    public Integer getPurchase_order_count(){
        return this.purchase_order_count ;
    }

    /**
     * 设置 [采购订单数]
     */
    @JsonProperty("purchase_order_count")
    public void setPurchase_order_count(Integer  purchase_order_count){
        this.purchase_order_count = purchase_order_count ;
        this.purchase_order_countDirtyFlag = true ;
    }

    /**
     * 获取 [采购订单数]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_order_countDirtyFlag(){
        return this.purchase_order_countDirtyFlag ;
    }

    /**
     * 获取 [网站业务伙伴的详细说明]
     */
    @JsonProperty("website_description")
    public String getWebsite_description(){
        return this.website_description ;
    }

    /**
     * 设置 [网站业务伙伴的详细说明]
     */
    @JsonProperty("website_description")
    public void setWebsite_description(String  website_description){
        this.website_description = website_description ;
        this.website_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [网站业务伙伴的详细说明]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_descriptionDirtyFlag(){
        return this.website_descriptionDirtyFlag ;
    }

    /**
     * 获取 [供应商付款条款]
     */
    @JsonProperty("property_supplier_payment_term_id")
    public Integer getProperty_supplier_payment_term_id(){
        return this.property_supplier_payment_term_id ;
    }

    /**
     * 设置 [供应商付款条款]
     */
    @JsonProperty("property_supplier_payment_term_id")
    public void setProperty_supplier_payment_term_id(Integer  property_supplier_payment_term_id){
        this.property_supplier_payment_term_id = property_supplier_payment_term_id ;
        this.property_supplier_payment_term_idDirtyFlag = true ;
    }

    /**
     * 获取 [供应商付款条款]脏标记
     */
    @JsonIgnore
    public boolean getProperty_supplier_payment_term_idDirtyFlag(){
        return this.property_supplier_payment_term_idDirtyFlag ;
    }

    /**
     * 获取 [商业实体]
     */
    @JsonProperty("commercial_partner_id")
    public Integer getCommercial_partner_id(){
        return this.commercial_partner_id ;
    }

    /**
     * 设置 [商业实体]
     */
    @JsonProperty("commercial_partner_id")
    public void setCommercial_partner_id(Integer  commercial_partner_id){
        this.commercial_partner_id = commercial_partner_id ;
        this.commercial_partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [商业实体]脏标记
     */
    @JsonIgnore
    public boolean getCommercial_partner_idDirtyFlag(){
        return this.commercial_partner_idDirtyFlag ;
    }

    /**
     * 获取 [安全联系人别名]
     */
    @JsonProperty("alias_contact")
    public String getAlias_contact(){
        return this.alias_contact ;
    }

    /**
     * 设置 [安全联系人别名]
     */
    @JsonProperty("alias_contact")
    public void setAlias_contact(String  alias_contact){
        this.alias_contact = alias_contact ;
        this.alias_contactDirtyFlag = true ;
    }

    /**
     * 获取 [安全联系人别名]脏标记
     */
    @JsonIgnore
    public boolean getAlias_contactDirtyFlag(){
        return this.alias_contactDirtyFlag ;
    }

    /**
     * 获取 [公司名称]
     */
    @JsonProperty("company_name")
    public String getCompany_name(){
        return this.company_name ;
    }

    /**
     * 设置 [公司名称]
     */
    @JsonProperty("company_name")
    public void setCompany_name(String  company_name){
        this.company_name = company_name ;
        this.company_nameDirtyFlag = true ;
    }

    /**
     * 获取 [公司名称]脏标记
     */
    @JsonIgnore
    public boolean getCompany_nameDirtyFlag(){
        return this.company_nameDirtyFlag ;
    }

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }

    /**
     * 获取 [相关的业务伙伴]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [相关的业务伙伴]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [相关的业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }

    /**
     * 获取 [别名]
     */
    @JsonProperty("alias_id")
    public Integer getAlias_id(){
        return this.alias_id ;
    }

    /**
     * 设置 [别名]
     */
    @JsonProperty("alias_id")
    public void setAlias_id(Integer  alias_id){
        this.alias_id = alias_id ;
        this.alias_idDirtyFlag = true ;
    }

    /**
     * 获取 [别名]脏标记
     */
    @JsonIgnore
    public boolean getAlias_idDirtyFlag(){
        return this.alias_idDirtyFlag ;
    }

    /**
     * 获取 [用户的销售团队]
     */
    @JsonProperty("sale_team_id")
    public Integer getSale_team_id(){
        return this.sale_team_id ;
    }

    /**
     * 设置 [用户的销售团队]
     */
    @JsonProperty("sale_team_id")
    public void setSale_team_id(Integer  sale_team_id){
        this.sale_team_id = sale_team_id ;
        this.sale_team_idDirtyFlag = true ;
    }

    /**
     * 获取 [用户的销售团队]脏标记
     */
    @JsonIgnore
    public boolean getSale_team_idDirtyFlag(){
        return this.sale_team_idDirtyFlag ;
    }

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }



}
