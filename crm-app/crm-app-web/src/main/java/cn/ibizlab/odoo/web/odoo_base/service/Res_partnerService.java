package cn.ibizlab.odoo.web.odoo_base.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.web.odoo_base.domain.Res_partner;
import cn.ibizlab.odoo.web.odoo_base.filter.*;
import cn.ibizlab.odoo.web.odoo_base.feign.Res_partnerFeignClient;
import cn.ibizlab.odoo.web.WebApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Res_partnerService {

    Res_partnerFeignClient client;

    @Autowired
    public Res_partnerService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Res_partnerFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Res_partnerFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public boolean remove( Integer res_partner_id) {
        return client.remove( res_partner_id);
    }

    public Res_partner get( Integer res_partner_id) {
        return client.get( res_partner_id);
    }

    public boolean checkKey(Res_partner res_partner) {
        return client.checkKey(res_partner);
    }

    public Res_partner save(Integer res_partner_id, Res_partner res_partner) {
        return client.save(res_partner_id, res_partner);
    }

    public Res_partner update(Integer res_partner_id, Res_partner res_partner) {
        return client.update(res_partner_id, res_partner);
    }

    public Res_partner createBatch(Integer res_partner_id, Res_partner res_partner) {
        return client.createBatch(res_partner_id, res_partner);
    }

    public Res_partner updateBatch(Integer res_partner_id, Res_partner res_partner) {
        return client.updateBatch(res_partner_id, res_partner);
    }

    public Res_partner getDraft(Integer res_partner_id, Res_partner res_partner) {
        return client.getDraft(res_partner_id, res_partner);
    }

	public Res_partner create(Res_partner res_partner) {
        return client.create(res_partner);
    }

    public Res_partner removeBatch(Integer res_partner_id, Res_partner res_partner) {
        return client.removeBatch(res_partner_id, res_partner);
    }

	public Page<Res_partner> fetchContacts(Res_partnerSearchContext searchContext) {
        return client.fetchContacts(searchContext);
    }

	public Page<Res_partner> fetchCompany(Res_partnerSearchContext searchContext) {
        return client.fetchCompany(searchContext);
    }

	public Page<Res_partner> fetchDefault(Res_partnerSearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }

}
