package cn.ibizlab.odoo.web.security.permission.odoo_crm;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;

import cn.ibizlab.odoo.web.odoo_crm.service.Crm_leadService;
import cn.ibizlab.odoo.web.odoo_crm.domain.Crm_lead;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Component("crm_lead_pms")
public class Crm_leadPermission {

    @Autowired
    Crm_leadService crm_leadservice;

    public boolean check(String action){
        return true ;
    }

    public boolean check(Integer crm_lead_id, String action){
        return true ;
    }

    public boolean checkByRes_partner(Integer res_partner_id,  Integer crm_lead_id, String action){
        return true ;
    }

    public boolean checkByRes_partner(Integer res_partner_id,  String action){
        return true ;
    }


}
