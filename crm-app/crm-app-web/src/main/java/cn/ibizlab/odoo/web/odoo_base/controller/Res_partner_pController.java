package cn.ibizlab.odoo.web.odoo_base.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.web.odoo_base.service.Res_partner_pService;
import cn.ibizlab.odoo.web.odoo_base.domain.Res_partner_p;
import cn.ibizlab.odoo.web.odoo_base.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Res_partner_pController {
	@Autowired
    Res_partner_pService res_partner_pservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_ps/{res_partner_p_id}")
    @PreAuthorize("@res_partner_p_pms.check(#res_partner_p_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("res_partner_p_id") Integer res_partner_p_id) {
        boolean b = res_partner_pservice.remove( res_partner_p_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/res_partner_ps/{res_partner_p_id}")
    @PreAuthorize("@res_partner_p_pms.check(#res_partner_p_id,'READ')")
    public ResponseEntity<Res_partner_p> get(@PathVariable("res_partner_p_id") Integer res_partner_p_id) {
        Res_partner_p res_partner_p = res_partner_pservice.get( res_partner_p_id);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_p);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_ps/checkkey")
    @PreAuthorize("@res_partner_p_pms.check('')")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_partner_p res_partner_p) {
        boolean b = res_partner_pservice.checkKey(res_partner_p);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_ps/{res_partner_p_id}/save")
    @PreAuthorize("@res_partner_p_pms.check(#res_partner_p_id,'')")
    public ResponseEntity<Res_partner_p> save(@PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_p res_partner_p) {
        Res_partner_p res_partner_p2 = res_partner_pservice.save(res_partner_p_id, res_partner_p);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_p2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partner_ps/{res_partner_p_id}")
    @PreAuthorize("@res_partner_p_pms.check(#res_partner_p_id,'UPDATE')")
    public ResponseEntity<Res_partner_p> update(@PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_p res_partner_p) {
        Res_partner_p res_partner_p2 = res_partner_pservice.update(res_partner_p_id, res_partner_p);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_p2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/res_partner_ps/{res_partner_p_id}")
    @PreAuthorize("@res_partner_p_pms.check(#res_partner_p_id,'UPDATE')")
    public ResponseEntity<Res_partner_p> api_update(@PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_p res_partner_p) {
        Res_partner_p res_partner_p2 = res_partner_pservice.update(res_partner_p_id, res_partner_p);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_p2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_ps/{res_partner_p_id}/createbatch")
    @PreAuthorize("@res_partner_p_pms.check(#res_partner_p_id,'CREATE')")
    public ResponseEntity<Res_partner_p> createBatch(@PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_p res_partner_p) {
        Res_partner_p res_partner_p2 = res_partner_pservice.createBatch(res_partner_p_id, res_partner_p);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_p2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partner_ps/{res_partner_p_id}/updatebatch")
    @PreAuthorize("@res_partner_p_pms.check(#res_partner_p_id,'UPDATE')")
    public ResponseEntity<Res_partner_p> updateBatch(@PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_p res_partner_p) {
        Res_partner_p res_partner_p2 = res_partner_pservice.updateBatch(res_partner_p_id, res_partner_p);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_p2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/res_partner_ps/getdraft")
    @PreAuthorize("@res_partner_p_pms.check('CREATE')")
    public ResponseEntity<Res_partner_p> getDraft() {
        //Res_partner_p res_partner_p = res_partner_pservice.getDraft( res_partner_p_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Res_partner_p());
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_ps")
    @PreAuthorize("@res_partner_p_pms.check('CREATE')")
    public ResponseEntity<Res_partner_p> create(@RequestBody Res_partner_p res_partner_p) {
        Res_partner_p res_partner_p2 = res_partner_pservice.create(res_partner_p);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_p2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_ps/{res_partner_p_id}/removebatch")
    @PreAuthorize("@res_partner_p_pms.check(#res_partner_p_id,'DELETE')")
    public ResponseEntity<Res_partner_p> removeBatch(@PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_p res_partner_p) {
        Res_partner_p res_partner_p2 = res_partner_pservice.removeBatch(res_partner_p_id, res_partner_p);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_p2);
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/res_partner_ps/fetchcontacts")
    @PreAuthorize("@res_partner_p_pms.check('READ')")
	public ResponseEntity<List<Res_partner_p>> fetchContacts(Res_partner_pSearchContext searchContext,Pageable pageable) {
        
        Page<Res_partner_p> page = res_partner_pservice.fetchContacts(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/res_partner_ps/fetchcompany")
    @PreAuthorize("@res_partner_p_pms.check('READ')")
	public ResponseEntity<List<Res_partner_p>> fetchCompany(Res_partner_pSearchContext searchContext,Pageable pageable) {
        
        Page<Res_partner_p> page = res_partner_pservice.fetchCompany(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/res_partner_ps/fetchdefault")
    @PreAuthorize("@res_partner_p_pms.check('READ')")
	public ResponseEntity<List<Res_partner_p>> fetchDefault(Res_partner_pSearchContext searchContext,Pageable pageable) {
        
        Page<Res_partner_p> page = res_partner_pservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partners/{res_partner_id}/res_partner_ps/{res_partner_p_id}")
    @PreAuthorize("@res_partner_p_pms.checkByRes_partner(#res_partner_id,#res_partner_p_id,'DELETE')")
    public ResponseEntity<Boolean> removeByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("res_partner_p_id") Integer res_partner_p_id) {
        boolean b = res_partner_pservice.removeByRes_partner(res_partner_id,  res_partner_p_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

	@RequestMapping(method = RequestMethod.GET, value = "/res_partners/{res_partner_id}/res_partner_ps/{res_partner_p_id}")
    @PreAuthorize("@res_partner_p_pms.checkByRes_partner(#res_partner_id,#res_partner_p_id,'READ')")
    public ResponseEntity<Res_partner_p> getByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("res_partner_p_id") Integer res_partner_p_id) {
        Res_partner_p res_partner_p = res_partner_pservice.getByRes_partner(res_partner_id,  res_partner_p_id);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_p);
    }

	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/res_partner_ps/checkkey")
    @PreAuthorize("@res_partner_p_pms.checkByRes_partner(#res_partner_id,'')")
    public ResponseEntity<Boolean> checkKeyByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Res_partner_p res_partner_p) {
        boolean b = res_partner_pservice.checkKeyByRes_partner(res_partner_id, res_partner_p);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/res_partner_ps/{res_partner_p_id}/save")
    @PreAuthorize("@res_partner_p_pms.checkByRes_partner(#res_partner_id,#res_partner_p_id,'')")
    public ResponseEntity<Res_partner_p> saveByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_p res_partner_p) {
        Res_partner_p res_partner_p2 = res_partner_pservice.saveByRes_partner(res_partner_id, res_partner_p_id, res_partner_p);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_p2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/res_partners/{res_partner_id}/res_partner_ps/{res_partner_p_id}")
    @PreAuthorize("@res_partner_p_pms.checkByRes_partner(#res_partner_id,#res_partner_p_id,'UPDATE')")
    public ResponseEntity<Res_partner_p> updateByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_p res_partner_p) {
        Res_partner_p res_partner_p2 = res_partner_pservice.updateByRes_partner(res_partner_id, res_partner_p_id, res_partner_p);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_p2);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/api/res_partners/{res_partner_id}/res_partner_ps/{res_partner_p_id}")
    @PreAuthorize("@res_partner_p_pms.checkByRes_partner(#res_partner_id,#res_partner_p_id,'UPDATE')")
    public ResponseEntity<Res_partner_p> api_updateByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_p res_partner_p) {
        Res_partner_p res_partner_p2 = res_partner_pservice.updateByRes_partner(res_partner_id, res_partner_p_id, res_partner_p);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_p2);
    }

	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/res_partner_ps/{res_partner_p_id}/createbatch")
    @PreAuthorize("@res_partner_p_pms.checkByRes_partner(#res_partner_id,#res_partner_p_id,'CREATE')")
    public ResponseEntity<Res_partner_p> createBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_p res_partner_p) {
        Res_partner_p res_partner_p2 = res_partner_pservice.createBatchByRes_partner(res_partner_id, res_partner_p_id, res_partner_p);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_p2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/res_partners/{res_partner_id}/res_partner_ps/{res_partner_p_id}/updatebatch")
    @PreAuthorize("@res_partner_p_pms.checkByRes_partner(#res_partner_id,#res_partner_p_id,'UPDATE')")
    public ResponseEntity<Res_partner_p> updateBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_p res_partner_p) {
        Res_partner_p res_partner_p2 = res_partner_pservice.updateBatchByRes_partner(res_partner_id, res_partner_p_id, res_partner_p);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_p2);
    }

	@RequestMapping(method = RequestMethod.GET, value = "/res_partners/{res_partner_id}/res_partner_ps/getdraft")
    @PreAuthorize("@res_partner_p_pms.checkByRes_partner(#res_partner_id,'CREATE')")
    public ResponseEntity<Res_partner_p> getDraftByRes_partner() {
        //Res_partner_p res_partner_p = res_partner_pservice.getDraftByRes_partner(res_partner_id,  res_partner_p_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Res_partner_p());
    }

	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/res_partner_ps")
    @PreAuthorize("@res_partner_p_pms.checkByRes_partner(#res_partner_id,'CREATE')")
    public ResponseEntity<Res_partner_p> createByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Res_partner_p res_partner_p) {
        Res_partner_p res_partner_p2 = res_partner_pservice.createByRes_partner(res_partner_id, res_partner_p);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_p2);
    }

	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partners/{res_partner_id}/res_partner_ps/{res_partner_p_id}/removebatch")
    @PreAuthorize("@res_partner_p_pms.checkByRes_partner(#res_partner_id,#res_partner_p_id,'DELETE')")
    public ResponseEntity<Res_partner_p> removeBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_p res_partner_p) {
        Res_partner_p res_partner_p2 = res_partner_pservice.removeBatchByRes_partner(res_partner_id, res_partner_p_id, res_partner_p);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_p2);
    }

    @RequestMapping(method= RequestMethod.GET , value="/res_partners/{res_partner_id}/res_partner_ps/fetchcontacts")
    @PreAuthorize("@res_partner_p_pms.checkByRes_partner(#res_partner_id,'READ')")
	public ResponseEntity<List<Res_partner_p>> fetchContactsByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id,Res_partner_pSearchContext searchContext,Pageable pageable) {
        searchContext.setN_parent_id_eq(res_partner_id);
        Page<Res_partner_p> page = res_partner_pservice.fetchContactsByRes_partner(res_partner_id , searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }

    @RequestMapping(method= RequestMethod.GET , value="/res_partners/{res_partner_id}/res_partner_ps/fetchcompany")
    @PreAuthorize("@res_partner_p_pms.checkByRes_partner(#res_partner_id,'READ')")
	public ResponseEntity<List<Res_partner_p>> fetchCompanyByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id,Res_partner_pSearchContext searchContext,Pageable pageable) {
        searchContext.setN_parent_id_eq(res_partner_id);
        Page<Res_partner_p> page = res_partner_pservice.fetchCompanyByRes_partner(res_partner_id , searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }

    @RequestMapping(method= RequestMethod.GET , value="/res_partners/{res_partner_id}/res_partner_ps/fetchdefault")
    @PreAuthorize("@res_partner_p_pms.checkByRes_partner(#res_partner_id,'READ')")
	public ResponseEntity<List<Res_partner_p>> fetchDefaultByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id,Res_partner_pSearchContext searchContext,Pageable pageable) {
        searchContext.setN_parent_id_eq(res_partner_id);
        Page<Res_partner_p> page = res_partner_pservice.fetchDefaultByRes_partner(res_partner_id , searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}
