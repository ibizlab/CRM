package cn.ibizlab.odoo.web.r7rt_dyna.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 应用实体[动态图表]
 */
public class DynaChart implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 建立时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp createDate;

    @JsonIgnore
    private boolean createDateDirtyFlag;
    
    /**
     * 动态图表名称
     */
    private String dynaChartName;

    @JsonIgnore
    private boolean dynaChartNameDirtyFlag;
    
    /**
     * 更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp updateDate;

    @JsonIgnore
    private boolean updateDateDirtyFlag;
    
    /**
     * 模型标识
     */
    private String modelId;

    @JsonIgnore
    private boolean modelIdDirtyFlag;
    
    /**
     * 应用标识
     */
    private String appId;

    @JsonIgnore
    private boolean appIdDirtyFlag;
    
    /**
     * 模型
     */
    private String model;

    @JsonIgnore
    private boolean modelDirtyFlag;
    
    /**
     * 更新人
     */
    private String updateMan;

    @JsonIgnore
    private boolean updateManDirtyFlag;
    
    /**
     * 用户标识
     */
    private String userId;

    @JsonIgnore
    private boolean userIdDirtyFlag;
    
    /**
     * 建立人
     */
    private String createMan;

    @JsonIgnore
    private boolean createManDirtyFlag;
    
    /**
     * 动态图表标识
     */
    private String dynaChartId;

    @JsonIgnore
    private boolean dynaChartIdDirtyFlag;
    

    /**
     * 获取 [建立时间]
     */
    @JsonProperty("createdate")
    public Timestamp getCreateDate(){
        return this.createDate ;
    }

    /**
     * 设置 [建立时间]
     */
    @JsonProperty("createdate")
    public void setCreateDate(Timestamp  createDate){
        this.createDate = createDate ;
        this.createDateDirtyFlag = true ;
    }

    /**
     * 获取 [建立时间]脏标记
     */
    @JsonIgnore
    public boolean getCreateDateDirtyFlag(){
        return this.createDateDirtyFlag ;
    }

    /**
     * 获取 [动态图表名称]
     */
    @JsonProperty("dynachartname")
    public String getDynaChartName(){
        return this.dynaChartName ;
    }

    /**
     * 设置 [动态图表名称]
     */
    @JsonProperty("dynachartname")
    public void setDynaChartName(String  dynaChartName){
        this.dynaChartName = dynaChartName ;
        this.dynaChartNameDirtyFlag = true ;
    }

    /**
     * 获取 [动态图表名称]脏标记
     */
    @JsonIgnore
    public boolean getDynaChartNameDirtyFlag(){
        return this.dynaChartNameDirtyFlag ;
    }

    /**
     * 获取 [更新时间]
     */
    @JsonProperty("updatedate")
    public Timestamp getUpdateDate(){
        return this.updateDate ;
    }

    /**
     * 设置 [更新时间]
     */
    @JsonProperty("updatedate")
    public void setUpdateDate(Timestamp  updateDate){
        this.updateDate = updateDate ;
        this.updateDateDirtyFlag = true ;
    }

    /**
     * 获取 [更新时间]脏标记
     */
    @JsonIgnore
    public boolean getUpdateDateDirtyFlag(){
        return this.updateDateDirtyFlag ;
    }

    /**
     * 获取 [模型标识]
     */
    @JsonProperty("modelid")
    public String getModelId(){
        return this.modelId ;
    }

    /**
     * 设置 [模型标识]
     */
    @JsonProperty("modelid")
    public void setModelId(String  modelId){
        this.modelId = modelId ;
        this.modelIdDirtyFlag = true ;
    }

    /**
     * 获取 [模型标识]脏标记
     */
    @JsonIgnore
    public boolean getModelIdDirtyFlag(){
        return this.modelIdDirtyFlag ;
    }

    /**
     * 获取 [应用标识]
     */
    @JsonProperty("appid")
    public String getAppId(){
        return this.appId ;
    }

    /**
     * 设置 [应用标识]
     */
    @JsonProperty("appid")
    public void setAppId(String  appId){
        this.appId = appId ;
        this.appIdDirtyFlag = true ;
    }

    /**
     * 获取 [应用标识]脏标记
     */
    @JsonIgnore
    public boolean getAppIdDirtyFlag(){
        return this.appIdDirtyFlag ;
    }

    /**
     * 获取 [模型]
     */
    @JsonProperty("model")
    public String getModel(){
        return this.model ;
    }

    /**
     * 设置 [模型]
     */
    @JsonProperty("model")
    public void setModel(String  model){
        this.model = model ;
        this.modelDirtyFlag = true ;
    }

    /**
     * 获取 [模型]脏标记
     */
    @JsonIgnore
    public boolean getModelDirtyFlag(){
        return this.modelDirtyFlag ;
    }

    /**
     * 获取 [更新人]
     */
    @JsonProperty("updateman")
    public String getUpdateMan(){
        return this.updateMan ;
    }

    /**
     * 设置 [更新人]
     */
    @JsonProperty("updateman")
    public void setUpdateMan(String  updateMan){
        this.updateMan = updateMan ;
        this.updateManDirtyFlag = true ;
    }

    /**
     * 获取 [更新人]脏标记
     */
    @JsonIgnore
    public boolean getUpdateManDirtyFlag(){
        return this.updateManDirtyFlag ;
    }

    /**
     * 获取 [用户标识]
     */
    @JsonProperty("userid")
    public String getUserId(){
        return this.userId ;
    }

    /**
     * 设置 [用户标识]
     */
    @JsonProperty("userid")
    public void setUserId(String  userId){
        this.userId = userId ;
        this.userIdDirtyFlag = true ;
    }

    /**
     * 获取 [用户标识]脏标记
     */
    @JsonIgnore
    public boolean getUserIdDirtyFlag(){
        return this.userIdDirtyFlag ;
    }

    /**
     * 获取 [建立人]
     */
    @JsonProperty("createman")
    public String getCreateMan(){
        return this.createMan ;
    }

    /**
     * 设置 [建立人]
     */
    @JsonProperty("createman")
    public void setCreateMan(String  createMan){
        this.createMan = createMan ;
        this.createManDirtyFlag = true ;
    }

    /**
     * 获取 [建立人]脏标记
     */
    @JsonIgnore
    public boolean getCreateManDirtyFlag(){
        return this.createManDirtyFlag ;
    }

    /**
     * 获取 [动态图表标识]
     */
    @JsonProperty("dynachartid")
    public String getDynaChartId(){
        return this.dynaChartId ;
    }

    /**
     * 设置 [动态图表标识]
     */
    @JsonProperty("dynachartid")
    public void setDynaChartId(String  dynaChartId){
        this.dynaChartId = dynaChartId ;
        this.dynaChartIdDirtyFlag = true ;
    }

    /**
     * 获取 [动态图表标识]脏标记
     */
    @JsonIgnore
    public boolean getDynaChartIdDirtyFlag(){
        return this.dynaChartIdDirtyFlag ;
    }



}
