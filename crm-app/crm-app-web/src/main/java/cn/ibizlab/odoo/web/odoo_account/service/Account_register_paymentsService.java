package cn.ibizlab.odoo.web.odoo_account.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.web.odoo_account.domain.Account_register_payments;
import cn.ibizlab.odoo.web.odoo_account.filter.*;
import cn.ibizlab.odoo.web.odoo_account.feign.Account_register_paymentsFeignClient;
import cn.ibizlab.odoo.web.WebApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Account_register_paymentsService {

    Account_register_paymentsFeignClient client;

    @Autowired
    public Account_register_paymentsService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Account_register_paymentsFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Account_register_paymentsFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public boolean checkKey(Account_register_payments account_register_payments) {
        return client.checkKey(account_register_payments);
    }

    public Account_register_payments save(Integer account_register_payments_id, Account_register_payments account_register_payments) {
        return client.save(account_register_payments_id, account_register_payments);
    }

    public Account_register_payments updateBatch(Integer account_register_payments_id, Account_register_payments account_register_payments) {
        return client.updateBatch(account_register_payments_id, account_register_payments);
    }

    public Account_register_payments get( Integer account_register_payments_id) {
        return client.get( account_register_payments_id);
    }

    public Account_register_payments removeBatch(Integer account_register_payments_id, Account_register_payments account_register_payments) {
        return client.removeBatch(account_register_payments_id, account_register_payments);
    }

	public Account_register_payments create(Account_register_payments account_register_payments) {
        return client.create(account_register_payments);
    }

    public Account_register_payments update(Integer account_register_payments_id, Account_register_payments account_register_payments) {
        return client.update(account_register_payments_id, account_register_payments);
    }

    public Account_register_payments createBatch(Integer account_register_payments_id, Account_register_payments account_register_payments) {
        return client.createBatch(account_register_payments_id, account_register_payments);
    }

    public boolean remove( Integer account_register_payments_id) {
        return client.remove( account_register_payments_id);
    }

    public Account_register_payments getDraft(Integer account_register_payments_id, Account_register_payments account_register_payments) {
        return client.getDraft(account_register_payments_id, account_register_payments);
    }

	public Page<Account_register_payments> fetchDefault(Account_register_paymentsSearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }

}
