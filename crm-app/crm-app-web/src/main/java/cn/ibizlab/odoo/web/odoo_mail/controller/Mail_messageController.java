package cn.ibizlab.odoo.web.odoo_mail.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.web.odoo_mail.service.Mail_messageService;
import cn.ibizlab.odoo.web.odoo_mail.domain.Mail_message;
import cn.ibizlab.odoo.web.odoo_mail.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Mail_messageController {
	@Autowired
    Mail_messageService mail_messageservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/mail_messages")
    @PreAuthorize("@mail_message_pms.check('CREATE')")
    public ResponseEntity<Mail_message> create(@RequestBody Mail_message mail_message) {
        Mail_message mail_message2 = mail_messageservice.create(mail_message);
        return ResponseEntity.status(HttpStatus.OK).body(mail_message2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/mail_messages/{mail_message_id}")
    @PreAuthorize("@mail_message_pms.check(#mail_message_id,'READ')")
    public ResponseEntity<Mail_message> get(@PathVariable("mail_message_id") Integer mail_message_id) {
        Mail_message mail_message = mail_messageservice.get( mail_message_id);
        return ResponseEntity.status(HttpStatus.OK).body(mail_message);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/mail_messages/checkkey")
    @PreAuthorize("@mail_message_pms.check('')")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_message mail_message) {
        boolean b = mail_messageservice.checkKey(mail_message);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_messages/{mail_message_id}/updatebatch")
    @PreAuthorize("@mail_message_pms.check(#mail_message_id,'UPDATE')")
    public ResponseEntity<Mail_message> updateBatch(@PathVariable("mail_message_id") Integer mail_message_id, @RequestBody Mail_message mail_message) {
        Mail_message mail_message2 = mail_messageservice.updateBatch(mail_message_id, mail_message);
        return ResponseEntity.status(HttpStatus.OK).body(mail_message2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/mail_messages/getdraft")
    @PreAuthorize("@mail_message_pms.check('CREATE')")
    public ResponseEntity<Mail_message> getDraft() {
        //Mail_message mail_message = mail_messageservice.getDraft( mail_message_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Mail_message());
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/mail_messages/{mail_message_id}/createbatch")
    @PreAuthorize("@mail_message_pms.check(#mail_message_id,'CREATE')")
    public ResponseEntity<Mail_message> createBatch(@PathVariable("mail_message_id") Integer mail_message_id, @RequestBody Mail_message mail_message) {
        Mail_message mail_message2 = mail_messageservice.createBatch(mail_message_id, mail_message);
        return ResponseEntity.status(HttpStatus.OK).body(mail_message2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_messages/{mail_message_id}/removebatch")
    @PreAuthorize("@mail_message_pms.check(#mail_message_id,'DELETE')")
    public ResponseEntity<Mail_message> removeBatch(@PathVariable("mail_message_id") Integer mail_message_id, @RequestBody Mail_message mail_message) {
        Mail_message mail_message2 = mail_messageservice.removeBatch(mail_message_id, mail_message);
        return ResponseEntity.status(HttpStatus.OK).body(mail_message2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_messages/{mail_message_id}")
    @PreAuthorize("@mail_message_pms.check(#mail_message_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_message_id") Integer mail_message_id) {
        boolean b = mail_messageservice.remove( mail_message_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_messages/{mail_message_id}")
    @PreAuthorize("@mail_message_pms.check(#mail_message_id,'UPDATE')")
    public ResponseEntity<Mail_message> update(@PathVariable("mail_message_id") Integer mail_message_id, @RequestBody Mail_message mail_message) {
        Mail_message mail_message2 = mail_messageservice.update(mail_message_id, mail_message);
        return ResponseEntity.status(HttpStatus.OK).body(mail_message2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/mail_messages/{mail_message_id}")
    @PreAuthorize("@mail_message_pms.check(#mail_message_id,'UPDATE')")
    public ResponseEntity<Mail_message> api_update(@PathVariable("mail_message_id") Integer mail_message_id, @RequestBody Mail_message mail_message) {
        Mail_message mail_message2 = mail_messageservice.update(mail_message_id, mail_message);
        return ResponseEntity.status(HttpStatus.OK).body(mail_message2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/mail_messages/{mail_message_id}/save")
    @PreAuthorize("@mail_message_pms.check(#mail_message_id,'')")
    public ResponseEntity<Mail_message> save(@PathVariable("mail_message_id") Integer mail_message_id, @RequestBody Mail_message mail_message) {
        Mail_message mail_message2 = mail_messageservice.save(mail_message_id, mail_message);
        return ResponseEntity.status(HttpStatus.OK).body(mail_message2);
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/mail_messages/fetchdefault")
    @PreAuthorize("@mail_message_pms.check('READ')")
	public ResponseEntity<List<Mail_message>> fetchDefault(Mail_messageSearchContext searchContext,Pageable pageable) {
        
        Page<Mail_message> page = mail_messageservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}
