package cn.ibizlab.odoo.web.r7rt_dyna.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.web.r7rt_dyna.domain.DynaDashboard;
import cn.ibizlab.odoo.web.r7rt_dyna.filter.*;
import cn.ibizlab.odoo.web.r7rt_dyna.feign.DynaDashboardFeignClient;
import cn.ibizlab.odoo.web.WebApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class DynaDashboardService {

    DynaDashboardFeignClient client;

    @Autowired
    public DynaDashboardService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(DynaDashboardFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(DynaDashboardFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public DynaDashboard save(String dynadashboard_id, DynaDashboard dynadashboard) {
        return client.save(dynadashboard_id, dynadashboard);
    }

    public DynaDashboard update(String dynadashboard_id, DynaDashboard dynadashboard) {
        return client.update(dynadashboard_id, dynadashboard);
    }

    public DynaDashboard get( String dynadashboard_id) {
        return client.get( dynadashboard_id);
    }

    public boolean remove( String dynadashboard_id) {
        return client.remove( dynadashboard_id);
    }

    public boolean checkKey(DynaDashboard dynadashboard) {
        return client.checkKey(dynadashboard);
    }

    public DynaDashboard getDraft(String dynadashboard_id, DynaDashboard dynadashboard) {
        return client.getDraft(dynadashboard_id, dynadashboard);
    }

	public DynaDashboard create(DynaDashboard dynadashboard) {
        return client.create(dynadashboard);
    }

	public Page<DynaDashboard> fetchDefault(DynaDashboardSearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }

}
