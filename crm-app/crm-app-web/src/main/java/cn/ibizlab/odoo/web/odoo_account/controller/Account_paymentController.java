package cn.ibizlab.odoo.web.odoo_account.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.web.odoo_account.service.Account_paymentService;
import cn.ibizlab.odoo.web.odoo_account.domain.Account_payment;
import cn.ibizlab.odoo.web.odoo_account.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Account_paymentController {
	@Autowired
    Account_paymentService account_paymentservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/account_payments/checkkey")
    @PreAuthorize("@account_payment_pms.check('')")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_payment account_payment) {
        boolean b = account_paymentservice.checkKey(account_payment);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/account_payments/{account_payment_id}")
    @PreAuthorize("@account_payment_pms.check(#account_payment_id,'UPDATE')")
    public ResponseEntity<Account_payment> update(@PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_payment account_payment) {
        Account_payment account_payment2 = account_paymentservice.update(account_payment_id, account_payment);
        return ResponseEntity.status(HttpStatus.OK).body(account_payment2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/account_payments/{account_payment_id}")
    @PreAuthorize("@account_payment_pms.check(#account_payment_id,'UPDATE')")
    public ResponseEntity<Account_payment> api_update(@PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_payment account_payment) {
        Account_payment account_payment2 = account_paymentservice.update(account_payment_id, account_payment);
        return ResponseEntity.status(HttpStatus.OK).body(account_payment2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/account_payments/getdraft")
    @PreAuthorize("@account_payment_pms.check('CREATE')")
    public ResponseEntity<Account_payment> getDraft() {
        //Account_payment account_payment = account_paymentservice.getDraft( account_payment_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Account_payment());
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_payments/{account_payment_id}")
    @PreAuthorize("@account_payment_pms.check(#account_payment_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("account_payment_id") Integer account_payment_id) {
        boolean b = account_paymentservice.remove( account_payment_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_payments/{account_payment_id}/removebatch")
    @PreAuthorize("@account_payment_pms.check(#account_payment_id,'DELETE')")
    public ResponseEntity<Account_payment> removeBatch(@PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_payment account_payment) {
        Account_payment account_payment2 = account_paymentservice.removeBatch(account_payment_id, account_payment);
        return ResponseEntity.status(HttpStatus.OK).body(account_payment2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/account_payments/{account_payment_id}/save")
    @PreAuthorize("@account_payment_pms.check(#account_payment_id,'')")
    public ResponseEntity<Account_payment> save(@PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_payment account_payment) {
        Account_payment account_payment2 = account_paymentservice.save(account_payment_id, account_payment);
        return ResponseEntity.status(HttpStatus.OK).body(account_payment2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/account_payments/{account_payment_id}/updatebatch")
    @PreAuthorize("@account_payment_pms.check(#account_payment_id,'UPDATE')")
    public ResponseEntity<Account_payment> updateBatch(@PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_payment account_payment) {
        Account_payment account_payment2 = account_paymentservice.updateBatch(account_payment_id, account_payment);
        return ResponseEntity.status(HttpStatus.OK).body(account_payment2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/account_payments")
    @PreAuthorize("@account_payment_pms.check('CREATE')")
    public ResponseEntity<Account_payment> create(@RequestBody Account_payment account_payment) {
        Account_payment account_payment2 = account_paymentservice.create(account_payment);
        return ResponseEntity.status(HttpStatus.OK).body(account_payment2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/account_payments/{account_payment_id}")
    @PreAuthorize("@account_payment_pms.check(#account_payment_id,'READ')")
    public ResponseEntity<Account_payment> get(@PathVariable("account_payment_id") Integer account_payment_id) {
        Account_payment account_payment = account_paymentservice.get( account_payment_id);
        return ResponseEntity.status(HttpStatus.OK).body(account_payment);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/account_payments/{account_payment_id}/createbatch")
    @PreAuthorize("@account_payment_pms.check(#account_payment_id,'CREATE')")
    public ResponseEntity<Account_payment> createBatch(@PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_payment account_payment) {
        Account_payment account_payment2 = account_paymentservice.createBatch(account_payment_id, account_payment);
        return ResponseEntity.status(HttpStatus.OK).body(account_payment2);
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/account_payments/fetchdefault")
    @PreAuthorize("@account_payment_pms.check('READ')")
	public ResponseEntity<List<Account_payment>> fetchDefault(Account_paymentSearchContext searchContext,Pageable pageable) {
        
        Page<Account_payment> page = account_paymentservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/account_payments/checkkey")
    @PreAuthorize("@account_payment_pms.checkByRes_partner(#res_partner_id,'')")
    public ResponseEntity<Boolean> checkKeyByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Account_payment account_payment) {
        boolean b = account_paymentservice.checkKeyByRes_partner(res_partner_id, account_payment);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/res_partners/{res_partner_id}/account_payments/{account_payment_id}")
    @PreAuthorize("@account_payment_pms.checkByRes_partner(#res_partner_id,#account_payment_id,'UPDATE')")
    public ResponseEntity<Account_payment> updateByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_payment account_payment) {
        Account_payment account_payment2 = account_paymentservice.updateByRes_partner(res_partner_id, account_payment_id, account_payment);
        return ResponseEntity.status(HttpStatus.OK).body(account_payment2);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/api/res_partners/{res_partner_id}/account_payments/{account_payment_id}")
    @PreAuthorize("@account_payment_pms.checkByRes_partner(#res_partner_id,#account_payment_id,'UPDATE')")
    public ResponseEntity<Account_payment> api_updateByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_payment account_payment) {
        Account_payment account_payment2 = account_paymentservice.updateByRes_partner(res_partner_id, account_payment_id, account_payment);
        return ResponseEntity.status(HttpStatus.OK).body(account_payment2);
    }

	@RequestMapping(method = RequestMethod.GET, value = "/res_partners/{res_partner_id}/account_payments/getdraft")
    @PreAuthorize("@account_payment_pms.checkByRes_partner(#res_partner_id,'CREATE')")
    public ResponseEntity<Account_payment> getDraftByRes_partner() {
        //Account_payment account_payment = account_paymentservice.getDraftByRes_partner(res_partner_id,  account_payment_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Account_payment());
    }

	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partners/{res_partner_id}/account_payments/{account_payment_id}")
    @PreAuthorize("@account_payment_pms.checkByRes_partner(#res_partner_id,#account_payment_id,'DELETE')")
    public ResponseEntity<Boolean> removeByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_payment_id") Integer account_payment_id) {
        boolean b = account_paymentservice.removeByRes_partner(res_partner_id,  account_payment_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partners/{res_partner_id}/account_payments/{account_payment_id}/removebatch")
    @PreAuthorize("@account_payment_pms.checkByRes_partner(#res_partner_id,#account_payment_id,'DELETE')")
    public ResponseEntity<Account_payment> removeBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_payment account_payment) {
        Account_payment account_payment2 = account_paymentservice.removeBatchByRes_partner(res_partner_id, account_payment_id, account_payment);
        return ResponseEntity.status(HttpStatus.OK).body(account_payment2);
    }

	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/account_payments/{account_payment_id}/save")
    @PreAuthorize("@account_payment_pms.checkByRes_partner(#res_partner_id,#account_payment_id,'')")
    public ResponseEntity<Account_payment> saveByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_payment account_payment) {
        Account_payment account_payment2 = account_paymentservice.saveByRes_partner(res_partner_id, account_payment_id, account_payment);
        return ResponseEntity.status(HttpStatus.OK).body(account_payment2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/res_partners/{res_partner_id}/account_payments/{account_payment_id}/updatebatch")
    @PreAuthorize("@account_payment_pms.checkByRes_partner(#res_partner_id,#account_payment_id,'UPDATE')")
    public ResponseEntity<Account_payment> updateBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_payment account_payment) {
        Account_payment account_payment2 = account_paymentservice.updateBatchByRes_partner(res_partner_id, account_payment_id, account_payment);
        return ResponseEntity.status(HttpStatus.OK).body(account_payment2);
    }

	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/account_payments")
    @PreAuthorize("@account_payment_pms.checkByRes_partner(#res_partner_id,'CREATE')")
    public ResponseEntity<Account_payment> createByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Account_payment account_payment) {
        Account_payment account_payment2 = account_paymentservice.createByRes_partner(res_partner_id, account_payment);
        return ResponseEntity.status(HttpStatus.OK).body(account_payment2);
    }

	@RequestMapping(method = RequestMethod.GET, value = "/res_partners/{res_partner_id}/account_payments/{account_payment_id}")
    @PreAuthorize("@account_payment_pms.checkByRes_partner(#res_partner_id,#account_payment_id,'READ')")
    public ResponseEntity<Account_payment> getByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_payment_id") Integer account_payment_id) {
        Account_payment account_payment = account_paymentservice.getByRes_partner(res_partner_id,  account_payment_id);
        return ResponseEntity.status(HttpStatus.OK).body(account_payment);
    }

	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/account_payments/{account_payment_id}/createbatch")
    @PreAuthorize("@account_payment_pms.checkByRes_partner(#res_partner_id,#account_payment_id,'CREATE')")
    public ResponseEntity<Account_payment> createBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_payment account_payment) {
        Account_payment account_payment2 = account_paymentservice.createBatchByRes_partner(res_partner_id, account_payment_id, account_payment);
        return ResponseEntity.status(HttpStatus.OK).body(account_payment2);
    }

    @RequestMapping(method= RequestMethod.GET , value="/res_partners/{res_partner_id}/account_payments/fetchdefault")
    @PreAuthorize("@account_payment_pms.checkByRes_partner(#res_partner_id,'READ')")
	public ResponseEntity<List<Account_payment>> fetchDefaultByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id,Account_paymentSearchContext searchContext,Pageable pageable) {
        searchContext.setN_partner_id_eq(res_partner_id);
        Page<Account_payment> page = account_paymentservice.fetchDefaultByRes_partner(res_partner_id , searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}
