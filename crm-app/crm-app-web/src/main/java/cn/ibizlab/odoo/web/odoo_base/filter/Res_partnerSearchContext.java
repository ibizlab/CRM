package cn.ibizlab.odoo.web.odoo_base.filter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Data
@IBIZLog
public class Res_partnerSearchContext extends SearchContext implements Serializable {

	public String n_type_eq;//[地址类型]
	public String n_trust_eq;//[对此债务人的信任度]
	public String n_invoice_warn_eq;//[发票]
	public String n_is_company_eq;//[公司]
	public String n_tz_eq;//[时区]
	public String n_sale_warn_eq;//[销售警告]
	public String n_picking_warn_eq;//[库存拣货]
	public String n_purchase_warn_eq;//[采购订单]
	public String n_activity_state_eq;//[活动状态]
	public String n_name_like;//[名称]
	public String n_company_type_eq;//[公司类别]
	public String n_lang_eq;//[语言]
	public String n_write_uid_text_eq;//[最后更新者]
	public String n_write_uid_text_like;//[最后更新者]
	public String n_title_text_eq;//[称谓]
	public String n_title_text_like;//[称谓]
	public String n_company_id_text_eq;//[公司]
	public String n_company_id_text_like;//[公司]
	public String n_country_id_text_eq;//[国家/地区]
	public String n_country_id_text_like;//[国家/地区]
	public String n_state_id_text_eq;//[省/ 州]
	public String n_state_id_text_like;//[省/ 州]
	public String n_commercial_partner_id_text_eq;//[商业实体]
	public String n_commercial_partner_id_text_like;//[商业实体]
	public String n_parent_name_eq;//[上级名称]
	public String n_parent_name_like;//[上级名称]
	public String n_user_id_text_eq;//[销售员]
	public String n_user_id_text_like;//[销售员]
	public String n_create_uid_text_eq;//[创建人]
	public String n_create_uid_text_like;//[创建人]
	public String n_industry_id_text_eq;//[工业]
	public String n_industry_id_text_like;//[工业]
	public String n_team_id_text_eq;//[销售团队]
	public String n_team_id_text_like;//[销售团队]
	public Integer n_team_id_eq;//[销售团队]
	public Integer n_state_id_eq;//[省/ 州]
	public Integer n_user_id_eq;//[销售员]
	public Integer n_create_uid_eq;//[创建人]
	public Integer n_parent_id_eq;//[关联公司]
	public Integer n_title_eq;//[称谓]
	public Integer n_write_uid_eq;//[最后更新者]
	public Integer n_commercial_partner_id_eq;//[商业实体]
	public Integer n_industry_id_eq;//[工业]
	public Integer n_company_id_eq;//[公司]
	public Integer n_country_id_eq;//[国家/地区]

}