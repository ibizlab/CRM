package cn.ibizlab.odoo.web.odoo_product.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.web.odoo_product.service.Product_productService;
import cn.ibizlab.odoo.web.odoo_product.domain.Product_product;
import cn.ibizlab.odoo.web.odoo_product.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Product_productController {
	@Autowired
    Product_productService product_productservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/product_products/{product_product_id}/updatebatch")
    @PreAuthorize("@product_product_pms.check(#product_product_id,'UPDATE')")
    public ResponseEntity<Product_product> updateBatch(@PathVariable("product_product_id") Integer product_product_id, @RequestBody Product_product product_product) {
        Product_product product_product2 = product_productservice.updateBatch(product_product_id, product_product);
        return ResponseEntity.status(HttpStatus.OK).body(product_product2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/product_products/{product_product_id}")
    @PreAuthorize("@product_product_pms.check(#product_product_id,'READ')")
    public ResponseEntity<Product_product> get(@PathVariable("product_product_id") Integer product_product_id) {
        Product_product product_product = product_productservice.get( product_product_id);
        return ResponseEntity.status(HttpStatus.OK).body(product_product);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/{product_product_id}/save")
    @PreAuthorize("@product_product_pms.check(#product_product_id,'')")
    public ResponseEntity<Product_product> save(@PathVariable("product_product_id") Integer product_product_id, @RequestBody Product_product product_product) {
        Product_product product_product2 = product_productservice.save(product_product_id, product_product);
        return ResponseEntity.status(HttpStatus.OK).body(product_product2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/product_products")
    @PreAuthorize("@product_product_pms.check('CREATE')")
    public ResponseEntity<Product_product> create(@RequestBody Product_product product_product) {
        Product_product product_product2 = product_productservice.create(product_product);
        return ResponseEntity.status(HttpStatus.OK).body(product_product2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/checkkey")
    @PreAuthorize("@product_product_pms.check('')")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_product product_product) {
        boolean b = product_productservice.checkKey(product_product);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/product_products/{product_product_id}")
    @PreAuthorize("@product_product_pms.check(#product_product_id,'UPDATE')")
    public ResponseEntity<Product_product> update(@PathVariable("product_product_id") Integer product_product_id, @RequestBody Product_product product_product) {
        Product_product product_product2 = product_productservice.update(product_product_id, product_product);
        return ResponseEntity.status(HttpStatus.OK).body(product_product2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/product_products/{product_product_id}")
    @PreAuthorize("@product_product_pms.check(#product_product_id,'UPDATE')")
    public ResponseEntity<Product_product> api_update(@PathVariable("product_product_id") Integer product_product_id, @RequestBody Product_product product_product) {
        Product_product product_product2 = product_productservice.update(product_product_id, product_product);
        return ResponseEntity.status(HttpStatus.OK).body(product_product2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_products/{product_product_id}/removebatch")
    @PreAuthorize("@product_product_pms.check(#product_product_id,'DELETE')")
    public ResponseEntity<Product_product> removeBatch(@PathVariable("product_product_id") Integer product_product_id, @RequestBody Product_product product_product) {
        Product_product product_product2 = product_productservice.removeBatch(product_product_id, product_product);
        return ResponseEntity.status(HttpStatus.OK).body(product_product2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/product_products/getdraft")
    @PreAuthorize("@product_product_pms.check('CREATE')")
    public ResponseEntity<Product_product> getDraft() {
        //Product_product product_product = product_productservice.getDraft( product_product_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Product_product());
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_products/{product_product_id}")
    @PreAuthorize("@product_product_pms.check(#product_product_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("product_product_id") Integer product_product_id) {
        boolean b = product_productservice.remove( product_product_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/{product_product_id}/createbatch")
    @PreAuthorize("@product_product_pms.check(#product_product_id,'CREATE')")
    public ResponseEntity<Product_product> createBatch(@PathVariable("product_product_id") Integer product_product_id, @RequestBody Product_product product_product) {
        Product_product product_product2 = product_productservice.createBatch(product_product_id, product_product);
        return ResponseEntity.status(HttpStatus.OK).body(product_product2);
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/product_products/fetchdefault")
    @PreAuthorize("@product_product_pms.check('READ')")
	public ResponseEntity<List<Product_product>> fetchDefault(Product_productSearchContext searchContext,Pageable pageable) {
        
        Page<Product_product> page = product_productservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}
