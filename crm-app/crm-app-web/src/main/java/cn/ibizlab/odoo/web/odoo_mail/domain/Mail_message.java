package cn.ibizlab.odoo.web.odoo_mail.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 应用实体[消息]
 */
public class Mail_message implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 待处理
     */
    private String needaction;

    @JsonIgnore
    private boolean needactionDirtyFlag;
    
    /**
     * 布局
     */
    private String layout;

    @JsonIgnore
    private boolean layoutDirtyFlag;
    
    /**
     * 相关的文档模型
     */
    private String model;

    @JsonIgnore
    private boolean modelDirtyFlag;
    
    /**
     * 无响应
     */
    private String no_auto_thread;

    @JsonIgnore
    private boolean no_auto_threadDirtyFlag;
    
    /**
     * 收件人
     */
    private String partner_ids;

    @JsonIgnore
    private boolean partner_idsDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;
    
    /**
     * 消息记录名称
     */
    private String record_name;

    @JsonIgnore
    private boolean record_nameDirtyFlag;
    
    /**
     * 消息ID
     */
    private String message_id;

    @JsonIgnore
    private boolean message_idDirtyFlag;
    
    /**
     * 追踪值
     */
    private String tracking_value_ids;

    @JsonIgnore
    private boolean tracking_value_idsDirtyFlag;
    
    /**
     * 管理状态
     */
    private String moderation_status;

    @JsonIgnore
    private boolean moderation_statusDirtyFlag;
    
    /**
     * 通知
     */
    private String notification_ids;

    @JsonIgnore
    private boolean notification_idsDirtyFlag;
    
    /**
     * 从
     */
    private String email_from;

    @JsonIgnore
    private boolean email_fromDirtyFlag;
    
    /**
     * 渠道
     */
    private String channel_ids;

    @JsonIgnore
    private boolean channel_idsDirtyFlag;
    
    /**
     * ID
     */
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;
    
    /**
     * 说明
     */
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;
    
    /**
     * 加星的邮件
     */
    private String starred;

    @JsonIgnore
    private boolean starredDirtyFlag;
    
    /**
     * 相关评级
     */
    private String rating_ids;

    @JsonIgnore
    private boolean rating_idsDirtyFlag;
    
    /**
     * 待处理的业务伙伴
     */
    private String needaction_partner_ids;

    @JsonIgnore
    private boolean needaction_partner_idsDirtyFlag;
    
    /**
     * 回复 至
     */
    private String reply_to;

    @JsonIgnore
    private boolean reply_toDirtyFlag;
    
    /**
     * 邮件发送服务器
     */
    private Integer mail_server_id;

    @JsonIgnore
    private boolean mail_server_idDirtyFlag;
    
    /**
     * 相关文档编号
     */
    private Integer res_id;

    @JsonIgnore
    private boolean res_idDirtyFlag;
    
    /**
     * 需审核
     */
    private String need_moderation;

    @JsonIgnore
    private boolean need_moderationDirtyFlag;
    
    /**
     * 主题
     */
    private String subject;

    @JsonIgnore
    private boolean subjectDirtyFlag;
    
    /**
     * 内容
     */
    private String body;

    @JsonIgnore
    private boolean bodyDirtyFlag;
    
    /**
     * 显示名称
     */
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;
    
    /**
     * 已发布
     */
    private String website_published;

    @JsonIgnore
    private boolean website_publishedDirtyFlag;
    
    /**
     * 附件
     */
    private String attachment_ids;

    @JsonIgnore
    private boolean attachment_idsDirtyFlag;
    
    /**
     * 下级消息
     */
    private String child_ids;

    @JsonIgnore
    private boolean child_idsDirtyFlag;
    
    /**
     * 评级值
     */
    private Double rating_value;

    @JsonIgnore
    private boolean rating_valueDirtyFlag;
    
    /**
     * 添加签名
     */
    private String add_sign;

    @JsonIgnore
    private boolean add_signDirtyFlag;
    
    /**
     * 收藏夹
     */
    private String starred_partner_ids;

    @JsonIgnore
    private boolean starred_partner_idsDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;
    
    /**
     * 日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;
    
    /**
     * 有误差
     */
    private String has_error;

    @JsonIgnore
    private boolean has_errorDirtyFlag;
    
    /**
     * 类型
     */
    private String message_type;

    @JsonIgnore
    private boolean message_typeDirtyFlag;
    
    /**
     * 作者
     */
    private String author_id_text;

    @JsonIgnore
    private boolean author_id_textDirtyFlag;
    
    /**
     * 创建人
     */
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;
    
    /**
     * 邮件活动类型
     */
    private String mail_activity_type_id_text;

    @JsonIgnore
    private boolean mail_activity_type_id_textDirtyFlag;
    
    /**
     * 管理员
     */
    private String moderator_id_text;

    @JsonIgnore
    private boolean moderator_id_textDirtyFlag;
    
    /**
     * 最后更新者
     */
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;
    
    /**
     * 子类型
     */
    private String subtype_id_text;

    @JsonIgnore
    private boolean subtype_id_textDirtyFlag;
    
    /**
     * 作者头像
     */
    private byte[] author_avatar;

    @JsonIgnore
    private boolean author_avatarDirtyFlag;
    
    /**
     * 最后更新者
     */
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;
    
    /**
     * 邮件活动类型
     */
    private Integer mail_activity_type_id;

    @JsonIgnore
    private boolean mail_activity_type_idDirtyFlag;
    
    /**
     * 管理员
     */
    private Integer moderator_id;

    @JsonIgnore
    private boolean moderator_idDirtyFlag;
    
    /**
     * 创建人
     */
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;
    
    /**
     * 作者
     */
    private Integer author_id;

    @JsonIgnore
    private boolean author_idDirtyFlag;
    
    /**
     * 上级消息
     */
    private Integer parent_id;

    @JsonIgnore
    private boolean parent_idDirtyFlag;
    
    /**
     * 子类型
     */
    private Integer subtype_id;

    @JsonIgnore
    private boolean subtype_idDirtyFlag;
    

    /**
     * 获取 [待处理]
     */
    @JsonProperty("needaction")
    public String getNeedaction(){
        return this.needaction ;
    }

    /**
     * 设置 [待处理]
     */
    @JsonProperty("needaction")
    public void setNeedaction(String  needaction){
        this.needaction = needaction ;
        this.needactionDirtyFlag = true ;
    }

    /**
     * 获取 [待处理]脏标记
     */
    @JsonIgnore
    public boolean getNeedactionDirtyFlag(){
        return this.needactionDirtyFlag ;
    }

    /**
     * 获取 [布局]
     */
    @JsonProperty("layout")
    public String getLayout(){
        return this.layout ;
    }

    /**
     * 设置 [布局]
     */
    @JsonProperty("layout")
    public void setLayout(String  layout){
        this.layout = layout ;
        this.layoutDirtyFlag = true ;
    }

    /**
     * 获取 [布局]脏标记
     */
    @JsonIgnore
    public boolean getLayoutDirtyFlag(){
        return this.layoutDirtyFlag ;
    }

    /**
     * 获取 [相关的文档模型]
     */
    @JsonProperty("model")
    public String getModel(){
        return this.model ;
    }

    /**
     * 设置 [相关的文档模型]
     */
    @JsonProperty("model")
    public void setModel(String  model){
        this.model = model ;
        this.modelDirtyFlag = true ;
    }

    /**
     * 获取 [相关的文档模型]脏标记
     */
    @JsonIgnore
    public boolean getModelDirtyFlag(){
        return this.modelDirtyFlag ;
    }

    /**
     * 获取 [无响应]
     */
    @JsonProperty("no_auto_thread")
    public String getNo_auto_thread(){
        return this.no_auto_thread ;
    }

    /**
     * 设置 [无响应]
     */
    @JsonProperty("no_auto_thread")
    public void setNo_auto_thread(String  no_auto_thread){
        this.no_auto_thread = no_auto_thread ;
        this.no_auto_threadDirtyFlag = true ;
    }

    /**
     * 获取 [无响应]脏标记
     */
    @JsonIgnore
    public boolean getNo_auto_threadDirtyFlag(){
        return this.no_auto_threadDirtyFlag ;
    }

    /**
     * 获取 [收件人]
     */
    @JsonProperty("partner_ids")
    public String getPartner_ids(){
        return this.partner_ids ;
    }

    /**
     * 设置 [收件人]
     */
    @JsonProperty("partner_ids")
    public void setPartner_ids(String  partner_ids){
        this.partner_ids = partner_ids ;
        this.partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [收件人]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idsDirtyFlag(){
        return this.partner_idsDirtyFlag ;
    }

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }

    /**
     * 获取 [消息记录名称]
     */
    @JsonProperty("record_name")
    public String getRecord_name(){
        return this.record_name ;
    }

    /**
     * 设置 [消息记录名称]
     */
    @JsonProperty("record_name")
    public void setRecord_name(String  record_name){
        this.record_name = record_name ;
        this.record_nameDirtyFlag = true ;
    }

    /**
     * 获取 [消息记录名称]脏标记
     */
    @JsonIgnore
    public boolean getRecord_nameDirtyFlag(){
        return this.record_nameDirtyFlag ;
    }

    /**
     * 获取 [消息ID]
     */
    @JsonProperty("message_id")
    public String getMessage_id(){
        return this.message_id ;
    }

    /**
     * 设置 [消息ID]
     */
    @JsonProperty("message_id")
    public void setMessage_id(String  message_id){
        this.message_id = message_id ;
        this.message_idDirtyFlag = true ;
    }

    /**
     * 获取 [消息ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idDirtyFlag(){
        return this.message_idDirtyFlag ;
    }

    /**
     * 获取 [追踪值]
     */
    @JsonProperty("tracking_value_ids")
    public String getTracking_value_ids(){
        return this.tracking_value_ids ;
    }

    /**
     * 设置 [追踪值]
     */
    @JsonProperty("tracking_value_ids")
    public void setTracking_value_ids(String  tracking_value_ids){
        this.tracking_value_ids = tracking_value_ids ;
        this.tracking_value_idsDirtyFlag = true ;
    }

    /**
     * 获取 [追踪值]脏标记
     */
    @JsonIgnore
    public boolean getTracking_value_idsDirtyFlag(){
        return this.tracking_value_idsDirtyFlag ;
    }

    /**
     * 获取 [管理状态]
     */
    @JsonProperty("moderation_status")
    public String getModeration_status(){
        return this.moderation_status ;
    }

    /**
     * 设置 [管理状态]
     */
    @JsonProperty("moderation_status")
    public void setModeration_status(String  moderation_status){
        this.moderation_status = moderation_status ;
        this.moderation_statusDirtyFlag = true ;
    }

    /**
     * 获取 [管理状态]脏标记
     */
    @JsonIgnore
    public boolean getModeration_statusDirtyFlag(){
        return this.moderation_statusDirtyFlag ;
    }

    /**
     * 获取 [通知]
     */
    @JsonProperty("notification_ids")
    public String getNotification_ids(){
        return this.notification_ids ;
    }

    /**
     * 设置 [通知]
     */
    @JsonProperty("notification_ids")
    public void setNotification_ids(String  notification_ids){
        this.notification_ids = notification_ids ;
        this.notification_idsDirtyFlag = true ;
    }

    /**
     * 获取 [通知]脏标记
     */
    @JsonIgnore
    public boolean getNotification_idsDirtyFlag(){
        return this.notification_idsDirtyFlag ;
    }

    /**
     * 获取 [从]
     */
    @JsonProperty("email_from")
    public String getEmail_from(){
        return this.email_from ;
    }

    /**
     * 设置 [从]
     */
    @JsonProperty("email_from")
    public void setEmail_from(String  email_from){
        this.email_from = email_from ;
        this.email_fromDirtyFlag = true ;
    }

    /**
     * 获取 [从]脏标记
     */
    @JsonIgnore
    public boolean getEmail_fromDirtyFlag(){
        return this.email_fromDirtyFlag ;
    }

    /**
     * 获取 [渠道]
     */
    @JsonProperty("channel_ids")
    public String getChannel_ids(){
        return this.channel_ids ;
    }

    /**
     * 设置 [渠道]
     */
    @JsonProperty("channel_ids")
    public void setChannel_ids(String  channel_ids){
        this.channel_ids = channel_ids ;
        this.channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [渠道]脏标记
     */
    @JsonIgnore
    public boolean getChannel_idsDirtyFlag(){
        return this.channel_idsDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }

    /**
     * 获取 [说明]
     */
    @JsonProperty("description")
    public String getDescription(){
        return this.description ;
    }

    /**
     * 设置 [说明]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [说明]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return this.descriptionDirtyFlag ;
    }

    /**
     * 获取 [加星的邮件]
     */
    @JsonProperty("starred")
    public String getStarred(){
        return this.starred ;
    }

    /**
     * 设置 [加星的邮件]
     */
    @JsonProperty("starred")
    public void setStarred(String  starred){
        this.starred = starred ;
        this.starredDirtyFlag = true ;
    }

    /**
     * 获取 [加星的邮件]脏标记
     */
    @JsonIgnore
    public boolean getStarredDirtyFlag(){
        return this.starredDirtyFlag ;
    }

    /**
     * 获取 [相关评级]
     */
    @JsonProperty("rating_ids")
    public String getRating_ids(){
        return this.rating_ids ;
    }

    /**
     * 设置 [相关评级]
     */
    @JsonProperty("rating_ids")
    public void setRating_ids(String  rating_ids){
        this.rating_ids = rating_ids ;
        this.rating_idsDirtyFlag = true ;
    }

    /**
     * 获取 [相关评级]脏标记
     */
    @JsonIgnore
    public boolean getRating_idsDirtyFlag(){
        return this.rating_idsDirtyFlag ;
    }

    /**
     * 获取 [待处理的业务伙伴]
     */
    @JsonProperty("needaction_partner_ids")
    public String getNeedaction_partner_ids(){
        return this.needaction_partner_ids ;
    }

    /**
     * 设置 [待处理的业务伙伴]
     */
    @JsonProperty("needaction_partner_ids")
    public void setNeedaction_partner_ids(String  needaction_partner_ids){
        this.needaction_partner_ids = needaction_partner_ids ;
        this.needaction_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [待处理的业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getNeedaction_partner_idsDirtyFlag(){
        return this.needaction_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [回复 至]
     */
    @JsonProperty("reply_to")
    public String getReply_to(){
        return this.reply_to ;
    }

    /**
     * 设置 [回复 至]
     */
    @JsonProperty("reply_to")
    public void setReply_to(String  reply_to){
        this.reply_to = reply_to ;
        this.reply_toDirtyFlag = true ;
    }

    /**
     * 获取 [回复 至]脏标记
     */
    @JsonIgnore
    public boolean getReply_toDirtyFlag(){
        return this.reply_toDirtyFlag ;
    }

    /**
     * 获取 [邮件发送服务器]
     */
    @JsonProperty("mail_server_id")
    public Integer getMail_server_id(){
        return this.mail_server_id ;
    }

    /**
     * 设置 [邮件发送服务器]
     */
    @JsonProperty("mail_server_id")
    public void setMail_server_id(Integer  mail_server_id){
        this.mail_server_id = mail_server_id ;
        this.mail_server_idDirtyFlag = true ;
    }

    /**
     * 获取 [邮件发送服务器]脏标记
     */
    @JsonIgnore
    public boolean getMail_server_idDirtyFlag(){
        return this.mail_server_idDirtyFlag ;
    }

    /**
     * 获取 [相关文档编号]
     */
    @JsonProperty("res_id")
    public Integer getRes_id(){
        return this.res_id ;
    }

    /**
     * 设置 [相关文档编号]
     */
    @JsonProperty("res_id")
    public void setRes_id(Integer  res_id){
        this.res_id = res_id ;
        this.res_idDirtyFlag = true ;
    }

    /**
     * 获取 [相关文档编号]脏标记
     */
    @JsonIgnore
    public boolean getRes_idDirtyFlag(){
        return this.res_idDirtyFlag ;
    }

    /**
     * 获取 [需审核]
     */
    @JsonProperty("need_moderation")
    public String getNeed_moderation(){
        return this.need_moderation ;
    }

    /**
     * 设置 [需审核]
     */
    @JsonProperty("need_moderation")
    public void setNeed_moderation(String  need_moderation){
        this.need_moderation = need_moderation ;
        this.need_moderationDirtyFlag = true ;
    }

    /**
     * 获取 [需审核]脏标记
     */
    @JsonIgnore
    public boolean getNeed_moderationDirtyFlag(){
        return this.need_moderationDirtyFlag ;
    }

    /**
     * 获取 [主题]
     */
    @JsonProperty("subject")
    public String getSubject(){
        return this.subject ;
    }

    /**
     * 设置 [主题]
     */
    @JsonProperty("subject")
    public void setSubject(String  subject){
        this.subject = subject ;
        this.subjectDirtyFlag = true ;
    }

    /**
     * 获取 [主题]脏标记
     */
    @JsonIgnore
    public boolean getSubjectDirtyFlag(){
        return this.subjectDirtyFlag ;
    }

    /**
     * 获取 [内容]
     */
    @JsonProperty("body")
    public String getBody(){
        return this.body ;
    }

    /**
     * 设置 [内容]
     */
    @JsonProperty("body")
    public void setBody(String  body){
        this.body = body ;
        this.bodyDirtyFlag = true ;
    }

    /**
     * 获取 [内容]脏标记
     */
    @JsonIgnore
    public boolean getBodyDirtyFlag(){
        return this.bodyDirtyFlag ;
    }

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }

    /**
     * 获取 [已发布]
     */
    @JsonProperty("website_published")
    public String getWebsite_published(){
        return this.website_published ;
    }

    /**
     * 设置 [已发布]
     */
    @JsonProperty("website_published")
    public void setWebsite_published(String  website_published){
        this.website_published = website_published ;
        this.website_publishedDirtyFlag = true ;
    }

    /**
     * 获取 [已发布]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_publishedDirtyFlag(){
        return this.website_publishedDirtyFlag ;
    }

    /**
     * 获取 [附件]
     */
    @JsonProperty("attachment_ids")
    public String getAttachment_ids(){
        return this.attachment_ids ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("attachment_ids")
    public void setAttachment_ids(String  attachment_ids){
        this.attachment_ids = attachment_ids ;
        this.attachment_idsDirtyFlag = true ;
    }

    /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getAttachment_idsDirtyFlag(){
        return this.attachment_idsDirtyFlag ;
    }

    /**
     * 获取 [下级消息]
     */
    @JsonProperty("child_ids")
    public String getChild_ids(){
        return this.child_ids ;
    }

    /**
     * 设置 [下级消息]
     */
    @JsonProperty("child_ids")
    public void setChild_ids(String  child_ids){
        this.child_ids = child_ids ;
        this.child_idsDirtyFlag = true ;
    }

    /**
     * 获取 [下级消息]脏标记
     */
    @JsonIgnore
    public boolean getChild_idsDirtyFlag(){
        return this.child_idsDirtyFlag ;
    }

    /**
     * 获取 [评级值]
     */
    @JsonProperty("rating_value")
    public Double getRating_value(){
        return this.rating_value ;
    }

    /**
     * 设置 [评级值]
     */
    @JsonProperty("rating_value")
    public void setRating_value(Double  rating_value){
        this.rating_value = rating_value ;
        this.rating_valueDirtyFlag = true ;
    }

    /**
     * 获取 [评级值]脏标记
     */
    @JsonIgnore
    public boolean getRating_valueDirtyFlag(){
        return this.rating_valueDirtyFlag ;
    }

    /**
     * 获取 [添加签名]
     */
    @JsonProperty("add_sign")
    public String getAdd_sign(){
        return this.add_sign ;
    }

    /**
     * 设置 [添加签名]
     */
    @JsonProperty("add_sign")
    public void setAdd_sign(String  add_sign){
        this.add_sign = add_sign ;
        this.add_signDirtyFlag = true ;
    }

    /**
     * 获取 [添加签名]脏标记
     */
    @JsonIgnore
    public boolean getAdd_signDirtyFlag(){
        return this.add_signDirtyFlag ;
    }

    /**
     * 获取 [收藏夹]
     */
    @JsonProperty("starred_partner_ids")
    public String getStarred_partner_ids(){
        return this.starred_partner_ids ;
    }

    /**
     * 设置 [收藏夹]
     */
    @JsonProperty("starred_partner_ids")
    public void setStarred_partner_ids(String  starred_partner_ids){
        this.starred_partner_ids = starred_partner_ids ;
        this.starred_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [收藏夹]脏标记
     */
    @JsonIgnore
    public boolean getStarred_partner_idsDirtyFlag(){
        return this.starred_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }

    /**
     * 获取 [日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }

    /**
     * 获取 [有误差]
     */
    @JsonProperty("has_error")
    public String getHas_error(){
        return this.has_error ;
    }

    /**
     * 设置 [有误差]
     */
    @JsonProperty("has_error")
    public void setHas_error(String  has_error){
        this.has_error = has_error ;
        this.has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [有误差]脏标记
     */
    @JsonIgnore
    public boolean getHas_errorDirtyFlag(){
        return this.has_errorDirtyFlag ;
    }

    /**
     * 获取 [类型]
     */
    @JsonProperty("message_type")
    public String getMessage_type(){
        return this.message_type ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("message_type")
    public void setMessage_type(String  message_type){
        this.message_type = message_type ;
        this.message_typeDirtyFlag = true ;
    }

    /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getMessage_typeDirtyFlag(){
        return this.message_typeDirtyFlag ;
    }

    /**
     * 获取 [作者]
     */
    @JsonProperty("author_id_text")
    public String getAuthor_id_text(){
        return this.author_id_text ;
    }

    /**
     * 设置 [作者]
     */
    @JsonProperty("author_id_text")
    public void setAuthor_id_text(String  author_id_text){
        this.author_id_text = author_id_text ;
        this.author_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [作者]脏标记
     */
    @JsonIgnore
    public boolean getAuthor_id_textDirtyFlag(){
        return this.author_id_textDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [邮件活动类型]
     */
    @JsonProperty("mail_activity_type_id_text")
    public String getMail_activity_type_id_text(){
        return this.mail_activity_type_id_text ;
    }

    /**
     * 设置 [邮件活动类型]
     */
    @JsonProperty("mail_activity_type_id_text")
    public void setMail_activity_type_id_text(String  mail_activity_type_id_text){
        this.mail_activity_type_id_text = mail_activity_type_id_text ;
        this.mail_activity_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [邮件活动类型]脏标记
     */
    @JsonIgnore
    public boolean getMail_activity_type_id_textDirtyFlag(){
        return this.mail_activity_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [管理员]
     */
    @JsonProperty("moderator_id_text")
    public String getModerator_id_text(){
        return this.moderator_id_text ;
    }

    /**
     * 设置 [管理员]
     */
    @JsonProperty("moderator_id_text")
    public void setModerator_id_text(String  moderator_id_text){
        this.moderator_id_text = moderator_id_text ;
        this.moderator_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [管理员]脏标记
     */
    @JsonIgnore
    public boolean getModerator_id_textDirtyFlag(){
        return this.moderator_id_textDirtyFlag ;
    }

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [子类型]
     */
    @JsonProperty("subtype_id_text")
    public String getSubtype_id_text(){
        return this.subtype_id_text ;
    }

    /**
     * 设置 [子类型]
     */
    @JsonProperty("subtype_id_text")
    public void setSubtype_id_text(String  subtype_id_text){
        this.subtype_id_text = subtype_id_text ;
        this.subtype_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [子类型]脏标记
     */
    @JsonIgnore
    public boolean getSubtype_id_textDirtyFlag(){
        return this.subtype_id_textDirtyFlag ;
    }

    /**
     * 获取 [作者头像]
     */
    @JsonProperty("author_avatar")
    public byte[] getAuthor_avatar(){
        return this.author_avatar ;
    }

    /**
     * 设置 [作者头像]
     */
    @JsonProperty("author_avatar")
    public void setAuthor_avatar(byte[]  author_avatar){
        this.author_avatar = author_avatar ;
        this.author_avatarDirtyFlag = true ;
    }

    /**
     * 获取 [作者头像]脏标记
     */
    @JsonIgnore
    public boolean getAuthor_avatarDirtyFlag(){
        return this.author_avatarDirtyFlag ;
    }

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }

    /**
     * 获取 [邮件活动类型]
     */
    @JsonProperty("mail_activity_type_id")
    public Integer getMail_activity_type_id(){
        return this.mail_activity_type_id ;
    }

    /**
     * 设置 [邮件活动类型]
     */
    @JsonProperty("mail_activity_type_id")
    public void setMail_activity_type_id(Integer  mail_activity_type_id){
        this.mail_activity_type_id = mail_activity_type_id ;
        this.mail_activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [邮件活动类型]脏标记
     */
    @JsonIgnore
    public boolean getMail_activity_type_idDirtyFlag(){
        return this.mail_activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [管理员]
     */
    @JsonProperty("moderator_id")
    public Integer getModerator_id(){
        return this.moderator_id ;
    }

    /**
     * 设置 [管理员]
     */
    @JsonProperty("moderator_id")
    public void setModerator_id(Integer  moderator_id){
        this.moderator_id = moderator_id ;
        this.moderator_idDirtyFlag = true ;
    }

    /**
     * 获取 [管理员]脏标记
     */
    @JsonIgnore
    public boolean getModerator_idDirtyFlag(){
        return this.moderator_idDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }

    /**
     * 获取 [作者]
     */
    @JsonProperty("author_id")
    public Integer getAuthor_id(){
        return this.author_id ;
    }

    /**
     * 设置 [作者]
     */
    @JsonProperty("author_id")
    public void setAuthor_id(Integer  author_id){
        this.author_id = author_id ;
        this.author_idDirtyFlag = true ;
    }

    /**
     * 获取 [作者]脏标记
     */
    @JsonIgnore
    public boolean getAuthor_idDirtyFlag(){
        return this.author_idDirtyFlag ;
    }

    /**
     * 获取 [上级消息]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return this.parent_id ;
    }

    /**
     * 设置 [上级消息]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

    /**
     * 获取 [上级消息]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return this.parent_idDirtyFlag ;
    }

    /**
     * 获取 [子类型]
     */
    @JsonProperty("subtype_id")
    public Integer getSubtype_id(){
        return this.subtype_id ;
    }

    /**
     * 设置 [子类型]
     */
    @JsonProperty("subtype_id")
    public void setSubtype_id(Integer  subtype_id){
        this.subtype_id = subtype_id ;
        this.subtype_idDirtyFlag = true ;
    }

    /**
     * 获取 [子类型]脏标记
     */
    @JsonIgnore
    public boolean getSubtype_idDirtyFlag(){
        return this.subtype_idDirtyFlag ;
    }



}
