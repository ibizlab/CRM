package cn.ibizlab.odoo.web.odoo_crm.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.web.odoo_crm.domain.Crm_team;
import cn.ibizlab.odoo.web.odoo_crm.filter.*;
import cn.ibizlab.odoo.web.odoo_crm.feign.Crm_teamFeignClient;
import cn.ibizlab.odoo.web.WebApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Crm_teamService {

    Crm_teamFeignClient client;

    @Autowired
    public Crm_teamService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Crm_teamFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Crm_teamFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public Crm_team get( Integer crm_team_id) {
        return client.get( crm_team_id);
    }

    public boolean checkKey(Crm_team crm_team) {
        return client.checkKey(crm_team);
    }

    public Crm_team getDraft(Integer crm_team_id, Crm_team crm_team) {
        return client.getDraft(crm_team_id, crm_team);
    }

    public Crm_team removeBatch(Integer crm_team_id, Crm_team crm_team) {
        return client.removeBatch(crm_team_id, crm_team);
    }

    public Crm_team createBatch(Integer crm_team_id, Crm_team crm_team) {
        return client.createBatch(crm_team_id, crm_team);
    }

    public Crm_team update(Integer crm_team_id, Crm_team crm_team) {
        return client.update(crm_team_id, crm_team);
    }

    public boolean remove( Integer crm_team_id) {
        return client.remove( crm_team_id);
    }

	public Crm_team create(Crm_team crm_team) {
        return client.create(crm_team);
    }

    public Crm_team updateBatch(Integer crm_team_id, Crm_team crm_team) {
        return client.updateBatch(crm_team_id, crm_team);
    }

    public Crm_team save(Integer crm_team_id, Crm_team crm_team) {
        return client.save(crm_team_id, crm_team);
    }

	public Page<Crm_team> fetchDefault(Crm_teamSearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }

}
