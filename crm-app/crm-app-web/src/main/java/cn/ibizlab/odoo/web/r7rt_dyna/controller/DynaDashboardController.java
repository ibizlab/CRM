package cn.ibizlab.odoo.web.r7rt_dyna.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.web.r7rt_dyna.service.DynaDashboardService;
import cn.ibizlab.odoo.web.r7rt_dyna.domain.DynaDashboard;
import cn.ibizlab.odoo.web.r7rt_dyna.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class DynaDashboardController {
	@Autowired
    DynaDashboardService dynadashboardservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/dynadashboards/{dynadashboard_id}/save")
    @PreAuthorize("@dynadashboard_pms.check(#dynadashboard_id,'')")
    public ResponseEntity<DynaDashboard> save(@PathVariable("dynadashboard_id") String dynadashboard_id, @RequestBody DynaDashboard dynadashboard) {
        DynaDashboard dynadashboard2 = dynadashboardservice.save(dynadashboard_id, dynadashboard);
        return ResponseEntity.status(HttpStatus.OK).body(dynadashboard2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/dynadashboards/{dynadashboard_id}")
    @PreAuthorize("@dynadashboard_pms.check(#dynadashboard_id,'UPDATE')")
    public ResponseEntity<DynaDashboard> update(@PathVariable("dynadashboard_id") String dynadashboard_id, @RequestBody DynaDashboard dynadashboard) {
        DynaDashboard dynadashboard2 = dynadashboardservice.update(dynadashboard_id, dynadashboard);
        return ResponseEntity.status(HttpStatus.OK).body(dynadashboard2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/dynadashboards/{dynadashboard_id}")
    @PreAuthorize("@dynadashboard_pms.check(#dynadashboard_id,'UPDATE')")
    public ResponseEntity<DynaDashboard> api_update(@PathVariable("dynadashboard_id") String dynadashboard_id, @RequestBody DynaDashboard dynadashboard) {
        DynaDashboard dynadashboard2 = dynadashboardservice.update(dynadashboard_id, dynadashboard);
        return ResponseEntity.status(HttpStatus.OK).body(dynadashboard2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/dynadashboards/{dynadashboard_id}")
    @PreAuthorize("@dynadashboard_pms.check(#dynadashboard_id,'READ')")
    public ResponseEntity<DynaDashboard> get(@PathVariable("dynadashboard_id") String dynadashboard_id) {
        DynaDashboard dynadashboard = dynadashboardservice.get( dynadashboard_id);
        return ResponseEntity.status(HttpStatus.OK).body(dynadashboard);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/dynadashboards/{dynadashboard_id}")
    @PreAuthorize("@dynadashboard_pms.check(#dynadashboard_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("dynadashboard_id") String dynadashboard_id) {
        boolean b = dynadashboardservice.remove( dynadashboard_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/dynadashboards/checkkey")
    @PreAuthorize("@dynadashboard_pms.check('')")
    public ResponseEntity<Boolean> checkKey(@RequestBody DynaDashboard dynadashboard) {
        boolean b = dynadashboardservice.checkKey(dynadashboard);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/dynadashboards/getdraft")
    @PreAuthorize("@dynadashboard_pms.check('CREATE')")
    public ResponseEntity<DynaDashboard> getDraft() {
        //DynaDashboard dynadashboard = dynadashboardservice.getDraft( dynadashboard_id);
        return ResponseEntity.status(HttpStatus.OK).body(new DynaDashboard());
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/dynadashboards")
    @PreAuthorize("@dynadashboard_pms.check('CREATE')")
    public ResponseEntity<DynaDashboard> create(@RequestBody DynaDashboard dynadashboard) {
        DynaDashboard dynadashboard2 = dynadashboardservice.create(dynadashboard);
        return ResponseEntity.status(HttpStatus.OK).body(dynadashboard2);
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/dynadashboards/fetchdefault")
    @PreAuthorize("@dynadashboard_pms.check('READ')")
	public ResponseEntity<List<DynaDashboard>> fetchDefault(DynaDashboardSearchContext searchContext,Pageable pageable) {
        
        Page<DynaDashboard> page = dynadashboardservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}
