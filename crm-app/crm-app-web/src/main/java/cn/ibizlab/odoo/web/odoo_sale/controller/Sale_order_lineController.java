package cn.ibizlab.odoo.web.odoo_sale.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.web.odoo_sale.service.Sale_order_lineService;
import cn.ibizlab.odoo.web.odoo_sale.domain.Sale_order_line;
import cn.ibizlab.odoo.web.odoo_sale.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Sale_order_lineController {
	@Autowired
    Sale_order_lineService sale_order_lineservice;




    public ResponseEntity<Sale_order_line> createTempMajorByRes_partnerSale_order(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }

    public ResponseEntity<Sale_order_line> getDraftByRes_partnerSale_order() {
        return null;
    }


    public ResponseEntity<Sale_order_line> getDraftTempMajorByRes_partnerSale_order(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }

    public ResponseEntity<Sale_order_line> getByRes_partnerSale_order(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id) {
        return null;
    }

    public ResponseEntity<Boolean> removeByRes_partnerSale_order(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id) {
        return null;
    }


    public ResponseEntity<Sale_order_line> removeTempByRes_partnerSale_order(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }


    public ResponseEntity<Sale_order_line> createBatchByRes_partnerSale_order(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }


    public ResponseEntity<Sale_order_line> removeTempMajorByRes_partnerSale_order(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }

    public ResponseEntity<Sale_order_line> updateByRes_partnerSale_order(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }


    public ResponseEntity<Sale_order_line> getTempByRes_partnerSale_order(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }

    public ResponseEntity<Sale_order_line> createByRes_partnerSale_order(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }


    public ResponseEntity<Sale_order_line> updateTempByRes_partnerSale_order(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }

    public ResponseEntity<Boolean> checkKeyByRes_partnerSale_order(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }


    public ResponseEntity<Sale_order_line> createTempByRes_partnerSale_order(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }


    public ResponseEntity<Sale_order_line> getTempMajorByRes_partnerSale_order(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }


    public ResponseEntity<Sale_order_line> removeBatchByRes_partnerSale_order(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }


    public ResponseEntity<Sale_order_line> saveByRes_partnerSale_order(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }


    public ResponseEntity<Sale_order_line> updateTempMajorByRes_partnerSale_order(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }


    public ResponseEntity<Sale_order_line> getDraftTempByRes_partnerSale_order(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }


    public ResponseEntity<Sale_order_line> updateBatchByRes_partnerSale_order(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }

	public ResponseEntity<List<Sale_order_line>> fetchDefaultByRes_partnerSale_order() {
        return null;
    }



    public ResponseEntity<Sale_order_line> createTempMajorBySale_order(@PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }

    public ResponseEntity<Sale_order_line> getDraftBySale_order() {
        return null;
    }


    public ResponseEntity<Sale_order_line> getDraftTempMajorBySale_order(@PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }

    public ResponseEntity<Sale_order_line> getBySale_order(@PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id) {
        return null;
    }

    public ResponseEntity<Boolean> removeBySale_order(@PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id) {
        return null;
    }


    public ResponseEntity<Sale_order_line> removeTempBySale_order(@PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }


    public ResponseEntity<Sale_order_line> createBatchBySale_order(@PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }


    public ResponseEntity<Sale_order_line> removeTempMajorBySale_order(@PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }

    public ResponseEntity<Sale_order_line> updateBySale_order(@PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }


    public ResponseEntity<Sale_order_line> getTempBySale_order(@PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }

    public ResponseEntity<Sale_order_line> createBySale_order(@PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }


    public ResponseEntity<Sale_order_line> updateTempBySale_order(@PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }

    public ResponseEntity<Boolean> checkKeyBySale_order(@PathVariable("sale_order_id") Integer sale_order_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }


    public ResponseEntity<Sale_order_line> createTempBySale_order(@PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }


    public ResponseEntity<Sale_order_line> getTempMajorBySale_order(@PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }


    public ResponseEntity<Sale_order_line> removeBatchBySale_order(@PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }


    public ResponseEntity<Sale_order_line> saveBySale_order(@PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }


    public ResponseEntity<Sale_order_line> updateTempMajorBySale_order(@PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }


    public ResponseEntity<Sale_order_line> getDraftTempBySale_order(@PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }


    public ResponseEntity<Sale_order_line> updateBatchBySale_order(@PathVariable("sale_order_id") Integer sale_order_id, @PathVariable("sale_order_line_id") Integer sale_order_line_id, @RequestBody Sale_order_line sale_order_line) {
        return null;
    }

	public ResponseEntity<List<Sale_order_line>> fetchDefaultBySale_order() {
        return null;
    }


}
