package cn.ibizlab.odoo.web.odoo_sale.filter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Data
@IBIZLog
public class Sale_orderSearchContext extends SearchContext implements Serializable {

	public String n_picking_policy_eq;//[送货策略]
	public String n_activity_state_eq;//[活动状态]
	public String n_invoice_status_eq;//[发票状态]
	public String n_state_eq;//[状态]
	public String n_name_like;//[订单关联]
	public String n_partner_invoice_id_text_eq;//[发票地址]
	public String n_partner_invoice_id_text_like;//[发票地址]
	public String n_incoterm_text_eq;//[贸易条款]
	public String n_incoterm_text_like;//[贸易条款]
	public String n_team_id_text_eq;//[销售团队]
	public String n_team_id_text_like;//[销售团队]
	public String n_analytic_account_id_text_eq;//[分析账户]
	public String n_analytic_account_id_text_like;//[分析账户]
	public String n_partner_id_text_eq;//[客户]
	public String n_partner_id_text_like;//[客户]
	public String n_campaign_id_text_eq;//[营销]
	public String n_campaign_id_text_like;//[营销]
	public String n_create_uid_text_eq;//[创建人]
	public String n_create_uid_text_like;//[创建人]
	public String n_company_id_text_eq;//[公司]
	public String n_company_id_text_like;//[公司]
	public String n_partner_shipping_id_text_eq;//[送货地址]
	public String n_partner_shipping_id_text_like;//[送货地址]
	public String n_warehouse_id_text_eq;//[仓库]
	public String n_warehouse_id_text_like;//[仓库]
	public String n_user_id_text_eq;//[销售员]
	public String n_user_id_text_like;//[销售员]
	public String n_opportunity_id_text_eq;//[商机]
	public String n_opportunity_id_text_like;//[商机]
	public String n_source_id_text_eq;//[来源]
	public String n_source_id_text_like;//[来源]
	public String n_medium_id_text_eq;//[媒体]
	public String n_medium_id_text_like;//[媒体]
	public String n_fiscal_position_id_text_eq;//[税科目调整]
	public String n_fiscal_position_id_text_like;//[税科目调整]
	public String n_write_uid_text_eq;//[最后更新人]
	public String n_write_uid_text_like;//[最后更新人]
	public String n_pricelist_id_text_eq;//[价格表]
	public String n_pricelist_id_text_like;//[价格表]
	public String n_payment_term_id_text_eq;//[付款条款]
	public String n_payment_term_id_text_like;//[付款条款]
	public String n_sale_order_template_id_text_eq;//[报价单模板]
	public String n_sale_order_template_id_text_like;//[报价单模板]
	public Integer n_write_uid_eq;//[最后更新人]
	public Integer n_team_id_eq;//[销售团队]
	public Integer n_partner_invoice_id_eq;//[发票地址]
	public Integer n_fiscal_position_id_eq;//[税科目调整]
	public Integer n_partner_id_eq;//[客户]
	public Integer n_campaign_id_eq;//[营销]
	public Integer n_source_id_eq;//[来源]
	public Integer n_sale_order_template_id_eq;//[报价单模板]
	public Integer n_user_id_eq;//[销售员]
	public Integer n_company_id_eq;//[公司]
	public Integer n_medium_id_eq;//[媒体]
	public Integer n_analytic_account_id_eq;//[分析账户]
	public Integer n_payment_term_id_eq;//[付款条款]
	public Integer n_partner_shipping_id_eq;//[送货地址]
	public Integer n_opportunity_id_eq;//[商机]
	public Integer n_incoterm_eq;//[贸易条款]
	public Integer n_pricelist_id_eq;//[价格表]
	public Integer n_create_uid_eq;//[创建人]
	public Integer n_warehouse_id_eq;//[仓库]

}