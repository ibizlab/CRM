package cn.ibizlab.odoo.web.odoo_account.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import cn.ibizlab.odoo.web.odoo_account.service.Account_invoiceService;
import cn.ibizlab.odoo.web.odoo_account.domain.Account_invoice;
import cn.ibizlab.odoo.web.odoo_account.filter.*;
import cn.ibizlab.odoo.util.log.IBIZLog;


@Slf4j
@IBIZLog
@RestController
@RequestMapping(value = "")
public class Account_invoiceController {
	@Autowired
    Account_invoiceService account_invoiceservice;


    //@GlobalTransactional
    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoices/{account_invoice_id}")
    @PreAuthorize("@account_invoice_pms.check(#account_invoice_id,'DELETE')")
    public ResponseEntity<Boolean> remove(@PathVariable("account_invoice_id") Integer account_invoice_id) {
        boolean b = account_invoiceservice.remove( account_invoice_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoices/checkkey")
    @PreAuthorize("@account_invoice_pms.check('')")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_invoice account_invoice) {
        boolean b = account_invoiceservice.checkKey(account_invoice);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoices")
    @PreAuthorize("@account_invoice_pms.check('CREATE')")
    public ResponseEntity<Account_invoice> create(@RequestBody Account_invoice account_invoice) {
        Account_invoice account_invoice2 = account_invoiceservice.create(account_invoice);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoices/{account_invoice_id}")
    @PreAuthorize("@account_invoice_pms.check(#account_invoice_id,'UPDATE')")
    public ResponseEntity<Account_invoice> update(@PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoice account_invoice) {
        Account_invoice account_invoice2 = account_invoiceservice.update(account_invoice_id, account_invoice);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/api/account_invoices/{account_invoice_id}")
    @PreAuthorize("@account_invoice_pms.check(#account_invoice_id,'UPDATE')")
    public ResponseEntity<Account_invoice> api_update(@PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoice account_invoice) {
        Account_invoice account_invoice2 = account_invoiceservice.update(account_invoice_id, account_invoice);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoices/{account_invoice_id}/updatebatch")
    @PreAuthorize("@account_invoice_pms.check(#account_invoice_id,'UPDATE')")
    public ResponseEntity<Account_invoice> updateBatch(@PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoice account_invoice) {
        Account_invoice account_invoice2 = account_invoiceservice.updateBatch(account_invoice_id, account_invoice);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoices/{account_invoice_id}")
    @PreAuthorize("@account_invoice_pms.check(#account_invoice_id,'READ')")
    public ResponseEntity<Account_invoice> get(@PathVariable("account_invoice_id") Integer account_invoice_id) {
        Account_invoice account_invoice = account_invoiceservice.get( account_invoice_id);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoices/{account_invoice_id}/createbatch")
    @PreAuthorize("@account_invoice_pms.check(#account_invoice_id,'CREATE')")
    public ResponseEntity<Account_invoice> createBatch(@PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoice account_invoice) {
        Account_invoice account_invoice2 = account_invoiceservice.createBatch(account_invoice_id, account_invoice);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoices/getdraft")
    @PreAuthorize("@account_invoice_pms.check('CREATE')")
    public ResponseEntity<Account_invoice> getDraft() {
        //Account_invoice account_invoice = account_invoiceservice.getDraft( account_invoice_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Account_invoice());
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoices/{account_invoice_id}/save")
    @PreAuthorize("@account_invoice_pms.check(#account_invoice_id,'')")
    public ResponseEntity<Account_invoice> save(@PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoice account_invoice) {
        Account_invoice account_invoice2 = account_invoiceservice.save(account_invoice_id, account_invoice);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice2);
    }

    //@GlobalTransactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoices/{account_invoice_id}/removebatch")
    @PreAuthorize("@account_invoice_pms.check(#account_invoice_id,'DELETE')")
    public ResponseEntity<Account_invoice> removeBatch(@PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoice account_invoice) {
        Account_invoice account_invoice2 = account_invoiceservice.removeBatch(account_invoice_id, account_invoice);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice2);
    }

    //@GlobalTransactional
    @RequestMapping(method= RequestMethod.GET , value="/account_invoices/fetchdefault")
    @PreAuthorize("@account_invoice_pms.check('READ')")
	public ResponseEntity<List<Account_invoice>> fetchDefault(Account_invoiceSearchContext searchContext,Pageable pageable) {
        
        Page<Account_invoice> page = account_invoiceservice.fetchDefault(searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partners/{res_partner_id}/account_invoices/{account_invoice_id}")
    @PreAuthorize("@account_invoice_pms.checkByRes_partner(#res_partner_id,#account_invoice_id,'DELETE')")
    public ResponseEntity<Boolean> removeByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_invoice_id") Integer account_invoice_id) {
        boolean b = account_invoiceservice.removeByRes_partner(res_partner_id,  account_invoice_id);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/account_invoices/checkkey")
    @PreAuthorize("@account_invoice_pms.checkByRes_partner(#res_partner_id,'')")
    public ResponseEntity<Boolean> checkKeyByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Account_invoice account_invoice) {
        boolean b = account_invoiceservice.checkKeyByRes_partner(res_partner_id, account_invoice);
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/account_invoices")
    @PreAuthorize("@account_invoice_pms.checkByRes_partner(#res_partner_id,'CREATE')")
    public ResponseEntity<Account_invoice> createByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Account_invoice account_invoice) {
        Account_invoice account_invoice2 = account_invoiceservice.createByRes_partner(res_partner_id, account_invoice);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/res_partners/{res_partner_id}/account_invoices/{account_invoice_id}")
    @PreAuthorize("@account_invoice_pms.checkByRes_partner(#res_partner_id,#account_invoice_id,'UPDATE')")
    public ResponseEntity<Account_invoice> updateByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoice account_invoice) {
        Account_invoice account_invoice2 = account_invoiceservice.updateByRes_partner(res_partner_id, account_invoice_id, account_invoice);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice2);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/api/res_partners/{res_partner_id}/account_invoices/{account_invoice_id}")
    @PreAuthorize("@account_invoice_pms.checkByRes_partner(#res_partner_id,#account_invoice_id,'UPDATE')")
    public ResponseEntity<Account_invoice> api_updateByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoice account_invoice) {
        Account_invoice account_invoice2 = account_invoiceservice.updateByRes_partner(res_partner_id, account_invoice_id, account_invoice);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice2);
    }

	@RequestMapping(method = RequestMethod.PUT, value = "/res_partners/{res_partner_id}/account_invoices/{account_invoice_id}/updatebatch")
    @PreAuthorize("@account_invoice_pms.checkByRes_partner(#res_partner_id,#account_invoice_id,'UPDATE')")
    public ResponseEntity<Account_invoice> updateBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoice account_invoice) {
        Account_invoice account_invoice2 = account_invoiceservice.updateBatchByRes_partner(res_partner_id, account_invoice_id, account_invoice);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice2);
    }

	@RequestMapping(method = RequestMethod.GET, value = "/res_partners/{res_partner_id}/account_invoices/{account_invoice_id}")
    @PreAuthorize("@account_invoice_pms.checkByRes_partner(#res_partner_id,#account_invoice_id,'READ')")
    public ResponseEntity<Account_invoice> getByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_invoice_id") Integer account_invoice_id) {
        Account_invoice account_invoice = account_invoiceservice.getByRes_partner(res_partner_id,  account_invoice_id);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice);
    }

	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/account_invoices/{account_invoice_id}/createbatch")
    @PreAuthorize("@account_invoice_pms.checkByRes_partner(#res_partner_id,#account_invoice_id,'CREATE')")
    public ResponseEntity<Account_invoice> createBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoice account_invoice) {
        Account_invoice account_invoice2 = account_invoiceservice.createBatchByRes_partner(res_partner_id, account_invoice_id, account_invoice);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice2);
    }

	@RequestMapping(method = RequestMethod.GET, value = "/res_partners/{res_partner_id}/account_invoices/getdraft")
    @PreAuthorize("@account_invoice_pms.checkByRes_partner(#res_partner_id,'CREATE')")
    public ResponseEntity<Account_invoice> getDraftByRes_partner() {
        //Account_invoice account_invoice = account_invoiceservice.getDraftByRes_partner(res_partner_id,  account_invoice_id);
        return ResponseEntity.status(HttpStatus.OK).body(new Account_invoice());
    }

	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/{res_partner_id}/account_invoices/{account_invoice_id}/save")
    @PreAuthorize("@account_invoice_pms.checkByRes_partner(#res_partner_id,#account_invoice_id,'')")
    public ResponseEntity<Account_invoice> saveByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoice account_invoice) {
        Account_invoice account_invoice2 = account_invoiceservice.saveByRes_partner(res_partner_id, account_invoice_id, account_invoice);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice2);
    }

	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partners/{res_partner_id}/account_invoices/{account_invoice_id}/removebatch")
    @PreAuthorize("@account_invoice_pms.checkByRes_partner(#res_partner_id,#account_invoice_id,'DELETE')")
    public ResponseEntity<Account_invoice> removeBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoice account_invoice) {
        Account_invoice account_invoice2 = account_invoiceservice.removeBatchByRes_partner(res_partner_id, account_invoice_id, account_invoice);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice2);
    }

    @RequestMapping(method= RequestMethod.GET , value="/res_partners/{res_partner_id}/account_invoices/fetchdefault")
    @PreAuthorize("@account_invoice_pms.checkByRes_partner(#res_partner_id,'READ')")
	public ResponseEntity<List<Account_invoice>> fetchDefaultByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id,Account_invoiceSearchContext searchContext,Pageable pageable) {
        searchContext.setN_partner_id_eq(res_partner_id);
        Page<Account_invoice> page = account_invoiceservice.fetchDefaultByRes_partner(res_partner_id , searchContext);
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(pageable.getPageNumber()))
        		.header("x-per-page", String.valueOf(pageable.getPageSize()))
        		.header("x-total", String.valueOf(page.getTotalElements()))
        		.body(page.getContent());
    }


}
