package cn.ibizlab.odoo.web.security.permission.odoo_sale;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;

import cn.ibizlab.odoo.web.odoo_sale.service.Sale_orderService;
import cn.ibizlab.odoo.web.odoo_sale.domain.Sale_order;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Component("sale_order_pms")
public class Sale_orderPermission {

    @Autowired
    Sale_orderService sale_orderservice;

    public boolean check(String action){
        return true ;
    }

    public boolean check(Integer sale_order_id, String action){
        return true ;
    }

    public boolean checkByRes_partner(Integer res_partner_id,  Integer sale_order_id, String action){
        return true ;
    }

    public boolean checkByRes_partner(Integer res_partner_id,  String action){
        return true ;
    }


}
