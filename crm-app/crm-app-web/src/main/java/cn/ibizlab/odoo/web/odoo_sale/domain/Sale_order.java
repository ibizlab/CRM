package cn.ibizlab.odoo.web.odoo_sale.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 应用实体[销售订单]
 */
public class Sale_order implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 单据日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_order;

    @JsonIgnore
    private boolean date_orderDirtyFlag;
    
    /**
     * 发票
     */
    private String invoice_ids;

    @JsonIgnore
    private boolean invoice_idsDirtyFlag;
    
    /**
     * 下一活动类型
     */
    private Integer activity_type_id;

    @JsonIgnore
    private boolean activity_type_idDirtyFlag;
    
    /**
     * 未税金额
     */
    private Double amount_untaxed;

    @JsonIgnore
    private boolean amount_untaxedDirtyFlag;
    
    /**
     * 验证
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp validity_date;

    @JsonIgnore
    private boolean validity_dateDirtyFlag;
    
    /**
     * 折扣前金额
     */
    private Double amount_undiscounted;

    @JsonIgnore
    private boolean amount_undiscountedDirtyFlag;
    
    /**
     * 发票数
     */
    private Integer invoice_count;

    @JsonIgnore
    private boolean invoice_countDirtyFlag;
    
    /**
     * 访问警告
     */
    private String access_warning;

    @JsonIgnore
    private boolean access_warningDirtyFlag;
    
    /**
     * 警告
     */
    private String warning_stock;

    @JsonIgnore
    private boolean warning_stockDirtyFlag;
    
    /**
     * 是关注者
     */
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;
    
    /**
     * 实际日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp effective_date;

    @JsonIgnore
    private boolean effective_dateDirtyFlag;
    
    /**
     * 网站
     */
    private Integer website_id;

    @JsonIgnore
    private boolean website_idDirtyFlag;
    
    /**
     * 在线签名
     */
    private String require_signature;

    @JsonIgnore
    private boolean require_signatureDirtyFlag;
    
    /**
     * 汇率
     */
    private Double currency_rate;

    @JsonIgnore
    private boolean currency_rateDirtyFlag;
    
    /**
     * 送货策略
     */
    private String picking_policy;

    @JsonIgnore
    private boolean picking_policyDirtyFlag;
    
    /**
     * 下一活动截止日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp activity_date_deadline;

    @JsonIgnore
    private boolean activity_date_deadlineDirtyFlag;
    
    /**
     * 签名
     */
    private byte[] signature;

    @JsonIgnore
    private boolean signatureDirtyFlag;
    
    /**
     * 补货组
     */
    private Integer procurement_group_id;

    @JsonIgnore
    private boolean procurement_group_idDirtyFlag;
    
    /**
     * 活动状态
     */
    private String activity_state;

    @JsonIgnore
    private boolean activity_stateDirtyFlag;
    
    /**
     * 购物车数量
     */
    private Integer cart_quantity;

    @JsonIgnore
    private boolean cart_quantityDirtyFlag;
    
    /**
     * 类型名称
     */
    private String type_name;

    @JsonIgnore
    private boolean type_nameDirtyFlag;
    
    /**
     * 附件
     */
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 下一活动摘要
     */
    private String activity_summary;

    @JsonIgnore
    private boolean activity_summaryDirtyFlag;
    
    /**
     * 出库单
     */
    private Integer delivery_count;

    @JsonIgnore
    private boolean delivery_countDirtyFlag;
    
    /**
     * 已签核
     */
    private String signed_by;

    @JsonIgnore
    private boolean signed_byDirtyFlag;
    
    /**
     * 订单行
     */
    private String order_line;

    @JsonIgnore
    private boolean order_lineDirtyFlag;
    
    /**
     * 创建日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;
    
    /**
     * 在线支付
     */
    private String require_payment;

    @JsonIgnore
    private boolean require_paymentDirtyFlag;
    
    /**
     * 只是服务
     */
    private String only_services;

    @JsonIgnore
    private boolean only_servicesDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;
    
    /**
     * 网页显示订单明细
     */
    private String website_order_line;

    @JsonIgnore
    private boolean website_order_lineDirtyFlag;
    
    /**
     * 付款参考:
     */
    private String reference;

    @JsonIgnore
    private boolean referenceDirtyFlag;
    
    /**
     * 附件数量
     */
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;
    
    /**
     * 发票状态
     */
    private String invoice_status;

    @JsonIgnore
    private boolean invoice_statusDirtyFlag;
    
    /**
     * 标签
     */
    private String tag_ids;

    @JsonIgnore
    private boolean tag_idsDirtyFlag;
    
    /**
     * ID
     */
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;
    
    /**
     * 交易
     */
    private String transaction_ids;

    @JsonIgnore
    private boolean transaction_idsDirtyFlag;
    
    /**
     * 行动数量
     */
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;
    
    /**
     * 遗弃的购物车
     */
    private String is_abandoned_cart;

    @JsonIgnore
    private boolean is_abandoned_cartDirtyFlag;
    
    /**
     * 状态
     */
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;
    
    /**
     * 消息递送错误
     */
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;
    
    /**
     * # 费用
     */
    private Integer expense_count;

    @JsonIgnore
    private boolean expense_countDirtyFlag;
    
    /**
     * 已授权的交易
     */
    private String authorized_transaction_ids;

    @JsonIgnore
    private boolean authorized_transaction_idsDirtyFlag;
    
    /**
     * 承诺日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp commitment_date;

    @JsonIgnore
    private boolean commitment_dateDirtyFlag;
    
    /**
     * 过期了
     */
    private String is_expired;

    @JsonIgnore
    private boolean is_expiredDirtyFlag;
    
    /**
     * 网站信息
     */
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;
    
    /**
     * 剩余确认天数
     */
    private Integer remaining_validity_days;

    @JsonIgnore
    private boolean remaining_validity_daysDirtyFlag;
    
    /**
     * 预计日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp expected_date;

    @JsonIgnore
    private boolean expected_dateDirtyFlag;
    
    /**
     * 采购订单号
     */
    private Integer purchase_order_count;

    @JsonIgnore
    private boolean purchase_order_countDirtyFlag;
    
    /**
     * 关注者
     */
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;
    
    /**
     * 门户访问网址
     */
    private String access_url;

    @JsonIgnore
    private boolean access_urlDirtyFlag;
    
    /**
     * 订单关联
     */
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;
    
    /**
     * 可选产品行
     */
    private String sale_order_option_ids;

    @JsonIgnore
    private boolean sale_order_option_idsDirtyFlag;
    
    /**
     * 消息
     */
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;
    
    /**
     * 源文档
     */
    private String origin;

    @JsonIgnore
    private boolean originDirtyFlag;
    
    /**
     * 确认日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp confirmation_date;

    @JsonIgnore
    private boolean confirmation_dateDirtyFlag;
    
    /**
     * 条款和条件
     */
    private String note;

    @JsonIgnore
    private boolean noteDirtyFlag;
    
    /**
     * 按组分配税额
     */
    private byte[] amount_by_group;

    @JsonIgnore
    private boolean amount_by_groupDirtyFlag;
    
    /**
     * 拣货
     */
    private String picking_ids;

    @JsonIgnore
    private boolean picking_idsDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;
    
    /**
     * 活动
     */
    private String activity_ids;

    @JsonIgnore
    private boolean activity_idsDirtyFlag;
    
    /**
     * 错误数
     */
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;
    
    /**
     * 安全令牌
     */
    private String access_token;

    @JsonIgnore
    private boolean access_tokenDirtyFlag;
    
    /**
     * 总计
     */
    private Double amount_total;

    @JsonIgnore
    private boolean amount_totalDirtyFlag;
    
    /**
     * 客户订单号
     */
    private String client_order_ref;

    @JsonIgnore
    private boolean client_order_refDirtyFlag;
    
    /**
     * 未读消息
     */
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;
    
    /**
     * 责任用户
     */
    private Integer activity_user_id;

    @JsonIgnore
    private boolean activity_user_idDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;
    
    /**
     * 购物车恢复EMail已发送
     */
    private String cart_recovery_email_sent;

    @JsonIgnore
    private boolean cart_recovery_email_sentDirtyFlag;
    
    /**
     * 显示名称
     */
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;
    
    /**
     * 需要采取行动
     */
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;
    
    /**
     * 税率
     */
    private Double amount_tax;

    @JsonIgnore
    private boolean amount_taxDirtyFlag;
    
    /**
     * 费用
     */
    private String expense_ids;

    @JsonIgnore
    private boolean expense_idsDirtyFlag;
    
    /**
     * 发票地址
     */
    private String partner_invoice_id_text;

    @JsonIgnore
    private boolean partner_invoice_id_textDirtyFlag;
    
    /**
     * 贸易条款
     */
    private String incoterm_text;

    @JsonIgnore
    private boolean incoterm_textDirtyFlag;
    
    /**
     * 销售团队
     */
    private String team_id_text;

    @JsonIgnore
    private boolean team_id_textDirtyFlag;
    
    /**
     * 分析账户
     */
    private String analytic_account_id_text;

    @JsonIgnore
    private boolean analytic_account_id_textDirtyFlag;
    
    /**
     * 客户
     */
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;
    
    /**
     * 营销
     */
    private String campaign_id_text;

    @JsonIgnore
    private boolean campaign_id_textDirtyFlag;
    
    /**
     * 创建人
     */
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;
    
    /**
     * 公司
     */
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;
    
    /**
     * 送货地址
     */
    private String partner_shipping_id_text;

    @JsonIgnore
    private boolean partner_shipping_id_textDirtyFlag;
    
    /**
     * 仓库
     */
    private String warehouse_id_text;

    @JsonIgnore
    private boolean warehouse_id_textDirtyFlag;
    
    /**
     * 销售员
     */
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;
    
    /**
     * 商机
     */
    private String opportunity_id_text;

    @JsonIgnore
    private boolean opportunity_id_textDirtyFlag;
    
    /**
     * 来源
     */
    private String source_id_text;

    @JsonIgnore
    private boolean source_id_textDirtyFlag;
    
    /**
     * 媒体
     */
    private String medium_id_text;

    @JsonIgnore
    private boolean medium_id_textDirtyFlag;
    
    /**
     * 税科目调整
     */
    private String fiscal_position_id_text;

    @JsonIgnore
    private boolean fiscal_position_id_textDirtyFlag;
    
    /**
     * 最后更新人
     */
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;
    
    /**
     * 价格表
     */
    private String pricelist_id_text;

    @JsonIgnore
    private boolean pricelist_id_textDirtyFlag;
    
    /**
     * 付款条款
     */
    private String payment_term_id_text;

    @JsonIgnore
    private boolean payment_term_id_textDirtyFlag;
    
    /**
     * 报价单模板
     */
    private String sale_order_template_id_text;

    @JsonIgnore
    private boolean sale_order_template_id_textDirtyFlag;
    
    /**
     * 币种
     */
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;
    
    /**
     * 最后更新人
     */
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;
    
    /**
     * 销售团队
     */
    private Integer team_id;

    @JsonIgnore
    private boolean team_idDirtyFlag;
    
    /**
     * 发票地址
     */
    private Integer partner_invoice_id;

    @JsonIgnore
    private boolean partner_invoice_idDirtyFlag;
    
    /**
     * 税科目调整
     */
    private Integer fiscal_position_id;

    @JsonIgnore
    private boolean fiscal_position_idDirtyFlag;
    
    /**
     * 客户
     */
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;
    
    /**
     * 营销
     */
    private Integer campaign_id;

    @JsonIgnore
    private boolean campaign_idDirtyFlag;
    
    /**
     * 来源
     */
    private Integer source_id;

    @JsonIgnore
    private boolean source_idDirtyFlag;
    
    /**
     * 报价单模板
     */
    private Integer sale_order_template_id;

    @JsonIgnore
    private boolean sale_order_template_idDirtyFlag;
    
    /**
     * 销售员
     */
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;
    
    /**
     * 公司
     */
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;
    
    /**
     * 媒体
     */
    private Integer medium_id;

    @JsonIgnore
    private boolean medium_idDirtyFlag;
    
    /**
     * 分析账户
     */
    private Integer analytic_account_id;

    @JsonIgnore
    private boolean analytic_account_idDirtyFlag;
    
    /**
     * 付款条款
     */
    private Integer payment_term_id;

    @JsonIgnore
    private boolean payment_term_idDirtyFlag;
    
    /**
     * 送货地址
     */
    private Integer partner_shipping_id;

    @JsonIgnore
    private boolean partner_shipping_idDirtyFlag;
    
    /**
     * 商机
     */
    private Integer opportunity_id;

    @JsonIgnore
    private boolean opportunity_idDirtyFlag;
    
    /**
     * 贸易条款
     */
    private Integer incoterm;

    @JsonIgnore
    private boolean incotermDirtyFlag;
    
    /**
     * 价格表
     */
    private Integer pricelist_id;

    @JsonIgnore
    private boolean pricelist_idDirtyFlag;
    
    /**
     * 创建人
     */
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;
    
    /**
     * 仓库
     */
    private Integer warehouse_id;

    @JsonIgnore
    private boolean warehouse_idDirtyFlag;
    

    /**
     * 获取 [单据日期]
     */
    @JsonProperty("date_order")
    public Timestamp getDate_order(){
        return this.date_order ;
    }

    /**
     * 设置 [单据日期]
     */
    @JsonProperty("date_order")
    public void setDate_order(Timestamp  date_order){
        this.date_order = date_order ;
        this.date_orderDirtyFlag = true ;
    }

    /**
     * 获取 [单据日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_orderDirtyFlag(){
        return this.date_orderDirtyFlag ;
    }

    /**
     * 获取 [发票]
     */
    @JsonProperty("invoice_ids")
    public String getInvoice_ids(){
        return this.invoice_ids ;
    }

    /**
     * 设置 [发票]
     */
    @JsonProperty("invoice_ids")
    public void setInvoice_ids(String  invoice_ids){
        this.invoice_ids = invoice_ids ;
        this.invoice_idsDirtyFlag = true ;
    }

    /**
     * 获取 [发票]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_idsDirtyFlag(){
        return this.invoice_idsDirtyFlag ;
    }

    /**
     * 获取 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return this.activity_type_id ;
    }

    /**
     * 设置 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [下一活动类型]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return this.activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [未税金额]
     */
    @JsonProperty("amount_untaxed")
    public Double getAmount_untaxed(){
        return this.amount_untaxed ;
    }

    /**
     * 设置 [未税金额]
     */
    @JsonProperty("amount_untaxed")
    public void setAmount_untaxed(Double  amount_untaxed){
        this.amount_untaxed = amount_untaxed ;
        this.amount_untaxedDirtyFlag = true ;
    }

    /**
     * 获取 [未税金额]脏标记
     */
    @JsonIgnore
    public boolean getAmount_untaxedDirtyFlag(){
        return this.amount_untaxedDirtyFlag ;
    }

    /**
     * 获取 [验证]
     */
    @JsonProperty("validity_date")
    public Timestamp getValidity_date(){
        return this.validity_date ;
    }

    /**
     * 设置 [验证]
     */
    @JsonProperty("validity_date")
    public void setValidity_date(Timestamp  validity_date){
        this.validity_date = validity_date ;
        this.validity_dateDirtyFlag = true ;
    }

    /**
     * 获取 [验证]脏标记
     */
    @JsonIgnore
    public boolean getValidity_dateDirtyFlag(){
        return this.validity_dateDirtyFlag ;
    }

    /**
     * 获取 [折扣前金额]
     */
    @JsonProperty("amount_undiscounted")
    public Double getAmount_undiscounted(){
        return this.amount_undiscounted ;
    }

    /**
     * 设置 [折扣前金额]
     */
    @JsonProperty("amount_undiscounted")
    public void setAmount_undiscounted(Double  amount_undiscounted){
        this.amount_undiscounted = amount_undiscounted ;
        this.amount_undiscountedDirtyFlag = true ;
    }

    /**
     * 获取 [折扣前金额]脏标记
     */
    @JsonIgnore
    public boolean getAmount_undiscountedDirtyFlag(){
        return this.amount_undiscountedDirtyFlag ;
    }

    /**
     * 获取 [发票数]
     */
    @JsonProperty("invoice_count")
    public Integer getInvoice_count(){
        return this.invoice_count ;
    }

    /**
     * 设置 [发票数]
     */
    @JsonProperty("invoice_count")
    public void setInvoice_count(Integer  invoice_count){
        this.invoice_count = invoice_count ;
        this.invoice_countDirtyFlag = true ;
    }

    /**
     * 获取 [发票数]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_countDirtyFlag(){
        return this.invoice_countDirtyFlag ;
    }

    /**
     * 获取 [访问警告]
     */
    @JsonProperty("access_warning")
    public String getAccess_warning(){
        return this.access_warning ;
    }

    /**
     * 设置 [访问警告]
     */
    @JsonProperty("access_warning")
    public void setAccess_warning(String  access_warning){
        this.access_warning = access_warning ;
        this.access_warningDirtyFlag = true ;
    }

    /**
     * 获取 [访问警告]脏标记
     */
    @JsonIgnore
    public boolean getAccess_warningDirtyFlag(){
        return this.access_warningDirtyFlag ;
    }

    /**
     * 获取 [警告]
     */
    @JsonProperty("warning_stock")
    public String getWarning_stock(){
        return this.warning_stock ;
    }

    /**
     * 设置 [警告]
     */
    @JsonProperty("warning_stock")
    public void setWarning_stock(String  warning_stock){
        this.warning_stock = warning_stock ;
        this.warning_stockDirtyFlag = true ;
    }

    /**
     * 获取 [警告]脏标记
     */
    @JsonIgnore
    public boolean getWarning_stockDirtyFlag(){
        return this.warning_stockDirtyFlag ;
    }

    /**
     * 获取 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [是关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [实际日期]
     */
    @JsonProperty("effective_date")
    public Timestamp getEffective_date(){
        return this.effective_date ;
    }

    /**
     * 设置 [实际日期]
     */
    @JsonProperty("effective_date")
    public void setEffective_date(Timestamp  effective_date){
        this.effective_date = effective_date ;
        this.effective_dateDirtyFlag = true ;
    }

    /**
     * 获取 [实际日期]脏标记
     */
    @JsonIgnore
    public boolean getEffective_dateDirtyFlag(){
        return this.effective_dateDirtyFlag ;
    }

    /**
     * 获取 [网站]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return this.website_id ;
    }

    /**
     * 设置 [网站]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

    /**
     * 获取 [网站]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return this.website_idDirtyFlag ;
    }

    /**
     * 获取 [在线签名]
     */
    @JsonProperty("require_signature")
    public String getRequire_signature(){
        return this.require_signature ;
    }

    /**
     * 设置 [在线签名]
     */
    @JsonProperty("require_signature")
    public void setRequire_signature(String  require_signature){
        this.require_signature = require_signature ;
        this.require_signatureDirtyFlag = true ;
    }

    /**
     * 获取 [在线签名]脏标记
     */
    @JsonIgnore
    public boolean getRequire_signatureDirtyFlag(){
        return this.require_signatureDirtyFlag ;
    }

    /**
     * 获取 [汇率]
     */
    @JsonProperty("currency_rate")
    public Double getCurrency_rate(){
        return this.currency_rate ;
    }

    /**
     * 设置 [汇率]
     */
    @JsonProperty("currency_rate")
    public void setCurrency_rate(Double  currency_rate){
        this.currency_rate = currency_rate ;
        this.currency_rateDirtyFlag = true ;
    }

    /**
     * 获取 [汇率]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_rateDirtyFlag(){
        return this.currency_rateDirtyFlag ;
    }

    /**
     * 获取 [送货策略]
     */
    @JsonProperty("picking_policy")
    public String getPicking_policy(){
        return this.picking_policy ;
    }

    /**
     * 设置 [送货策略]
     */
    @JsonProperty("picking_policy")
    public void setPicking_policy(String  picking_policy){
        this.picking_policy = picking_policy ;
        this.picking_policyDirtyFlag = true ;
    }

    /**
     * 获取 [送货策略]脏标记
     */
    @JsonIgnore
    public boolean getPicking_policyDirtyFlag(){
        return this.picking_policyDirtyFlag ;
    }

    /**
     * 获取 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return this.activity_date_deadline ;
    }

    /**
     * 设置 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return this.activity_date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [签名]
     */
    @JsonProperty("signature")
    public byte[] getSignature(){
        return this.signature ;
    }

    /**
     * 设置 [签名]
     */
    @JsonProperty("signature")
    public void setSignature(byte[]  signature){
        this.signature = signature ;
        this.signatureDirtyFlag = true ;
    }

    /**
     * 获取 [签名]脏标记
     */
    @JsonIgnore
    public boolean getSignatureDirtyFlag(){
        return this.signatureDirtyFlag ;
    }

    /**
     * 获取 [补货组]
     */
    @JsonProperty("procurement_group_id")
    public Integer getProcurement_group_id(){
        return this.procurement_group_id ;
    }

    /**
     * 设置 [补货组]
     */
    @JsonProperty("procurement_group_id")
    public void setProcurement_group_id(Integer  procurement_group_id){
        this.procurement_group_id = procurement_group_id ;
        this.procurement_group_idDirtyFlag = true ;
    }

    /**
     * 获取 [补货组]脏标记
     */
    @JsonIgnore
    public boolean getProcurement_group_idDirtyFlag(){
        return this.procurement_group_idDirtyFlag ;
    }

    /**
     * 获取 [活动状态]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return this.activity_state ;
    }

    /**
     * 设置 [活动状态]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

    /**
     * 获取 [活动状态]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return this.activity_stateDirtyFlag ;
    }

    /**
     * 获取 [购物车数量]
     */
    @JsonProperty("cart_quantity")
    public Integer getCart_quantity(){
        return this.cart_quantity ;
    }

    /**
     * 设置 [购物车数量]
     */
    @JsonProperty("cart_quantity")
    public void setCart_quantity(Integer  cart_quantity){
        this.cart_quantity = cart_quantity ;
        this.cart_quantityDirtyFlag = true ;
    }

    /**
     * 获取 [购物车数量]脏标记
     */
    @JsonIgnore
    public boolean getCart_quantityDirtyFlag(){
        return this.cart_quantityDirtyFlag ;
    }

    /**
     * 获取 [类型名称]
     */
    @JsonProperty("type_name")
    public String getType_name(){
        return this.type_name ;
    }

    /**
     * 设置 [类型名称]
     */
    @JsonProperty("type_name")
    public void setType_name(String  type_name){
        this.type_name = type_name ;
        this.type_nameDirtyFlag = true ;
    }

    /**
     * 获取 [类型名称]脏标记
     */
    @JsonIgnore
    public boolean getType_nameDirtyFlag(){
        return this.type_nameDirtyFlag ;
    }

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return this.activity_summary ;
    }

    /**
     * 设置 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

    /**
     * 获取 [下一活动摘要]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return this.activity_summaryDirtyFlag ;
    }

    /**
     * 获取 [出库单]
     */
    @JsonProperty("delivery_count")
    public Integer getDelivery_count(){
        return this.delivery_count ;
    }

    /**
     * 设置 [出库单]
     */
    @JsonProperty("delivery_count")
    public void setDelivery_count(Integer  delivery_count){
        this.delivery_count = delivery_count ;
        this.delivery_countDirtyFlag = true ;
    }

    /**
     * 获取 [出库单]脏标记
     */
    @JsonIgnore
    public boolean getDelivery_countDirtyFlag(){
        return this.delivery_countDirtyFlag ;
    }

    /**
     * 获取 [已签核]
     */
    @JsonProperty("signed_by")
    public String getSigned_by(){
        return this.signed_by ;
    }

    /**
     * 设置 [已签核]
     */
    @JsonProperty("signed_by")
    public void setSigned_by(String  signed_by){
        this.signed_by = signed_by ;
        this.signed_byDirtyFlag = true ;
    }

    /**
     * 获取 [已签核]脏标记
     */
    @JsonIgnore
    public boolean getSigned_byDirtyFlag(){
        return this.signed_byDirtyFlag ;
    }

    /**
     * 获取 [订单行]
     */
    @JsonProperty("order_line")
    public String getOrder_line(){
        return this.order_line ;
    }

    /**
     * 设置 [订单行]
     */
    @JsonProperty("order_line")
    public void setOrder_line(String  order_line){
        this.order_line = order_line ;
        this.order_lineDirtyFlag = true ;
    }

    /**
     * 获取 [订单行]脏标记
     */
    @JsonIgnore
    public boolean getOrder_lineDirtyFlag(){
        return this.order_lineDirtyFlag ;
    }

    /**
     * 获取 [创建日期]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建日期]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [创建日期]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }

    /**
     * 获取 [在线支付]
     */
    @JsonProperty("require_payment")
    public String getRequire_payment(){
        return this.require_payment ;
    }

    /**
     * 设置 [在线支付]
     */
    @JsonProperty("require_payment")
    public void setRequire_payment(String  require_payment){
        this.require_payment = require_payment ;
        this.require_paymentDirtyFlag = true ;
    }

    /**
     * 获取 [在线支付]脏标记
     */
    @JsonIgnore
    public boolean getRequire_paymentDirtyFlag(){
        return this.require_paymentDirtyFlag ;
    }

    /**
     * 获取 [只是服务]
     */
    @JsonProperty("only_services")
    public String getOnly_services(){
        return this.only_services ;
    }

    /**
     * 设置 [只是服务]
     */
    @JsonProperty("only_services")
    public void setOnly_services(String  only_services){
        this.only_services = only_services ;
        this.only_servicesDirtyFlag = true ;
    }

    /**
     * 获取 [只是服务]脏标记
     */
    @JsonIgnore
    public boolean getOnly_servicesDirtyFlag(){
        return this.only_servicesDirtyFlag ;
    }

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [网页显示订单明细]
     */
    @JsonProperty("website_order_line")
    public String getWebsite_order_line(){
        return this.website_order_line ;
    }

    /**
     * 设置 [网页显示订单明细]
     */
    @JsonProperty("website_order_line")
    public void setWebsite_order_line(String  website_order_line){
        this.website_order_line = website_order_line ;
        this.website_order_lineDirtyFlag = true ;
    }

    /**
     * 获取 [网页显示订单明细]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_order_lineDirtyFlag(){
        return this.website_order_lineDirtyFlag ;
    }

    /**
     * 获取 [付款参考:]
     */
    @JsonProperty("reference")
    public String getReference(){
        return this.reference ;
    }

    /**
     * 设置 [付款参考:]
     */
    @JsonProperty("reference")
    public void setReference(String  reference){
        this.reference = reference ;
        this.referenceDirtyFlag = true ;
    }

    /**
     * 获取 [付款参考:]脏标记
     */
    @JsonIgnore
    public boolean getReferenceDirtyFlag(){
        return this.referenceDirtyFlag ;
    }

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }

    /**
     * 获取 [发票状态]
     */
    @JsonProperty("invoice_status")
    public String getInvoice_status(){
        return this.invoice_status ;
    }

    /**
     * 设置 [发票状态]
     */
    @JsonProperty("invoice_status")
    public void setInvoice_status(String  invoice_status){
        this.invoice_status = invoice_status ;
        this.invoice_statusDirtyFlag = true ;
    }

    /**
     * 获取 [发票状态]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_statusDirtyFlag(){
        return this.invoice_statusDirtyFlag ;
    }

    /**
     * 获取 [标签]
     */
    @JsonProperty("tag_ids")
    public String getTag_ids(){
        return this.tag_ids ;
    }

    /**
     * 设置 [标签]
     */
    @JsonProperty("tag_ids")
    public void setTag_ids(String  tag_ids){
        this.tag_ids = tag_ids ;
        this.tag_idsDirtyFlag = true ;
    }

    /**
     * 获取 [标签]脏标记
     */
    @JsonIgnore
    public boolean getTag_idsDirtyFlag(){
        return this.tag_idsDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }

    /**
     * 获取 [交易]
     */
    @JsonProperty("transaction_ids")
    public String getTransaction_ids(){
        return this.transaction_ids ;
    }

    /**
     * 设置 [交易]
     */
    @JsonProperty("transaction_ids")
    public void setTransaction_ids(String  transaction_ids){
        this.transaction_ids = transaction_ids ;
        this.transaction_idsDirtyFlag = true ;
    }

    /**
     * 获取 [交易]脏标记
     */
    @JsonIgnore
    public boolean getTransaction_idsDirtyFlag(){
        return this.transaction_idsDirtyFlag ;
    }

    /**
     * 获取 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [行动数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [遗弃的购物车]
     */
    @JsonProperty("is_abandoned_cart")
    public String getIs_abandoned_cart(){
        return this.is_abandoned_cart ;
    }

    /**
     * 设置 [遗弃的购物车]
     */
    @JsonProperty("is_abandoned_cart")
    public void setIs_abandoned_cart(String  is_abandoned_cart){
        this.is_abandoned_cart = is_abandoned_cart ;
        this.is_abandoned_cartDirtyFlag = true ;
    }

    /**
     * 获取 [遗弃的购物车]脏标记
     */
    @JsonIgnore
    public boolean getIs_abandoned_cartDirtyFlag(){
        return this.is_abandoned_cartDirtyFlag ;
    }

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [# 费用]
     */
    @JsonProperty("expense_count")
    public Integer getExpense_count(){
        return this.expense_count ;
    }

    /**
     * 设置 [# 费用]
     */
    @JsonProperty("expense_count")
    public void setExpense_count(Integer  expense_count){
        this.expense_count = expense_count ;
        this.expense_countDirtyFlag = true ;
    }

    /**
     * 获取 [# 费用]脏标记
     */
    @JsonIgnore
    public boolean getExpense_countDirtyFlag(){
        return this.expense_countDirtyFlag ;
    }

    /**
     * 获取 [已授权的交易]
     */
    @JsonProperty("authorized_transaction_ids")
    public String getAuthorized_transaction_ids(){
        return this.authorized_transaction_ids ;
    }

    /**
     * 设置 [已授权的交易]
     */
    @JsonProperty("authorized_transaction_ids")
    public void setAuthorized_transaction_ids(String  authorized_transaction_ids){
        this.authorized_transaction_ids = authorized_transaction_ids ;
        this.authorized_transaction_idsDirtyFlag = true ;
    }

    /**
     * 获取 [已授权的交易]脏标记
     */
    @JsonIgnore
    public boolean getAuthorized_transaction_idsDirtyFlag(){
        return this.authorized_transaction_idsDirtyFlag ;
    }

    /**
     * 获取 [承诺日期]
     */
    @JsonProperty("commitment_date")
    public Timestamp getCommitment_date(){
        return this.commitment_date ;
    }

    /**
     * 设置 [承诺日期]
     */
    @JsonProperty("commitment_date")
    public void setCommitment_date(Timestamp  commitment_date){
        this.commitment_date = commitment_date ;
        this.commitment_dateDirtyFlag = true ;
    }

    /**
     * 获取 [承诺日期]脏标记
     */
    @JsonIgnore
    public boolean getCommitment_dateDirtyFlag(){
        return this.commitment_dateDirtyFlag ;
    }

    /**
     * 获取 [过期了]
     */
    @JsonProperty("is_expired")
    public String getIs_expired(){
        return this.is_expired ;
    }

    /**
     * 设置 [过期了]
     */
    @JsonProperty("is_expired")
    public void setIs_expired(String  is_expired){
        this.is_expired = is_expired ;
        this.is_expiredDirtyFlag = true ;
    }

    /**
     * 获取 [过期了]脏标记
     */
    @JsonIgnore
    public boolean getIs_expiredDirtyFlag(){
        return this.is_expiredDirtyFlag ;
    }

    /**
     * 获取 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [网站信息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [剩余确认天数]
     */
    @JsonProperty("remaining_validity_days")
    public Integer getRemaining_validity_days(){
        return this.remaining_validity_days ;
    }

    /**
     * 设置 [剩余确认天数]
     */
    @JsonProperty("remaining_validity_days")
    public void setRemaining_validity_days(Integer  remaining_validity_days){
        this.remaining_validity_days = remaining_validity_days ;
        this.remaining_validity_daysDirtyFlag = true ;
    }

    /**
     * 获取 [剩余确认天数]脏标记
     */
    @JsonIgnore
    public boolean getRemaining_validity_daysDirtyFlag(){
        return this.remaining_validity_daysDirtyFlag ;
    }

    /**
     * 获取 [预计日期]
     */
    @JsonProperty("expected_date")
    public Timestamp getExpected_date(){
        return this.expected_date ;
    }

    /**
     * 设置 [预计日期]
     */
    @JsonProperty("expected_date")
    public void setExpected_date(Timestamp  expected_date){
        this.expected_date = expected_date ;
        this.expected_dateDirtyFlag = true ;
    }

    /**
     * 获取 [预计日期]脏标记
     */
    @JsonIgnore
    public boolean getExpected_dateDirtyFlag(){
        return this.expected_dateDirtyFlag ;
    }

    /**
     * 获取 [采购订单号]
     */
    @JsonProperty("purchase_order_count")
    public Integer getPurchase_order_count(){
        return this.purchase_order_count ;
    }

    /**
     * 设置 [采购订单号]
     */
    @JsonProperty("purchase_order_count")
    public void setPurchase_order_count(Integer  purchase_order_count){
        this.purchase_order_count = purchase_order_count ;
        this.purchase_order_countDirtyFlag = true ;
    }

    /**
     * 获取 [采购订单号]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_order_countDirtyFlag(){
        return this.purchase_order_countDirtyFlag ;
    }

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [门户访问网址]
     */
    @JsonProperty("access_url")
    public String getAccess_url(){
        return this.access_url ;
    }

    /**
     * 设置 [门户访问网址]
     */
    @JsonProperty("access_url")
    public void setAccess_url(String  access_url){
        this.access_url = access_url ;
        this.access_urlDirtyFlag = true ;
    }

    /**
     * 获取 [门户访问网址]脏标记
     */
    @JsonIgnore
    public boolean getAccess_urlDirtyFlag(){
        return this.access_urlDirtyFlag ;
    }

    /**
     * 获取 [订单关联]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [订单关联]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [订单关联]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [可选产品行]
     */
    @JsonProperty("sale_order_option_ids")
    public String getSale_order_option_ids(){
        return this.sale_order_option_ids ;
    }

    /**
     * 设置 [可选产品行]
     */
    @JsonProperty("sale_order_option_ids")
    public void setSale_order_option_ids(String  sale_order_option_ids){
        this.sale_order_option_ids = sale_order_option_ids ;
        this.sale_order_option_idsDirtyFlag = true ;
    }

    /**
     * 获取 [可选产品行]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_option_idsDirtyFlag(){
        return this.sale_order_option_idsDirtyFlag ;
    }

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }

    /**
     * 获取 [源文档]
     */
    @JsonProperty("origin")
    public String getOrigin(){
        return this.origin ;
    }

    /**
     * 设置 [源文档]
     */
    @JsonProperty("origin")
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.originDirtyFlag = true ;
    }

    /**
     * 获取 [源文档]脏标记
     */
    @JsonIgnore
    public boolean getOriginDirtyFlag(){
        return this.originDirtyFlag ;
    }

    /**
     * 获取 [确认日期]
     */
    @JsonProperty("confirmation_date")
    public Timestamp getConfirmation_date(){
        return this.confirmation_date ;
    }

    /**
     * 设置 [确认日期]
     */
    @JsonProperty("confirmation_date")
    public void setConfirmation_date(Timestamp  confirmation_date){
        this.confirmation_date = confirmation_date ;
        this.confirmation_dateDirtyFlag = true ;
    }

    /**
     * 获取 [确认日期]脏标记
     */
    @JsonIgnore
    public boolean getConfirmation_dateDirtyFlag(){
        return this.confirmation_dateDirtyFlag ;
    }

    /**
     * 获取 [条款和条件]
     */
    @JsonProperty("note")
    public String getNote(){
        return this.note ;
    }

    /**
     * 设置 [条款和条件]
     */
    @JsonProperty("note")
    public void setNote(String  note){
        this.note = note ;
        this.noteDirtyFlag = true ;
    }

    /**
     * 获取 [条款和条件]脏标记
     */
    @JsonIgnore
    public boolean getNoteDirtyFlag(){
        return this.noteDirtyFlag ;
    }

    /**
     * 获取 [按组分配税额]
     */
    @JsonProperty("amount_by_group")
    public byte[] getAmount_by_group(){
        return this.amount_by_group ;
    }

    /**
     * 设置 [按组分配税额]
     */
    @JsonProperty("amount_by_group")
    public void setAmount_by_group(byte[]  amount_by_group){
        this.amount_by_group = amount_by_group ;
        this.amount_by_groupDirtyFlag = true ;
    }

    /**
     * 获取 [按组分配税额]脏标记
     */
    @JsonIgnore
    public boolean getAmount_by_groupDirtyFlag(){
        return this.amount_by_groupDirtyFlag ;
    }

    /**
     * 获取 [拣货]
     */
    @JsonProperty("picking_ids")
    public String getPicking_ids(){
        return this.picking_ids ;
    }

    /**
     * 设置 [拣货]
     */
    @JsonProperty("picking_ids")
    public void setPicking_ids(String  picking_ids){
        this.picking_ids = picking_ids ;
        this.picking_idsDirtyFlag = true ;
    }

    /**
     * 获取 [拣货]脏标记
     */
    @JsonIgnore
    public boolean getPicking_idsDirtyFlag(){
        return this.picking_idsDirtyFlag ;
    }

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return this.activity_ids ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return this.activity_idsDirtyFlag ;
    }

    /**
     * 获取 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [错误数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [安全令牌]
     */
    @JsonProperty("access_token")
    public String getAccess_token(){
        return this.access_token ;
    }

    /**
     * 设置 [安全令牌]
     */
    @JsonProperty("access_token")
    public void setAccess_token(String  access_token){
        this.access_token = access_token ;
        this.access_tokenDirtyFlag = true ;
    }

    /**
     * 获取 [安全令牌]脏标记
     */
    @JsonIgnore
    public boolean getAccess_tokenDirtyFlag(){
        return this.access_tokenDirtyFlag ;
    }

    /**
     * 获取 [总计]
     */
    @JsonProperty("amount_total")
    public Double getAmount_total(){
        return this.amount_total ;
    }

    /**
     * 设置 [总计]
     */
    @JsonProperty("amount_total")
    public void setAmount_total(Double  amount_total){
        this.amount_total = amount_total ;
        this.amount_totalDirtyFlag = true ;
    }

    /**
     * 获取 [总计]脏标记
     */
    @JsonIgnore
    public boolean getAmount_totalDirtyFlag(){
        return this.amount_totalDirtyFlag ;
    }

    /**
     * 获取 [客户订单号]
     */
    @JsonProperty("client_order_ref")
    public String getClient_order_ref(){
        return this.client_order_ref ;
    }

    /**
     * 设置 [客户订单号]
     */
    @JsonProperty("client_order_ref")
    public void setClient_order_ref(String  client_order_ref){
        this.client_order_ref = client_order_ref ;
        this.client_order_refDirtyFlag = true ;
    }

    /**
     * 获取 [客户订单号]脏标记
     */
    @JsonIgnore
    public boolean getClient_order_refDirtyFlag(){
        return this.client_order_refDirtyFlag ;
    }

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }

    /**
     * 获取 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return this.activity_user_id ;
    }

    /**
     * 设置 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [责任用户]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return this.activity_user_idDirtyFlag ;
    }

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [购物车恢复EMail已发送]
     */
    @JsonProperty("cart_recovery_email_sent")
    public String getCart_recovery_email_sent(){
        return this.cart_recovery_email_sent ;
    }

    /**
     * 设置 [购物车恢复EMail已发送]
     */
    @JsonProperty("cart_recovery_email_sent")
    public void setCart_recovery_email_sent(String  cart_recovery_email_sent){
        this.cart_recovery_email_sent = cart_recovery_email_sent ;
        this.cart_recovery_email_sentDirtyFlag = true ;
    }

    /**
     * 获取 [购物车恢复EMail已发送]脏标记
     */
    @JsonIgnore
    public boolean getCart_recovery_email_sentDirtyFlag(){
        return this.cart_recovery_email_sentDirtyFlag ;
    }

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }

    /**
     * 获取 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [需要采取行动]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }

    /**
     * 获取 [税率]
     */
    @JsonProperty("amount_tax")
    public Double getAmount_tax(){
        return this.amount_tax ;
    }

    /**
     * 设置 [税率]
     */
    @JsonProperty("amount_tax")
    public void setAmount_tax(Double  amount_tax){
        this.amount_tax = amount_tax ;
        this.amount_taxDirtyFlag = true ;
    }

    /**
     * 获取 [税率]脏标记
     */
    @JsonIgnore
    public boolean getAmount_taxDirtyFlag(){
        return this.amount_taxDirtyFlag ;
    }

    /**
     * 获取 [费用]
     */
    @JsonProperty("expense_ids")
    public String getExpense_ids(){
        return this.expense_ids ;
    }

    /**
     * 设置 [费用]
     */
    @JsonProperty("expense_ids")
    public void setExpense_ids(String  expense_ids){
        this.expense_ids = expense_ids ;
        this.expense_idsDirtyFlag = true ;
    }

    /**
     * 获取 [费用]脏标记
     */
    @JsonIgnore
    public boolean getExpense_idsDirtyFlag(){
        return this.expense_idsDirtyFlag ;
    }

    /**
     * 获取 [发票地址]
     */
    @JsonProperty("partner_invoice_id_text")
    public String getPartner_invoice_id_text(){
        return this.partner_invoice_id_text ;
    }

    /**
     * 设置 [发票地址]
     */
    @JsonProperty("partner_invoice_id_text")
    public void setPartner_invoice_id_text(String  partner_invoice_id_text){
        this.partner_invoice_id_text = partner_invoice_id_text ;
        this.partner_invoice_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [发票地址]脏标记
     */
    @JsonIgnore
    public boolean getPartner_invoice_id_textDirtyFlag(){
        return this.partner_invoice_id_textDirtyFlag ;
    }

    /**
     * 获取 [贸易条款]
     */
    @JsonProperty("incoterm_text")
    public String getIncoterm_text(){
        return this.incoterm_text ;
    }

    /**
     * 设置 [贸易条款]
     */
    @JsonProperty("incoterm_text")
    public void setIncoterm_text(String  incoterm_text){
        this.incoterm_text = incoterm_text ;
        this.incoterm_textDirtyFlag = true ;
    }

    /**
     * 获取 [贸易条款]脏标记
     */
    @JsonIgnore
    public boolean getIncoterm_textDirtyFlag(){
        return this.incoterm_textDirtyFlag ;
    }

    /**
     * 获取 [销售团队]
     */
    @JsonProperty("team_id_text")
    public String getTeam_id_text(){
        return this.team_id_text ;
    }

    /**
     * 设置 [销售团队]
     */
    @JsonProperty("team_id_text")
    public void setTeam_id_text(String  team_id_text){
        this.team_id_text = team_id_text ;
        this.team_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [销售团队]脏标记
     */
    @JsonIgnore
    public boolean getTeam_id_textDirtyFlag(){
        return this.team_id_textDirtyFlag ;
    }

    /**
     * 获取 [分析账户]
     */
    @JsonProperty("analytic_account_id_text")
    public String getAnalytic_account_id_text(){
        return this.analytic_account_id_text ;
    }

    /**
     * 设置 [分析账户]
     */
    @JsonProperty("analytic_account_id_text")
    public void setAnalytic_account_id_text(String  analytic_account_id_text){
        this.analytic_account_id_text = analytic_account_id_text ;
        this.analytic_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [分析账户]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_account_id_textDirtyFlag(){
        return this.analytic_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [客户]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [客户]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [客户]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [营销]
     */
    @JsonProperty("campaign_id_text")
    public String getCampaign_id_text(){
        return this.campaign_id_text ;
    }

    /**
     * 设置 [营销]
     */
    @JsonProperty("campaign_id_text")
    public void setCampaign_id_text(String  campaign_id_text){
        this.campaign_id_text = campaign_id_text ;
        this.campaign_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [营销]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_id_textDirtyFlag(){
        return this.campaign_id_textDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }

    /**
     * 获取 [送货地址]
     */
    @JsonProperty("partner_shipping_id_text")
    public String getPartner_shipping_id_text(){
        return this.partner_shipping_id_text ;
    }

    /**
     * 设置 [送货地址]
     */
    @JsonProperty("partner_shipping_id_text")
    public void setPartner_shipping_id_text(String  partner_shipping_id_text){
        this.partner_shipping_id_text = partner_shipping_id_text ;
        this.partner_shipping_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [送货地址]脏标记
     */
    @JsonIgnore
    public boolean getPartner_shipping_id_textDirtyFlag(){
        return this.partner_shipping_id_textDirtyFlag ;
    }

    /**
     * 获取 [仓库]
     */
    @JsonProperty("warehouse_id_text")
    public String getWarehouse_id_text(){
        return this.warehouse_id_text ;
    }

    /**
     * 设置 [仓库]
     */
    @JsonProperty("warehouse_id_text")
    public void setWarehouse_id_text(String  warehouse_id_text){
        this.warehouse_id_text = warehouse_id_text ;
        this.warehouse_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [仓库]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_id_textDirtyFlag(){
        return this.warehouse_id_textDirtyFlag ;
    }

    /**
     * 获取 [销售员]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [销售员]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [销售员]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }

    /**
     * 获取 [商机]
     */
    @JsonProperty("opportunity_id_text")
    public String getOpportunity_id_text(){
        return this.opportunity_id_text ;
    }

    /**
     * 设置 [商机]
     */
    @JsonProperty("opportunity_id_text")
    public void setOpportunity_id_text(String  opportunity_id_text){
        this.opportunity_id_text = opportunity_id_text ;
        this.opportunity_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [商机]脏标记
     */
    @JsonIgnore
    public boolean getOpportunity_id_textDirtyFlag(){
        return this.opportunity_id_textDirtyFlag ;
    }

    /**
     * 获取 [来源]
     */
    @JsonProperty("source_id_text")
    public String getSource_id_text(){
        return this.source_id_text ;
    }

    /**
     * 设置 [来源]
     */
    @JsonProperty("source_id_text")
    public void setSource_id_text(String  source_id_text){
        this.source_id_text = source_id_text ;
        this.source_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [来源]脏标记
     */
    @JsonIgnore
    public boolean getSource_id_textDirtyFlag(){
        return this.source_id_textDirtyFlag ;
    }

    /**
     * 获取 [媒体]
     */
    @JsonProperty("medium_id_text")
    public String getMedium_id_text(){
        return this.medium_id_text ;
    }

    /**
     * 设置 [媒体]
     */
    @JsonProperty("medium_id_text")
    public void setMedium_id_text(String  medium_id_text){
        this.medium_id_text = medium_id_text ;
        this.medium_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [媒体]脏标记
     */
    @JsonIgnore
    public boolean getMedium_id_textDirtyFlag(){
        return this.medium_id_textDirtyFlag ;
    }

    /**
     * 获取 [税科目调整]
     */
    @JsonProperty("fiscal_position_id_text")
    public String getFiscal_position_id_text(){
        return this.fiscal_position_id_text ;
    }

    /**
     * 设置 [税科目调整]
     */
    @JsonProperty("fiscal_position_id_text")
    public void setFiscal_position_id_text(String  fiscal_position_id_text){
        this.fiscal_position_id_text = fiscal_position_id_text ;
        this.fiscal_position_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [税科目调整]脏标记
     */
    @JsonIgnore
    public boolean getFiscal_position_id_textDirtyFlag(){
        return this.fiscal_position_id_textDirtyFlag ;
    }

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [价格表]
     */
    @JsonProperty("pricelist_id_text")
    public String getPricelist_id_text(){
        return this.pricelist_id_text ;
    }

    /**
     * 设置 [价格表]
     */
    @JsonProperty("pricelist_id_text")
    public void setPricelist_id_text(String  pricelist_id_text){
        this.pricelist_id_text = pricelist_id_text ;
        this.pricelist_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [价格表]脏标记
     */
    @JsonIgnore
    public boolean getPricelist_id_textDirtyFlag(){
        return this.pricelist_id_textDirtyFlag ;
    }

    /**
     * 获取 [付款条款]
     */
    @JsonProperty("payment_term_id_text")
    public String getPayment_term_id_text(){
        return this.payment_term_id_text ;
    }

    /**
     * 设置 [付款条款]
     */
    @JsonProperty("payment_term_id_text")
    public void setPayment_term_id_text(String  payment_term_id_text){
        this.payment_term_id_text = payment_term_id_text ;
        this.payment_term_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [付款条款]脏标记
     */
    @JsonIgnore
    public boolean getPayment_term_id_textDirtyFlag(){
        return this.payment_term_id_textDirtyFlag ;
    }

    /**
     * 获取 [报价单模板]
     */
    @JsonProperty("sale_order_template_id_text")
    public String getSale_order_template_id_text(){
        return this.sale_order_template_id_text ;
    }

    /**
     * 设置 [报价单模板]
     */
    @JsonProperty("sale_order_template_id_text")
    public void setSale_order_template_id_text(String  sale_order_template_id_text){
        this.sale_order_template_id_text = sale_order_template_id_text ;
        this.sale_order_template_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [报价单模板]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_template_id_textDirtyFlag(){
        return this.sale_order_template_id_textDirtyFlag ;
    }

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }

    /**
     * 获取 [销售团队]
     */
    @JsonProperty("team_id")
    public Integer getTeam_id(){
        return this.team_id ;
    }

    /**
     * 设置 [销售团队]
     */
    @JsonProperty("team_id")
    public void setTeam_id(Integer  team_id){
        this.team_id = team_id ;
        this.team_idDirtyFlag = true ;
    }

    /**
     * 获取 [销售团队]脏标记
     */
    @JsonIgnore
    public boolean getTeam_idDirtyFlag(){
        return this.team_idDirtyFlag ;
    }

    /**
     * 获取 [发票地址]
     */
    @JsonProperty("partner_invoice_id")
    public Integer getPartner_invoice_id(){
        return this.partner_invoice_id ;
    }

    /**
     * 设置 [发票地址]
     */
    @JsonProperty("partner_invoice_id")
    public void setPartner_invoice_id(Integer  partner_invoice_id){
        this.partner_invoice_id = partner_invoice_id ;
        this.partner_invoice_idDirtyFlag = true ;
    }

    /**
     * 获取 [发票地址]脏标记
     */
    @JsonIgnore
    public boolean getPartner_invoice_idDirtyFlag(){
        return this.partner_invoice_idDirtyFlag ;
    }

    /**
     * 获取 [税科目调整]
     */
    @JsonProperty("fiscal_position_id")
    public Integer getFiscal_position_id(){
        return this.fiscal_position_id ;
    }

    /**
     * 设置 [税科目调整]
     */
    @JsonProperty("fiscal_position_id")
    public void setFiscal_position_id(Integer  fiscal_position_id){
        this.fiscal_position_id = fiscal_position_id ;
        this.fiscal_position_idDirtyFlag = true ;
    }

    /**
     * 获取 [税科目调整]脏标记
     */
    @JsonIgnore
    public boolean getFiscal_position_idDirtyFlag(){
        return this.fiscal_position_idDirtyFlag ;
    }

    /**
     * 获取 [客户]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [客户]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [客户]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }

    /**
     * 获取 [营销]
     */
    @JsonProperty("campaign_id")
    public Integer getCampaign_id(){
        return this.campaign_id ;
    }

    /**
     * 设置 [营销]
     */
    @JsonProperty("campaign_id")
    public void setCampaign_id(Integer  campaign_id){
        this.campaign_id = campaign_id ;
        this.campaign_idDirtyFlag = true ;
    }

    /**
     * 获取 [营销]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_idDirtyFlag(){
        return this.campaign_idDirtyFlag ;
    }

    /**
     * 获取 [来源]
     */
    @JsonProperty("source_id")
    public Integer getSource_id(){
        return this.source_id ;
    }

    /**
     * 设置 [来源]
     */
    @JsonProperty("source_id")
    public void setSource_id(Integer  source_id){
        this.source_id = source_id ;
        this.source_idDirtyFlag = true ;
    }

    /**
     * 获取 [来源]脏标记
     */
    @JsonIgnore
    public boolean getSource_idDirtyFlag(){
        return this.source_idDirtyFlag ;
    }

    /**
     * 获取 [报价单模板]
     */
    @JsonProperty("sale_order_template_id")
    public Integer getSale_order_template_id(){
        return this.sale_order_template_id ;
    }

    /**
     * 设置 [报价单模板]
     */
    @JsonProperty("sale_order_template_id")
    public void setSale_order_template_id(Integer  sale_order_template_id){
        this.sale_order_template_id = sale_order_template_id ;
        this.sale_order_template_idDirtyFlag = true ;
    }

    /**
     * 获取 [报价单模板]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_template_idDirtyFlag(){
        return this.sale_order_template_idDirtyFlag ;
    }

    /**
     * 获取 [销售员]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [销售员]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [销售员]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }

    /**
     * 获取 [媒体]
     */
    @JsonProperty("medium_id")
    public Integer getMedium_id(){
        return this.medium_id ;
    }

    /**
     * 设置 [媒体]
     */
    @JsonProperty("medium_id")
    public void setMedium_id(Integer  medium_id){
        this.medium_id = medium_id ;
        this.medium_idDirtyFlag = true ;
    }

    /**
     * 获取 [媒体]脏标记
     */
    @JsonIgnore
    public boolean getMedium_idDirtyFlag(){
        return this.medium_idDirtyFlag ;
    }

    /**
     * 获取 [分析账户]
     */
    @JsonProperty("analytic_account_id")
    public Integer getAnalytic_account_id(){
        return this.analytic_account_id ;
    }

    /**
     * 设置 [分析账户]
     */
    @JsonProperty("analytic_account_id")
    public void setAnalytic_account_id(Integer  analytic_account_id){
        this.analytic_account_id = analytic_account_id ;
        this.analytic_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [分析账户]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_account_idDirtyFlag(){
        return this.analytic_account_idDirtyFlag ;
    }

    /**
     * 获取 [付款条款]
     */
    @JsonProperty("payment_term_id")
    public Integer getPayment_term_id(){
        return this.payment_term_id ;
    }

    /**
     * 设置 [付款条款]
     */
    @JsonProperty("payment_term_id")
    public void setPayment_term_id(Integer  payment_term_id){
        this.payment_term_id = payment_term_id ;
        this.payment_term_idDirtyFlag = true ;
    }

    /**
     * 获取 [付款条款]脏标记
     */
    @JsonIgnore
    public boolean getPayment_term_idDirtyFlag(){
        return this.payment_term_idDirtyFlag ;
    }

    /**
     * 获取 [送货地址]
     */
    @JsonProperty("partner_shipping_id")
    public Integer getPartner_shipping_id(){
        return this.partner_shipping_id ;
    }

    /**
     * 设置 [送货地址]
     */
    @JsonProperty("partner_shipping_id")
    public void setPartner_shipping_id(Integer  partner_shipping_id){
        this.partner_shipping_id = partner_shipping_id ;
        this.partner_shipping_idDirtyFlag = true ;
    }

    /**
     * 获取 [送货地址]脏标记
     */
    @JsonIgnore
    public boolean getPartner_shipping_idDirtyFlag(){
        return this.partner_shipping_idDirtyFlag ;
    }

    /**
     * 获取 [商机]
     */
    @JsonProperty("opportunity_id")
    public Integer getOpportunity_id(){
        return this.opportunity_id ;
    }

    /**
     * 设置 [商机]
     */
    @JsonProperty("opportunity_id")
    public void setOpportunity_id(Integer  opportunity_id){
        this.opportunity_id = opportunity_id ;
        this.opportunity_idDirtyFlag = true ;
    }

    /**
     * 获取 [商机]脏标记
     */
    @JsonIgnore
    public boolean getOpportunity_idDirtyFlag(){
        return this.opportunity_idDirtyFlag ;
    }

    /**
     * 获取 [贸易条款]
     */
    @JsonProperty("incoterm")
    public Integer getIncoterm(){
        return this.incoterm ;
    }

    /**
     * 设置 [贸易条款]
     */
    @JsonProperty("incoterm")
    public void setIncoterm(Integer  incoterm){
        this.incoterm = incoterm ;
        this.incotermDirtyFlag = true ;
    }

    /**
     * 获取 [贸易条款]脏标记
     */
    @JsonIgnore
    public boolean getIncotermDirtyFlag(){
        return this.incotermDirtyFlag ;
    }

    /**
     * 获取 [价格表]
     */
    @JsonProperty("pricelist_id")
    public Integer getPricelist_id(){
        return this.pricelist_id ;
    }

    /**
     * 设置 [价格表]
     */
    @JsonProperty("pricelist_id")
    public void setPricelist_id(Integer  pricelist_id){
        this.pricelist_id = pricelist_id ;
        this.pricelist_idDirtyFlag = true ;
    }

    /**
     * 获取 [价格表]脏标记
     */
    @JsonIgnore
    public boolean getPricelist_idDirtyFlag(){
        return this.pricelist_idDirtyFlag ;
    }

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }

    /**
     * 获取 [仓库]
     */
    @JsonProperty("warehouse_id")
    public Integer getWarehouse_id(){
        return this.warehouse_id ;
    }

    /**
     * 设置 [仓库]
     */
    @JsonProperty("warehouse_id")
    public void setWarehouse_id(Integer  warehouse_id){
        this.warehouse_id = warehouse_id ;
        this.warehouse_idDirtyFlag = true ;
    }

    /**
     * 获取 [仓库]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_idDirtyFlag(){
        return this.warehouse_idDirtyFlag ;
    }



	private List <cn.ibizlab.odoo.web.odoo_sale.domain.Sale_order_line> sale_order_lines = new ArrayList<cn.ibizlab.odoo.web.odoo_sale.domain.Sale_order_line>() ;

    public List<cn.ibizlab.odoo.web.odoo_sale.domain.Sale_order_line> getSale_order_lines(){
        return sale_order_lines ;
    }

    public void setSale_order_lines(List <cn.ibizlab.odoo.web.odoo_sale.domain.Sale_order_line> sale_order_lines){
        this.sale_order_lines = sale_order_lines ;
    }

}
