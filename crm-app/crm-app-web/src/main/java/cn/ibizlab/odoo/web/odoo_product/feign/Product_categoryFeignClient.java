package cn.ibizlab.odoo.web.odoo_product.feign;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.web.odoo_product.domain.Product_category;
import cn.ibizlab.odoo.web.odoo_product.filter.*;


public interface Product_categoryFeignClient {



	@RequestMapping(method = RequestMethod.DELETE, value = "/web/product_categories/{product_category_id}")
    public Boolean remove(@PathVariable("product_category_id") Integer product_category_id) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/product_categories")
    public Product_category create(@RequestBody Product_category product_category) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/product_categories/{product_category_id}/updatebatch")
    public Product_category updateBatch(@PathVariable("product_category_id") Integer product_category_id, @RequestBody Product_category product_category) ;

	@RequestMapping(method = RequestMethod.DELETE, value = "/web/product_categories/{product_category_id}/removebatch")
    public Product_category removeBatch(@PathVariable("product_category_id") Integer product_category_id, @RequestBody Product_category product_category) ;

	@RequestMapping(method = RequestMethod.PUT, value = "/web/product_categories/{product_category_id}")
    public Product_category update(@PathVariable("product_category_id") Integer product_category_id, @RequestBody Product_category product_category) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/product_categories/{product_category_id}/save")
    public Product_category save(@PathVariable("product_category_id") Integer product_category_id, @RequestBody Product_category product_category) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/product_categories/{product_category_id}/getdraft")
    public Product_category getDraft(@PathVariable("product_category_id") Integer product_category_id, @RequestBody Product_category product_category) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/product_categories/{product_category_id}/createbatch")
    public Product_category createBatch(@PathVariable("product_category_id") Integer product_category_id, @RequestBody Product_category product_category) ;

	@RequestMapping(method = RequestMethod.POST, value = "/web/product_categories/checkkey")
    public Boolean checkKey(@RequestBody Product_category product_category) ;

	@RequestMapping(method = RequestMethod.GET, value = "/web/product_categories/{product_category_id}")
    public Product_category get(@PathVariable("product_category_id") Integer product_category_id) ;

    @RequestMapping(method= RequestMethod.GET , value="/web/product_categories/fetchdefault")
	public Page<Product_category> fetchDefault(Product_categorySearchContext searchContext) ;
}
