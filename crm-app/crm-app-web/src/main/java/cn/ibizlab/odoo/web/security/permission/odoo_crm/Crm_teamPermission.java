package cn.ibizlab.odoo.web.security.permission.odoo_crm;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;

import cn.ibizlab.odoo.web.odoo_crm.service.Crm_teamService;
import cn.ibizlab.odoo.web.odoo_crm.domain.Crm_team;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Component("crm_team_pms")
public class Crm_teamPermission {

    @Autowired
    Crm_teamService crm_teamservice;

    public boolean check(String action){
        return true ;
    }

    public boolean check(Integer crm_team_id, String action){
        return true ;
    }


}
