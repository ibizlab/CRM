package cn.ibizlab.odoo.web.odoo_calendar.service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.util.DigestUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import org.springframework.stereotype.Service;

import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.web.odoo_calendar.domain.Calendar_event;
import cn.ibizlab.odoo.web.odoo_calendar.filter.*;
import cn.ibizlab.odoo.web.odoo_calendar.feign.Calendar_eventFeignClient;
import cn.ibizlab.odoo.web.WebApplication.WebClientProperties;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Service
public class Calendar_eventService {

    Calendar_eventFeignClient client;

    @Autowired
    public Calendar_eventService(Decoder decoder, Encoder encoder, Client client, Contract contract, FeignRequestInterceptor feignRequestInterceptor,
                                    WebClientProperties webClientProperties) {
        if (webClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Calendar_eventFeignClient.class,"http://" + webClientProperties.getServiceId() + "/") ;
		} else if (webClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.client = nameBuilder.target(Calendar_eventFeignClient.class, "http://" + webClientProperties.getServiceUrl() + "/") ;
		}
    }

    public Calendar_event update(Integer calendar_event_id, Calendar_event calendar_event) {
        return client.update(calendar_event_id, calendar_event);
    }

    public Calendar_event updateBatch(Integer calendar_event_id, Calendar_event calendar_event) {
        return client.updateBatch(calendar_event_id, calendar_event);
    }

    public Calendar_event createBatch(Integer calendar_event_id, Calendar_event calendar_event) {
        return client.createBatch(calendar_event_id, calendar_event);
    }

    public Calendar_event get( Integer calendar_event_id) {
        return client.get( calendar_event_id);
    }

    public Calendar_event removeBatch(Integer calendar_event_id, Calendar_event calendar_event) {
        return client.removeBatch(calendar_event_id, calendar_event);
    }

    public boolean remove( Integer calendar_event_id) {
        return client.remove( calendar_event_id);
    }

    public Calendar_event getDraft(Integer calendar_event_id, Calendar_event calendar_event) {
        return client.getDraft(calendar_event_id, calendar_event);
    }

	public Calendar_event create(Calendar_event calendar_event) {
        return client.create(calendar_event);
    }

    public Calendar_event save(Integer calendar_event_id, Calendar_event calendar_event) {
        return client.save(calendar_event_id, calendar_event);
    }

    public boolean checkKey(Calendar_event calendar_event) {
        return client.checkKey(calendar_event);
    }

	public Page<Calendar_event> fetchDefault(Calendar_eventSearchContext searchContext) {
        return client.fetchDefault(searchContext);
    }

}
