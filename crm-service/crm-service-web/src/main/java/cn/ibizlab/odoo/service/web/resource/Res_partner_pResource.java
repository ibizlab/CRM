package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.web.dto.Res_partner_pDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_partnerService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partnerSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Res_partner_p" })
@RestController
@RequestMapping("")
public class Res_partner_pResource {

    @Autowired
    private IRes_partnerService res_partnerService;

    public IRes_partnerService getRes_partnerService() {
        return this.res_partnerService;
    }

    @ApiOperation(value = "删除数据", tags = {"Res_partner_p" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_partner_ps/{res_partner_p_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_partner_p_id") Integer res_partner_p_id) {
        Res_partner_pDTO res_partner_pdto = new Res_partner_pDTO();
		Res_partner domain = new Res_partner();
		res_partner_pdto.setId(res_partner_p_id);
		domain.setId(res_partner_p_id);
        Boolean rst = res_partnerService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Res_partner_p" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/res_partner_ps/{res_partner_p_id}")
    public ResponseEntity<Res_partner_pDTO> get(@PathVariable("res_partner_p_id") Integer res_partner_p_id) {
        Res_partner_pDTO dto = new Res_partner_pDTO();
        Res_partner domain = res_partnerService.get(res_partner_p_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "CheckKey", tags = {"Res_partner_p" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partner_ps/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_partner_pDTO res_partner_pdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "Save", tags = {"Res_partner_p" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partner_ps/{res_partner_p_id}/save")
    public ResponseEntity<Boolean> save(@RequestBody Res_partner_pDTO res_partner_pdto) {
        Res_partner res_partner = res_partner_pdto.toDO();
    	Boolean b = res_partnerService.save(res_partner) ;
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    @ApiOperation(value = "更新数据", tags = {"Res_partner_p" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_partner_ps/{res_partner_p_id}")

    public ResponseEntity<Res_partner_pDTO> update(@PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_pDTO res_partner_pdto) {
		Res_partner domain = res_partner_pdto.toDO();
        domain.setId(res_partner_p_id);
		res_partnerService.update(domain);
		Res_partner_pDTO dto = new Res_partner_pDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Res_partner_p" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partner_ps/createBatch")
    public ResponseEntity<Boolean> createBatchRes_partner(@RequestBody List<Res_partner_pDTO> res_partner_pdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Res_partner_p" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_partner_ps/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_partner_pDTO> res_partner_pdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Res_partner_p" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/res_partner_ps/{res_partner_p_id}/getdraft")

    public ResponseEntity<Res_partner_pDTO> getDraft(@PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_pDTO res_partner_pdto) {
        Res_partner res_partner = res_partner_pdto.toDO();
    	res_partner = res_partnerService.getDraft(res_partner) ;
    	res_partner_pdto.fromDO(res_partner);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_pdto);
    }

    @ApiOperation(value = "建立数据", tags = {"Res_partner_p" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partner_ps")

    public ResponseEntity<Res_partner_pDTO> create(@RequestBody Res_partner_pDTO res_partner_pdto) {
        Res_partner_pDTO dto = new Res_partner_pDTO();
        Res_partner domain = res_partner_pdto.toDO();
		res_partnerService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Res_partner_p" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_partner_ps/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Res_partner_pDTO> res_partner_pdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取联系人（人）", tags = {"Res_partner_p" } ,notes = "获取联系人（人）")
    @RequestMapping(method= RequestMethod.GET , value="/web/res_partner_ps/fetchcontacts")
	public ResponseEntity<Page<Res_partner_pDTO>> fetchContacts(Res_partnerSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Res_partner_pDTO> list = new ArrayList<Res_partner_pDTO>();
        
        Page<Res_partner> domains = res_partnerService.searchContacts(context) ;
        for(Res_partner res_partner : domains.getContent()){
            Res_partner_pDTO dto = new Res_partner_pDTO();
            dto.fromDO(res_partner);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}

	@ApiOperation(value = "获取联系人（公司）", tags = {"Res_partner_p" } ,notes = "获取联系人（公司）")
    @RequestMapping(method= RequestMethod.GET , value="/web/res_partner_ps/fetchcompany")
	public ResponseEntity<Page<Res_partner_pDTO>> fetchCompany(Res_partnerSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Res_partner_pDTO> list = new ArrayList<Res_partner_pDTO>();
        
        Page<Res_partner> domains = res_partnerService.searchCompany(context) ;
        for(Res_partner res_partner : domains.getContent()){
            Res_partner_pDTO dto = new Res_partner_pDTO();
            dto.fromDO(res_partner);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}

	@ApiOperation(value = "获取默认查询", tags = {"Res_partner_p" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/res_partner_ps/fetchdefault")
	public ResponseEntity<Page<Res_partner_pDTO>> fetchDefault(Res_partnerSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Res_partner_pDTO> list = new ArrayList<Res_partner_pDTO>();
        
        Page<Res_partner> domains = res_partnerService.searchDefault(context) ;
        for(Res_partner res_partner : domains.getContent()){
            Res_partner_pDTO dto = new Res_partner_pDTO();
            dto.fromDO(res_partner);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}



    @ApiOperation(value = "删除数据", tags = {"Res_partner_p" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_partners/{res_partner_id}/res_partner_ps/{res_partner_p_id}")

    public ResponseEntity<Boolean> removeByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("res_partner_p_id") Integer res_partner_p_id) {
        Res_partner_pDTO res_partner_pdto = new Res_partner_pDTO();
		Res_partner domain = new Res_partner();
		res_partner_pdto.setId(res_partner_p_id);
		domain.setId(res_partner_p_id);
        Boolean rst = res_partnerService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Res_partner_p" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/res_partners/{res_partner_id}/res_partner_ps/{res_partner_p_id}")
    public ResponseEntity<Res_partner_pDTO> getByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("res_partner_p_id") Integer res_partner_p_id) {
        Res_partner_pDTO dto = new Res_partner_pDTO();
        Res_partner domain = res_partnerService.get(res_partner_p_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "CheckKey", tags = {"Res_partner_p" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/res_partner_ps/checkkey")
    public ResponseEntity<Boolean> checkKeyByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Res_partner_pDTO res_partner_pdto) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "Save", tags = {"Res_partner_p" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/res_partner_ps/save")
    public ResponseEntity<Boolean> saveByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Res_partner_pDTO res_partner_pdto) {
        Res_partner domain = res_partner_pdto.toDO();
        domain.setParent_id(res_partner_id);
    	Boolean b = res_partnerService.save(domain) ;
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    @ApiOperation(value = "更新数据", tags = {"Res_partner_p" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_partners/{res_partner_id}/res_partner_ps/{res_partner_p_id}")

    public ResponseEntity<Res_partner_pDTO> updateByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_pDTO res_partner_pdto) {
		Res_partner domain = res_partner_pdto.toDO();
        domain.setParent_id(res_partner_id);
        domain.setId(res_partner_p_id);
		res_partnerService.update(domain);
		Res_partner_pDTO dto = new Res_partner_pDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Res_partner_p" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/res_partner_ps/createbatch")
    public ResponseEntity<Boolean> createBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody List<Res_partner_pDTO> res_partner_pdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Res_partner_p" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_partners/{res_partner_id}/res_partner_ps/updatebatch")
    public ResponseEntity<Boolean> updateBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody List<Res_partner_pDTO> res_partner_pdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Res_partner_p" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/res_partners/{res_partner_id}/res_partner_ps/{res_partner_pid}/getdraft")

    public ResponseEntity<Res_partner_pDTO> getDraftByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("res_partner_p_id") Integer res_partner_p_id, @RequestBody Res_partner_pDTO res_partner_pdto) {
        Res_partner res_partner = res_partner_pdto.toDO();
    	res_partner = res_partnerService.getDraft(res_partner) ;
    	res_partner_pdto.fromDO(res_partner);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_pdto);
    }

    @ApiOperation(value = "建立数据", tags = {"Res_partner_p" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/res_partner_ps")

    public ResponseEntity<Res_partner_pDTO> createByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Res_partner_pDTO res_partner_pdto) {
        Res_partner_pDTO dto = new Res_partner_pDTO();
        Res_partner domain = res_partner_pdto.toDO();
        domain.setParent_id(res_partner_id);
		res_partnerService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Res_partner_p" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_partners/{res_partner_id}/res_partner_ps/removebatch")
    public ResponseEntity<Boolean> removeBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody List<Res_partner_pDTO> res_partner_pdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取联系人（人）", tags = {"Res_partner_p" } ,notes = "获取联系人（人）")
    @RequestMapping(method= RequestMethod.GET , value="/web/res_partners/{res_partner_id}/res_partner_ps/fetchcontacts")
	public ResponseEntity<Page<Res_partner_pDTO>> fetchRes_partner_pContactsByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id,Res_partnerSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Res_partner_pDTO> list = new ArrayList<Res_partner_pDTO>();
        context.setN_parent_id_eq(res_partner_id);
        Page<Res_partner> domains = res_partnerService.searchContacts(context) ;
        for(Res_partner res_partner : domains.getContent()){
            Res_partner_pDTO dto = new Res_partner_pDTO();
            dto.fromDO(res_partner);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}

	@ApiOperation(value = "获取联系人（公司）", tags = {"Res_partner_p" } ,notes = "获取联系人（公司）")
    @RequestMapping(method= RequestMethod.GET , value="/web/res_partners/{res_partner_id}/res_partner_ps/fetchcompany")
	public ResponseEntity<Page<Res_partner_pDTO>> fetchRes_partner_pCompanyByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id,Res_partnerSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Res_partner_pDTO> list = new ArrayList<Res_partner_pDTO>();
        context.setN_parent_id_eq(res_partner_id);
        Page<Res_partner> domains = res_partnerService.searchCompany(context) ;
        for(Res_partner res_partner : domains.getContent()){
            Res_partner_pDTO dto = new Res_partner_pDTO();
            dto.fromDO(res_partner);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}

	@ApiOperation(value = "获取默认查询", tags = {"Res_partner_p" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/res_partners/{res_partner_id}/res_partner_ps/fetchdefault")
	public ResponseEntity<Page<Res_partner_pDTO>> fetchRes_partner_pDefault(@PathVariable("res_partner_id") Integer res_partner_id,Res_partnerSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Res_partner_pDTO> list = new ArrayList<Res_partner_pDTO>();
        context.setN_parent_id_eq(res_partner_id);
        Page<Res_partner> domains = res_partnerService.searchDefault(context) ;
        for(Res_partner res_partner : domains.getContent()){
            Res_partner_pDTO dto = new Res_partner_pDTO();
            dto.fromDO(res_partner);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
