package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.web.dto.Account_payment_term_lineDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_term_line;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_payment_term_lineService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_payment_term_lineSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_payment_term_line" })
@RestController
@RequestMapping("")
public class Account_payment_term_lineResource {

    @Autowired
    private IAccount_payment_term_lineService account_payment_term_lineService;

    public IAccount_payment_term_lineService getAccount_payment_term_lineService() {
        return this.account_payment_term_lineService;
    }

    @ApiOperation(value = "更新数据", tags = {"Account_payment_term_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/account_payment_term_lines/{account_payment_term_line_id}")

    public ResponseEntity<Account_payment_term_lineDTO> update(@PathVariable("account_payment_term_line_id") Integer account_payment_term_line_id, @RequestBody Account_payment_term_lineDTO account_payment_term_linedto) {
		Account_payment_term_line domain = account_payment_term_linedto.toDO();
        domain.setId(account_payment_term_line_id);
		account_payment_term_lineService.update(domain);
		Account_payment_term_lineDTO dto = new Account_payment_term_lineDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_payment_term_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/account_payment_term_lines")

    public ResponseEntity<Account_payment_term_lineDTO> create(@RequestBody Account_payment_term_lineDTO account_payment_term_linedto) {
        Account_payment_term_lineDTO dto = new Account_payment_term_lineDTO();
        Account_payment_term_line domain = account_payment_term_linedto.toDO();
		account_payment_term_lineService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_payment_term_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/account_payment_term_lines/{account_payment_term_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_payment_term_line_id") Integer account_payment_term_line_id) {
        Account_payment_term_lineDTO account_payment_term_linedto = new Account_payment_term_lineDTO();
		Account_payment_term_line domain = new Account_payment_term_line();
		account_payment_term_linedto.setId(account_payment_term_line_id);
		domain.setId(account_payment_term_line_id);
        Boolean rst = account_payment_term_lineService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_payment_term_line" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/account_payment_term_lines/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_payment_term_lineDTO> account_payment_term_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_payment_term_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/account_payment_term_lines/{account_payment_term_line_id}")
    public ResponseEntity<Account_payment_term_lineDTO> get(@PathVariable("account_payment_term_line_id") Integer account_payment_term_line_id) {
        Account_payment_term_lineDTO dto = new Account_payment_term_lineDTO();
        Account_payment_term_line domain = account_payment_term_lineService.get(account_payment_term_line_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_payment_term_line" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/account_payment_term_lines/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_payment_term_lineDTO> account_payment_term_linedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_payment_term_line" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/account_payment_term_lines/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_payment_term_line(@RequestBody List<Account_payment_term_lineDTO> account_payment_term_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Account_payment_term_line" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/account_payment_term_lines/{account_payment_term_line_id}/getdraft")

    public ResponseEntity<Account_payment_term_lineDTO> getDraft(@PathVariable("account_payment_term_line_id") Integer account_payment_term_line_id, @RequestBody Account_payment_term_lineDTO account_payment_term_linedto) {
        Account_payment_term_line account_payment_term_line = account_payment_term_linedto.toDO();
    	account_payment_term_line = account_payment_term_lineService.getDraft(account_payment_term_line) ;
    	account_payment_term_linedto.fromDO(account_payment_term_line);
        return ResponseEntity.status(HttpStatus.OK).body(account_payment_term_linedto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_payment_term_line" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/account_payment_term_lines/fetchdefault")
	public ResponseEntity<Page<Account_payment_term_lineDTO>> fetchDefault(Account_payment_term_lineSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_payment_term_lineDTO> list = new ArrayList<Account_payment_term_lineDTO>();
        
        Page<Account_payment_term_line> domains = account_payment_term_lineService.searchDefault(context) ;
        for(Account_payment_term_line account_payment_term_line : domains.getContent()){
            Account_payment_term_lineDTO dto = new Account_payment_term_lineDTO();
            dto.fromDO(account_payment_term_line);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
