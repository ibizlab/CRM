package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.web.dto.Crm_lead_lostDTO;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead_lost;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_lead_lostService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lead_lostSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Crm_lead_lost" })
@RestController
@RequestMapping("")
public class Crm_lead_lostResource {

    @Autowired
    private ICrm_lead_lostService crm_lead_lostService;

    public ICrm_lead_lostService getCrm_lead_lostService() {
        return this.crm_lead_lostService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Crm_lead_lost" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_lead_losts/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_lead_lostDTO> crm_lead_lostdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Crm_lead_lost" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_lead_losts/{crm_lead_lost_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("crm_lead_lost_id") Integer crm_lead_lost_id) {
        Crm_lead_lostDTO crm_lead_lostdto = new Crm_lead_lostDTO();
		Crm_lead_lost domain = new Crm_lead_lost();
		crm_lead_lostdto.setId(crm_lead_lost_id);
		domain.setId(crm_lead_lost_id);
        Boolean rst = crm_lead_lostService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Crm_lead_lost" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_lead_losts/{crm_lead_lost_id}")

    public ResponseEntity<Crm_lead_lostDTO> update(@PathVariable("crm_lead_lost_id") Integer crm_lead_lost_id, @RequestBody Crm_lead_lostDTO crm_lead_lostdto) {
		Crm_lead_lost domain = crm_lead_lostdto.toDO();
        domain.setId(crm_lead_lost_id);
		crm_lead_lostService.update(domain);
		Crm_lead_lostDTO dto = new Crm_lead_lostDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Crm_lead_lost" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_lead_losts/{crm_lead_lost_id}/getdraft")

    public ResponseEntity<Crm_lead_lostDTO> getDraft(@PathVariable("crm_lead_lost_id") Integer crm_lead_lost_id, @RequestBody Crm_lead_lostDTO crm_lead_lostdto) {
        Crm_lead_lost crm_lead_lost = crm_lead_lostdto.toDO();
    	crm_lead_lost = crm_lead_lostService.getDraft(crm_lead_lost) ;
    	crm_lead_lostdto.fromDO(crm_lead_lost);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead_lostdto);
    }

    @ApiOperation(value = "获取数据", tags = {"Crm_lead_lost" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_lead_losts/{crm_lead_lost_id}")
    public ResponseEntity<Crm_lead_lostDTO> get(@PathVariable("crm_lead_lost_id") Integer crm_lead_lost_id) {
        Crm_lead_lostDTO dto = new Crm_lead_lostDTO();
        Crm_lead_lost domain = crm_lead_lostService.get(crm_lead_lost_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Crm_lead_lost" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_lead_losts/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Crm_lead_lostDTO> crm_lead_lostdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Crm_lead_lost" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_lead_losts")

    public ResponseEntity<Crm_lead_lostDTO> create(@RequestBody Crm_lead_lostDTO crm_lead_lostdto) {
        Crm_lead_lostDTO dto = new Crm_lead_lostDTO();
        Crm_lead_lost domain = crm_lead_lostdto.toDO();
		crm_lead_lostService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Crm_lead_lost" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_lead_losts/createBatch")
    public ResponseEntity<Boolean> createBatchCrm_lead_lost(@RequestBody List<Crm_lead_lostDTO> crm_lead_lostdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Crm_lead_lost" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/crm_lead_losts/fetchdefault")
	public ResponseEntity<Page<Crm_lead_lostDTO>> fetchDefault(Crm_lead_lostSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Crm_lead_lostDTO> list = new ArrayList<Crm_lead_lostDTO>();
        
        Page<Crm_lead_lost> domains = crm_lead_lostService.searchDefault(context) ;
        for(Crm_lead_lost crm_lead_lost : domains.getContent()){
            Crm_lead_lostDTO dto = new Crm_lead_lostDTO();
            dto.fromDO(crm_lead_lost);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
