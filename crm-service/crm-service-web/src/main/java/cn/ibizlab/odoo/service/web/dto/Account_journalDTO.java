package cn.ibizlab.odoo.service.web.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_journal.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_journal;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Account_journalDTO]
 */
public class Account_journalDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [REFUND_SEQUENCE_NUMBER_NEXT]
     *
     */
    @Account_journalRefund_sequence_number_nextDefault(info = "默认规则")
    private Integer refund_sequence_number_next;

    @JsonIgnore
    private boolean refund_sequence_number_nextDirtyFlag;

    /**
     * 属性 [TYPE_CONTROL_IDS]
     *
     */
    @Account_journalType_control_idsDefault(info = "默认规则")
    private String type_control_ids;

    @JsonIgnore
    private boolean type_control_idsDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Account_journalWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [AT_LEAST_ONE_OUTBOUND]
     *
     */
    @Account_journalAt_least_one_outboundDefault(info = "默认规则")
    private String at_least_one_outbound;

    @JsonIgnore
    private boolean at_least_one_outboundDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Account_journalSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [SEQUENCE_ID]
     *
     */
    @Account_journalSequence_idDefault(info = "默认规则")
    private Integer sequence_id;

    @JsonIgnore
    private boolean sequence_idDirtyFlag;

    /**
     * 属性 [SHOW_ON_DASHBOARD]
     *
     */
    @Account_journalShow_on_dashboardDefault(info = "默认规则")
    private String show_on_dashboard;

    @JsonIgnore
    private boolean show_on_dashboardDirtyFlag;

    /**
     * 属性 [JOURNAL_USER]
     *
     */
    @Account_journalJournal_userDefault(info = "默认规则")
    private String journal_user;

    @JsonIgnore
    private boolean journal_userDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Account_journalIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [BANK_STATEMENTS_SOURCE]
     *
     */
    @Account_journalBank_statements_sourceDefault(info = "默认规则")
    private String bank_statements_source;

    @JsonIgnore
    private boolean bank_statements_sourceDirtyFlag;

    /**
     * 属性 [AT_LEAST_ONE_INBOUND]
     *
     */
    @Account_journalAt_least_one_inboundDefault(info = "默认规则")
    private String at_least_one_inbound;

    @JsonIgnore
    private boolean at_least_one_inboundDirtyFlag;

    /**
     * 属性 [POST_AT_BANK_REC]
     *
     */
    @Account_journalPost_at_bank_recDefault(info = "默认规则")
    private String post_at_bank_rec;

    @JsonIgnore
    private boolean post_at_bank_recDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Account_journalNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [SEQUENCE_NUMBER_NEXT]
     *
     */
    @Account_journalSequence_number_nextDefault(info = "默认规则")
    private Integer sequence_number_next;

    @JsonIgnore
    private boolean sequence_number_nextDirtyFlag;

    /**
     * 属性 [BELONGS_TO_COMPANY]
     *
     */
    @Account_journalBelongs_to_companyDefault(info = "默认规则")
    private String belongs_to_company;

    @JsonIgnore
    private boolean belongs_to_companyDirtyFlag;

    /**
     * 属性 [CODE]
     *
     */
    @Account_journalCodeDefault(info = "默认规则")
    private String code;

    @JsonIgnore
    private boolean codeDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Account_journalCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [INBOUND_PAYMENT_METHOD_IDS]
     *
     */
    @Account_journalInbound_payment_method_idsDefault(info = "默认规则")
    private String inbound_payment_method_ids;

    @JsonIgnore
    private boolean inbound_payment_method_idsDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Account_journalDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Account_journal__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [GROUP_INVOICE_LINES]
     *
     */
    @Account_journalGroup_invoice_linesDefault(info = "默认规则")
    private String group_invoice_lines;

    @JsonIgnore
    private boolean group_invoice_linesDirtyFlag;

    /**
     * 属性 [REFUND_SEQUENCE_ID]
     *
     */
    @Account_journalRefund_sequence_idDefault(info = "默认规则")
    private Integer refund_sequence_id;

    @JsonIgnore
    private boolean refund_sequence_idDirtyFlag;

    /**
     * 属性 [ALIAS_DOMAIN]
     *
     */
    @Account_journalAlias_domainDefault(info = "默认规则")
    private String alias_domain;

    @JsonIgnore
    private boolean alias_domainDirtyFlag;

    /**
     * 属性 [ACCOUNT_CONTROL_IDS]
     *
     */
    @Account_journalAccount_control_idsDefault(info = "默认规则")
    private String account_control_ids;

    @JsonIgnore
    private boolean account_control_idsDirtyFlag;

    /**
     * 属性 [UPDATE_POSTED]
     *
     */
    @Account_journalUpdate_postedDefault(info = "默认规则")
    private String update_posted;

    @JsonIgnore
    private boolean update_postedDirtyFlag;

    /**
     * 属性 [AMOUNT_AUTHORIZED_DIFF]
     *
     */
    @Account_journalAmount_authorized_diffDefault(info = "默认规则")
    private Double amount_authorized_diff;

    @JsonIgnore
    private boolean amount_authorized_diffDirtyFlag;

    /**
     * 属性 [KANBAN_DASHBOARD_GRAPH]
     *
     */
    @Account_journalKanban_dashboard_graphDefault(info = "默认规则")
    private String kanban_dashboard_graph;

    @JsonIgnore
    private boolean kanban_dashboard_graphDirtyFlag;

    /**
     * 属性 [OUTBOUND_PAYMENT_METHOD_IDS]
     *
     */
    @Account_journalOutbound_payment_method_idsDefault(info = "默认规则")
    private String outbound_payment_method_ids;

    @JsonIgnore
    private boolean outbound_payment_method_idsDirtyFlag;

    /**
     * 属性 [TYPE]
     *
     */
    @Account_journalTypeDefault(info = "默认规则")
    private String type;

    @JsonIgnore
    private boolean typeDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Account_journalActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [KANBAN_DASHBOARD]
     *
     */
    @Account_journalKanban_dashboardDefault(info = "默认规则")
    private String kanban_dashboard;

    @JsonIgnore
    private boolean kanban_dashboardDirtyFlag;

    /**
     * 属性 [REFUND_SEQUENCE]
     *
     */
    @Account_journalRefund_sequenceDefault(info = "默认规则")
    private String refund_sequence;

    @JsonIgnore
    private boolean refund_sequenceDirtyFlag;

    /**
     * 属性 [COLOR]
     *
     */
    @Account_journalColorDefault(info = "默认规则")
    private Integer color;

    @JsonIgnore
    private boolean colorDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Account_journalCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [PROFIT_ACCOUNT_ID_TEXT]
     *
     */
    @Account_journalProfit_account_id_textDefault(info = "默认规则")
    private String profit_account_id_text;

    @JsonIgnore
    private boolean profit_account_id_textDirtyFlag;

    /**
     * 属性 [ALIAS_NAME]
     *
     */
    @Account_journalAlias_nameDefault(info = "默认规则")
    private String alias_name;

    @JsonIgnore
    private boolean alias_nameDirtyFlag;

    /**
     * 属性 [BANK_ACC_NUMBER]
     *
     */
    @Account_journalBank_acc_numberDefault(info = "默认规则")
    private String bank_acc_number;

    @JsonIgnore
    private boolean bank_acc_numberDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Account_journalWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [BANK_ID]
     *
     */
    @Account_journalBank_idDefault(info = "默认规则")
    private Integer bank_id;

    @JsonIgnore
    private boolean bank_idDirtyFlag;

    /**
     * 属性 [LOSS_ACCOUNT_ID_TEXT]
     *
     */
    @Account_journalLoss_account_id_textDefault(info = "默认规则")
    private String loss_account_id_text;

    @JsonIgnore
    private boolean loss_account_id_textDirtyFlag;

    /**
     * 属性 [DEFAULT_CREDIT_ACCOUNT_ID_TEXT]
     *
     */
    @Account_journalDefault_credit_account_id_textDefault(info = "默认规则")
    private String default_credit_account_id_text;

    @JsonIgnore
    private boolean default_credit_account_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_PARTNER_ID]
     *
     */
    @Account_journalCompany_partner_idDefault(info = "默认规则")
    private Integer company_partner_id;

    @JsonIgnore
    private boolean company_partner_idDirtyFlag;

    /**
     * 属性 [DEFAULT_DEBIT_ACCOUNT_ID_TEXT]
     *
     */
    @Account_journalDefault_debit_account_id_textDefault(info = "默认规则")
    private String default_debit_account_id_text;

    @JsonIgnore
    private boolean default_debit_account_id_textDirtyFlag;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @Account_journalCurrency_id_textDefault(info = "默认规则")
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Account_journalCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [BANK_ACCOUNT_ID]
     *
     */
    @Account_journalBank_account_idDefault(info = "默认规则")
    private Integer bank_account_id;

    @JsonIgnore
    private boolean bank_account_idDirtyFlag;

    /**
     * 属性 [LOSS_ACCOUNT_ID]
     *
     */
    @Account_journalLoss_account_idDefault(info = "默认规则")
    private Integer loss_account_id;

    @JsonIgnore
    private boolean loss_account_idDirtyFlag;

    /**
     * 属性 [ALIAS_ID]
     *
     */
    @Account_journalAlias_idDefault(info = "默认规则")
    private Integer alias_id;

    @JsonIgnore
    private boolean alias_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Account_journalCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [DEFAULT_CREDIT_ACCOUNT_ID]
     *
     */
    @Account_journalDefault_credit_account_idDefault(info = "默认规则")
    private Integer default_credit_account_id;

    @JsonIgnore
    private boolean default_credit_account_idDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Account_journalCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Account_journalCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [DEFAULT_DEBIT_ACCOUNT_ID]
     *
     */
    @Account_journalDefault_debit_account_idDefault(info = "默认规则")
    private Integer default_debit_account_id;

    @JsonIgnore
    private boolean default_debit_account_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Account_journalWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [PROFIT_ACCOUNT_ID]
     *
     */
    @Account_journalProfit_account_idDefault(info = "默认规则")
    private Integer profit_account_id;

    @JsonIgnore
    private boolean profit_account_idDirtyFlag;


    /**
     * 获取 [REFUND_SEQUENCE_NUMBER_NEXT]
     */
    @JsonProperty("refund_sequence_number_next")
    public Integer getRefund_sequence_number_next(){
        return refund_sequence_number_next ;
    }

    /**
     * 设置 [REFUND_SEQUENCE_NUMBER_NEXT]
     */
    @JsonProperty("refund_sequence_number_next")
    public void setRefund_sequence_number_next(Integer  refund_sequence_number_next){
        this.refund_sequence_number_next = refund_sequence_number_next ;
        this.refund_sequence_number_nextDirtyFlag = true ;
    }

    /**
     * 获取 [REFUND_SEQUENCE_NUMBER_NEXT]脏标记
     */
    @JsonIgnore
    public boolean getRefund_sequence_number_nextDirtyFlag(){
        return refund_sequence_number_nextDirtyFlag ;
    }

    /**
     * 获取 [TYPE_CONTROL_IDS]
     */
    @JsonProperty("type_control_ids")
    public String getType_control_ids(){
        return type_control_ids ;
    }

    /**
     * 设置 [TYPE_CONTROL_IDS]
     */
    @JsonProperty("type_control_ids")
    public void setType_control_ids(String  type_control_ids){
        this.type_control_ids = type_control_ids ;
        this.type_control_idsDirtyFlag = true ;
    }

    /**
     * 获取 [TYPE_CONTROL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getType_control_idsDirtyFlag(){
        return type_control_idsDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [AT_LEAST_ONE_OUTBOUND]
     */
    @JsonProperty("at_least_one_outbound")
    public String getAt_least_one_outbound(){
        return at_least_one_outbound ;
    }

    /**
     * 设置 [AT_LEAST_ONE_OUTBOUND]
     */
    @JsonProperty("at_least_one_outbound")
    public void setAt_least_one_outbound(String  at_least_one_outbound){
        this.at_least_one_outbound = at_least_one_outbound ;
        this.at_least_one_outboundDirtyFlag = true ;
    }

    /**
     * 获取 [AT_LEAST_ONE_OUTBOUND]脏标记
     */
    @JsonIgnore
    public boolean getAt_least_one_outboundDirtyFlag(){
        return at_least_one_outboundDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE_ID]
     */
    @JsonProperty("sequence_id")
    public Integer getSequence_id(){
        return sequence_id ;
    }

    /**
     * 设置 [SEQUENCE_ID]
     */
    @JsonProperty("sequence_id")
    public void setSequence_id(Integer  sequence_id){
        this.sequence_id = sequence_id ;
        this.sequence_idDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE_ID]脏标记
     */
    @JsonIgnore
    public boolean getSequence_idDirtyFlag(){
        return sequence_idDirtyFlag ;
    }

    /**
     * 获取 [SHOW_ON_DASHBOARD]
     */
    @JsonProperty("show_on_dashboard")
    public String getShow_on_dashboard(){
        return show_on_dashboard ;
    }

    /**
     * 设置 [SHOW_ON_DASHBOARD]
     */
    @JsonProperty("show_on_dashboard")
    public void setShow_on_dashboard(String  show_on_dashboard){
        this.show_on_dashboard = show_on_dashboard ;
        this.show_on_dashboardDirtyFlag = true ;
    }

    /**
     * 获取 [SHOW_ON_DASHBOARD]脏标记
     */
    @JsonIgnore
    public boolean getShow_on_dashboardDirtyFlag(){
        return show_on_dashboardDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_USER]
     */
    @JsonProperty("journal_user")
    public String getJournal_user(){
        return journal_user ;
    }

    /**
     * 设置 [JOURNAL_USER]
     */
    @JsonProperty("journal_user")
    public void setJournal_user(String  journal_user){
        this.journal_user = journal_user ;
        this.journal_userDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_USER]脏标记
     */
    @JsonIgnore
    public boolean getJournal_userDirtyFlag(){
        return journal_userDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [BANK_STATEMENTS_SOURCE]
     */
    @JsonProperty("bank_statements_source")
    public String getBank_statements_source(){
        return bank_statements_source ;
    }

    /**
     * 设置 [BANK_STATEMENTS_SOURCE]
     */
    @JsonProperty("bank_statements_source")
    public void setBank_statements_source(String  bank_statements_source){
        this.bank_statements_source = bank_statements_source ;
        this.bank_statements_sourceDirtyFlag = true ;
    }

    /**
     * 获取 [BANK_STATEMENTS_SOURCE]脏标记
     */
    @JsonIgnore
    public boolean getBank_statements_sourceDirtyFlag(){
        return bank_statements_sourceDirtyFlag ;
    }

    /**
     * 获取 [AT_LEAST_ONE_INBOUND]
     */
    @JsonProperty("at_least_one_inbound")
    public String getAt_least_one_inbound(){
        return at_least_one_inbound ;
    }

    /**
     * 设置 [AT_LEAST_ONE_INBOUND]
     */
    @JsonProperty("at_least_one_inbound")
    public void setAt_least_one_inbound(String  at_least_one_inbound){
        this.at_least_one_inbound = at_least_one_inbound ;
        this.at_least_one_inboundDirtyFlag = true ;
    }

    /**
     * 获取 [AT_LEAST_ONE_INBOUND]脏标记
     */
    @JsonIgnore
    public boolean getAt_least_one_inboundDirtyFlag(){
        return at_least_one_inboundDirtyFlag ;
    }

    /**
     * 获取 [POST_AT_BANK_REC]
     */
    @JsonProperty("post_at_bank_rec")
    public String getPost_at_bank_rec(){
        return post_at_bank_rec ;
    }

    /**
     * 设置 [POST_AT_BANK_REC]
     */
    @JsonProperty("post_at_bank_rec")
    public void setPost_at_bank_rec(String  post_at_bank_rec){
        this.post_at_bank_rec = post_at_bank_rec ;
        this.post_at_bank_recDirtyFlag = true ;
    }

    /**
     * 获取 [POST_AT_BANK_REC]脏标记
     */
    @JsonIgnore
    public boolean getPost_at_bank_recDirtyFlag(){
        return post_at_bank_recDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE_NUMBER_NEXT]
     */
    @JsonProperty("sequence_number_next")
    public Integer getSequence_number_next(){
        return sequence_number_next ;
    }

    /**
     * 设置 [SEQUENCE_NUMBER_NEXT]
     */
    @JsonProperty("sequence_number_next")
    public void setSequence_number_next(Integer  sequence_number_next){
        this.sequence_number_next = sequence_number_next ;
        this.sequence_number_nextDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE_NUMBER_NEXT]脏标记
     */
    @JsonIgnore
    public boolean getSequence_number_nextDirtyFlag(){
        return sequence_number_nextDirtyFlag ;
    }

    /**
     * 获取 [BELONGS_TO_COMPANY]
     */
    @JsonProperty("belongs_to_company")
    public String getBelongs_to_company(){
        return belongs_to_company ;
    }

    /**
     * 设置 [BELONGS_TO_COMPANY]
     */
    @JsonProperty("belongs_to_company")
    public void setBelongs_to_company(String  belongs_to_company){
        this.belongs_to_company = belongs_to_company ;
        this.belongs_to_companyDirtyFlag = true ;
    }

    /**
     * 获取 [BELONGS_TO_COMPANY]脏标记
     */
    @JsonIgnore
    public boolean getBelongs_to_companyDirtyFlag(){
        return belongs_to_companyDirtyFlag ;
    }

    /**
     * 获取 [CODE]
     */
    @JsonProperty("code")
    public String getCode(){
        return code ;
    }

    /**
     * 设置 [CODE]
     */
    @JsonProperty("code")
    public void setCode(String  code){
        this.code = code ;
        this.codeDirtyFlag = true ;
    }

    /**
     * 获取 [CODE]脏标记
     */
    @JsonIgnore
    public boolean getCodeDirtyFlag(){
        return codeDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [INBOUND_PAYMENT_METHOD_IDS]
     */
    @JsonProperty("inbound_payment_method_ids")
    public String getInbound_payment_method_ids(){
        return inbound_payment_method_ids ;
    }

    /**
     * 设置 [INBOUND_PAYMENT_METHOD_IDS]
     */
    @JsonProperty("inbound_payment_method_ids")
    public void setInbound_payment_method_ids(String  inbound_payment_method_ids){
        this.inbound_payment_method_ids = inbound_payment_method_ids ;
        this.inbound_payment_method_idsDirtyFlag = true ;
    }

    /**
     * 获取 [INBOUND_PAYMENT_METHOD_IDS]脏标记
     */
    @JsonIgnore
    public boolean getInbound_payment_method_idsDirtyFlag(){
        return inbound_payment_method_idsDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [GROUP_INVOICE_LINES]
     */
    @JsonProperty("group_invoice_lines")
    public String getGroup_invoice_lines(){
        return group_invoice_lines ;
    }

    /**
     * 设置 [GROUP_INVOICE_LINES]
     */
    @JsonProperty("group_invoice_lines")
    public void setGroup_invoice_lines(String  group_invoice_lines){
        this.group_invoice_lines = group_invoice_lines ;
        this.group_invoice_linesDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_INVOICE_LINES]脏标记
     */
    @JsonIgnore
    public boolean getGroup_invoice_linesDirtyFlag(){
        return group_invoice_linesDirtyFlag ;
    }

    /**
     * 获取 [REFUND_SEQUENCE_ID]
     */
    @JsonProperty("refund_sequence_id")
    public Integer getRefund_sequence_id(){
        return refund_sequence_id ;
    }

    /**
     * 设置 [REFUND_SEQUENCE_ID]
     */
    @JsonProperty("refund_sequence_id")
    public void setRefund_sequence_id(Integer  refund_sequence_id){
        this.refund_sequence_id = refund_sequence_id ;
        this.refund_sequence_idDirtyFlag = true ;
    }

    /**
     * 获取 [REFUND_SEQUENCE_ID]脏标记
     */
    @JsonIgnore
    public boolean getRefund_sequence_idDirtyFlag(){
        return refund_sequence_idDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_DOMAIN]
     */
    @JsonProperty("alias_domain")
    public String getAlias_domain(){
        return alias_domain ;
    }

    /**
     * 设置 [ALIAS_DOMAIN]
     */
    @JsonProperty("alias_domain")
    public void setAlias_domain(String  alias_domain){
        this.alias_domain = alias_domain ;
        this.alias_domainDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_DOMAIN]脏标记
     */
    @JsonIgnore
    public boolean getAlias_domainDirtyFlag(){
        return alias_domainDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_CONTROL_IDS]
     */
    @JsonProperty("account_control_ids")
    public String getAccount_control_ids(){
        return account_control_ids ;
    }

    /**
     * 设置 [ACCOUNT_CONTROL_IDS]
     */
    @JsonProperty("account_control_ids")
    public void setAccount_control_ids(String  account_control_ids){
        this.account_control_ids = account_control_ids ;
        this.account_control_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_CONTROL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAccount_control_idsDirtyFlag(){
        return account_control_idsDirtyFlag ;
    }

    /**
     * 获取 [UPDATE_POSTED]
     */
    @JsonProperty("update_posted")
    public String getUpdate_posted(){
        return update_posted ;
    }

    /**
     * 设置 [UPDATE_POSTED]
     */
    @JsonProperty("update_posted")
    public void setUpdate_posted(String  update_posted){
        this.update_posted = update_posted ;
        this.update_postedDirtyFlag = true ;
    }

    /**
     * 获取 [UPDATE_POSTED]脏标记
     */
    @JsonIgnore
    public boolean getUpdate_postedDirtyFlag(){
        return update_postedDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT_AUTHORIZED_DIFF]
     */
    @JsonProperty("amount_authorized_diff")
    public Double getAmount_authorized_diff(){
        return amount_authorized_diff ;
    }

    /**
     * 设置 [AMOUNT_AUTHORIZED_DIFF]
     */
    @JsonProperty("amount_authorized_diff")
    public void setAmount_authorized_diff(Double  amount_authorized_diff){
        this.amount_authorized_diff = amount_authorized_diff ;
        this.amount_authorized_diffDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT_AUTHORIZED_DIFF]脏标记
     */
    @JsonIgnore
    public boolean getAmount_authorized_diffDirtyFlag(){
        return amount_authorized_diffDirtyFlag ;
    }

    /**
     * 获取 [KANBAN_DASHBOARD_GRAPH]
     */
    @JsonProperty("kanban_dashboard_graph")
    public String getKanban_dashboard_graph(){
        return kanban_dashboard_graph ;
    }

    /**
     * 设置 [KANBAN_DASHBOARD_GRAPH]
     */
    @JsonProperty("kanban_dashboard_graph")
    public void setKanban_dashboard_graph(String  kanban_dashboard_graph){
        this.kanban_dashboard_graph = kanban_dashboard_graph ;
        this.kanban_dashboard_graphDirtyFlag = true ;
    }

    /**
     * 获取 [KANBAN_DASHBOARD_GRAPH]脏标记
     */
    @JsonIgnore
    public boolean getKanban_dashboard_graphDirtyFlag(){
        return kanban_dashboard_graphDirtyFlag ;
    }

    /**
     * 获取 [OUTBOUND_PAYMENT_METHOD_IDS]
     */
    @JsonProperty("outbound_payment_method_ids")
    public String getOutbound_payment_method_ids(){
        return outbound_payment_method_ids ;
    }

    /**
     * 设置 [OUTBOUND_PAYMENT_METHOD_IDS]
     */
    @JsonProperty("outbound_payment_method_ids")
    public void setOutbound_payment_method_ids(String  outbound_payment_method_ids){
        this.outbound_payment_method_ids = outbound_payment_method_ids ;
        this.outbound_payment_method_idsDirtyFlag = true ;
    }

    /**
     * 获取 [OUTBOUND_PAYMENT_METHOD_IDS]脏标记
     */
    @JsonIgnore
    public boolean getOutbound_payment_method_idsDirtyFlag(){
        return outbound_payment_method_idsDirtyFlag ;
    }

    /**
     * 获取 [TYPE]
     */
    @JsonProperty("type")
    public String getType(){
        return type ;
    }

    /**
     * 设置 [TYPE]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

    /**
     * 获取 [TYPE]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return typeDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [KANBAN_DASHBOARD]
     */
    @JsonProperty("kanban_dashboard")
    public String getKanban_dashboard(){
        return kanban_dashboard ;
    }

    /**
     * 设置 [KANBAN_DASHBOARD]
     */
    @JsonProperty("kanban_dashboard")
    public void setKanban_dashboard(String  kanban_dashboard){
        this.kanban_dashboard = kanban_dashboard ;
        this.kanban_dashboardDirtyFlag = true ;
    }

    /**
     * 获取 [KANBAN_DASHBOARD]脏标记
     */
    @JsonIgnore
    public boolean getKanban_dashboardDirtyFlag(){
        return kanban_dashboardDirtyFlag ;
    }

    /**
     * 获取 [REFUND_SEQUENCE]
     */
    @JsonProperty("refund_sequence")
    public String getRefund_sequence(){
        return refund_sequence ;
    }

    /**
     * 设置 [REFUND_SEQUENCE]
     */
    @JsonProperty("refund_sequence")
    public void setRefund_sequence(String  refund_sequence){
        this.refund_sequence = refund_sequence ;
        this.refund_sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [REFUND_SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getRefund_sequenceDirtyFlag(){
        return refund_sequenceDirtyFlag ;
    }

    /**
     * 获取 [COLOR]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return color ;
    }

    /**
     * 设置 [COLOR]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [COLOR]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return colorDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PROFIT_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("profit_account_id_text")
    public String getProfit_account_id_text(){
        return profit_account_id_text ;
    }

    /**
     * 设置 [PROFIT_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("profit_account_id_text")
    public void setProfit_account_id_text(String  profit_account_id_text){
        this.profit_account_id_text = profit_account_id_text ;
        this.profit_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PROFIT_ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProfit_account_id_textDirtyFlag(){
        return profit_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_NAME]
     */
    @JsonProperty("alias_name")
    public String getAlias_name(){
        return alias_name ;
    }

    /**
     * 设置 [ALIAS_NAME]
     */
    @JsonProperty("alias_name")
    public void setAlias_name(String  alias_name){
        this.alias_name = alias_name ;
        this.alias_nameDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_NAME]脏标记
     */
    @JsonIgnore
    public boolean getAlias_nameDirtyFlag(){
        return alias_nameDirtyFlag ;
    }

    /**
     * 获取 [BANK_ACC_NUMBER]
     */
    @JsonProperty("bank_acc_number")
    public String getBank_acc_number(){
        return bank_acc_number ;
    }

    /**
     * 设置 [BANK_ACC_NUMBER]
     */
    @JsonProperty("bank_acc_number")
    public void setBank_acc_number(String  bank_acc_number){
        this.bank_acc_number = bank_acc_number ;
        this.bank_acc_numberDirtyFlag = true ;
    }

    /**
     * 获取 [BANK_ACC_NUMBER]脏标记
     */
    @JsonIgnore
    public boolean getBank_acc_numberDirtyFlag(){
        return bank_acc_numberDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [BANK_ID]
     */
    @JsonProperty("bank_id")
    public Integer getBank_id(){
        return bank_id ;
    }

    /**
     * 设置 [BANK_ID]
     */
    @JsonProperty("bank_id")
    public void setBank_id(Integer  bank_id){
        this.bank_id = bank_id ;
        this.bank_idDirtyFlag = true ;
    }

    /**
     * 获取 [BANK_ID]脏标记
     */
    @JsonIgnore
    public boolean getBank_idDirtyFlag(){
        return bank_idDirtyFlag ;
    }

    /**
     * 获取 [LOSS_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("loss_account_id_text")
    public String getLoss_account_id_text(){
        return loss_account_id_text ;
    }

    /**
     * 设置 [LOSS_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("loss_account_id_text")
    public void setLoss_account_id_text(String  loss_account_id_text){
        this.loss_account_id_text = loss_account_id_text ;
        this.loss_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOSS_ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLoss_account_id_textDirtyFlag(){
        return loss_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [DEFAULT_CREDIT_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("default_credit_account_id_text")
    public String getDefault_credit_account_id_text(){
        return default_credit_account_id_text ;
    }

    /**
     * 设置 [DEFAULT_CREDIT_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("default_credit_account_id_text")
    public void setDefault_credit_account_id_text(String  default_credit_account_id_text){
        this.default_credit_account_id_text = default_credit_account_id_text ;
        this.default_credit_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [DEFAULT_CREDIT_ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getDefault_credit_account_id_textDirtyFlag(){
        return default_credit_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_PARTNER_ID]
     */
    @JsonProperty("company_partner_id")
    public Integer getCompany_partner_id(){
        return company_partner_id ;
    }

    /**
     * 设置 [COMPANY_PARTNER_ID]
     */
    @JsonProperty("company_partner_id")
    public void setCompany_partner_id(Integer  company_partner_id){
        this.company_partner_id = company_partner_id ;
        this.company_partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_partner_idDirtyFlag(){
        return company_partner_idDirtyFlag ;
    }

    /**
     * 获取 [DEFAULT_DEBIT_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("default_debit_account_id_text")
    public String getDefault_debit_account_id_text(){
        return default_debit_account_id_text ;
    }

    /**
     * 设置 [DEFAULT_DEBIT_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("default_debit_account_id_text")
    public void setDefault_debit_account_id_text(String  default_debit_account_id_text){
        this.default_debit_account_id_text = default_debit_account_id_text ;
        this.default_debit_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [DEFAULT_DEBIT_ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getDefault_debit_account_id_textDirtyFlag(){
        return default_debit_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return currency_id_text ;
    }

    /**
     * 设置 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [BANK_ACCOUNT_ID]
     */
    @JsonProperty("bank_account_id")
    public Integer getBank_account_id(){
        return bank_account_id ;
    }

    /**
     * 设置 [BANK_ACCOUNT_ID]
     */
    @JsonProperty("bank_account_id")
    public void setBank_account_id(Integer  bank_account_id){
        this.bank_account_id = bank_account_id ;
        this.bank_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [BANK_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getBank_account_idDirtyFlag(){
        return bank_account_idDirtyFlag ;
    }

    /**
     * 获取 [LOSS_ACCOUNT_ID]
     */
    @JsonProperty("loss_account_id")
    public Integer getLoss_account_id(){
        return loss_account_id ;
    }

    /**
     * 设置 [LOSS_ACCOUNT_ID]
     */
    @JsonProperty("loss_account_id")
    public void setLoss_account_id(Integer  loss_account_id){
        this.loss_account_id = loss_account_id ;
        this.loss_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOSS_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getLoss_account_idDirtyFlag(){
        return loss_account_idDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_ID]
     */
    @JsonProperty("alias_id")
    public Integer getAlias_id(){
        return alias_id ;
    }

    /**
     * 设置 [ALIAS_ID]
     */
    @JsonProperty("alias_id")
    public void setAlias_id(Integer  alias_id){
        this.alias_id = alias_id ;
        this.alias_idDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_idDirtyFlag(){
        return alias_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [DEFAULT_CREDIT_ACCOUNT_ID]
     */
    @JsonProperty("default_credit_account_id")
    public Integer getDefault_credit_account_id(){
        return default_credit_account_id ;
    }

    /**
     * 设置 [DEFAULT_CREDIT_ACCOUNT_ID]
     */
    @JsonProperty("default_credit_account_id")
    public void setDefault_credit_account_id(Integer  default_credit_account_id){
        this.default_credit_account_id = default_credit_account_id ;
        this.default_credit_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [DEFAULT_CREDIT_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getDefault_credit_account_idDirtyFlag(){
        return default_credit_account_idDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [DEFAULT_DEBIT_ACCOUNT_ID]
     */
    @JsonProperty("default_debit_account_id")
    public Integer getDefault_debit_account_id(){
        return default_debit_account_id ;
    }

    /**
     * 设置 [DEFAULT_DEBIT_ACCOUNT_ID]
     */
    @JsonProperty("default_debit_account_id")
    public void setDefault_debit_account_id(Integer  default_debit_account_id){
        this.default_debit_account_id = default_debit_account_id ;
        this.default_debit_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [DEFAULT_DEBIT_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getDefault_debit_account_idDirtyFlag(){
        return default_debit_account_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [PROFIT_ACCOUNT_ID]
     */
    @JsonProperty("profit_account_id")
    public Integer getProfit_account_id(){
        return profit_account_id ;
    }

    /**
     * 设置 [PROFIT_ACCOUNT_ID]
     */
    @JsonProperty("profit_account_id")
    public void setProfit_account_id(Integer  profit_account_id){
        this.profit_account_id = profit_account_id ;
        this.profit_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROFIT_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProfit_account_idDirtyFlag(){
        return profit_account_idDirtyFlag ;
    }



    public Account_journal toDO() {
        Account_journal srfdomain = new Account_journal();
        if(getRefund_sequence_number_nextDirtyFlag())
            srfdomain.setRefund_sequence_number_next(refund_sequence_number_next);
        if(getType_control_idsDirtyFlag())
            srfdomain.setType_control_ids(type_control_ids);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getAt_least_one_outboundDirtyFlag())
            srfdomain.setAt_least_one_outbound(at_least_one_outbound);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getSequence_idDirtyFlag())
            srfdomain.setSequence_id(sequence_id);
        if(getShow_on_dashboardDirtyFlag())
            srfdomain.setShow_on_dashboard(show_on_dashboard);
        if(getJournal_userDirtyFlag())
            srfdomain.setJournal_user(journal_user);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getBank_statements_sourceDirtyFlag())
            srfdomain.setBank_statements_source(bank_statements_source);
        if(getAt_least_one_inboundDirtyFlag())
            srfdomain.setAt_least_one_inbound(at_least_one_inbound);
        if(getPost_at_bank_recDirtyFlag())
            srfdomain.setPost_at_bank_rec(post_at_bank_rec);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getSequence_number_nextDirtyFlag())
            srfdomain.setSequence_number_next(sequence_number_next);
        if(getBelongs_to_companyDirtyFlag())
            srfdomain.setBelongs_to_company(belongs_to_company);
        if(getCodeDirtyFlag())
            srfdomain.setCode(code);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getInbound_payment_method_idsDirtyFlag())
            srfdomain.setInbound_payment_method_ids(inbound_payment_method_ids);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getGroup_invoice_linesDirtyFlag())
            srfdomain.setGroup_invoice_lines(group_invoice_lines);
        if(getRefund_sequence_idDirtyFlag())
            srfdomain.setRefund_sequence_id(refund_sequence_id);
        if(getAlias_domainDirtyFlag())
            srfdomain.setAlias_domain(alias_domain);
        if(getAccount_control_idsDirtyFlag())
            srfdomain.setAccount_control_ids(account_control_ids);
        if(getUpdate_postedDirtyFlag())
            srfdomain.setUpdate_posted(update_posted);
        if(getAmount_authorized_diffDirtyFlag())
            srfdomain.setAmount_authorized_diff(amount_authorized_diff);
        if(getKanban_dashboard_graphDirtyFlag())
            srfdomain.setKanban_dashboard_graph(kanban_dashboard_graph);
        if(getOutbound_payment_method_idsDirtyFlag())
            srfdomain.setOutbound_payment_method_ids(outbound_payment_method_ids);
        if(getTypeDirtyFlag())
            srfdomain.setType(type);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getKanban_dashboardDirtyFlag())
            srfdomain.setKanban_dashboard(kanban_dashboard);
        if(getRefund_sequenceDirtyFlag())
            srfdomain.setRefund_sequence(refund_sequence);
        if(getColorDirtyFlag())
            srfdomain.setColor(color);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getProfit_account_id_textDirtyFlag())
            srfdomain.setProfit_account_id_text(profit_account_id_text);
        if(getAlias_nameDirtyFlag())
            srfdomain.setAlias_name(alias_name);
        if(getBank_acc_numberDirtyFlag())
            srfdomain.setBank_acc_number(bank_acc_number);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getBank_idDirtyFlag())
            srfdomain.setBank_id(bank_id);
        if(getLoss_account_id_textDirtyFlag())
            srfdomain.setLoss_account_id_text(loss_account_id_text);
        if(getDefault_credit_account_id_textDirtyFlag())
            srfdomain.setDefault_credit_account_id_text(default_credit_account_id_text);
        if(getCompany_partner_idDirtyFlag())
            srfdomain.setCompany_partner_id(company_partner_id);
        if(getDefault_debit_account_id_textDirtyFlag())
            srfdomain.setDefault_debit_account_id_text(default_debit_account_id_text);
        if(getCurrency_id_textDirtyFlag())
            srfdomain.setCurrency_id_text(currency_id_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getBank_account_idDirtyFlag())
            srfdomain.setBank_account_id(bank_account_id);
        if(getLoss_account_idDirtyFlag())
            srfdomain.setLoss_account_id(loss_account_id);
        if(getAlias_idDirtyFlag())
            srfdomain.setAlias_id(alias_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getDefault_credit_account_idDirtyFlag())
            srfdomain.setDefault_credit_account_id(default_credit_account_id);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getDefault_debit_account_idDirtyFlag())
            srfdomain.setDefault_debit_account_id(default_debit_account_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getProfit_account_idDirtyFlag())
            srfdomain.setProfit_account_id(profit_account_id);

        return srfdomain;
    }

    public void fromDO(Account_journal srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getRefund_sequence_number_nextDirtyFlag())
            this.setRefund_sequence_number_next(srfdomain.getRefund_sequence_number_next());
        if(srfdomain.getType_control_idsDirtyFlag())
            this.setType_control_ids(srfdomain.getType_control_ids());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getAt_least_one_outboundDirtyFlag())
            this.setAt_least_one_outbound(srfdomain.getAt_least_one_outbound());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getSequence_idDirtyFlag())
            this.setSequence_id(srfdomain.getSequence_id());
        if(srfdomain.getShow_on_dashboardDirtyFlag())
            this.setShow_on_dashboard(srfdomain.getShow_on_dashboard());
        if(srfdomain.getJournal_userDirtyFlag())
            this.setJournal_user(srfdomain.getJournal_user());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getBank_statements_sourceDirtyFlag())
            this.setBank_statements_source(srfdomain.getBank_statements_source());
        if(srfdomain.getAt_least_one_inboundDirtyFlag())
            this.setAt_least_one_inbound(srfdomain.getAt_least_one_inbound());
        if(srfdomain.getPost_at_bank_recDirtyFlag())
            this.setPost_at_bank_rec(srfdomain.getPost_at_bank_rec());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getSequence_number_nextDirtyFlag())
            this.setSequence_number_next(srfdomain.getSequence_number_next());
        if(srfdomain.getBelongs_to_companyDirtyFlag())
            this.setBelongs_to_company(srfdomain.getBelongs_to_company());
        if(srfdomain.getCodeDirtyFlag())
            this.setCode(srfdomain.getCode());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getInbound_payment_method_idsDirtyFlag())
            this.setInbound_payment_method_ids(srfdomain.getInbound_payment_method_ids());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getGroup_invoice_linesDirtyFlag())
            this.setGroup_invoice_lines(srfdomain.getGroup_invoice_lines());
        if(srfdomain.getRefund_sequence_idDirtyFlag())
            this.setRefund_sequence_id(srfdomain.getRefund_sequence_id());
        if(srfdomain.getAlias_domainDirtyFlag())
            this.setAlias_domain(srfdomain.getAlias_domain());
        if(srfdomain.getAccount_control_idsDirtyFlag())
            this.setAccount_control_ids(srfdomain.getAccount_control_ids());
        if(srfdomain.getUpdate_postedDirtyFlag())
            this.setUpdate_posted(srfdomain.getUpdate_posted());
        if(srfdomain.getAmount_authorized_diffDirtyFlag())
            this.setAmount_authorized_diff(srfdomain.getAmount_authorized_diff());
        if(srfdomain.getKanban_dashboard_graphDirtyFlag())
            this.setKanban_dashboard_graph(srfdomain.getKanban_dashboard_graph());
        if(srfdomain.getOutbound_payment_method_idsDirtyFlag())
            this.setOutbound_payment_method_ids(srfdomain.getOutbound_payment_method_ids());
        if(srfdomain.getTypeDirtyFlag())
            this.setType(srfdomain.getType());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getKanban_dashboardDirtyFlag())
            this.setKanban_dashboard(srfdomain.getKanban_dashboard());
        if(srfdomain.getRefund_sequenceDirtyFlag())
            this.setRefund_sequence(srfdomain.getRefund_sequence());
        if(srfdomain.getColorDirtyFlag())
            this.setColor(srfdomain.getColor());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getProfit_account_id_textDirtyFlag())
            this.setProfit_account_id_text(srfdomain.getProfit_account_id_text());
        if(srfdomain.getAlias_nameDirtyFlag())
            this.setAlias_name(srfdomain.getAlias_name());
        if(srfdomain.getBank_acc_numberDirtyFlag())
            this.setBank_acc_number(srfdomain.getBank_acc_number());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getBank_idDirtyFlag())
            this.setBank_id(srfdomain.getBank_id());
        if(srfdomain.getLoss_account_id_textDirtyFlag())
            this.setLoss_account_id_text(srfdomain.getLoss_account_id_text());
        if(srfdomain.getDefault_credit_account_id_textDirtyFlag())
            this.setDefault_credit_account_id_text(srfdomain.getDefault_credit_account_id_text());
        if(srfdomain.getCompany_partner_idDirtyFlag())
            this.setCompany_partner_id(srfdomain.getCompany_partner_id());
        if(srfdomain.getDefault_debit_account_id_textDirtyFlag())
            this.setDefault_debit_account_id_text(srfdomain.getDefault_debit_account_id_text());
        if(srfdomain.getCurrency_id_textDirtyFlag())
            this.setCurrency_id_text(srfdomain.getCurrency_id_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getBank_account_idDirtyFlag())
            this.setBank_account_id(srfdomain.getBank_account_id());
        if(srfdomain.getLoss_account_idDirtyFlag())
            this.setLoss_account_id(srfdomain.getLoss_account_id());
        if(srfdomain.getAlias_idDirtyFlag())
            this.setAlias_id(srfdomain.getAlias_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getDefault_credit_account_idDirtyFlag())
            this.setDefault_credit_account_id(srfdomain.getDefault_credit_account_id());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getDefault_debit_account_idDirtyFlag())
            this.setDefault_debit_account_id(srfdomain.getDefault_debit_account_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getProfit_account_idDirtyFlag())
            this.setProfit_account_id(srfdomain.getProfit_account_id());

    }

    public List<Account_journalDTO> fromDOPage(List<Account_journal> poPage)   {
        if(poPage == null)
            return null;
        List<Account_journalDTO> dtos=new ArrayList<Account_journalDTO>();
        for(Account_journal domain : poPage) {
            Account_journalDTO dto = new Account_journalDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

