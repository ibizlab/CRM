package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.web.dto.DynaDashboardDTO;
import cn.ibizlab.odoo.core.r7rt_dyna.domain.DynaDashboard;
import cn.ibizlab.odoo.core.r7rt_dyna.service.IDynaDashboardService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.r7rt_dyna.filter.DynaDashboardSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"DynaDashboard" })
@RestController
@RequestMapping("")
public class DynaDashboardResource {

    @Autowired
    private IDynaDashboardService dynadashboardService;

    public IDynaDashboardService getDynadashboardService() {
        return this.dynadashboardService;
    }

    @ApiOperation(value = "Save", tags = {"DynaDashboard" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/web/dynadashboards/{dynadashboard_id}/save")
    public ResponseEntity<Boolean> save(@RequestBody DynaDashboardDTO dynadashboarddto) {
        DynaDashboard dynadashboard = dynadashboarddto.toDO();
    	Boolean b = dynadashboardService.save(dynadashboard) ;
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    @ApiOperation(value = "Update", tags = {"DynaDashboard" },  notes = "Update")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/dynadashboards/{dynadashboard_id}")

    public ResponseEntity<DynaDashboardDTO> update(@PathVariable("dynadashboard_id") String dynadashboard_id, @RequestBody DynaDashboardDTO dynadashboarddto) {
		DynaDashboard domain = dynadashboarddto.toDO();
        domain.setDynaDashboardId(dynadashboard_id);
		dynadashboardService.update(domain);
		DynaDashboardDTO dto = new DynaDashboardDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "Get", tags = {"DynaDashboard" },  notes = "Get")
	@RequestMapping(method = RequestMethod.GET, value = "/web/dynadashboards/{dynadashboard_id}")
    public ResponseEntity<DynaDashboardDTO> get(@PathVariable("dynadashboard_id") String dynadashboard_id) {
        DynaDashboardDTO dto = new DynaDashboardDTO();
        DynaDashboard domain = dynadashboardService.get(dynadashboard_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "Remove", tags = {"DynaDashboard" },  notes = "Remove")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/dynadashboards/{dynadashboard_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("dynadashboard_id") String dynadashboard_id) {
        DynaDashboardDTO dynadashboarddto = new DynaDashboardDTO();
		DynaDashboard domain = new DynaDashboard();
		dynadashboarddto.setDynaDashboardId(dynadashboard_id);
		domain.setDynaDashboardId(dynadashboard_id);
        Boolean rst = dynadashboardService.remove(domain.getDynaDashboardId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "CheckKey", tags = {"DynaDashboard" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/web/dynadashboards/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody DynaDashboardDTO dynadashboarddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "GetDraft", tags = {"DynaDashboard" },  notes = "GetDraft")
	@RequestMapping(method = RequestMethod.GET, value = "/web/dynadashboards/{dynadashboard_id}/getdraft")

    public ResponseEntity<DynaDashboardDTO> getDraft(@PathVariable("dynadashboard_id") String dynadashboard_id, @RequestBody DynaDashboardDTO dynadashboarddto) {
        DynaDashboard dynadashboard = dynadashboarddto.toDO();
    	dynadashboard = dynadashboardService.getDraft(dynadashboard) ;
    	dynadashboarddto.fromDO(dynadashboard);
        return ResponseEntity.status(HttpStatus.OK).body(dynadashboarddto);
    }

    @ApiOperation(value = "Create", tags = {"DynaDashboard" },  notes = "Create")
	@RequestMapping(method = RequestMethod.POST, value = "/web/dynadashboards")

    public ResponseEntity<DynaDashboardDTO> create(@RequestBody DynaDashboardDTO dynadashboarddto) {
        DynaDashboardDTO dto = new DynaDashboardDTO();
        DynaDashboard domain = dynadashboarddto.toDO();
		dynadashboardService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取DEFAULT", tags = {"DynaDashboard" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/web/dynadashboards/fetchdefault")
	public ResponseEntity<Page<DynaDashboardDTO>> fetchDefault(DynaDashboardSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<DynaDashboardDTO> list = new ArrayList<DynaDashboardDTO>();
        
        Page<DynaDashboard> domains = dynadashboardService.searchDefault(context) ;
        for(DynaDashboard dynadashboard : domains.getContent()){
            DynaDashboardDTO dto = new DynaDashboardDTO();
            dto.fromDO(dynadashboard);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
