package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.web.dto.Account_invoiceDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoiceService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoiceSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_invoice" })
@RestController
@RequestMapping("")
public class Account_invoiceResource {

    @Autowired
    private IAccount_invoiceService account_invoiceService;

    public IAccount_invoiceService getAccount_invoiceService() {
        return this.account_invoiceService;
    }

    @ApiOperation(value = "删除数据", tags = {"Account_invoice" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/account_invoices/{account_invoice_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_invoice_id") Integer account_invoice_id) {
        Account_invoiceDTO account_invoicedto = new Account_invoiceDTO();
		Account_invoice domain = new Account_invoice();
		account_invoicedto.setId(account_invoice_id);
		domain.setId(account_invoice_id);
        Boolean rst = account_invoiceService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "CheckKey", tags = {"Account_invoice" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/web/account_invoices/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_invoiceDTO account_invoicedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_invoice" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/account_invoices")

    public ResponseEntity<Account_invoiceDTO> create(@RequestBody Account_invoiceDTO account_invoicedto) {
        Account_invoiceDTO dto = new Account_invoiceDTO();
        Account_invoice domain = account_invoicedto.toDO();
		account_invoiceService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_invoice" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/account_invoices/{account_invoice_id}")

    public ResponseEntity<Account_invoiceDTO> update(@PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoiceDTO account_invoicedto) {
		Account_invoice domain = account_invoicedto.toDO();
        domain.setId(account_invoice_id);
		account_invoiceService.update(domain);
		Account_invoiceDTO dto = new Account_invoiceDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_invoice" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/account_invoices/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_invoiceDTO> account_invoicedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_invoice" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/account_invoices/{account_invoice_id}")
    public ResponseEntity<Account_invoiceDTO> get(@PathVariable("account_invoice_id") Integer account_invoice_id) {
        Account_invoiceDTO dto = new Account_invoiceDTO();
        Account_invoice domain = account_invoiceService.get(account_invoice_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_invoice" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/account_invoices/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_invoice(@RequestBody List<Account_invoiceDTO> account_invoicedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Account_invoice" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/account_invoices/{account_invoice_id}/getdraft")

    public ResponseEntity<Account_invoiceDTO> getDraft(@PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoiceDTO account_invoicedto) {
        Account_invoice account_invoice = account_invoicedto.toDO();
    	account_invoice = account_invoiceService.getDraft(account_invoice) ;
    	account_invoicedto.fromDO(account_invoice);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoicedto);
    }

    @ApiOperation(value = "Save", tags = {"Account_invoice" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/web/account_invoices/{account_invoice_id}/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_invoiceDTO account_invoicedto) {
        Account_invoice account_invoice = account_invoicedto.toDO();
    	Boolean b = account_invoiceService.save(account_invoice) ;
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_invoice" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/account_invoices/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_invoiceDTO> account_invoicedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_invoice" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/account_invoices/fetchdefault")
	public ResponseEntity<Page<Account_invoiceDTO>> fetchDefault(Account_invoiceSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_invoiceDTO> list = new ArrayList<Account_invoiceDTO>();
        
        Page<Account_invoice> domains = account_invoiceService.searchDefault(context) ;
        for(Account_invoice account_invoice : domains.getContent()){
            Account_invoiceDTO dto = new Account_invoiceDTO();
            dto.fromDO(account_invoice);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}



    @ApiOperation(value = "删除数据", tags = {"Account_invoice" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_partners/{res_partner_id}/account_invoices/{account_invoice_id}")

    public ResponseEntity<Boolean> removeByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_invoice_id") Integer account_invoice_id) {
        Account_invoiceDTO account_invoicedto = new Account_invoiceDTO();
		Account_invoice domain = new Account_invoice();
		account_invoicedto.setId(account_invoice_id);
		domain.setId(account_invoice_id);
        Boolean rst = account_invoiceService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "CheckKey", tags = {"Account_invoice" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/account_invoices/checkkey")
    public ResponseEntity<Boolean> checkKeyByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Account_invoiceDTO account_invoicedto) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_invoice" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/account_invoices")

    public ResponseEntity<Account_invoiceDTO> createByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Account_invoiceDTO account_invoicedto) {
        Account_invoiceDTO dto = new Account_invoiceDTO();
        Account_invoice domain = account_invoicedto.toDO();
        domain.setPartner_id(res_partner_id);
		account_invoiceService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_invoice" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_partners/{res_partner_id}/account_invoices/{account_invoice_id}")

    public ResponseEntity<Account_invoiceDTO> updateByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoiceDTO account_invoicedto) {
		Account_invoice domain = account_invoicedto.toDO();
        domain.setPartner_id(res_partner_id);
        domain.setId(account_invoice_id);
		account_invoiceService.update(domain);
		Account_invoiceDTO dto = new Account_invoiceDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_invoice" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_partners/{res_partner_id}/account_invoices/updatebatch")
    public ResponseEntity<Boolean> updateBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody List<Account_invoiceDTO> account_invoicedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_invoice" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/res_partners/{res_partner_id}/account_invoices/{account_invoice_id}")
    public ResponseEntity<Account_invoiceDTO> getByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_invoice_id") Integer account_invoice_id) {
        Account_invoiceDTO dto = new Account_invoiceDTO();
        Account_invoice domain = account_invoiceService.get(account_invoice_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_invoice" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/account_invoices/createbatch")
    public ResponseEntity<Boolean> createBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody List<Account_invoiceDTO> account_invoicedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Account_invoice" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/res_partners/{res_partner_id}/account_invoices/{account_invoiceid}/getdraft")

    public ResponseEntity<Account_invoiceDTO> getDraftByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoiceDTO account_invoicedto) {
        Account_invoice account_invoice = account_invoicedto.toDO();
    	account_invoice = account_invoiceService.getDraft(account_invoice) ;
    	account_invoicedto.fromDO(account_invoice);
        return ResponseEntity.status(HttpStatus.OK).body(account_invoicedto);
    }

    @ApiOperation(value = "Save", tags = {"Account_invoice" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/web/res_partners/{res_partner_id}/account_invoices/save")
    public ResponseEntity<Boolean> saveByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Account_invoiceDTO account_invoicedto) {
        Account_invoice domain = account_invoicedto.toDO();
        domain.setPartner_id(res_partner_id);
    	Boolean b = account_invoiceService.save(domain) ;
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_invoice" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_partners/{res_partner_id}/account_invoices/removebatch")
    public ResponseEntity<Boolean> removeBatchByRes_partner(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody List<Account_invoiceDTO> account_invoicedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_invoice" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/res_partners/{res_partner_id}/account_invoices/fetchdefault")
	public ResponseEntity<Page<Account_invoiceDTO>> fetchAccount_invoiceDefault(@PathVariable("res_partner_id") Integer res_partner_id,Account_invoiceSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_invoiceDTO> list = new ArrayList<Account_invoiceDTO>();
        context.setN_partner_id_eq(res_partner_id);
        Page<Account_invoice> domains = account_invoiceService.searchDefault(context) ;
        for(Account_invoice account_invoice : domains.getContent()){
            Account_invoiceDTO dto = new Account_invoiceDTO();
            dto.fromDO(account_invoice);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
