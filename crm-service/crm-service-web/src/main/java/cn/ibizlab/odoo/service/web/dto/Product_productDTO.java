package cn.ibizlab.odoo.service.web.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_product.valuerule.anno.product_product.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_product;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Product_productDTO]
 */
public class Product_productDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [VARIANT_SELLER_IDS]
     *
     */
    @Product_productVariant_seller_idsDefault(info = "默认规则")
    private String variant_seller_ids;

    @JsonIgnore
    private boolean variant_seller_idsDirtyFlag;

    /**
     * 属性 [PRODUCT_TEMPLATE_ATTRIBUTE_VALUE_IDS]
     *
     */
    @Product_productProduct_template_attribute_value_idsDefault(info = "默认规则")
    private String product_template_attribute_value_ids;

    @JsonIgnore
    private boolean product_template_attribute_value_idsDirtyFlag;

    /**
     * 属性 [PRODUCT_VARIANT_IDS]
     *
     */
    @Product_productProduct_variant_idsDefault(info = "默认规则")
    private String product_variant_ids;

    @JsonIgnore
    private boolean product_variant_idsDirtyFlag;

    /**
     * 属性 [IMAGE_SMALL]
     *
     */
    @Product_productImage_smallDefault(info = "默认规则")
    private byte[] image_small;

    @JsonIgnore
    private boolean image_smallDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Product_productMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [VOLUME]
     *
     */
    @Product_productVolumeDefault(info = "默认规则")
    private Double volume;

    @JsonIgnore
    private boolean volumeDirtyFlag;

    /**
     * 属性 [LST_PRICE]
     *
     */
    @Product_productLst_priceDefault(info = "默认规则")
    private Double lst_price;

    @JsonIgnore
    private boolean lst_priceDirtyFlag;

    /**
     * 属性 [VALID_PRODUCT_ATTRIBUTE_IDS]
     *
     */
    @Product_productValid_product_attribute_idsDefault(info = "默认规则")
    private String valid_product_attribute_ids;

    @JsonIgnore
    private boolean valid_product_attribute_idsDirtyFlag;

    /**
     * 属性 [STOCK_FIFO_MANUAL_MOVE_IDS]
     *
     */
    @Product_productStock_fifo_manual_move_idsDefault(info = "默认规则")
    private String stock_fifo_manual_move_ids;

    @JsonIgnore
    private boolean stock_fifo_manual_move_idsDirtyFlag;

    /**
     * 属性 [STOCK_QUANT_IDS]
     *
     */
    @Product_productStock_quant_idsDefault(info = "默认规则")
    private String stock_quant_ids;

    @JsonIgnore
    private boolean stock_quant_idsDirtyFlag;

    /**
     * 属性 [SUPPLIER_TAXES_ID]
     *
     */
    @Product_productSupplier_taxes_idDefault(info = "默认规则")
    private String supplier_taxes_id;

    @JsonIgnore
    private boolean supplier_taxes_idDirtyFlag;

    /**
     * 属性 [PRICELIST_ITEM_IDS]
     *
     */
    @Product_productPricelist_item_idsDefault(info = "默认规则")
    private String pricelist_item_ids;

    @JsonIgnore
    private boolean pricelist_item_idsDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Product_product__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [ACCESSORY_PRODUCT_IDS]
     *
     */
    @Product_productAccessory_product_idsDefault(info = "默认规则")
    private String accessory_product_ids;

    @JsonIgnore
    private boolean accessory_product_idsDirtyFlag;

    /**
     * 属性 [SELLER_IDS]
     *
     */
    @Product_productSeller_idsDefault(info = "默认规则")
    private String seller_ids;

    @JsonIgnore
    private boolean seller_idsDirtyFlag;

    /**
     * 属性 [VALID_PRODUCT_ATTRIBUTE_VALUE_WNVA_IDS]
     *
     */
    @Product_productValid_product_attribute_value_wnva_idsDefault(info = "默认规则")
    private String valid_product_attribute_value_wnva_ids;

    @JsonIgnore
    private boolean valid_product_attribute_value_wnva_idsDirtyFlag;

    /**
     * 属性 [PARTNER_REF]
     *
     */
    @Product_productPartner_refDefault(info = "默认规则")
    private String partner_ref;

    @JsonIgnore
    private boolean partner_refDirtyFlag;

    /**
     * 属性 [PRODUCT_IMAGE_IDS]
     *
     */
    @Product_productProduct_image_idsDefault(info = "默认规则")
    private String product_image_ids;

    @JsonIgnore
    private boolean product_image_idsDirtyFlag;

    /**
     * 属性 [MRP_PRODUCT_QTY]
     *
     */
    @Product_productMrp_product_qtyDefault(info = "默认规则")
    private Double mrp_product_qty;

    @JsonIgnore
    private boolean mrp_product_qtyDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Product_productWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [VALID_PRODUCT_TEMPLATE_ATTRIBUTE_LINE_IDS]
     *
     */
    @Product_productValid_product_template_attribute_line_idsDefault(info = "默认规则")
    private String valid_product_template_attribute_line_ids;

    @JsonIgnore
    private boolean valid_product_template_attribute_line_idsDirtyFlag;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @Product_productActivity_type_idDefault(info = "默认规则")
    private Integer activity_type_id;

    @JsonIgnore
    private boolean activity_type_idDirtyFlag;

    /**
     * 属性 [PUBLIC_CATEG_IDS]
     *
     */
    @Product_productPublic_categ_idsDefault(info = "默认规则")
    private String public_categ_ids;

    @JsonIgnore
    private boolean public_categ_idsDirtyFlag;

    /**
     * 属性 [EVENT_TICKET_IDS]
     *
     */
    @Product_productEvent_ticket_idsDefault(info = "默认规则")
    private String event_ticket_ids;

    @JsonIgnore
    private boolean event_ticket_idsDirtyFlag;

    /**
     * 属性 [PRICE]
     *
     */
    @Product_productPriceDefault(info = "默认规则")
    private Double price;

    @JsonIgnore
    private boolean priceDirtyFlag;

    /**
     * 属性 [ATTRIBUTE_LINE_IDS]
     *
     */
    @Product_productAttribute_line_idsDefault(info = "默认规则")
    private String attribute_line_ids;

    @JsonIgnore
    private boolean attribute_line_idsDirtyFlag;

    /**
     * 属性 [VIRTUAL_AVAILABLE]
     *
     */
    @Product_productVirtual_availableDefault(info = "默认规则")
    private Double virtual_available;

    @JsonIgnore
    private boolean virtual_availableDirtyFlag;

    /**
     * 属性 [NBR_REORDERING_RULES]
     *
     */
    @Product_productNbr_reordering_rulesDefault(info = "默认规则")
    private Integer nbr_reordering_rules;

    @JsonIgnore
    private boolean nbr_reordering_rulesDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Product_productActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Product_productMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Product_productMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Product_productIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [WEBSITE_PRICE_DIFFERENCE]
     *
     */
    @Product_productWebsite_price_differenceDefault(info = "默认规则")
    private String website_price_difference;

    @JsonIgnore
    private boolean website_price_differenceDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Product_productMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [CART_QTY]
     *
     */
    @Product_productCart_qtyDefault(info = "默认规则")
    private Integer cart_qty;

    @JsonIgnore
    private boolean cart_qtyDirtyFlag;

    /**
     * 属性 [WEBSITE_PUBLIC_PRICE]
     *
     */
    @Product_productWebsite_public_priceDefault(info = "默认规则")
    private Double website_public_price;

    @JsonIgnore
    private boolean website_public_priceDirtyFlag;

    /**
     * 属性 [RATING_IDS]
     *
     */
    @Product_productRating_idsDefault(info = "默认规则")
    private String rating_ids;

    @JsonIgnore
    private boolean rating_idsDirtyFlag;

    /**
     * 属性 [BOM_LINE_IDS]
     *
     */
    @Product_productBom_line_idsDefault(info = "默认规则")
    private String bom_line_ids;

    @JsonIgnore
    private boolean bom_line_idsDirtyFlag;

    /**
     * 属性 [WEBSITE_PRICE]
     *
     */
    @Product_productWebsite_priceDefault(info = "默认规则")
    private Double website_price;

    @JsonIgnore
    private boolean website_priceDirtyFlag;

    /**
     * 属性 [OUTGOING_QTY]
     *
     */
    @Product_productOutgoing_qtyDefault(info = "默认规则")
    private Double outgoing_qty;

    @JsonIgnore
    private boolean outgoing_qtyDirtyFlag;

    /**
     * 属性 [SALES_COUNT]
     *
     */
    @Product_productSales_countDefault(info = "默认规则")
    private Double sales_count;

    @JsonIgnore
    private boolean sales_countDirtyFlag;

    /**
     * 属性 [VALID_PRODUCT_ATTRIBUTE_WNVA_IDS]
     *
     */
    @Product_productValid_product_attribute_wnva_idsDefault(info = "默认规则")
    private String valid_product_attribute_wnva_ids;

    @JsonIgnore
    private boolean valid_product_attribute_wnva_idsDirtyFlag;

    /**
     * 属性 [IMAGE_MEDIUM]
     *
     */
    @Product_productImage_mediumDefault(info = "默认规则")
    private byte[] image_medium;

    @JsonIgnore
    private boolean image_mediumDirtyFlag;

    /**
     * 属性 [VALID_EXISTING_VARIANT_IDS]
     *
     */
    @Product_productValid_existing_variant_idsDefault(info = "默认规则")
    private String valid_existing_variant_ids;

    @JsonIgnore
    private boolean valid_existing_variant_idsDirtyFlag;

    /**
     * 属性 [STOCK_VALUE_CURRENCY_ID]
     *
     */
    @Product_productStock_value_currency_idDefault(info = "默认规则")
    private Integer stock_value_currency_id;

    @JsonIgnore
    private boolean stock_value_currency_idDirtyFlag;

    /**
     * 属性 [STOCK_VALUE]
     *
     */
    @Product_productStock_valueDefault(info = "默认规则")
    private Double stock_value;

    @JsonIgnore
    private boolean stock_valueDirtyFlag;

    /**
     * 属性 [WEBSITE_STYLE_IDS]
     *
     */
    @Product_productWebsite_style_idsDefault(info = "默认规则")
    private String website_style_ids;

    @JsonIgnore
    private boolean website_style_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Product_productMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [WEIGHT]
     *
     */
    @Product_productWeightDefault(info = "默认规则")
    private Double weight;

    @JsonIgnore
    private boolean weightDirtyFlag;

    /**
     * 属性 [BOM_IDS]
     *
     */
    @Product_productBom_idsDefault(info = "默认规则")
    private String bom_ids;

    @JsonIgnore
    private boolean bom_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Product_productMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @Product_productActivity_stateDefault(info = "默认规则")
    private String activity_state;

    @JsonIgnore
    private boolean activity_stateDirtyFlag;

    /**
     * 属性 [VALID_PRODUCT_TEMPLATE_ATTRIBUTE_LINE_WNVA_IDS]
     *
     */
    @Product_productValid_product_template_attribute_line_wnva_idsDefault(info = "默认规则")
    private String valid_product_template_attribute_line_wnva_ids;

    @JsonIgnore
    private boolean valid_product_template_attribute_line_wnva_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Product_productMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Product_productMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Product_productCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [VALID_PRODUCT_ATTRIBUTE_VALUE_IDS]
     *
     */
    @Product_productValid_product_attribute_value_idsDefault(info = "默认规则")
    private String valid_product_attribute_value_ids;

    @JsonIgnore
    private boolean valid_product_attribute_value_idsDirtyFlag;

    /**
     * 属性 [QTY_AVAILABLE]
     *
     */
    @Product_productQty_availableDefault(info = "默认规则")
    private Double qty_available;

    @JsonIgnore
    private boolean qty_availableDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Product_productMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [IMAGE_VARIANT]
     *
     */
    @Product_productImage_variantDefault(info = "默认规则")
    private byte[] image_variant;

    @JsonIgnore
    private boolean image_variantDirtyFlag;

    /**
     * 属性 [STOCK_MOVE_IDS]
     *
     */
    @Product_productStock_move_idsDefault(info = "默认规则")
    private String stock_move_ids;

    @JsonIgnore
    private boolean stock_move_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Product_productMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Product_productWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [STOCK_FIFO_REAL_TIME_AML_IDS]
     *
     */
    @Product_productStock_fifo_real_time_aml_idsDefault(info = "默认规则")
    private String stock_fifo_real_time_aml_ids;

    @JsonIgnore
    private boolean stock_fifo_real_time_aml_idsDirtyFlag;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @Product_productActivity_date_deadlineDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp activity_date_deadline;

    @JsonIgnore
    private boolean activity_date_deadlineDirtyFlag;

    /**
     * 属性 [CODE]
     *
     */
    @Product_productCodeDefault(info = "默认规则")
    private String code;

    @JsonIgnore
    private boolean codeDirtyFlag;

    /**
     * 属性 [REORDERING_MIN_QTY]
     *
     */
    @Product_productReordering_min_qtyDefault(info = "默认规则")
    private Double reordering_min_qty;

    @JsonIgnore
    private boolean reordering_min_qtyDirtyFlag;

    /**
     * 属性 [IMAGE]
     *
     */
    @Product_productImageDefault(info = "默认规则")
    private byte[] image;

    @JsonIgnore
    private boolean imageDirtyFlag;

    /**
     * 属性 [ROUTE_IDS]
     *
     */
    @Product_productRoute_idsDefault(info = "默认规则")
    private String route_ids;

    @JsonIgnore
    private boolean route_idsDirtyFlag;

    /**
     * 属性 [TAXES_ID]
     *
     */
    @Product_productTaxes_idDefault(info = "默认规则")
    private String taxes_id;

    @JsonIgnore
    private boolean taxes_idDirtyFlag;

    /**
     * 属性 [BOM_COUNT]
     *
     */
    @Product_productBom_countDefault(info = "默认规则")
    private Integer bom_count;

    @JsonIgnore
    private boolean bom_countDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Product_productMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [PACKAGING_IDS]
     *
     */
    @Product_productPackaging_idsDefault(info = "默认规则")
    private String packaging_ids;

    @JsonIgnore
    private boolean packaging_idsDirtyFlag;

    /**
     * 属性 [VALID_ARCHIVED_VARIANT_IDS]
     *
     */
    @Product_productValid_archived_variant_idsDefault(info = "默认规则")
    private String valid_archived_variant_ids;

    @JsonIgnore
    private boolean valid_archived_variant_idsDirtyFlag;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @Product_productActivity_user_idDefault(info = "默认规则")
    private Integer activity_user_id;

    @JsonIgnore
    private boolean activity_user_idDirtyFlag;

    /**
     * 属性 [ITEM_IDS]
     *
     */
    @Product_productItem_idsDefault(info = "默认规则")
    private String item_ids;

    @JsonIgnore
    private boolean item_idsDirtyFlag;

    /**
     * 属性 [PURCHASED_PRODUCT_QTY]
     *
     */
    @Product_productPurchased_product_qtyDefault(info = "默认规则")
    private Double purchased_product_qty;

    @JsonIgnore
    private boolean purchased_product_qtyDirtyFlag;

    /**
     * 属性 [REORDERING_MAX_QTY]
     *
     */
    @Product_productReordering_max_qtyDefault(info = "默认规则")
    private Double reordering_max_qty;

    @JsonIgnore
    private boolean reordering_max_qtyDirtyFlag;

    /**
     * 属性 [ORDERPOINT_IDS]
     *
     */
    @Product_productOrderpoint_idsDefault(info = "默认规则")
    private String orderpoint_ids;

    @JsonIgnore
    private boolean orderpoint_idsDirtyFlag;

    /**
     * 属性 [OPTIONAL_PRODUCT_IDS]
     *
     */
    @Product_productOptional_product_idsDefault(info = "默认规则")
    private String optional_product_ids;

    @JsonIgnore
    private boolean optional_product_idsDirtyFlag;

    /**
     * 属性 [IS_PRODUCT_VARIANT]
     *
     */
    @Product_productIs_product_variantDefault(info = "默认规则")
    private String is_product_variant;

    @JsonIgnore
    private boolean is_product_variantDirtyFlag;

    /**
     * 属性 [USED_IN_BOM_COUNT]
     *
     */
    @Product_productUsed_in_bom_countDefault(info = "默认规则")
    private Integer used_in_bom_count;

    @JsonIgnore
    private boolean used_in_bom_countDirtyFlag;

    /**
     * 属性 [QTY_AT_DATE]
     *
     */
    @Product_productQty_at_dateDefault(info = "默认规则")
    private Double qty_at_date;

    @JsonIgnore
    private boolean qty_at_dateDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Product_productMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @Product_productActivity_idsDefault(info = "默认规则")
    private String activity_ids;

    @JsonIgnore
    private boolean activity_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Product_productMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [BARCODE]
     *
     */
    @Product_productBarcodeDefault(info = "默认规则")
    private String barcode;

    @JsonIgnore
    private boolean barcodeDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Product_productDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [STANDARD_PRICE]
     *
     */
    @Product_productStandard_priceDefault(info = "默认规则")
    private Double standard_price;

    @JsonIgnore
    private boolean standard_priceDirtyFlag;

    /**
     * 属性 [ATTRIBUTE_VALUE_IDS]
     *
     */
    @Product_productAttribute_value_idsDefault(info = "默认规则")
    private String attribute_value_ids;

    @JsonIgnore
    private boolean attribute_value_idsDirtyFlag;

    /**
     * 属性 [PRICE_EXTRA]
     *
     */
    @Product_productPrice_extraDefault(info = "默认规则")
    private Double price_extra;

    @JsonIgnore
    private boolean price_extraDirtyFlag;

    /**
     * 属性 [VARIANT_BOM_IDS]
     *
     */
    @Product_productVariant_bom_idsDefault(info = "默认规则")
    private String variant_bom_ids;

    @JsonIgnore
    private boolean variant_bom_idsDirtyFlag;

    /**
     * 属性 [ALTERNATIVE_PRODUCT_IDS]
     *
     */
    @Product_productAlternative_product_idsDefault(info = "默认规则")
    private String alternative_product_ids;

    @JsonIgnore
    private boolean alternative_product_idsDirtyFlag;

    /**
     * 属性 [DEFAULT_CODE]
     *
     */
    @Product_productDefault_codeDefault(info = "默认规则")
    private String default_code;

    @JsonIgnore
    private boolean default_codeDirtyFlag;

    /**
     * 属性 [ROUTE_FROM_CATEG_IDS]
     *
     */
    @Product_productRoute_from_categ_idsDefault(info = "默认规则")
    private String route_from_categ_ids;

    @JsonIgnore
    private boolean route_from_categ_idsDirtyFlag;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @Product_productActivity_summaryDefault(info = "默认规则")
    private String activity_summary;

    @JsonIgnore
    private boolean activity_summaryDirtyFlag;

    /**
     * 属性 [INCOMING_QTY]
     *
     */
    @Product_productIncoming_qtyDefault(info = "默认规则")
    private Double incoming_qty;

    @JsonIgnore
    private boolean incoming_qtyDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Product_productCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [TRACKING]
     *
     */
    @Product_productTrackingDefault(info = "默认规则")
    private String tracking;

    @JsonIgnore
    private boolean trackingDirtyFlag;

    /**
     * 属性 [DESCRIPTION_PICKING]
     *
     */
    @Product_productDescription_pickingDefault(info = "默认规则")
    private String description_picking;

    @JsonIgnore
    private boolean description_pickingDirtyFlag;

    /**
     * 属性 [PROPERTY_STOCK_ACCOUNT_OUTPUT]
     *
     */
    @Product_productProperty_stock_account_outputDefault(info = "默认规则")
    private Integer property_stock_account_output;

    @JsonIgnore
    private boolean property_stock_account_outputDirtyFlag;

    /**
     * 属性 [SALE_OK]
     *
     */
    @Product_productSale_okDefault(info = "默认规则")
    private String sale_ok;

    @JsonIgnore
    private boolean sale_okDirtyFlag;

    /**
     * 属性 [WEBSITE_DESCRIPTION]
     *
     */
    @Product_productWebsite_descriptionDefault(info = "默认规则")
    private String website_description;

    @JsonIgnore
    private boolean website_descriptionDirtyFlag;

    /**
     * 属性 [WEBSITE_META_OG_IMG]
     *
     */
    @Product_productWebsite_meta_og_imgDefault(info = "默认规则")
    private String website_meta_og_img;

    @JsonIgnore
    private boolean website_meta_og_imgDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Product_productCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [TO_WEIGHT]
     *
     */
    @Product_productTo_weightDefault(info = "默认规则")
    private String to_weight;

    @JsonIgnore
    private boolean to_weightDirtyFlag;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @Product_productDescriptionDefault(info = "默认规则")
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;

    /**
     * 属性 [DESCRIPTION_PICKINGIN]
     *
     */
    @Product_productDescription_pickinginDefault(info = "默认规则")
    private String description_pickingin;

    @JsonIgnore
    private boolean description_pickinginDirtyFlag;

    /**
     * 属性 [LIST_PRICE]
     *
     */
    @Product_productList_priceDefault(info = "默认规则")
    private Double list_price;

    @JsonIgnore
    private boolean list_priceDirtyFlag;

    /**
     * 属性 [HIDE_EXPENSE_POLICY]
     *
     */
    @Product_productHide_expense_policyDefault(info = "默认规则")
    private String hide_expense_policy;

    @JsonIgnore
    private boolean hide_expense_policyDirtyFlag;

    /**
     * 属性 [DESCRIPTION_SALE]
     *
     */
    @Product_productDescription_saleDefault(info = "默认规则")
    private String description_sale;

    @JsonIgnore
    private boolean description_saleDirtyFlag;

    /**
     * 属性 [COST_METHOD]
     *
     */
    @Product_productCost_methodDefault(info = "默认规则")
    private String cost_method;

    @JsonIgnore
    private boolean cost_methodDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Product_productSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [SALE_LINE_WARN_MSG]
     *
     */
    @Product_productSale_line_warn_msgDefault(info = "默认规则")
    private String sale_line_warn_msg;

    @JsonIgnore
    private boolean sale_line_warn_msgDirtyFlag;

    /**
     * 属性 [WAREHOUSE_ID]
     *
     */
    @Product_productWarehouse_idDefault(info = "默认规则")
    private Integer warehouse_id;

    @JsonIgnore
    private boolean warehouse_idDirtyFlag;

    /**
     * 属性 [RENTAL]
     *
     */
    @Product_productRentalDefault(info = "默认规则")
    private String rental;

    @JsonIgnore
    private boolean rentalDirtyFlag;

    /**
     * 属性 [PROPERTY_ACCOUNT_CREDITOR_PRICE_DIFFERENCE]
     *
     */
    @Product_productProperty_account_creditor_price_differenceDefault(info = "默认规则")
    private Integer property_account_creditor_price_difference;

    @JsonIgnore
    private boolean property_account_creditor_price_differenceDirtyFlag;

    /**
     * 属性 [WEIGHT_UOM_NAME]
     *
     */
    @Product_productWeight_uom_nameDefault(info = "默认规则")
    private String weight_uom_name;

    @JsonIgnore
    private boolean weight_uom_nameDirtyFlag;

    /**
     * 属性 [COST_CURRENCY_ID]
     *
     */
    @Product_productCost_currency_idDefault(info = "默认规则")
    private Integer cost_currency_id;

    @JsonIgnore
    private boolean cost_currency_idDirtyFlag;

    /**
     * 属性 [PROPERTY_STOCK_ACCOUNT_INPUT]
     *
     */
    @Product_productProperty_stock_account_inputDefault(info = "默认规则")
    private Integer property_stock_account_input;

    @JsonIgnore
    private boolean property_stock_account_inputDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Product_productNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [PRODUCE_DELAY]
     *
     */
    @Product_productProduce_delayDefault(info = "默认规则")
    private Double produce_delay;

    @JsonIgnore
    private boolean produce_delayDirtyFlag;

    /**
     * 属性 [IS_SEO_OPTIMIZED]
     *
     */
    @Product_productIs_seo_optimizedDefault(info = "默认规则")
    private String is_seo_optimized;

    @JsonIgnore
    private boolean is_seo_optimizedDirtyFlag;

    /**
     * 属性 [WEBSITE_URL]
     *
     */
    @Product_productWebsite_urlDefault(info = "默认规则")
    private String website_url;

    @JsonIgnore
    private boolean website_urlDirtyFlag;

    /**
     * 属性 [RATING_LAST_FEEDBACK]
     *
     */
    @Product_productRating_last_feedbackDefault(info = "默认规则")
    private String rating_last_feedback;

    @JsonIgnore
    private boolean rating_last_feedbackDirtyFlag;

    /**
     * 属性 [WEBSITE_SIZE_Y]
     *
     */
    @Product_productWebsite_size_yDefault(info = "默认规则")
    private Integer website_size_y;

    @JsonIgnore
    private boolean website_size_yDirtyFlag;

    /**
     * 属性 [EVENT_OK]
     *
     */
    @Product_productEvent_okDefault(info = "默认规则")
    private String event_ok;

    @JsonIgnore
    private boolean event_okDirtyFlag;

    /**
     * 属性 [INVENTORY_AVAILABILITY]
     *
     */
    @Product_productInventory_availabilityDefault(info = "默认规则")
    private String inventory_availability;

    @JsonIgnore
    private boolean inventory_availabilityDirtyFlag;

    /**
     * 属性 [PURCHASE_OK]
     *
     */
    @Product_productPurchase_okDefault(info = "默认规则")
    private String purchase_ok;

    @JsonIgnore
    private boolean purchase_okDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Product_productCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [RATING_LAST_VALUE]
     *
     */
    @Product_productRating_last_valueDefault(info = "默认规则")
    private Double rating_last_value;

    @JsonIgnore
    private boolean rating_last_valueDirtyFlag;

    /**
     * 属性 [WEBSITE_META_TITLE]
     *
     */
    @Product_productWebsite_meta_titleDefault(info = "默认规则")
    private String website_meta_title;

    @JsonIgnore
    private boolean website_meta_titleDirtyFlag;

    /**
     * 属性 [RATING_LAST_IMAGE]
     *
     */
    @Product_productRating_last_imageDefault(info = "默认规则")
    private byte[] rating_last_image;

    @JsonIgnore
    private boolean rating_last_imageDirtyFlag;

    /**
     * 属性 [DESCRIPTION_PURCHASE]
     *
     */
    @Product_productDescription_purchaseDefault(info = "默认规则")
    private String description_purchase;

    @JsonIgnore
    private boolean description_purchaseDirtyFlag;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @Product_productWebsite_idDefault(info = "默认规则")
    private Integer website_id;

    @JsonIgnore
    private boolean website_idDirtyFlag;

    /**
     * 属性 [CAN_BE_EXPENSED]
     *
     */
    @Product_productCan_be_expensedDefault(info = "默认规则")
    private String can_be_expensed;

    @JsonIgnore
    private boolean can_be_expensedDirtyFlag;

    /**
     * 属性 [SALE_LINE_WARN]
     *
     */
    @Product_productSale_line_warnDefault(info = "默认规则")
    private String sale_line_warn;

    @JsonIgnore
    private boolean sale_line_warnDirtyFlag;

    /**
     * 属性 [WEBSITE_SIZE_X]
     *
     */
    @Product_productWebsite_size_xDefault(info = "默认规则")
    private Integer website_size_x;

    @JsonIgnore
    private boolean website_size_xDirtyFlag;

    /**
     * 属性 [SERVICE_TO_PURCHASE]
     *
     */
    @Product_productService_to_purchaseDefault(info = "默认规则")
    private String service_to_purchase;

    @JsonIgnore
    private boolean service_to_purchaseDirtyFlag;

    /**
     * 属性 [WEBSITE_SEQUENCE]
     *
     */
    @Product_productWebsite_sequenceDefault(info = "默认规则")
    private Integer website_sequence;

    @JsonIgnore
    private boolean website_sequenceDirtyFlag;

    /**
     * 属性 [PROPERTY_STOCK_INVENTORY]
     *
     */
    @Product_productProperty_stock_inventoryDefault(info = "默认规则")
    private Integer property_stock_inventory;

    @JsonIgnore
    private boolean property_stock_inventoryDirtyFlag;

    /**
     * 属性 [LOCATION_ID]
     *
     */
    @Product_productLocation_idDefault(info = "默认规则")
    private Integer location_id;

    @JsonIgnore
    private boolean location_idDirtyFlag;

    /**
     * 属性 [PROPERTY_VALUATION]
     *
     */
    @Product_productProperty_valuationDefault(info = "默认规则")
    private String property_valuation;

    @JsonIgnore
    private boolean property_valuationDirtyFlag;

    /**
     * 属性 [IS_PUBLISHED]
     *
     */
    @Product_productIs_publishedDefault(info = "默认规则")
    private String is_published;

    @JsonIgnore
    private boolean is_publishedDirtyFlag;

    /**
     * 属性 [EXPENSE_POLICY]
     *
     */
    @Product_productExpense_policyDefault(info = "默认规则")
    private String expense_policy;

    @JsonIgnore
    private boolean expense_policyDirtyFlag;

    /**
     * 属性 [WEIGHT_UOM_ID]
     *
     */
    @Product_productWeight_uom_idDefault(info = "默认规则")
    private Integer weight_uom_id;

    @JsonIgnore
    private boolean weight_uom_idDirtyFlag;

    /**
     * 属性 [COLOR]
     *
     */
    @Product_productColorDefault(info = "默认规则")
    private Integer color;

    @JsonIgnore
    private boolean colorDirtyFlag;

    /**
     * 属性 [PROPERTY_STOCK_PRODUCTION]
     *
     */
    @Product_productProperty_stock_productionDefault(info = "默认规则")
    private Integer property_stock_production;

    @JsonIgnore
    private boolean property_stock_productionDirtyFlag;

    /**
     * 属性 [WEBSITE_PUBLISHED]
     *
     */
    @Product_productWebsite_publishedDefault(info = "默认规则")
    private String website_published;

    @JsonIgnore
    private boolean website_publishedDirtyFlag;

    /**
     * 属性 [WEBSITE_META_KEYWORDS]
     *
     */
    @Product_productWebsite_meta_keywordsDefault(info = "默认规则")
    private String website_meta_keywords;

    @JsonIgnore
    private boolean website_meta_keywordsDirtyFlag;

    /**
     * 属性 [DESCRIPTION_PICKINGOUT]
     *
     */
    @Product_productDescription_pickingoutDefault(info = "默认规则")
    private String description_pickingout;

    @JsonIgnore
    private boolean description_pickingoutDirtyFlag;

    /**
     * 属性 [PRICELIST_ID]
     *
     */
    @Product_productPricelist_idDefault(info = "默认规则")
    private Integer pricelist_id;

    @JsonIgnore
    private boolean pricelist_idDirtyFlag;

    /**
     * 属性 [RATING_COUNT]
     *
     */
    @Product_productRating_countDefault(info = "默认规则")
    private Integer rating_count;

    @JsonIgnore
    private boolean rating_countDirtyFlag;

    /**
     * 属性 [WEBSITE_META_DESCRIPTION]
     *
     */
    @Product_productWebsite_meta_descriptionDefault(info = "默认规则")
    private String website_meta_description;

    @JsonIgnore
    private boolean website_meta_descriptionDirtyFlag;

    /**
     * 属性 [VALUATION]
     *
     */
    @Product_productValuationDefault(info = "默认规则")
    private String valuation;

    @JsonIgnore
    private boolean valuationDirtyFlag;

    /**
     * 属性 [INVOICE_POLICY]
     *
     */
    @Product_productInvoice_policyDefault(info = "默认规则")
    private String invoice_policy;

    @JsonIgnore
    private boolean invoice_policyDirtyFlag;

    /**
     * 属性 [PURCHASE_LINE_WARN_MSG]
     *
     */
    @Product_productPurchase_line_warn_msgDefault(info = "默认规则")
    private String purchase_line_warn_msg;

    @JsonIgnore
    private boolean purchase_line_warn_msgDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Product_productWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [PROPERTY_ACCOUNT_INCOME_ID]
     *
     */
    @Product_productProperty_account_income_idDefault(info = "默认规则")
    private Integer property_account_income_id;

    @JsonIgnore
    private boolean property_account_income_idDirtyFlag;

    /**
     * 属性 [PROPERTY_COST_METHOD]
     *
     */
    @Product_productProperty_cost_methodDefault(info = "默认规则")
    private String property_cost_method;

    @JsonIgnore
    private boolean property_cost_methodDirtyFlag;

    /**
     * 属性 [CATEG_ID]
     *
     */
    @Product_productCateg_idDefault(info = "默认规则")
    private Integer categ_id;

    @JsonIgnore
    private boolean categ_idDirtyFlag;

    /**
     * 属性 [ISPARTS]
     *
     */
    @Product_productIsPartsDefault(info = "默认规则")
    private String isParts;

    @JsonIgnore
    private boolean isPartsDirtyFlag;

    /**
     * 属性 [UOM_ID]
     *
     */
    @Product_productUom_idDefault(info = "默认规则")
    private Integer uom_id;

    @JsonIgnore
    private boolean uom_idDirtyFlag;

    /**
     * 属性 [PRODUCT_VARIANT_ID]
     *
     */
    @Product_productProduct_variant_idDefault(info = "默认规则")
    private Integer product_variant_id;

    @JsonIgnore
    private boolean product_variant_idDirtyFlag;

    /**
     * 属性 [TYPE]
     *
     */
    @Product_productTypeDefault(info = "默认规则")
    private String type;

    @JsonIgnore
    private boolean typeDirtyFlag;

    /**
     * 属性 [PURCHASE_METHOD]
     *
     */
    @Product_productPurchase_methodDefault(info = "默认规则")
    private String purchase_method;

    @JsonIgnore
    private boolean purchase_methodDirtyFlag;

    /**
     * 属性 [RESPONSIBLE_ID]
     *
     */
    @Product_productResponsible_idDefault(info = "默认规则")
    private Integer responsible_id;

    @JsonIgnore
    private boolean responsible_idDirtyFlag;

    /**
     * 属性 [SERVICE_TYPE]
     *
     */
    @Product_productService_typeDefault(info = "默认规则")
    private String service_type;

    @JsonIgnore
    private boolean service_typeDirtyFlag;

    /**
     * 属性 [UOM_NAME]
     *
     */
    @Product_productUom_nameDefault(info = "默认规则")
    private String uom_name;

    @JsonIgnore
    private boolean uom_nameDirtyFlag;

    /**
     * 属性 [AVAILABLE_THRESHOLD]
     *
     */
    @Product_productAvailable_thresholdDefault(info = "默认规则")
    private Double available_threshold;

    @JsonIgnore
    private boolean available_thresholdDirtyFlag;

    /**
     * 属性 [PURCHASE_LINE_WARN]
     *
     */
    @Product_productPurchase_line_warnDefault(info = "默认规则")
    private String purchase_line_warn;

    @JsonIgnore
    private boolean purchase_line_warnDirtyFlag;

    /**
     * 属性 [PRODUCT_VARIANT_COUNT]
     *
     */
    @Product_productProduct_variant_countDefault(info = "默认规则")
    private Integer product_variant_count;

    @JsonIgnore
    private boolean product_variant_countDirtyFlag;

    /**
     * 属性 [POS_CATEG_ID]
     *
     */
    @Product_productPos_categ_idDefault(info = "默认规则")
    private Integer pos_categ_id;

    @JsonIgnore
    private boolean pos_categ_idDirtyFlag;

    /**
     * 属性 [CUSTOM_MESSAGE]
     *
     */
    @Product_productCustom_messageDefault(info = "默认规则")
    private String custom_message;

    @JsonIgnore
    private boolean custom_messageDirtyFlag;

    /**
     * 属性 [PROPERTY_ACCOUNT_EXPENSE_ID]
     *
     */
    @Product_productProperty_account_expense_idDefault(info = "默认规则")
    private Integer property_account_expense_id;

    @JsonIgnore
    private boolean property_account_expense_idDirtyFlag;

    /**
     * 属性 [SALE_DELAY]
     *
     */
    @Product_productSale_delayDefault(info = "默认规则")
    private Double sale_delay;

    @JsonIgnore
    private boolean sale_delayDirtyFlag;

    /**
     * 属性 [UOM_PO_ID]
     *
     */
    @Product_productUom_po_idDefault(info = "默认规则")
    private Integer uom_po_id;

    @JsonIgnore
    private boolean uom_po_idDirtyFlag;

    /**
     * 属性 [AVAILABLE_IN_POS]
     *
     */
    @Product_productAvailable_in_posDefault(info = "默认规则")
    private String available_in_pos;

    @JsonIgnore
    private boolean available_in_posDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Product_productCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Product_productWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [PRODUCT_TMPL_ID]
     *
     */
    @Product_productProduct_tmpl_idDefault(info = "默认规则")
    private Integer product_tmpl_id;

    @JsonIgnore
    private boolean product_tmpl_idDirtyFlag;


    /**
     * 获取 [VARIANT_SELLER_IDS]
     */
    @JsonProperty("variant_seller_ids")
    public String getVariant_seller_ids(){
        return variant_seller_ids ;
    }

    /**
     * 设置 [VARIANT_SELLER_IDS]
     */
    @JsonProperty("variant_seller_ids")
    public void setVariant_seller_ids(String  variant_seller_ids){
        this.variant_seller_ids = variant_seller_ids ;
        this.variant_seller_idsDirtyFlag = true ;
    }

    /**
     * 获取 [VARIANT_SELLER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getVariant_seller_idsDirtyFlag(){
        return variant_seller_idsDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_TEMPLATE_ATTRIBUTE_VALUE_IDS]
     */
    @JsonProperty("product_template_attribute_value_ids")
    public String getProduct_template_attribute_value_ids(){
        return product_template_attribute_value_ids ;
    }

    /**
     * 设置 [PRODUCT_TEMPLATE_ATTRIBUTE_VALUE_IDS]
     */
    @JsonProperty("product_template_attribute_value_ids")
    public void setProduct_template_attribute_value_ids(String  product_template_attribute_value_ids){
        this.product_template_attribute_value_ids = product_template_attribute_value_ids ;
        this.product_template_attribute_value_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_TEMPLATE_ATTRIBUTE_VALUE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getProduct_template_attribute_value_idsDirtyFlag(){
        return product_template_attribute_value_idsDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_VARIANT_IDS]
     */
    @JsonProperty("product_variant_ids")
    public String getProduct_variant_ids(){
        return product_variant_ids ;
    }

    /**
     * 设置 [PRODUCT_VARIANT_IDS]
     */
    @JsonProperty("product_variant_ids")
    public void setProduct_variant_ids(String  product_variant_ids){
        this.product_variant_ids = product_variant_ids ;
        this.product_variant_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_VARIANT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getProduct_variant_idsDirtyFlag(){
        return product_variant_idsDirtyFlag ;
    }

    /**
     * 获取 [IMAGE_SMALL]
     */
    @JsonProperty("image_small")
    public byte[] getImage_small(){
        return image_small ;
    }

    /**
     * 设置 [IMAGE_SMALL]
     */
    @JsonProperty("image_small")
    public void setImage_small(byte[]  image_small){
        this.image_small = image_small ;
        this.image_smallDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE_SMALL]脏标记
     */
    @JsonIgnore
    public boolean getImage_smallDirtyFlag(){
        return image_smallDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [VOLUME]
     */
    @JsonProperty("volume")
    public Double getVolume(){
        return volume ;
    }

    /**
     * 设置 [VOLUME]
     */
    @JsonProperty("volume")
    public void setVolume(Double  volume){
        this.volume = volume ;
        this.volumeDirtyFlag = true ;
    }

    /**
     * 获取 [VOLUME]脏标记
     */
    @JsonIgnore
    public boolean getVolumeDirtyFlag(){
        return volumeDirtyFlag ;
    }

    /**
     * 获取 [LST_PRICE]
     */
    @JsonProperty("lst_price")
    public Double getLst_price(){
        return lst_price ;
    }

    /**
     * 设置 [LST_PRICE]
     */
    @JsonProperty("lst_price")
    public void setLst_price(Double  lst_price){
        this.lst_price = lst_price ;
        this.lst_priceDirtyFlag = true ;
    }

    /**
     * 获取 [LST_PRICE]脏标记
     */
    @JsonIgnore
    public boolean getLst_priceDirtyFlag(){
        return lst_priceDirtyFlag ;
    }

    /**
     * 获取 [VALID_PRODUCT_ATTRIBUTE_IDS]
     */
    @JsonProperty("valid_product_attribute_ids")
    public String getValid_product_attribute_ids(){
        return valid_product_attribute_ids ;
    }

    /**
     * 设置 [VALID_PRODUCT_ATTRIBUTE_IDS]
     */
    @JsonProperty("valid_product_attribute_ids")
    public void setValid_product_attribute_ids(String  valid_product_attribute_ids){
        this.valid_product_attribute_ids = valid_product_attribute_ids ;
        this.valid_product_attribute_idsDirtyFlag = true ;
    }

    /**
     * 获取 [VALID_PRODUCT_ATTRIBUTE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getValid_product_attribute_idsDirtyFlag(){
        return valid_product_attribute_idsDirtyFlag ;
    }

    /**
     * 获取 [STOCK_FIFO_MANUAL_MOVE_IDS]
     */
    @JsonProperty("stock_fifo_manual_move_ids")
    public String getStock_fifo_manual_move_ids(){
        return stock_fifo_manual_move_ids ;
    }

    /**
     * 设置 [STOCK_FIFO_MANUAL_MOVE_IDS]
     */
    @JsonProperty("stock_fifo_manual_move_ids")
    public void setStock_fifo_manual_move_ids(String  stock_fifo_manual_move_ids){
        this.stock_fifo_manual_move_ids = stock_fifo_manual_move_ids ;
        this.stock_fifo_manual_move_idsDirtyFlag = true ;
    }

    /**
     * 获取 [STOCK_FIFO_MANUAL_MOVE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getStock_fifo_manual_move_idsDirtyFlag(){
        return stock_fifo_manual_move_idsDirtyFlag ;
    }

    /**
     * 获取 [STOCK_QUANT_IDS]
     */
    @JsonProperty("stock_quant_ids")
    public String getStock_quant_ids(){
        return stock_quant_ids ;
    }

    /**
     * 设置 [STOCK_QUANT_IDS]
     */
    @JsonProperty("stock_quant_ids")
    public void setStock_quant_ids(String  stock_quant_ids){
        this.stock_quant_ids = stock_quant_ids ;
        this.stock_quant_idsDirtyFlag = true ;
    }

    /**
     * 获取 [STOCK_QUANT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getStock_quant_idsDirtyFlag(){
        return stock_quant_idsDirtyFlag ;
    }

    /**
     * 获取 [SUPPLIER_TAXES_ID]
     */
    @JsonProperty("supplier_taxes_id")
    public String getSupplier_taxes_id(){
        return supplier_taxes_id ;
    }

    /**
     * 设置 [SUPPLIER_TAXES_ID]
     */
    @JsonProperty("supplier_taxes_id")
    public void setSupplier_taxes_id(String  supplier_taxes_id){
        this.supplier_taxes_id = supplier_taxes_id ;
        this.supplier_taxes_idDirtyFlag = true ;
    }

    /**
     * 获取 [SUPPLIER_TAXES_ID]脏标记
     */
    @JsonIgnore
    public boolean getSupplier_taxes_idDirtyFlag(){
        return supplier_taxes_idDirtyFlag ;
    }

    /**
     * 获取 [PRICELIST_ITEM_IDS]
     */
    @JsonProperty("pricelist_item_ids")
    public String getPricelist_item_ids(){
        return pricelist_item_ids ;
    }

    /**
     * 设置 [PRICELIST_ITEM_IDS]
     */
    @JsonProperty("pricelist_item_ids")
    public void setPricelist_item_ids(String  pricelist_item_ids){
        this.pricelist_item_ids = pricelist_item_ids ;
        this.pricelist_item_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PRICELIST_ITEM_IDS]脏标记
     */
    @JsonIgnore
    public boolean getPricelist_item_idsDirtyFlag(){
        return pricelist_item_idsDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [ACCESSORY_PRODUCT_IDS]
     */
    @JsonProperty("accessory_product_ids")
    public String getAccessory_product_ids(){
        return accessory_product_ids ;
    }

    /**
     * 设置 [ACCESSORY_PRODUCT_IDS]
     */
    @JsonProperty("accessory_product_ids")
    public void setAccessory_product_ids(String  accessory_product_ids){
        this.accessory_product_ids = accessory_product_ids ;
        this.accessory_product_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ACCESSORY_PRODUCT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAccessory_product_idsDirtyFlag(){
        return accessory_product_idsDirtyFlag ;
    }

    /**
     * 获取 [SELLER_IDS]
     */
    @JsonProperty("seller_ids")
    public String getSeller_ids(){
        return seller_ids ;
    }

    /**
     * 设置 [SELLER_IDS]
     */
    @JsonProperty("seller_ids")
    public void setSeller_ids(String  seller_ids){
        this.seller_ids = seller_ids ;
        this.seller_idsDirtyFlag = true ;
    }

    /**
     * 获取 [SELLER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getSeller_idsDirtyFlag(){
        return seller_idsDirtyFlag ;
    }

    /**
     * 获取 [VALID_PRODUCT_ATTRIBUTE_VALUE_WNVA_IDS]
     */
    @JsonProperty("valid_product_attribute_value_wnva_ids")
    public String getValid_product_attribute_value_wnva_ids(){
        return valid_product_attribute_value_wnva_ids ;
    }

    /**
     * 设置 [VALID_PRODUCT_ATTRIBUTE_VALUE_WNVA_IDS]
     */
    @JsonProperty("valid_product_attribute_value_wnva_ids")
    public void setValid_product_attribute_value_wnva_ids(String  valid_product_attribute_value_wnva_ids){
        this.valid_product_attribute_value_wnva_ids = valid_product_attribute_value_wnva_ids ;
        this.valid_product_attribute_value_wnva_idsDirtyFlag = true ;
    }

    /**
     * 获取 [VALID_PRODUCT_ATTRIBUTE_VALUE_WNVA_IDS]脏标记
     */
    @JsonIgnore
    public boolean getValid_product_attribute_value_wnva_idsDirtyFlag(){
        return valid_product_attribute_value_wnva_idsDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_REF]
     */
    @JsonProperty("partner_ref")
    public String getPartner_ref(){
        return partner_ref ;
    }

    /**
     * 设置 [PARTNER_REF]
     */
    @JsonProperty("partner_ref")
    public void setPartner_ref(String  partner_ref){
        this.partner_ref = partner_ref ;
        this.partner_refDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_REF]脏标记
     */
    @JsonIgnore
    public boolean getPartner_refDirtyFlag(){
        return partner_refDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_IMAGE_IDS]
     */
    @JsonProperty("product_image_ids")
    public String getProduct_image_ids(){
        return product_image_ids ;
    }

    /**
     * 设置 [PRODUCT_IMAGE_IDS]
     */
    @JsonProperty("product_image_ids")
    public void setProduct_image_ids(String  product_image_ids){
        this.product_image_ids = product_image_ids ;
        this.product_image_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_IMAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getProduct_image_idsDirtyFlag(){
        return product_image_idsDirtyFlag ;
    }

    /**
     * 获取 [MRP_PRODUCT_QTY]
     */
    @JsonProperty("mrp_product_qty")
    public Double getMrp_product_qty(){
        return mrp_product_qty ;
    }

    /**
     * 设置 [MRP_PRODUCT_QTY]
     */
    @JsonProperty("mrp_product_qty")
    public void setMrp_product_qty(Double  mrp_product_qty){
        this.mrp_product_qty = mrp_product_qty ;
        this.mrp_product_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [MRP_PRODUCT_QTY]脏标记
     */
    @JsonIgnore
    public boolean getMrp_product_qtyDirtyFlag(){
        return mrp_product_qtyDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [VALID_PRODUCT_TEMPLATE_ATTRIBUTE_LINE_IDS]
     */
    @JsonProperty("valid_product_template_attribute_line_ids")
    public String getValid_product_template_attribute_line_ids(){
        return valid_product_template_attribute_line_ids ;
    }

    /**
     * 设置 [VALID_PRODUCT_TEMPLATE_ATTRIBUTE_LINE_IDS]
     */
    @JsonProperty("valid_product_template_attribute_line_ids")
    public void setValid_product_template_attribute_line_ids(String  valid_product_template_attribute_line_ids){
        this.valid_product_template_attribute_line_ids = valid_product_template_attribute_line_ids ;
        this.valid_product_template_attribute_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [VALID_PRODUCT_TEMPLATE_ATTRIBUTE_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getValid_product_template_attribute_line_idsDirtyFlag(){
        return valid_product_template_attribute_line_idsDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return activity_type_id ;
    }

    /**
     * 设置 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [PUBLIC_CATEG_IDS]
     */
    @JsonProperty("public_categ_ids")
    public String getPublic_categ_ids(){
        return public_categ_ids ;
    }

    /**
     * 设置 [PUBLIC_CATEG_IDS]
     */
    @JsonProperty("public_categ_ids")
    public void setPublic_categ_ids(String  public_categ_ids){
        this.public_categ_ids = public_categ_ids ;
        this.public_categ_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PUBLIC_CATEG_IDS]脏标记
     */
    @JsonIgnore
    public boolean getPublic_categ_idsDirtyFlag(){
        return public_categ_idsDirtyFlag ;
    }

    /**
     * 获取 [EVENT_TICKET_IDS]
     */
    @JsonProperty("event_ticket_ids")
    public String getEvent_ticket_ids(){
        return event_ticket_ids ;
    }

    /**
     * 设置 [EVENT_TICKET_IDS]
     */
    @JsonProperty("event_ticket_ids")
    public void setEvent_ticket_ids(String  event_ticket_ids){
        this.event_ticket_ids = event_ticket_ids ;
        this.event_ticket_idsDirtyFlag = true ;
    }

    /**
     * 获取 [EVENT_TICKET_IDS]脏标记
     */
    @JsonIgnore
    public boolean getEvent_ticket_idsDirtyFlag(){
        return event_ticket_idsDirtyFlag ;
    }

    /**
     * 获取 [PRICE]
     */
    @JsonProperty("price")
    public Double getPrice(){
        return price ;
    }

    /**
     * 设置 [PRICE]
     */
    @JsonProperty("price")
    public void setPrice(Double  price){
        this.price = price ;
        this.priceDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE]脏标记
     */
    @JsonIgnore
    public boolean getPriceDirtyFlag(){
        return priceDirtyFlag ;
    }

    /**
     * 获取 [ATTRIBUTE_LINE_IDS]
     */
    @JsonProperty("attribute_line_ids")
    public String getAttribute_line_ids(){
        return attribute_line_ids ;
    }

    /**
     * 设置 [ATTRIBUTE_LINE_IDS]
     */
    @JsonProperty("attribute_line_ids")
    public void setAttribute_line_ids(String  attribute_line_ids){
        this.attribute_line_ids = attribute_line_ids ;
        this.attribute_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ATTRIBUTE_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAttribute_line_idsDirtyFlag(){
        return attribute_line_idsDirtyFlag ;
    }

    /**
     * 获取 [VIRTUAL_AVAILABLE]
     */
    @JsonProperty("virtual_available")
    public Double getVirtual_available(){
        return virtual_available ;
    }

    /**
     * 设置 [VIRTUAL_AVAILABLE]
     */
    @JsonProperty("virtual_available")
    public void setVirtual_available(Double  virtual_available){
        this.virtual_available = virtual_available ;
        this.virtual_availableDirtyFlag = true ;
    }

    /**
     * 获取 [VIRTUAL_AVAILABLE]脏标记
     */
    @JsonIgnore
    public boolean getVirtual_availableDirtyFlag(){
        return virtual_availableDirtyFlag ;
    }

    /**
     * 获取 [NBR_REORDERING_RULES]
     */
    @JsonProperty("nbr_reordering_rules")
    public Integer getNbr_reordering_rules(){
        return nbr_reordering_rules ;
    }

    /**
     * 设置 [NBR_REORDERING_RULES]
     */
    @JsonProperty("nbr_reordering_rules")
    public void setNbr_reordering_rules(Integer  nbr_reordering_rules){
        this.nbr_reordering_rules = nbr_reordering_rules ;
        this.nbr_reordering_rulesDirtyFlag = true ;
    }

    /**
     * 获取 [NBR_REORDERING_RULES]脏标记
     */
    @JsonIgnore
    public boolean getNbr_reordering_rulesDirtyFlag(){
        return nbr_reordering_rulesDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_PRICE_DIFFERENCE]
     */
    @JsonProperty("website_price_difference")
    public String getWebsite_price_difference(){
        return website_price_difference ;
    }

    /**
     * 设置 [WEBSITE_PRICE_DIFFERENCE]
     */
    @JsonProperty("website_price_difference")
    public void setWebsite_price_difference(String  website_price_difference){
        this.website_price_difference = website_price_difference ;
        this.website_price_differenceDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_PRICE_DIFFERENCE]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_price_differenceDirtyFlag(){
        return website_price_differenceDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [CART_QTY]
     */
    @JsonProperty("cart_qty")
    public Integer getCart_qty(){
        return cart_qty ;
    }

    /**
     * 设置 [CART_QTY]
     */
    @JsonProperty("cart_qty")
    public void setCart_qty(Integer  cart_qty){
        this.cart_qty = cart_qty ;
        this.cart_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [CART_QTY]脏标记
     */
    @JsonIgnore
    public boolean getCart_qtyDirtyFlag(){
        return cart_qtyDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_PUBLIC_PRICE]
     */
    @JsonProperty("website_public_price")
    public Double getWebsite_public_price(){
        return website_public_price ;
    }

    /**
     * 设置 [WEBSITE_PUBLIC_PRICE]
     */
    @JsonProperty("website_public_price")
    public void setWebsite_public_price(Double  website_public_price){
        this.website_public_price = website_public_price ;
        this.website_public_priceDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_PUBLIC_PRICE]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_public_priceDirtyFlag(){
        return website_public_priceDirtyFlag ;
    }

    /**
     * 获取 [RATING_IDS]
     */
    @JsonProperty("rating_ids")
    public String getRating_ids(){
        return rating_ids ;
    }

    /**
     * 设置 [RATING_IDS]
     */
    @JsonProperty("rating_ids")
    public void setRating_ids(String  rating_ids){
        this.rating_ids = rating_ids ;
        this.rating_idsDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_IDS]脏标记
     */
    @JsonIgnore
    public boolean getRating_idsDirtyFlag(){
        return rating_idsDirtyFlag ;
    }

    /**
     * 获取 [BOM_LINE_IDS]
     */
    @JsonProperty("bom_line_ids")
    public String getBom_line_ids(){
        return bom_line_ids ;
    }

    /**
     * 设置 [BOM_LINE_IDS]
     */
    @JsonProperty("bom_line_ids")
    public void setBom_line_ids(String  bom_line_ids){
        this.bom_line_ids = bom_line_ids ;
        this.bom_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [BOM_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getBom_line_idsDirtyFlag(){
        return bom_line_idsDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_PRICE]
     */
    @JsonProperty("website_price")
    public Double getWebsite_price(){
        return website_price ;
    }

    /**
     * 设置 [WEBSITE_PRICE]
     */
    @JsonProperty("website_price")
    public void setWebsite_price(Double  website_price){
        this.website_price = website_price ;
        this.website_priceDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_PRICE]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_priceDirtyFlag(){
        return website_priceDirtyFlag ;
    }

    /**
     * 获取 [OUTGOING_QTY]
     */
    @JsonProperty("outgoing_qty")
    public Double getOutgoing_qty(){
        return outgoing_qty ;
    }

    /**
     * 设置 [OUTGOING_QTY]
     */
    @JsonProperty("outgoing_qty")
    public void setOutgoing_qty(Double  outgoing_qty){
        this.outgoing_qty = outgoing_qty ;
        this.outgoing_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [OUTGOING_QTY]脏标记
     */
    @JsonIgnore
    public boolean getOutgoing_qtyDirtyFlag(){
        return outgoing_qtyDirtyFlag ;
    }

    /**
     * 获取 [SALES_COUNT]
     */
    @JsonProperty("sales_count")
    public Double getSales_count(){
        return sales_count ;
    }

    /**
     * 设置 [SALES_COUNT]
     */
    @JsonProperty("sales_count")
    public void setSales_count(Double  sales_count){
        this.sales_count = sales_count ;
        this.sales_countDirtyFlag = true ;
    }

    /**
     * 获取 [SALES_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getSales_countDirtyFlag(){
        return sales_countDirtyFlag ;
    }

    /**
     * 获取 [VALID_PRODUCT_ATTRIBUTE_WNVA_IDS]
     */
    @JsonProperty("valid_product_attribute_wnva_ids")
    public String getValid_product_attribute_wnva_ids(){
        return valid_product_attribute_wnva_ids ;
    }

    /**
     * 设置 [VALID_PRODUCT_ATTRIBUTE_WNVA_IDS]
     */
    @JsonProperty("valid_product_attribute_wnva_ids")
    public void setValid_product_attribute_wnva_ids(String  valid_product_attribute_wnva_ids){
        this.valid_product_attribute_wnva_ids = valid_product_attribute_wnva_ids ;
        this.valid_product_attribute_wnva_idsDirtyFlag = true ;
    }

    /**
     * 获取 [VALID_PRODUCT_ATTRIBUTE_WNVA_IDS]脏标记
     */
    @JsonIgnore
    public boolean getValid_product_attribute_wnva_idsDirtyFlag(){
        return valid_product_attribute_wnva_idsDirtyFlag ;
    }

    /**
     * 获取 [IMAGE_MEDIUM]
     */
    @JsonProperty("image_medium")
    public byte[] getImage_medium(){
        return image_medium ;
    }

    /**
     * 设置 [IMAGE_MEDIUM]
     */
    @JsonProperty("image_medium")
    public void setImage_medium(byte[]  image_medium){
        this.image_medium = image_medium ;
        this.image_mediumDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE_MEDIUM]脏标记
     */
    @JsonIgnore
    public boolean getImage_mediumDirtyFlag(){
        return image_mediumDirtyFlag ;
    }

    /**
     * 获取 [VALID_EXISTING_VARIANT_IDS]
     */
    @JsonProperty("valid_existing_variant_ids")
    public String getValid_existing_variant_ids(){
        return valid_existing_variant_ids ;
    }

    /**
     * 设置 [VALID_EXISTING_VARIANT_IDS]
     */
    @JsonProperty("valid_existing_variant_ids")
    public void setValid_existing_variant_ids(String  valid_existing_variant_ids){
        this.valid_existing_variant_ids = valid_existing_variant_ids ;
        this.valid_existing_variant_idsDirtyFlag = true ;
    }

    /**
     * 获取 [VALID_EXISTING_VARIANT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getValid_existing_variant_idsDirtyFlag(){
        return valid_existing_variant_idsDirtyFlag ;
    }

    /**
     * 获取 [STOCK_VALUE_CURRENCY_ID]
     */
    @JsonProperty("stock_value_currency_id")
    public Integer getStock_value_currency_id(){
        return stock_value_currency_id ;
    }

    /**
     * 设置 [STOCK_VALUE_CURRENCY_ID]
     */
    @JsonProperty("stock_value_currency_id")
    public void setStock_value_currency_id(Integer  stock_value_currency_id){
        this.stock_value_currency_id = stock_value_currency_id ;
        this.stock_value_currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [STOCK_VALUE_CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getStock_value_currency_idDirtyFlag(){
        return stock_value_currency_idDirtyFlag ;
    }

    /**
     * 获取 [STOCK_VALUE]
     */
    @JsonProperty("stock_value")
    public Double getStock_value(){
        return stock_value ;
    }

    /**
     * 设置 [STOCK_VALUE]
     */
    @JsonProperty("stock_value")
    public void setStock_value(Double  stock_value){
        this.stock_value = stock_value ;
        this.stock_valueDirtyFlag = true ;
    }

    /**
     * 获取 [STOCK_VALUE]脏标记
     */
    @JsonIgnore
    public boolean getStock_valueDirtyFlag(){
        return stock_valueDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_STYLE_IDS]
     */
    @JsonProperty("website_style_ids")
    public String getWebsite_style_ids(){
        return website_style_ids ;
    }

    /**
     * 设置 [WEBSITE_STYLE_IDS]
     */
    @JsonProperty("website_style_ids")
    public void setWebsite_style_ids(String  website_style_ids){
        this.website_style_ids = website_style_ids ;
        this.website_style_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_STYLE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_style_idsDirtyFlag(){
        return website_style_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [WEIGHT]
     */
    @JsonProperty("weight")
    public Double getWeight(){
        return weight ;
    }

    /**
     * 设置 [WEIGHT]
     */
    @JsonProperty("weight")
    public void setWeight(Double  weight){
        this.weight = weight ;
        this.weightDirtyFlag = true ;
    }

    /**
     * 获取 [WEIGHT]脏标记
     */
    @JsonIgnore
    public boolean getWeightDirtyFlag(){
        return weightDirtyFlag ;
    }

    /**
     * 获取 [BOM_IDS]
     */
    @JsonProperty("bom_ids")
    public String getBom_ids(){
        return bom_ids ;
    }

    /**
     * 设置 [BOM_IDS]
     */
    @JsonProperty("bom_ids")
    public void setBom_ids(String  bom_ids){
        this.bom_ids = bom_ids ;
        this.bom_idsDirtyFlag = true ;
    }

    /**
     * 获取 [BOM_IDS]脏标记
     */
    @JsonIgnore
    public boolean getBom_idsDirtyFlag(){
        return bom_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return activity_state ;
    }

    /**
     * 设置 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_STATE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return activity_stateDirtyFlag ;
    }

    /**
     * 获取 [VALID_PRODUCT_TEMPLATE_ATTRIBUTE_LINE_WNVA_IDS]
     */
    @JsonProperty("valid_product_template_attribute_line_wnva_ids")
    public String getValid_product_template_attribute_line_wnva_ids(){
        return valid_product_template_attribute_line_wnva_ids ;
    }

    /**
     * 设置 [VALID_PRODUCT_TEMPLATE_ATTRIBUTE_LINE_WNVA_IDS]
     */
    @JsonProperty("valid_product_template_attribute_line_wnva_ids")
    public void setValid_product_template_attribute_line_wnva_ids(String  valid_product_template_attribute_line_wnva_ids){
        this.valid_product_template_attribute_line_wnva_ids = valid_product_template_attribute_line_wnva_ids ;
        this.valid_product_template_attribute_line_wnva_idsDirtyFlag = true ;
    }

    /**
     * 获取 [VALID_PRODUCT_TEMPLATE_ATTRIBUTE_LINE_WNVA_IDS]脏标记
     */
    @JsonIgnore
    public boolean getValid_product_template_attribute_line_wnva_idsDirtyFlag(){
        return valid_product_template_attribute_line_wnva_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [VALID_PRODUCT_ATTRIBUTE_VALUE_IDS]
     */
    @JsonProperty("valid_product_attribute_value_ids")
    public String getValid_product_attribute_value_ids(){
        return valid_product_attribute_value_ids ;
    }

    /**
     * 设置 [VALID_PRODUCT_ATTRIBUTE_VALUE_IDS]
     */
    @JsonProperty("valid_product_attribute_value_ids")
    public void setValid_product_attribute_value_ids(String  valid_product_attribute_value_ids){
        this.valid_product_attribute_value_ids = valid_product_attribute_value_ids ;
        this.valid_product_attribute_value_idsDirtyFlag = true ;
    }

    /**
     * 获取 [VALID_PRODUCT_ATTRIBUTE_VALUE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getValid_product_attribute_value_idsDirtyFlag(){
        return valid_product_attribute_value_idsDirtyFlag ;
    }

    /**
     * 获取 [QTY_AVAILABLE]
     */
    @JsonProperty("qty_available")
    public Double getQty_available(){
        return qty_available ;
    }

    /**
     * 设置 [QTY_AVAILABLE]
     */
    @JsonProperty("qty_available")
    public void setQty_available(Double  qty_available){
        this.qty_available = qty_available ;
        this.qty_availableDirtyFlag = true ;
    }

    /**
     * 获取 [QTY_AVAILABLE]脏标记
     */
    @JsonIgnore
    public boolean getQty_availableDirtyFlag(){
        return qty_availableDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [IMAGE_VARIANT]
     */
    @JsonProperty("image_variant")
    public byte[] getImage_variant(){
        return image_variant ;
    }

    /**
     * 设置 [IMAGE_VARIANT]
     */
    @JsonProperty("image_variant")
    public void setImage_variant(byte[]  image_variant){
        this.image_variant = image_variant ;
        this.image_variantDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE_VARIANT]脏标记
     */
    @JsonIgnore
    public boolean getImage_variantDirtyFlag(){
        return image_variantDirtyFlag ;
    }

    /**
     * 获取 [STOCK_MOVE_IDS]
     */
    @JsonProperty("stock_move_ids")
    public String getStock_move_ids(){
        return stock_move_ids ;
    }

    /**
     * 设置 [STOCK_MOVE_IDS]
     */
    @JsonProperty("stock_move_ids")
    public void setStock_move_ids(String  stock_move_ids){
        this.stock_move_ids = stock_move_ids ;
        this.stock_move_idsDirtyFlag = true ;
    }

    /**
     * 获取 [STOCK_MOVE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getStock_move_idsDirtyFlag(){
        return stock_move_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [STOCK_FIFO_REAL_TIME_AML_IDS]
     */
    @JsonProperty("stock_fifo_real_time_aml_ids")
    public String getStock_fifo_real_time_aml_ids(){
        return stock_fifo_real_time_aml_ids ;
    }

    /**
     * 设置 [STOCK_FIFO_REAL_TIME_AML_IDS]
     */
    @JsonProperty("stock_fifo_real_time_aml_ids")
    public void setStock_fifo_real_time_aml_ids(String  stock_fifo_real_time_aml_ids){
        this.stock_fifo_real_time_aml_ids = stock_fifo_real_time_aml_ids ;
        this.stock_fifo_real_time_aml_idsDirtyFlag = true ;
    }

    /**
     * 获取 [STOCK_FIFO_REAL_TIME_AML_IDS]脏标记
     */
    @JsonIgnore
    public boolean getStock_fifo_real_time_aml_idsDirtyFlag(){
        return stock_fifo_real_time_aml_idsDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return activity_date_deadline ;
    }

    /**
     * 设置 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return activity_date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [CODE]
     */
    @JsonProperty("code")
    public String getCode(){
        return code ;
    }

    /**
     * 设置 [CODE]
     */
    @JsonProperty("code")
    public void setCode(String  code){
        this.code = code ;
        this.codeDirtyFlag = true ;
    }

    /**
     * 获取 [CODE]脏标记
     */
    @JsonIgnore
    public boolean getCodeDirtyFlag(){
        return codeDirtyFlag ;
    }

    /**
     * 获取 [REORDERING_MIN_QTY]
     */
    @JsonProperty("reordering_min_qty")
    public Double getReordering_min_qty(){
        return reordering_min_qty ;
    }

    /**
     * 设置 [REORDERING_MIN_QTY]
     */
    @JsonProperty("reordering_min_qty")
    public void setReordering_min_qty(Double  reordering_min_qty){
        this.reordering_min_qty = reordering_min_qty ;
        this.reordering_min_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [REORDERING_MIN_QTY]脏标记
     */
    @JsonIgnore
    public boolean getReordering_min_qtyDirtyFlag(){
        return reordering_min_qtyDirtyFlag ;
    }

    /**
     * 获取 [IMAGE]
     */
    @JsonProperty("image")
    public byte[] getImage(){
        return image ;
    }

    /**
     * 设置 [IMAGE]
     */
    @JsonProperty("image")
    public void setImage(byte[]  image){
        this.image = image ;
        this.imageDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE]脏标记
     */
    @JsonIgnore
    public boolean getImageDirtyFlag(){
        return imageDirtyFlag ;
    }

    /**
     * 获取 [ROUTE_IDS]
     */
    @JsonProperty("route_ids")
    public String getRoute_ids(){
        return route_ids ;
    }

    /**
     * 设置 [ROUTE_IDS]
     */
    @JsonProperty("route_ids")
    public void setRoute_ids(String  route_ids){
        this.route_ids = route_ids ;
        this.route_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ROUTE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getRoute_idsDirtyFlag(){
        return route_idsDirtyFlag ;
    }

    /**
     * 获取 [TAXES_ID]
     */
    @JsonProperty("taxes_id")
    public String getTaxes_id(){
        return taxes_id ;
    }

    /**
     * 设置 [TAXES_ID]
     */
    @JsonProperty("taxes_id")
    public void setTaxes_id(String  taxes_id){
        this.taxes_id = taxes_id ;
        this.taxes_idDirtyFlag = true ;
    }

    /**
     * 获取 [TAXES_ID]脏标记
     */
    @JsonIgnore
    public boolean getTaxes_idDirtyFlag(){
        return taxes_idDirtyFlag ;
    }

    /**
     * 获取 [BOM_COUNT]
     */
    @JsonProperty("bom_count")
    public Integer getBom_count(){
        return bom_count ;
    }

    /**
     * 设置 [BOM_COUNT]
     */
    @JsonProperty("bom_count")
    public void setBom_count(Integer  bom_count){
        this.bom_count = bom_count ;
        this.bom_countDirtyFlag = true ;
    }

    /**
     * 获取 [BOM_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getBom_countDirtyFlag(){
        return bom_countDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [PACKAGING_IDS]
     */
    @JsonProperty("packaging_ids")
    public String getPackaging_ids(){
        return packaging_ids ;
    }

    /**
     * 设置 [PACKAGING_IDS]
     */
    @JsonProperty("packaging_ids")
    public void setPackaging_ids(String  packaging_ids){
        this.packaging_ids = packaging_ids ;
        this.packaging_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PACKAGING_IDS]脏标记
     */
    @JsonIgnore
    public boolean getPackaging_idsDirtyFlag(){
        return packaging_idsDirtyFlag ;
    }

    /**
     * 获取 [VALID_ARCHIVED_VARIANT_IDS]
     */
    @JsonProperty("valid_archived_variant_ids")
    public String getValid_archived_variant_ids(){
        return valid_archived_variant_ids ;
    }

    /**
     * 设置 [VALID_ARCHIVED_VARIANT_IDS]
     */
    @JsonProperty("valid_archived_variant_ids")
    public void setValid_archived_variant_ids(String  valid_archived_variant_ids){
        this.valid_archived_variant_ids = valid_archived_variant_ids ;
        this.valid_archived_variant_idsDirtyFlag = true ;
    }

    /**
     * 获取 [VALID_ARCHIVED_VARIANT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getValid_archived_variant_idsDirtyFlag(){
        return valid_archived_variant_idsDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return activity_user_id ;
    }

    /**
     * 设置 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return activity_user_idDirtyFlag ;
    }

    /**
     * 获取 [ITEM_IDS]
     */
    @JsonProperty("item_ids")
    public String getItem_ids(){
        return item_ids ;
    }

    /**
     * 设置 [ITEM_IDS]
     */
    @JsonProperty("item_ids")
    public void setItem_ids(String  item_ids){
        this.item_ids = item_ids ;
        this.item_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ITEM_IDS]脏标记
     */
    @JsonIgnore
    public boolean getItem_idsDirtyFlag(){
        return item_idsDirtyFlag ;
    }

    /**
     * 获取 [PURCHASED_PRODUCT_QTY]
     */
    @JsonProperty("purchased_product_qty")
    public Double getPurchased_product_qty(){
        return purchased_product_qty ;
    }

    /**
     * 设置 [PURCHASED_PRODUCT_QTY]
     */
    @JsonProperty("purchased_product_qty")
    public void setPurchased_product_qty(Double  purchased_product_qty){
        this.purchased_product_qty = purchased_product_qty ;
        this.purchased_product_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASED_PRODUCT_QTY]脏标记
     */
    @JsonIgnore
    public boolean getPurchased_product_qtyDirtyFlag(){
        return purchased_product_qtyDirtyFlag ;
    }

    /**
     * 获取 [REORDERING_MAX_QTY]
     */
    @JsonProperty("reordering_max_qty")
    public Double getReordering_max_qty(){
        return reordering_max_qty ;
    }

    /**
     * 设置 [REORDERING_MAX_QTY]
     */
    @JsonProperty("reordering_max_qty")
    public void setReordering_max_qty(Double  reordering_max_qty){
        this.reordering_max_qty = reordering_max_qty ;
        this.reordering_max_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [REORDERING_MAX_QTY]脏标记
     */
    @JsonIgnore
    public boolean getReordering_max_qtyDirtyFlag(){
        return reordering_max_qtyDirtyFlag ;
    }

    /**
     * 获取 [ORDERPOINT_IDS]
     */
    @JsonProperty("orderpoint_ids")
    public String getOrderpoint_ids(){
        return orderpoint_ids ;
    }

    /**
     * 设置 [ORDERPOINT_IDS]
     */
    @JsonProperty("orderpoint_ids")
    public void setOrderpoint_ids(String  orderpoint_ids){
        this.orderpoint_ids = orderpoint_ids ;
        this.orderpoint_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ORDERPOINT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getOrderpoint_idsDirtyFlag(){
        return orderpoint_idsDirtyFlag ;
    }

    /**
     * 获取 [OPTIONAL_PRODUCT_IDS]
     */
    @JsonProperty("optional_product_ids")
    public String getOptional_product_ids(){
        return optional_product_ids ;
    }

    /**
     * 设置 [OPTIONAL_PRODUCT_IDS]
     */
    @JsonProperty("optional_product_ids")
    public void setOptional_product_ids(String  optional_product_ids){
        this.optional_product_ids = optional_product_ids ;
        this.optional_product_idsDirtyFlag = true ;
    }

    /**
     * 获取 [OPTIONAL_PRODUCT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getOptional_product_idsDirtyFlag(){
        return optional_product_idsDirtyFlag ;
    }

    /**
     * 获取 [IS_PRODUCT_VARIANT]
     */
    @JsonProperty("is_product_variant")
    public String getIs_product_variant(){
        return is_product_variant ;
    }

    /**
     * 设置 [IS_PRODUCT_VARIANT]
     */
    @JsonProperty("is_product_variant")
    public void setIs_product_variant(String  is_product_variant){
        this.is_product_variant = is_product_variant ;
        this.is_product_variantDirtyFlag = true ;
    }

    /**
     * 获取 [IS_PRODUCT_VARIANT]脏标记
     */
    @JsonIgnore
    public boolean getIs_product_variantDirtyFlag(){
        return is_product_variantDirtyFlag ;
    }

    /**
     * 获取 [USED_IN_BOM_COUNT]
     */
    @JsonProperty("used_in_bom_count")
    public Integer getUsed_in_bom_count(){
        return used_in_bom_count ;
    }

    /**
     * 设置 [USED_IN_BOM_COUNT]
     */
    @JsonProperty("used_in_bom_count")
    public void setUsed_in_bom_count(Integer  used_in_bom_count){
        this.used_in_bom_count = used_in_bom_count ;
        this.used_in_bom_countDirtyFlag = true ;
    }

    /**
     * 获取 [USED_IN_BOM_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getUsed_in_bom_countDirtyFlag(){
        return used_in_bom_countDirtyFlag ;
    }

    /**
     * 获取 [QTY_AT_DATE]
     */
    @JsonProperty("qty_at_date")
    public Double getQty_at_date(){
        return qty_at_date ;
    }

    /**
     * 设置 [QTY_AT_DATE]
     */
    @JsonProperty("qty_at_date")
    public void setQty_at_date(Double  qty_at_date){
        this.qty_at_date = qty_at_date ;
        this.qty_at_dateDirtyFlag = true ;
    }

    /**
     * 获取 [QTY_AT_DATE]脏标记
     */
    @JsonIgnore
    public boolean getQty_at_dateDirtyFlag(){
        return qty_at_dateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return activity_ids ;
    }

    /**
     * 设置 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return activity_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [BARCODE]
     */
    @JsonProperty("barcode")
    public String getBarcode(){
        return barcode ;
    }

    /**
     * 设置 [BARCODE]
     */
    @JsonProperty("barcode")
    public void setBarcode(String  barcode){
        this.barcode = barcode ;
        this.barcodeDirtyFlag = true ;
    }

    /**
     * 获取 [BARCODE]脏标记
     */
    @JsonIgnore
    public boolean getBarcodeDirtyFlag(){
        return barcodeDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [STANDARD_PRICE]
     */
    @JsonProperty("standard_price")
    public Double getStandard_price(){
        return standard_price ;
    }

    /**
     * 设置 [STANDARD_PRICE]
     */
    @JsonProperty("standard_price")
    public void setStandard_price(Double  standard_price){
        this.standard_price = standard_price ;
        this.standard_priceDirtyFlag = true ;
    }

    /**
     * 获取 [STANDARD_PRICE]脏标记
     */
    @JsonIgnore
    public boolean getStandard_priceDirtyFlag(){
        return standard_priceDirtyFlag ;
    }

    /**
     * 获取 [ATTRIBUTE_VALUE_IDS]
     */
    @JsonProperty("attribute_value_ids")
    public String getAttribute_value_ids(){
        return attribute_value_ids ;
    }

    /**
     * 设置 [ATTRIBUTE_VALUE_IDS]
     */
    @JsonProperty("attribute_value_ids")
    public void setAttribute_value_ids(String  attribute_value_ids){
        this.attribute_value_ids = attribute_value_ids ;
        this.attribute_value_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ATTRIBUTE_VALUE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAttribute_value_idsDirtyFlag(){
        return attribute_value_idsDirtyFlag ;
    }

    /**
     * 获取 [PRICE_EXTRA]
     */
    @JsonProperty("price_extra")
    public Double getPrice_extra(){
        return price_extra ;
    }

    /**
     * 设置 [PRICE_EXTRA]
     */
    @JsonProperty("price_extra")
    public void setPrice_extra(Double  price_extra){
        this.price_extra = price_extra ;
        this.price_extraDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_EXTRA]脏标记
     */
    @JsonIgnore
    public boolean getPrice_extraDirtyFlag(){
        return price_extraDirtyFlag ;
    }

    /**
     * 获取 [VARIANT_BOM_IDS]
     */
    @JsonProperty("variant_bom_ids")
    public String getVariant_bom_ids(){
        return variant_bom_ids ;
    }

    /**
     * 设置 [VARIANT_BOM_IDS]
     */
    @JsonProperty("variant_bom_ids")
    public void setVariant_bom_ids(String  variant_bom_ids){
        this.variant_bom_ids = variant_bom_ids ;
        this.variant_bom_idsDirtyFlag = true ;
    }

    /**
     * 获取 [VARIANT_BOM_IDS]脏标记
     */
    @JsonIgnore
    public boolean getVariant_bom_idsDirtyFlag(){
        return variant_bom_idsDirtyFlag ;
    }

    /**
     * 获取 [ALTERNATIVE_PRODUCT_IDS]
     */
    @JsonProperty("alternative_product_ids")
    public String getAlternative_product_ids(){
        return alternative_product_ids ;
    }

    /**
     * 设置 [ALTERNATIVE_PRODUCT_IDS]
     */
    @JsonProperty("alternative_product_ids")
    public void setAlternative_product_ids(String  alternative_product_ids){
        this.alternative_product_ids = alternative_product_ids ;
        this.alternative_product_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ALTERNATIVE_PRODUCT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAlternative_product_idsDirtyFlag(){
        return alternative_product_idsDirtyFlag ;
    }

    /**
     * 获取 [DEFAULT_CODE]
     */
    @JsonProperty("default_code")
    public String getDefault_code(){
        return default_code ;
    }

    /**
     * 设置 [DEFAULT_CODE]
     */
    @JsonProperty("default_code")
    public void setDefault_code(String  default_code){
        this.default_code = default_code ;
        this.default_codeDirtyFlag = true ;
    }

    /**
     * 获取 [DEFAULT_CODE]脏标记
     */
    @JsonIgnore
    public boolean getDefault_codeDirtyFlag(){
        return default_codeDirtyFlag ;
    }

    /**
     * 获取 [ROUTE_FROM_CATEG_IDS]
     */
    @JsonProperty("route_from_categ_ids")
    public String getRoute_from_categ_ids(){
        return route_from_categ_ids ;
    }

    /**
     * 设置 [ROUTE_FROM_CATEG_IDS]
     */
    @JsonProperty("route_from_categ_ids")
    public void setRoute_from_categ_ids(String  route_from_categ_ids){
        this.route_from_categ_ids = route_from_categ_ids ;
        this.route_from_categ_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ROUTE_FROM_CATEG_IDS]脏标记
     */
    @JsonIgnore
    public boolean getRoute_from_categ_idsDirtyFlag(){
        return route_from_categ_idsDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return activity_summary ;
    }

    /**
     * 设置 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return activity_summaryDirtyFlag ;
    }

    /**
     * 获取 [INCOMING_QTY]
     */
    @JsonProperty("incoming_qty")
    public Double getIncoming_qty(){
        return incoming_qty ;
    }

    /**
     * 设置 [INCOMING_QTY]
     */
    @JsonProperty("incoming_qty")
    public void setIncoming_qty(Double  incoming_qty){
        this.incoming_qty = incoming_qty ;
        this.incoming_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [INCOMING_QTY]脏标记
     */
    @JsonIgnore
    public boolean getIncoming_qtyDirtyFlag(){
        return incoming_qtyDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [TRACKING]
     */
    @JsonProperty("tracking")
    public String getTracking(){
        return tracking ;
    }

    /**
     * 设置 [TRACKING]
     */
    @JsonProperty("tracking")
    public void setTracking(String  tracking){
        this.tracking = tracking ;
        this.trackingDirtyFlag = true ;
    }

    /**
     * 获取 [TRACKING]脏标记
     */
    @JsonIgnore
    public boolean getTrackingDirtyFlag(){
        return trackingDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION_PICKING]
     */
    @JsonProperty("description_picking")
    public String getDescription_picking(){
        return description_picking ;
    }

    /**
     * 设置 [DESCRIPTION_PICKING]
     */
    @JsonProperty("description_picking")
    public void setDescription_picking(String  description_picking){
        this.description_picking = description_picking ;
        this.description_pickingDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION_PICKING]脏标记
     */
    @JsonIgnore
    public boolean getDescription_pickingDirtyFlag(){
        return description_pickingDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_STOCK_ACCOUNT_OUTPUT]
     */
    @JsonProperty("property_stock_account_output")
    public Integer getProperty_stock_account_output(){
        return property_stock_account_output ;
    }

    /**
     * 设置 [PROPERTY_STOCK_ACCOUNT_OUTPUT]
     */
    @JsonProperty("property_stock_account_output")
    public void setProperty_stock_account_output(Integer  property_stock_account_output){
        this.property_stock_account_output = property_stock_account_output ;
        this.property_stock_account_outputDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_STOCK_ACCOUNT_OUTPUT]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_account_outputDirtyFlag(){
        return property_stock_account_outputDirtyFlag ;
    }

    /**
     * 获取 [SALE_OK]
     */
    @JsonProperty("sale_ok")
    public String getSale_ok(){
        return sale_ok ;
    }

    /**
     * 设置 [SALE_OK]
     */
    @JsonProperty("sale_ok")
    public void setSale_ok(String  sale_ok){
        this.sale_ok = sale_ok ;
        this.sale_okDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_OK]脏标记
     */
    @JsonIgnore
    public boolean getSale_okDirtyFlag(){
        return sale_okDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_DESCRIPTION]
     */
    @JsonProperty("website_description")
    public String getWebsite_description(){
        return website_description ;
    }

    /**
     * 设置 [WEBSITE_DESCRIPTION]
     */
    @JsonProperty("website_description")
    public void setWebsite_description(String  website_description){
        this.website_description = website_description ;
        this.website_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_descriptionDirtyFlag(){
        return website_descriptionDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_OG_IMG]
     */
    @JsonProperty("website_meta_og_img")
    public String getWebsite_meta_og_img(){
        return website_meta_og_img ;
    }

    /**
     * 设置 [WEBSITE_META_OG_IMG]
     */
    @JsonProperty("website_meta_og_img")
    public void setWebsite_meta_og_img(String  website_meta_og_img){
        this.website_meta_og_img = website_meta_og_img ;
        this.website_meta_og_imgDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_OG_IMG]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_og_imgDirtyFlag(){
        return website_meta_og_imgDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [TO_WEIGHT]
     */
    @JsonProperty("to_weight")
    public String getTo_weight(){
        return to_weight ;
    }

    /**
     * 设置 [TO_WEIGHT]
     */
    @JsonProperty("to_weight")
    public void setTo_weight(String  to_weight){
        this.to_weight = to_weight ;
        this.to_weightDirtyFlag = true ;
    }

    /**
     * 获取 [TO_WEIGHT]脏标记
     */
    @JsonIgnore
    public boolean getTo_weightDirtyFlag(){
        return to_weightDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION]
     */
    @JsonProperty("description")
    public String getDescription(){
        return description ;
    }

    /**
     * 设置 [DESCRIPTION]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return descriptionDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION_PICKINGIN]
     */
    @JsonProperty("description_pickingin")
    public String getDescription_pickingin(){
        return description_pickingin ;
    }

    /**
     * 设置 [DESCRIPTION_PICKINGIN]
     */
    @JsonProperty("description_pickingin")
    public void setDescription_pickingin(String  description_pickingin){
        this.description_pickingin = description_pickingin ;
        this.description_pickinginDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION_PICKINGIN]脏标记
     */
    @JsonIgnore
    public boolean getDescription_pickinginDirtyFlag(){
        return description_pickinginDirtyFlag ;
    }

    /**
     * 获取 [LIST_PRICE]
     */
    @JsonProperty("list_price")
    public Double getList_price(){
        return list_price ;
    }

    /**
     * 设置 [LIST_PRICE]
     */
    @JsonProperty("list_price")
    public void setList_price(Double  list_price){
        this.list_price = list_price ;
        this.list_priceDirtyFlag = true ;
    }

    /**
     * 获取 [LIST_PRICE]脏标记
     */
    @JsonIgnore
    public boolean getList_priceDirtyFlag(){
        return list_priceDirtyFlag ;
    }

    /**
     * 获取 [HIDE_EXPENSE_POLICY]
     */
    @JsonProperty("hide_expense_policy")
    public String getHide_expense_policy(){
        return hide_expense_policy ;
    }

    /**
     * 设置 [HIDE_EXPENSE_POLICY]
     */
    @JsonProperty("hide_expense_policy")
    public void setHide_expense_policy(String  hide_expense_policy){
        this.hide_expense_policy = hide_expense_policy ;
        this.hide_expense_policyDirtyFlag = true ;
    }

    /**
     * 获取 [HIDE_EXPENSE_POLICY]脏标记
     */
    @JsonIgnore
    public boolean getHide_expense_policyDirtyFlag(){
        return hide_expense_policyDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION_SALE]
     */
    @JsonProperty("description_sale")
    public String getDescription_sale(){
        return description_sale ;
    }

    /**
     * 设置 [DESCRIPTION_SALE]
     */
    @JsonProperty("description_sale")
    public void setDescription_sale(String  description_sale){
        this.description_sale = description_sale ;
        this.description_saleDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION_SALE]脏标记
     */
    @JsonIgnore
    public boolean getDescription_saleDirtyFlag(){
        return description_saleDirtyFlag ;
    }

    /**
     * 获取 [COST_METHOD]
     */
    @JsonProperty("cost_method")
    public String getCost_method(){
        return cost_method ;
    }

    /**
     * 设置 [COST_METHOD]
     */
    @JsonProperty("cost_method")
    public void setCost_method(String  cost_method){
        this.cost_method = cost_method ;
        this.cost_methodDirtyFlag = true ;
    }

    /**
     * 获取 [COST_METHOD]脏标记
     */
    @JsonIgnore
    public boolean getCost_methodDirtyFlag(){
        return cost_methodDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [SALE_LINE_WARN_MSG]
     */
    @JsonProperty("sale_line_warn_msg")
    public String getSale_line_warn_msg(){
        return sale_line_warn_msg ;
    }

    /**
     * 设置 [SALE_LINE_WARN_MSG]
     */
    @JsonProperty("sale_line_warn_msg")
    public void setSale_line_warn_msg(String  sale_line_warn_msg){
        this.sale_line_warn_msg = sale_line_warn_msg ;
        this.sale_line_warn_msgDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_LINE_WARN_MSG]脏标记
     */
    @JsonIgnore
    public boolean getSale_line_warn_msgDirtyFlag(){
        return sale_line_warn_msgDirtyFlag ;
    }

    /**
     * 获取 [WAREHOUSE_ID]
     */
    @JsonProperty("warehouse_id")
    public Integer getWarehouse_id(){
        return warehouse_id ;
    }

    /**
     * 设置 [WAREHOUSE_ID]
     */
    @JsonProperty("warehouse_id")
    public void setWarehouse_id(Integer  warehouse_id){
        this.warehouse_id = warehouse_id ;
        this.warehouse_idDirtyFlag = true ;
    }

    /**
     * 获取 [WAREHOUSE_ID]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_idDirtyFlag(){
        return warehouse_idDirtyFlag ;
    }

    /**
     * 获取 [RENTAL]
     */
    @JsonProperty("rental")
    public String getRental(){
        return rental ;
    }

    /**
     * 设置 [RENTAL]
     */
    @JsonProperty("rental")
    public void setRental(String  rental){
        this.rental = rental ;
        this.rentalDirtyFlag = true ;
    }

    /**
     * 获取 [RENTAL]脏标记
     */
    @JsonIgnore
    public boolean getRentalDirtyFlag(){
        return rentalDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_CREDITOR_PRICE_DIFFERENCE]
     */
    @JsonProperty("property_account_creditor_price_difference")
    public Integer getProperty_account_creditor_price_difference(){
        return property_account_creditor_price_difference ;
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_CREDITOR_PRICE_DIFFERENCE]
     */
    @JsonProperty("property_account_creditor_price_difference")
    public void setProperty_account_creditor_price_difference(Integer  property_account_creditor_price_difference){
        this.property_account_creditor_price_difference = property_account_creditor_price_difference ;
        this.property_account_creditor_price_differenceDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_CREDITOR_PRICE_DIFFERENCE]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_creditor_price_differenceDirtyFlag(){
        return property_account_creditor_price_differenceDirtyFlag ;
    }

    /**
     * 获取 [WEIGHT_UOM_NAME]
     */
    @JsonProperty("weight_uom_name")
    public String getWeight_uom_name(){
        return weight_uom_name ;
    }

    /**
     * 设置 [WEIGHT_UOM_NAME]
     */
    @JsonProperty("weight_uom_name")
    public void setWeight_uom_name(String  weight_uom_name){
        this.weight_uom_name = weight_uom_name ;
        this.weight_uom_nameDirtyFlag = true ;
    }

    /**
     * 获取 [WEIGHT_UOM_NAME]脏标记
     */
    @JsonIgnore
    public boolean getWeight_uom_nameDirtyFlag(){
        return weight_uom_nameDirtyFlag ;
    }

    /**
     * 获取 [COST_CURRENCY_ID]
     */
    @JsonProperty("cost_currency_id")
    public Integer getCost_currency_id(){
        return cost_currency_id ;
    }

    /**
     * 设置 [COST_CURRENCY_ID]
     */
    @JsonProperty("cost_currency_id")
    public void setCost_currency_id(Integer  cost_currency_id){
        this.cost_currency_id = cost_currency_id ;
        this.cost_currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [COST_CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCost_currency_idDirtyFlag(){
        return cost_currency_idDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_STOCK_ACCOUNT_INPUT]
     */
    @JsonProperty("property_stock_account_input")
    public Integer getProperty_stock_account_input(){
        return property_stock_account_input ;
    }

    /**
     * 设置 [PROPERTY_STOCK_ACCOUNT_INPUT]
     */
    @JsonProperty("property_stock_account_input")
    public void setProperty_stock_account_input(Integer  property_stock_account_input){
        this.property_stock_account_input = property_stock_account_input ;
        this.property_stock_account_inputDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_STOCK_ACCOUNT_INPUT]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_account_inputDirtyFlag(){
        return property_stock_account_inputDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [PRODUCE_DELAY]
     */
    @JsonProperty("produce_delay")
    public Double getProduce_delay(){
        return produce_delay ;
    }

    /**
     * 设置 [PRODUCE_DELAY]
     */
    @JsonProperty("produce_delay")
    public void setProduce_delay(Double  produce_delay){
        this.produce_delay = produce_delay ;
        this.produce_delayDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCE_DELAY]脏标记
     */
    @JsonIgnore
    public boolean getProduce_delayDirtyFlag(){
        return produce_delayDirtyFlag ;
    }

    /**
     * 获取 [IS_SEO_OPTIMIZED]
     */
    @JsonProperty("is_seo_optimized")
    public String getIs_seo_optimized(){
        return is_seo_optimized ;
    }

    /**
     * 设置 [IS_SEO_OPTIMIZED]
     */
    @JsonProperty("is_seo_optimized")
    public void setIs_seo_optimized(String  is_seo_optimized){
        this.is_seo_optimized = is_seo_optimized ;
        this.is_seo_optimizedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_SEO_OPTIMIZED]脏标记
     */
    @JsonIgnore
    public boolean getIs_seo_optimizedDirtyFlag(){
        return is_seo_optimizedDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_URL]
     */
    @JsonProperty("website_url")
    public String getWebsite_url(){
        return website_url ;
    }

    /**
     * 设置 [WEBSITE_URL]
     */
    @JsonProperty("website_url")
    public void setWebsite_url(String  website_url){
        this.website_url = website_url ;
        this.website_urlDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_URL]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_urlDirtyFlag(){
        return website_urlDirtyFlag ;
    }

    /**
     * 获取 [RATING_LAST_FEEDBACK]
     */
    @JsonProperty("rating_last_feedback")
    public String getRating_last_feedback(){
        return rating_last_feedback ;
    }

    /**
     * 设置 [RATING_LAST_FEEDBACK]
     */
    @JsonProperty("rating_last_feedback")
    public void setRating_last_feedback(String  rating_last_feedback){
        this.rating_last_feedback = rating_last_feedback ;
        this.rating_last_feedbackDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_LAST_FEEDBACK]脏标记
     */
    @JsonIgnore
    public boolean getRating_last_feedbackDirtyFlag(){
        return rating_last_feedbackDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_SIZE_Y]
     */
    @JsonProperty("website_size_y")
    public Integer getWebsite_size_y(){
        return website_size_y ;
    }

    /**
     * 设置 [WEBSITE_SIZE_Y]
     */
    @JsonProperty("website_size_y")
    public void setWebsite_size_y(Integer  website_size_y){
        this.website_size_y = website_size_y ;
        this.website_size_yDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_SIZE_Y]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_size_yDirtyFlag(){
        return website_size_yDirtyFlag ;
    }

    /**
     * 获取 [EVENT_OK]
     */
    @JsonProperty("event_ok")
    public String getEvent_ok(){
        return event_ok ;
    }

    /**
     * 设置 [EVENT_OK]
     */
    @JsonProperty("event_ok")
    public void setEvent_ok(String  event_ok){
        this.event_ok = event_ok ;
        this.event_okDirtyFlag = true ;
    }

    /**
     * 获取 [EVENT_OK]脏标记
     */
    @JsonIgnore
    public boolean getEvent_okDirtyFlag(){
        return event_okDirtyFlag ;
    }

    /**
     * 获取 [INVENTORY_AVAILABILITY]
     */
    @JsonProperty("inventory_availability")
    public String getInventory_availability(){
        return inventory_availability ;
    }

    /**
     * 设置 [INVENTORY_AVAILABILITY]
     */
    @JsonProperty("inventory_availability")
    public void setInventory_availability(String  inventory_availability){
        this.inventory_availability = inventory_availability ;
        this.inventory_availabilityDirtyFlag = true ;
    }

    /**
     * 获取 [INVENTORY_AVAILABILITY]脏标记
     */
    @JsonIgnore
    public boolean getInventory_availabilityDirtyFlag(){
        return inventory_availabilityDirtyFlag ;
    }

    /**
     * 获取 [PURCHASE_OK]
     */
    @JsonProperty("purchase_ok")
    public String getPurchase_ok(){
        return purchase_ok ;
    }

    /**
     * 设置 [PURCHASE_OK]
     */
    @JsonProperty("purchase_ok")
    public void setPurchase_ok(String  purchase_ok){
        this.purchase_ok = purchase_ok ;
        this.purchase_okDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASE_OK]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_okDirtyFlag(){
        return purchase_okDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [RATING_LAST_VALUE]
     */
    @JsonProperty("rating_last_value")
    public Double getRating_last_value(){
        return rating_last_value ;
    }

    /**
     * 设置 [RATING_LAST_VALUE]
     */
    @JsonProperty("rating_last_value")
    public void setRating_last_value(Double  rating_last_value){
        this.rating_last_value = rating_last_value ;
        this.rating_last_valueDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_LAST_VALUE]脏标记
     */
    @JsonIgnore
    public boolean getRating_last_valueDirtyFlag(){
        return rating_last_valueDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_TITLE]
     */
    @JsonProperty("website_meta_title")
    public String getWebsite_meta_title(){
        return website_meta_title ;
    }

    /**
     * 设置 [WEBSITE_META_TITLE]
     */
    @JsonProperty("website_meta_title")
    public void setWebsite_meta_title(String  website_meta_title){
        this.website_meta_title = website_meta_title ;
        this.website_meta_titleDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_TITLE]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_titleDirtyFlag(){
        return website_meta_titleDirtyFlag ;
    }

    /**
     * 获取 [RATING_LAST_IMAGE]
     */
    @JsonProperty("rating_last_image")
    public byte[] getRating_last_image(){
        return rating_last_image ;
    }

    /**
     * 设置 [RATING_LAST_IMAGE]
     */
    @JsonProperty("rating_last_image")
    public void setRating_last_image(byte[]  rating_last_image){
        this.rating_last_image = rating_last_image ;
        this.rating_last_imageDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_LAST_IMAGE]脏标记
     */
    @JsonIgnore
    public boolean getRating_last_imageDirtyFlag(){
        return rating_last_imageDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION_PURCHASE]
     */
    @JsonProperty("description_purchase")
    public String getDescription_purchase(){
        return description_purchase ;
    }

    /**
     * 设置 [DESCRIPTION_PURCHASE]
     */
    @JsonProperty("description_purchase")
    public void setDescription_purchase(String  description_purchase){
        this.description_purchase = description_purchase ;
        this.description_purchaseDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION_PURCHASE]脏标记
     */
    @JsonIgnore
    public boolean getDescription_purchaseDirtyFlag(){
        return description_purchaseDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return website_id ;
    }

    /**
     * 设置 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_ID]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return website_idDirtyFlag ;
    }

    /**
     * 获取 [CAN_BE_EXPENSED]
     */
    @JsonProperty("can_be_expensed")
    public String getCan_be_expensed(){
        return can_be_expensed ;
    }

    /**
     * 设置 [CAN_BE_EXPENSED]
     */
    @JsonProperty("can_be_expensed")
    public void setCan_be_expensed(String  can_be_expensed){
        this.can_be_expensed = can_be_expensed ;
        this.can_be_expensedDirtyFlag = true ;
    }

    /**
     * 获取 [CAN_BE_EXPENSED]脏标记
     */
    @JsonIgnore
    public boolean getCan_be_expensedDirtyFlag(){
        return can_be_expensedDirtyFlag ;
    }

    /**
     * 获取 [SALE_LINE_WARN]
     */
    @JsonProperty("sale_line_warn")
    public String getSale_line_warn(){
        return sale_line_warn ;
    }

    /**
     * 设置 [SALE_LINE_WARN]
     */
    @JsonProperty("sale_line_warn")
    public void setSale_line_warn(String  sale_line_warn){
        this.sale_line_warn = sale_line_warn ;
        this.sale_line_warnDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_LINE_WARN]脏标记
     */
    @JsonIgnore
    public boolean getSale_line_warnDirtyFlag(){
        return sale_line_warnDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_SIZE_X]
     */
    @JsonProperty("website_size_x")
    public Integer getWebsite_size_x(){
        return website_size_x ;
    }

    /**
     * 设置 [WEBSITE_SIZE_X]
     */
    @JsonProperty("website_size_x")
    public void setWebsite_size_x(Integer  website_size_x){
        this.website_size_x = website_size_x ;
        this.website_size_xDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_SIZE_X]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_size_xDirtyFlag(){
        return website_size_xDirtyFlag ;
    }

    /**
     * 获取 [SERVICE_TO_PURCHASE]
     */
    @JsonProperty("service_to_purchase")
    public String getService_to_purchase(){
        return service_to_purchase ;
    }

    /**
     * 设置 [SERVICE_TO_PURCHASE]
     */
    @JsonProperty("service_to_purchase")
    public void setService_to_purchase(String  service_to_purchase){
        this.service_to_purchase = service_to_purchase ;
        this.service_to_purchaseDirtyFlag = true ;
    }

    /**
     * 获取 [SERVICE_TO_PURCHASE]脏标记
     */
    @JsonIgnore
    public boolean getService_to_purchaseDirtyFlag(){
        return service_to_purchaseDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_SEQUENCE]
     */
    @JsonProperty("website_sequence")
    public Integer getWebsite_sequence(){
        return website_sequence ;
    }

    /**
     * 设置 [WEBSITE_SEQUENCE]
     */
    @JsonProperty("website_sequence")
    public void setWebsite_sequence(Integer  website_sequence){
        this.website_sequence = website_sequence ;
        this.website_sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_sequenceDirtyFlag(){
        return website_sequenceDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_STOCK_INVENTORY]
     */
    @JsonProperty("property_stock_inventory")
    public Integer getProperty_stock_inventory(){
        return property_stock_inventory ;
    }

    /**
     * 设置 [PROPERTY_STOCK_INVENTORY]
     */
    @JsonProperty("property_stock_inventory")
    public void setProperty_stock_inventory(Integer  property_stock_inventory){
        this.property_stock_inventory = property_stock_inventory ;
        this.property_stock_inventoryDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_STOCK_INVENTORY]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_inventoryDirtyFlag(){
        return property_stock_inventoryDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_ID]
     */
    @JsonProperty("location_id")
    public Integer getLocation_id(){
        return location_id ;
    }

    /**
     * 设置 [LOCATION_ID]
     */
    @JsonProperty("location_id")
    public void setLocation_id(Integer  location_id){
        this.location_id = location_id ;
        this.location_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_ID]脏标记
     */
    @JsonIgnore
    public boolean getLocation_idDirtyFlag(){
        return location_idDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_VALUATION]
     */
    @JsonProperty("property_valuation")
    public String getProperty_valuation(){
        return property_valuation ;
    }

    /**
     * 设置 [PROPERTY_VALUATION]
     */
    @JsonProperty("property_valuation")
    public void setProperty_valuation(String  property_valuation){
        this.property_valuation = property_valuation ;
        this.property_valuationDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_VALUATION]脏标记
     */
    @JsonIgnore
    public boolean getProperty_valuationDirtyFlag(){
        return property_valuationDirtyFlag ;
    }

    /**
     * 获取 [IS_PUBLISHED]
     */
    @JsonProperty("is_published")
    public String getIs_published(){
        return is_published ;
    }

    /**
     * 设置 [IS_PUBLISHED]
     */
    @JsonProperty("is_published")
    public void setIs_published(String  is_published){
        this.is_published = is_published ;
        this.is_publishedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_PUBLISHED]脏标记
     */
    @JsonIgnore
    public boolean getIs_publishedDirtyFlag(){
        return is_publishedDirtyFlag ;
    }

    /**
     * 获取 [EXPENSE_POLICY]
     */
    @JsonProperty("expense_policy")
    public String getExpense_policy(){
        return expense_policy ;
    }

    /**
     * 设置 [EXPENSE_POLICY]
     */
    @JsonProperty("expense_policy")
    public void setExpense_policy(String  expense_policy){
        this.expense_policy = expense_policy ;
        this.expense_policyDirtyFlag = true ;
    }

    /**
     * 获取 [EXPENSE_POLICY]脏标记
     */
    @JsonIgnore
    public boolean getExpense_policyDirtyFlag(){
        return expense_policyDirtyFlag ;
    }

    /**
     * 获取 [WEIGHT_UOM_ID]
     */
    @JsonProperty("weight_uom_id")
    public Integer getWeight_uom_id(){
        return weight_uom_id ;
    }

    /**
     * 设置 [WEIGHT_UOM_ID]
     */
    @JsonProperty("weight_uom_id")
    public void setWeight_uom_id(Integer  weight_uom_id){
        this.weight_uom_id = weight_uom_id ;
        this.weight_uom_idDirtyFlag = true ;
    }

    /**
     * 获取 [WEIGHT_UOM_ID]脏标记
     */
    @JsonIgnore
    public boolean getWeight_uom_idDirtyFlag(){
        return weight_uom_idDirtyFlag ;
    }

    /**
     * 获取 [COLOR]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return color ;
    }

    /**
     * 设置 [COLOR]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [COLOR]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return colorDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_STOCK_PRODUCTION]
     */
    @JsonProperty("property_stock_production")
    public Integer getProperty_stock_production(){
        return property_stock_production ;
    }

    /**
     * 设置 [PROPERTY_STOCK_PRODUCTION]
     */
    @JsonProperty("property_stock_production")
    public void setProperty_stock_production(Integer  property_stock_production){
        this.property_stock_production = property_stock_production ;
        this.property_stock_productionDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_STOCK_PRODUCTION]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_productionDirtyFlag(){
        return property_stock_productionDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_PUBLISHED]
     */
    @JsonProperty("website_published")
    public String getWebsite_published(){
        return website_published ;
    }

    /**
     * 设置 [WEBSITE_PUBLISHED]
     */
    @JsonProperty("website_published")
    public void setWebsite_published(String  website_published){
        this.website_published = website_published ;
        this.website_publishedDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_PUBLISHED]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_publishedDirtyFlag(){
        return website_publishedDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_KEYWORDS]
     */
    @JsonProperty("website_meta_keywords")
    public String getWebsite_meta_keywords(){
        return website_meta_keywords ;
    }

    /**
     * 设置 [WEBSITE_META_KEYWORDS]
     */
    @JsonProperty("website_meta_keywords")
    public void setWebsite_meta_keywords(String  website_meta_keywords){
        this.website_meta_keywords = website_meta_keywords ;
        this.website_meta_keywordsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_KEYWORDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_keywordsDirtyFlag(){
        return website_meta_keywordsDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION_PICKINGOUT]
     */
    @JsonProperty("description_pickingout")
    public String getDescription_pickingout(){
        return description_pickingout ;
    }

    /**
     * 设置 [DESCRIPTION_PICKINGOUT]
     */
    @JsonProperty("description_pickingout")
    public void setDescription_pickingout(String  description_pickingout){
        this.description_pickingout = description_pickingout ;
        this.description_pickingoutDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION_PICKINGOUT]脏标记
     */
    @JsonIgnore
    public boolean getDescription_pickingoutDirtyFlag(){
        return description_pickingoutDirtyFlag ;
    }

    /**
     * 获取 [PRICELIST_ID]
     */
    @JsonProperty("pricelist_id")
    public Integer getPricelist_id(){
        return pricelist_id ;
    }

    /**
     * 设置 [PRICELIST_ID]
     */
    @JsonProperty("pricelist_id")
    public void setPricelist_id(Integer  pricelist_id){
        this.pricelist_id = pricelist_id ;
        this.pricelist_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRICELIST_ID]脏标记
     */
    @JsonIgnore
    public boolean getPricelist_idDirtyFlag(){
        return pricelist_idDirtyFlag ;
    }

    /**
     * 获取 [RATING_COUNT]
     */
    @JsonProperty("rating_count")
    public Integer getRating_count(){
        return rating_count ;
    }

    /**
     * 设置 [RATING_COUNT]
     */
    @JsonProperty("rating_count")
    public void setRating_count(Integer  rating_count){
        this.rating_count = rating_count ;
        this.rating_countDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getRating_countDirtyFlag(){
        return rating_countDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_DESCRIPTION]
     */
    @JsonProperty("website_meta_description")
    public String getWebsite_meta_description(){
        return website_meta_description ;
    }

    /**
     * 设置 [WEBSITE_META_DESCRIPTION]
     */
    @JsonProperty("website_meta_description")
    public void setWebsite_meta_description(String  website_meta_description){
        this.website_meta_description = website_meta_description ;
        this.website_meta_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_descriptionDirtyFlag(){
        return website_meta_descriptionDirtyFlag ;
    }

    /**
     * 获取 [VALUATION]
     */
    @JsonProperty("valuation")
    public String getValuation(){
        return valuation ;
    }

    /**
     * 设置 [VALUATION]
     */
    @JsonProperty("valuation")
    public void setValuation(String  valuation){
        this.valuation = valuation ;
        this.valuationDirtyFlag = true ;
    }

    /**
     * 获取 [VALUATION]脏标记
     */
    @JsonIgnore
    public boolean getValuationDirtyFlag(){
        return valuationDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_POLICY]
     */
    @JsonProperty("invoice_policy")
    public String getInvoice_policy(){
        return invoice_policy ;
    }

    /**
     * 设置 [INVOICE_POLICY]
     */
    @JsonProperty("invoice_policy")
    public void setInvoice_policy(String  invoice_policy){
        this.invoice_policy = invoice_policy ;
        this.invoice_policyDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_POLICY]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_policyDirtyFlag(){
        return invoice_policyDirtyFlag ;
    }

    /**
     * 获取 [PURCHASE_LINE_WARN_MSG]
     */
    @JsonProperty("purchase_line_warn_msg")
    public String getPurchase_line_warn_msg(){
        return purchase_line_warn_msg ;
    }

    /**
     * 设置 [PURCHASE_LINE_WARN_MSG]
     */
    @JsonProperty("purchase_line_warn_msg")
    public void setPurchase_line_warn_msg(String  purchase_line_warn_msg){
        this.purchase_line_warn_msg = purchase_line_warn_msg ;
        this.purchase_line_warn_msgDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASE_LINE_WARN_MSG]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_line_warn_msgDirtyFlag(){
        return purchase_line_warn_msgDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_INCOME_ID]
     */
    @JsonProperty("property_account_income_id")
    public Integer getProperty_account_income_id(){
        return property_account_income_id ;
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_INCOME_ID]
     */
    @JsonProperty("property_account_income_id")
    public void setProperty_account_income_id(Integer  property_account_income_id){
        this.property_account_income_id = property_account_income_id ;
        this.property_account_income_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_INCOME_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_income_idDirtyFlag(){
        return property_account_income_idDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_COST_METHOD]
     */
    @JsonProperty("property_cost_method")
    public String getProperty_cost_method(){
        return property_cost_method ;
    }

    /**
     * 设置 [PROPERTY_COST_METHOD]
     */
    @JsonProperty("property_cost_method")
    public void setProperty_cost_method(String  property_cost_method){
        this.property_cost_method = property_cost_method ;
        this.property_cost_methodDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_COST_METHOD]脏标记
     */
    @JsonIgnore
    public boolean getProperty_cost_methodDirtyFlag(){
        return property_cost_methodDirtyFlag ;
    }

    /**
     * 获取 [CATEG_ID]
     */
    @JsonProperty("categ_id")
    public Integer getCateg_id(){
        return categ_id ;
    }

    /**
     * 设置 [CATEG_ID]
     */
    @JsonProperty("categ_id")
    public void setCateg_id(Integer  categ_id){
        this.categ_id = categ_id ;
        this.categ_idDirtyFlag = true ;
    }

    /**
     * 获取 [CATEG_ID]脏标记
     */
    @JsonIgnore
    public boolean getCateg_idDirtyFlag(){
        return categ_idDirtyFlag ;
    }

    /**
     * 获取 [ISPARTS]
     */
    @JsonProperty("isparts")
    public String getIsParts(){
        return isParts ;
    }

    /**
     * 设置 [ISPARTS]
     */
    @JsonProperty("isparts")
    public void setIsParts(String  isParts){
        this.isParts = isParts ;
        this.isPartsDirtyFlag = true ;
    }

    /**
     * 获取 [ISPARTS]脏标记
     */
    @JsonIgnore
    public boolean getIsPartsDirtyFlag(){
        return isPartsDirtyFlag ;
    }

    /**
     * 获取 [UOM_ID]
     */
    @JsonProperty("uom_id")
    public Integer getUom_id(){
        return uom_id ;
    }

    /**
     * 设置 [UOM_ID]
     */
    @JsonProperty("uom_id")
    public void setUom_id(Integer  uom_id){
        this.uom_id = uom_id ;
        this.uom_idDirtyFlag = true ;
    }

    /**
     * 获取 [UOM_ID]脏标记
     */
    @JsonIgnore
    public boolean getUom_idDirtyFlag(){
        return uom_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_VARIANT_ID]
     */
    @JsonProperty("product_variant_id")
    public Integer getProduct_variant_id(){
        return product_variant_id ;
    }

    /**
     * 设置 [PRODUCT_VARIANT_ID]
     */
    @JsonProperty("product_variant_id")
    public void setProduct_variant_id(Integer  product_variant_id){
        this.product_variant_id = product_variant_id ;
        this.product_variant_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_VARIANT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_variant_idDirtyFlag(){
        return product_variant_idDirtyFlag ;
    }

    /**
     * 获取 [TYPE]
     */
    @JsonProperty("type")
    public String getType(){
        return type ;
    }

    /**
     * 设置 [TYPE]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

    /**
     * 获取 [TYPE]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return typeDirtyFlag ;
    }

    /**
     * 获取 [PURCHASE_METHOD]
     */
    @JsonProperty("purchase_method")
    public String getPurchase_method(){
        return purchase_method ;
    }

    /**
     * 设置 [PURCHASE_METHOD]
     */
    @JsonProperty("purchase_method")
    public void setPurchase_method(String  purchase_method){
        this.purchase_method = purchase_method ;
        this.purchase_methodDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASE_METHOD]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_methodDirtyFlag(){
        return purchase_methodDirtyFlag ;
    }

    /**
     * 获取 [RESPONSIBLE_ID]
     */
    @JsonProperty("responsible_id")
    public Integer getResponsible_id(){
        return responsible_id ;
    }

    /**
     * 设置 [RESPONSIBLE_ID]
     */
    @JsonProperty("responsible_id")
    public void setResponsible_id(Integer  responsible_id){
        this.responsible_id = responsible_id ;
        this.responsible_idDirtyFlag = true ;
    }

    /**
     * 获取 [RESPONSIBLE_ID]脏标记
     */
    @JsonIgnore
    public boolean getResponsible_idDirtyFlag(){
        return responsible_idDirtyFlag ;
    }

    /**
     * 获取 [SERVICE_TYPE]
     */
    @JsonProperty("service_type")
    public String getService_type(){
        return service_type ;
    }

    /**
     * 设置 [SERVICE_TYPE]
     */
    @JsonProperty("service_type")
    public void setService_type(String  service_type){
        this.service_type = service_type ;
        this.service_typeDirtyFlag = true ;
    }

    /**
     * 获取 [SERVICE_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getService_typeDirtyFlag(){
        return service_typeDirtyFlag ;
    }

    /**
     * 获取 [UOM_NAME]
     */
    @JsonProperty("uom_name")
    public String getUom_name(){
        return uom_name ;
    }

    /**
     * 设置 [UOM_NAME]
     */
    @JsonProperty("uom_name")
    public void setUom_name(String  uom_name){
        this.uom_name = uom_name ;
        this.uom_nameDirtyFlag = true ;
    }

    /**
     * 获取 [UOM_NAME]脏标记
     */
    @JsonIgnore
    public boolean getUom_nameDirtyFlag(){
        return uom_nameDirtyFlag ;
    }

    /**
     * 获取 [AVAILABLE_THRESHOLD]
     */
    @JsonProperty("available_threshold")
    public Double getAvailable_threshold(){
        return available_threshold ;
    }

    /**
     * 设置 [AVAILABLE_THRESHOLD]
     */
    @JsonProperty("available_threshold")
    public void setAvailable_threshold(Double  available_threshold){
        this.available_threshold = available_threshold ;
        this.available_thresholdDirtyFlag = true ;
    }

    /**
     * 获取 [AVAILABLE_THRESHOLD]脏标记
     */
    @JsonIgnore
    public boolean getAvailable_thresholdDirtyFlag(){
        return available_thresholdDirtyFlag ;
    }

    /**
     * 获取 [PURCHASE_LINE_WARN]
     */
    @JsonProperty("purchase_line_warn")
    public String getPurchase_line_warn(){
        return purchase_line_warn ;
    }

    /**
     * 设置 [PURCHASE_LINE_WARN]
     */
    @JsonProperty("purchase_line_warn")
    public void setPurchase_line_warn(String  purchase_line_warn){
        this.purchase_line_warn = purchase_line_warn ;
        this.purchase_line_warnDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASE_LINE_WARN]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_line_warnDirtyFlag(){
        return purchase_line_warnDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_VARIANT_COUNT]
     */
    @JsonProperty("product_variant_count")
    public Integer getProduct_variant_count(){
        return product_variant_count ;
    }

    /**
     * 设置 [PRODUCT_VARIANT_COUNT]
     */
    @JsonProperty("product_variant_count")
    public void setProduct_variant_count(Integer  product_variant_count){
        this.product_variant_count = product_variant_count ;
        this.product_variant_countDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_VARIANT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_variant_countDirtyFlag(){
        return product_variant_countDirtyFlag ;
    }

    /**
     * 获取 [POS_CATEG_ID]
     */
    @JsonProperty("pos_categ_id")
    public Integer getPos_categ_id(){
        return pos_categ_id ;
    }

    /**
     * 设置 [POS_CATEG_ID]
     */
    @JsonProperty("pos_categ_id")
    public void setPos_categ_id(Integer  pos_categ_id){
        this.pos_categ_id = pos_categ_id ;
        this.pos_categ_idDirtyFlag = true ;
    }

    /**
     * 获取 [POS_CATEG_ID]脏标记
     */
    @JsonIgnore
    public boolean getPos_categ_idDirtyFlag(){
        return pos_categ_idDirtyFlag ;
    }

    /**
     * 获取 [CUSTOM_MESSAGE]
     */
    @JsonProperty("custom_message")
    public String getCustom_message(){
        return custom_message ;
    }

    /**
     * 设置 [CUSTOM_MESSAGE]
     */
    @JsonProperty("custom_message")
    public void setCustom_message(String  custom_message){
        this.custom_message = custom_message ;
        this.custom_messageDirtyFlag = true ;
    }

    /**
     * 获取 [CUSTOM_MESSAGE]脏标记
     */
    @JsonIgnore
    public boolean getCustom_messageDirtyFlag(){
        return custom_messageDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_EXPENSE_ID]
     */
    @JsonProperty("property_account_expense_id")
    public Integer getProperty_account_expense_id(){
        return property_account_expense_id ;
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_EXPENSE_ID]
     */
    @JsonProperty("property_account_expense_id")
    public void setProperty_account_expense_id(Integer  property_account_expense_id){
        this.property_account_expense_id = property_account_expense_id ;
        this.property_account_expense_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_EXPENSE_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_expense_idDirtyFlag(){
        return property_account_expense_idDirtyFlag ;
    }

    /**
     * 获取 [SALE_DELAY]
     */
    @JsonProperty("sale_delay")
    public Double getSale_delay(){
        return sale_delay ;
    }

    /**
     * 设置 [SALE_DELAY]
     */
    @JsonProperty("sale_delay")
    public void setSale_delay(Double  sale_delay){
        this.sale_delay = sale_delay ;
        this.sale_delayDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_DELAY]脏标记
     */
    @JsonIgnore
    public boolean getSale_delayDirtyFlag(){
        return sale_delayDirtyFlag ;
    }

    /**
     * 获取 [UOM_PO_ID]
     */
    @JsonProperty("uom_po_id")
    public Integer getUom_po_id(){
        return uom_po_id ;
    }

    /**
     * 设置 [UOM_PO_ID]
     */
    @JsonProperty("uom_po_id")
    public void setUom_po_id(Integer  uom_po_id){
        this.uom_po_id = uom_po_id ;
        this.uom_po_idDirtyFlag = true ;
    }

    /**
     * 获取 [UOM_PO_ID]脏标记
     */
    @JsonIgnore
    public boolean getUom_po_idDirtyFlag(){
        return uom_po_idDirtyFlag ;
    }

    /**
     * 获取 [AVAILABLE_IN_POS]
     */
    @JsonProperty("available_in_pos")
    public String getAvailable_in_pos(){
        return available_in_pos ;
    }

    /**
     * 设置 [AVAILABLE_IN_POS]
     */
    @JsonProperty("available_in_pos")
    public void setAvailable_in_pos(String  available_in_pos){
        this.available_in_pos = available_in_pos ;
        this.available_in_posDirtyFlag = true ;
    }

    /**
     * 获取 [AVAILABLE_IN_POS]脏标记
     */
    @JsonIgnore
    public boolean getAvailable_in_posDirtyFlag(){
        return available_in_posDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID]
     */
    @JsonProperty("product_tmpl_id")
    public Integer getProduct_tmpl_id(){
        return product_tmpl_id ;
    }

    /**
     * 设置 [PRODUCT_TMPL_ID]
     */
    @JsonProperty("product_tmpl_id")
    public void setProduct_tmpl_id(Integer  product_tmpl_id){
        this.product_tmpl_id = product_tmpl_id ;
        this.product_tmpl_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_idDirtyFlag(){
        return product_tmpl_idDirtyFlag ;
    }



    public Product_product toDO() {
        Product_product srfdomain = new Product_product();
        if(getVariant_seller_idsDirtyFlag())
            srfdomain.setVariant_seller_ids(variant_seller_ids);
        if(getProduct_template_attribute_value_idsDirtyFlag())
            srfdomain.setProduct_template_attribute_value_ids(product_template_attribute_value_ids);
        if(getProduct_variant_idsDirtyFlag())
            srfdomain.setProduct_variant_ids(product_variant_ids);
        if(getImage_smallDirtyFlag())
            srfdomain.setImage_small(image_small);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getVolumeDirtyFlag())
            srfdomain.setVolume(volume);
        if(getLst_priceDirtyFlag())
            srfdomain.setLst_price(lst_price);
        if(getValid_product_attribute_idsDirtyFlag())
            srfdomain.setValid_product_attribute_ids(valid_product_attribute_ids);
        if(getStock_fifo_manual_move_idsDirtyFlag())
            srfdomain.setStock_fifo_manual_move_ids(stock_fifo_manual_move_ids);
        if(getStock_quant_idsDirtyFlag())
            srfdomain.setStock_quant_ids(stock_quant_ids);
        if(getSupplier_taxes_idDirtyFlag())
            srfdomain.setSupplier_taxes_id(supplier_taxes_id);
        if(getPricelist_item_idsDirtyFlag())
            srfdomain.setPricelist_item_ids(pricelist_item_ids);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getAccessory_product_idsDirtyFlag())
            srfdomain.setAccessory_product_ids(accessory_product_ids);
        if(getSeller_idsDirtyFlag())
            srfdomain.setSeller_ids(seller_ids);
        if(getValid_product_attribute_value_wnva_idsDirtyFlag())
            srfdomain.setValid_product_attribute_value_wnva_ids(valid_product_attribute_value_wnva_ids);
        if(getPartner_refDirtyFlag())
            srfdomain.setPartner_ref(partner_ref);
        if(getProduct_image_idsDirtyFlag())
            srfdomain.setProduct_image_ids(product_image_ids);
        if(getMrp_product_qtyDirtyFlag())
            srfdomain.setMrp_product_qty(mrp_product_qty);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getValid_product_template_attribute_line_idsDirtyFlag())
            srfdomain.setValid_product_template_attribute_line_ids(valid_product_template_attribute_line_ids);
        if(getActivity_type_idDirtyFlag())
            srfdomain.setActivity_type_id(activity_type_id);
        if(getPublic_categ_idsDirtyFlag())
            srfdomain.setPublic_categ_ids(public_categ_ids);
        if(getEvent_ticket_idsDirtyFlag())
            srfdomain.setEvent_ticket_ids(event_ticket_ids);
        if(getPriceDirtyFlag())
            srfdomain.setPrice(price);
        if(getAttribute_line_idsDirtyFlag())
            srfdomain.setAttribute_line_ids(attribute_line_ids);
        if(getVirtual_availableDirtyFlag())
            srfdomain.setVirtual_available(virtual_available);
        if(getNbr_reordering_rulesDirtyFlag())
            srfdomain.setNbr_reordering_rules(nbr_reordering_rules);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getWebsite_price_differenceDirtyFlag())
            srfdomain.setWebsite_price_difference(website_price_difference);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getCart_qtyDirtyFlag())
            srfdomain.setCart_qty(cart_qty);
        if(getWebsite_public_priceDirtyFlag())
            srfdomain.setWebsite_public_price(website_public_price);
        if(getRating_idsDirtyFlag())
            srfdomain.setRating_ids(rating_ids);
        if(getBom_line_idsDirtyFlag())
            srfdomain.setBom_line_ids(bom_line_ids);
        if(getWebsite_priceDirtyFlag())
            srfdomain.setWebsite_price(website_price);
        if(getOutgoing_qtyDirtyFlag())
            srfdomain.setOutgoing_qty(outgoing_qty);
        if(getSales_countDirtyFlag())
            srfdomain.setSales_count(sales_count);
        if(getValid_product_attribute_wnva_idsDirtyFlag())
            srfdomain.setValid_product_attribute_wnva_ids(valid_product_attribute_wnva_ids);
        if(getImage_mediumDirtyFlag())
            srfdomain.setImage_medium(image_medium);
        if(getValid_existing_variant_idsDirtyFlag())
            srfdomain.setValid_existing_variant_ids(valid_existing_variant_ids);
        if(getStock_value_currency_idDirtyFlag())
            srfdomain.setStock_value_currency_id(stock_value_currency_id);
        if(getStock_valueDirtyFlag())
            srfdomain.setStock_value(stock_value);
        if(getWebsite_style_idsDirtyFlag())
            srfdomain.setWebsite_style_ids(website_style_ids);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getWeightDirtyFlag())
            srfdomain.setWeight(weight);
        if(getBom_idsDirtyFlag())
            srfdomain.setBom_ids(bom_ids);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getActivity_stateDirtyFlag())
            srfdomain.setActivity_state(activity_state);
        if(getValid_product_template_attribute_line_wnva_idsDirtyFlag())
            srfdomain.setValid_product_template_attribute_line_wnva_ids(valid_product_template_attribute_line_wnva_ids);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getValid_product_attribute_value_idsDirtyFlag())
            srfdomain.setValid_product_attribute_value_ids(valid_product_attribute_value_ids);
        if(getQty_availableDirtyFlag())
            srfdomain.setQty_available(qty_available);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getImage_variantDirtyFlag())
            srfdomain.setImage_variant(image_variant);
        if(getStock_move_idsDirtyFlag())
            srfdomain.setStock_move_ids(stock_move_ids);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getStock_fifo_real_time_aml_idsDirtyFlag())
            srfdomain.setStock_fifo_real_time_aml_ids(stock_fifo_real_time_aml_ids);
        if(getActivity_date_deadlineDirtyFlag())
            srfdomain.setActivity_date_deadline(activity_date_deadline);
        if(getCodeDirtyFlag())
            srfdomain.setCode(code);
        if(getReordering_min_qtyDirtyFlag())
            srfdomain.setReordering_min_qty(reordering_min_qty);
        if(getImageDirtyFlag())
            srfdomain.setImage(image);
        if(getRoute_idsDirtyFlag())
            srfdomain.setRoute_ids(route_ids);
        if(getTaxes_idDirtyFlag())
            srfdomain.setTaxes_id(taxes_id);
        if(getBom_countDirtyFlag())
            srfdomain.setBom_count(bom_count);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getPackaging_idsDirtyFlag())
            srfdomain.setPackaging_ids(packaging_ids);
        if(getValid_archived_variant_idsDirtyFlag())
            srfdomain.setValid_archived_variant_ids(valid_archived_variant_ids);
        if(getActivity_user_idDirtyFlag())
            srfdomain.setActivity_user_id(activity_user_id);
        if(getItem_idsDirtyFlag())
            srfdomain.setItem_ids(item_ids);
        if(getPurchased_product_qtyDirtyFlag())
            srfdomain.setPurchased_product_qty(purchased_product_qty);
        if(getReordering_max_qtyDirtyFlag())
            srfdomain.setReordering_max_qty(reordering_max_qty);
        if(getOrderpoint_idsDirtyFlag())
            srfdomain.setOrderpoint_ids(orderpoint_ids);
        if(getOptional_product_idsDirtyFlag())
            srfdomain.setOptional_product_ids(optional_product_ids);
        if(getIs_product_variantDirtyFlag())
            srfdomain.setIs_product_variant(is_product_variant);
        if(getUsed_in_bom_countDirtyFlag())
            srfdomain.setUsed_in_bom_count(used_in_bom_count);
        if(getQty_at_dateDirtyFlag())
            srfdomain.setQty_at_date(qty_at_date);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getActivity_idsDirtyFlag())
            srfdomain.setActivity_ids(activity_ids);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getBarcodeDirtyFlag())
            srfdomain.setBarcode(barcode);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getStandard_priceDirtyFlag())
            srfdomain.setStandard_price(standard_price);
        if(getAttribute_value_idsDirtyFlag())
            srfdomain.setAttribute_value_ids(attribute_value_ids);
        if(getPrice_extraDirtyFlag())
            srfdomain.setPrice_extra(price_extra);
        if(getVariant_bom_idsDirtyFlag())
            srfdomain.setVariant_bom_ids(variant_bom_ids);
        if(getAlternative_product_idsDirtyFlag())
            srfdomain.setAlternative_product_ids(alternative_product_ids);
        if(getDefault_codeDirtyFlag())
            srfdomain.setDefault_code(default_code);
        if(getRoute_from_categ_idsDirtyFlag())
            srfdomain.setRoute_from_categ_ids(route_from_categ_ids);
        if(getActivity_summaryDirtyFlag())
            srfdomain.setActivity_summary(activity_summary);
        if(getIncoming_qtyDirtyFlag())
            srfdomain.setIncoming_qty(incoming_qty);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getTrackingDirtyFlag())
            srfdomain.setTracking(tracking);
        if(getDescription_pickingDirtyFlag())
            srfdomain.setDescription_picking(description_picking);
        if(getProperty_stock_account_outputDirtyFlag())
            srfdomain.setProperty_stock_account_output(property_stock_account_output);
        if(getSale_okDirtyFlag())
            srfdomain.setSale_ok(sale_ok);
        if(getWebsite_descriptionDirtyFlag())
            srfdomain.setWebsite_description(website_description);
        if(getWebsite_meta_og_imgDirtyFlag())
            srfdomain.setWebsite_meta_og_img(website_meta_og_img);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getTo_weightDirtyFlag())
            srfdomain.setTo_weight(to_weight);
        if(getDescriptionDirtyFlag())
            srfdomain.setDescription(description);
        if(getDescription_pickinginDirtyFlag())
            srfdomain.setDescription_pickingin(description_pickingin);
        if(getList_priceDirtyFlag())
            srfdomain.setList_price(list_price);
        if(getHide_expense_policyDirtyFlag())
            srfdomain.setHide_expense_policy(hide_expense_policy);
        if(getDescription_saleDirtyFlag())
            srfdomain.setDescription_sale(description_sale);
        if(getCost_methodDirtyFlag())
            srfdomain.setCost_method(cost_method);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getSale_line_warn_msgDirtyFlag())
            srfdomain.setSale_line_warn_msg(sale_line_warn_msg);
        if(getWarehouse_idDirtyFlag())
            srfdomain.setWarehouse_id(warehouse_id);
        if(getRentalDirtyFlag())
            srfdomain.setRental(rental);
        if(getProperty_account_creditor_price_differenceDirtyFlag())
            srfdomain.setProperty_account_creditor_price_difference(property_account_creditor_price_difference);
        if(getWeight_uom_nameDirtyFlag())
            srfdomain.setWeight_uom_name(weight_uom_name);
        if(getCost_currency_idDirtyFlag())
            srfdomain.setCost_currency_id(cost_currency_id);
        if(getProperty_stock_account_inputDirtyFlag())
            srfdomain.setProperty_stock_account_input(property_stock_account_input);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getProduce_delayDirtyFlag())
            srfdomain.setProduce_delay(produce_delay);
        if(getIs_seo_optimizedDirtyFlag())
            srfdomain.setIs_seo_optimized(is_seo_optimized);
        if(getWebsite_urlDirtyFlag())
            srfdomain.setWebsite_url(website_url);
        if(getRating_last_feedbackDirtyFlag())
            srfdomain.setRating_last_feedback(rating_last_feedback);
        if(getWebsite_size_yDirtyFlag())
            srfdomain.setWebsite_size_y(website_size_y);
        if(getEvent_okDirtyFlag())
            srfdomain.setEvent_ok(event_ok);
        if(getInventory_availabilityDirtyFlag())
            srfdomain.setInventory_availability(inventory_availability);
        if(getPurchase_okDirtyFlag())
            srfdomain.setPurchase_ok(purchase_ok);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getRating_last_valueDirtyFlag())
            srfdomain.setRating_last_value(rating_last_value);
        if(getWebsite_meta_titleDirtyFlag())
            srfdomain.setWebsite_meta_title(website_meta_title);
        if(getRating_last_imageDirtyFlag())
            srfdomain.setRating_last_image(rating_last_image);
        if(getDescription_purchaseDirtyFlag())
            srfdomain.setDescription_purchase(description_purchase);
        if(getWebsite_idDirtyFlag())
            srfdomain.setWebsite_id(website_id);
        if(getCan_be_expensedDirtyFlag())
            srfdomain.setCan_be_expensed(can_be_expensed);
        if(getSale_line_warnDirtyFlag())
            srfdomain.setSale_line_warn(sale_line_warn);
        if(getWebsite_size_xDirtyFlag())
            srfdomain.setWebsite_size_x(website_size_x);
        if(getService_to_purchaseDirtyFlag())
            srfdomain.setService_to_purchase(service_to_purchase);
        if(getWebsite_sequenceDirtyFlag())
            srfdomain.setWebsite_sequence(website_sequence);
        if(getProperty_stock_inventoryDirtyFlag())
            srfdomain.setProperty_stock_inventory(property_stock_inventory);
        if(getLocation_idDirtyFlag())
            srfdomain.setLocation_id(location_id);
        if(getProperty_valuationDirtyFlag())
            srfdomain.setProperty_valuation(property_valuation);
        if(getIs_publishedDirtyFlag())
            srfdomain.setIs_published(is_published);
        if(getExpense_policyDirtyFlag())
            srfdomain.setExpense_policy(expense_policy);
        if(getWeight_uom_idDirtyFlag())
            srfdomain.setWeight_uom_id(weight_uom_id);
        if(getColorDirtyFlag())
            srfdomain.setColor(color);
        if(getProperty_stock_productionDirtyFlag())
            srfdomain.setProperty_stock_production(property_stock_production);
        if(getWebsite_publishedDirtyFlag())
            srfdomain.setWebsite_published(website_published);
        if(getWebsite_meta_keywordsDirtyFlag())
            srfdomain.setWebsite_meta_keywords(website_meta_keywords);
        if(getDescription_pickingoutDirtyFlag())
            srfdomain.setDescription_pickingout(description_pickingout);
        if(getPricelist_idDirtyFlag())
            srfdomain.setPricelist_id(pricelist_id);
        if(getRating_countDirtyFlag())
            srfdomain.setRating_count(rating_count);
        if(getWebsite_meta_descriptionDirtyFlag())
            srfdomain.setWebsite_meta_description(website_meta_description);
        if(getValuationDirtyFlag())
            srfdomain.setValuation(valuation);
        if(getInvoice_policyDirtyFlag())
            srfdomain.setInvoice_policy(invoice_policy);
        if(getPurchase_line_warn_msgDirtyFlag())
            srfdomain.setPurchase_line_warn_msg(purchase_line_warn_msg);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getProperty_account_income_idDirtyFlag())
            srfdomain.setProperty_account_income_id(property_account_income_id);
        if(getProperty_cost_methodDirtyFlag())
            srfdomain.setProperty_cost_method(property_cost_method);
        if(getCateg_idDirtyFlag())
            srfdomain.setCateg_id(categ_id);
        if(getIsPartsDirtyFlag())
            srfdomain.setIsParts(isParts);
        if(getUom_idDirtyFlag())
            srfdomain.setUom_id(uom_id);
        if(getProduct_variant_idDirtyFlag())
            srfdomain.setProduct_variant_id(product_variant_id);
        if(getTypeDirtyFlag())
            srfdomain.setType(type);
        if(getPurchase_methodDirtyFlag())
            srfdomain.setPurchase_method(purchase_method);
        if(getResponsible_idDirtyFlag())
            srfdomain.setResponsible_id(responsible_id);
        if(getService_typeDirtyFlag())
            srfdomain.setService_type(service_type);
        if(getUom_nameDirtyFlag())
            srfdomain.setUom_name(uom_name);
        if(getAvailable_thresholdDirtyFlag())
            srfdomain.setAvailable_threshold(available_threshold);
        if(getPurchase_line_warnDirtyFlag())
            srfdomain.setPurchase_line_warn(purchase_line_warn);
        if(getProduct_variant_countDirtyFlag())
            srfdomain.setProduct_variant_count(product_variant_count);
        if(getPos_categ_idDirtyFlag())
            srfdomain.setPos_categ_id(pos_categ_id);
        if(getCustom_messageDirtyFlag())
            srfdomain.setCustom_message(custom_message);
        if(getProperty_account_expense_idDirtyFlag())
            srfdomain.setProperty_account_expense_id(property_account_expense_id);
        if(getSale_delayDirtyFlag())
            srfdomain.setSale_delay(sale_delay);
        if(getUom_po_idDirtyFlag())
            srfdomain.setUom_po_id(uom_po_id);
        if(getAvailable_in_posDirtyFlag())
            srfdomain.setAvailable_in_pos(available_in_pos);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getProduct_tmpl_idDirtyFlag())
            srfdomain.setProduct_tmpl_id(product_tmpl_id);

        return srfdomain;
    }

    public void fromDO(Product_product srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getVariant_seller_idsDirtyFlag())
            this.setVariant_seller_ids(srfdomain.getVariant_seller_ids());
        if(srfdomain.getProduct_template_attribute_value_idsDirtyFlag())
            this.setProduct_template_attribute_value_ids(srfdomain.getProduct_template_attribute_value_ids());
        if(srfdomain.getProduct_variant_idsDirtyFlag())
            this.setProduct_variant_ids(srfdomain.getProduct_variant_ids());
        if(srfdomain.getImage_smallDirtyFlag())
            this.setImage_small(srfdomain.getImage_small());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getVolumeDirtyFlag())
            this.setVolume(srfdomain.getVolume());
        if(srfdomain.getLst_priceDirtyFlag())
            this.setLst_price(srfdomain.getLst_price());
        if(srfdomain.getValid_product_attribute_idsDirtyFlag())
            this.setValid_product_attribute_ids(srfdomain.getValid_product_attribute_ids());
        if(srfdomain.getStock_fifo_manual_move_idsDirtyFlag())
            this.setStock_fifo_manual_move_ids(srfdomain.getStock_fifo_manual_move_ids());
        if(srfdomain.getStock_quant_idsDirtyFlag())
            this.setStock_quant_ids(srfdomain.getStock_quant_ids());
        if(srfdomain.getSupplier_taxes_idDirtyFlag())
            this.setSupplier_taxes_id(srfdomain.getSupplier_taxes_id());
        if(srfdomain.getPricelist_item_idsDirtyFlag())
            this.setPricelist_item_ids(srfdomain.getPricelist_item_ids());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getAccessory_product_idsDirtyFlag())
            this.setAccessory_product_ids(srfdomain.getAccessory_product_ids());
        if(srfdomain.getSeller_idsDirtyFlag())
            this.setSeller_ids(srfdomain.getSeller_ids());
        if(srfdomain.getValid_product_attribute_value_wnva_idsDirtyFlag())
            this.setValid_product_attribute_value_wnva_ids(srfdomain.getValid_product_attribute_value_wnva_ids());
        if(srfdomain.getPartner_refDirtyFlag())
            this.setPartner_ref(srfdomain.getPartner_ref());
        if(srfdomain.getProduct_image_idsDirtyFlag())
            this.setProduct_image_ids(srfdomain.getProduct_image_ids());
        if(srfdomain.getMrp_product_qtyDirtyFlag())
            this.setMrp_product_qty(srfdomain.getMrp_product_qty());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getValid_product_template_attribute_line_idsDirtyFlag())
            this.setValid_product_template_attribute_line_ids(srfdomain.getValid_product_template_attribute_line_ids());
        if(srfdomain.getActivity_type_idDirtyFlag())
            this.setActivity_type_id(srfdomain.getActivity_type_id());
        if(srfdomain.getPublic_categ_idsDirtyFlag())
            this.setPublic_categ_ids(srfdomain.getPublic_categ_ids());
        if(srfdomain.getEvent_ticket_idsDirtyFlag())
            this.setEvent_ticket_ids(srfdomain.getEvent_ticket_ids());
        if(srfdomain.getPriceDirtyFlag())
            this.setPrice(srfdomain.getPrice());
        if(srfdomain.getAttribute_line_idsDirtyFlag())
            this.setAttribute_line_ids(srfdomain.getAttribute_line_ids());
        if(srfdomain.getVirtual_availableDirtyFlag())
            this.setVirtual_available(srfdomain.getVirtual_available());
        if(srfdomain.getNbr_reordering_rulesDirtyFlag())
            this.setNbr_reordering_rules(srfdomain.getNbr_reordering_rules());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getWebsite_price_differenceDirtyFlag())
            this.setWebsite_price_difference(srfdomain.getWebsite_price_difference());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getCart_qtyDirtyFlag())
            this.setCart_qty(srfdomain.getCart_qty());
        if(srfdomain.getWebsite_public_priceDirtyFlag())
            this.setWebsite_public_price(srfdomain.getWebsite_public_price());
        if(srfdomain.getRating_idsDirtyFlag())
            this.setRating_ids(srfdomain.getRating_ids());
        if(srfdomain.getBom_line_idsDirtyFlag())
            this.setBom_line_ids(srfdomain.getBom_line_ids());
        if(srfdomain.getWebsite_priceDirtyFlag())
            this.setWebsite_price(srfdomain.getWebsite_price());
        if(srfdomain.getOutgoing_qtyDirtyFlag())
            this.setOutgoing_qty(srfdomain.getOutgoing_qty());
        if(srfdomain.getSales_countDirtyFlag())
            this.setSales_count(srfdomain.getSales_count());
        if(srfdomain.getValid_product_attribute_wnva_idsDirtyFlag())
            this.setValid_product_attribute_wnva_ids(srfdomain.getValid_product_attribute_wnva_ids());
        if(srfdomain.getImage_mediumDirtyFlag())
            this.setImage_medium(srfdomain.getImage_medium());
        if(srfdomain.getValid_existing_variant_idsDirtyFlag())
            this.setValid_existing_variant_ids(srfdomain.getValid_existing_variant_ids());
        if(srfdomain.getStock_value_currency_idDirtyFlag())
            this.setStock_value_currency_id(srfdomain.getStock_value_currency_id());
        if(srfdomain.getStock_valueDirtyFlag())
            this.setStock_value(srfdomain.getStock_value());
        if(srfdomain.getWebsite_style_idsDirtyFlag())
            this.setWebsite_style_ids(srfdomain.getWebsite_style_ids());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getWeightDirtyFlag())
            this.setWeight(srfdomain.getWeight());
        if(srfdomain.getBom_idsDirtyFlag())
            this.setBom_ids(srfdomain.getBom_ids());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getActivity_stateDirtyFlag())
            this.setActivity_state(srfdomain.getActivity_state());
        if(srfdomain.getValid_product_template_attribute_line_wnva_idsDirtyFlag())
            this.setValid_product_template_attribute_line_wnva_ids(srfdomain.getValid_product_template_attribute_line_wnva_ids());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getValid_product_attribute_value_idsDirtyFlag())
            this.setValid_product_attribute_value_ids(srfdomain.getValid_product_attribute_value_ids());
        if(srfdomain.getQty_availableDirtyFlag())
            this.setQty_available(srfdomain.getQty_available());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getImage_variantDirtyFlag())
            this.setImage_variant(srfdomain.getImage_variant());
        if(srfdomain.getStock_move_idsDirtyFlag())
            this.setStock_move_ids(srfdomain.getStock_move_ids());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getStock_fifo_real_time_aml_idsDirtyFlag())
            this.setStock_fifo_real_time_aml_ids(srfdomain.getStock_fifo_real_time_aml_ids());
        if(srfdomain.getActivity_date_deadlineDirtyFlag())
            this.setActivity_date_deadline(srfdomain.getActivity_date_deadline());
        if(srfdomain.getCodeDirtyFlag())
            this.setCode(srfdomain.getCode());
        if(srfdomain.getReordering_min_qtyDirtyFlag())
            this.setReordering_min_qty(srfdomain.getReordering_min_qty());
        if(srfdomain.getImageDirtyFlag())
            this.setImage(srfdomain.getImage());
        if(srfdomain.getRoute_idsDirtyFlag())
            this.setRoute_ids(srfdomain.getRoute_ids());
        if(srfdomain.getTaxes_idDirtyFlag())
            this.setTaxes_id(srfdomain.getTaxes_id());
        if(srfdomain.getBom_countDirtyFlag())
            this.setBom_count(srfdomain.getBom_count());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getPackaging_idsDirtyFlag())
            this.setPackaging_ids(srfdomain.getPackaging_ids());
        if(srfdomain.getValid_archived_variant_idsDirtyFlag())
            this.setValid_archived_variant_ids(srfdomain.getValid_archived_variant_ids());
        if(srfdomain.getActivity_user_idDirtyFlag())
            this.setActivity_user_id(srfdomain.getActivity_user_id());
        if(srfdomain.getItem_idsDirtyFlag())
            this.setItem_ids(srfdomain.getItem_ids());
        if(srfdomain.getPurchased_product_qtyDirtyFlag())
            this.setPurchased_product_qty(srfdomain.getPurchased_product_qty());
        if(srfdomain.getReordering_max_qtyDirtyFlag())
            this.setReordering_max_qty(srfdomain.getReordering_max_qty());
        if(srfdomain.getOrderpoint_idsDirtyFlag())
            this.setOrderpoint_ids(srfdomain.getOrderpoint_ids());
        if(srfdomain.getOptional_product_idsDirtyFlag())
            this.setOptional_product_ids(srfdomain.getOptional_product_ids());
        if(srfdomain.getIs_product_variantDirtyFlag())
            this.setIs_product_variant(srfdomain.getIs_product_variant());
        if(srfdomain.getUsed_in_bom_countDirtyFlag())
            this.setUsed_in_bom_count(srfdomain.getUsed_in_bom_count());
        if(srfdomain.getQty_at_dateDirtyFlag())
            this.setQty_at_date(srfdomain.getQty_at_date());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getActivity_idsDirtyFlag())
            this.setActivity_ids(srfdomain.getActivity_ids());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getBarcodeDirtyFlag())
            this.setBarcode(srfdomain.getBarcode());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getStandard_priceDirtyFlag())
            this.setStandard_price(srfdomain.getStandard_price());
        if(srfdomain.getAttribute_value_idsDirtyFlag())
            this.setAttribute_value_ids(srfdomain.getAttribute_value_ids());
        if(srfdomain.getPrice_extraDirtyFlag())
            this.setPrice_extra(srfdomain.getPrice_extra());
        if(srfdomain.getVariant_bom_idsDirtyFlag())
            this.setVariant_bom_ids(srfdomain.getVariant_bom_ids());
        if(srfdomain.getAlternative_product_idsDirtyFlag())
            this.setAlternative_product_ids(srfdomain.getAlternative_product_ids());
        if(srfdomain.getDefault_codeDirtyFlag())
            this.setDefault_code(srfdomain.getDefault_code());
        if(srfdomain.getRoute_from_categ_idsDirtyFlag())
            this.setRoute_from_categ_ids(srfdomain.getRoute_from_categ_ids());
        if(srfdomain.getActivity_summaryDirtyFlag())
            this.setActivity_summary(srfdomain.getActivity_summary());
        if(srfdomain.getIncoming_qtyDirtyFlag())
            this.setIncoming_qty(srfdomain.getIncoming_qty());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getTrackingDirtyFlag())
            this.setTracking(srfdomain.getTracking());
        if(srfdomain.getDescription_pickingDirtyFlag())
            this.setDescription_picking(srfdomain.getDescription_picking());
        if(srfdomain.getProperty_stock_account_outputDirtyFlag())
            this.setProperty_stock_account_output(srfdomain.getProperty_stock_account_output());
        if(srfdomain.getSale_okDirtyFlag())
            this.setSale_ok(srfdomain.getSale_ok());
        if(srfdomain.getWebsite_descriptionDirtyFlag())
            this.setWebsite_description(srfdomain.getWebsite_description());
        if(srfdomain.getWebsite_meta_og_imgDirtyFlag())
            this.setWebsite_meta_og_img(srfdomain.getWebsite_meta_og_img());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getTo_weightDirtyFlag())
            this.setTo_weight(srfdomain.getTo_weight());
        if(srfdomain.getDescriptionDirtyFlag())
            this.setDescription(srfdomain.getDescription());
        if(srfdomain.getDescription_pickinginDirtyFlag())
            this.setDescription_pickingin(srfdomain.getDescription_pickingin());
        if(srfdomain.getList_priceDirtyFlag())
            this.setList_price(srfdomain.getList_price());
        if(srfdomain.getHide_expense_policyDirtyFlag())
            this.setHide_expense_policy(srfdomain.getHide_expense_policy());
        if(srfdomain.getDescription_saleDirtyFlag())
            this.setDescription_sale(srfdomain.getDescription_sale());
        if(srfdomain.getCost_methodDirtyFlag())
            this.setCost_method(srfdomain.getCost_method());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getSale_line_warn_msgDirtyFlag())
            this.setSale_line_warn_msg(srfdomain.getSale_line_warn_msg());
        if(srfdomain.getWarehouse_idDirtyFlag())
            this.setWarehouse_id(srfdomain.getWarehouse_id());
        if(srfdomain.getRentalDirtyFlag())
            this.setRental(srfdomain.getRental());
        if(srfdomain.getProperty_account_creditor_price_differenceDirtyFlag())
            this.setProperty_account_creditor_price_difference(srfdomain.getProperty_account_creditor_price_difference());
        if(srfdomain.getWeight_uom_nameDirtyFlag())
            this.setWeight_uom_name(srfdomain.getWeight_uom_name());
        if(srfdomain.getCost_currency_idDirtyFlag())
            this.setCost_currency_id(srfdomain.getCost_currency_id());
        if(srfdomain.getProperty_stock_account_inputDirtyFlag())
            this.setProperty_stock_account_input(srfdomain.getProperty_stock_account_input());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getProduce_delayDirtyFlag())
            this.setProduce_delay(srfdomain.getProduce_delay());
        if(srfdomain.getIs_seo_optimizedDirtyFlag())
            this.setIs_seo_optimized(srfdomain.getIs_seo_optimized());
        if(srfdomain.getWebsite_urlDirtyFlag())
            this.setWebsite_url(srfdomain.getWebsite_url());
        if(srfdomain.getRating_last_feedbackDirtyFlag())
            this.setRating_last_feedback(srfdomain.getRating_last_feedback());
        if(srfdomain.getWebsite_size_yDirtyFlag())
            this.setWebsite_size_y(srfdomain.getWebsite_size_y());
        if(srfdomain.getEvent_okDirtyFlag())
            this.setEvent_ok(srfdomain.getEvent_ok());
        if(srfdomain.getInventory_availabilityDirtyFlag())
            this.setInventory_availability(srfdomain.getInventory_availability());
        if(srfdomain.getPurchase_okDirtyFlag())
            this.setPurchase_ok(srfdomain.getPurchase_ok());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getRating_last_valueDirtyFlag())
            this.setRating_last_value(srfdomain.getRating_last_value());
        if(srfdomain.getWebsite_meta_titleDirtyFlag())
            this.setWebsite_meta_title(srfdomain.getWebsite_meta_title());
        if(srfdomain.getRating_last_imageDirtyFlag())
            this.setRating_last_image(srfdomain.getRating_last_image());
        if(srfdomain.getDescription_purchaseDirtyFlag())
            this.setDescription_purchase(srfdomain.getDescription_purchase());
        if(srfdomain.getWebsite_idDirtyFlag())
            this.setWebsite_id(srfdomain.getWebsite_id());
        if(srfdomain.getCan_be_expensedDirtyFlag())
            this.setCan_be_expensed(srfdomain.getCan_be_expensed());
        if(srfdomain.getSale_line_warnDirtyFlag())
            this.setSale_line_warn(srfdomain.getSale_line_warn());
        if(srfdomain.getWebsite_size_xDirtyFlag())
            this.setWebsite_size_x(srfdomain.getWebsite_size_x());
        if(srfdomain.getService_to_purchaseDirtyFlag())
            this.setService_to_purchase(srfdomain.getService_to_purchase());
        if(srfdomain.getWebsite_sequenceDirtyFlag())
            this.setWebsite_sequence(srfdomain.getWebsite_sequence());
        if(srfdomain.getProperty_stock_inventoryDirtyFlag())
            this.setProperty_stock_inventory(srfdomain.getProperty_stock_inventory());
        if(srfdomain.getLocation_idDirtyFlag())
            this.setLocation_id(srfdomain.getLocation_id());
        if(srfdomain.getProperty_valuationDirtyFlag())
            this.setProperty_valuation(srfdomain.getProperty_valuation());
        if(srfdomain.getIs_publishedDirtyFlag())
            this.setIs_published(srfdomain.getIs_published());
        if(srfdomain.getExpense_policyDirtyFlag())
            this.setExpense_policy(srfdomain.getExpense_policy());
        if(srfdomain.getWeight_uom_idDirtyFlag())
            this.setWeight_uom_id(srfdomain.getWeight_uom_id());
        if(srfdomain.getColorDirtyFlag())
            this.setColor(srfdomain.getColor());
        if(srfdomain.getProperty_stock_productionDirtyFlag())
            this.setProperty_stock_production(srfdomain.getProperty_stock_production());
        if(srfdomain.getWebsite_publishedDirtyFlag())
            this.setWebsite_published(srfdomain.getWebsite_published());
        if(srfdomain.getWebsite_meta_keywordsDirtyFlag())
            this.setWebsite_meta_keywords(srfdomain.getWebsite_meta_keywords());
        if(srfdomain.getDescription_pickingoutDirtyFlag())
            this.setDescription_pickingout(srfdomain.getDescription_pickingout());
        if(srfdomain.getPricelist_idDirtyFlag())
            this.setPricelist_id(srfdomain.getPricelist_id());
        if(srfdomain.getRating_countDirtyFlag())
            this.setRating_count(srfdomain.getRating_count());
        if(srfdomain.getWebsite_meta_descriptionDirtyFlag())
            this.setWebsite_meta_description(srfdomain.getWebsite_meta_description());
        if(srfdomain.getValuationDirtyFlag())
            this.setValuation(srfdomain.getValuation());
        if(srfdomain.getInvoice_policyDirtyFlag())
            this.setInvoice_policy(srfdomain.getInvoice_policy());
        if(srfdomain.getPurchase_line_warn_msgDirtyFlag())
            this.setPurchase_line_warn_msg(srfdomain.getPurchase_line_warn_msg());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getProperty_account_income_idDirtyFlag())
            this.setProperty_account_income_id(srfdomain.getProperty_account_income_id());
        if(srfdomain.getProperty_cost_methodDirtyFlag())
            this.setProperty_cost_method(srfdomain.getProperty_cost_method());
        if(srfdomain.getCateg_idDirtyFlag())
            this.setCateg_id(srfdomain.getCateg_id());
        if(srfdomain.getIsPartsDirtyFlag())
            this.setIsParts(srfdomain.getIsParts());
        if(srfdomain.getUom_idDirtyFlag())
            this.setUom_id(srfdomain.getUom_id());
        if(srfdomain.getProduct_variant_idDirtyFlag())
            this.setProduct_variant_id(srfdomain.getProduct_variant_id());
        if(srfdomain.getTypeDirtyFlag())
            this.setType(srfdomain.getType());
        if(srfdomain.getPurchase_methodDirtyFlag())
            this.setPurchase_method(srfdomain.getPurchase_method());
        if(srfdomain.getResponsible_idDirtyFlag())
            this.setResponsible_id(srfdomain.getResponsible_id());
        if(srfdomain.getService_typeDirtyFlag())
            this.setService_type(srfdomain.getService_type());
        if(srfdomain.getUom_nameDirtyFlag())
            this.setUom_name(srfdomain.getUom_name());
        if(srfdomain.getAvailable_thresholdDirtyFlag())
            this.setAvailable_threshold(srfdomain.getAvailable_threshold());
        if(srfdomain.getPurchase_line_warnDirtyFlag())
            this.setPurchase_line_warn(srfdomain.getPurchase_line_warn());
        if(srfdomain.getProduct_variant_countDirtyFlag())
            this.setProduct_variant_count(srfdomain.getProduct_variant_count());
        if(srfdomain.getPos_categ_idDirtyFlag())
            this.setPos_categ_id(srfdomain.getPos_categ_id());
        if(srfdomain.getCustom_messageDirtyFlag())
            this.setCustom_message(srfdomain.getCustom_message());
        if(srfdomain.getProperty_account_expense_idDirtyFlag())
            this.setProperty_account_expense_id(srfdomain.getProperty_account_expense_id());
        if(srfdomain.getSale_delayDirtyFlag())
            this.setSale_delay(srfdomain.getSale_delay());
        if(srfdomain.getUom_po_idDirtyFlag())
            this.setUom_po_id(srfdomain.getUom_po_id());
        if(srfdomain.getAvailable_in_posDirtyFlag())
            this.setAvailable_in_pos(srfdomain.getAvailable_in_pos());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getProduct_tmpl_idDirtyFlag())
            this.setProduct_tmpl_id(srfdomain.getProduct_tmpl_id());

    }

    public List<Product_productDTO> fromDOPage(List<Product_product> poPage)   {
        if(poPage == null)
            return null;
        List<Product_productDTO> dtos=new ArrayList<Product_productDTO>();
        for(Product_product domain : poPage) {
            Product_productDTO dto = new Product_productDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

