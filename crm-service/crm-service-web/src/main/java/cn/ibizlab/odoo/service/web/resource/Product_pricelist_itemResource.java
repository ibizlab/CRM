package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.web.dto.Product_pricelist_itemDTO;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_pricelist_item;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_pricelist_itemService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_pricelist_itemSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Product_pricelist_item" })
@RestController
@RequestMapping("")
public class Product_pricelist_itemResource {

    @Autowired
    private IProduct_pricelist_itemService product_pricelist_itemService;

    public IProduct_pricelist_itemService getProduct_pricelist_itemService() {
        return this.product_pricelist_itemService;
    }

    @ApiOperation(value = "获取数据", tags = {"Product_pricelist_item" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/product_pricelist_items/{product_pricelist_item_id}")
    public ResponseEntity<Product_pricelist_itemDTO> get(@PathVariable("product_pricelist_item_id") Integer product_pricelist_item_id) {
        Product_pricelist_itemDTO dto = new Product_pricelist_itemDTO();
        Product_pricelist_item domain = product_pricelist_itemService.get(product_pricelist_item_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Product_pricelist_item" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/product_pricelist_items")

    public ResponseEntity<Product_pricelist_itemDTO> create(@RequestBody Product_pricelist_itemDTO product_pricelist_itemdto) {
        Product_pricelist_itemDTO dto = new Product_pricelist_itemDTO();
        Product_pricelist_item domain = product_pricelist_itemdto.toDO();
		product_pricelist_itemService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Product_pricelist_item" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/product_pricelist_items/{product_pricelist_item_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_pricelist_item_id") Integer product_pricelist_item_id) {
        Product_pricelist_itemDTO product_pricelist_itemdto = new Product_pricelist_itemDTO();
		Product_pricelist_item domain = new Product_pricelist_item();
		product_pricelist_itemdto.setId(product_pricelist_item_id);
		domain.setId(product_pricelist_item_id);
        Boolean rst = product_pricelist_itemService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Product_pricelist_item" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/product_pricelist_items/{product_pricelist_item_id}/getdraft")

    public ResponseEntity<Product_pricelist_itemDTO> getDraft(@PathVariable("product_pricelist_item_id") Integer product_pricelist_item_id, @RequestBody Product_pricelist_itemDTO product_pricelist_itemdto) {
        Product_pricelist_item product_pricelist_item = product_pricelist_itemdto.toDO();
    	product_pricelist_item = product_pricelist_itemService.getDraft(product_pricelist_item) ;
    	product_pricelist_itemdto.fromDO(product_pricelist_item);
        return ResponseEntity.status(HttpStatus.OK).body(product_pricelist_itemdto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Product_pricelist_item" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/product_pricelist_items/createBatch")
    public ResponseEntity<Boolean> createBatchProduct_pricelist_item(@RequestBody List<Product_pricelist_itemDTO> product_pricelist_itemdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Product_pricelist_item" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/product_pricelist_items/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_pricelist_itemDTO> product_pricelist_itemdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Product_pricelist_item" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/product_pricelist_items/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Product_pricelist_itemDTO> product_pricelist_itemdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Product_pricelist_item" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/product_pricelist_items/{product_pricelist_item_id}")

    public ResponseEntity<Product_pricelist_itemDTO> update(@PathVariable("product_pricelist_item_id") Integer product_pricelist_item_id, @RequestBody Product_pricelist_itemDTO product_pricelist_itemdto) {
		Product_pricelist_item domain = product_pricelist_itemdto.toDO();
        domain.setId(product_pricelist_item_id);
		product_pricelist_itemService.update(domain);
		Product_pricelist_itemDTO dto = new Product_pricelist_itemDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "CheckKey", tags = {"Product_pricelist_item" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/web/product_pricelist_items/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_pricelist_itemDTO product_pricelist_itemdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "Save", tags = {"Product_pricelist_item" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/web/product_pricelist_items/{product_pricelist_item_id}/save")
    public ResponseEntity<Boolean> save(@RequestBody Product_pricelist_itemDTO product_pricelist_itemdto) {
        Product_pricelist_item product_pricelist_item = product_pricelist_itemdto.toDO();
    	Boolean b = product_pricelist_itemService.save(product_pricelist_item) ;
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Product_pricelist_item" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/product_pricelist_items/fetchdefault")
	public ResponseEntity<Page<Product_pricelist_itemDTO>> fetchDefault(Product_pricelist_itemSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Product_pricelist_itemDTO> list = new ArrayList<Product_pricelist_itemDTO>();
        
        Page<Product_pricelist_item> domains = product_pricelist_itemService.searchDefault(context) ;
        for(Product_pricelist_item product_pricelist_item : domains.getContent()){
            Product_pricelist_itemDTO dto = new Product_pricelist_itemDTO();
            dto.fromDO(product_pricelist_item);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
