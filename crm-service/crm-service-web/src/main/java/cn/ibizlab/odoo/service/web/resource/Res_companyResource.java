package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.web.dto.Res_companyDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_company;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_companyService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_companySearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Res_company" })
@RestController
@RequestMapping("")
public class Res_companyResource {

    @Autowired
    private IRes_companyService res_companyService;

    public IRes_companyService getRes_companyService() {
        return this.res_companyService;
    }

    @ApiOperation(value = "获取数据", tags = {"Res_company" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/res_companies/{res_company_id}")
    public ResponseEntity<Res_companyDTO> get(@PathVariable("res_company_id") Integer res_company_id) {
        Res_companyDTO dto = new Res_companyDTO();
        Res_company domain = res_companyService.get(res_company_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Res_company" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/res_companies/{res_company_id}/getdraft")

    public ResponseEntity<Res_companyDTO> getDraft(@PathVariable("res_company_id") Integer res_company_id, @RequestBody Res_companyDTO res_companydto) {
        Res_company res_company = res_companydto.toDO();
    	res_company = res_companyService.getDraft(res_company) ;
    	res_companydto.fromDO(res_company);
        return ResponseEntity.status(HttpStatus.OK).body(res_companydto);
    }

    @ApiOperation(value = "删除数据", tags = {"Res_company" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_companies/{res_company_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_company_id") Integer res_company_id) {
        Res_companyDTO res_companydto = new Res_companyDTO();
		Res_company domain = new Res_company();
		res_companydto.setId(res_company_id);
		domain.setId(res_company_id);
        Boolean rst = res_companyService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Res_company" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/res_companies/createBatch")
    public ResponseEntity<Boolean> createBatchRes_company(@RequestBody List<Res_companyDTO> res_companydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Res_company" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_companies/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_companyDTO> res_companydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Res_company" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/res_companies")

    public ResponseEntity<Res_companyDTO> create(@RequestBody Res_companyDTO res_companydto) {
        Res_companyDTO dto = new Res_companyDTO();
        Res_company domain = res_companydto.toDO();
		res_companyService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Res_company" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/res_companies/{res_company_id}")

    public ResponseEntity<Res_companyDTO> update(@PathVariable("res_company_id") Integer res_company_id, @RequestBody Res_companyDTO res_companydto) {
		Res_company domain = res_companydto.toDO();
        domain.setId(res_company_id);
		res_companyService.update(domain);
		Res_companyDTO dto = new Res_companyDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Res_company" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/res_companies/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Res_companyDTO> res_companydtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Res_company" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/res_companies/fetchdefault")
	public ResponseEntity<Page<Res_companyDTO>> fetchDefault(Res_companySearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Res_companyDTO> list = new ArrayList<Res_companyDTO>();
        
        Page<Res_company> domains = res_companyService.searchDefault(context) ;
        for(Res_company res_company : domains.getContent()){
            Res_companyDTO dto = new Res_companyDTO();
            dto.fromDO(res_company);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
