package cn.ibizlab.odoo.service.web.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_product.valuerule.anno.product_category.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_category;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Product_categoryDTO]
 */
public class Product_categoryDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Product_category__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [PROPERTY_STOCK_VALUATION_ACCOUNT_ID]
     *
     */
    @Product_categoryProperty_stock_valuation_account_idDefault(info = "默认规则")
    private Integer property_stock_valuation_account_id;

    @JsonIgnore
    private boolean property_stock_valuation_account_idDirtyFlag;

    /**
     * 属性 [CHILD_ID]
     *
     */
    @Product_categoryChild_idDefault(info = "默认规则")
    private String child_id;

    @JsonIgnore
    private boolean child_idDirtyFlag;

    /**
     * 属性 [PRODUCT_COUNT]
     *
     */
    @Product_categoryProduct_countDefault(info = "默认规则")
    private Integer product_count;

    @JsonIgnore
    private boolean product_countDirtyFlag;

    /**
     * 属性 [ROUTE_IDS]
     *
     */
    @Product_categoryRoute_idsDefault(info = "默认规则")
    private String route_ids;

    @JsonIgnore
    private boolean route_idsDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Product_categoryNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Product_categoryDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [PROPERTY_VALUATION]
     *
     */
    @Product_categoryProperty_valuationDefault(info = "默认规则")
    private String property_valuation;

    @JsonIgnore
    private boolean property_valuationDirtyFlag;

    /**
     * 属性 [PARENT_PATH]
     *
     */
    @Product_categoryParent_pathDefault(info = "默认规则")
    private String parent_path;

    @JsonIgnore
    private boolean parent_pathDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Product_categoryIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [COMPLETE_NAME]
     *
     */
    @Product_categoryComplete_nameDefault(info = "默认规则")
    private String complete_name;

    @JsonIgnore
    private boolean complete_nameDirtyFlag;

    /**
     * 属性 [PROPERTY_ACCOUNT_CREDITOR_PRICE_DIFFERENCE_CATEG]
     *
     */
    @Product_categoryProperty_account_creditor_price_difference_categDefault(info = "默认规则")
    private Integer property_account_creditor_price_difference_categ;

    @JsonIgnore
    private boolean property_account_creditor_price_difference_categDirtyFlag;

    /**
     * 属性 [PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID]
     *
     */
    @Product_categoryProperty_stock_account_output_categ_idDefault(info = "默认规则")
    private Integer property_stock_account_output_categ_id;

    @JsonIgnore
    private boolean property_stock_account_output_categ_idDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Product_categoryCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [TOTAL_ROUTE_IDS]
     *
     */
    @Product_categoryTotal_route_idsDefault(info = "默认规则")
    private String total_route_ids;

    @JsonIgnore
    private boolean total_route_idsDirtyFlag;

    /**
     * 属性 [PROPERTY_COST_METHOD]
     *
     */
    @Product_categoryProperty_cost_methodDefault(info = "默认规则")
    private String property_cost_method;

    @JsonIgnore
    private boolean property_cost_methodDirtyFlag;

    /**
     * 属性 [PROPERTY_ACCOUNT_INCOME_CATEG_ID]
     *
     */
    @Product_categoryProperty_account_income_categ_idDefault(info = "默认规则")
    private Integer property_account_income_categ_id;

    @JsonIgnore
    private boolean property_account_income_categ_idDirtyFlag;

    /**
     * 属性 [PROPERTY_ACCOUNT_EXPENSE_CATEG_ID]
     *
     */
    @Product_categoryProperty_account_expense_categ_idDefault(info = "默认规则")
    private Integer property_account_expense_categ_id;

    @JsonIgnore
    private boolean property_account_expense_categ_idDirtyFlag;

    /**
     * 属性 [PROPERTY_STOCK_JOURNAL]
     *
     */
    @Product_categoryProperty_stock_journalDefault(info = "默认规则")
    private Integer property_stock_journal;

    @JsonIgnore
    private boolean property_stock_journalDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Product_categoryWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID]
     *
     */
    @Product_categoryProperty_stock_account_input_categ_idDefault(info = "默认规则")
    private Integer property_stock_account_input_categ_id;

    @JsonIgnore
    private boolean property_stock_account_input_categ_idDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Product_categoryWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [PARENT_ID_TEXT]
     *
     */
    @Product_categoryParent_id_textDefault(info = "默认规则")
    private String parent_id_text;

    @JsonIgnore
    private boolean parent_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Product_categoryCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [REMOVAL_STRATEGY_ID_TEXT]
     *
     */
    @Product_categoryRemoval_strategy_id_textDefault(info = "默认规则")
    private String removal_strategy_id_text;

    @JsonIgnore
    private boolean removal_strategy_id_textDirtyFlag;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @Product_categoryParent_idDefault(info = "默认规则")
    private Integer parent_id;

    @JsonIgnore
    private boolean parent_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Product_categoryCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [REMOVAL_STRATEGY_ID]
     *
     */
    @Product_categoryRemoval_strategy_idDefault(info = "默认规则")
    private Integer removal_strategy_id;

    @JsonIgnore
    private boolean removal_strategy_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Product_categoryWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_STOCK_VALUATION_ACCOUNT_ID]
     */
    @JsonProperty("property_stock_valuation_account_id")
    public Integer getProperty_stock_valuation_account_id(){
        return property_stock_valuation_account_id ;
    }

    /**
     * 设置 [PROPERTY_STOCK_VALUATION_ACCOUNT_ID]
     */
    @JsonProperty("property_stock_valuation_account_id")
    public void setProperty_stock_valuation_account_id(Integer  property_stock_valuation_account_id){
        this.property_stock_valuation_account_id = property_stock_valuation_account_id ;
        this.property_stock_valuation_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_STOCK_VALUATION_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_valuation_account_idDirtyFlag(){
        return property_stock_valuation_account_idDirtyFlag ;
    }

    /**
     * 获取 [CHILD_ID]
     */
    @JsonProperty("child_id")
    public String getChild_id(){
        return child_id ;
    }

    /**
     * 设置 [CHILD_ID]
     */
    @JsonProperty("child_id")
    public void setChild_id(String  child_id){
        this.child_id = child_id ;
        this.child_idDirtyFlag = true ;
    }

    /**
     * 获取 [CHILD_ID]脏标记
     */
    @JsonIgnore
    public boolean getChild_idDirtyFlag(){
        return child_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_COUNT]
     */
    @JsonProperty("product_count")
    public Integer getProduct_count(){
        return product_count ;
    }

    /**
     * 设置 [PRODUCT_COUNT]
     */
    @JsonProperty("product_count")
    public void setProduct_count(Integer  product_count){
        this.product_count = product_count ;
        this.product_countDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_countDirtyFlag(){
        return product_countDirtyFlag ;
    }

    /**
     * 获取 [ROUTE_IDS]
     */
    @JsonProperty("route_ids")
    public String getRoute_ids(){
        return route_ids ;
    }

    /**
     * 设置 [ROUTE_IDS]
     */
    @JsonProperty("route_ids")
    public void setRoute_ids(String  route_ids){
        this.route_ids = route_ids ;
        this.route_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ROUTE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getRoute_idsDirtyFlag(){
        return route_idsDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_VALUATION]
     */
    @JsonProperty("property_valuation")
    public String getProperty_valuation(){
        return property_valuation ;
    }

    /**
     * 设置 [PROPERTY_VALUATION]
     */
    @JsonProperty("property_valuation")
    public void setProperty_valuation(String  property_valuation){
        this.property_valuation = property_valuation ;
        this.property_valuationDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_VALUATION]脏标记
     */
    @JsonIgnore
    public boolean getProperty_valuationDirtyFlag(){
        return property_valuationDirtyFlag ;
    }

    /**
     * 获取 [PARENT_PATH]
     */
    @JsonProperty("parent_path")
    public String getParent_path(){
        return parent_path ;
    }

    /**
     * 设置 [PARENT_PATH]
     */
    @JsonProperty("parent_path")
    public void setParent_path(String  parent_path){
        this.parent_path = parent_path ;
        this.parent_pathDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_PATH]脏标记
     */
    @JsonIgnore
    public boolean getParent_pathDirtyFlag(){
        return parent_pathDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [COMPLETE_NAME]
     */
    @JsonProperty("complete_name")
    public String getComplete_name(){
        return complete_name ;
    }

    /**
     * 设置 [COMPLETE_NAME]
     */
    @JsonProperty("complete_name")
    public void setComplete_name(String  complete_name){
        this.complete_name = complete_name ;
        this.complete_nameDirtyFlag = true ;
    }

    /**
     * 获取 [COMPLETE_NAME]脏标记
     */
    @JsonIgnore
    public boolean getComplete_nameDirtyFlag(){
        return complete_nameDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_CREDITOR_PRICE_DIFFERENCE_CATEG]
     */
    @JsonProperty("property_account_creditor_price_difference_categ")
    public Integer getProperty_account_creditor_price_difference_categ(){
        return property_account_creditor_price_difference_categ ;
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_CREDITOR_PRICE_DIFFERENCE_CATEG]
     */
    @JsonProperty("property_account_creditor_price_difference_categ")
    public void setProperty_account_creditor_price_difference_categ(Integer  property_account_creditor_price_difference_categ){
        this.property_account_creditor_price_difference_categ = property_account_creditor_price_difference_categ ;
        this.property_account_creditor_price_difference_categDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_CREDITOR_PRICE_DIFFERENCE_CATEG]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_creditor_price_difference_categDirtyFlag(){
        return property_account_creditor_price_difference_categDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID]
     */
    @JsonProperty("property_stock_account_output_categ_id")
    public Integer getProperty_stock_account_output_categ_id(){
        return property_stock_account_output_categ_id ;
    }

    /**
     * 设置 [PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID]
     */
    @JsonProperty("property_stock_account_output_categ_id")
    public void setProperty_stock_account_output_categ_id(Integer  property_stock_account_output_categ_id){
        this.property_stock_account_output_categ_id = property_stock_account_output_categ_id ;
        this.property_stock_account_output_categ_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_account_output_categ_idDirtyFlag(){
        return property_stock_account_output_categ_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [TOTAL_ROUTE_IDS]
     */
    @JsonProperty("total_route_ids")
    public String getTotal_route_ids(){
        return total_route_ids ;
    }

    /**
     * 设置 [TOTAL_ROUTE_IDS]
     */
    @JsonProperty("total_route_ids")
    public void setTotal_route_ids(String  total_route_ids){
        this.total_route_ids = total_route_ids ;
        this.total_route_idsDirtyFlag = true ;
    }

    /**
     * 获取 [TOTAL_ROUTE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getTotal_route_idsDirtyFlag(){
        return total_route_idsDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_COST_METHOD]
     */
    @JsonProperty("property_cost_method")
    public String getProperty_cost_method(){
        return property_cost_method ;
    }

    /**
     * 设置 [PROPERTY_COST_METHOD]
     */
    @JsonProperty("property_cost_method")
    public void setProperty_cost_method(String  property_cost_method){
        this.property_cost_method = property_cost_method ;
        this.property_cost_methodDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_COST_METHOD]脏标记
     */
    @JsonIgnore
    public boolean getProperty_cost_methodDirtyFlag(){
        return property_cost_methodDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_INCOME_CATEG_ID]
     */
    @JsonProperty("property_account_income_categ_id")
    public Integer getProperty_account_income_categ_id(){
        return property_account_income_categ_id ;
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_INCOME_CATEG_ID]
     */
    @JsonProperty("property_account_income_categ_id")
    public void setProperty_account_income_categ_id(Integer  property_account_income_categ_id){
        this.property_account_income_categ_id = property_account_income_categ_id ;
        this.property_account_income_categ_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_INCOME_CATEG_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_income_categ_idDirtyFlag(){
        return property_account_income_categ_idDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_EXPENSE_CATEG_ID]
     */
    @JsonProperty("property_account_expense_categ_id")
    public Integer getProperty_account_expense_categ_id(){
        return property_account_expense_categ_id ;
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_EXPENSE_CATEG_ID]
     */
    @JsonProperty("property_account_expense_categ_id")
    public void setProperty_account_expense_categ_id(Integer  property_account_expense_categ_id){
        this.property_account_expense_categ_id = property_account_expense_categ_id ;
        this.property_account_expense_categ_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_EXPENSE_CATEG_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_expense_categ_idDirtyFlag(){
        return property_account_expense_categ_idDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_STOCK_JOURNAL]
     */
    @JsonProperty("property_stock_journal")
    public Integer getProperty_stock_journal(){
        return property_stock_journal ;
    }

    /**
     * 设置 [PROPERTY_STOCK_JOURNAL]
     */
    @JsonProperty("property_stock_journal")
    public void setProperty_stock_journal(Integer  property_stock_journal){
        this.property_stock_journal = property_stock_journal ;
        this.property_stock_journalDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_STOCK_JOURNAL]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_journalDirtyFlag(){
        return property_stock_journalDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID]
     */
    @JsonProperty("property_stock_account_input_categ_id")
    public Integer getProperty_stock_account_input_categ_id(){
        return property_stock_account_input_categ_id ;
    }

    /**
     * 设置 [PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID]
     */
    @JsonProperty("property_stock_account_input_categ_id")
    public void setProperty_stock_account_input_categ_id(Integer  property_stock_account_input_categ_id){
        this.property_stock_account_input_categ_id = property_stock_account_input_categ_id ;
        this.property_stock_account_input_categ_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_account_input_categ_idDirtyFlag(){
        return property_stock_account_input_categ_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PARENT_ID_TEXT]
     */
    @JsonProperty("parent_id_text")
    public String getParent_id_text(){
        return parent_id_text ;
    }

    /**
     * 设置 [PARENT_ID_TEXT]
     */
    @JsonProperty("parent_id_text")
    public void setParent_id_text(String  parent_id_text){
        this.parent_id_text = parent_id_text ;
        this.parent_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getParent_id_textDirtyFlag(){
        return parent_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [REMOVAL_STRATEGY_ID_TEXT]
     */
    @JsonProperty("removal_strategy_id_text")
    public String getRemoval_strategy_id_text(){
        return removal_strategy_id_text ;
    }

    /**
     * 设置 [REMOVAL_STRATEGY_ID_TEXT]
     */
    @JsonProperty("removal_strategy_id_text")
    public void setRemoval_strategy_id_text(String  removal_strategy_id_text){
        this.removal_strategy_id_text = removal_strategy_id_text ;
        this.removal_strategy_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [REMOVAL_STRATEGY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getRemoval_strategy_id_textDirtyFlag(){
        return removal_strategy_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return parent_id ;
    }

    /**
     * 设置 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return parent_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [REMOVAL_STRATEGY_ID]
     */
    @JsonProperty("removal_strategy_id")
    public Integer getRemoval_strategy_id(){
        return removal_strategy_id ;
    }

    /**
     * 设置 [REMOVAL_STRATEGY_ID]
     */
    @JsonProperty("removal_strategy_id")
    public void setRemoval_strategy_id(Integer  removal_strategy_id){
        this.removal_strategy_id = removal_strategy_id ;
        this.removal_strategy_idDirtyFlag = true ;
    }

    /**
     * 获取 [REMOVAL_STRATEGY_ID]脏标记
     */
    @JsonIgnore
    public boolean getRemoval_strategy_idDirtyFlag(){
        return removal_strategy_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Product_category toDO() {
        Product_category srfdomain = new Product_category();
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getProperty_stock_valuation_account_idDirtyFlag())
            srfdomain.setProperty_stock_valuation_account_id(property_stock_valuation_account_id);
        if(getChild_idDirtyFlag())
            srfdomain.setChild_id(child_id);
        if(getProduct_countDirtyFlag())
            srfdomain.setProduct_count(product_count);
        if(getRoute_idsDirtyFlag())
            srfdomain.setRoute_ids(route_ids);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getProperty_valuationDirtyFlag())
            srfdomain.setProperty_valuation(property_valuation);
        if(getParent_pathDirtyFlag())
            srfdomain.setParent_path(parent_path);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getComplete_nameDirtyFlag())
            srfdomain.setComplete_name(complete_name);
        if(getProperty_account_creditor_price_difference_categDirtyFlag())
            srfdomain.setProperty_account_creditor_price_difference_categ(property_account_creditor_price_difference_categ);
        if(getProperty_stock_account_output_categ_idDirtyFlag())
            srfdomain.setProperty_stock_account_output_categ_id(property_stock_account_output_categ_id);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getTotal_route_idsDirtyFlag())
            srfdomain.setTotal_route_ids(total_route_ids);
        if(getProperty_cost_methodDirtyFlag())
            srfdomain.setProperty_cost_method(property_cost_method);
        if(getProperty_account_income_categ_idDirtyFlag())
            srfdomain.setProperty_account_income_categ_id(property_account_income_categ_id);
        if(getProperty_account_expense_categ_idDirtyFlag())
            srfdomain.setProperty_account_expense_categ_id(property_account_expense_categ_id);
        if(getProperty_stock_journalDirtyFlag())
            srfdomain.setProperty_stock_journal(property_stock_journal);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getProperty_stock_account_input_categ_idDirtyFlag())
            srfdomain.setProperty_stock_account_input_categ_id(property_stock_account_input_categ_id);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getParent_id_textDirtyFlag())
            srfdomain.setParent_id_text(parent_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getRemoval_strategy_id_textDirtyFlag())
            srfdomain.setRemoval_strategy_id_text(removal_strategy_id_text);
        if(getParent_idDirtyFlag())
            srfdomain.setParent_id(parent_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getRemoval_strategy_idDirtyFlag())
            srfdomain.setRemoval_strategy_id(removal_strategy_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Product_category srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getProperty_stock_valuation_account_idDirtyFlag())
            this.setProperty_stock_valuation_account_id(srfdomain.getProperty_stock_valuation_account_id());
        if(srfdomain.getChild_idDirtyFlag())
            this.setChild_id(srfdomain.getChild_id());
        if(srfdomain.getProduct_countDirtyFlag())
            this.setProduct_count(srfdomain.getProduct_count());
        if(srfdomain.getRoute_idsDirtyFlag())
            this.setRoute_ids(srfdomain.getRoute_ids());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getProperty_valuationDirtyFlag())
            this.setProperty_valuation(srfdomain.getProperty_valuation());
        if(srfdomain.getParent_pathDirtyFlag())
            this.setParent_path(srfdomain.getParent_path());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getComplete_nameDirtyFlag())
            this.setComplete_name(srfdomain.getComplete_name());
        if(srfdomain.getProperty_account_creditor_price_difference_categDirtyFlag())
            this.setProperty_account_creditor_price_difference_categ(srfdomain.getProperty_account_creditor_price_difference_categ());
        if(srfdomain.getProperty_stock_account_output_categ_idDirtyFlag())
            this.setProperty_stock_account_output_categ_id(srfdomain.getProperty_stock_account_output_categ_id());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getTotal_route_idsDirtyFlag())
            this.setTotal_route_ids(srfdomain.getTotal_route_ids());
        if(srfdomain.getProperty_cost_methodDirtyFlag())
            this.setProperty_cost_method(srfdomain.getProperty_cost_method());
        if(srfdomain.getProperty_account_income_categ_idDirtyFlag())
            this.setProperty_account_income_categ_id(srfdomain.getProperty_account_income_categ_id());
        if(srfdomain.getProperty_account_expense_categ_idDirtyFlag())
            this.setProperty_account_expense_categ_id(srfdomain.getProperty_account_expense_categ_id());
        if(srfdomain.getProperty_stock_journalDirtyFlag())
            this.setProperty_stock_journal(srfdomain.getProperty_stock_journal());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getProperty_stock_account_input_categ_idDirtyFlag())
            this.setProperty_stock_account_input_categ_id(srfdomain.getProperty_stock_account_input_categ_id());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getParent_id_textDirtyFlag())
            this.setParent_id_text(srfdomain.getParent_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getRemoval_strategy_id_textDirtyFlag())
            this.setRemoval_strategy_id_text(srfdomain.getRemoval_strategy_id_text());
        if(srfdomain.getParent_idDirtyFlag())
            this.setParent_id(srfdomain.getParent_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getRemoval_strategy_idDirtyFlag())
            this.setRemoval_strategy_id(srfdomain.getRemoval_strategy_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Product_categoryDTO> fromDOPage(List<Product_category> poPage)   {
        if(poPage == null)
            return null;
        List<Product_categoryDTO> dtos=new ArrayList<Product_categoryDTO>();
        for(Product_category domain : poPage) {
            Product_categoryDTO dto = new Product_categoryDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

