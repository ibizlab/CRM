package cn.ibizlab.odoo.service.web.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_sale.valuerule.anno.sale_order_line.*;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_line;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Sale_order_lineDTO]
 */
public class Sale_order_lineDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [IS_EXPENSE]
     *
     */
    @Sale_order_lineIs_expenseDefault(info = "默认规则")
    private String is_expense;

    @JsonIgnore
    private boolean is_expenseDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Sale_order_lineWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [QTY_TO_INVOICE]
     *
     */
    @Sale_order_lineQty_to_invoiceDefault(info = "默认规则")
    private Double qty_to_invoice;

    @JsonIgnore
    private boolean qty_to_invoiceDirtyFlag;

    /**
     * 属性 [ANALYTIC_TAG_IDS]
     *
     */
    @Sale_order_lineAnalytic_tag_idsDefault(info = "默认规则")
    private String analytic_tag_ids;

    @JsonIgnore
    private boolean analytic_tag_idsDirtyFlag;

    /**
     * 属性 [WARNING_STOCK]
     *
     */
    @Sale_order_lineWarning_stockDefault(info = "默认规则")
    private String warning_stock;

    @JsonIgnore
    private boolean warning_stockDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Sale_order_lineIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Sale_order_lineDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [PRICE_REDUCE_TAXEXCL]
     *
     */
    @Sale_order_linePrice_reduce_taxexclDefault(info = "默认规则")
    private Double price_reduce_taxexcl;

    @JsonIgnore
    private boolean price_reduce_taxexclDirtyFlag;

    /**
     * 属性 [PRODUCT_CUSTOM_ATTRIBUTE_VALUE_IDS]
     *
     */
    @Sale_order_lineProduct_custom_attribute_value_idsDefault(info = "默认规则")
    private String product_custom_attribute_value_ids;

    @JsonIgnore
    private boolean product_custom_attribute_value_idsDirtyFlag;

    /**
     * 属性 [QTY_DELIVERED_MANUAL]
     *
     */
    @Sale_order_lineQty_delivered_manualDefault(info = "默认规则")
    private Double qty_delivered_manual;

    @JsonIgnore
    private boolean qty_delivered_manualDirtyFlag;

    /**
     * 属性 [PRICE_TAX]
     *
     */
    @Sale_order_linePrice_taxDefault(info = "默认规则")
    private Double price_tax;

    @JsonIgnore
    private boolean price_taxDirtyFlag;

    /**
     * 属性 [QTY_DELIVERED_METHOD]
     *
     */
    @Sale_order_lineQty_delivered_methodDefault(info = "默认规则")
    private String qty_delivered_method;

    @JsonIgnore
    private boolean qty_delivered_methodDirtyFlag;

    /**
     * 属性 [INVOICE_STATUS]
     *
     */
    @Sale_order_lineInvoice_statusDefault(info = "默认规则")
    private String invoice_status;

    @JsonIgnore
    private boolean invoice_statusDirtyFlag;

    /**
     * 属性 [DISPLAY_TYPE]
     *
     */
    @Sale_order_lineDisplay_typeDefault(info = "默认规则")
    private String display_type;

    @JsonIgnore
    private boolean display_typeDirtyFlag;

    /**
     * 属性 [MOVE_IDS]
     *
     */
    @Sale_order_lineMove_idsDefault(info = "默认规则")
    private String move_ids;

    @JsonIgnore
    private boolean move_idsDirtyFlag;

    /**
     * 属性 [UNTAXED_AMOUNT_INVOICED]
     *
     */
    @Sale_order_lineUntaxed_amount_invoicedDefault(info = "默认规则")
    private Double untaxed_amount_invoiced;

    @JsonIgnore
    private boolean untaxed_amount_invoicedDirtyFlag;

    /**
     * 属性 [PRODUCT_NO_VARIANT_ATTRIBUTE_VALUE_IDS]
     *
     */
    @Sale_order_lineProduct_no_variant_attribute_value_idsDefault(info = "默认规则")
    private String product_no_variant_attribute_value_ids;

    @JsonIgnore
    private boolean product_no_variant_attribute_value_idsDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Sale_order_line__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [IS_DOWNPAYMENT]
     *
     */
    @Sale_order_lineIs_downpaymentDefault(info = "默认规则")
    private String is_downpayment;

    @JsonIgnore
    private boolean is_downpaymentDirtyFlag;

    /**
     * 属性 [ANALYTIC_LINE_IDS]
     *
     */
    @Sale_order_lineAnalytic_line_idsDefault(info = "默认规则")
    private String analytic_line_ids;

    @JsonIgnore
    private boolean analytic_line_idsDirtyFlag;

    /**
     * 属性 [PRICE_REDUCE_TAXINC]
     *
     */
    @Sale_order_linePrice_reduce_taxincDefault(info = "默认规则")
    private Double price_reduce_taxinc;

    @JsonIgnore
    private boolean price_reduce_taxincDirtyFlag;

    /**
     * 属性 [QTY_DELIVERED]
     *
     */
    @Sale_order_lineQty_deliveredDefault(info = "默认规则")
    private Double qty_delivered;

    @JsonIgnore
    private boolean qty_deliveredDirtyFlag;

    /**
     * 属性 [DISCOUNT]
     *
     */
    @Sale_order_lineDiscountDefault(info = "默认规则")
    private Double discount;

    @JsonIgnore
    private boolean discountDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_QTY]
     *
     */
    @Sale_order_lineProduct_uom_qtyDefault(info = "默认规则")
    private Double product_uom_qty;

    @JsonIgnore
    private boolean product_uom_qtyDirtyFlag;

    /**
     * 属性 [PRICE_REDUCE]
     *
     */
    @Sale_order_linePrice_reduceDefault(info = "默认规则")
    private Double price_reduce;

    @JsonIgnore
    private boolean price_reduceDirtyFlag;

    /**
     * 属性 [CUSTOMER_LEAD]
     *
     */
    @Sale_order_lineCustomer_leadDefault(info = "默认规则")
    private Double customer_lead;

    @JsonIgnore
    private boolean customer_leadDirtyFlag;

    /**
     * 属性 [PRODUCT_UPDATABLE]
     *
     */
    @Sale_order_lineProduct_updatableDefault(info = "默认规则")
    private String product_updatable;

    @JsonIgnore
    private boolean product_updatableDirtyFlag;

    /**
     * 属性 [QTY_INVOICED]
     *
     */
    @Sale_order_lineQty_invoicedDefault(info = "默认规则")
    private Double qty_invoiced;

    @JsonIgnore
    private boolean qty_invoicedDirtyFlag;

    /**
     * 属性 [UNTAXED_AMOUNT_TO_INVOICE]
     *
     */
    @Sale_order_lineUntaxed_amount_to_invoiceDefault(info = "默认规则")
    private Double untaxed_amount_to_invoice;

    @JsonIgnore
    private boolean untaxed_amount_to_invoiceDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Sale_order_lineNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [PRICE_UNIT]
     *
     */
    @Sale_order_linePrice_unitDefault(info = "默认规则")
    private Double price_unit;

    @JsonIgnore
    private boolean price_unitDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Sale_order_lineCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [PURCHASE_LINE_COUNT]
     *
     */
    @Sale_order_linePurchase_line_countDefault(info = "默认规则")
    private Integer purchase_line_count;

    @JsonIgnore
    private boolean purchase_line_countDirtyFlag;

    /**
     * 属性 [NAME_SHORT]
     *
     */
    @Sale_order_lineName_shortDefault(info = "默认规则")
    private String name_short;

    @JsonIgnore
    private boolean name_shortDirtyFlag;

    /**
     * 属性 [OPTION_LINE_IDS]
     *
     */
    @Sale_order_lineOption_line_idsDefault(info = "默认规则")
    private String option_line_ids;

    @JsonIgnore
    private boolean option_line_idsDirtyFlag;

    /**
     * 属性 [TAX_ID]
     *
     */
    @Sale_order_lineTax_idDefault(info = "默认规则")
    private String tax_id;

    @JsonIgnore
    private boolean tax_idDirtyFlag;

    /**
     * 属性 [SALE_ORDER_OPTION_IDS]
     *
     */
    @Sale_order_lineSale_order_option_idsDefault(info = "默认规则")
    private String sale_order_option_ids;

    @JsonIgnore
    private boolean sale_order_option_idsDirtyFlag;

    /**
     * 属性 [PRICE_SUBTOTAL]
     *
     */
    @Sale_order_linePrice_subtotalDefault(info = "默认规则")
    private Double price_subtotal;

    @JsonIgnore
    private boolean price_subtotalDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Sale_order_lineSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [PRICE_TOTAL]
     *
     */
    @Sale_order_linePrice_totalDefault(info = "默认规则")
    private Double price_total;

    @JsonIgnore
    private boolean price_totalDirtyFlag;

    /**
     * 属性 [INVOICE_LINES]
     *
     */
    @Sale_order_lineInvoice_linesDefault(info = "默认规则")
    private String invoice_lines;

    @JsonIgnore
    private boolean invoice_linesDirtyFlag;

    /**
     * 属性 [PURCHASE_LINE_IDS]
     *
     */
    @Sale_order_linePurchase_line_idsDefault(info = "默认规则")
    private String purchase_line_ids;

    @JsonIgnore
    private boolean purchase_line_idsDirtyFlag;

    /**
     * 属性 [EVENT_OK]
     *
     */
    @Sale_order_lineEvent_okDefault(info = "默认规则")
    private String event_ok;

    @JsonIgnore
    private boolean event_okDirtyFlag;

    /**
     * 属性 [EVENT_TICKET_ID_TEXT]
     *
     */
    @Sale_order_lineEvent_ticket_id_textDefault(info = "默认规则")
    private String event_ticket_id_text;

    @JsonIgnore
    private boolean event_ticket_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Sale_order_lineCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [LINKED_LINE_ID_TEXT]
     *
     */
    @Sale_order_lineLinked_line_id_textDefault(info = "默认规则")
    private String linked_line_id_text;

    @JsonIgnore
    private boolean linked_line_id_textDirtyFlag;

    /**
     * 属性 [ORDER_PARTNER_ID_TEXT]
     *
     */
    @Sale_order_lineOrder_partner_id_textDefault(info = "默认规则")
    private String order_partner_id_text;

    @JsonIgnore
    private boolean order_partner_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_IMAGE]
     *
     */
    @Sale_order_lineProduct_imageDefault(info = "默认规则")
    private byte[] product_image;

    @JsonIgnore
    private boolean product_imageDirtyFlag;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @Sale_order_lineCurrency_id_textDefault(info = "默认规则")
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @Sale_order_lineProduct_id_textDefault(info = "默认规则")
    private String product_id_text;

    @JsonIgnore
    private boolean product_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_PACKAGING_TEXT]
     *
     */
    @Sale_order_lineProduct_packaging_textDefault(info = "默认规则")
    private String product_packaging_text;

    @JsonIgnore
    private boolean product_packaging_textDirtyFlag;

    /**
     * 属性 [ORDER_ID_TEXT]
     *
     */
    @Sale_order_lineOrder_id_textDefault(info = "默认规则")
    private String order_id_text;

    @JsonIgnore
    private boolean order_id_textDirtyFlag;

    /**
     * 属性 [EVENT_ID_TEXT]
     *
     */
    @Sale_order_lineEvent_id_textDefault(info = "默认规则")
    private String event_id_text;

    @JsonIgnore
    private boolean event_id_textDirtyFlag;

    /**
     * 属性 [SALESMAN_ID_TEXT]
     *
     */
    @Sale_order_lineSalesman_id_textDefault(info = "默认规则")
    private String salesman_id_text;

    @JsonIgnore
    private boolean salesman_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_TEXT]
     *
     */
    @Sale_order_lineProduct_uom_textDefault(info = "默认规则")
    private String product_uom_text;

    @JsonIgnore
    private boolean product_uom_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Sale_order_lineWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Sale_order_lineCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Sale_order_lineStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [ROUTE_ID_TEXT]
     *
     */
    @Sale_order_lineRoute_id_textDefault(info = "默认规则")
    private String route_id_text;

    @JsonIgnore
    private boolean route_id_textDirtyFlag;

    /**
     * 属性 [SALESMAN_ID]
     *
     */
    @Sale_order_lineSalesman_idDefault(info = "默认规则")
    private Integer salesman_id;

    @JsonIgnore
    private boolean salesman_idDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Sale_order_lineCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [ORDER_ID]
     *
     */
    @Sale_order_lineOrder_idDefault(info = "默认规则")
    private Integer order_id;

    @JsonIgnore
    private boolean order_idDirtyFlag;

    /**
     * 属性 [EVENT_ID]
     *
     */
    @Sale_order_lineEvent_idDefault(info = "默认规则")
    private Integer event_id;

    @JsonIgnore
    private boolean event_idDirtyFlag;

    /**
     * 属性 [LINKED_LINE_ID]
     *
     */
    @Sale_order_lineLinked_line_idDefault(info = "默认规则")
    private Integer linked_line_id;

    @JsonIgnore
    private boolean linked_line_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Sale_order_lineWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Sale_order_lineCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [ROUTE_ID]
     *
     */
    @Sale_order_lineRoute_idDefault(info = "默认规则")
    private Integer route_id;

    @JsonIgnore
    private boolean route_idDirtyFlag;

    /**
     * 属性 [ORDER_PARTNER_ID]
     *
     */
    @Sale_order_lineOrder_partner_idDefault(info = "默认规则")
    private Integer order_partner_id;

    @JsonIgnore
    private boolean order_partner_idDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM]
     *
     */
    @Sale_order_lineProduct_uomDefault(info = "默认规则")
    private Integer product_uom;

    @JsonIgnore
    private boolean product_uomDirtyFlag;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @Sale_order_lineProduct_idDefault(info = "默认规则")
    private Integer product_id;

    @JsonIgnore
    private boolean product_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Sale_order_lineCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [PRODUCT_PACKAGING]
     *
     */
    @Sale_order_lineProduct_packagingDefault(info = "默认规则")
    private Integer product_packaging;

    @JsonIgnore
    private boolean product_packagingDirtyFlag;

    /**
     * 属性 [EVENT_TICKET_ID]
     *
     */
    @Sale_order_lineEvent_ticket_idDefault(info = "默认规则")
    private Integer event_ticket_id;

    @JsonIgnore
    private boolean event_ticket_idDirtyFlag;


    /**
     * 获取 [IS_EXPENSE]
     */
    @JsonProperty("is_expense")
    public String getIs_expense(){
        return is_expense ;
    }

    /**
     * 设置 [IS_EXPENSE]
     */
    @JsonProperty("is_expense")
    public void setIs_expense(String  is_expense){
        this.is_expense = is_expense ;
        this.is_expenseDirtyFlag = true ;
    }

    /**
     * 获取 [IS_EXPENSE]脏标记
     */
    @JsonIgnore
    public boolean getIs_expenseDirtyFlag(){
        return is_expenseDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [QTY_TO_INVOICE]
     */
    @JsonProperty("qty_to_invoice")
    public Double getQty_to_invoice(){
        return qty_to_invoice ;
    }

    /**
     * 设置 [QTY_TO_INVOICE]
     */
    @JsonProperty("qty_to_invoice")
    public void setQty_to_invoice(Double  qty_to_invoice){
        this.qty_to_invoice = qty_to_invoice ;
        this.qty_to_invoiceDirtyFlag = true ;
    }

    /**
     * 获取 [QTY_TO_INVOICE]脏标记
     */
    @JsonIgnore
    public boolean getQty_to_invoiceDirtyFlag(){
        return qty_to_invoiceDirtyFlag ;
    }

    /**
     * 获取 [ANALYTIC_TAG_IDS]
     */
    @JsonProperty("analytic_tag_ids")
    public String getAnalytic_tag_ids(){
        return analytic_tag_ids ;
    }

    /**
     * 设置 [ANALYTIC_TAG_IDS]
     */
    @JsonProperty("analytic_tag_ids")
    public void setAnalytic_tag_ids(String  analytic_tag_ids){
        this.analytic_tag_ids = analytic_tag_ids ;
        this.analytic_tag_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ANALYTIC_TAG_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_tag_idsDirtyFlag(){
        return analytic_tag_idsDirtyFlag ;
    }

    /**
     * 获取 [WARNING_STOCK]
     */
    @JsonProperty("warning_stock")
    public String getWarning_stock(){
        return warning_stock ;
    }

    /**
     * 设置 [WARNING_STOCK]
     */
    @JsonProperty("warning_stock")
    public void setWarning_stock(String  warning_stock){
        this.warning_stock = warning_stock ;
        this.warning_stockDirtyFlag = true ;
    }

    /**
     * 获取 [WARNING_STOCK]脏标记
     */
    @JsonIgnore
    public boolean getWarning_stockDirtyFlag(){
        return warning_stockDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [PRICE_REDUCE_TAXEXCL]
     */
    @JsonProperty("price_reduce_taxexcl")
    public Double getPrice_reduce_taxexcl(){
        return price_reduce_taxexcl ;
    }

    /**
     * 设置 [PRICE_REDUCE_TAXEXCL]
     */
    @JsonProperty("price_reduce_taxexcl")
    public void setPrice_reduce_taxexcl(Double  price_reduce_taxexcl){
        this.price_reduce_taxexcl = price_reduce_taxexcl ;
        this.price_reduce_taxexclDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_REDUCE_TAXEXCL]脏标记
     */
    @JsonIgnore
    public boolean getPrice_reduce_taxexclDirtyFlag(){
        return price_reduce_taxexclDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_CUSTOM_ATTRIBUTE_VALUE_IDS]
     */
    @JsonProperty("product_custom_attribute_value_ids")
    public String getProduct_custom_attribute_value_ids(){
        return product_custom_attribute_value_ids ;
    }

    /**
     * 设置 [PRODUCT_CUSTOM_ATTRIBUTE_VALUE_IDS]
     */
    @JsonProperty("product_custom_attribute_value_ids")
    public void setProduct_custom_attribute_value_ids(String  product_custom_attribute_value_ids){
        this.product_custom_attribute_value_ids = product_custom_attribute_value_ids ;
        this.product_custom_attribute_value_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_CUSTOM_ATTRIBUTE_VALUE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getProduct_custom_attribute_value_idsDirtyFlag(){
        return product_custom_attribute_value_idsDirtyFlag ;
    }

    /**
     * 获取 [QTY_DELIVERED_MANUAL]
     */
    @JsonProperty("qty_delivered_manual")
    public Double getQty_delivered_manual(){
        return qty_delivered_manual ;
    }

    /**
     * 设置 [QTY_DELIVERED_MANUAL]
     */
    @JsonProperty("qty_delivered_manual")
    public void setQty_delivered_manual(Double  qty_delivered_manual){
        this.qty_delivered_manual = qty_delivered_manual ;
        this.qty_delivered_manualDirtyFlag = true ;
    }

    /**
     * 获取 [QTY_DELIVERED_MANUAL]脏标记
     */
    @JsonIgnore
    public boolean getQty_delivered_manualDirtyFlag(){
        return qty_delivered_manualDirtyFlag ;
    }

    /**
     * 获取 [PRICE_TAX]
     */
    @JsonProperty("price_tax")
    public Double getPrice_tax(){
        return price_tax ;
    }

    /**
     * 设置 [PRICE_TAX]
     */
    @JsonProperty("price_tax")
    public void setPrice_tax(Double  price_tax){
        this.price_tax = price_tax ;
        this.price_taxDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_TAX]脏标记
     */
    @JsonIgnore
    public boolean getPrice_taxDirtyFlag(){
        return price_taxDirtyFlag ;
    }

    /**
     * 获取 [QTY_DELIVERED_METHOD]
     */
    @JsonProperty("qty_delivered_method")
    public String getQty_delivered_method(){
        return qty_delivered_method ;
    }

    /**
     * 设置 [QTY_DELIVERED_METHOD]
     */
    @JsonProperty("qty_delivered_method")
    public void setQty_delivered_method(String  qty_delivered_method){
        this.qty_delivered_method = qty_delivered_method ;
        this.qty_delivered_methodDirtyFlag = true ;
    }

    /**
     * 获取 [QTY_DELIVERED_METHOD]脏标记
     */
    @JsonIgnore
    public boolean getQty_delivered_methodDirtyFlag(){
        return qty_delivered_methodDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_STATUS]
     */
    @JsonProperty("invoice_status")
    public String getInvoice_status(){
        return invoice_status ;
    }

    /**
     * 设置 [INVOICE_STATUS]
     */
    @JsonProperty("invoice_status")
    public void setInvoice_status(String  invoice_status){
        this.invoice_status = invoice_status ;
        this.invoice_statusDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_STATUS]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_statusDirtyFlag(){
        return invoice_statusDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_TYPE]
     */
    @JsonProperty("display_type")
    public String getDisplay_type(){
        return display_type ;
    }

    /**
     * 设置 [DISPLAY_TYPE]
     */
    @JsonProperty("display_type")
    public void setDisplay_type(String  display_type){
        this.display_type = display_type ;
        this.display_typeDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_typeDirtyFlag(){
        return display_typeDirtyFlag ;
    }

    /**
     * 获取 [MOVE_IDS]
     */
    @JsonProperty("move_ids")
    public String getMove_ids(){
        return move_ids ;
    }

    /**
     * 设置 [MOVE_IDS]
     */
    @JsonProperty("move_ids")
    public void setMove_ids(String  move_ids){
        this.move_ids = move_ids ;
        this.move_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMove_idsDirtyFlag(){
        return move_idsDirtyFlag ;
    }

    /**
     * 获取 [UNTAXED_AMOUNT_INVOICED]
     */
    @JsonProperty("untaxed_amount_invoiced")
    public Double getUntaxed_amount_invoiced(){
        return untaxed_amount_invoiced ;
    }

    /**
     * 设置 [UNTAXED_AMOUNT_INVOICED]
     */
    @JsonProperty("untaxed_amount_invoiced")
    public void setUntaxed_amount_invoiced(Double  untaxed_amount_invoiced){
        this.untaxed_amount_invoiced = untaxed_amount_invoiced ;
        this.untaxed_amount_invoicedDirtyFlag = true ;
    }

    /**
     * 获取 [UNTAXED_AMOUNT_INVOICED]脏标记
     */
    @JsonIgnore
    public boolean getUntaxed_amount_invoicedDirtyFlag(){
        return untaxed_amount_invoicedDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_NO_VARIANT_ATTRIBUTE_VALUE_IDS]
     */
    @JsonProperty("product_no_variant_attribute_value_ids")
    public String getProduct_no_variant_attribute_value_ids(){
        return product_no_variant_attribute_value_ids ;
    }

    /**
     * 设置 [PRODUCT_NO_VARIANT_ATTRIBUTE_VALUE_IDS]
     */
    @JsonProperty("product_no_variant_attribute_value_ids")
    public void setProduct_no_variant_attribute_value_ids(String  product_no_variant_attribute_value_ids){
        this.product_no_variant_attribute_value_ids = product_no_variant_attribute_value_ids ;
        this.product_no_variant_attribute_value_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_NO_VARIANT_ATTRIBUTE_VALUE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getProduct_no_variant_attribute_value_idsDirtyFlag(){
        return product_no_variant_attribute_value_idsDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [IS_DOWNPAYMENT]
     */
    @JsonProperty("is_downpayment")
    public String getIs_downpayment(){
        return is_downpayment ;
    }

    /**
     * 设置 [IS_DOWNPAYMENT]
     */
    @JsonProperty("is_downpayment")
    public void setIs_downpayment(String  is_downpayment){
        this.is_downpayment = is_downpayment ;
        this.is_downpaymentDirtyFlag = true ;
    }

    /**
     * 获取 [IS_DOWNPAYMENT]脏标记
     */
    @JsonIgnore
    public boolean getIs_downpaymentDirtyFlag(){
        return is_downpaymentDirtyFlag ;
    }

    /**
     * 获取 [ANALYTIC_LINE_IDS]
     */
    @JsonProperty("analytic_line_ids")
    public String getAnalytic_line_ids(){
        return analytic_line_ids ;
    }

    /**
     * 设置 [ANALYTIC_LINE_IDS]
     */
    @JsonProperty("analytic_line_ids")
    public void setAnalytic_line_ids(String  analytic_line_ids){
        this.analytic_line_ids = analytic_line_ids ;
        this.analytic_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ANALYTIC_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_line_idsDirtyFlag(){
        return analytic_line_idsDirtyFlag ;
    }

    /**
     * 获取 [PRICE_REDUCE_TAXINC]
     */
    @JsonProperty("price_reduce_taxinc")
    public Double getPrice_reduce_taxinc(){
        return price_reduce_taxinc ;
    }

    /**
     * 设置 [PRICE_REDUCE_TAXINC]
     */
    @JsonProperty("price_reduce_taxinc")
    public void setPrice_reduce_taxinc(Double  price_reduce_taxinc){
        this.price_reduce_taxinc = price_reduce_taxinc ;
        this.price_reduce_taxincDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_REDUCE_TAXINC]脏标记
     */
    @JsonIgnore
    public boolean getPrice_reduce_taxincDirtyFlag(){
        return price_reduce_taxincDirtyFlag ;
    }

    /**
     * 获取 [QTY_DELIVERED]
     */
    @JsonProperty("qty_delivered")
    public Double getQty_delivered(){
        return qty_delivered ;
    }

    /**
     * 设置 [QTY_DELIVERED]
     */
    @JsonProperty("qty_delivered")
    public void setQty_delivered(Double  qty_delivered){
        this.qty_delivered = qty_delivered ;
        this.qty_deliveredDirtyFlag = true ;
    }

    /**
     * 获取 [QTY_DELIVERED]脏标记
     */
    @JsonIgnore
    public boolean getQty_deliveredDirtyFlag(){
        return qty_deliveredDirtyFlag ;
    }

    /**
     * 获取 [DISCOUNT]
     */
    @JsonProperty("discount")
    public Double getDiscount(){
        return discount ;
    }

    /**
     * 设置 [DISCOUNT]
     */
    @JsonProperty("discount")
    public void setDiscount(Double  discount){
        this.discount = discount ;
        this.discountDirtyFlag = true ;
    }

    /**
     * 获取 [DISCOUNT]脏标记
     */
    @JsonIgnore
    public boolean getDiscountDirtyFlag(){
        return discountDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_QTY]
     */
    @JsonProperty("product_uom_qty")
    public Double getProduct_uom_qty(){
        return product_uom_qty ;
    }

    /**
     * 设置 [PRODUCT_UOM_QTY]
     */
    @JsonProperty("product_uom_qty")
    public void setProduct_uom_qty(Double  product_uom_qty){
        this.product_uom_qty = product_uom_qty ;
        this.product_uom_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_QTY]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_qtyDirtyFlag(){
        return product_uom_qtyDirtyFlag ;
    }

    /**
     * 获取 [PRICE_REDUCE]
     */
    @JsonProperty("price_reduce")
    public Double getPrice_reduce(){
        return price_reduce ;
    }

    /**
     * 设置 [PRICE_REDUCE]
     */
    @JsonProperty("price_reduce")
    public void setPrice_reduce(Double  price_reduce){
        this.price_reduce = price_reduce ;
        this.price_reduceDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_REDUCE]脏标记
     */
    @JsonIgnore
    public boolean getPrice_reduceDirtyFlag(){
        return price_reduceDirtyFlag ;
    }

    /**
     * 获取 [CUSTOMER_LEAD]
     */
    @JsonProperty("customer_lead")
    public Double getCustomer_lead(){
        return customer_lead ;
    }

    /**
     * 设置 [CUSTOMER_LEAD]
     */
    @JsonProperty("customer_lead")
    public void setCustomer_lead(Double  customer_lead){
        this.customer_lead = customer_lead ;
        this.customer_leadDirtyFlag = true ;
    }

    /**
     * 获取 [CUSTOMER_LEAD]脏标记
     */
    @JsonIgnore
    public boolean getCustomer_leadDirtyFlag(){
        return customer_leadDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UPDATABLE]
     */
    @JsonProperty("product_updatable")
    public String getProduct_updatable(){
        return product_updatable ;
    }

    /**
     * 设置 [PRODUCT_UPDATABLE]
     */
    @JsonProperty("product_updatable")
    public void setProduct_updatable(String  product_updatable){
        this.product_updatable = product_updatable ;
        this.product_updatableDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UPDATABLE]脏标记
     */
    @JsonIgnore
    public boolean getProduct_updatableDirtyFlag(){
        return product_updatableDirtyFlag ;
    }

    /**
     * 获取 [QTY_INVOICED]
     */
    @JsonProperty("qty_invoiced")
    public Double getQty_invoiced(){
        return qty_invoiced ;
    }

    /**
     * 设置 [QTY_INVOICED]
     */
    @JsonProperty("qty_invoiced")
    public void setQty_invoiced(Double  qty_invoiced){
        this.qty_invoiced = qty_invoiced ;
        this.qty_invoicedDirtyFlag = true ;
    }

    /**
     * 获取 [QTY_INVOICED]脏标记
     */
    @JsonIgnore
    public boolean getQty_invoicedDirtyFlag(){
        return qty_invoicedDirtyFlag ;
    }

    /**
     * 获取 [UNTAXED_AMOUNT_TO_INVOICE]
     */
    @JsonProperty("untaxed_amount_to_invoice")
    public Double getUntaxed_amount_to_invoice(){
        return untaxed_amount_to_invoice ;
    }

    /**
     * 设置 [UNTAXED_AMOUNT_TO_INVOICE]
     */
    @JsonProperty("untaxed_amount_to_invoice")
    public void setUntaxed_amount_to_invoice(Double  untaxed_amount_to_invoice){
        this.untaxed_amount_to_invoice = untaxed_amount_to_invoice ;
        this.untaxed_amount_to_invoiceDirtyFlag = true ;
    }

    /**
     * 获取 [UNTAXED_AMOUNT_TO_INVOICE]脏标记
     */
    @JsonIgnore
    public boolean getUntaxed_amount_to_invoiceDirtyFlag(){
        return untaxed_amount_to_invoiceDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [PRICE_UNIT]
     */
    @JsonProperty("price_unit")
    public Double getPrice_unit(){
        return price_unit ;
    }

    /**
     * 设置 [PRICE_UNIT]
     */
    @JsonProperty("price_unit")
    public void setPrice_unit(Double  price_unit){
        this.price_unit = price_unit ;
        this.price_unitDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_UNIT]脏标记
     */
    @JsonIgnore
    public boolean getPrice_unitDirtyFlag(){
        return price_unitDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [PURCHASE_LINE_COUNT]
     */
    @JsonProperty("purchase_line_count")
    public Integer getPurchase_line_count(){
        return purchase_line_count ;
    }

    /**
     * 设置 [PURCHASE_LINE_COUNT]
     */
    @JsonProperty("purchase_line_count")
    public void setPurchase_line_count(Integer  purchase_line_count){
        this.purchase_line_count = purchase_line_count ;
        this.purchase_line_countDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASE_LINE_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_line_countDirtyFlag(){
        return purchase_line_countDirtyFlag ;
    }

    /**
     * 获取 [NAME_SHORT]
     */
    @JsonProperty("name_short")
    public String getName_short(){
        return name_short ;
    }

    /**
     * 设置 [NAME_SHORT]
     */
    @JsonProperty("name_short")
    public void setName_short(String  name_short){
        this.name_short = name_short ;
        this.name_shortDirtyFlag = true ;
    }

    /**
     * 获取 [NAME_SHORT]脏标记
     */
    @JsonIgnore
    public boolean getName_shortDirtyFlag(){
        return name_shortDirtyFlag ;
    }

    /**
     * 获取 [OPTION_LINE_IDS]
     */
    @JsonProperty("option_line_ids")
    public String getOption_line_ids(){
        return option_line_ids ;
    }

    /**
     * 设置 [OPTION_LINE_IDS]
     */
    @JsonProperty("option_line_ids")
    public void setOption_line_ids(String  option_line_ids){
        this.option_line_ids = option_line_ids ;
        this.option_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [OPTION_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getOption_line_idsDirtyFlag(){
        return option_line_idsDirtyFlag ;
    }

    /**
     * 获取 [TAX_ID]
     */
    @JsonProperty("tax_id")
    public String getTax_id(){
        return tax_id ;
    }

    /**
     * 设置 [TAX_ID]
     */
    @JsonProperty("tax_id")
    public void setTax_id(String  tax_id){
        this.tax_id = tax_id ;
        this.tax_idDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_ID]脏标记
     */
    @JsonIgnore
    public boolean getTax_idDirtyFlag(){
        return tax_idDirtyFlag ;
    }

    /**
     * 获取 [SALE_ORDER_OPTION_IDS]
     */
    @JsonProperty("sale_order_option_ids")
    public String getSale_order_option_ids(){
        return sale_order_option_ids ;
    }

    /**
     * 设置 [SALE_ORDER_OPTION_IDS]
     */
    @JsonProperty("sale_order_option_ids")
    public void setSale_order_option_ids(String  sale_order_option_ids){
        this.sale_order_option_ids = sale_order_option_ids ;
        this.sale_order_option_idsDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_ORDER_OPTION_IDS]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_option_idsDirtyFlag(){
        return sale_order_option_idsDirtyFlag ;
    }

    /**
     * 获取 [PRICE_SUBTOTAL]
     */
    @JsonProperty("price_subtotal")
    public Double getPrice_subtotal(){
        return price_subtotal ;
    }

    /**
     * 设置 [PRICE_SUBTOTAL]
     */
    @JsonProperty("price_subtotal")
    public void setPrice_subtotal(Double  price_subtotal){
        this.price_subtotal = price_subtotal ;
        this.price_subtotalDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_SUBTOTAL]脏标记
     */
    @JsonIgnore
    public boolean getPrice_subtotalDirtyFlag(){
        return price_subtotalDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [PRICE_TOTAL]
     */
    @JsonProperty("price_total")
    public Double getPrice_total(){
        return price_total ;
    }

    /**
     * 设置 [PRICE_TOTAL]
     */
    @JsonProperty("price_total")
    public void setPrice_total(Double  price_total){
        this.price_total = price_total ;
        this.price_totalDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_TOTAL]脏标记
     */
    @JsonIgnore
    public boolean getPrice_totalDirtyFlag(){
        return price_totalDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_LINES]
     */
    @JsonProperty("invoice_lines")
    public String getInvoice_lines(){
        return invoice_lines ;
    }

    /**
     * 设置 [INVOICE_LINES]
     */
    @JsonProperty("invoice_lines")
    public void setInvoice_lines(String  invoice_lines){
        this.invoice_lines = invoice_lines ;
        this.invoice_linesDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_LINES]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_linesDirtyFlag(){
        return invoice_linesDirtyFlag ;
    }

    /**
     * 获取 [PURCHASE_LINE_IDS]
     */
    @JsonProperty("purchase_line_ids")
    public String getPurchase_line_ids(){
        return purchase_line_ids ;
    }

    /**
     * 设置 [PURCHASE_LINE_IDS]
     */
    @JsonProperty("purchase_line_ids")
    public void setPurchase_line_ids(String  purchase_line_ids){
        this.purchase_line_ids = purchase_line_ids ;
        this.purchase_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASE_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_line_idsDirtyFlag(){
        return purchase_line_idsDirtyFlag ;
    }

    /**
     * 获取 [EVENT_OK]
     */
    @JsonProperty("event_ok")
    public String getEvent_ok(){
        return event_ok ;
    }

    /**
     * 设置 [EVENT_OK]
     */
    @JsonProperty("event_ok")
    public void setEvent_ok(String  event_ok){
        this.event_ok = event_ok ;
        this.event_okDirtyFlag = true ;
    }

    /**
     * 获取 [EVENT_OK]脏标记
     */
    @JsonIgnore
    public boolean getEvent_okDirtyFlag(){
        return event_okDirtyFlag ;
    }

    /**
     * 获取 [EVENT_TICKET_ID_TEXT]
     */
    @JsonProperty("event_ticket_id_text")
    public String getEvent_ticket_id_text(){
        return event_ticket_id_text ;
    }

    /**
     * 设置 [EVENT_TICKET_ID_TEXT]
     */
    @JsonProperty("event_ticket_id_text")
    public void setEvent_ticket_id_text(String  event_ticket_id_text){
        this.event_ticket_id_text = event_ticket_id_text ;
        this.event_ticket_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [EVENT_TICKET_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getEvent_ticket_id_textDirtyFlag(){
        return event_ticket_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [LINKED_LINE_ID_TEXT]
     */
    @JsonProperty("linked_line_id_text")
    public String getLinked_line_id_text(){
        return linked_line_id_text ;
    }

    /**
     * 设置 [LINKED_LINE_ID_TEXT]
     */
    @JsonProperty("linked_line_id_text")
    public void setLinked_line_id_text(String  linked_line_id_text){
        this.linked_line_id_text = linked_line_id_text ;
        this.linked_line_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LINKED_LINE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLinked_line_id_textDirtyFlag(){
        return linked_line_id_textDirtyFlag ;
    }

    /**
     * 获取 [ORDER_PARTNER_ID_TEXT]
     */
    @JsonProperty("order_partner_id_text")
    public String getOrder_partner_id_text(){
        return order_partner_id_text ;
    }

    /**
     * 设置 [ORDER_PARTNER_ID_TEXT]
     */
    @JsonProperty("order_partner_id_text")
    public void setOrder_partner_id_text(String  order_partner_id_text){
        this.order_partner_id_text = order_partner_id_text ;
        this.order_partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ORDER_PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getOrder_partner_id_textDirtyFlag(){
        return order_partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_IMAGE]
     */
    @JsonProperty("product_image")
    public byte[] getProduct_image(){
        return product_image ;
    }

    /**
     * 设置 [PRODUCT_IMAGE]
     */
    @JsonProperty("product_image")
    public void setProduct_image(byte[]  product_image){
        this.product_image = product_image ;
        this.product_imageDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_IMAGE]脏标记
     */
    @JsonIgnore
    public boolean getProduct_imageDirtyFlag(){
        return product_imageDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return currency_id_text ;
    }

    /**
     * 设置 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return product_id_text ;
    }

    /**
     * 设置 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return product_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_PACKAGING_TEXT]
     */
    @JsonProperty("product_packaging_text")
    public String getProduct_packaging_text(){
        return product_packaging_text ;
    }

    /**
     * 设置 [PRODUCT_PACKAGING_TEXT]
     */
    @JsonProperty("product_packaging_text")
    public void setProduct_packaging_text(String  product_packaging_text){
        this.product_packaging_text = product_packaging_text ;
        this.product_packaging_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_PACKAGING_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_packaging_textDirtyFlag(){
        return product_packaging_textDirtyFlag ;
    }

    /**
     * 获取 [ORDER_ID_TEXT]
     */
    @JsonProperty("order_id_text")
    public String getOrder_id_text(){
        return order_id_text ;
    }

    /**
     * 设置 [ORDER_ID_TEXT]
     */
    @JsonProperty("order_id_text")
    public void setOrder_id_text(String  order_id_text){
        this.order_id_text = order_id_text ;
        this.order_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ORDER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getOrder_id_textDirtyFlag(){
        return order_id_textDirtyFlag ;
    }

    /**
     * 获取 [EVENT_ID_TEXT]
     */
    @JsonProperty("event_id_text")
    public String getEvent_id_text(){
        return event_id_text ;
    }

    /**
     * 设置 [EVENT_ID_TEXT]
     */
    @JsonProperty("event_id_text")
    public void setEvent_id_text(String  event_id_text){
        this.event_id_text = event_id_text ;
        this.event_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [EVENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getEvent_id_textDirtyFlag(){
        return event_id_textDirtyFlag ;
    }

    /**
     * 获取 [SALESMAN_ID_TEXT]
     */
    @JsonProperty("salesman_id_text")
    public String getSalesman_id_text(){
        return salesman_id_text ;
    }

    /**
     * 设置 [SALESMAN_ID_TEXT]
     */
    @JsonProperty("salesman_id_text")
    public void setSalesman_id_text(String  salesman_id_text){
        this.salesman_id_text = salesman_id_text ;
        this.salesman_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SALESMAN_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSalesman_id_textDirtyFlag(){
        return salesman_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_TEXT]
     */
    @JsonProperty("product_uom_text")
    public String getProduct_uom_text(){
        return product_uom_text ;
    }

    /**
     * 设置 [PRODUCT_UOM_TEXT]
     */
    @JsonProperty("product_uom_text")
    public void setProduct_uom_text(String  product_uom_text){
        this.product_uom_text = product_uom_text ;
        this.product_uom_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_textDirtyFlag(){
        return product_uom_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [ROUTE_ID_TEXT]
     */
    @JsonProperty("route_id_text")
    public String getRoute_id_text(){
        return route_id_text ;
    }

    /**
     * 设置 [ROUTE_ID_TEXT]
     */
    @JsonProperty("route_id_text")
    public void setRoute_id_text(String  route_id_text){
        this.route_id_text = route_id_text ;
        this.route_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ROUTE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getRoute_id_textDirtyFlag(){
        return route_id_textDirtyFlag ;
    }

    /**
     * 获取 [SALESMAN_ID]
     */
    @JsonProperty("salesman_id")
    public Integer getSalesman_id(){
        return salesman_id ;
    }

    /**
     * 设置 [SALESMAN_ID]
     */
    @JsonProperty("salesman_id")
    public void setSalesman_id(Integer  salesman_id){
        this.salesman_id = salesman_id ;
        this.salesman_idDirtyFlag = true ;
    }

    /**
     * 获取 [SALESMAN_ID]脏标记
     */
    @JsonIgnore
    public boolean getSalesman_idDirtyFlag(){
        return salesman_idDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [ORDER_ID]
     */
    @JsonProperty("order_id")
    public Integer getOrder_id(){
        return order_id ;
    }

    /**
     * 设置 [ORDER_ID]
     */
    @JsonProperty("order_id")
    public void setOrder_id(Integer  order_id){
        this.order_id = order_id ;
        this.order_idDirtyFlag = true ;
    }

    /**
     * 获取 [ORDER_ID]脏标记
     */
    @JsonIgnore
    public boolean getOrder_idDirtyFlag(){
        return order_idDirtyFlag ;
    }

    /**
     * 获取 [EVENT_ID]
     */
    @JsonProperty("event_id")
    public Integer getEvent_id(){
        return event_id ;
    }

    /**
     * 设置 [EVENT_ID]
     */
    @JsonProperty("event_id")
    public void setEvent_id(Integer  event_id){
        this.event_id = event_id ;
        this.event_idDirtyFlag = true ;
    }

    /**
     * 获取 [EVENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getEvent_idDirtyFlag(){
        return event_idDirtyFlag ;
    }

    /**
     * 获取 [LINKED_LINE_ID]
     */
    @JsonProperty("linked_line_id")
    public Integer getLinked_line_id(){
        return linked_line_id ;
    }

    /**
     * 设置 [LINKED_LINE_ID]
     */
    @JsonProperty("linked_line_id")
    public void setLinked_line_id(Integer  linked_line_id){
        this.linked_line_id = linked_line_id ;
        this.linked_line_idDirtyFlag = true ;
    }

    /**
     * 获取 [LINKED_LINE_ID]脏标记
     */
    @JsonIgnore
    public boolean getLinked_line_idDirtyFlag(){
        return linked_line_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [ROUTE_ID]
     */
    @JsonProperty("route_id")
    public Integer getRoute_id(){
        return route_id ;
    }

    /**
     * 设置 [ROUTE_ID]
     */
    @JsonProperty("route_id")
    public void setRoute_id(Integer  route_id){
        this.route_id = route_id ;
        this.route_idDirtyFlag = true ;
    }

    /**
     * 获取 [ROUTE_ID]脏标记
     */
    @JsonIgnore
    public boolean getRoute_idDirtyFlag(){
        return route_idDirtyFlag ;
    }

    /**
     * 获取 [ORDER_PARTNER_ID]
     */
    @JsonProperty("order_partner_id")
    public Integer getOrder_partner_id(){
        return order_partner_id ;
    }

    /**
     * 设置 [ORDER_PARTNER_ID]
     */
    @JsonProperty("order_partner_id")
    public void setOrder_partner_id(Integer  order_partner_id){
        this.order_partner_id = order_partner_id ;
        this.order_partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [ORDER_PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getOrder_partner_idDirtyFlag(){
        return order_partner_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM]
     */
    @JsonProperty("product_uom")
    public Integer getProduct_uom(){
        return product_uom ;
    }

    /**
     * 设置 [PRODUCT_UOM]
     */
    @JsonProperty("product_uom")
    public void setProduct_uom(Integer  product_uom){
        this.product_uom = product_uom ;
        this.product_uomDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uomDirtyFlag(){
        return product_uomDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return product_id ;
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return product_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_PACKAGING]
     */
    @JsonProperty("product_packaging")
    public Integer getProduct_packaging(){
        return product_packaging ;
    }

    /**
     * 设置 [PRODUCT_PACKAGING]
     */
    @JsonProperty("product_packaging")
    public void setProduct_packaging(Integer  product_packaging){
        this.product_packaging = product_packaging ;
        this.product_packagingDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_PACKAGING]脏标记
     */
    @JsonIgnore
    public boolean getProduct_packagingDirtyFlag(){
        return product_packagingDirtyFlag ;
    }

    /**
     * 获取 [EVENT_TICKET_ID]
     */
    @JsonProperty("event_ticket_id")
    public Integer getEvent_ticket_id(){
        return event_ticket_id ;
    }

    /**
     * 设置 [EVENT_TICKET_ID]
     */
    @JsonProperty("event_ticket_id")
    public void setEvent_ticket_id(Integer  event_ticket_id){
        this.event_ticket_id = event_ticket_id ;
        this.event_ticket_idDirtyFlag = true ;
    }

    /**
     * 获取 [EVENT_TICKET_ID]脏标记
     */
    @JsonIgnore
    public boolean getEvent_ticket_idDirtyFlag(){
        return event_ticket_idDirtyFlag ;
    }



    public Sale_order_line toDO() {
        Sale_order_line srfdomain = new Sale_order_line();
        if(getIs_expenseDirtyFlag())
            srfdomain.setIs_expense(is_expense);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getQty_to_invoiceDirtyFlag())
            srfdomain.setQty_to_invoice(qty_to_invoice);
        if(getAnalytic_tag_idsDirtyFlag())
            srfdomain.setAnalytic_tag_ids(analytic_tag_ids);
        if(getWarning_stockDirtyFlag())
            srfdomain.setWarning_stock(warning_stock);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getPrice_reduce_taxexclDirtyFlag())
            srfdomain.setPrice_reduce_taxexcl(price_reduce_taxexcl);
        if(getProduct_custom_attribute_value_idsDirtyFlag())
            srfdomain.setProduct_custom_attribute_value_ids(product_custom_attribute_value_ids);
        if(getQty_delivered_manualDirtyFlag())
            srfdomain.setQty_delivered_manual(qty_delivered_manual);
        if(getPrice_taxDirtyFlag())
            srfdomain.setPrice_tax(price_tax);
        if(getQty_delivered_methodDirtyFlag())
            srfdomain.setQty_delivered_method(qty_delivered_method);
        if(getInvoice_statusDirtyFlag())
            srfdomain.setInvoice_status(invoice_status);
        if(getDisplay_typeDirtyFlag())
            srfdomain.setDisplay_type(display_type);
        if(getMove_idsDirtyFlag())
            srfdomain.setMove_ids(move_ids);
        if(getUntaxed_amount_invoicedDirtyFlag())
            srfdomain.setUntaxed_amount_invoiced(untaxed_amount_invoiced);
        if(getProduct_no_variant_attribute_value_idsDirtyFlag())
            srfdomain.setProduct_no_variant_attribute_value_ids(product_no_variant_attribute_value_ids);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getIs_downpaymentDirtyFlag())
            srfdomain.setIs_downpayment(is_downpayment);
        if(getAnalytic_line_idsDirtyFlag())
            srfdomain.setAnalytic_line_ids(analytic_line_ids);
        if(getPrice_reduce_taxincDirtyFlag())
            srfdomain.setPrice_reduce_taxinc(price_reduce_taxinc);
        if(getQty_deliveredDirtyFlag())
            srfdomain.setQty_delivered(qty_delivered);
        if(getDiscountDirtyFlag())
            srfdomain.setDiscount(discount);
        if(getProduct_uom_qtyDirtyFlag())
            srfdomain.setProduct_uom_qty(product_uom_qty);
        if(getPrice_reduceDirtyFlag())
            srfdomain.setPrice_reduce(price_reduce);
        if(getCustomer_leadDirtyFlag())
            srfdomain.setCustomer_lead(customer_lead);
        if(getProduct_updatableDirtyFlag())
            srfdomain.setProduct_updatable(product_updatable);
        if(getQty_invoicedDirtyFlag())
            srfdomain.setQty_invoiced(qty_invoiced);
        if(getUntaxed_amount_to_invoiceDirtyFlag())
            srfdomain.setUntaxed_amount_to_invoice(untaxed_amount_to_invoice);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getPrice_unitDirtyFlag())
            srfdomain.setPrice_unit(price_unit);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getPurchase_line_countDirtyFlag())
            srfdomain.setPurchase_line_count(purchase_line_count);
        if(getName_shortDirtyFlag())
            srfdomain.setName_short(name_short);
        if(getOption_line_idsDirtyFlag())
            srfdomain.setOption_line_ids(option_line_ids);
        if(getTax_idDirtyFlag())
            srfdomain.setTax_id(tax_id);
        if(getSale_order_option_idsDirtyFlag())
            srfdomain.setSale_order_option_ids(sale_order_option_ids);
        if(getPrice_subtotalDirtyFlag())
            srfdomain.setPrice_subtotal(price_subtotal);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getPrice_totalDirtyFlag())
            srfdomain.setPrice_total(price_total);
        if(getInvoice_linesDirtyFlag())
            srfdomain.setInvoice_lines(invoice_lines);
        if(getPurchase_line_idsDirtyFlag())
            srfdomain.setPurchase_line_ids(purchase_line_ids);
        if(getEvent_okDirtyFlag())
            srfdomain.setEvent_ok(event_ok);
        if(getEvent_ticket_id_textDirtyFlag())
            srfdomain.setEvent_ticket_id_text(event_ticket_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getLinked_line_id_textDirtyFlag())
            srfdomain.setLinked_line_id_text(linked_line_id_text);
        if(getOrder_partner_id_textDirtyFlag())
            srfdomain.setOrder_partner_id_text(order_partner_id_text);
        if(getProduct_imageDirtyFlag())
            srfdomain.setProduct_image(product_image);
        if(getCurrency_id_textDirtyFlag())
            srfdomain.setCurrency_id_text(currency_id_text);
        if(getProduct_id_textDirtyFlag())
            srfdomain.setProduct_id_text(product_id_text);
        if(getProduct_packaging_textDirtyFlag())
            srfdomain.setProduct_packaging_text(product_packaging_text);
        if(getOrder_id_textDirtyFlag())
            srfdomain.setOrder_id_text(order_id_text);
        if(getEvent_id_textDirtyFlag())
            srfdomain.setEvent_id_text(event_id_text);
        if(getSalesman_id_textDirtyFlag())
            srfdomain.setSalesman_id_text(salesman_id_text);
        if(getProduct_uom_textDirtyFlag())
            srfdomain.setProduct_uom_text(product_uom_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getRoute_id_textDirtyFlag())
            srfdomain.setRoute_id_text(route_id_text);
        if(getSalesman_idDirtyFlag())
            srfdomain.setSalesman_id(salesman_id);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getOrder_idDirtyFlag())
            srfdomain.setOrder_id(order_id);
        if(getEvent_idDirtyFlag())
            srfdomain.setEvent_id(event_id);
        if(getLinked_line_idDirtyFlag())
            srfdomain.setLinked_line_id(linked_line_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getRoute_idDirtyFlag())
            srfdomain.setRoute_id(route_id);
        if(getOrder_partner_idDirtyFlag())
            srfdomain.setOrder_partner_id(order_partner_id);
        if(getProduct_uomDirtyFlag())
            srfdomain.setProduct_uom(product_uom);
        if(getProduct_idDirtyFlag())
            srfdomain.setProduct_id(product_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getProduct_packagingDirtyFlag())
            srfdomain.setProduct_packaging(product_packaging);
        if(getEvent_ticket_idDirtyFlag())
            srfdomain.setEvent_ticket_id(event_ticket_id);

        return srfdomain;
    }

    public void fromDO(Sale_order_line srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getIs_expenseDirtyFlag())
            this.setIs_expense(srfdomain.getIs_expense());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getQty_to_invoiceDirtyFlag())
            this.setQty_to_invoice(srfdomain.getQty_to_invoice());
        if(srfdomain.getAnalytic_tag_idsDirtyFlag())
            this.setAnalytic_tag_ids(srfdomain.getAnalytic_tag_ids());
        if(srfdomain.getWarning_stockDirtyFlag())
            this.setWarning_stock(srfdomain.getWarning_stock());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getPrice_reduce_taxexclDirtyFlag())
            this.setPrice_reduce_taxexcl(srfdomain.getPrice_reduce_taxexcl());
        if(srfdomain.getProduct_custom_attribute_value_idsDirtyFlag())
            this.setProduct_custom_attribute_value_ids(srfdomain.getProduct_custom_attribute_value_ids());
        if(srfdomain.getQty_delivered_manualDirtyFlag())
            this.setQty_delivered_manual(srfdomain.getQty_delivered_manual());
        if(srfdomain.getPrice_taxDirtyFlag())
            this.setPrice_tax(srfdomain.getPrice_tax());
        if(srfdomain.getQty_delivered_methodDirtyFlag())
            this.setQty_delivered_method(srfdomain.getQty_delivered_method());
        if(srfdomain.getInvoice_statusDirtyFlag())
            this.setInvoice_status(srfdomain.getInvoice_status());
        if(srfdomain.getDisplay_typeDirtyFlag())
            this.setDisplay_type(srfdomain.getDisplay_type());
        if(srfdomain.getMove_idsDirtyFlag())
            this.setMove_ids(srfdomain.getMove_ids());
        if(srfdomain.getUntaxed_amount_invoicedDirtyFlag())
            this.setUntaxed_amount_invoiced(srfdomain.getUntaxed_amount_invoiced());
        if(srfdomain.getProduct_no_variant_attribute_value_idsDirtyFlag())
            this.setProduct_no_variant_attribute_value_ids(srfdomain.getProduct_no_variant_attribute_value_ids());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getIs_downpaymentDirtyFlag())
            this.setIs_downpayment(srfdomain.getIs_downpayment());
        if(srfdomain.getAnalytic_line_idsDirtyFlag())
            this.setAnalytic_line_ids(srfdomain.getAnalytic_line_ids());
        if(srfdomain.getPrice_reduce_taxincDirtyFlag())
            this.setPrice_reduce_taxinc(srfdomain.getPrice_reduce_taxinc());
        if(srfdomain.getQty_deliveredDirtyFlag())
            this.setQty_delivered(srfdomain.getQty_delivered());
        if(srfdomain.getDiscountDirtyFlag())
            this.setDiscount(srfdomain.getDiscount());
        if(srfdomain.getProduct_uom_qtyDirtyFlag())
            this.setProduct_uom_qty(srfdomain.getProduct_uom_qty());
        if(srfdomain.getPrice_reduceDirtyFlag())
            this.setPrice_reduce(srfdomain.getPrice_reduce());
        if(srfdomain.getCustomer_leadDirtyFlag())
            this.setCustomer_lead(srfdomain.getCustomer_lead());
        if(srfdomain.getProduct_updatableDirtyFlag())
            this.setProduct_updatable(srfdomain.getProduct_updatable());
        if(srfdomain.getQty_invoicedDirtyFlag())
            this.setQty_invoiced(srfdomain.getQty_invoiced());
        if(srfdomain.getUntaxed_amount_to_invoiceDirtyFlag())
            this.setUntaxed_amount_to_invoice(srfdomain.getUntaxed_amount_to_invoice());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getPrice_unitDirtyFlag())
            this.setPrice_unit(srfdomain.getPrice_unit());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getPurchase_line_countDirtyFlag())
            this.setPurchase_line_count(srfdomain.getPurchase_line_count());
        if(srfdomain.getName_shortDirtyFlag())
            this.setName_short(srfdomain.getName_short());
        if(srfdomain.getOption_line_idsDirtyFlag())
            this.setOption_line_ids(srfdomain.getOption_line_ids());
        if(srfdomain.getTax_idDirtyFlag())
            this.setTax_id(srfdomain.getTax_id());
        if(srfdomain.getSale_order_option_idsDirtyFlag())
            this.setSale_order_option_ids(srfdomain.getSale_order_option_ids());
        if(srfdomain.getPrice_subtotalDirtyFlag())
            this.setPrice_subtotal(srfdomain.getPrice_subtotal());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getPrice_totalDirtyFlag())
            this.setPrice_total(srfdomain.getPrice_total());
        if(srfdomain.getInvoice_linesDirtyFlag())
            this.setInvoice_lines(srfdomain.getInvoice_lines());
        if(srfdomain.getPurchase_line_idsDirtyFlag())
            this.setPurchase_line_ids(srfdomain.getPurchase_line_ids());
        if(srfdomain.getEvent_okDirtyFlag())
            this.setEvent_ok(srfdomain.getEvent_ok());
        if(srfdomain.getEvent_ticket_id_textDirtyFlag())
            this.setEvent_ticket_id_text(srfdomain.getEvent_ticket_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getLinked_line_id_textDirtyFlag())
            this.setLinked_line_id_text(srfdomain.getLinked_line_id_text());
        if(srfdomain.getOrder_partner_id_textDirtyFlag())
            this.setOrder_partner_id_text(srfdomain.getOrder_partner_id_text());
        if(srfdomain.getProduct_imageDirtyFlag())
            this.setProduct_image(srfdomain.getProduct_image());
        if(srfdomain.getCurrency_id_textDirtyFlag())
            this.setCurrency_id_text(srfdomain.getCurrency_id_text());
        if(srfdomain.getProduct_id_textDirtyFlag())
            this.setProduct_id_text(srfdomain.getProduct_id_text());
        if(srfdomain.getProduct_packaging_textDirtyFlag())
            this.setProduct_packaging_text(srfdomain.getProduct_packaging_text());
        if(srfdomain.getOrder_id_textDirtyFlag())
            this.setOrder_id_text(srfdomain.getOrder_id_text());
        if(srfdomain.getEvent_id_textDirtyFlag())
            this.setEvent_id_text(srfdomain.getEvent_id_text());
        if(srfdomain.getSalesman_id_textDirtyFlag())
            this.setSalesman_id_text(srfdomain.getSalesman_id_text());
        if(srfdomain.getProduct_uom_textDirtyFlag())
            this.setProduct_uom_text(srfdomain.getProduct_uom_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getRoute_id_textDirtyFlag())
            this.setRoute_id_text(srfdomain.getRoute_id_text());
        if(srfdomain.getSalesman_idDirtyFlag())
            this.setSalesman_id(srfdomain.getSalesman_id());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getOrder_idDirtyFlag())
            this.setOrder_id(srfdomain.getOrder_id());
        if(srfdomain.getEvent_idDirtyFlag())
            this.setEvent_id(srfdomain.getEvent_id());
        if(srfdomain.getLinked_line_idDirtyFlag())
            this.setLinked_line_id(srfdomain.getLinked_line_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getRoute_idDirtyFlag())
            this.setRoute_id(srfdomain.getRoute_id());
        if(srfdomain.getOrder_partner_idDirtyFlag())
            this.setOrder_partner_id(srfdomain.getOrder_partner_id());
        if(srfdomain.getProduct_uomDirtyFlag())
            this.setProduct_uom(srfdomain.getProduct_uom());
        if(srfdomain.getProduct_idDirtyFlag())
            this.setProduct_id(srfdomain.getProduct_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getProduct_packagingDirtyFlag())
            this.setProduct_packaging(srfdomain.getProduct_packaging());
        if(srfdomain.getEvent_ticket_idDirtyFlag())
            this.setEvent_ticket_id(srfdomain.getEvent_ticket_id());

    }

    public List<Sale_order_lineDTO> fromDOPage(List<Sale_order_line> poPage)   {
        if(poPage == null)
            return null;
        List<Sale_order_lineDTO> dtos=new ArrayList<Sale_order_lineDTO>();
        for(Sale_order_line domain : poPage) {
            Sale_order_lineDTO dto = new Sale_order_lineDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

