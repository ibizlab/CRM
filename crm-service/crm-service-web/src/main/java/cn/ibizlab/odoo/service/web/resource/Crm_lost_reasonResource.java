package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.web.dto.Crm_lost_reasonDTO;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lost_reason;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_lost_reasonService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lost_reasonSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Crm_lost_reason" })
@RestController
@RequestMapping("")
public class Crm_lost_reasonResource {

    @Autowired
    private ICrm_lost_reasonService crm_lost_reasonService;

    public ICrm_lost_reasonService getCrm_lost_reasonService() {
        return this.crm_lost_reasonService;
    }

    @ApiOperation(value = "获取数据", tags = {"Crm_lost_reason" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_lost_reasons/{crm_lost_reason_id}")
    public ResponseEntity<Crm_lost_reasonDTO> get(@PathVariable("crm_lost_reason_id") Integer crm_lost_reason_id) {
        Crm_lost_reasonDTO dto = new Crm_lost_reasonDTO();
        Crm_lost_reason domain = crm_lost_reasonService.get(crm_lost_reason_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Crm_lost_reason" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_lost_reasons/{crm_lost_reason_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("crm_lost_reason_id") Integer crm_lost_reason_id) {
        Crm_lost_reasonDTO crm_lost_reasondto = new Crm_lost_reasonDTO();
		Crm_lost_reason domain = new Crm_lost_reason();
		crm_lost_reasondto.setId(crm_lost_reason_id);
		domain.setId(crm_lost_reason_id);
        Boolean rst = crm_lost_reasonService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Crm_lost_reason" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_lost_reasons/{crm_lost_reason_id}")

    public ResponseEntity<Crm_lost_reasonDTO> update(@PathVariable("crm_lost_reason_id") Integer crm_lost_reason_id, @RequestBody Crm_lost_reasonDTO crm_lost_reasondto) {
		Crm_lost_reason domain = crm_lost_reasondto.toDO();
        domain.setId(crm_lost_reason_id);
		crm_lost_reasonService.update(domain);
		Crm_lost_reasonDTO dto = new Crm_lost_reasonDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Crm_lost_reason" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_lost_reasons/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Crm_lost_reasonDTO> crm_lost_reasondtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Crm_lost_reason" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_lost_reasons/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_lost_reasonDTO> crm_lost_reasondtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Crm_lost_reason" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_lost_reasons")

    public ResponseEntity<Crm_lost_reasonDTO> create(@RequestBody Crm_lost_reasonDTO crm_lost_reasondto) {
        Crm_lost_reasonDTO dto = new Crm_lost_reasonDTO();
        Crm_lost_reason domain = crm_lost_reasondto.toDO();
		crm_lost_reasonService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Crm_lost_reason" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_lost_reasons/{crm_lost_reason_id}/getdraft")

    public ResponseEntity<Crm_lost_reasonDTO> getDraft(@PathVariable("crm_lost_reason_id") Integer crm_lost_reason_id, @RequestBody Crm_lost_reasonDTO crm_lost_reasondto) {
        Crm_lost_reason crm_lost_reason = crm_lost_reasondto.toDO();
    	crm_lost_reason = crm_lost_reasonService.getDraft(crm_lost_reason) ;
    	crm_lost_reasondto.fromDO(crm_lost_reason);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lost_reasondto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Crm_lost_reason" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_lost_reasons/createBatch")
    public ResponseEntity<Boolean> createBatchCrm_lost_reason(@RequestBody List<Crm_lost_reasonDTO> crm_lost_reasondtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Crm_lost_reason" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/crm_lost_reasons/fetchdefault")
	public ResponseEntity<Page<Crm_lost_reasonDTO>> fetchDefault(Crm_lost_reasonSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Crm_lost_reasonDTO> list = new ArrayList<Crm_lost_reasonDTO>();
        
        Page<Crm_lost_reason> domains = crm_lost_reasonService.searchDefault(context) ;
        for(Crm_lost_reason crm_lost_reason : domains.getContent()){
            Crm_lost_reasonDTO dto = new Crm_lost_reasonDTO();
            dto.fromDO(crm_lost_reason);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
