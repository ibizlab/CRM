package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.web.dto.Crm_lead2opportunity_partner_massDTO;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead2opportunity_partner_mass;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_lead2opportunity_partner_massService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lead2opportunity_partner_massSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Crm_lead2opportunity_partner_mass" })
@RestController
@RequestMapping("")
public class Crm_lead2opportunity_partner_massResource {

    @Autowired
    private ICrm_lead2opportunity_partner_massService crm_lead2opportunity_partner_massService;

    public ICrm_lead2opportunity_partner_massService getCrm_lead2opportunity_partner_massService() {
        return this.crm_lead2opportunity_partner_massService;
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Crm_lead2opportunity_partner_mass" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_lead2opportunity_partner_masses/{crm_lead2opportunity_partner_mass_id}/getdraft")

    public ResponseEntity<Crm_lead2opportunity_partner_massDTO> getDraft(@PathVariable("crm_lead2opportunity_partner_mass_id") Integer crm_lead2opportunity_partner_mass_id, @RequestBody Crm_lead2opportunity_partner_massDTO crm_lead2opportunity_partner_massdto) {
        Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass = crm_lead2opportunity_partner_massdto.toDO();
    	crm_lead2opportunity_partner_mass = crm_lead2opportunity_partner_massService.getDraft(crm_lead2opportunity_partner_mass) ;
    	crm_lead2opportunity_partner_massdto.fromDO(crm_lead2opportunity_partner_mass);
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2opportunity_partner_massdto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Crm_lead2opportunity_partner_mass" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_lead2opportunity_partner_masses/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Crm_lead2opportunity_partner_massDTO> crm_lead2opportunity_partner_massdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Crm_lead2opportunity_partner_mass" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_lead2opportunity_partner_masses/createBatch")
    public ResponseEntity<Boolean> createBatchCrm_lead2opportunity_partner_mass(@RequestBody List<Crm_lead2opportunity_partner_massDTO> crm_lead2opportunity_partner_massdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Crm_lead2opportunity_partner_mass" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/crm_lead2opportunity_partner_masses/{crm_lead2opportunity_partner_mass_id}")
    public ResponseEntity<Crm_lead2opportunity_partner_massDTO> get(@PathVariable("crm_lead2opportunity_partner_mass_id") Integer crm_lead2opportunity_partner_mass_id) {
        Crm_lead2opportunity_partner_massDTO dto = new Crm_lead2opportunity_partner_massDTO();
        Crm_lead2opportunity_partner_mass domain = crm_lead2opportunity_partner_massService.get(crm_lead2opportunity_partner_mass_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Crm_lead2opportunity_partner_mass" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_lead2opportunity_partner_masses/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_lead2opportunity_partner_massDTO> crm_lead2opportunity_partner_massdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Crm_lead2opportunity_partner_mass" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/crm_lead2opportunity_partner_masses")

    public ResponseEntity<Crm_lead2opportunity_partner_massDTO> create(@RequestBody Crm_lead2opportunity_partner_massDTO crm_lead2opportunity_partner_massdto) {
        Crm_lead2opportunity_partner_massDTO dto = new Crm_lead2opportunity_partner_massDTO();
        Crm_lead2opportunity_partner_mass domain = crm_lead2opportunity_partner_massdto.toDO();
		crm_lead2opportunity_partner_massService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Crm_lead2opportunity_partner_mass" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/crm_lead2opportunity_partner_masses/{crm_lead2opportunity_partner_mass_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("crm_lead2opportunity_partner_mass_id") Integer crm_lead2opportunity_partner_mass_id) {
        Crm_lead2opportunity_partner_massDTO crm_lead2opportunity_partner_massdto = new Crm_lead2opportunity_partner_massDTO();
		Crm_lead2opportunity_partner_mass domain = new Crm_lead2opportunity_partner_mass();
		crm_lead2opportunity_partner_massdto.setId(crm_lead2opportunity_partner_mass_id);
		domain.setId(crm_lead2opportunity_partner_mass_id);
        Boolean rst = crm_lead2opportunity_partner_massService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Crm_lead2opportunity_partner_mass" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/crm_lead2opportunity_partner_masses/{crm_lead2opportunity_partner_mass_id}")

    public ResponseEntity<Crm_lead2opportunity_partner_massDTO> update(@PathVariable("crm_lead2opportunity_partner_mass_id") Integer crm_lead2opportunity_partner_mass_id, @RequestBody Crm_lead2opportunity_partner_massDTO crm_lead2opportunity_partner_massdto) {
		Crm_lead2opportunity_partner_mass domain = crm_lead2opportunity_partner_massdto.toDO();
        domain.setId(crm_lead2opportunity_partner_mass_id);
		crm_lead2opportunity_partner_massService.update(domain);
		Crm_lead2opportunity_partner_massDTO dto = new Crm_lead2opportunity_partner_massDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Crm_lead2opportunity_partner_mass" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/crm_lead2opportunity_partner_masses/fetchdefault")
	public ResponseEntity<Page<Crm_lead2opportunity_partner_massDTO>> fetchDefault(Crm_lead2opportunity_partner_massSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Crm_lead2opportunity_partner_massDTO> list = new ArrayList<Crm_lead2opportunity_partner_massDTO>();
        
        Page<Crm_lead2opportunity_partner_mass> domains = crm_lead2opportunity_partner_massService.searchDefault(context) ;
        for(Crm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass : domains.getContent()){
            Crm_lead2opportunity_partner_massDTO dto = new Crm_lead2opportunity_partner_massDTO();
            dto.fromDO(crm_lead2opportunity_partner_mass);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
