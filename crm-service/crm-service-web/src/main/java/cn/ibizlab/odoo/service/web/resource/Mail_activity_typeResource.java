package cn.ibizlab.odoo.service.web.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.web.dto.Mail_activity_typeDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity_type;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_activity_typeService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_activity_typeSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_activity_type" })
@RestController
@RequestMapping("")
public class Mail_activity_typeResource {

    @Autowired
    private IMail_activity_typeService mail_activity_typeService;

    public IMail_activity_typeService getMail_activity_typeService() {
        return this.mail_activity_typeService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_activity_type" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/mail_activity_types/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_activity_typeDTO> mail_activity_typedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_activity_type" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/mail_activity_types/createBatch")
    public ResponseEntity<Boolean> createBatchMail_activity_type(@RequestBody List<Mail_activity_typeDTO> mail_activity_typedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_activity_type" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web/mail_activity_types/{mail_activity_type_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_activity_type_id") Integer mail_activity_type_id) {
        Mail_activity_typeDTO mail_activity_typedto = new Mail_activity_typeDTO();
		Mail_activity_type domain = new Mail_activity_type();
		mail_activity_typedto.setId(mail_activity_type_id);
		domain.setId(mail_activity_type_id);
        Boolean rst = mail_activity_typeService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_activity_type" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/mail_activity_types/{mail_activity_type_id}")
    public ResponseEntity<Mail_activity_typeDTO> get(@PathVariable("mail_activity_type_id") Integer mail_activity_type_id) {
        Mail_activity_typeDTO dto = new Mail_activity_typeDTO();
        Mail_activity_type domain = mail_activity_typeService.get(mail_activity_type_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取草稿数据", tags = {"Mail_activity_type" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web/mail_activity_types/{mail_activity_type_id}/getdraft")

    public ResponseEntity<Mail_activity_typeDTO> getDraft(@PathVariable("mail_activity_type_id") Integer mail_activity_type_id, @RequestBody Mail_activity_typeDTO mail_activity_typedto) {
        Mail_activity_type mail_activity_type = mail_activity_typedto.toDO();
    	mail_activity_type = mail_activity_typeService.getDraft(mail_activity_type) ;
    	mail_activity_typedto.fromDO(mail_activity_type);
        return ResponseEntity.status(HttpStatus.OK).body(mail_activity_typedto);
    }

    @ApiOperation(value = "CheckKey", tags = {"Mail_activity_type" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/web/mail_activity_types/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_activity_typeDTO mail_activity_typedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_activity_type" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web/mail_activity_types")

    public ResponseEntity<Mail_activity_typeDTO> create(@RequestBody Mail_activity_typeDTO mail_activity_typedto) {
        Mail_activity_typeDTO dto = new Mail_activity_typeDTO();
        Mail_activity_type domain = mail_activity_typedto.toDO();
		mail_activity_typeService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_activity_type" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/mail_activity_types/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_activity_typeDTO> mail_activity_typedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_activity_type" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web/mail_activity_types/{mail_activity_type_id}")

    public ResponseEntity<Mail_activity_typeDTO> update(@PathVariable("mail_activity_type_id") Integer mail_activity_type_id, @RequestBody Mail_activity_typeDTO mail_activity_typedto) {
		Mail_activity_type domain = mail_activity_typedto.toDO();
        domain.setId(mail_activity_type_id);
		mail_activity_typeService.update(domain);
		Mail_activity_typeDTO dto = new Mail_activity_typeDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "Save", tags = {"Mail_activity_type" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/web/mail_activity_types/{mail_activity_type_id}/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_activity_typeDTO mail_activity_typedto) {
        Mail_activity_type mail_activity_type = mail_activity_typedto.toDO();
    	Boolean b = mail_activity_typeService.save(mail_activity_type) ;
        return ResponseEntity.status(HttpStatus.OK).body(b);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_activity_type" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web/mail_activity_types/fetchdefault")
	public ResponseEntity<Page<Mail_activity_typeDTO>> fetchDefault(Mail_activity_typeSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_activity_typeDTO> list = new ArrayList<Mail_activity_typeDTO>();
        
        Page<Mail_activity_type> domains = mail_activity_typeService.searchDefault(context) ;
        for(Mail_activity_type mail_activity_type : domains.getContent()){
            Mail_activity_typeDTO dto = new Mail_activity_typeDTO();
            dto.fromDO(mail_activity_type);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
