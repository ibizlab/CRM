package cn.ibizlab.odoo.service.web.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_sale.valuerule.anno.sale_order.*;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Sale_orderDTO]
 */
public class Sale_orderDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DATE_ORDER]
     *
     */
    @Sale_orderDate_orderDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_order;

    @JsonIgnore
    private boolean date_orderDirtyFlag;

    /**
     * 属性 [INVOICE_IDS]
     *
     */
    @Sale_orderInvoice_idsDefault(info = "默认规则")
    private String invoice_ids;

    @JsonIgnore
    private boolean invoice_idsDirtyFlag;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @Sale_orderActivity_type_idDefault(info = "默认规则")
    private Integer activity_type_id;

    @JsonIgnore
    private boolean activity_type_idDirtyFlag;

    /**
     * 属性 [AMOUNT_UNTAXED]
     *
     */
    @Sale_orderAmount_untaxedDefault(info = "默认规则")
    private Double amount_untaxed;

    @JsonIgnore
    private boolean amount_untaxedDirtyFlag;

    /**
     * 属性 [VALIDITY_DATE]
     *
     */
    @Sale_orderValidity_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp validity_date;

    @JsonIgnore
    private boolean validity_dateDirtyFlag;

    /**
     * 属性 [AMOUNT_UNDISCOUNTED]
     *
     */
    @Sale_orderAmount_undiscountedDefault(info = "默认规则")
    private Double amount_undiscounted;

    @JsonIgnore
    private boolean amount_undiscountedDirtyFlag;

    /**
     * 属性 [INVOICE_COUNT]
     *
     */
    @Sale_orderInvoice_countDefault(info = "默认规则")
    private Integer invoice_count;

    @JsonIgnore
    private boolean invoice_countDirtyFlag;

    /**
     * 属性 [ACCESS_WARNING]
     *
     */
    @Sale_orderAccess_warningDefault(info = "默认规则")
    private String access_warning;

    @JsonIgnore
    private boolean access_warningDirtyFlag;

    /**
     * 属性 [WARNING_STOCK]
     *
     */
    @Sale_orderWarning_stockDefault(info = "默认规则")
    private String warning_stock;

    @JsonIgnore
    private boolean warning_stockDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Sale_orderMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [EFFECTIVE_DATE]
     *
     */
    @Sale_orderEffective_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp effective_date;

    @JsonIgnore
    private boolean effective_dateDirtyFlag;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @Sale_orderWebsite_idDefault(info = "默认规则")
    private Integer website_id;

    @JsonIgnore
    private boolean website_idDirtyFlag;

    /**
     * 属性 [REQUIRE_SIGNATURE]
     *
     */
    @Sale_orderRequire_signatureDefault(info = "默认规则")
    private String require_signature;

    @JsonIgnore
    private boolean require_signatureDirtyFlag;

    /**
     * 属性 [CURRENCY_RATE]
     *
     */
    @Sale_orderCurrency_rateDefault(info = "默认规则")
    private Double currency_rate;

    @JsonIgnore
    private boolean currency_rateDirtyFlag;

    /**
     * 属性 [PICKING_POLICY]
     *
     */
    @Sale_orderPicking_policyDefault(info = "默认规则")
    private String picking_policy;

    @JsonIgnore
    private boolean picking_policyDirtyFlag;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @Sale_orderActivity_date_deadlineDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp activity_date_deadline;

    @JsonIgnore
    private boolean activity_date_deadlineDirtyFlag;

    /**
     * 属性 [SIGNATURE]
     *
     */
    @Sale_orderSignatureDefault(info = "默认规则")
    private byte[] signature;

    @JsonIgnore
    private boolean signatureDirtyFlag;

    /**
     * 属性 [PROCUREMENT_GROUP_ID]
     *
     */
    @Sale_orderProcurement_group_idDefault(info = "默认规则")
    private Integer procurement_group_id;

    @JsonIgnore
    private boolean procurement_group_idDirtyFlag;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @Sale_orderActivity_stateDefault(info = "默认规则")
    private String activity_state;

    @JsonIgnore
    private boolean activity_stateDirtyFlag;

    /**
     * 属性 [CART_QUANTITY]
     *
     */
    @Sale_orderCart_quantityDefault(info = "默认规则")
    private Integer cart_quantity;

    @JsonIgnore
    private boolean cart_quantityDirtyFlag;

    /**
     * 属性 [TYPE_NAME]
     *
     */
    @Sale_orderType_nameDefault(info = "默认规则")
    private String type_name;

    @JsonIgnore
    private boolean type_nameDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Sale_orderMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @Sale_orderActivity_summaryDefault(info = "默认规则")
    private String activity_summary;

    @JsonIgnore
    private boolean activity_summaryDirtyFlag;

    /**
     * 属性 [DELIVERY_COUNT]
     *
     */
    @Sale_orderDelivery_countDefault(info = "默认规则")
    private Integer delivery_count;

    @JsonIgnore
    private boolean delivery_countDirtyFlag;

    /**
     * 属性 [SIGNED_BY]
     *
     */
    @Sale_orderSigned_byDefault(info = "默认规则")
    private String signed_by;

    @JsonIgnore
    private boolean signed_byDirtyFlag;

    /**
     * 属性 [ORDER_LINE]
     *
     */
    @Sale_orderOrder_lineDefault(info = "默认规则")
    private String order_line;

    @JsonIgnore
    private boolean order_lineDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Sale_orderCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [REQUIRE_PAYMENT]
     *
     */
    @Sale_orderRequire_paymentDefault(info = "默认规则")
    private String require_payment;

    @JsonIgnore
    private boolean require_paymentDirtyFlag;

    /**
     * 属性 [ONLY_SERVICES]
     *
     */
    @Sale_orderOnly_servicesDefault(info = "默认规则")
    private String only_services;

    @JsonIgnore
    private boolean only_servicesDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Sale_orderMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [WEBSITE_ORDER_LINE]
     *
     */
    @Sale_orderWebsite_order_lineDefault(info = "默认规则")
    private String website_order_line;

    @JsonIgnore
    private boolean website_order_lineDirtyFlag;

    /**
     * 属性 [REFERENCE]
     *
     */
    @Sale_orderReferenceDefault(info = "默认规则")
    private String reference;

    @JsonIgnore
    private boolean referenceDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Sale_orderMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Sale_orderWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [INVOICE_STATUS]
     *
     */
    @Sale_orderInvoice_statusDefault(info = "默认规则")
    private String invoice_status;

    @JsonIgnore
    private boolean invoice_statusDirtyFlag;

    /**
     * 属性 [TAG_IDS]
     *
     */
    @Sale_orderTag_idsDefault(info = "默认规则")
    private String tag_ids;

    @JsonIgnore
    private boolean tag_idsDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Sale_orderIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [TRANSACTION_IDS]
     *
     */
    @Sale_orderTransaction_idsDefault(info = "默认规则")
    private String transaction_ids;

    @JsonIgnore
    private boolean transaction_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Sale_orderMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [IS_ABANDONED_CART]
     *
     */
    @Sale_orderIs_abandoned_cartDefault(info = "默认规则")
    private String is_abandoned_cart;

    @JsonIgnore
    private boolean is_abandoned_cartDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Sale_orderStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Sale_orderMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [EXPENSE_COUNT]
     *
     */
    @Sale_orderExpense_countDefault(info = "默认规则")
    private Integer expense_count;

    @JsonIgnore
    private boolean expense_countDirtyFlag;

    /**
     * 属性 [AUTHORIZED_TRANSACTION_IDS]
     *
     */
    @Sale_orderAuthorized_transaction_idsDefault(info = "默认规则")
    private String authorized_transaction_ids;

    @JsonIgnore
    private boolean authorized_transaction_idsDirtyFlag;

    /**
     * 属性 [COMMITMENT_DATE]
     *
     */
    @Sale_orderCommitment_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp commitment_date;

    @JsonIgnore
    private boolean commitment_dateDirtyFlag;

    /**
     * 属性 [IS_EXPIRED]
     *
     */
    @Sale_orderIs_expiredDefault(info = "默认规则")
    private String is_expired;

    @JsonIgnore
    private boolean is_expiredDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Sale_orderWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [REMAINING_VALIDITY_DAYS]
     *
     */
    @Sale_orderRemaining_validity_daysDefault(info = "默认规则")
    private Integer remaining_validity_days;

    @JsonIgnore
    private boolean remaining_validity_daysDirtyFlag;

    /**
     * 属性 [EXPECTED_DATE]
     *
     */
    @Sale_orderExpected_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp expected_date;

    @JsonIgnore
    private boolean expected_dateDirtyFlag;

    /**
     * 属性 [PURCHASE_ORDER_COUNT]
     *
     */
    @Sale_orderPurchase_order_countDefault(info = "默认规则")
    private Integer purchase_order_count;

    @JsonIgnore
    private boolean purchase_order_countDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Sale_orderMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [ACCESS_URL]
     *
     */
    @Sale_orderAccess_urlDefault(info = "默认规则")
    private String access_url;

    @JsonIgnore
    private boolean access_urlDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Sale_orderNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Sale_orderMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [SALE_ORDER_OPTION_IDS]
     *
     */
    @Sale_orderSale_order_option_idsDefault(info = "默认规则")
    private String sale_order_option_ids;

    @JsonIgnore
    private boolean sale_order_option_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Sale_orderMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [ORIGIN]
     *
     */
    @Sale_orderOriginDefault(info = "默认规则")
    private String origin;

    @JsonIgnore
    private boolean originDirtyFlag;

    /**
     * 属性 [CONFIRMATION_DATE]
     *
     */
    @Sale_orderConfirmation_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp confirmation_date;

    @JsonIgnore
    private boolean confirmation_dateDirtyFlag;

    /**
     * 属性 [NOTE]
     *
     */
    @Sale_orderNoteDefault(info = "默认规则")
    private String note;

    @JsonIgnore
    private boolean noteDirtyFlag;

    /**
     * 属性 [AMOUNT_BY_GROUP]
     *
     */
    @Sale_orderAmount_by_groupDefault(info = "默认规则")
    private byte[] amount_by_group;

    @JsonIgnore
    private boolean amount_by_groupDirtyFlag;

    /**
     * 属性 [PICKING_IDS]
     *
     */
    @Sale_orderPicking_idsDefault(info = "默认规则")
    private String picking_ids;

    @JsonIgnore
    private boolean picking_idsDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Sale_order__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @Sale_orderActivity_idsDefault(info = "默认规则")
    private String activity_ids;

    @JsonIgnore
    private boolean activity_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Sale_orderMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [ACCESS_TOKEN]
     *
     */
    @Sale_orderAccess_tokenDefault(info = "默认规则")
    private String access_token;

    @JsonIgnore
    private boolean access_tokenDirtyFlag;

    /**
     * 属性 [AMOUNT_TOTAL]
     *
     */
    @Sale_orderAmount_totalDefault(info = "默认规则")
    private Double amount_total;

    @JsonIgnore
    private boolean amount_totalDirtyFlag;

    /**
     * 属性 [CLIENT_ORDER_REF]
     *
     */
    @Sale_orderClient_order_refDefault(info = "默认规则")
    private String client_order_ref;

    @JsonIgnore
    private boolean client_order_refDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Sale_orderMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @Sale_orderActivity_user_idDefault(info = "默认规则")
    private Integer activity_user_id;

    @JsonIgnore
    private boolean activity_user_idDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Sale_orderMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [CART_RECOVERY_EMAIL_SENT]
     *
     */
    @Sale_orderCart_recovery_email_sentDefault(info = "默认规则")
    private String cart_recovery_email_sent;

    @JsonIgnore
    private boolean cart_recovery_email_sentDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Sale_orderDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Sale_orderMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [AMOUNT_TAX]
     *
     */
    @Sale_orderAmount_taxDefault(info = "默认规则")
    private Double amount_tax;

    @JsonIgnore
    private boolean amount_taxDirtyFlag;

    /**
     * 属性 [EXPENSE_IDS]
     *
     */
    @Sale_orderExpense_idsDefault(info = "默认规则")
    private String expense_ids;

    @JsonIgnore
    private boolean expense_idsDirtyFlag;

    /**
     * 属性 [PARTNER_INVOICE_ID_TEXT]
     *
     */
    @Sale_orderPartner_invoice_id_textDefault(info = "默认规则")
    private String partner_invoice_id_text;

    @JsonIgnore
    private boolean partner_invoice_id_textDirtyFlag;

    /**
     * 属性 [INCOTERM_TEXT]
     *
     */
    @Sale_orderIncoterm_textDefault(info = "默认规则")
    private String incoterm_text;

    @JsonIgnore
    private boolean incoterm_textDirtyFlag;

    /**
     * 属性 [TEAM_ID_TEXT]
     *
     */
    @Sale_orderTeam_id_textDefault(info = "默认规则")
    private String team_id_text;

    @JsonIgnore
    private boolean team_id_textDirtyFlag;

    /**
     * 属性 [ANALYTIC_ACCOUNT_ID_TEXT]
     *
     */
    @Sale_orderAnalytic_account_id_textDefault(info = "默认规则")
    private String analytic_account_id_text;

    @JsonIgnore
    private boolean analytic_account_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Sale_orderPartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [CAMPAIGN_ID_TEXT]
     *
     */
    @Sale_orderCampaign_id_textDefault(info = "默认规则")
    private String campaign_id_text;

    @JsonIgnore
    private boolean campaign_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Sale_orderCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Sale_orderCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_SHIPPING_ID_TEXT]
     *
     */
    @Sale_orderPartner_shipping_id_textDefault(info = "默认规则")
    private String partner_shipping_id_text;

    @JsonIgnore
    private boolean partner_shipping_id_textDirtyFlag;

    /**
     * 属性 [WAREHOUSE_ID_TEXT]
     *
     */
    @Sale_orderWarehouse_id_textDefault(info = "默认规则")
    private String warehouse_id_text;

    @JsonIgnore
    private boolean warehouse_id_textDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Sale_orderUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [OPPORTUNITY_ID_TEXT]
     *
     */
    @Sale_orderOpportunity_id_textDefault(info = "默认规则")
    private String opportunity_id_text;

    @JsonIgnore
    private boolean opportunity_id_textDirtyFlag;

    /**
     * 属性 [SOURCE_ID_TEXT]
     *
     */
    @Sale_orderSource_id_textDefault(info = "默认规则")
    private String source_id_text;

    @JsonIgnore
    private boolean source_id_textDirtyFlag;

    /**
     * 属性 [MEDIUM_ID_TEXT]
     *
     */
    @Sale_orderMedium_id_textDefault(info = "默认规则")
    private String medium_id_text;

    @JsonIgnore
    private boolean medium_id_textDirtyFlag;

    /**
     * 属性 [FISCAL_POSITION_ID_TEXT]
     *
     */
    @Sale_orderFiscal_position_id_textDefault(info = "默认规则")
    private String fiscal_position_id_text;

    @JsonIgnore
    private boolean fiscal_position_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Sale_orderWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [PRICELIST_ID_TEXT]
     *
     */
    @Sale_orderPricelist_id_textDefault(info = "默认规则")
    private String pricelist_id_text;

    @JsonIgnore
    private boolean pricelist_id_textDirtyFlag;

    /**
     * 属性 [PAYMENT_TERM_ID_TEXT]
     *
     */
    @Sale_orderPayment_term_id_textDefault(info = "默认规则")
    private String payment_term_id_text;

    @JsonIgnore
    private boolean payment_term_id_textDirtyFlag;

    /**
     * 属性 [SALE_ORDER_TEMPLATE_ID_TEXT]
     *
     */
    @Sale_orderSale_order_template_id_textDefault(info = "默认规则")
    private String sale_order_template_id_text;

    @JsonIgnore
    private boolean sale_order_template_id_textDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Sale_orderCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Sale_orderWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [TEAM_ID]
     *
     */
    @Sale_orderTeam_idDefault(info = "默认规则")
    private Integer team_id;

    @JsonIgnore
    private boolean team_idDirtyFlag;

    /**
     * 属性 [PARTNER_INVOICE_ID]
     *
     */
    @Sale_orderPartner_invoice_idDefault(info = "默认规则")
    private Integer partner_invoice_id;

    @JsonIgnore
    private boolean partner_invoice_idDirtyFlag;

    /**
     * 属性 [FISCAL_POSITION_ID]
     *
     */
    @Sale_orderFiscal_position_idDefault(info = "默认规则")
    private Integer fiscal_position_id;

    @JsonIgnore
    private boolean fiscal_position_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Sale_orderPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [CAMPAIGN_ID]
     *
     */
    @Sale_orderCampaign_idDefault(info = "默认规则")
    private Integer campaign_id;

    @JsonIgnore
    private boolean campaign_idDirtyFlag;

    /**
     * 属性 [SOURCE_ID]
     *
     */
    @Sale_orderSource_idDefault(info = "默认规则")
    private Integer source_id;

    @JsonIgnore
    private boolean source_idDirtyFlag;

    /**
     * 属性 [SALE_ORDER_TEMPLATE_ID]
     *
     */
    @Sale_orderSale_order_template_idDefault(info = "默认规则")
    private Integer sale_order_template_id;

    @JsonIgnore
    private boolean sale_order_template_idDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Sale_orderUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Sale_orderCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [MEDIUM_ID]
     *
     */
    @Sale_orderMedium_idDefault(info = "默认规则")
    private Integer medium_id;

    @JsonIgnore
    private boolean medium_idDirtyFlag;

    /**
     * 属性 [ANALYTIC_ACCOUNT_ID]
     *
     */
    @Sale_orderAnalytic_account_idDefault(info = "默认规则")
    private Integer analytic_account_id;

    @JsonIgnore
    private boolean analytic_account_idDirtyFlag;

    /**
     * 属性 [PAYMENT_TERM_ID]
     *
     */
    @Sale_orderPayment_term_idDefault(info = "默认规则")
    private Integer payment_term_id;

    @JsonIgnore
    private boolean payment_term_idDirtyFlag;

    /**
     * 属性 [PARTNER_SHIPPING_ID]
     *
     */
    @Sale_orderPartner_shipping_idDefault(info = "默认规则")
    private Integer partner_shipping_id;

    @JsonIgnore
    private boolean partner_shipping_idDirtyFlag;

    /**
     * 属性 [OPPORTUNITY_ID]
     *
     */
    @Sale_orderOpportunity_idDefault(info = "默认规则")
    private Integer opportunity_id;

    @JsonIgnore
    private boolean opportunity_idDirtyFlag;

    /**
     * 属性 [INCOTERM]
     *
     */
    @Sale_orderIncotermDefault(info = "默认规则")
    private Integer incoterm;

    @JsonIgnore
    private boolean incotermDirtyFlag;

    /**
     * 属性 [PRICELIST_ID]
     *
     */
    @Sale_orderPricelist_idDefault(info = "默认规则")
    private Integer pricelist_id;

    @JsonIgnore
    private boolean pricelist_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Sale_orderCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WAREHOUSE_ID]
     *
     */
    @Sale_orderWarehouse_idDefault(info = "默认规则")
    private Integer warehouse_id;

    @JsonIgnore
    private boolean warehouse_idDirtyFlag;


    /**
     * 获取 [DATE_ORDER]
     */
    @JsonProperty("date_order")
    public Timestamp getDate_order(){
        return date_order ;
    }

    /**
     * 设置 [DATE_ORDER]
     */
    @JsonProperty("date_order")
    public void setDate_order(Timestamp  date_order){
        this.date_order = date_order ;
        this.date_orderDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_ORDER]脏标记
     */
    @JsonIgnore
    public boolean getDate_orderDirtyFlag(){
        return date_orderDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_IDS]
     */
    @JsonProperty("invoice_ids")
    public String getInvoice_ids(){
        return invoice_ids ;
    }

    /**
     * 设置 [INVOICE_IDS]
     */
    @JsonProperty("invoice_ids")
    public void setInvoice_ids(String  invoice_ids){
        this.invoice_ids = invoice_ids ;
        this.invoice_idsDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_idsDirtyFlag(){
        return invoice_idsDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return activity_type_id ;
    }

    /**
     * 设置 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT_UNTAXED]
     */
    @JsonProperty("amount_untaxed")
    public Double getAmount_untaxed(){
        return amount_untaxed ;
    }

    /**
     * 设置 [AMOUNT_UNTAXED]
     */
    @JsonProperty("amount_untaxed")
    public void setAmount_untaxed(Double  amount_untaxed){
        this.amount_untaxed = amount_untaxed ;
        this.amount_untaxedDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT_UNTAXED]脏标记
     */
    @JsonIgnore
    public boolean getAmount_untaxedDirtyFlag(){
        return amount_untaxedDirtyFlag ;
    }

    /**
     * 获取 [VALIDITY_DATE]
     */
    @JsonProperty("validity_date")
    public Timestamp getValidity_date(){
        return validity_date ;
    }

    /**
     * 设置 [VALIDITY_DATE]
     */
    @JsonProperty("validity_date")
    public void setValidity_date(Timestamp  validity_date){
        this.validity_date = validity_date ;
        this.validity_dateDirtyFlag = true ;
    }

    /**
     * 获取 [VALIDITY_DATE]脏标记
     */
    @JsonIgnore
    public boolean getValidity_dateDirtyFlag(){
        return validity_dateDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT_UNDISCOUNTED]
     */
    @JsonProperty("amount_undiscounted")
    public Double getAmount_undiscounted(){
        return amount_undiscounted ;
    }

    /**
     * 设置 [AMOUNT_UNDISCOUNTED]
     */
    @JsonProperty("amount_undiscounted")
    public void setAmount_undiscounted(Double  amount_undiscounted){
        this.amount_undiscounted = amount_undiscounted ;
        this.amount_undiscountedDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT_UNDISCOUNTED]脏标记
     */
    @JsonIgnore
    public boolean getAmount_undiscountedDirtyFlag(){
        return amount_undiscountedDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_COUNT]
     */
    @JsonProperty("invoice_count")
    public Integer getInvoice_count(){
        return invoice_count ;
    }

    /**
     * 设置 [INVOICE_COUNT]
     */
    @JsonProperty("invoice_count")
    public void setInvoice_count(Integer  invoice_count){
        this.invoice_count = invoice_count ;
        this.invoice_countDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_countDirtyFlag(){
        return invoice_countDirtyFlag ;
    }

    /**
     * 获取 [ACCESS_WARNING]
     */
    @JsonProperty("access_warning")
    public String getAccess_warning(){
        return access_warning ;
    }

    /**
     * 设置 [ACCESS_WARNING]
     */
    @JsonProperty("access_warning")
    public void setAccess_warning(String  access_warning){
        this.access_warning = access_warning ;
        this.access_warningDirtyFlag = true ;
    }

    /**
     * 获取 [ACCESS_WARNING]脏标记
     */
    @JsonIgnore
    public boolean getAccess_warningDirtyFlag(){
        return access_warningDirtyFlag ;
    }

    /**
     * 获取 [WARNING_STOCK]
     */
    @JsonProperty("warning_stock")
    public String getWarning_stock(){
        return warning_stock ;
    }

    /**
     * 设置 [WARNING_STOCK]
     */
    @JsonProperty("warning_stock")
    public void setWarning_stock(String  warning_stock){
        this.warning_stock = warning_stock ;
        this.warning_stockDirtyFlag = true ;
    }

    /**
     * 获取 [WARNING_STOCK]脏标记
     */
    @JsonIgnore
    public boolean getWarning_stockDirtyFlag(){
        return warning_stockDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [EFFECTIVE_DATE]
     */
    @JsonProperty("effective_date")
    public Timestamp getEffective_date(){
        return effective_date ;
    }

    /**
     * 设置 [EFFECTIVE_DATE]
     */
    @JsonProperty("effective_date")
    public void setEffective_date(Timestamp  effective_date){
        this.effective_date = effective_date ;
        this.effective_dateDirtyFlag = true ;
    }

    /**
     * 获取 [EFFECTIVE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getEffective_dateDirtyFlag(){
        return effective_dateDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return website_id ;
    }

    /**
     * 设置 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_ID]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return website_idDirtyFlag ;
    }

    /**
     * 获取 [REQUIRE_SIGNATURE]
     */
    @JsonProperty("require_signature")
    public String getRequire_signature(){
        return require_signature ;
    }

    /**
     * 设置 [REQUIRE_SIGNATURE]
     */
    @JsonProperty("require_signature")
    public void setRequire_signature(String  require_signature){
        this.require_signature = require_signature ;
        this.require_signatureDirtyFlag = true ;
    }

    /**
     * 获取 [REQUIRE_SIGNATURE]脏标记
     */
    @JsonIgnore
    public boolean getRequire_signatureDirtyFlag(){
        return require_signatureDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_RATE]
     */
    @JsonProperty("currency_rate")
    public Double getCurrency_rate(){
        return currency_rate ;
    }

    /**
     * 设置 [CURRENCY_RATE]
     */
    @JsonProperty("currency_rate")
    public void setCurrency_rate(Double  currency_rate){
        this.currency_rate = currency_rate ;
        this.currency_rateDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_RATE]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_rateDirtyFlag(){
        return currency_rateDirtyFlag ;
    }

    /**
     * 获取 [PICKING_POLICY]
     */
    @JsonProperty("picking_policy")
    public String getPicking_policy(){
        return picking_policy ;
    }

    /**
     * 设置 [PICKING_POLICY]
     */
    @JsonProperty("picking_policy")
    public void setPicking_policy(String  picking_policy){
        this.picking_policy = picking_policy ;
        this.picking_policyDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_POLICY]脏标记
     */
    @JsonIgnore
    public boolean getPicking_policyDirtyFlag(){
        return picking_policyDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return activity_date_deadline ;
    }

    /**
     * 设置 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return activity_date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [SIGNATURE]
     */
    @JsonProperty("signature")
    public byte[] getSignature(){
        return signature ;
    }

    /**
     * 设置 [SIGNATURE]
     */
    @JsonProperty("signature")
    public void setSignature(byte[]  signature){
        this.signature = signature ;
        this.signatureDirtyFlag = true ;
    }

    /**
     * 获取 [SIGNATURE]脏标记
     */
    @JsonIgnore
    public boolean getSignatureDirtyFlag(){
        return signatureDirtyFlag ;
    }

    /**
     * 获取 [PROCUREMENT_GROUP_ID]
     */
    @JsonProperty("procurement_group_id")
    public Integer getProcurement_group_id(){
        return procurement_group_id ;
    }

    /**
     * 设置 [PROCUREMENT_GROUP_ID]
     */
    @JsonProperty("procurement_group_id")
    public void setProcurement_group_id(Integer  procurement_group_id){
        this.procurement_group_id = procurement_group_id ;
        this.procurement_group_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROCUREMENT_GROUP_ID]脏标记
     */
    @JsonIgnore
    public boolean getProcurement_group_idDirtyFlag(){
        return procurement_group_idDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return activity_state ;
    }

    /**
     * 设置 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_STATE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return activity_stateDirtyFlag ;
    }

    /**
     * 获取 [CART_QUANTITY]
     */
    @JsonProperty("cart_quantity")
    public Integer getCart_quantity(){
        return cart_quantity ;
    }

    /**
     * 设置 [CART_QUANTITY]
     */
    @JsonProperty("cart_quantity")
    public void setCart_quantity(Integer  cart_quantity){
        this.cart_quantity = cart_quantity ;
        this.cart_quantityDirtyFlag = true ;
    }

    /**
     * 获取 [CART_QUANTITY]脏标记
     */
    @JsonIgnore
    public boolean getCart_quantityDirtyFlag(){
        return cart_quantityDirtyFlag ;
    }

    /**
     * 获取 [TYPE_NAME]
     */
    @JsonProperty("type_name")
    public String getType_name(){
        return type_name ;
    }

    /**
     * 设置 [TYPE_NAME]
     */
    @JsonProperty("type_name")
    public void setType_name(String  type_name){
        this.type_name = type_name ;
        this.type_nameDirtyFlag = true ;
    }

    /**
     * 获取 [TYPE_NAME]脏标记
     */
    @JsonIgnore
    public boolean getType_nameDirtyFlag(){
        return type_nameDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return activity_summary ;
    }

    /**
     * 设置 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return activity_summaryDirtyFlag ;
    }

    /**
     * 获取 [DELIVERY_COUNT]
     */
    @JsonProperty("delivery_count")
    public Integer getDelivery_count(){
        return delivery_count ;
    }

    /**
     * 设置 [DELIVERY_COUNT]
     */
    @JsonProperty("delivery_count")
    public void setDelivery_count(Integer  delivery_count){
        this.delivery_count = delivery_count ;
        this.delivery_countDirtyFlag = true ;
    }

    /**
     * 获取 [DELIVERY_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getDelivery_countDirtyFlag(){
        return delivery_countDirtyFlag ;
    }

    /**
     * 获取 [SIGNED_BY]
     */
    @JsonProperty("signed_by")
    public String getSigned_by(){
        return signed_by ;
    }

    /**
     * 设置 [SIGNED_BY]
     */
    @JsonProperty("signed_by")
    public void setSigned_by(String  signed_by){
        this.signed_by = signed_by ;
        this.signed_byDirtyFlag = true ;
    }

    /**
     * 获取 [SIGNED_BY]脏标记
     */
    @JsonIgnore
    public boolean getSigned_byDirtyFlag(){
        return signed_byDirtyFlag ;
    }

    /**
     * 获取 [ORDER_LINE]
     */
    @JsonProperty("order_line")
    public String getOrder_line(){
        return order_line ;
    }

    /**
     * 设置 [ORDER_LINE]
     */
    @JsonProperty("order_line")
    public void setOrder_line(String  order_line){
        this.order_line = order_line ;
        this.order_lineDirtyFlag = true ;
    }

    /**
     * 获取 [ORDER_LINE]脏标记
     */
    @JsonIgnore
    public boolean getOrder_lineDirtyFlag(){
        return order_lineDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [REQUIRE_PAYMENT]
     */
    @JsonProperty("require_payment")
    public String getRequire_payment(){
        return require_payment ;
    }

    /**
     * 设置 [REQUIRE_PAYMENT]
     */
    @JsonProperty("require_payment")
    public void setRequire_payment(String  require_payment){
        this.require_payment = require_payment ;
        this.require_paymentDirtyFlag = true ;
    }

    /**
     * 获取 [REQUIRE_PAYMENT]脏标记
     */
    @JsonIgnore
    public boolean getRequire_paymentDirtyFlag(){
        return require_paymentDirtyFlag ;
    }

    /**
     * 获取 [ONLY_SERVICES]
     */
    @JsonProperty("only_services")
    public String getOnly_services(){
        return only_services ;
    }

    /**
     * 设置 [ONLY_SERVICES]
     */
    @JsonProperty("only_services")
    public void setOnly_services(String  only_services){
        this.only_services = only_services ;
        this.only_servicesDirtyFlag = true ;
    }

    /**
     * 获取 [ONLY_SERVICES]脏标记
     */
    @JsonIgnore
    public boolean getOnly_servicesDirtyFlag(){
        return only_servicesDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_ORDER_LINE]
     */
    @JsonProperty("website_order_line")
    public String getWebsite_order_line(){
        return website_order_line ;
    }

    /**
     * 设置 [WEBSITE_ORDER_LINE]
     */
    @JsonProperty("website_order_line")
    public void setWebsite_order_line(String  website_order_line){
        this.website_order_line = website_order_line ;
        this.website_order_lineDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_ORDER_LINE]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_order_lineDirtyFlag(){
        return website_order_lineDirtyFlag ;
    }

    /**
     * 获取 [REFERENCE]
     */
    @JsonProperty("reference")
    public String getReference(){
        return reference ;
    }

    /**
     * 设置 [REFERENCE]
     */
    @JsonProperty("reference")
    public void setReference(String  reference){
        this.reference = reference ;
        this.referenceDirtyFlag = true ;
    }

    /**
     * 获取 [REFERENCE]脏标记
     */
    @JsonIgnore
    public boolean getReferenceDirtyFlag(){
        return referenceDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_STATUS]
     */
    @JsonProperty("invoice_status")
    public String getInvoice_status(){
        return invoice_status ;
    }

    /**
     * 设置 [INVOICE_STATUS]
     */
    @JsonProperty("invoice_status")
    public void setInvoice_status(String  invoice_status){
        this.invoice_status = invoice_status ;
        this.invoice_statusDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_STATUS]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_statusDirtyFlag(){
        return invoice_statusDirtyFlag ;
    }

    /**
     * 获取 [TAG_IDS]
     */
    @JsonProperty("tag_ids")
    public String getTag_ids(){
        return tag_ids ;
    }

    /**
     * 设置 [TAG_IDS]
     */
    @JsonProperty("tag_ids")
    public void setTag_ids(String  tag_ids){
        this.tag_ids = tag_ids ;
        this.tag_idsDirtyFlag = true ;
    }

    /**
     * 获取 [TAG_IDS]脏标记
     */
    @JsonIgnore
    public boolean getTag_idsDirtyFlag(){
        return tag_idsDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [TRANSACTION_IDS]
     */
    @JsonProperty("transaction_ids")
    public String getTransaction_ids(){
        return transaction_ids ;
    }

    /**
     * 设置 [TRANSACTION_IDS]
     */
    @JsonProperty("transaction_ids")
    public void setTransaction_ids(String  transaction_ids){
        this.transaction_ids = transaction_ids ;
        this.transaction_idsDirtyFlag = true ;
    }

    /**
     * 获取 [TRANSACTION_IDS]脏标记
     */
    @JsonIgnore
    public boolean getTransaction_idsDirtyFlag(){
        return transaction_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [IS_ABANDONED_CART]
     */
    @JsonProperty("is_abandoned_cart")
    public String getIs_abandoned_cart(){
        return is_abandoned_cart ;
    }

    /**
     * 设置 [IS_ABANDONED_CART]
     */
    @JsonProperty("is_abandoned_cart")
    public void setIs_abandoned_cart(String  is_abandoned_cart){
        this.is_abandoned_cart = is_abandoned_cart ;
        this.is_abandoned_cartDirtyFlag = true ;
    }

    /**
     * 获取 [IS_ABANDONED_CART]脏标记
     */
    @JsonIgnore
    public boolean getIs_abandoned_cartDirtyFlag(){
        return is_abandoned_cartDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [EXPENSE_COUNT]
     */
    @JsonProperty("expense_count")
    public Integer getExpense_count(){
        return expense_count ;
    }

    /**
     * 设置 [EXPENSE_COUNT]
     */
    @JsonProperty("expense_count")
    public void setExpense_count(Integer  expense_count){
        this.expense_count = expense_count ;
        this.expense_countDirtyFlag = true ;
    }

    /**
     * 获取 [EXPENSE_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getExpense_countDirtyFlag(){
        return expense_countDirtyFlag ;
    }

    /**
     * 获取 [AUTHORIZED_TRANSACTION_IDS]
     */
    @JsonProperty("authorized_transaction_ids")
    public String getAuthorized_transaction_ids(){
        return authorized_transaction_ids ;
    }

    /**
     * 设置 [AUTHORIZED_TRANSACTION_IDS]
     */
    @JsonProperty("authorized_transaction_ids")
    public void setAuthorized_transaction_ids(String  authorized_transaction_ids){
        this.authorized_transaction_ids = authorized_transaction_ids ;
        this.authorized_transaction_idsDirtyFlag = true ;
    }

    /**
     * 获取 [AUTHORIZED_TRANSACTION_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAuthorized_transaction_idsDirtyFlag(){
        return authorized_transaction_idsDirtyFlag ;
    }

    /**
     * 获取 [COMMITMENT_DATE]
     */
    @JsonProperty("commitment_date")
    public Timestamp getCommitment_date(){
        return commitment_date ;
    }

    /**
     * 设置 [COMMITMENT_DATE]
     */
    @JsonProperty("commitment_date")
    public void setCommitment_date(Timestamp  commitment_date){
        this.commitment_date = commitment_date ;
        this.commitment_dateDirtyFlag = true ;
    }

    /**
     * 获取 [COMMITMENT_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCommitment_dateDirtyFlag(){
        return commitment_dateDirtyFlag ;
    }

    /**
     * 获取 [IS_EXPIRED]
     */
    @JsonProperty("is_expired")
    public String getIs_expired(){
        return is_expired ;
    }

    /**
     * 设置 [IS_EXPIRED]
     */
    @JsonProperty("is_expired")
    public void setIs_expired(String  is_expired){
        this.is_expired = is_expired ;
        this.is_expiredDirtyFlag = true ;
    }

    /**
     * 获取 [IS_EXPIRED]脏标记
     */
    @JsonIgnore
    public boolean getIs_expiredDirtyFlag(){
        return is_expiredDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [REMAINING_VALIDITY_DAYS]
     */
    @JsonProperty("remaining_validity_days")
    public Integer getRemaining_validity_days(){
        return remaining_validity_days ;
    }

    /**
     * 设置 [REMAINING_VALIDITY_DAYS]
     */
    @JsonProperty("remaining_validity_days")
    public void setRemaining_validity_days(Integer  remaining_validity_days){
        this.remaining_validity_days = remaining_validity_days ;
        this.remaining_validity_daysDirtyFlag = true ;
    }

    /**
     * 获取 [REMAINING_VALIDITY_DAYS]脏标记
     */
    @JsonIgnore
    public boolean getRemaining_validity_daysDirtyFlag(){
        return remaining_validity_daysDirtyFlag ;
    }

    /**
     * 获取 [EXPECTED_DATE]
     */
    @JsonProperty("expected_date")
    public Timestamp getExpected_date(){
        return expected_date ;
    }

    /**
     * 设置 [EXPECTED_DATE]
     */
    @JsonProperty("expected_date")
    public void setExpected_date(Timestamp  expected_date){
        this.expected_date = expected_date ;
        this.expected_dateDirtyFlag = true ;
    }

    /**
     * 获取 [EXPECTED_DATE]脏标记
     */
    @JsonIgnore
    public boolean getExpected_dateDirtyFlag(){
        return expected_dateDirtyFlag ;
    }

    /**
     * 获取 [PURCHASE_ORDER_COUNT]
     */
    @JsonProperty("purchase_order_count")
    public Integer getPurchase_order_count(){
        return purchase_order_count ;
    }

    /**
     * 设置 [PURCHASE_ORDER_COUNT]
     */
    @JsonProperty("purchase_order_count")
    public void setPurchase_order_count(Integer  purchase_order_count){
        this.purchase_order_count = purchase_order_count ;
        this.purchase_order_countDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASE_ORDER_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_order_countDirtyFlag(){
        return purchase_order_countDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [ACCESS_URL]
     */
    @JsonProperty("access_url")
    public String getAccess_url(){
        return access_url ;
    }

    /**
     * 设置 [ACCESS_URL]
     */
    @JsonProperty("access_url")
    public void setAccess_url(String  access_url){
        this.access_url = access_url ;
        this.access_urlDirtyFlag = true ;
    }

    /**
     * 获取 [ACCESS_URL]脏标记
     */
    @JsonIgnore
    public boolean getAccess_urlDirtyFlag(){
        return access_urlDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [SALE_ORDER_OPTION_IDS]
     */
    @JsonProperty("sale_order_option_ids")
    public String getSale_order_option_ids(){
        return sale_order_option_ids ;
    }

    /**
     * 设置 [SALE_ORDER_OPTION_IDS]
     */
    @JsonProperty("sale_order_option_ids")
    public void setSale_order_option_ids(String  sale_order_option_ids){
        this.sale_order_option_ids = sale_order_option_ids ;
        this.sale_order_option_idsDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_ORDER_OPTION_IDS]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_option_idsDirtyFlag(){
        return sale_order_option_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [ORIGIN]
     */
    @JsonProperty("origin")
    public String getOrigin(){
        return origin ;
    }

    /**
     * 设置 [ORIGIN]
     */
    @JsonProperty("origin")
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.originDirtyFlag = true ;
    }

    /**
     * 获取 [ORIGIN]脏标记
     */
    @JsonIgnore
    public boolean getOriginDirtyFlag(){
        return originDirtyFlag ;
    }

    /**
     * 获取 [CONFIRMATION_DATE]
     */
    @JsonProperty("confirmation_date")
    public Timestamp getConfirmation_date(){
        return confirmation_date ;
    }

    /**
     * 设置 [CONFIRMATION_DATE]
     */
    @JsonProperty("confirmation_date")
    public void setConfirmation_date(Timestamp  confirmation_date){
        this.confirmation_date = confirmation_date ;
        this.confirmation_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CONFIRMATION_DATE]脏标记
     */
    @JsonIgnore
    public boolean getConfirmation_dateDirtyFlag(){
        return confirmation_dateDirtyFlag ;
    }

    /**
     * 获取 [NOTE]
     */
    @JsonProperty("note")
    public String getNote(){
        return note ;
    }

    /**
     * 设置 [NOTE]
     */
    @JsonProperty("note")
    public void setNote(String  note){
        this.note = note ;
        this.noteDirtyFlag = true ;
    }

    /**
     * 获取 [NOTE]脏标记
     */
    @JsonIgnore
    public boolean getNoteDirtyFlag(){
        return noteDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT_BY_GROUP]
     */
    @JsonProperty("amount_by_group")
    public byte[] getAmount_by_group(){
        return amount_by_group ;
    }

    /**
     * 设置 [AMOUNT_BY_GROUP]
     */
    @JsonProperty("amount_by_group")
    public void setAmount_by_group(byte[]  amount_by_group){
        this.amount_by_group = amount_by_group ;
        this.amount_by_groupDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT_BY_GROUP]脏标记
     */
    @JsonIgnore
    public boolean getAmount_by_groupDirtyFlag(){
        return amount_by_groupDirtyFlag ;
    }

    /**
     * 获取 [PICKING_IDS]
     */
    @JsonProperty("picking_ids")
    public String getPicking_ids(){
        return picking_ids ;
    }

    /**
     * 设置 [PICKING_IDS]
     */
    @JsonProperty("picking_ids")
    public void setPicking_ids(String  picking_ids){
        this.picking_ids = picking_ids ;
        this.picking_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_IDS]脏标记
     */
    @JsonIgnore
    public boolean getPicking_idsDirtyFlag(){
        return picking_idsDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return activity_ids ;
    }

    /**
     * 设置 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return activity_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [ACCESS_TOKEN]
     */
    @JsonProperty("access_token")
    public String getAccess_token(){
        return access_token ;
    }

    /**
     * 设置 [ACCESS_TOKEN]
     */
    @JsonProperty("access_token")
    public void setAccess_token(String  access_token){
        this.access_token = access_token ;
        this.access_tokenDirtyFlag = true ;
    }

    /**
     * 获取 [ACCESS_TOKEN]脏标记
     */
    @JsonIgnore
    public boolean getAccess_tokenDirtyFlag(){
        return access_tokenDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT_TOTAL]
     */
    @JsonProperty("amount_total")
    public Double getAmount_total(){
        return amount_total ;
    }

    /**
     * 设置 [AMOUNT_TOTAL]
     */
    @JsonProperty("amount_total")
    public void setAmount_total(Double  amount_total){
        this.amount_total = amount_total ;
        this.amount_totalDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT_TOTAL]脏标记
     */
    @JsonIgnore
    public boolean getAmount_totalDirtyFlag(){
        return amount_totalDirtyFlag ;
    }

    /**
     * 获取 [CLIENT_ORDER_REF]
     */
    @JsonProperty("client_order_ref")
    public String getClient_order_ref(){
        return client_order_ref ;
    }

    /**
     * 设置 [CLIENT_ORDER_REF]
     */
    @JsonProperty("client_order_ref")
    public void setClient_order_ref(String  client_order_ref){
        this.client_order_ref = client_order_ref ;
        this.client_order_refDirtyFlag = true ;
    }

    /**
     * 获取 [CLIENT_ORDER_REF]脏标记
     */
    @JsonIgnore
    public boolean getClient_order_refDirtyFlag(){
        return client_order_refDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return activity_user_id ;
    }

    /**
     * 设置 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return activity_user_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [CART_RECOVERY_EMAIL_SENT]
     */
    @JsonProperty("cart_recovery_email_sent")
    public String getCart_recovery_email_sent(){
        return cart_recovery_email_sent ;
    }

    /**
     * 设置 [CART_RECOVERY_EMAIL_SENT]
     */
    @JsonProperty("cart_recovery_email_sent")
    public void setCart_recovery_email_sent(String  cart_recovery_email_sent){
        this.cart_recovery_email_sent = cart_recovery_email_sent ;
        this.cart_recovery_email_sentDirtyFlag = true ;
    }

    /**
     * 获取 [CART_RECOVERY_EMAIL_SENT]脏标记
     */
    @JsonIgnore
    public boolean getCart_recovery_email_sentDirtyFlag(){
        return cart_recovery_email_sentDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT_TAX]
     */
    @JsonProperty("amount_tax")
    public Double getAmount_tax(){
        return amount_tax ;
    }

    /**
     * 设置 [AMOUNT_TAX]
     */
    @JsonProperty("amount_tax")
    public void setAmount_tax(Double  amount_tax){
        this.amount_tax = amount_tax ;
        this.amount_taxDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT_TAX]脏标记
     */
    @JsonIgnore
    public boolean getAmount_taxDirtyFlag(){
        return amount_taxDirtyFlag ;
    }

    /**
     * 获取 [EXPENSE_IDS]
     */
    @JsonProperty("expense_ids")
    public String getExpense_ids(){
        return expense_ids ;
    }

    /**
     * 设置 [EXPENSE_IDS]
     */
    @JsonProperty("expense_ids")
    public void setExpense_ids(String  expense_ids){
        this.expense_ids = expense_ids ;
        this.expense_idsDirtyFlag = true ;
    }

    /**
     * 获取 [EXPENSE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getExpense_idsDirtyFlag(){
        return expense_idsDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_INVOICE_ID_TEXT]
     */
    @JsonProperty("partner_invoice_id_text")
    public String getPartner_invoice_id_text(){
        return partner_invoice_id_text ;
    }

    /**
     * 设置 [PARTNER_INVOICE_ID_TEXT]
     */
    @JsonProperty("partner_invoice_id_text")
    public void setPartner_invoice_id_text(String  partner_invoice_id_text){
        this.partner_invoice_id_text = partner_invoice_id_text ;
        this.partner_invoice_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_INVOICE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_invoice_id_textDirtyFlag(){
        return partner_invoice_id_textDirtyFlag ;
    }

    /**
     * 获取 [INCOTERM_TEXT]
     */
    @JsonProperty("incoterm_text")
    public String getIncoterm_text(){
        return incoterm_text ;
    }

    /**
     * 设置 [INCOTERM_TEXT]
     */
    @JsonProperty("incoterm_text")
    public void setIncoterm_text(String  incoterm_text){
        this.incoterm_text = incoterm_text ;
        this.incoterm_textDirtyFlag = true ;
    }

    /**
     * 获取 [INCOTERM_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getIncoterm_textDirtyFlag(){
        return incoterm_textDirtyFlag ;
    }

    /**
     * 获取 [TEAM_ID_TEXT]
     */
    @JsonProperty("team_id_text")
    public String getTeam_id_text(){
        return team_id_text ;
    }

    /**
     * 设置 [TEAM_ID_TEXT]
     */
    @JsonProperty("team_id_text")
    public void setTeam_id_text(String  team_id_text){
        this.team_id_text = team_id_text ;
        this.team_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [TEAM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getTeam_id_textDirtyFlag(){
        return team_id_textDirtyFlag ;
    }

    /**
     * 获取 [ANALYTIC_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("analytic_account_id_text")
    public String getAnalytic_account_id_text(){
        return analytic_account_id_text ;
    }

    /**
     * 设置 [ANALYTIC_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("analytic_account_id_text")
    public void setAnalytic_account_id_text(String  analytic_account_id_text){
        this.analytic_account_id_text = analytic_account_id_text ;
        this.analytic_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ANALYTIC_ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_account_id_textDirtyFlag(){
        return analytic_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [CAMPAIGN_ID_TEXT]
     */
    @JsonProperty("campaign_id_text")
    public String getCampaign_id_text(){
        return campaign_id_text ;
    }

    /**
     * 设置 [CAMPAIGN_ID_TEXT]
     */
    @JsonProperty("campaign_id_text")
    public void setCampaign_id_text(String  campaign_id_text){
        this.campaign_id_text = campaign_id_text ;
        this.campaign_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CAMPAIGN_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_id_textDirtyFlag(){
        return campaign_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_SHIPPING_ID_TEXT]
     */
    @JsonProperty("partner_shipping_id_text")
    public String getPartner_shipping_id_text(){
        return partner_shipping_id_text ;
    }

    /**
     * 设置 [PARTNER_SHIPPING_ID_TEXT]
     */
    @JsonProperty("partner_shipping_id_text")
    public void setPartner_shipping_id_text(String  partner_shipping_id_text){
        this.partner_shipping_id_text = partner_shipping_id_text ;
        this.partner_shipping_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_SHIPPING_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_shipping_id_textDirtyFlag(){
        return partner_shipping_id_textDirtyFlag ;
    }

    /**
     * 获取 [WAREHOUSE_ID_TEXT]
     */
    @JsonProperty("warehouse_id_text")
    public String getWarehouse_id_text(){
        return warehouse_id_text ;
    }

    /**
     * 设置 [WAREHOUSE_ID_TEXT]
     */
    @JsonProperty("warehouse_id_text")
    public void setWarehouse_id_text(String  warehouse_id_text){
        this.warehouse_id_text = warehouse_id_text ;
        this.warehouse_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [WAREHOUSE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_id_textDirtyFlag(){
        return warehouse_id_textDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [OPPORTUNITY_ID_TEXT]
     */
    @JsonProperty("opportunity_id_text")
    public String getOpportunity_id_text(){
        return opportunity_id_text ;
    }

    /**
     * 设置 [OPPORTUNITY_ID_TEXT]
     */
    @JsonProperty("opportunity_id_text")
    public void setOpportunity_id_text(String  opportunity_id_text){
        this.opportunity_id_text = opportunity_id_text ;
        this.opportunity_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [OPPORTUNITY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getOpportunity_id_textDirtyFlag(){
        return opportunity_id_textDirtyFlag ;
    }

    /**
     * 获取 [SOURCE_ID_TEXT]
     */
    @JsonProperty("source_id_text")
    public String getSource_id_text(){
        return source_id_text ;
    }

    /**
     * 设置 [SOURCE_ID_TEXT]
     */
    @JsonProperty("source_id_text")
    public void setSource_id_text(String  source_id_text){
        this.source_id_text = source_id_text ;
        this.source_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SOURCE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSource_id_textDirtyFlag(){
        return source_id_textDirtyFlag ;
    }

    /**
     * 获取 [MEDIUM_ID_TEXT]
     */
    @JsonProperty("medium_id_text")
    public String getMedium_id_text(){
        return medium_id_text ;
    }

    /**
     * 设置 [MEDIUM_ID_TEXT]
     */
    @JsonProperty("medium_id_text")
    public void setMedium_id_text(String  medium_id_text){
        this.medium_id_text = medium_id_text ;
        this.medium_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MEDIUM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMedium_id_textDirtyFlag(){
        return medium_id_textDirtyFlag ;
    }

    /**
     * 获取 [FISCAL_POSITION_ID_TEXT]
     */
    @JsonProperty("fiscal_position_id_text")
    public String getFiscal_position_id_text(){
        return fiscal_position_id_text ;
    }

    /**
     * 设置 [FISCAL_POSITION_ID_TEXT]
     */
    @JsonProperty("fiscal_position_id_text")
    public void setFiscal_position_id_text(String  fiscal_position_id_text){
        this.fiscal_position_id_text = fiscal_position_id_text ;
        this.fiscal_position_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [FISCAL_POSITION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getFiscal_position_id_textDirtyFlag(){
        return fiscal_position_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PRICELIST_ID_TEXT]
     */
    @JsonProperty("pricelist_id_text")
    public String getPricelist_id_text(){
        return pricelist_id_text ;
    }

    /**
     * 设置 [PRICELIST_ID_TEXT]
     */
    @JsonProperty("pricelist_id_text")
    public void setPricelist_id_text(String  pricelist_id_text){
        this.pricelist_id_text = pricelist_id_text ;
        this.pricelist_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRICELIST_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPricelist_id_textDirtyFlag(){
        return pricelist_id_textDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_TERM_ID_TEXT]
     */
    @JsonProperty("payment_term_id_text")
    public String getPayment_term_id_text(){
        return payment_term_id_text ;
    }

    /**
     * 设置 [PAYMENT_TERM_ID_TEXT]
     */
    @JsonProperty("payment_term_id_text")
    public void setPayment_term_id_text(String  payment_term_id_text){
        this.payment_term_id_text = payment_term_id_text ;
        this.payment_term_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_TERM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPayment_term_id_textDirtyFlag(){
        return payment_term_id_textDirtyFlag ;
    }

    /**
     * 获取 [SALE_ORDER_TEMPLATE_ID_TEXT]
     */
    @JsonProperty("sale_order_template_id_text")
    public String getSale_order_template_id_text(){
        return sale_order_template_id_text ;
    }

    /**
     * 设置 [SALE_ORDER_TEMPLATE_ID_TEXT]
     */
    @JsonProperty("sale_order_template_id_text")
    public void setSale_order_template_id_text(String  sale_order_template_id_text){
        this.sale_order_template_id_text = sale_order_template_id_text ;
        this.sale_order_template_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_ORDER_TEMPLATE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_template_id_textDirtyFlag(){
        return sale_order_template_id_textDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [TEAM_ID]
     */
    @JsonProperty("team_id")
    public Integer getTeam_id(){
        return team_id ;
    }

    /**
     * 设置 [TEAM_ID]
     */
    @JsonProperty("team_id")
    public void setTeam_id(Integer  team_id){
        this.team_id = team_id ;
        this.team_idDirtyFlag = true ;
    }

    /**
     * 获取 [TEAM_ID]脏标记
     */
    @JsonIgnore
    public boolean getTeam_idDirtyFlag(){
        return team_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_INVOICE_ID]
     */
    @JsonProperty("partner_invoice_id")
    public Integer getPartner_invoice_id(){
        return partner_invoice_id ;
    }

    /**
     * 设置 [PARTNER_INVOICE_ID]
     */
    @JsonProperty("partner_invoice_id")
    public void setPartner_invoice_id(Integer  partner_invoice_id){
        this.partner_invoice_id = partner_invoice_id ;
        this.partner_invoice_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_INVOICE_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_invoice_idDirtyFlag(){
        return partner_invoice_idDirtyFlag ;
    }

    /**
     * 获取 [FISCAL_POSITION_ID]
     */
    @JsonProperty("fiscal_position_id")
    public Integer getFiscal_position_id(){
        return fiscal_position_id ;
    }

    /**
     * 设置 [FISCAL_POSITION_ID]
     */
    @JsonProperty("fiscal_position_id")
    public void setFiscal_position_id(Integer  fiscal_position_id){
        this.fiscal_position_id = fiscal_position_id ;
        this.fiscal_position_idDirtyFlag = true ;
    }

    /**
     * 获取 [FISCAL_POSITION_ID]脏标记
     */
    @JsonIgnore
    public boolean getFiscal_position_idDirtyFlag(){
        return fiscal_position_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [CAMPAIGN_ID]
     */
    @JsonProperty("campaign_id")
    public Integer getCampaign_id(){
        return campaign_id ;
    }

    /**
     * 设置 [CAMPAIGN_ID]
     */
    @JsonProperty("campaign_id")
    public void setCampaign_id(Integer  campaign_id){
        this.campaign_id = campaign_id ;
        this.campaign_idDirtyFlag = true ;
    }

    /**
     * 获取 [CAMPAIGN_ID]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_idDirtyFlag(){
        return campaign_idDirtyFlag ;
    }

    /**
     * 获取 [SOURCE_ID]
     */
    @JsonProperty("source_id")
    public Integer getSource_id(){
        return source_id ;
    }

    /**
     * 设置 [SOURCE_ID]
     */
    @JsonProperty("source_id")
    public void setSource_id(Integer  source_id){
        this.source_id = source_id ;
        this.source_idDirtyFlag = true ;
    }

    /**
     * 获取 [SOURCE_ID]脏标记
     */
    @JsonIgnore
    public boolean getSource_idDirtyFlag(){
        return source_idDirtyFlag ;
    }

    /**
     * 获取 [SALE_ORDER_TEMPLATE_ID]
     */
    @JsonProperty("sale_order_template_id")
    public Integer getSale_order_template_id(){
        return sale_order_template_id ;
    }

    /**
     * 设置 [SALE_ORDER_TEMPLATE_ID]
     */
    @JsonProperty("sale_order_template_id")
    public void setSale_order_template_id(Integer  sale_order_template_id){
        this.sale_order_template_id = sale_order_template_id ;
        this.sale_order_template_idDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_ORDER_TEMPLATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_template_idDirtyFlag(){
        return sale_order_template_idDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [MEDIUM_ID]
     */
    @JsonProperty("medium_id")
    public Integer getMedium_id(){
        return medium_id ;
    }

    /**
     * 设置 [MEDIUM_ID]
     */
    @JsonProperty("medium_id")
    public void setMedium_id(Integer  medium_id){
        this.medium_id = medium_id ;
        this.medium_idDirtyFlag = true ;
    }

    /**
     * 获取 [MEDIUM_ID]脏标记
     */
    @JsonIgnore
    public boolean getMedium_idDirtyFlag(){
        return medium_idDirtyFlag ;
    }

    /**
     * 获取 [ANALYTIC_ACCOUNT_ID]
     */
    @JsonProperty("analytic_account_id")
    public Integer getAnalytic_account_id(){
        return analytic_account_id ;
    }

    /**
     * 设置 [ANALYTIC_ACCOUNT_ID]
     */
    @JsonProperty("analytic_account_id")
    public void setAnalytic_account_id(Integer  analytic_account_id){
        this.analytic_account_id = analytic_account_id ;
        this.analytic_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [ANALYTIC_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_account_idDirtyFlag(){
        return analytic_account_idDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_TERM_ID]
     */
    @JsonProperty("payment_term_id")
    public Integer getPayment_term_id(){
        return payment_term_id ;
    }

    /**
     * 设置 [PAYMENT_TERM_ID]
     */
    @JsonProperty("payment_term_id")
    public void setPayment_term_id(Integer  payment_term_id){
        this.payment_term_id = payment_term_id ;
        this.payment_term_idDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_TERM_ID]脏标记
     */
    @JsonIgnore
    public boolean getPayment_term_idDirtyFlag(){
        return payment_term_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_SHIPPING_ID]
     */
    @JsonProperty("partner_shipping_id")
    public Integer getPartner_shipping_id(){
        return partner_shipping_id ;
    }

    /**
     * 设置 [PARTNER_SHIPPING_ID]
     */
    @JsonProperty("partner_shipping_id")
    public void setPartner_shipping_id(Integer  partner_shipping_id){
        this.partner_shipping_id = partner_shipping_id ;
        this.partner_shipping_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_SHIPPING_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_shipping_idDirtyFlag(){
        return partner_shipping_idDirtyFlag ;
    }

    /**
     * 获取 [OPPORTUNITY_ID]
     */
    @JsonProperty("opportunity_id")
    public Integer getOpportunity_id(){
        return opportunity_id ;
    }

    /**
     * 设置 [OPPORTUNITY_ID]
     */
    @JsonProperty("opportunity_id")
    public void setOpportunity_id(Integer  opportunity_id){
        this.opportunity_id = opportunity_id ;
        this.opportunity_idDirtyFlag = true ;
    }

    /**
     * 获取 [OPPORTUNITY_ID]脏标记
     */
    @JsonIgnore
    public boolean getOpportunity_idDirtyFlag(){
        return opportunity_idDirtyFlag ;
    }

    /**
     * 获取 [INCOTERM]
     */
    @JsonProperty("incoterm")
    public Integer getIncoterm(){
        return incoterm ;
    }

    /**
     * 设置 [INCOTERM]
     */
    @JsonProperty("incoterm")
    public void setIncoterm(Integer  incoterm){
        this.incoterm = incoterm ;
        this.incotermDirtyFlag = true ;
    }

    /**
     * 获取 [INCOTERM]脏标记
     */
    @JsonIgnore
    public boolean getIncotermDirtyFlag(){
        return incotermDirtyFlag ;
    }

    /**
     * 获取 [PRICELIST_ID]
     */
    @JsonProperty("pricelist_id")
    public Integer getPricelist_id(){
        return pricelist_id ;
    }

    /**
     * 设置 [PRICELIST_ID]
     */
    @JsonProperty("pricelist_id")
    public void setPricelist_id(Integer  pricelist_id){
        this.pricelist_id = pricelist_id ;
        this.pricelist_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRICELIST_ID]脏标记
     */
    @JsonIgnore
    public boolean getPricelist_idDirtyFlag(){
        return pricelist_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WAREHOUSE_ID]
     */
    @JsonProperty("warehouse_id")
    public Integer getWarehouse_id(){
        return warehouse_id ;
    }

    /**
     * 设置 [WAREHOUSE_ID]
     */
    @JsonProperty("warehouse_id")
    public void setWarehouse_id(Integer  warehouse_id){
        this.warehouse_id = warehouse_id ;
        this.warehouse_idDirtyFlag = true ;
    }

    /**
     * 获取 [WAREHOUSE_ID]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_idDirtyFlag(){
        return warehouse_idDirtyFlag ;
    }


    /**
     *  [SALE_ORDER_LINE]
     */
	private List<Sale_order_lineDTO> sale_order_lines = new ArrayList<Sale_order_lineDTO>();

    @JsonProperty("sale_order_lines")
    public List<Sale_order_lineDTO> getSale_order_lines(){
        return sale_order_lines ;
    }

    @JsonProperty("sale_order_lines")
    public void setSale_order_lines(List <Sale_order_lineDTO> sale_order_lines){
        this.sale_order_lines = sale_order_lines ;
    }


    public Sale_order toDO() {
        Sale_order srfdomain = new Sale_order();
        if(getDate_orderDirtyFlag())
            srfdomain.setDate_order(date_order);
        if(getInvoice_idsDirtyFlag())
            srfdomain.setInvoice_ids(invoice_ids);
        if(getActivity_type_idDirtyFlag())
            srfdomain.setActivity_type_id(activity_type_id);
        if(getAmount_untaxedDirtyFlag())
            srfdomain.setAmount_untaxed(amount_untaxed);
        if(getValidity_dateDirtyFlag())
            srfdomain.setValidity_date(validity_date);
        if(getAmount_undiscountedDirtyFlag())
            srfdomain.setAmount_undiscounted(amount_undiscounted);
        if(getInvoice_countDirtyFlag())
            srfdomain.setInvoice_count(invoice_count);
        if(getAccess_warningDirtyFlag())
            srfdomain.setAccess_warning(access_warning);
        if(getWarning_stockDirtyFlag())
            srfdomain.setWarning_stock(warning_stock);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getEffective_dateDirtyFlag())
            srfdomain.setEffective_date(effective_date);
        if(getWebsite_idDirtyFlag())
            srfdomain.setWebsite_id(website_id);
        if(getRequire_signatureDirtyFlag())
            srfdomain.setRequire_signature(require_signature);
        if(getCurrency_rateDirtyFlag())
            srfdomain.setCurrency_rate(currency_rate);
        if(getPicking_policyDirtyFlag())
            srfdomain.setPicking_policy(picking_policy);
        if(getActivity_date_deadlineDirtyFlag())
            srfdomain.setActivity_date_deadline(activity_date_deadline);
        if(getSignatureDirtyFlag())
            srfdomain.setSignature(signature);
        if(getProcurement_group_idDirtyFlag())
            srfdomain.setProcurement_group_id(procurement_group_id);
        if(getActivity_stateDirtyFlag())
            srfdomain.setActivity_state(activity_state);
        if(getCart_quantityDirtyFlag())
            srfdomain.setCart_quantity(cart_quantity);
        if(getType_nameDirtyFlag())
            srfdomain.setType_name(type_name);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getActivity_summaryDirtyFlag())
            srfdomain.setActivity_summary(activity_summary);
        if(getDelivery_countDirtyFlag())
            srfdomain.setDelivery_count(delivery_count);
        if(getSigned_byDirtyFlag())
            srfdomain.setSigned_by(signed_by);
        if(getOrder_lineDirtyFlag())
            srfdomain.setOrder_line(order_line);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getRequire_paymentDirtyFlag())
            srfdomain.setRequire_payment(require_payment);
        if(getOnly_servicesDirtyFlag())
            srfdomain.setOnly_services(only_services);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getWebsite_order_lineDirtyFlag())
            srfdomain.setWebsite_order_line(website_order_line);
        if(getReferenceDirtyFlag())
            srfdomain.setReference(reference);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getInvoice_statusDirtyFlag())
            srfdomain.setInvoice_status(invoice_status);
        if(getTag_idsDirtyFlag())
            srfdomain.setTag_ids(tag_ids);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getTransaction_idsDirtyFlag())
            srfdomain.setTransaction_ids(transaction_ids);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getIs_abandoned_cartDirtyFlag())
            srfdomain.setIs_abandoned_cart(is_abandoned_cart);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getExpense_countDirtyFlag())
            srfdomain.setExpense_count(expense_count);
        if(getAuthorized_transaction_idsDirtyFlag())
            srfdomain.setAuthorized_transaction_ids(authorized_transaction_ids);
        if(getCommitment_dateDirtyFlag())
            srfdomain.setCommitment_date(commitment_date);
        if(getIs_expiredDirtyFlag())
            srfdomain.setIs_expired(is_expired);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getRemaining_validity_daysDirtyFlag())
            srfdomain.setRemaining_validity_days(remaining_validity_days);
        if(getExpected_dateDirtyFlag())
            srfdomain.setExpected_date(expected_date);
        if(getPurchase_order_countDirtyFlag())
            srfdomain.setPurchase_order_count(purchase_order_count);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getAccess_urlDirtyFlag())
            srfdomain.setAccess_url(access_url);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getSale_order_option_idsDirtyFlag())
            srfdomain.setSale_order_option_ids(sale_order_option_ids);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getOriginDirtyFlag())
            srfdomain.setOrigin(origin);
        if(getConfirmation_dateDirtyFlag())
            srfdomain.setConfirmation_date(confirmation_date);
        if(getNoteDirtyFlag())
            srfdomain.setNote(note);
        if(getAmount_by_groupDirtyFlag())
            srfdomain.setAmount_by_group(amount_by_group);
        if(getPicking_idsDirtyFlag())
            srfdomain.setPicking_ids(picking_ids);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getActivity_idsDirtyFlag())
            srfdomain.setActivity_ids(activity_ids);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getAccess_tokenDirtyFlag())
            srfdomain.setAccess_token(access_token);
        if(getAmount_totalDirtyFlag())
            srfdomain.setAmount_total(amount_total);
        if(getClient_order_refDirtyFlag())
            srfdomain.setClient_order_ref(client_order_ref);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getActivity_user_idDirtyFlag())
            srfdomain.setActivity_user_id(activity_user_id);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getCart_recovery_email_sentDirtyFlag())
            srfdomain.setCart_recovery_email_sent(cart_recovery_email_sent);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getAmount_taxDirtyFlag())
            srfdomain.setAmount_tax(amount_tax);
        if(getExpense_idsDirtyFlag())
            srfdomain.setExpense_ids(expense_ids);
        if(getPartner_invoice_id_textDirtyFlag())
            srfdomain.setPartner_invoice_id_text(partner_invoice_id_text);
        if(getIncoterm_textDirtyFlag())
            srfdomain.setIncoterm_text(incoterm_text);
        if(getTeam_id_textDirtyFlag())
            srfdomain.setTeam_id_text(team_id_text);
        if(getAnalytic_account_id_textDirtyFlag())
            srfdomain.setAnalytic_account_id_text(analytic_account_id_text);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getCampaign_id_textDirtyFlag())
            srfdomain.setCampaign_id_text(campaign_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getPartner_shipping_id_textDirtyFlag())
            srfdomain.setPartner_shipping_id_text(partner_shipping_id_text);
        if(getWarehouse_id_textDirtyFlag())
            srfdomain.setWarehouse_id_text(warehouse_id_text);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getOpportunity_id_textDirtyFlag())
            srfdomain.setOpportunity_id_text(opportunity_id_text);
        if(getSource_id_textDirtyFlag())
            srfdomain.setSource_id_text(source_id_text);
        if(getMedium_id_textDirtyFlag())
            srfdomain.setMedium_id_text(medium_id_text);
        if(getFiscal_position_id_textDirtyFlag())
            srfdomain.setFiscal_position_id_text(fiscal_position_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getPricelist_id_textDirtyFlag())
            srfdomain.setPricelist_id_text(pricelist_id_text);
        if(getPayment_term_id_textDirtyFlag())
            srfdomain.setPayment_term_id_text(payment_term_id_text);
        if(getSale_order_template_id_textDirtyFlag())
            srfdomain.setSale_order_template_id_text(sale_order_template_id_text);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getTeam_idDirtyFlag())
            srfdomain.setTeam_id(team_id);
        if(getPartner_invoice_idDirtyFlag())
            srfdomain.setPartner_invoice_id(partner_invoice_id);
        if(getFiscal_position_idDirtyFlag())
            srfdomain.setFiscal_position_id(fiscal_position_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getCampaign_idDirtyFlag())
            srfdomain.setCampaign_id(campaign_id);
        if(getSource_idDirtyFlag())
            srfdomain.setSource_id(source_id);
        if(getSale_order_template_idDirtyFlag())
            srfdomain.setSale_order_template_id(sale_order_template_id);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getMedium_idDirtyFlag())
            srfdomain.setMedium_id(medium_id);
        if(getAnalytic_account_idDirtyFlag())
            srfdomain.setAnalytic_account_id(analytic_account_id);
        if(getPayment_term_idDirtyFlag())
            srfdomain.setPayment_term_id(payment_term_id);
        if(getPartner_shipping_idDirtyFlag())
            srfdomain.setPartner_shipping_id(partner_shipping_id);
        if(getOpportunity_idDirtyFlag())
            srfdomain.setOpportunity_id(opportunity_id);
        if(getIncotermDirtyFlag())
            srfdomain.setIncoterm(incoterm);
        if(getPricelist_idDirtyFlag())
            srfdomain.setPricelist_id(pricelist_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWarehouse_idDirtyFlag())
            srfdomain.setWarehouse_id(warehouse_id);

        for(Sale_order_lineDTO dto : this.getSale_order_lines()){
            srfdomain.getSale_order_lines().add(dto.toDO());    
        }

        return srfdomain;
    }

    public void fromDO(Sale_order srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getDate_orderDirtyFlag())
            this.setDate_order(srfdomain.getDate_order());
        if(srfdomain.getInvoice_idsDirtyFlag())
            this.setInvoice_ids(srfdomain.getInvoice_ids());
        if(srfdomain.getActivity_type_idDirtyFlag())
            this.setActivity_type_id(srfdomain.getActivity_type_id());
        if(srfdomain.getAmount_untaxedDirtyFlag())
            this.setAmount_untaxed(srfdomain.getAmount_untaxed());
        if(srfdomain.getValidity_dateDirtyFlag())
            this.setValidity_date(srfdomain.getValidity_date());
        if(srfdomain.getAmount_undiscountedDirtyFlag())
            this.setAmount_undiscounted(srfdomain.getAmount_undiscounted());
        if(srfdomain.getInvoice_countDirtyFlag())
            this.setInvoice_count(srfdomain.getInvoice_count());
        if(srfdomain.getAccess_warningDirtyFlag())
            this.setAccess_warning(srfdomain.getAccess_warning());
        if(srfdomain.getWarning_stockDirtyFlag())
            this.setWarning_stock(srfdomain.getWarning_stock());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getEffective_dateDirtyFlag())
            this.setEffective_date(srfdomain.getEffective_date());
        if(srfdomain.getWebsite_idDirtyFlag())
            this.setWebsite_id(srfdomain.getWebsite_id());
        if(srfdomain.getRequire_signatureDirtyFlag())
            this.setRequire_signature(srfdomain.getRequire_signature());
        if(srfdomain.getCurrency_rateDirtyFlag())
            this.setCurrency_rate(srfdomain.getCurrency_rate());
        if(srfdomain.getPicking_policyDirtyFlag())
            this.setPicking_policy(srfdomain.getPicking_policy());
        if(srfdomain.getActivity_date_deadlineDirtyFlag())
            this.setActivity_date_deadline(srfdomain.getActivity_date_deadline());
        if(srfdomain.getSignatureDirtyFlag())
            this.setSignature(srfdomain.getSignature());
        if(srfdomain.getProcurement_group_idDirtyFlag())
            this.setProcurement_group_id(srfdomain.getProcurement_group_id());
        if(srfdomain.getActivity_stateDirtyFlag())
            this.setActivity_state(srfdomain.getActivity_state());
        if(srfdomain.getCart_quantityDirtyFlag())
            this.setCart_quantity(srfdomain.getCart_quantity());
        if(srfdomain.getType_nameDirtyFlag())
            this.setType_name(srfdomain.getType_name());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getActivity_summaryDirtyFlag())
            this.setActivity_summary(srfdomain.getActivity_summary());
        if(srfdomain.getDelivery_countDirtyFlag())
            this.setDelivery_count(srfdomain.getDelivery_count());
        if(srfdomain.getSigned_byDirtyFlag())
            this.setSigned_by(srfdomain.getSigned_by());
        if(srfdomain.getOrder_lineDirtyFlag())
            this.setOrder_line(srfdomain.getOrder_line());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getRequire_paymentDirtyFlag())
            this.setRequire_payment(srfdomain.getRequire_payment());
        if(srfdomain.getOnly_servicesDirtyFlag())
            this.setOnly_services(srfdomain.getOnly_services());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getWebsite_order_lineDirtyFlag())
            this.setWebsite_order_line(srfdomain.getWebsite_order_line());
        if(srfdomain.getReferenceDirtyFlag())
            this.setReference(srfdomain.getReference());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getInvoice_statusDirtyFlag())
            this.setInvoice_status(srfdomain.getInvoice_status());
        if(srfdomain.getTag_idsDirtyFlag())
            this.setTag_ids(srfdomain.getTag_ids());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getTransaction_idsDirtyFlag())
            this.setTransaction_ids(srfdomain.getTransaction_ids());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getIs_abandoned_cartDirtyFlag())
            this.setIs_abandoned_cart(srfdomain.getIs_abandoned_cart());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getExpense_countDirtyFlag())
            this.setExpense_count(srfdomain.getExpense_count());
        if(srfdomain.getAuthorized_transaction_idsDirtyFlag())
            this.setAuthorized_transaction_ids(srfdomain.getAuthorized_transaction_ids());
        if(srfdomain.getCommitment_dateDirtyFlag())
            this.setCommitment_date(srfdomain.getCommitment_date());
        if(srfdomain.getIs_expiredDirtyFlag())
            this.setIs_expired(srfdomain.getIs_expired());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getRemaining_validity_daysDirtyFlag())
            this.setRemaining_validity_days(srfdomain.getRemaining_validity_days());
        if(srfdomain.getExpected_dateDirtyFlag())
            this.setExpected_date(srfdomain.getExpected_date());
        if(srfdomain.getPurchase_order_countDirtyFlag())
            this.setPurchase_order_count(srfdomain.getPurchase_order_count());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getAccess_urlDirtyFlag())
            this.setAccess_url(srfdomain.getAccess_url());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getSale_order_option_idsDirtyFlag())
            this.setSale_order_option_ids(srfdomain.getSale_order_option_ids());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getOriginDirtyFlag())
            this.setOrigin(srfdomain.getOrigin());
        if(srfdomain.getConfirmation_dateDirtyFlag())
            this.setConfirmation_date(srfdomain.getConfirmation_date());
        if(srfdomain.getNoteDirtyFlag())
            this.setNote(srfdomain.getNote());
        if(srfdomain.getAmount_by_groupDirtyFlag())
            this.setAmount_by_group(srfdomain.getAmount_by_group());
        if(srfdomain.getPicking_idsDirtyFlag())
            this.setPicking_ids(srfdomain.getPicking_ids());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getActivity_idsDirtyFlag())
            this.setActivity_ids(srfdomain.getActivity_ids());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getAccess_tokenDirtyFlag())
            this.setAccess_token(srfdomain.getAccess_token());
        if(srfdomain.getAmount_totalDirtyFlag())
            this.setAmount_total(srfdomain.getAmount_total());
        if(srfdomain.getClient_order_refDirtyFlag())
            this.setClient_order_ref(srfdomain.getClient_order_ref());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getActivity_user_idDirtyFlag())
            this.setActivity_user_id(srfdomain.getActivity_user_id());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getCart_recovery_email_sentDirtyFlag())
            this.setCart_recovery_email_sent(srfdomain.getCart_recovery_email_sent());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getAmount_taxDirtyFlag())
            this.setAmount_tax(srfdomain.getAmount_tax());
        if(srfdomain.getExpense_idsDirtyFlag())
            this.setExpense_ids(srfdomain.getExpense_ids());
        if(srfdomain.getPartner_invoice_id_textDirtyFlag())
            this.setPartner_invoice_id_text(srfdomain.getPartner_invoice_id_text());
        if(srfdomain.getIncoterm_textDirtyFlag())
            this.setIncoterm_text(srfdomain.getIncoterm_text());
        if(srfdomain.getTeam_id_textDirtyFlag())
            this.setTeam_id_text(srfdomain.getTeam_id_text());
        if(srfdomain.getAnalytic_account_id_textDirtyFlag())
            this.setAnalytic_account_id_text(srfdomain.getAnalytic_account_id_text());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getCampaign_id_textDirtyFlag())
            this.setCampaign_id_text(srfdomain.getCampaign_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getPartner_shipping_id_textDirtyFlag())
            this.setPartner_shipping_id_text(srfdomain.getPartner_shipping_id_text());
        if(srfdomain.getWarehouse_id_textDirtyFlag())
            this.setWarehouse_id_text(srfdomain.getWarehouse_id_text());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getOpportunity_id_textDirtyFlag())
            this.setOpportunity_id_text(srfdomain.getOpportunity_id_text());
        if(srfdomain.getSource_id_textDirtyFlag())
            this.setSource_id_text(srfdomain.getSource_id_text());
        if(srfdomain.getMedium_id_textDirtyFlag())
            this.setMedium_id_text(srfdomain.getMedium_id_text());
        if(srfdomain.getFiscal_position_id_textDirtyFlag())
            this.setFiscal_position_id_text(srfdomain.getFiscal_position_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getPricelist_id_textDirtyFlag())
            this.setPricelist_id_text(srfdomain.getPricelist_id_text());
        if(srfdomain.getPayment_term_id_textDirtyFlag())
            this.setPayment_term_id_text(srfdomain.getPayment_term_id_text());
        if(srfdomain.getSale_order_template_id_textDirtyFlag())
            this.setSale_order_template_id_text(srfdomain.getSale_order_template_id_text());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getTeam_idDirtyFlag())
            this.setTeam_id(srfdomain.getTeam_id());
        if(srfdomain.getPartner_invoice_idDirtyFlag())
            this.setPartner_invoice_id(srfdomain.getPartner_invoice_id());
        if(srfdomain.getFiscal_position_idDirtyFlag())
            this.setFiscal_position_id(srfdomain.getFiscal_position_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getCampaign_idDirtyFlag())
            this.setCampaign_id(srfdomain.getCampaign_id());
        if(srfdomain.getSource_idDirtyFlag())
            this.setSource_id(srfdomain.getSource_id());
        if(srfdomain.getSale_order_template_idDirtyFlag())
            this.setSale_order_template_id(srfdomain.getSale_order_template_id());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getMedium_idDirtyFlag())
            this.setMedium_id(srfdomain.getMedium_id());
        if(srfdomain.getAnalytic_account_idDirtyFlag())
            this.setAnalytic_account_id(srfdomain.getAnalytic_account_id());
        if(srfdomain.getPayment_term_idDirtyFlag())
            this.setPayment_term_id(srfdomain.getPayment_term_id());
        if(srfdomain.getPartner_shipping_idDirtyFlag())
            this.setPartner_shipping_id(srfdomain.getPartner_shipping_id());
        if(srfdomain.getOpportunity_idDirtyFlag())
            this.setOpportunity_id(srfdomain.getOpportunity_id());
        if(srfdomain.getIncotermDirtyFlag())
            this.setIncoterm(srfdomain.getIncoterm());
        if(srfdomain.getPricelist_idDirtyFlag())
            this.setPricelist_id(srfdomain.getPricelist_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWarehouse_idDirtyFlag())
            this.setWarehouse_id(srfdomain.getWarehouse_id());

        sale_order_lines.clear();
        for(cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_line sale_order_line : srfdomain.getSale_order_lines()){
            Sale_order_lineDTO dto = new Sale_order_lineDTO() ;
            dto.fromDO(sale_order_line);
            sale_order_lines.add(dto);
        }

    }

    public List<Sale_orderDTO> fromDOPage(List<Sale_order> poPage)   {
        if(poPage == null)
            return null;
        List<Sale_orderDTO> dtos=new ArrayList<Sale_orderDTO>();
        for(Sale_order domain : poPage) {
            Sale_orderDTO dto = new Sale_orderDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

