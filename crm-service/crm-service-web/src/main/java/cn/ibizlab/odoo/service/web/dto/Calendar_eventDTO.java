package cn.ibizlab.odoo.service.web.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_calendar.valuerule.anno.calendar_event.*;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_event;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Calendar_eventDTO]
 */
public class Calendar_eventDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [LOCATION]
     *
     */
    @Calendar_eventLocationDefault(info = "默认规则")
    private String location;

    @JsonIgnore
    private boolean locationDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Calendar_eventMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @Calendar_eventDescriptionDefault(info = "默认规则")
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;

    /**
     * 属性 [RECURRENT_ID_DATE]
     *
     */
    @Calendar_eventRecurrent_id_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp recurrent_id_date;

    @JsonIgnore
    private boolean recurrent_id_dateDirtyFlag;

    /**
     * 属性 [SA]
     *
     */
    @Calendar_eventSaDefault(info = "默认规则")
    private String sa;

    @JsonIgnore
    private boolean saDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Calendar_eventDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [RECURRENCY]
     *
     */
    @Calendar_eventRecurrencyDefault(info = "默认规则")
    private String recurrency;

    @JsonIgnore
    private boolean recurrencyDirtyFlag;

    /**
     * 属性 [COUNT]
     *
     */
    @Calendar_eventCountDefault(info = "默认规则")
    private Integer count;

    @JsonIgnore
    private boolean countDirtyFlag;

    /**
     * 属性 [FR]
     *
     */
    @Calendar_eventFrDefault(info = "默认规则")
    private String fr;

    @JsonIgnore
    private boolean frDirtyFlag;

    /**
     * 属性 [RRULE]
     *
     */
    @Calendar_eventRruleDefault(info = "默认规则")
    private String rrule;

    @JsonIgnore
    private boolean rruleDirtyFlag;

    /**
     * 属性 [MONTH_BY]
     *
     */
    @Calendar_eventMonth_byDefault(info = "默认规则")
    private String month_by;

    @JsonIgnore
    private boolean month_byDirtyFlag;

    /**
     * 属性 [WEEK_LIST]
     *
     */
    @Calendar_eventWeek_listDefault(info = "默认规则")
    private String week_list;

    @JsonIgnore
    private boolean week_listDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Calendar_eventMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [STOP]
     *
     */
    @Calendar_eventStopDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp stop;

    @JsonIgnore
    private boolean stopDirtyFlag;

    /**
     * 属性 [RES_ID]
     *
     */
    @Calendar_eventRes_idDefault(info = "默认规则")
    private Integer res_id;

    @JsonIgnore
    private boolean res_idDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Calendar_eventMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [RECURRENT_ID]
     *
     */
    @Calendar_eventRecurrent_idDefault(info = "默认规则")
    private Integer recurrent_id;

    @JsonIgnore
    private boolean recurrent_idDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Calendar_eventMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [END_TYPE]
     *
     */
    @Calendar_eventEnd_typeDefault(info = "默认规则")
    private String end_type;

    @JsonIgnore
    private boolean end_typeDirtyFlag;

    /**
     * 属性 [ATTENDEE_IDS]
     *
     */
    @Calendar_eventAttendee_idsDefault(info = "默认规则")
    private String attendee_ids;

    @JsonIgnore
    private boolean attendee_idsDirtyFlag;

    /**
     * 属性 [ATTENDEE_STATUS]
     *
     */
    @Calendar_eventAttendee_statusDefault(info = "默认规则")
    private String attendee_status;

    @JsonIgnore
    private boolean attendee_statusDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Calendar_eventActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [RRULE_TYPE]
     *
     */
    @Calendar_eventRrule_typeDefault(info = "默认规则")
    private String rrule_type;

    @JsonIgnore
    private boolean rrule_typeDirtyFlag;

    /**
     * 属性 [INTERVAL]
     *
     */
    @Calendar_eventIntervalDefault(info = "默认规则")
    private Integer interval;

    @JsonIgnore
    private boolean intervalDirtyFlag;

    /**
     * 属性 [PRIVACY]
     *
     */
    @Calendar_eventPrivacyDefault(info = "默认规则")
    private String privacy;

    @JsonIgnore
    private boolean privacyDirtyFlag;

    /**
     * 属性 [DURATION]
     *
     */
    @Calendar_eventDurationDefault(info = "默认规则")
    private Double duration;

    @JsonIgnore
    private boolean durationDirtyFlag;

    /**
     * 属性 [START_DATETIME]
     *
     */
    @Calendar_eventStart_datetimeDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp start_datetime;

    @JsonIgnore
    private boolean start_datetimeDirtyFlag;

    /**
     * 属性 [IS_ATTENDEE]
     *
     */
    @Calendar_eventIs_attendeeDefault(info = "默认规则")
    private String is_attendee;

    @JsonIgnore
    private boolean is_attendeeDirtyFlag;

    /**
     * 属性 [START_DATE]
     *
     */
    @Calendar_eventStart_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp start_date;

    @JsonIgnore
    private boolean start_dateDirtyFlag;

    /**
     * 属性 [MO]
     *
     */
    @Calendar_eventMoDefault(info = "默认规则")
    private String mo;

    @JsonIgnore
    private boolean moDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Calendar_eventStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Calendar_event__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [WE]
     *
     */
    @Calendar_eventWeDefault(info = "默认规则")
    private String we;

    @JsonIgnore
    private boolean weDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Calendar_eventMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [DISPLAY_TIME]
     *
     */
    @Calendar_eventDisplay_timeDefault(info = "默认规则")
    private String display_time;

    @JsonIgnore
    private boolean display_timeDirtyFlag;

    /**
     * 属性 [DISPLAY_START]
     *
     */
    @Calendar_eventDisplay_startDefault(info = "默认规则")
    private String display_start;

    @JsonIgnore
    private boolean display_startDirtyFlag;

    /**
     * 属性 [STOP_DATE]
     *
     */
    @Calendar_eventStop_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp stop_date;

    @JsonIgnore
    private boolean stop_dateDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Calendar_eventMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Calendar_eventMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [FINAL_DATE]
     *
     */
    @Calendar_eventFinal_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp final_date;

    @JsonIgnore
    private boolean final_dateDirtyFlag;

    /**
     * 属性 [RES_MODEL_ID]
     *
     */
    @Calendar_eventRes_model_idDefault(info = "默认规则")
    private Integer res_model_id;

    @JsonIgnore
    private boolean res_model_idDirtyFlag;

    /**
     * 属性 [IS_HIGHLIGHTED]
     *
     */
    @Calendar_eventIs_highlightedDefault(info = "默认规则")
    private String is_highlighted;

    @JsonIgnore
    private boolean is_highlightedDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Calendar_eventIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Calendar_eventMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Calendar_eventWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [STOP_DATETIME]
     *
     */
    @Calendar_eventStop_datetimeDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp stop_datetime;

    @JsonIgnore
    private boolean stop_datetimeDirtyFlag;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @Calendar_eventActivity_idsDefault(info = "默认规则")
    private String activity_ids;

    @JsonIgnore
    private boolean activity_idsDirtyFlag;

    /**
     * 属性 [RES_MODEL]
     *
     */
    @Calendar_eventRes_modelDefault(info = "默认规则")
    private String res_model;

    @JsonIgnore
    private boolean res_modelDirtyFlag;

    /**
     * 属性 [TU]
     *
     */
    @Calendar_eventTuDefault(info = "默认规则")
    private String tu;

    @JsonIgnore
    private boolean tuDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Calendar_eventCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [SHOW_AS]
     *
     */
    @Calendar_eventShow_asDefault(info = "默认规则")
    private String show_as;

    @JsonIgnore
    private boolean show_asDirtyFlag;

    /**
     * 属性 [CATEG_IDS]
     *
     */
    @Calendar_eventCateg_idsDefault(info = "默认规则")
    private String categ_ids;

    @JsonIgnore
    private boolean categ_idsDirtyFlag;

    /**
     * 属性 [ALARM_IDS]
     *
     */
    @Calendar_eventAlarm_idsDefault(info = "默认规则")
    private String alarm_ids;

    @JsonIgnore
    private boolean alarm_idsDirtyFlag;

    /**
     * 属性 [TH]
     *
     */
    @Calendar_eventThDefault(info = "默认规则")
    private String th;

    @JsonIgnore
    private boolean thDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Calendar_eventMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [BYDAY]
     *
     */
    @Calendar_eventBydayDefault(info = "默认规则")
    private String byday;

    @JsonIgnore
    private boolean bydayDirtyFlag;

    /**
     * 属性 [DAY]
     *
     */
    @Calendar_eventDayDefault(info = "默认规则")
    private Integer day;

    @JsonIgnore
    private boolean dayDirtyFlag;

    /**
     * 属性 [START]
     *
     */
    @Calendar_eventStartDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp start;

    @JsonIgnore
    private boolean startDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Calendar_eventMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [PARTNER_IDS]
     *
     */
    @Calendar_eventPartner_idsDefault(info = "默认规则")
    private String partner_ids;

    @JsonIgnore
    private boolean partner_idsDirtyFlag;

    /**
     * 属性 [ALLDAY]
     *
     */
    @Calendar_eventAlldayDefault(info = "默认规则")
    private String allday;

    @JsonIgnore
    private boolean alldayDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Calendar_eventMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Calendar_eventWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Calendar_eventMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [SU]
     *
     */
    @Calendar_eventSuDefault(info = "默认规则")
    private String su;

    @JsonIgnore
    private boolean suDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Calendar_eventMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Calendar_eventNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [OPPORTUNITY_ID_TEXT]
     *
     */
    @Calendar_eventOpportunity_id_textDefault(info = "默认规则")
    private String opportunity_id_text;

    @JsonIgnore
    private boolean opportunity_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Calendar_eventCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Calendar_eventWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Calendar_eventUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Calendar_eventPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [APPLICANT_ID_TEXT]
     *
     */
    @Calendar_eventApplicant_id_textDefault(info = "默认规则")
    private String applicant_id_text;

    @JsonIgnore
    private boolean applicant_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Calendar_eventCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [APPLICANT_ID]
     *
     */
    @Calendar_eventApplicant_idDefault(info = "默认规则")
    private Integer applicant_id;

    @JsonIgnore
    private boolean applicant_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Calendar_eventWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Calendar_eventUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;

    /**
     * 属性 [OPPORTUNITY_ID]
     *
     */
    @Calendar_eventOpportunity_idDefault(info = "默认规则")
    private Integer opportunity_id;

    @JsonIgnore
    private boolean opportunity_idDirtyFlag;


    /**
     * 获取 [LOCATION]
     */
    @JsonProperty("location")
    public String getLocation(){
        return location ;
    }

    /**
     * 设置 [LOCATION]
     */
    @JsonProperty("location")
    public void setLocation(String  location){
        this.location = location ;
        this.locationDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION]脏标记
     */
    @JsonIgnore
    public boolean getLocationDirtyFlag(){
        return locationDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION]
     */
    @JsonProperty("description")
    public String getDescription(){
        return description ;
    }

    /**
     * 设置 [DESCRIPTION]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return descriptionDirtyFlag ;
    }

    /**
     * 获取 [RECURRENT_ID_DATE]
     */
    @JsonProperty("recurrent_id_date")
    public Timestamp getRecurrent_id_date(){
        return recurrent_id_date ;
    }

    /**
     * 设置 [RECURRENT_ID_DATE]
     */
    @JsonProperty("recurrent_id_date")
    public void setRecurrent_id_date(Timestamp  recurrent_id_date){
        this.recurrent_id_date = recurrent_id_date ;
        this.recurrent_id_dateDirtyFlag = true ;
    }

    /**
     * 获取 [RECURRENT_ID_DATE]脏标记
     */
    @JsonIgnore
    public boolean getRecurrent_id_dateDirtyFlag(){
        return recurrent_id_dateDirtyFlag ;
    }

    /**
     * 获取 [SA]
     */
    @JsonProperty("sa")
    public String getSa(){
        return sa ;
    }

    /**
     * 设置 [SA]
     */
    @JsonProperty("sa")
    public void setSa(String  sa){
        this.sa = sa ;
        this.saDirtyFlag = true ;
    }

    /**
     * 获取 [SA]脏标记
     */
    @JsonIgnore
    public boolean getSaDirtyFlag(){
        return saDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [RECURRENCY]
     */
    @JsonProperty("recurrency")
    public String getRecurrency(){
        return recurrency ;
    }

    /**
     * 设置 [RECURRENCY]
     */
    @JsonProperty("recurrency")
    public void setRecurrency(String  recurrency){
        this.recurrency = recurrency ;
        this.recurrencyDirtyFlag = true ;
    }

    /**
     * 获取 [RECURRENCY]脏标记
     */
    @JsonIgnore
    public boolean getRecurrencyDirtyFlag(){
        return recurrencyDirtyFlag ;
    }

    /**
     * 获取 [COUNT]
     */
    @JsonProperty("count")
    public Integer getCount(){
        return count ;
    }

    /**
     * 设置 [COUNT]
     */
    @JsonProperty("count")
    public void setCount(Integer  count){
        this.count = count ;
        this.countDirtyFlag = true ;
    }

    /**
     * 获取 [COUNT]脏标记
     */
    @JsonIgnore
    public boolean getCountDirtyFlag(){
        return countDirtyFlag ;
    }

    /**
     * 获取 [FR]
     */
    @JsonProperty("fr")
    public String getFr(){
        return fr ;
    }

    /**
     * 设置 [FR]
     */
    @JsonProperty("fr")
    public void setFr(String  fr){
        this.fr = fr ;
        this.frDirtyFlag = true ;
    }

    /**
     * 获取 [FR]脏标记
     */
    @JsonIgnore
    public boolean getFrDirtyFlag(){
        return frDirtyFlag ;
    }

    /**
     * 获取 [RRULE]
     */
    @JsonProperty("rrule")
    public String getRrule(){
        return rrule ;
    }

    /**
     * 设置 [RRULE]
     */
    @JsonProperty("rrule")
    public void setRrule(String  rrule){
        this.rrule = rrule ;
        this.rruleDirtyFlag = true ;
    }

    /**
     * 获取 [RRULE]脏标记
     */
    @JsonIgnore
    public boolean getRruleDirtyFlag(){
        return rruleDirtyFlag ;
    }

    /**
     * 获取 [MONTH_BY]
     */
    @JsonProperty("month_by")
    public String getMonth_by(){
        return month_by ;
    }

    /**
     * 设置 [MONTH_BY]
     */
    @JsonProperty("month_by")
    public void setMonth_by(String  month_by){
        this.month_by = month_by ;
        this.month_byDirtyFlag = true ;
    }

    /**
     * 获取 [MONTH_BY]脏标记
     */
    @JsonIgnore
    public boolean getMonth_byDirtyFlag(){
        return month_byDirtyFlag ;
    }

    /**
     * 获取 [WEEK_LIST]
     */
    @JsonProperty("week_list")
    public String getWeek_list(){
        return week_list ;
    }

    /**
     * 设置 [WEEK_LIST]
     */
    @JsonProperty("week_list")
    public void setWeek_list(String  week_list){
        this.week_list = week_list ;
        this.week_listDirtyFlag = true ;
    }

    /**
     * 获取 [WEEK_LIST]脏标记
     */
    @JsonIgnore
    public boolean getWeek_listDirtyFlag(){
        return week_listDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [STOP]
     */
    @JsonProperty("stop")
    public Timestamp getStop(){
        return stop ;
    }

    /**
     * 设置 [STOP]
     */
    @JsonProperty("stop")
    public void setStop(Timestamp  stop){
        this.stop = stop ;
        this.stopDirtyFlag = true ;
    }

    /**
     * 获取 [STOP]脏标记
     */
    @JsonIgnore
    public boolean getStopDirtyFlag(){
        return stopDirtyFlag ;
    }

    /**
     * 获取 [RES_ID]
     */
    @JsonProperty("res_id")
    public Integer getRes_id(){
        return res_id ;
    }

    /**
     * 设置 [RES_ID]
     */
    @JsonProperty("res_id")
    public void setRes_id(Integer  res_id){
        this.res_id = res_id ;
        this.res_idDirtyFlag = true ;
    }

    /**
     * 获取 [RES_ID]脏标记
     */
    @JsonIgnore
    public boolean getRes_idDirtyFlag(){
        return res_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [RECURRENT_ID]
     */
    @JsonProperty("recurrent_id")
    public Integer getRecurrent_id(){
        return recurrent_id ;
    }

    /**
     * 设置 [RECURRENT_ID]
     */
    @JsonProperty("recurrent_id")
    public void setRecurrent_id(Integer  recurrent_id){
        this.recurrent_id = recurrent_id ;
        this.recurrent_idDirtyFlag = true ;
    }

    /**
     * 获取 [RECURRENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getRecurrent_idDirtyFlag(){
        return recurrent_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [END_TYPE]
     */
    @JsonProperty("end_type")
    public String getEnd_type(){
        return end_type ;
    }

    /**
     * 设置 [END_TYPE]
     */
    @JsonProperty("end_type")
    public void setEnd_type(String  end_type){
        this.end_type = end_type ;
        this.end_typeDirtyFlag = true ;
    }

    /**
     * 获取 [END_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getEnd_typeDirtyFlag(){
        return end_typeDirtyFlag ;
    }

    /**
     * 获取 [ATTENDEE_IDS]
     */
    @JsonProperty("attendee_ids")
    public String getAttendee_ids(){
        return attendee_ids ;
    }

    /**
     * 设置 [ATTENDEE_IDS]
     */
    @JsonProperty("attendee_ids")
    public void setAttendee_ids(String  attendee_ids){
        this.attendee_ids = attendee_ids ;
        this.attendee_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ATTENDEE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAttendee_idsDirtyFlag(){
        return attendee_idsDirtyFlag ;
    }

    /**
     * 获取 [ATTENDEE_STATUS]
     */
    @JsonProperty("attendee_status")
    public String getAttendee_status(){
        return attendee_status ;
    }

    /**
     * 设置 [ATTENDEE_STATUS]
     */
    @JsonProperty("attendee_status")
    public void setAttendee_status(String  attendee_status){
        this.attendee_status = attendee_status ;
        this.attendee_statusDirtyFlag = true ;
    }

    /**
     * 获取 [ATTENDEE_STATUS]脏标记
     */
    @JsonIgnore
    public boolean getAttendee_statusDirtyFlag(){
        return attendee_statusDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [RRULE_TYPE]
     */
    @JsonProperty("rrule_type")
    public String getRrule_type(){
        return rrule_type ;
    }

    /**
     * 设置 [RRULE_TYPE]
     */
    @JsonProperty("rrule_type")
    public void setRrule_type(String  rrule_type){
        this.rrule_type = rrule_type ;
        this.rrule_typeDirtyFlag = true ;
    }

    /**
     * 获取 [RRULE_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getRrule_typeDirtyFlag(){
        return rrule_typeDirtyFlag ;
    }

    /**
     * 获取 [INTERVAL]
     */
    @JsonProperty("interval")
    public Integer getInterval(){
        return interval ;
    }

    /**
     * 设置 [INTERVAL]
     */
    @JsonProperty("interval")
    public void setInterval(Integer  interval){
        this.interval = interval ;
        this.intervalDirtyFlag = true ;
    }

    /**
     * 获取 [INTERVAL]脏标记
     */
    @JsonIgnore
    public boolean getIntervalDirtyFlag(){
        return intervalDirtyFlag ;
    }

    /**
     * 获取 [PRIVACY]
     */
    @JsonProperty("privacy")
    public String getPrivacy(){
        return privacy ;
    }

    /**
     * 设置 [PRIVACY]
     */
    @JsonProperty("privacy")
    public void setPrivacy(String  privacy){
        this.privacy = privacy ;
        this.privacyDirtyFlag = true ;
    }

    /**
     * 获取 [PRIVACY]脏标记
     */
    @JsonIgnore
    public boolean getPrivacyDirtyFlag(){
        return privacyDirtyFlag ;
    }

    /**
     * 获取 [DURATION]
     */
    @JsonProperty("duration")
    public Double getDuration(){
        return duration ;
    }

    /**
     * 设置 [DURATION]
     */
    @JsonProperty("duration")
    public void setDuration(Double  duration){
        this.duration = duration ;
        this.durationDirtyFlag = true ;
    }

    /**
     * 获取 [DURATION]脏标记
     */
    @JsonIgnore
    public boolean getDurationDirtyFlag(){
        return durationDirtyFlag ;
    }

    /**
     * 获取 [START_DATETIME]
     */
    @JsonProperty("start_datetime")
    public Timestamp getStart_datetime(){
        return start_datetime ;
    }

    /**
     * 设置 [START_DATETIME]
     */
    @JsonProperty("start_datetime")
    public void setStart_datetime(Timestamp  start_datetime){
        this.start_datetime = start_datetime ;
        this.start_datetimeDirtyFlag = true ;
    }

    /**
     * 获取 [START_DATETIME]脏标记
     */
    @JsonIgnore
    public boolean getStart_datetimeDirtyFlag(){
        return start_datetimeDirtyFlag ;
    }

    /**
     * 获取 [IS_ATTENDEE]
     */
    @JsonProperty("is_attendee")
    public String getIs_attendee(){
        return is_attendee ;
    }

    /**
     * 设置 [IS_ATTENDEE]
     */
    @JsonProperty("is_attendee")
    public void setIs_attendee(String  is_attendee){
        this.is_attendee = is_attendee ;
        this.is_attendeeDirtyFlag = true ;
    }

    /**
     * 获取 [IS_ATTENDEE]脏标记
     */
    @JsonIgnore
    public boolean getIs_attendeeDirtyFlag(){
        return is_attendeeDirtyFlag ;
    }

    /**
     * 获取 [START_DATE]
     */
    @JsonProperty("start_date")
    public Timestamp getStart_date(){
        return start_date ;
    }

    /**
     * 设置 [START_DATE]
     */
    @JsonProperty("start_date")
    public void setStart_date(Timestamp  start_date){
        this.start_date = start_date ;
        this.start_dateDirtyFlag = true ;
    }

    /**
     * 获取 [START_DATE]脏标记
     */
    @JsonIgnore
    public boolean getStart_dateDirtyFlag(){
        return start_dateDirtyFlag ;
    }

    /**
     * 获取 [MO]
     */
    @JsonProperty("mo")
    public String getMo(){
        return mo ;
    }

    /**
     * 设置 [MO]
     */
    @JsonProperty("mo")
    public void setMo(String  mo){
        this.mo = mo ;
        this.moDirtyFlag = true ;
    }

    /**
     * 获取 [MO]脏标记
     */
    @JsonIgnore
    public boolean getMoDirtyFlag(){
        return moDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [WE]
     */
    @JsonProperty("we")
    public String getWe(){
        return we ;
    }

    /**
     * 设置 [WE]
     */
    @JsonProperty("we")
    public void setWe(String  we){
        this.we = we ;
        this.weDirtyFlag = true ;
    }

    /**
     * 获取 [WE]脏标记
     */
    @JsonIgnore
    public boolean getWeDirtyFlag(){
        return weDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_TIME]
     */
    @JsonProperty("display_time")
    public String getDisplay_time(){
        return display_time ;
    }

    /**
     * 设置 [DISPLAY_TIME]
     */
    @JsonProperty("display_time")
    public void setDisplay_time(String  display_time){
        this.display_time = display_time ;
        this.display_timeDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_TIME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_timeDirtyFlag(){
        return display_timeDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_START]
     */
    @JsonProperty("display_start")
    public String getDisplay_start(){
        return display_start ;
    }

    /**
     * 设置 [DISPLAY_START]
     */
    @JsonProperty("display_start")
    public void setDisplay_start(String  display_start){
        this.display_start = display_start ;
        this.display_startDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_START]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_startDirtyFlag(){
        return display_startDirtyFlag ;
    }

    /**
     * 获取 [STOP_DATE]
     */
    @JsonProperty("stop_date")
    public Timestamp getStop_date(){
        return stop_date ;
    }

    /**
     * 设置 [STOP_DATE]
     */
    @JsonProperty("stop_date")
    public void setStop_date(Timestamp  stop_date){
        this.stop_date = stop_date ;
        this.stop_dateDirtyFlag = true ;
    }

    /**
     * 获取 [STOP_DATE]脏标记
     */
    @JsonIgnore
    public boolean getStop_dateDirtyFlag(){
        return stop_dateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [FINAL_DATE]
     */
    @JsonProperty("final_date")
    public Timestamp getFinal_date(){
        return final_date ;
    }

    /**
     * 设置 [FINAL_DATE]
     */
    @JsonProperty("final_date")
    public void setFinal_date(Timestamp  final_date){
        this.final_date = final_date ;
        this.final_dateDirtyFlag = true ;
    }

    /**
     * 获取 [FINAL_DATE]脏标记
     */
    @JsonIgnore
    public boolean getFinal_dateDirtyFlag(){
        return final_dateDirtyFlag ;
    }

    /**
     * 获取 [RES_MODEL_ID]
     */
    @JsonProperty("res_model_id")
    public Integer getRes_model_id(){
        return res_model_id ;
    }

    /**
     * 设置 [RES_MODEL_ID]
     */
    @JsonProperty("res_model_id")
    public void setRes_model_id(Integer  res_model_id){
        this.res_model_id = res_model_id ;
        this.res_model_idDirtyFlag = true ;
    }

    /**
     * 获取 [RES_MODEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getRes_model_idDirtyFlag(){
        return res_model_idDirtyFlag ;
    }

    /**
     * 获取 [IS_HIGHLIGHTED]
     */
    @JsonProperty("is_highlighted")
    public String getIs_highlighted(){
        return is_highlighted ;
    }

    /**
     * 设置 [IS_HIGHLIGHTED]
     */
    @JsonProperty("is_highlighted")
    public void setIs_highlighted(String  is_highlighted){
        this.is_highlighted = is_highlighted ;
        this.is_highlightedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_HIGHLIGHTED]脏标记
     */
    @JsonIgnore
    public boolean getIs_highlightedDirtyFlag(){
        return is_highlightedDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [STOP_DATETIME]
     */
    @JsonProperty("stop_datetime")
    public Timestamp getStop_datetime(){
        return stop_datetime ;
    }

    /**
     * 设置 [STOP_DATETIME]
     */
    @JsonProperty("stop_datetime")
    public void setStop_datetime(Timestamp  stop_datetime){
        this.stop_datetime = stop_datetime ;
        this.stop_datetimeDirtyFlag = true ;
    }

    /**
     * 获取 [STOP_DATETIME]脏标记
     */
    @JsonIgnore
    public boolean getStop_datetimeDirtyFlag(){
        return stop_datetimeDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return activity_ids ;
    }

    /**
     * 设置 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return activity_idsDirtyFlag ;
    }

    /**
     * 获取 [RES_MODEL]
     */
    @JsonProperty("res_model")
    public String getRes_model(){
        return res_model ;
    }

    /**
     * 设置 [RES_MODEL]
     */
    @JsonProperty("res_model")
    public void setRes_model(String  res_model){
        this.res_model = res_model ;
        this.res_modelDirtyFlag = true ;
    }

    /**
     * 获取 [RES_MODEL]脏标记
     */
    @JsonIgnore
    public boolean getRes_modelDirtyFlag(){
        return res_modelDirtyFlag ;
    }

    /**
     * 获取 [TU]
     */
    @JsonProperty("tu")
    public String getTu(){
        return tu ;
    }

    /**
     * 设置 [TU]
     */
    @JsonProperty("tu")
    public void setTu(String  tu){
        this.tu = tu ;
        this.tuDirtyFlag = true ;
    }

    /**
     * 获取 [TU]脏标记
     */
    @JsonIgnore
    public boolean getTuDirtyFlag(){
        return tuDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [SHOW_AS]
     */
    @JsonProperty("show_as")
    public String getShow_as(){
        return show_as ;
    }

    /**
     * 设置 [SHOW_AS]
     */
    @JsonProperty("show_as")
    public void setShow_as(String  show_as){
        this.show_as = show_as ;
        this.show_asDirtyFlag = true ;
    }

    /**
     * 获取 [SHOW_AS]脏标记
     */
    @JsonIgnore
    public boolean getShow_asDirtyFlag(){
        return show_asDirtyFlag ;
    }

    /**
     * 获取 [CATEG_IDS]
     */
    @JsonProperty("categ_ids")
    public String getCateg_ids(){
        return categ_ids ;
    }

    /**
     * 设置 [CATEG_IDS]
     */
    @JsonProperty("categ_ids")
    public void setCateg_ids(String  categ_ids){
        this.categ_ids = categ_ids ;
        this.categ_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CATEG_IDS]脏标记
     */
    @JsonIgnore
    public boolean getCateg_idsDirtyFlag(){
        return categ_idsDirtyFlag ;
    }

    /**
     * 获取 [ALARM_IDS]
     */
    @JsonProperty("alarm_ids")
    public String getAlarm_ids(){
        return alarm_ids ;
    }

    /**
     * 设置 [ALARM_IDS]
     */
    @JsonProperty("alarm_ids")
    public void setAlarm_ids(String  alarm_ids){
        this.alarm_ids = alarm_ids ;
        this.alarm_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ALARM_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAlarm_idsDirtyFlag(){
        return alarm_idsDirtyFlag ;
    }

    /**
     * 获取 [TH]
     */
    @JsonProperty("th")
    public String getTh(){
        return th ;
    }

    /**
     * 设置 [TH]
     */
    @JsonProperty("th")
    public void setTh(String  th){
        this.th = th ;
        this.thDirtyFlag = true ;
    }

    /**
     * 获取 [TH]脏标记
     */
    @JsonIgnore
    public boolean getThDirtyFlag(){
        return thDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [BYDAY]
     */
    @JsonProperty("byday")
    public String getByday(){
        return byday ;
    }

    /**
     * 设置 [BYDAY]
     */
    @JsonProperty("byday")
    public void setByday(String  byday){
        this.byday = byday ;
        this.bydayDirtyFlag = true ;
    }

    /**
     * 获取 [BYDAY]脏标记
     */
    @JsonIgnore
    public boolean getBydayDirtyFlag(){
        return bydayDirtyFlag ;
    }

    /**
     * 获取 [DAY]
     */
    @JsonProperty("day")
    public Integer getDay(){
        return day ;
    }

    /**
     * 设置 [DAY]
     */
    @JsonProperty("day")
    public void setDay(Integer  day){
        this.day = day ;
        this.dayDirtyFlag = true ;
    }

    /**
     * 获取 [DAY]脏标记
     */
    @JsonIgnore
    public boolean getDayDirtyFlag(){
        return dayDirtyFlag ;
    }

    /**
     * 获取 [START]
     */
    @JsonProperty("start")
    public Timestamp getStart(){
        return start ;
    }

    /**
     * 设置 [START]
     */
    @JsonProperty("start")
    public void setStart(Timestamp  start){
        this.start = start ;
        this.startDirtyFlag = true ;
    }

    /**
     * 获取 [START]脏标记
     */
    @JsonIgnore
    public boolean getStartDirtyFlag(){
        return startDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_IDS]
     */
    @JsonProperty("partner_ids")
    public String getPartner_ids(){
        return partner_ids ;
    }

    /**
     * 设置 [PARTNER_IDS]
     */
    @JsonProperty("partner_ids")
    public void setPartner_ids(String  partner_ids){
        this.partner_ids = partner_ids ;
        this.partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idsDirtyFlag(){
        return partner_idsDirtyFlag ;
    }

    /**
     * 获取 [ALLDAY]
     */
    @JsonProperty("allday")
    public String getAllday(){
        return allday ;
    }

    /**
     * 设置 [ALLDAY]
     */
    @JsonProperty("allday")
    public void setAllday(String  allday){
        this.allday = allday ;
        this.alldayDirtyFlag = true ;
    }

    /**
     * 获取 [ALLDAY]脏标记
     */
    @JsonIgnore
    public boolean getAlldayDirtyFlag(){
        return alldayDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [SU]
     */
    @JsonProperty("su")
    public String getSu(){
        return su ;
    }

    /**
     * 设置 [SU]
     */
    @JsonProperty("su")
    public void setSu(String  su){
        this.su = su ;
        this.suDirtyFlag = true ;
    }

    /**
     * 获取 [SU]脏标记
     */
    @JsonIgnore
    public boolean getSuDirtyFlag(){
        return suDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [OPPORTUNITY_ID_TEXT]
     */
    @JsonProperty("opportunity_id_text")
    public String getOpportunity_id_text(){
        return opportunity_id_text ;
    }

    /**
     * 设置 [OPPORTUNITY_ID_TEXT]
     */
    @JsonProperty("opportunity_id_text")
    public void setOpportunity_id_text(String  opportunity_id_text){
        this.opportunity_id_text = opportunity_id_text ;
        this.opportunity_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [OPPORTUNITY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getOpportunity_id_textDirtyFlag(){
        return opportunity_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [APPLICANT_ID_TEXT]
     */
    @JsonProperty("applicant_id_text")
    public String getApplicant_id_text(){
        return applicant_id_text ;
    }

    /**
     * 设置 [APPLICANT_ID_TEXT]
     */
    @JsonProperty("applicant_id_text")
    public void setApplicant_id_text(String  applicant_id_text){
        this.applicant_id_text = applicant_id_text ;
        this.applicant_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [APPLICANT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getApplicant_id_textDirtyFlag(){
        return applicant_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [APPLICANT_ID]
     */
    @JsonProperty("applicant_id")
    public Integer getApplicant_id(){
        return applicant_id ;
    }

    /**
     * 设置 [APPLICANT_ID]
     */
    @JsonProperty("applicant_id")
    public void setApplicant_id(Integer  applicant_id){
        this.applicant_id = applicant_id ;
        this.applicant_idDirtyFlag = true ;
    }

    /**
     * 获取 [APPLICANT_ID]脏标记
     */
    @JsonIgnore
    public boolean getApplicant_idDirtyFlag(){
        return applicant_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }

    /**
     * 获取 [OPPORTUNITY_ID]
     */
    @JsonProperty("opportunity_id")
    public Integer getOpportunity_id(){
        return opportunity_id ;
    }

    /**
     * 设置 [OPPORTUNITY_ID]
     */
    @JsonProperty("opportunity_id")
    public void setOpportunity_id(Integer  opportunity_id){
        this.opportunity_id = opportunity_id ;
        this.opportunity_idDirtyFlag = true ;
    }

    /**
     * 获取 [OPPORTUNITY_ID]脏标记
     */
    @JsonIgnore
    public boolean getOpportunity_idDirtyFlag(){
        return opportunity_idDirtyFlag ;
    }



    public Calendar_event toDO() {
        Calendar_event srfdomain = new Calendar_event();
        if(getLocationDirtyFlag())
            srfdomain.setLocation(location);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getDescriptionDirtyFlag())
            srfdomain.setDescription(description);
        if(getRecurrent_id_dateDirtyFlag())
            srfdomain.setRecurrent_id_date(recurrent_id_date);
        if(getSaDirtyFlag())
            srfdomain.setSa(sa);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getRecurrencyDirtyFlag())
            srfdomain.setRecurrency(recurrency);
        if(getCountDirtyFlag())
            srfdomain.setCount(count);
        if(getFrDirtyFlag())
            srfdomain.setFr(fr);
        if(getRruleDirtyFlag())
            srfdomain.setRrule(rrule);
        if(getMonth_byDirtyFlag())
            srfdomain.setMonth_by(month_by);
        if(getWeek_listDirtyFlag())
            srfdomain.setWeek_list(week_list);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getStopDirtyFlag())
            srfdomain.setStop(stop);
        if(getRes_idDirtyFlag())
            srfdomain.setRes_id(res_id);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getRecurrent_idDirtyFlag())
            srfdomain.setRecurrent_id(recurrent_id);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getEnd_typeDirtyFlag())
            srfdomain.setEnd_type(end_type);
        if(getAttendee_idsDirtyFlag())
            srfdomain.setAttendee_ids(attendee_ids);
        if(getAttendee_statusDirtyFlag())
            srfdomain.setAttendee_status(attendee_status);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getRrule_typeDirtyFlag())
            srfdomain.setRrule_type(rrule_type);
        if(getIntervalDirtyFlag())
            srfdomain.setInterval(interval);
        if(getPrivacyDirtyFlag())
            srfdomain.setPrivacy(privacy);
        if(getDurationDirtyFlag())
            srfdomain.setDuration(duration);
        if(getStart_datetimeDirtyFlag())
            srfdomain.setStart_datetime(start_datetime);
        if(getIs_attendeeDirtyFlag())
            srfdomain.setIs_attendee(is_attendee);
        if(getStart_dateDirtyFlag())
            srfdomain.setStart_date(start_date);
        if(getMoDirtyFlag())
            srfdomain.setMo(mo);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getWeDirtyFlag())
            srfdomain.setWe(we);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getDisplay_timeDirtyFlag())
            srfdomain.setDisplay_time(display_time);
        if(getDisplay_startDirtyFlag())
            srfdomain.setDisplay_start(display_start);
        if(getStop_dateDirtyFlag())
            srfdomain.setStop_date(stop_date);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getFinal_dateDirtyFlag())
            srfdomain.setFinal_date(final_date);
        if(getRes_model_idDirtyFlag())
            srfdomain.setRes_model_id(res_model_id);
        if(getIs_highlightedDirtyFlag())
            srfdomain.setIs_highlighted(is_highlighted);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getStop_datetimeDirtyFlag())
            srfdomain.setStop_datetime(stop_datetime);
        if(getActivity_idsDirtyFlag())
            srfdomain.setActivity_ids(activity_ids);
        if(getRes_modelDirtyFlag())
            srfdomain.setRes_model(res_model);
        if(getTuDirtyFlag())
            srfdomain.setTu(tu);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getShow_asDirtyFlag())
            srfdomain.setShow_as(show_as);
        if(getCateg_idsDirtyFlag())
            srfdomain.setCateg_ids(categ_ids);
        if(getAlarm_idsDirtyFlag())
            srfdomain.setAlarm_ids(alarm_ids);
        if(getThDirtyFlag())
            srfdomain.setTh(th);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getBydayDirtyFlag())
            srfdomain.setByday(byday);
        if(getDayDirtyFlag())
            srfdomain.setDay(day);
        if(getStartDirtyFlag())
            srfdomain.setStart(start);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getPartner_idsDirtyFlag())
            srfdomain.setPartner_ids(partner_ids);
        if(getAlldayDirtyFlag())
            srfdomain.setAllday(allday);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getSuDirtyFlag())
            srfdomain.setSu(su);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getOpportunity_id_textDirtyFlag())
            srfdomain.setOpportunity_id_text(opportunity_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getApplicant_id_textDirtyFlag())
            srfdomain.setApplicant_id_text(applicant_id_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getApplicant_idDirtyFlag())
            srfdomain.setApplicant_id(applicant_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);
        if(getOpportunity_idDirtyFlag())
            srfdomain.setOpportunity_id(opportunity_id);

        return srfdomain;
    }

    public void fromDO(Calendar_event srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getLocationDirtyFlag())
            this.setLocation(srfdomain.getLocation());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getDescriptionDirtyFlag())
            this.setDescription(srfdomain.getDescription());
        if(srfdomain.getRecurrent_id_dateDirtyFlag())
            this.setRecurrent_id_date(srfdomain.getRecurrent_id_date());
        if(srfdomain.getSaDirtyFlag())
            this.setSa(srfdomain.getSa());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getRecurrencyDirtyFlag())
            this.setRecurrency(srfdomain.getRecurrency());
        if(srfdomain.getCountDirtyFlag())
            this.setCount(srfdomain.getCount());
        if(srfdomain.getFrDirtyFlag())
            this.setFr(srfdomain.getFr());
        if(srfdomain.getRruleDirtyFlag())
            this.setRrule(srfdomain.getRrule());
        if(srfdomain.getMonth_byDirtyFlag())
            this.setMonth_by(srfdomain.getMonth_by());
        if(srfdomain.getWeek_listDirtyFlag())
            this.setWeek_list(srfdomain.getWeek_list());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getStopDirtyFlag())
            this.setStop(srfdomain.getStop());
        if(srfdomain.getRes_idDirtyFlag())
            this.setRes_id(srfdomain.getRes_id());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getRecurrent_idDirtyFlag())
            this.setRecurrent_id(srfdomain.getRecurrent_id());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getEnd_typeDirtyFlag())
            this.setEnd_type(srfdomain.getEnd_type());
        if(srfdomain.getAttendee_idsDirtyFlag())
            this.setAttendee_ids(srfdomain.getAttendee_ids());
        if(srfdomain.getAttendee_statusDirtyFlag())
            this.setAttendee_status(srfdomain.getAttendee_status());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getRrule_typeDirtyFlag())
            this.setRrule_type(srfdomain.getRrule_type());
        if(srfdomain.getIntervalDirtyFlag())
            this.setInterval(srfdomain.getInterval());
        if(srfdomain.getPrivacyDirtyFlag())
            this.setPrivacy(srfdomain.getPrivacy());
        if(srfdomain.getDurationDirtyFlag())
            this.setDuration(srfdomain.getDuration());
        if(srfdomain.getStart_datetimeDirtyFlag())
            this.setStart_datetime(srfdomain.getStart_datetime());
        if(srfdomain.getIs_attendeeDirtyFlag())
            this.setIs_attendee(srfdomain.getIs_attendee());
        if(srfdomain.getStart_dateDirtyFlag())
            this.setStart_date(srfdomain.getStart_date());
        if(srfdomain.getMoDirtyFlag())
            this.setMo(srfdomain.getMo());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getWeDirtyFlag())
            this.setWe(srfdomain.getWe());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getDisplay_timeDirtyFlag())
            this.setDisplay_time(srfdomain.getDisplay_time());
        if(srfdomain.getDisplay_startDirtyFlag())
            this.setDisplay_start(srfdomain.getDisplay_start());
        if(srfdomain.getStop_dateDirtyFlag())
            this.setStop_date(srfdomain.getStop_date());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getFinal_dateDirtyFlag())
            this.setFinal_date(srfdomain.getFinal_date());
        if(srfdomain.getRes_model_idDirtyFlag())
            this.setRes_model_id(srfdomain.getRes_model_id());
        if(srfdomain.getIs_highlightedDirtyFlag())
            this.setIs_highlighted(srfdomain.getIs_highlighted());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getStop_datetimeDirtyFlag())
            this.setStop_datetime(srfdomain.getStop_datetime());
        if(srfdomain.getActivity_idsDirtyFlag())
            this.setActivity_ids(srfdomain.getActivity_ids());
        if(srfdomain.getRes_modelDirtyFlag())
            this.setRes_model(srfdomain.getRes_model());
        if(srfdomain.getTuDirtyFlag())
            this.setTu(srfdomain.getTu());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getShow_asDirtyFlag())
            this.setShow_as(srfdomain.getShow_as());
        if(srfdomain.getCateg_idsDirtyFlag())
            this.setCateg_ids(srfdomain.getCateg_ids());
        if(srfdomain.getAlarm_idsDirtyFlag())
            this.setAlarm_ids(srfdomain.getAlarm_ids());
        if(srfdomain.getThDirtyFlag())
            this.setTh(srfdomain.getTh());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getBydayDirtyFlag())
            this.setByday(srfdomain.getByday());
        if(srfdomain.getDayDirtyFlag())
            this.setDay(srfdomain.getDay());
        if(srfdomain.getStartDirtyFlag())
            this.setStart(srfdomain.getStart());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getPartner_idsDirtyFlag())
            this.setPartner_ids(srfdomain.getPartner_ids());
        if(srfdomain.getAlldayDirtyFlag())
            this.setAllday(srfdomain.getAllday());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getSuDirtyFlag())
            this.setSu(srfdomain.getSu());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getOpportunity_id_textDirtyFlag())
            this.setOpportunity_id_text(srfdomain.getOpportunity_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getApplicant_id_textDirtyFlag())
            this.setApplicant_id_text(srfdomain.getApplicant_id_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getApplicant_idDirtyFlag())
            this.setApplicant_id(srfdomain.getApplicant_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());
        if(srfdomain.getOpportunity_idDirtyFlag())
            this.setOpportunity_id(srfdomain.getOpportunity_id());

    }

    public List<Calendar_eventDTO> fromDOPage(List<Calendar_event> poPage)   {
        if(poPage == null)
            return null;
        List<Calendar_eventDTO> dtos=new ArrayList<Calendar_eventDTO>();
        for(Calendar_event domain : poPage) {
            Calendar_eventDTO dto = new Calendar_eventDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

