package cn.ibizlab.odoo.client.r7rt_dyna.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.IDynaChart;
import cn.ibizlab.odoo.client.r7rt_dyna.config.r7rt_dynaClientProperties;
import cn.ibizlab.odoo.core.client.service.IDynaChartClientService;
import cn.ibizlab.odoo.client.r7rt_dyna.model.DynaChartImpl;
import cn.ibizlab.odoo.client.r7rt_dyna.feign.DynaChartFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[DynaChart] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class DynaChartClientServiceImpl implements IDynaChartClientService {

    DynaChartFeignClient dynaChartFeignClient;

    @Autowired
    public DynaChartClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,r7rt_dynaClientProperties r7rt_dynaClientProperties) {
        if (r7rt_dynaClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.dynaChartFeignClient = nameBuilder.target(DynaChartFeignClient.class,"http://"+r7rt_dynaClientProperties.getServiceId()+"/") ;
		}else if (r7rt_dynaClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.dynaChartFeignClient = nameBuilder.target(DynaChartFeignClient.class,r7rt_dynaClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public IDynaChart createModel() {
		return new DynaChartImpl();
	}


    public Page<IDynaChart> select(SearchContext context){
        return null ;
    }


    public void get(IDynaChart dynachart){
        IDynaChart clientModel = dynaChartFeignClient.get(dynachart.getDynaChartId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), dynachart.getClass(), false);
        copier.copy(clientModel, dynachart, null);
    }


    public void getDraft(IDynaChart dynachart){
        IDynaChart clientModel = dynaChartFeignClient.getDraft(dynachart.getDynaChartId(),(DynaChartImpl)dynachart) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), dynachart.getClass(), false);
        copier.copy(clientModel, dynachart, null);
    }


    public void checkKey(IDynaChart dynachart){
        IDynaChart clientModel = dynaChartFeignClient.checkKey(dynachart.getDynaChartId(),(DynaChartImpl)dynachart) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), dynachart.getClass(), false);
        copier.copy(clientModel, dynachart, null);
    }


    public void update(IDynaChart dynachart){
        IDynaChart clientModel = dynaChartFeignClient.update(dynachart.getDynaChartId(),(DynaChartImpl)dynachart) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), dynachart.getClass(), false);
        copier.copy(clientModel, dynachart, null);
    }


    public void remove(IDynaChart dynachart){
        dynaChartFeignClient.remove(dynachart.getDynaChartId()) ;
    }


    public void create(IDynaChart dynachart){
        IDynaChart clientModel = dynaChartFeignClient.create((DynaChartImpl)dynachart) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), dynachart.getClass(), false);
        copier.copy(clientModel, dynachart, null);
    }


    public void save(IDynaChart dynachart){
        IDynaChart clientModel = dynaChartFeignClient.save(dynachart.getDynaChartId(),(DynaChartImpl)dynachart) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), dynachart.getClass(), false);
        copier.copy(clientModel, dynachart, null);
    }


    public Page<IDynaChart> fetchDefault(SearchContext context){
        Page<DynaChartImpl> page = this.dynaChartFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }



}

