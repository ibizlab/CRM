package cn.ibizlab.odoo.client.r7rt_dyna.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ConditionalOnClass(r7rt_dynaClientConfiguration.class)
@ConditionalOnWebApplication
@EnableConfigurationProperties(r7rt_dynaClientProperties.class)
@Import({
    FeignClientsConfiguration.class
})
public class r7rt_dynaAutoConfiguration {

}
