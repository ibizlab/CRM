package cn.ibizlab.odoo.client.odoo_portal.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iportal_mixin;
import cn.ibizlab.odoo.client.odoo_portal.model.portal_mixinImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[portal_mixin] 服务对象接口
 */
public interface portal_mixinFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_portal/portal_mixins")
    public portal_mixinImpl create(@RequestBody portal_mixinImpl portal_mixin);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_portal/portal_mixins/fetchdefault")
    public Page<portal_mixinImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_portal/portal_mixins/createbatch")
    public portal_mixinImpl createBatch(@RequestBody List<portal_mixinImpl> portal_mixins);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_portal/portal_mixins/{id}")
    public portal_mixinImpl update(@PathVariable("id") Integer id,@RequestBody portal_mixinImpl portal_mixin);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_portal/portal_mixins/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_portal/portal_mixins/updatebatch")
    public portal_mixinImpl updateBatch(@RequestBody List<portal_mixinImpl> portal_mixins);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_portal/portal_mixins/removebatch")
    public portal_mixinImpl removeBatch(@RequestBody List<portal_mixinImpl> portal_mixins);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_portal/portal_mixins/{id}")
    public portal_mixinImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_portal/portal_mixins/select")
    public Page<portal_mixinImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_portal/portal_mixins/{id}/getdraft")
    public portal_mixinImpl getDraft(@PathVariable("id") Integer id,@RequestBody portal_mixinImpl portal_mixin);



}
