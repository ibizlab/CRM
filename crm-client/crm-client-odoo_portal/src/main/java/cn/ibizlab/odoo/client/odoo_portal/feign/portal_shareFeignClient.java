package cn.ibizlab.odoo.client.odoo_portal.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iportal_share;
import cn.ibizlab.odoo.client.odoo_portal.model.portal_shareImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[portal_share] 服务对象接口
 */
public interface portal_shareFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_portal/portal_shares")
    public portal_shareImpl create(@RequestBody portal_shareImpl portal_share);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_portal/portal_shares/{id}")
    public portal_shareImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_portal/portal_shares/removebatch")
    public portal_shareImpl removeBatch(@RequestBody List<portal_shareImpl> portal_shares);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_portal/portal_shares/updatebatch")
    public portal_shareImpl updateBatch(@RequestBody List<portal_shareImpl> portal_shares);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_portal/portal_shares/createbatch")
    public portal_shareImpl createBatch(@RequestBody List<portal_shareImpl> portal_shares);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_portal/portal_shares/{id}")
    public portal_shareImpl update(@PathVariable("id") Integer id,@RequestBody portal_shareImpl portal_share);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_portal/portal_shares/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_portal/portal_shares/fetchdefault")
    public Page<portal_shareImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_portal/portal_shares/select")
    public Page<portal_shareImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_portal/portal_shares/{id}/getdraft")
    public portal_shareImpl getDraft(@PathVariable("id") Integer id,@RequestBody portal_shareImpl portal_share);



}
