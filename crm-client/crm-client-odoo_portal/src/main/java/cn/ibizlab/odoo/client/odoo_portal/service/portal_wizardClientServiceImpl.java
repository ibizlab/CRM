package cn.ibizlab.odoo.client.odoo_portal.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iportal_wizard;
import cn.ibizlab.odoo.client.odoo_portal.config.odoo_portalClientProperties;
import cn.ibizlab.odoo.core.client.service.Iportal_wizardClientService;
import cn.ibizlab.odoo.client.odoo_portal.model.portal_wizardImpl;
import cn.ibizlab.odoo.client.odoo_portal.feign.portal_wizardFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[portal_wizard] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class portal_wizardClientServiceImpl implements Iportal_wizardClientService {

    portal_wizardFeignClient portal_wizardFeignClient;

    @Autowired
    public portal_wizardClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_portalClientProperties odoo_portalClientProperties) {
        if (odoo_portalClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.portal_wizardFeignClient = nameBuilder.target(portal_wizardFeignClient.class,"http://"+odoo_portalClientProperties.getServiceId()+"/") ;
		}else if (odoo_portalClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.portal_wizardFeignClient = nameBuilder.target(portal_wizardFeignClient.class,odoo_portalClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iportal_wizard createModel() {
		return new portal_wizardImpl();
	}


    public void updateBatch(List<Iportal_wizard> portal_wizards){
        if(portal_wizards!=null){
            List<portal_wizardImpl> list = new ArrayList<portal_wizardImpl>();
            for(Iportal_wizard iportal_wizard :portal_wizards){
                list.add((portal_wizardImpl)iportal_wizard) ;
            }
            portal_wizardFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Iportal_wizard> portal_wizards){
        if(portal_wizards!=null){
            List<portal_wizardImpl> list = new ArrayList<portal_wizardImpl>();
            for(Iportal_wizard iportal_wizard :portal_wizards){
                list.add((portal_wizardImpl)iportal_wizard) ;
            }
            portal_wizardFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Iportal_wizard> portal_wizards){
        if(portal_wizards!=null){
            List<portal_wizardImpl> list = new ArrayList<portal_wizardImpl>();
            for(Iportal_wizard iportal_wizard :portal_wizards){
                list.add((portal_wizardImpl)iportal_wizard) ;
            }
            portal_wizardFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iportal_wizard> fetchDefault(SearchContext context){
        Page<portal_wizardImpl> page = this.portal_wizardFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Iportal_wizard portal_wizard){
        Iportal_wizard clientModel = portal_wizardFeignClient.create((portal_wizardImpl)portal_wizard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), portal_wizard.getClass(), false);
        copier.copy(clientModel, portal_wizard, null);
    }


    public void remove(Iportal_wizard portal_wizard){
        portal_wizardFeignClient.remove(portal_wizard.getId()) ;
    }


    public void get(Iportal_wizard portal_wizard){
        Iportal_wizard clientModel = portal_wizardFeignClient.get(portal_wizard.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), portal_wizard.getClass(), false);
        copier.copy(clientModel, portal_wizard, null);
    }


    public void update(Iportal_wizard portal_wizard){
        Iportal_wizard clientModel = portal_wizardFeignClient.update(portal_wizard.getId(),(portal_wizardImpl)portal_wizard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), portal_wizard.getClass(), false);
        copier.copy(clientModel, portal_wizard, null);
    }


    public Page<Iportal_wizard> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iportal_wizard portal_wizard){
        Iportal_wizard clientModel = portal_wizardFeignClient.getDraft(portal_wizard.getId(),(portal_wizardImpl)portal_wizard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), portal_wizard.getClass(), false);
        copier.copy(clientModel, portal_wizard, null);
    }



}

