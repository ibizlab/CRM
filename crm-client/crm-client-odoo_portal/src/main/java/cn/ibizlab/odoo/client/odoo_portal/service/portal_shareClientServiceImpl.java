package cn.ibizlab.odoo.client.odoo_portal.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iportal_share;
import cn.ibizlab.odoo.client.odoo_portal.config.odoo_portalClientProperties;
import cn.ibizlab.odoo.core.client.service.Iportal_shareClientService;
import cn.ibizlab.odoo.client.odoo_portal.model.portal_shareImpl;
import cn.ibizlab.odoo.client.odoo_portal.feign.portal_shareFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[portal_share] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class portal_shareClientServiceImpl implements Iportal_shareClientService {

    portal_shareFeignClient portal_shareFeignClient;

    @Autowired
    public portal_shareClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_portalClientProperties odoo_portalClientProperties) {
        if (odoo_portalClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.portal_shareFeignClient = nameBuilder.target(portal_shareFeignClient.class,"http://"+odoo_portalClientProperties.getServiceId()+"/") ;
		}else if (odoo_portalClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.portal_shareFeignClient = nameBuilder.target(portal_shareFeignClient.class,odoo_portalClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iportal_share createModel() {
		return new portal_shareImpl();
	}


    public void create(Iportal_share portal_share){
        Iportal_share clientModel = portal_shareFeignClient.create((portal_shareImpl)portal_share) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), portal_share.getClass(), false);
        copier.copy(clientModel, portal_share, null);
    }


    public void get(Iportal_share portal_share){
        Iportal_share clientModel = portal_shareFeignClient.get(portal_share.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), portal_share.getClass(), false);
        copier.copy(clientModel, portal_share, null);
    }


    public void removeBatch(List<Iportal_share> portal_shares){
        if(portal_shares!=null){
            List<portal_shareImpl> list = new ArrayList<portal_shareImpl>();
            for(Iportal_share iportal_share :portal_shares){
                list.add((portal_shareImpl)iportal_share) ;
            }
            portal_shareFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Iportal_share> portal_shares){
        if(portal_shares!=null){
            List<portal_shareImpl> list = new ArrayList<portal_shareImpl>();
            for(Iportal_share iportal_share :portal_shares){
                list.add((portal_shareImpl)iportal_share) ;
            }
            portal_shareFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Iportal_share> portal_shares){
        if(portal_shares!=null){
            List<portal_shareImpl> list = new ArrayList<portal_shareImpl>();
            for(Iportal_share iportal_share :portal_shares){
                list.add((portal_shareImpl)iportal_share) ;
            }
            portal_shareFeignClient.createBatch(list) ;
        }
    }


    public void update(Iportal_share portal_share){
        Iportal_share clientModel = portal_shareFeignClient.update(portal_share.getId(),(portal_shareImpl)portal_share) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), portal_share.getClass(), false);
        copier.copy(clientModel, portal_share, null);
    }


    public void remove(Iportal_share portal_share){
        portal_shareFeignClient.remove(portal_share.getId()) ;
    }


    public Page<Iportal_share> fetchDefault(SearchContext context){
        Page<portal_shareImpl> page = this.portal_shareFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Iportal_share> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iportal_share portal_share){
        Iportal_share clientModel = portal_shareFeignClient.getDraft(portal_share.getId(),(portal_shareImpl)portal_share) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), portal_share.getClass(), false);
        copier.copy(clientModel, portal_share, null);
    }



}

