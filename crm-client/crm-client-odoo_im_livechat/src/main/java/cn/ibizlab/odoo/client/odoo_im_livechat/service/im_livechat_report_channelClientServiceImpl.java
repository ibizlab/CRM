package cn.ibizlab.odoo.client.odoo_im_livechat.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iim_livechat_report_channel;
import cn.ibizlab.odoo.client.odoo_im_livechat.config.odoo_im_livechatClientProperties;
import cn.ibizlab.odoo.core.client.service.Iim_livechat_report_channelClientService;
import cn.ibizlab.odoo.client.odoo_im_livechat.model.im_livechat_report_channelImpl;
import cn.ibizlab.odoo.client.odoo_im_livechat.feign.im_livechat_report_channelFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[im_livechat_report_channel] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class im_livechat_report_channelClientServiceImpl implements Iim_livechat_report_channelClientService {

    im_livechat_report_channelFeignClient im_livechat_report_channelFeignClient;

    @Autowired
    public im_livechat_report_channelClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_im_livechatClientProperties odoo_im_livechatClientProperties) {
        if (odoo_im_livechatClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.im_livechat_report_channelFeignClient = nameBuilder.target(im_livechat_report_channelFeignClient.class,"http://"+odoo_im_livechatClientProperties.getServiceId()+"/") ;
		}else if (odoo_im_livechatClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.im_livechat_report_channelFeignClient = nameBuilder.target(im_livechat_report_channelFeignClient.class,odoo_im_livechatClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iim_livechat_report_channel createModel() {
		return new im_livechat_report_channelImpl();
	}


    public void createBatch(List<Iim_livechat_report_channel> im_livechat_report_channels){
        if(im_livechat_report_channels!=null){
            List<im_livechat_report_channelImpl> list = new ArrayList<im_livechat_report_channelImpl>();
            for(Iim_livechat_report_channel iim_livechat_report_channel :im_livechat_report_channels){
                list.add((im_livechat_report_channelImpl)iim_livechat_report_channel) ;
            }
            im_livechat_report_channelFeignClient.createBatch(list) ;
        }
    }


    public void get(Iim_livechat_report_channel im_livechat_report_channel){
        Iim_livechat_report_channel clientModel = im_livechat_report_channelFeignClient.get(im_livechat_report_channel.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), im_livechat_report_channel.getClass(), false);
        copier.copy(clientModel, im_livechat_report_channel, null);
    }


    public void create(Iim_livechat_report_channel im_livechat_report_channel){
        Iim_livechat_report_channel clientModel = im_livechat_report_channelFeignClient.create((im_livechat_report_channelImpl)im_livechat_report_channel) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), im_livechat_report_channel.getClass(), false);
        copier.copy(clientModel, im_livechat_report_channel, null);
    }


    public void update(Iim_livechat_report_channel im_livechat_report_channel){
        Iim_livechat_report_channel clientModel = im_livechat_report_channelFeignClient.update(im_livechat_report_channel.getId(),(im_livechat_report_channelImpl)im_livechat_report_channel) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), im_livechat_report_channel.getClass(), false);
        copier.copy(clientModel, im_livechat_report_channel, null);
    }


    public void remove(Iim_livechat_report_channel im_livechat_report_channel){
        im_livechat_report_channelFeignClient.remove(im_livechat_report_channel.getId()) ;
    }


    public void updateBatch(List<Iim_livechat_report_channel> im_livechat_report_channels){
        if(im_livechat_report_channels!=null){
            List<im_livechat_report_channelImpl> list = new ArrayList<im_livechat_report_channelImpl>();
            for(Iim_livechat_report_channel iim_livechat_report_channel :im_livechat_report_channels){
                list.add((im_livechat_report_channelImpl)iim_livechat_report_channel) ;
            }
            im_livechat_report_channelFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iim_livechat_report_channel> fetchDefault(SearchContext context){
        Page<im_livechat_report_channelImpl> page = this.im_livechat_report_channelFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Iim_livechat_report_channel> im_livechat_report_channels){
        if(im_livechat_report_channels!=null){
            List<im_livechat_report_channelImpl> list = new ArrayList<im_livechat_report_channelImpl>();
            for(Iim_livechat_report_channel iim_livechat_report_channel :im_livechat_report_channels){
                list.add((im_livechat_report_channelImpl)iim_livechat_report_channel) ;
            }
            im_livechat_report_channelFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iim_livechat_report_channel> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iim_livechat_report_channel im_livechat_report_channel){
        Iim_livechat_report_channel clientModel = im_livechat_report_channelFeignClient.getDraft(im_livechat_report_channel.getId(),(im_livechat_report_channelImpl)im_livechat_report_channel) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), im_livechat_report_channel.getClass(), false);
        copier.copy(clientModel, im_livechat_report_channel, null);
    }



}

