package cn.ibizlab.odoo.client.odoo_im_livechat.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iim_livechat_report_operator;
import cn.ibizlab.odoo.client.odoo_im_livechat.config.odoo_im_livechatClientProperties;
import cn.ibizlab.odoo.core.client.service.Iim_livechat_report_operatorClientService;
import cn.ibizlab.odoo.client.odoo_im_livechat.model.im_livechat_report_operatorImpl;
import cn.ibizlab.odoo.client.odoo_im_livechat.feign.im_livechat_report_operatorFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[im_livechat_report_operator] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class im_livechat_report_operatorClientServiceImpl implements Iim_livechat_report_operatorClientService {

    im_livechat_report_operatorFeignClient im_livechat_report_operatorFeignClient;

    @Autowired
    public im_livechat_report_operatorClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_im_livechatClientProperties odoo_im_livechatClientProperties) {
        if (odoo_im_livechatClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.im_livechat_report_operatorFeignClient = nameBuilder.target(im_livechat_report_operatorFeignClient.class,"http://"+odoo_im_livechatClientProperties.getServiceId()+"/") ;
		}else if (odoo_im_livechatClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.im_livechat_report_operatorFeignClient = nameBuilder.target(im_livechat_report_operatorFeignClient.class,odoo_im_livechatClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iim_livechat_report_operator createModel() {
		return new im_livechat_report_operatorImpl();
	}


    public Page<Iim_livechat_report_operator> fetchDefault(SearchContext context){
        Page<im_livechat_report_operatorImpl> page = this.im_livechat_report_operatorFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Iim_livechat_report_operator> im_livechat_report_operators){
        if(im_livechat_report_operators!=null){
            List<im_livechat_report_operatorImpl> list = new ArrayList<im_livechat_report_operatorImpl>();
            for(Iim_livechat_report_operator iim_livechat_report_operator :im_livechat_report_operators){
                list.add((im_livechat_report_operatorImpl)iim_livechat_report_operator) ;
            }
            im_livechat_report_operatorFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Iim_livechat_report_operator> im_livechat_report_operators){
        if(im_livechat_report_operators!=null){
            List<im_livechat_report_operatorImpl> list = new ArrayList<im_livechat_report_operatorImpl>();
            for(Iim_livechat_report_operator iim_livechat_report_operator :im_livechat_report_operators){
                list.add((im_livechat_report_operatorImpl)iim_livechat_report_operator) ;
            }
            im_livechat_report_operatorFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Iim_livechat_report_operator> im_livechat_report_operators){
        if(im_livechat_report_operators!=null){
            List<im_livechat_report_operatorImpl> list = new ArrayList<im_livechat_report_operatorImpl>();
            for(Iim_livechat_report_operator iim_livechat_report_operator :im_livechat_report_operators){
                list.add((im_livechat_report_operatorImpl)iim_livechat_report_operator) ;
            }
            im_livechat_report_operatorFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Iim_livechat_report_operator im_livechat_report_operator){
        im_livechat_report_operatorFeignClient.remove(im_livechat_report_operator.getId()) ;
    }


    public void create(Iim_livechat_report_operator im_livechat_report_operator){
        Iim_livechat_report_operator clientModel = im_livechat_report_operatorFeignClient.create((im_livechat_report_operatorImpl)im_livechat_report_operator) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), im_livechat_report_operator.getClass(), false);
        copier.copy(clientModel, im_livechat_report_operator, null);
    }


    public void update(Iim_livechat_report_operator im_livechat_report_operator){
        Iim_livechat_report_operator clientModel = im_livechat_report_operatorFeignClient.update(im_livechat_report_operator.getId(),(im_livechat_report_operatorImpl)im_livechat_report_operator) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), im_livechat_report_operator.getClass(), false);
        copier.copy(clientModel, im_livechat_report_operator, null);
    }


    public void get(Iim_livechat_report_operator im_livechat_report_operator){
        Iim_livechat_report_operator clientModel = im_livechat_report_operatorFeignClient.get(im_livechat_report_operator.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), im_livechat_report_operator.getClass(), false);
        copier.copy(clientModel, im_livechat_report_operator, null);
    }


    public Page<Iim_livechat_report_operator> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iim_livechat_report_operator im_livechat_report_operator){
        Iim_livechat_report_operator clientModel = im_livechat_report_operatorFeignClient.getDraft(im_livechat_report_operator.getId(),(im_livechat_report_operatorImpl)im_livechat_report_operator) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), im_livechat_report_operator.getClass(), false);
        copier.copy(clientModel, im_livechat_report_operator, null);
    }



}

