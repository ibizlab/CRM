package cn.ibizlab.odoo.client.odoo_im_livechat.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iim_livechat_channel_rule;
import cn.ibizlab.odoo.client.odoo_im_livechat.config.odoo_im_livechatClientProperties;
import cn.ibizlab.odoo.core.client.service.Iim_livechat_channel_ruleClientService;
import cn.ibizlab.odoo.client.odoo_im_livechat.model.im_livechat_channel_ruleImpl;
import cn.ibizlab.odoo.client.odoo_im_livechat.feign.im_livechat_channel_ruleFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[im_livechat_channel_rule] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class im_livechat_channel_ruleClientServiceImpl implements Iim_livechat_channel_ruleClientService {

    im_livechat_channel_ruleFeignClient im_livechat_channel_ruleFeignClient;

    @Autowired
    public im_livechat_channel_ruleClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_im_livechatClientProperties odoo_im_livechatClientProperties) {
        if (odoo_im_livechatClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.im_livechat_channel_ruleFeignClient = nameBuilder.target(im_livechat_channel_ruleFeignClient.class,"http://"+odoo_im_livechatClientProperties.getServiceId()+"/") ;
		}else if (odoo_im_livechatClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.im_livechat_channel_ruleFeignClient = nameBuilder.target(im_livechat_channel_ruleFeignClient.class,odoo_im_livechatClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iim_livechat_channel_rule createModel() {
		return new im_livechat_channel_ruleImpl();
	}


    public void createBatch(List<Iim_livechat_channel_rule> im_livechat_channel_rules){
        if(im_livechat_channel_rules!=null){
            List<im_livechat_channel_ruleImpl> list = new ArrayList<im_livechat_channel_ruleImpl>();
            for(Iim_livechat_channel_rule iim_livechat_channel_rule :im_livechat_channel_rules){
                list.add((im_livechat_channel_ruleImpl)iim_livechat_channel_rule) ;
            }
            im_livechat_channel_ruleFeignClient.createBatch(list) ;
        }
    }


    public Page<Iim_livechat_channel_rule> fetchDefault(SearchContext context){
        Page<im_livechat_channel_ruleImpl> page = this.im_livechat_channel_ruleFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Iim_livechat_channel_rule> im_livechat_channel_rules){
        if(im_livechat_channel_rules!=null){
            List<im_livechat_channel_ruleImpl> list = new ArrayList<im_livechat_channel_ruleImpl>();
            for(Iim_livechat_channel_rule iim_livechat_channel_rule :im_livechat_channel_rules){
                list.add((im_livechat_channel_ruleImpl)iim_livechat_channel_rule) ;
            }
            im_livechat_channel_ruleFeignClient.removeBatch(list) ;
        }
    }


    public void get(Iim_livechat_channel_rule im_livechat_channel_rule){
        Iim_livechat_channel_rule clientModel = im_livechat_channel_ruleFeignClient.get(im_livechat_channel_rule.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), im_livechat_channel_rule.getClass(), false);
        copier.copy(clientModel, im_livechat_channel_rule, null);
    }


    public void update(Iim_livechat_channel_rule im_livechat_channel_rule){
        Iim_livechat_channel_rule clientModel = im_livechat_channel_ruleFeignClient.update(im_livechat_channel_rule.getId(),(im_livechat_channel_ruleImpl)im_livechat_channel_rule) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), im_livechat_channel_rule.getClass(), false);
        copier.copy(clientModel, im_livechat_channel_rule, null);
    }


    public void create(Iim_livechat_channel_rule im_livechat_channel_rule){
        Iim_livechat_channel_rule clientModel = im_livechat_channel_ruleFeignClient.create((im_livechat_channel_ruleImpl)im_livechat_channel_rule) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), im_livechat_channel_rule.getClass(), false);
        copier.copy(clientModel, im_livechat_channel_rule, null);
    }


    public void remove(Iim_livechat_channel_rule im_livechat_channel_rule){
        im_livechat_channel_ruleFeignClient.remove(im_livechat_channel_rule.getId()) ;
    }


    public void updateBatch(List<Iim_livechat_channel_rule> im_livechat_channel_rules){
        if(im_livechat_channel_rules!=null){
            List<im_livechat_channel_ruleImpl> list = new ArrayList<im_livechat_channel_ruleImpl>();
            for(Iim_livechat_channel_rule iim_livechat_channel_rule :im_livechat_channel_rules){
                list.add((im_livechat_channel_ruleImpl)iim_livechat_channel_rule) ;
            }
            im_livechat_channel_ruleFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iim_livechat_channel_rule> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iim_livechat_channel_rule im_livechat_channel_rule){
        Iim_livechat_channel_rule clientModel = im_livechat_channel_ruleFeignClient.getDraft(im_livechat_channel_rule.getId(),(im_livechat_channel_ruleImpl)im_livechat_channel_rule) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), im_livechat_channel_rule.getClass(), false);
        copier.copy(clientModel, im_livechat_channel_rule, null);
    }



}

