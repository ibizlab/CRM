package cn.ibizlab.odoo.client.odoo_im_livechat.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iim_livechat_channel;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[im_livechat_channel] 对象
 */
public class im_livechat_channelImpl implements Iim_livechat_channel,Serializable{

    /**
     * 您是否在频道中？
     */
    public String are_you_inside;

    @JsonIgnore
    public boolean are_you_insideDirtyFlag;
    
    /**
     * 按钮的文本
     */
    public String button_text;

    @JsonIgnore
    public boolean button_textDirtyFlag;
    
    /**
     * 会话
     */
    public String channel_ids;

    @JsonIgnore
    public boolean channel_idsDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 欢迎信息
     */
    public String default_message;

    @JsonIgnore
    public boolean default_messageDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 图像
     */
    public byte[] image;

    @JsonIgnore
    public boolean imageDirtyFlag;
    
    /**
     * 普通
     */
    public byte[] image_medium;

    @JsonIgnore
    public boolean image_mediumDirtyFlag;
    
    /**
     * 缩略图
     */
    public byte[] image_small;

    @JsonIgnore
    public boolean image_smallDirtyFlag;
    
    /**
     * 聊天输入为空时显示
     */
    public String input_placeholder;

    @JsonIgnore
    public boolean input_placeholderDirtyFlag;
    
    /**
     * 已发布
     */
    public String is_published;

    @JsonIgnore
    public boolean is_publishedDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 对话数
     */
    public Integer nbr_channel;

    @JsonIgnore
    public boolean nbr_channelDirtyFlag;
    
    /**
     * % 高兴
     */
    public Integer rating_percentage_satisfaction;

    @JsonIgnore
    public boolean rating_percentage_satisfactionDirtyFlag;
    
    /**
     * 规则
     */
    public String rule_ids;

    @JsonIgnore
    public boolean rule_idsDirtyFlag;
    
    /**
     * 脚本（外部）
     */
    public String script_external;

    @JsonIgnore
    public boolean script_externalDirtyFlag;
    
    /**
     * 操作员
     */
    public String user_ids;

    @JsonIgnore
    public boolean user_idsDirtyFlag;
    
    /**
     * 网站说明
     */
    public String website_description;

    @JsonIgnore
    public boolean website_descriptionDirtyFlag;
    
    /**
     * 在当前网站显示
     */
    public String website_published;

    @JsonIgnore
    public boolean website_publishedDirtyFlag;
    
    /**
     * 网站网址
     */
    public String website_url;

    @JsonIgnore
    public boolean website_urlDirtyFlag;
    
    /**
     * Web页
     */
    public String web_page;

    @JsonIgnore
    public boolean web_pageDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [您是否在频道中？]
     */
    @JsonProperty("are_you_inside")
    public String getAre_you_inside(){
        return this.are_you_inside ;
    }

    /**
     * 设置 [您是否在频道中？]
     */
    @JsonProperty("are_you_inside")
    public void setAre_you_inside(String  are_you_inside){
        this.are_you_inside = are_you_inside ;
        this.are_you_insideDirtyFlag = true ;
    }

     /**
     * 获取 [您是否在频道中？]脏标记
     */
    @JsonIgnore
    public boolean getAre_you_insideDirtyFlag(){
        return this.are_you_insideDirtyFlag ;
    }   

    /**
     * 获取 [按钮的文本]
     */
    @JsonProperty("button_text")
    public String getButton_text(){
        return this.button_text ;
    }

    /**
     * 设置 [按钮的文本]
     */
    @JsonProperty("button_text")
    public void setButton_text(String  button_text){
        this.button_text = button_text ;
        this.button_textDirtyFlag = true ;
    }

     /**
     * 获取 [按钮的文本]脏标记
     */
    @JsonIgnore
    public boolean getButton_textDirtyFlag(){
        return this.button_textDirtyFlag ;
    }   

    /**
     * 获取 [会话]
     */
    @JsonProperty("channel_ids")
    public String getChannel_ids(){
        return this.channel_ids ;
    }

    /**
     * 设置 [会话]
     */
    @JsonProperty("channel_ids")
    public void setChannel_ids(String  channel_ids){
        this.channel_ids = channel_ids ;
        this.channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [会话]脏标记
     */
    @JsonIgnore
    public boolean getChannel_idsDirtyFlag(){
        return this.channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [欢迎信息]
     */
    @JsonProperty("default_message")
    public String getDefault_message(){
        return this.default_message ;
    }

    /**
     * 设置 [欢迎信息]
     */
    @JsonProperty("default_message")
    public void setDefault_message(String  default_message){
        this.default_message = default_message ;
        this.default_messageDirtyFlag = true ;
    }

     /**
     * 获取 [欢迎信息]脏标记
     */
    @JsonIgnore
    public boolean getDefault_messageDirtyFlag(){
        return this.default_messageDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [图像]
     */
    @JsonProperty("image")
    public byte[] getImage(){
        return this.image ;
    }

    /**
     * 设置 [图像]
     */
    @JsonProperty("image")
    public void setImage(byte[]  image){
        this.image = image ;
        this.imageDirtyFlag = true ;
    }

     /**
     * 获取 [图像]脏标记
     */
    @JsonIgnore
    public boolean getImageDirtyFlag(){
        return this.imageDirtyFlag ;
    }   

    /**
     * 获取 [普通]
     */
    @JsonProperty("image_medium")
    public byte[] getImage_medium(){
        return this.image_medium ;
    }

    /**
     * 设置 [普通]
     */
    @JsonProperty("image_medium")
    public void setImage_medium(byte[]  image_medium){
        this.image_medium = image_medium ;
        this.image_mediumDirtyFlag = true ;
    }

     /**
     * 获取 [普通]脏标记
     */
    @JsonIgnore
    public boolean getImage_mediumDirtyFlag(){
        return this.image_mediumDirtyFlag ;
    }   

    /**
     * 获取 [缩略图]
     */
    @JsonProperty("image_small")
    public byte[] getImage_small(){
        return this.image_small ;
    }

    /**
     * 设置 [缩略图]
     */
    @JsonProperty("image_small")
    public void setImage_small(byte[]  image_small){
        this.image_small = image_small ;
        this.image_smallDirtyFlag = true ;
    }

     /**
     * 获取 [缩略图]脏标记
     */
    @JsonIgnore
    public boolean getImage_smallDirtyFlag(){
        return this.image_smallDirtyFlag ;
    }   

    /**
     * 获取 [聊天输入为空时显示]
     */
    @JsonProperty("input_placeholder")
    public String getInput_placeholder(){
        return this.input_placeholder ;
    }

    /**
     * 设置 [聊天输入为空时显示]
     */
    @JsonProperty("input_placeholder")
    public void setInput_placeholder(String  input_placeholder){
        this.input_placeholder = input_placeholder ;
        this.input_placeholderDirtyFlag = true ;
    }

     /**
     * 获取 [聊天输入为空时显示]脏标记
     */
    @JsonIgnore
    public boolean getInput_placeholderDirtyFlag(){
        return this.input_placeholderDirtyFlag ;
    }   

    /**
     * 获取 [已发布]
     */
    @JsonProperty("is_published")
    public String getIs_published(){
        return this.is_published ;
    }

    /**
     * 设置 [已发布]
     */
    @JsonProperty("is_published")
    public void setIs_published(String  is_published){
        this.is_published = is_published ;
        this.is_publishedDirtyFlag = true ;
    }

     /**
     * 获取 [已发布]脏标记
     */
    @JsonIgnore
    public boolean getIs_publishedDirtyFlag(){
        return this.is_publishedDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [对话数]
     */
    @JsonProperty("nbr_channel")
    public Integer getNbr_channel(){
        return this.nbr_channel ;
    }

    /**
     * 设置 [对话数]
     */
    @JsonProperty("nbr_channel")
    public void setNbr_channel(Integer  nbr_channel){
        this.nbr_channel = nbr_channel ;
        this.nbr_channelDirtyFlag = true ;
    }

     /**
     * 获取 [对话数]脏标记
     */
    @JsonIgnore
    public boolean getNbr_channelDirtyFlag(){
        return this.nbr_channelDirtyFlag ;
    }   

    /**
     * 获取 [% 高兴]
     */
    @JsonProperty("rating_percentage_satisfaction")
    public Integer getRating_percentage_satisfaction(){
        return this.rating_percentage_satisfaction ;
    }

    /**
     * 设置 [% 高兴]
     */
    @JsonProperty("rating_percentage_satisfaction")
    public void setRating_percentage_satisfaction(Integer  rating_percentage_satisfaction){
        this.rating_percentage_satisfaction = rating_percentage_satisfaction ;
        this.rating_percentage_satisfactionDirtyFlag = true ;
    }

     /**
     * 获取 [% 高兴]脏标记
     */
    @JsonIgnore
    public boolean getRating_percentage_satisfactionDirtyFlag(){
        return this.rating_percentage_satisfactionDirtyFlag ;
    }   

    /**
     * 获取 [规则]
     */
    @JsonProperty("rule_ids")
    public String getRule_ids(){
        return this.rule_ids ;
    }

    /**
     * 设置 [规则]
     */
    @JsonProperty("rule_ids")
    public void setRule_ids(String  rule_ids){
        this.rule_ids = rule_ids ;
        this.rule_idsDirtyFlag = true ;
    }

     /**
     * 获取 [规则]脏标记
     */
    @JsonIgnore
    public boolean getRule_idsDirtyFlag(){
        return this.rule_idsDirtyFlag ;
    }   

    /**
     * 获取 [脚本（外部）]
     */
    @JsonProperty("script_external")
    public String getScript_external(){
        return this.script_external ;
    }

    /**
     * 设置 [脚本（外部）]
     */
    @JsonProperty("script_external")
    public void setScript_external(String  script_external){
        this.script_external = script_external ;
        this.script_externalDirtyFlag = true ;
    }

     /**
     * 获取 [脚本（外部）]脏标记
     */
    @JsonIgnore
    public boolean getScript_externalDirtyFlag(){
        return this.script_externalDirtyFlag ;
    }   

    /**
     * 获取 [操作员]
     */
    @JsonProperty("user_ids")
    public String getUser_ids(){
        return this.user_ids ;
    }

    /**
     * 设置 [操作员]
     */
    @JsonProperty("user_ids")
    public void setUser_ids(String  user_ids){
        this.user_ids = user_ids ;
        this.user_idsDirtyFlag = true ;
    }

     /**
     * 获取 [操作员]脏标记
     */
    @JsonIgnore
    public boolean getUser_idsDirtyFlag(){
        return this.user_idsDirtyFlag ;
    }   

    /**
     * 获取 [网站说明]
     */
    @JsonProperty("website_description")
    public String getWebsite_description(){
        return this.website_description ;
    }

    /**
     * 设置 [网站说明]
     */
    @JsonProperty("website_description")
    public void setWebsite_description(String  website_description){
        this.website_description = website_description ;
        this.website_descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [网站说明]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_descriptionDirtyFlag(){
        return this.website_descriptionDirtyFlag ;
    }   

    /**
     * 获取 [在当前网站显示]
     */
    @JsonProperty("website_published")
    public String getWebsite_published(){
        return this.website_published ;
    }

    /**
     * 设置 [在当前网站显示]
     */
    @JsonProperty("website_published")
    public void setWebsite_published(String  website_published){
        this.website_published = website_published ;
        this.website_publishedDirtyFlag = true ;
    }

     /**
     * 获取 [在当前网站显示]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_publishedDirtyFlag(){
        return this.website_publishedDirtyFlag ;
    }   

    /**
     * 获取 [网站网址]
     */
    @JsonProperty("website_url")
    public String getWebsite_url(){
        return this.website_url ;
    }

    /**
     * 设置 [网站网址]
     */
    @JsonProperty("website_url")
    public void setWebsite_url(String  website_url){
        this.website_url = website_url ;
        this.website_urlDirtyFlag = true ;
    }

     /**
     * 获取 [网站网址]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_urlDirtyFlag(){
        return this.website_urlDirtyFlag ;
    }   

    /**
     * 获取 [Web页]
     */
    @JsonProperty("web_page")
    public String getWeb_page(){
        return this.web_page ;
    }

    /**
     * 设置 [Web页]
     */
    @JsonProperty("web_page")
    public void setWeb_page(String  web_page){
        this.web_page = web_page ;
        this.web_pageDirtyFlag = true ;
    }

     /**
     * 获取 [Web页]脏标记
     */
    @JsonIgnore
    public boolean getWeb_pageDirtyFlag(){
        return this.web_pageDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
