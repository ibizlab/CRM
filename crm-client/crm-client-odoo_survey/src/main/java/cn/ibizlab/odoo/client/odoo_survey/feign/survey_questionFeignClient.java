package cn.ibizlab.odoo.client.odoo_survey.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Isurvey_question;
import cn.ibizlab.odoo.client.odoo_survey.model.survey_questionImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[survey_question] 服务对象接口
 */
public interface survey_questionFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_questions/fetchdefault")
    public Page<survey_questionImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_questions/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_questions/{id}")
    public survey_questionImpl update(@PathVariable("id") Integer id,@RequestBody survey_questionImpl survey_question);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_questions/{id}")
    public survey_questionImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_questions")
    public survey_questionImpl create(@RequestBody survey_questionImpl survey_question);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_questions/createbatch")
    public survey_questionImpl createBatch(@RequestBody List<survey_questionImpl> survey_questions);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_questions/updatebatch")
    public survey_questionImpl updateBatch(@RequestBody List<survey_questionImpl> survey_questions);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_questions/removebatch")
    public survey_questionImpl removeBatch(@RequestBody List<survey_questionImpl> survey_questions);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_questions/select")
    public Page<survey_questionImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_questions/{id}/getdraft")
    public survey_questionImpl getDraft(@PathVariable("id") Integer id,@RequestBody survey_questionImpl survey_question);



}
