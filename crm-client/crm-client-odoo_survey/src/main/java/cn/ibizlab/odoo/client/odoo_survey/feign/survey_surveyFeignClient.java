package cn.ibizlab.odoo.client.odoo_survey.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Isurvey_survey;
import cn.ibizlab.odoo.client.odoo_survey.model.survey_surveyImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[survey_survey] 服务对象接口
 */
public interface survey_surveyFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_surveys")
    public survey_surveyImpl create(@RequestBody survey_surveyImpl survey_survey);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_surveys/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_surveys/createbatch")
    public survey_surveyImpl createBatch(@RequestBody List<survey_surveyImpl> survey_surveys);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_surveys/{id}")
    public survey_surveyImpl update(@PathVariable("id") Integer id,@RequestBody survey_surveyImpl survey_survey);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_surveys/updatebatch")
    public survey_surveyImpl updateBatch(@RequestBody List<survey_surveyImpl> survey_surveys);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_surveys/removebatch")
    public survey_surveyImpl removeBatch(@RequestBody List<survey_surveyImpl> survey_surveys);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_surveys/{id}")
    public survey_surveyImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_surveys/fetchdefault")
    public Page<survey_surveyImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_surveys/select")
    public Page<survey_surveyImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_surveys/{id}/getdraft")
    public survey_surveyImpl getDraft(@PathVariable("id") Integer id,@RequestBody survey_surveyImpl survey_survey);



}
