package cn.ibizlab.odoo.client.odoo_survey.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Isurvey_stage;
import cn.ibizlab.odoo.client.odoo_survey.model.survey_stageImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[survey_stage] 服务对象接口
 */
public interface survey_stageFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_stages/updatebatch")
    public survey_stageImpl updateBatch(@RequestBody List<survey_stageImpl> survey_stages);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_stages/removebatch")
    public survey_stageImpl removeBatch(@RequestBody List<survey_stageImpl> survey_stages);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_stages")
    public survey_stageImpl create(@RequestBody survey_stageImpl survey_stage);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_stages/fetchdefault")
    public Page<survey_stageImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_stages/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_stages/createbatch")
    public survey_stageImpl createBatch(@RequestBody List<survey_stageImpl> survey_stages);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_stages/{id}")
    public survey_stageImpl update(@PathVariable("id") Integer id,@RequestBody survey_stageImpl survey_stage);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_stages/{id}")
    public survey_stageImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_stages/select")
    public Page<survey_stageImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_stages/{id}/getdraft")
    public survey_stageImpl getDraft(@PathVariable("id") Integer id,@RequestBody survey_stageImpl survey_stage);



}
