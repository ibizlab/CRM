package cn.ibizlab.odoo.client.odoo_survey.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Isurvey_user_input_line;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[survey_user_input_line] 对象
 */
public class survey_user_input_lineImpl implements Isurvey_user_input_line,Serializable{

    /**
     * 回复类型
     */
    public String answer_type;

    @JsonIgnore
    public boolean answer_typeDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 创建日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_create;

    @JsonIgnore
    public boolean date_createDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 页
     */
    public Integer page_id;

    @JsonIgnore
    public boolean page_idDirtyFlag;
    
    /**
     * 疑问
     */
    public Integer question_id;

    @JsonIgnore
    public boolean question_idDirtyFlag;
    
    /**
     * 这个选项分配的分数
     */
    public Double quizz_mark;

    @JsonIgnore
    public boolean quizz_markDirtyFlag;
    
    /**
     * 忽略
     */
    public String skipped;

    @JsonIgnore
    public boolean skippedDirtyFlag;
    
    /**
     * 问卷
     */
    public Integer survey_id;

    @JsonIgnore
    public boolean survey_idDirtyFlag;
    
    /**
     * 用户输入
     */
    public Integer user_input_id;

    @JsonIgnore
    public boolean user_input_idDirtyFlag;
    
    /**
     * 回复日期
     */
    public Timestamp value_date;

    @JsonIgnore
    public boolean value_dateDirtyFlag;
    
    /**
     * 自由文本答案
     */
    public String value_free_text;

    @JsonIgnore
    public boolean value_free_textDirtyFlag;
    
    /**
     * 数字答案
     */
    public Double value_number;

    @JsonIgnore
    public boolean value_numberDirtyFlag;
    
    /**
     * 建议答案
     */
    public Integer value_suggested;

    @JsonIgnore
    public boolean value_suggestedDirtyFlag;
    
    /**
     * 答案行
     */
    public Integer value_suggested_row;

    @JsonIgnore
    public boolean value_suggested_rowDirtyFlag;
    
    /**
     * 文本答案
     */
    public String value_text;

    @JsonIgnore
    public boolean value_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [回复类型]
     */
    @JsonProperty("answer_type")
    public String getAnswer_type(){
        return this.answer_type ;
    }

    /**
     * 设置 [回复类型]
     */
    @JsonProperty("answer_type")
    public void setAnswer_type(String  answer_type){
        this.answer_type = answer_type ;
        this.answer_typeDirtyFlag = true ;
    }

     /**
     * 获取 [回复类型]脏标记
     */
    @JsonIgnore
    public boolean getAnswer_typeDirtyFlag(){
        return this.answer_typeDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [创建日期]
     */
    @JsonProperty("date_create")
    public Timestamp getDate_create(){
        return this.date_create ;
    }

    /**
     * 设置 [创建日期]
     */
    @JsonProperty("date_create")
    public void setDate_create(Timestamp  date_create){
        this.date_create = date_create ;
        this.date_createDirtyFlag = true ;
    }

     /**
     * 获取 [创建日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_createDirtyFlag(){
        return this.date_createDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [页]
     */
    @JsonProperty("page_id")
    public Integer getPage_id(){
        return this.page_id ;
    }

    /**
     * 设置 [页]
     */
    @JsonProperty("page_id")
    public void setPage_id(Integer  page_id){
        this.page_id = page_id ;
        this.page_idDirtyFlag = true ;
    }

     /**
     * 获取 [页]脏标记
     */
    @JsonIgnore
    public boolean getPage_idDirtyFlag(){
        return this.page_idDirtyFlag ;
    }   

    /**
     * 获取 [疑问]
     */
    @JsonProperty("question_id")
    public Integer getQuestion_id(){
        return this.question_id ;
    }

    /**
     * 设置 [疑问]
     */
    @JsonProperty("question_id")
    public void setQuestion_id(Integer  question_id){
        this.question_id = question_id ;
        this.question_idDirtyFlag = true ;
    }

     /**
     * 获取 [疑问]脏标记
     */
    @JsonIgnore
    public boolean getQuestion_idDirtyFlag(){
        return this.question_idDirtyFlag ;
    }   

    /**
     * 获取 [这个选项分配的分数]
     */
    @JsonProperty("quizz_mark")
    public Double getQuizz_mark(){
        return this.quizz_mark ;
    }

    /**
     * 设置 [这个选项分配的分数]
     */
    @JsonProperty("quizz_mark")
    public void setQuizz_mark(Double  quizz_mark){
        this.quizz_mark = quizz_mark ;
        this.quizz_markDirtyFlag = true ;
    }

     /**
     * 获取 [这个选项分配的分数]脏标记
     */
    @JsonIgnore
    public boolean getQuizz_markDirtyFlag(){
        return this.quizz_markDirtyFlag ;
    }   

    /**
     * 获取 [忽略]
     */
    @JsonProperty("skipped")
    public String getSkipped(){
        return this.skipped ;
    }

    /**
     * 设置 [忽略]
     */
    @JsonProperty("skipped")
    public void setSkipped(String  skipped){
        this.skipped = skipped ;
        this.skippedDirtyFlag = true ;
    }

     /**
     * 获取 [忽略]脏标记
     */
    @JsonIgnore
    public boolean getSkippedDirtyFlag(){
        return this.skippedDirtyFlag ;
    }   

    /**
     * 获取 [问卷]
     */
    @JsonProperty("survey_id")
    public Integer getSurvey_id(){
        return this.survey_id ;
    }

    /**
     * 设置 [问卷]
     */
    @JsonProperty("survey_id")
    public void setSurvey_id(Integer  survey_id){
        this.survey_id = survey_id ;
        this.survey_idDirtyFlag = true ;
    }

     /**
     * 获取 [问卷]脏标记
     */
    @JsonIgnore
    public boolean getSurvey_idDirtyFlag(){
        return this.survey_idDirtyFlag ;
    }   

    /**
     * 获取 [用户输入]
     */
    @JsonProperty("user_input_id")
    public Integer getUser_input_id(){
        return this.user_input_id ;
    }

    /**
     * 设置 [用户输入]
     */
    @JsonProperty("user_input_id")
    public void setUser_input_id(Integer  user_input_id){
        this.user_input_id = user_input_id ;
        this.user_input_idDirtyFlag = true ;
    }

     /**
     * 获取 [用户输入]脏标记
     */
    @JsonIgnore
    public boolean getUser_input_idDirtyFlag(){
        return this.user_input_idDirtyFlag ;
    }   

    /**
     * 获取 [回复日期]
     */
    @JsonProperty("value_date")
    public Timestamp getValue_date(){
        return this.value_date ;
    }

    /**
     * 设置 [回复日期]
     */
    @JsonProperty("value_date")
    public void setValue_date(Timestamp  value_date){
        this.value_date = value_date ;
        this.value_dateDirtyFlag = true ;
    }

     /**
     * 获取 [回复日期]脏标记
     */
    @JsonIgnore
    public boolean getValue_dateDirtyFlag(){
        return this.value_dateDirtyFlag ;
    }   

    /**
     * 获取 [自由文本答案]
     */
    @JsonProperty("value_free_text")
    public String getValue_free_text(){
        return this.value_free_text ;
    }

    /**
     * 设置 [自由文本答案]
     */
    @JsonProperty("value_free_text")
    public void setValue_free_text(String  value_free_text){
        this.value_free_text = value_free_text ;
        this.value_free_textDirtyFlag = true ;
    }

     /**
     * 获取 [自由文本答案]脏标记
     */
    @JsonIgnore
    public boolean getValue_free_textDirtyFlag(){
        return this.value_free_textDirtyFlag ;
    }   

    /**
     * 获取 [数字答案]
     */
    @JsonProperty("value_number")
    public Double getValue_number(){
        return this.value_number ;
    }

    /**
     * 设置 [数字答案]
     */
    @JsonProperty("value_number")
    public void setValue_number(Double  value_number){
        this.value_number = value_number ;
        this.value_numberDirtyFlag = true ;
    }

     /**
     * 获取 [数字答案]脏标记
     */
    @JsonIgnore
    public boolean getValue_numberDirtyFlag(){
        return this.value_numberDirtyFlag ;
    }   

    /**
     * 获取 [建议答案]
     */
    @JsonProperty("value_suggested")
    public Integer getValue_suggested(){
        return this.value_suggested ;
    }

    /**
     * 设置 [建议答案]
     */
    @JsonProperty("value_suggested")
    public void setValue_suggested(Integer  value_suggested){
        this.value_suggested = value_suggested ;
        this.value_suggestedDirtyFlag = true ;
    }

     /**
     * 获取 [建议答案]脏标记
     */
    @JsonIgnore
    public boolean getValue_suggestedDirtyFlag(){
        return this.value_suggestedDirtyFlag ;
    }   

    /**
     * 获取 [答案行]
     */
    @JsonProperty("value_suggested_row")
    public Integer getValue_suggested_row(){
        return this.value_suggested_row ;
    }

    /**
     * 设置 [答案行]
     */
    @JsonProperty("value_suggested_row")
    public void setValue_suggested_row(Integer  value_suggested_row){
        this.value_suggested_row = value_suggested_row ;
        this.value_suggested_rowDirtyFlag = true ;
    }

     /**
     * 获取 [答案行]脏标记
     */
    @JsonIgnore
    public boolean getValue_suggested_rowDirtyFlag(){
        return this.value_suggested_rowDirtyFlag ;
    }   

    /**
     * 获取 [文本答案]
     */
    @JsonProperty("value_text")
    public String getValue_text(){
        return this.value_text ;
    }

    /**
     * 设置 [文本答案]
     */
    @JsonProperty("value_text")
    public void setValue_text(String  value_text){
        this.value_text = value_text ;
        this.value_textDirtyFlag = true ;
    }

     /**
     * 获取 [文本答案]脏标记
     */
    @JsonIgnore
    public boolean getValue_textDirtyFlag(){
        return this.value_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
