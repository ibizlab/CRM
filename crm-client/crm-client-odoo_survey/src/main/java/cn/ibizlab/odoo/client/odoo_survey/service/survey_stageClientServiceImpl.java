package cn.ibizlab.odoo.client.odoo_survey.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isurvey_stage;
import cn.ibizlab.odoo.client.odoo_survey.config.odoo_surveyClientProperties;
import cn.ibizlab.odoo.core.client.service.Isurvey_stageClientService;
import cn.ibizlab.odoo.client.odoo_survey.model.survey_stageImpl;
import cn.ibizlab.odoo.client.odoo_survey.feign.survey_stageFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[survey_stage] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class survey_stageClientServiceImpl implements Isurvey_stageClientService {

    survey_stageFeignClient survey_stageFeignClient;

    @Autowired
    public survey_stageClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_surveyClientProperties odoo_surveyClientProperties) {
        if (odoo_surveyClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.survey_stageFeignClient = nameBuilder.target(survey_stageFeignClient.class,"http://"+odoo_surveyClientProperties.getServiceId()+"/") ;
		}else if (odoo_surveyClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.survey_stageFeignClient = nameBuilder.target(survey_stageFeignClient.class,odoo_surveyClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Isurvey_stage createModel() {
		return new survey_stageImpl();
	}


    public void updateBatch(List<Isurvey_stage> survey_stages){
        if(survey_stages!=null){
            List<survey_stageImpl> list = new ArrayList<survey_stageImpl>();
            for(Isurvey_stage isurvey_stage :survey_stages){
                list.add((survey_stageImpl)isurvey_stage) ;
            }
            survey_stageFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Isurvey_stage> survey_stages){
        if(survey_stages!=null){
            List<survey_stageImpl> list = new ArrayList<survey_stageImpl>();
            for(Isurvey_stage isurvey_stage :survey_stages){
                list.add((survey_stageImpl)isurvey_stage) ;
            }
            survey_stageFeignClient.removeBatch(list) ;
        }
    }


    public void create(Isurvey_stage survey_stage){
        Isurvey_stage clientModel = survey_stageFeignClient.create((survey_stageImpl)survey_stage) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_stage.getClass(), false);
        copier.copy(clientModel, survey_stage, null);
    }


    public Page<Isurvey_stage> fetchDefault(SearchContext context){
        Page<survey_stageImpl> page = this.survey_stageFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Isurvey_stage survey_stage){
        survey_stageFeignClient.remove(survey_stage.getId()) ;
    }


    public void createBatch(List<Isurvey_stage> survey_stages){
        if(survey_stages!=null){
            List<survey_stageImpl> list = new ArrayList<survey_stageImpl>();
            for(Isurvey_stage isurvey_stage :survey_stages){
                list.add((survey_stageImpl)isurvey_stage) ;
            }
            survey_stageFeignClient.createBatch(list) ;
        }
    }


    public void update(Isurvey_stage survey_stage){
        Isurvey_stage clientModel = survey_stageFeignClient.update(survey_stage.getId(),(survey_stageImpl)survey_stage) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_stage.getClass(), false);
        copier.copy(clientModel, survey_stage, null);
    }


    public void get(Isurvey_stage survey_stage){
        Isurvey_stage clientModel = survey_stageFeignClient.get(survey_stage.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_stage.getClass(), false);
        copier.copy(clientModel, survey_stage, null);
    }


    public Page<Isurvey_stage> select(SearchContext context){
        return null ;
    }


    public void getDraft(Isurvey_stage survey_stage){
        Isurvey_stage clientModel = survey_stageFeignClient.getDraft(survey_stage.getId(),(survey_stageImpl)survey_stage) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_stage.getClass(), false);
        copier.copy(clientModel, survey_stage, null);
    }



}

