package cn.ibizlab.odoo.client.odoo_survey.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isurvey_survey;
import cn.ibizlab.odoo.client.odoo_survey.config.odoo_surveyClientProperties;
import cn.ibizlab.odoo.core.client.service.Isurvey_surveyClientService;
import cn.ibizlab.odoo.client.odoo_survey.model.survey_surveyImpl;
import cn.ibizlab.odoo.client.odoo_survey.feign.survey_surveyFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[survey_survey] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class survey_surveyClientServiceImpl implements Isurvey_surveyClientService {

    survey_surveyFeignClient survey_surveyFeignClient;

    @Autowired
    public survey_surveyClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_surveyClientProperties odoo_surveyClientProperties) {
        if (odoo_surveyClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.survey_surveyFeignClient = nameBuilder.target(survey_surveyFeignClient.class,"http://"+odoo_surveyClientProperties.getServiceId()+"/") ;
		}else if (odoo_surveyClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.survey_surveyFeignClient = nameBuilder.target(survey_surveyFeignClient.class,odoo_surveyClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Isurvey_survey createModel() {
		return new survey_surveyImpl();
	}


    public void create(Isurvey_survey survey_survey){
        Isurvey_survey clientModel = survey_surveyFeignClient.create((survey_surveyImpl)survey_survey) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_survey.getClass(), false);
        copier.copy(clientModel, survey_survey, null);
    }


    public void remove(Isurvey_survey survey_survey){
        survey_surveyFeignClient.remove(survey_survey.getId()) ;
    }


    public void createBatch(List<Isurvey_survey> survey_surveys){
        if(survey_surveys!=null){
            List<survey_surveyImpl> list = new ArrayList<survey_surveyImpl>();
            for(Isurvey_survey isurvey_survey :survey_surveys){
                list.add((survey_surveyImpl)isurvey_survey) ;
            }
            survey_surveyFeignClient.createBatch(list) ;
        }
    }


    public void update(Isurvey_survey survey_survey){
        Isurvey_survey clientModel = survey_surveyFeignClient.update(survey_survey.getId(),(survey_surveyImpl)survey_survey) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_survey.getClass(), false);
        copier.copy(clientModel, survey_survey, null);
    }


    public void updateBatch(List<Isurvey_survey> survey_surveys){
        if(survey_surveys!=null){
            List<survey_surveyImpl> list = new ArrayList<survey_surveyImpl>();
            for(Isurvey_survey isurvey_survey :survey_surveys){
                list.add((survey_surveyImpl)isurvey_survey) ;
            }
            survey_surveyFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Isurvey_survey> survey_surveys){
        if(survey_surveys!=null){
            List<survey_surveyImpl> list = new ArrayList<survey_surveyImpl>();
            for(Isurvey_survey isurvey_survey :survey_surveys){
                list.add((survey_surveyImpl)isurvey_survey) ;
            }
            survey_surveyFeignClient.removeBatch(list) ;
        }
    }


    public void get(Isurvey_survey survey_survey){
        Isurvey_survey clientModel = survey_surveyFeignClient.get(survey_survey.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_survey.getClass(), false);
        copier.copy(clientModel, survey_survey, null);
    }


    public Page<Isurvey_survey> fetchDefault(SearchContext context){
        Page<survey_surveyImpl> page = this.survey_surveyFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Isurvey_survey> select(SearchContext context){
        return null ;
    }


    public void getDraft(Isurvey_survey survey_survey){
        Isurvey_survey clientModel = survey_surveyFeignClient.getDraft(survey_survey.getId(),(survey_surveyImpl)survey_survey) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_survey.getClass(), false);
        copier.copy(clientModel, survey_survey, null);
    }



}

