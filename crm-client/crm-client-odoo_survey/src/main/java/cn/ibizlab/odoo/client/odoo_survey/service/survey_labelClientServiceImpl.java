package cn.ibizlab.odoo.client.odoo_survey.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isurvey_label;
import cn.ibizlab.odoo.client.odoo_survey.config.odoo_surveyClientProperties;
import cn.ibizlab.odoo.core.client.service.Isurvey_labelClientService;
import cn.ibizlab.odoo.client.odoo_survey.model.survey_labelImpl;
import cn.ibizlab.odoo.client.odoo_survey.feign.survey_labelFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[survey_label] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class survey_labelClientServiceImpl implements Isurvey_labelClientService {

    survey_labelFeignClient survey_labelFeignClient;

    @Autowired
    public survey_labelClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_surveyClientProperties odoo_surveyClientProperties) {
        if (odoo_surveyClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.survey_labelFeignClient = nameBuilder.target(survey_labelFeignClient.class,"http://"+odoo_surveyClientProperties.getServiceId()+"/") ;
		}else if (odoo_surveyClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.survey_labelFeignClient = nameBuilder.target(survey_labelFeignClient.class,odoo_surveyClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Isurvey_label createModel() {
		return new survey_labelImpl();
	}


    public void remove(Isurvey_label survey_label){
        survey_labelFeignClient.remove(survey_label.getId()) ;
    }


    public void updateBatch(List<Isurvey_label> survey_labels){
        if(survey_labels!=null){
            List<survey_labelImpl> list = new ArrayList<survey_labelImpl>();
            for(Isurvey_label isurvey_label :survey_labels){
                list.add((survey_labelImpl)isurvey_label) ;
            }
            survey_labelFeignClient.updateBatch(list) ;
        }
    }


    public void update(Isurvey_label survey_label){
        Isurvey_label clientModel = survey_labelFeignClient.update(survey_label.getId(),(survey_labelImpl)survey_label) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_label.getClass(), false);
        copier.copy(clientModel, survey_label, null);
    }


    public void get(Isurvey_label survey_label){
        Isurvey_label clientModel = survey_labelFeignClient.get(survey_label.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_label.getClass(), false);
        copier.copy(clientModel, survey_label, null);
    }


    public Page<Isurvey_label> fetchDefault(SearchContext context){
        Page<survey_labelImpl> page = this.survey_labelFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Isurvey_label> survey_labels){
        if(survey_labels!=null){
            List<survey_labelImpl> list = new ArrayList<survey_labelImpl>();
            for(Isurvey_label isurvey_label :survey_labels){
                list.add((survey_labelImpl)isurvey_label) ;
            }
            survey_labelFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Isurvey_label> survey_labels){
        if(survey_labels!=null){
            List<survey_labelImpl> list = new ArrayList<survey_labelImpl>();
            for(Isurvey_label isurvey_label :survey_labels){
                list.add((survey_labelImpl)isurvey_label) ;
            }
            survey_labelFeignClient.createBatch(list) ;
        }
    }


    public void create(Isurvey_label survey_label){
        Isurvey_label clientModel = survey_labelFeignClient.create((survey_labelImpl)survey_label) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_label.getClass(), false);
        copier.copy(clientModel, survey_label, null);
    }


    public Page<Isurvey_label> select(SearchContext context){
        return null ;
    }


    public void getDraft(Isurvey_label survey_label){
        Isurvey_label clientModel = survey_labelFeignClient.getDraft(survey_label.getId(),(survey_labelImpl)survey_label) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), survey_label.getClass(), false);
        copier.copy(clientModel, survey_label, null);
    }



}

