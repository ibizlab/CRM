package cn.ibizlab.odoo.client.odoo_asset.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iasset_asset;
import cn.ibizlab.odoo.client.odoo_asset.config.odoo_assetClientProperties;
import cn.ibizlab.odoo.core.client.service.Iasset_assetClientService;
import cn.ibizlab.odoo.client.odoo_asset.model.asset_assetImpl;
import cn.ibizlab.odoo.client.odoo_asset.feign.asset_assetFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[asset_asset] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class asset_assetClientServiceImpl implements Iasset_assetClientService {

    asset_assetFeignClient asset_assetFeignClient;

    @Autowired
    public asset_assetClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_assetClientProperties odoo_assetClientProperties) {
        if (odoo_assetClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.asset_assetFeignClient = nameBuilder.target(asset_assetFeignClient.class,"http://"+odoo_assetClientProperties.getServiceId()+"/") ;
		}else if (odoo_assetClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.asset_assetFeignClient = nameBuilder.target(asset_assetFeignClient.class,odoo_assetClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iasset_asset createModel() {
		return new asset_assetImpl();
	}


    public void get(Iasset_asset asset_asset){
        Iasset_asset clientModel = asset_assetFeignClient.get(asset_asset.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), asset_asset.getClass(), false);
        copier.copy(clientModel, asset_asset, null);
    }


    public void create(Iasset_asset asset_asset){
        Iasset_asset clientModel = asset_assetFeignClient.create((asset_assetImpl)asset_asset) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), asset_asset.getClass(), false);
        copier.copy(clientModel, asset_asset, null);
    }


    public void update(Iasset_asset asset_asset){
        Iasset_asset clientModel = asset_assetFeignClient.update(asset_asset.getId(),(asset_assetImpl)asset_asset) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), asset_asset.getClass(), false);
        copier.copy(clientModel, asset_asset, null);
    }


    public void createBatch(List<Iasset_asset> asset_assets){
        if(asset_assets!=null){
            List<asset_assetImpl> list = new ArrayList<asset_assetImpl>();
            for(Iasset_asset iasset_asset :asset_assets){
                list.add((asset_assetImpl)iasset_asset) ;
            }
            asset_assetFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Iasset_asset> asset_assets){
        if(asset_assets!=null){
            List<asset_assetImpl> list = new ArrayList<asset_assetImpl>();
            for(Iasset_asset iasset_asset :asset_assets){
                list.add((asset_assetImpl)iasset_asset) ;
            }
            asset_assetFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Iasset_asset asset_asset){
        asset_assetFeignClient.remove(asset_asset.getId()) ;
    }


    public void removeBatch(List<Iasset_asset> asset_assets){
        if(asset_assets!=null){
            List<asset_assetImpl> list = new ArrayList<asset_assetImpl>();
            for(Iasset_asset iasset_asset :asset_assets){
                list.add((asset_assetImpl)iasset_asset) ;
            }
            asset_assetFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iasset_asset> fetchDefault(SearchContext context){
        Page<asset_assetImpl> page = this.asset_assetFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Iasset_asset> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iasset_asset asset_asset){
        Iasset_asset clientModel = asset_assetFeignClient.getDraft(asset_asset.getId(),(asset_assetImpl)asset_asset) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), asset_asset.getClass(), false);
        copier.copy(clientModel, asset_asset, null);
    }



}

