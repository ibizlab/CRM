package cn.ibizlab.odoo.client.odoo_asset.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iasset_asset;
import cn.ibizlab.odoo.client.odoo_asset.model.asset_assetImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[asset_asset] 服务对象接口
 */
public interface asset_assetFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_asset/asset_assets/{id}")
    public asset_assetImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_asset/asset_assets")
    public asset_assetImpl create(@RequestBody asset_assetImpl asset_asset);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_asset/asset_assets/{id}")
    public asset_assetImpl update(@PathVariable("id") Integer id,@RequestBody asset_assetImpl asset_asset);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_asset/asset_assets/createbatch")
    public asset_assetImpl createBatch(@RequestBody List<asset_assetImpl> asset_assets);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_asset/asset_assets/updatebatch")
    public asset_assetImpl updateBatch(@RequestBody List<asset_assetImpl> asset_assets);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_asset/asset_assets/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_asset/asset_assets/removebatch")
    public asset_assetImpl removeBatch(@RequestBody List<asset_assetImpl> asset_assets);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_asset/asset_assets/fetchdefault")
    public Page<asset_assetImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_asset/asset_assets/select")
    public Page<asset_assetImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_asset/asset_assets/{id}/getdraft")
    public asset_assetImpl getDraft(@PathVariable("id") Integer id,@RequestBody asset_assetImpl asset_asset);



}
