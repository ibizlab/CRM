package cn.ibizlab.odoo.client.odoo_lunch.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ilunch_order_line_lucky;
import cn.ibizlab.odoo.client.odoo_lunch.config.odoo_lunchClientProperties;
import cn.ibizlab.odoo.core.client.service.Ilunch_order_line_luckyClientService;
import cn.ibizlab.odoo.client.odoo_lunch.model.lunch_order_line_luckyImpl;
import cn.ibizlab.odoo.client.odoo_lunch.feign.lunch_order_line_luckyFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[lunch_order_line_lucky] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class lunch_order_line_luckyClientServiceImpl implements Ilunch_order_line_luckyClientService {

    lunch_order_line_luckyFeignClient lunch_order_line_luckyFeignClient;

    @Autowired
    public lunch_order_line_luckyClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_lunchClientProperties odoo_lunchClientProperties) {
        if (odoo_lunchClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.lunch_order_line_luckyFeignClient = nameBuilder.target(lunch_order_line_luckyFeignClient.class,"http://"+odoo_lunchClientProperties.getServiceId()+"/") ;
		}else if (odoo_lunchClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.lunch_order_line_luckyFeignClient = nameBuilder.target(lunch_order_line_luckyFeignClient.class,odoo_lunchClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ilunch_order_line_lucky createModel() {
		return new lunch_order_line_luckyImpl();
	}


    public Page<Ilunch_order_line_lucky> fetchDefault(SearchContext context){
        Page<lunch_order_line_luckyImpl> page = this.lunch_order_line_luckyFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Ilunch_order_line_lucky lunch_order_line_lucky){
        Ilunch_order_line_lucky clientModel = lunch_order_line_luckyFeignClient.get(lunch_order_line_lucky.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), lunch_order_line_lucky.getClass(), false);
        copier.copy(clientModel, lunch_order_line_lucky, null);
    }


    public void createBatch(List<Ilunch_order_line_lucky> lunch_order_line_luckies){
        if(lunch_order_line_luckies!=null){
            List<lunch_order_line_luckyImpl> list = new ArrayList<lunch_order_line_luckyImpl>();
            for(Ilunch_order_line_lucky ilunch_order_line_lucky :lunch_order_line_luckies){
                list.add((lunch_order_line_luckyImpl)ilunch_order_line_lucky) ;
            }
            lunch_order_line_luckyFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Ilunch_order_line_lucky> lunch_order_line_luckies){
        if(lunch_order_line_luckies!=null){
            List<lunch_order_line_luckyImpl> list = new ArrayList<lunch_order_line_luckyImpl>();
            for(Ilunch_order_line_lucky ilunch_order_line_lucky :lunch_order_line_luckies){
                list.add((lunch_order_line_luckyImpl)ilunch_order_line_lucky) ;
            }
            lunch_order_line_luckyFeignClient.updateBatch(list) ;
        }
    }


    public void create(Ilunch_order_line_lucky lunch_order_line_lucky){
        Ilunch_order_line_lucky clientModel = lunch_order_line_luckyFeignClient.create((lunch_order_line_luckyImpl)lunch_order_line_lucky) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), lunch_order_line_lucky.getClass(), false);
        copier.copy(clientModel, lunch_order_line_lucky, null);
    }


    public void update(Ilunch_order_line_lucky lunch_order_line_lucky){
        Ilunch_order_line_lucky clientModel = lunch_order_line_luckyFeignClient.update(lunch_order_line_lucky.getId(),(lunch_order_line_luckyImpl)lunch_order_line_lucky) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), lunch_order_line_lucky.getClass(), false);
        copier.copy(clientModel, lunch_order_line_lucky, null);
    }


    public void remove(Ilunch_order_line_lucky lunch_order_line_lucky){
        lunch_order_line_luckyFeignClient.remove(lunch_order_line_lucky.getId()) ;
    }


    public void removeBatch(List<Ilunch_order_line_lucky> lunch_order_line_luckies){
        if(lunch_order_line_luckies!=null){
            List<lunch_order_line_luckyImpl> list = new ArrayList<lunch_order_line_luckyImpl>();
            for(Ilunch_order_line_lucky ilunch_order_line_lucky :lunch_order_line_luckies){
                list.add((lunch_order_line_luckyImpl)ilunch_order_line_lucky) ;
            }
            lunch_order_line_luckyFeignClient.removeBatch(list) ;
        }
    }


    public Page<Ilunch_order_line_lucky> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ilunch_order_line_lucky lunch_order_line_lucky){
        Ilunch_order_line_lucky clientModel = lunch_order_line_luckyFeignClient.getDraft(lunch_order_line_lucky.getId(),(lunch_order_line_luckyImpl)lunch_order_line_lucky) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), lunch_order_line_lucky.getClass(), false);
        copier.copy(clientModel, lunch_order_line_lucky, null);
    }



}

