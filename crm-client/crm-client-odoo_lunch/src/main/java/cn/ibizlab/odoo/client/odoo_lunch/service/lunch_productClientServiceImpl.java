package cn.ibizlab.odoo.client.odoo_lunch.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ilunch_product;
import cn.ibizlab.odoo.client.odoo_lunch.config.odoo_lunchClientProperties;
import cn.ibizlab.odoo.core.client.service.Ilunch_productClientService;
import cn.ibizlab.odoo.client.odoo_lunch.model.lunch_productImpl;
import cn.ibizlab.odoo.client.odoo_lunch.feign.lunch_productFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[lunch_product] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class lunch_productClientServiceImpl implements Ilunch_productClientService {

    lunch_productFeignClient lunch_productFeignClient;

    @Autowired
    public lunch_productClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_lunchClientProperties odoo_lunchClientProperties) {
        if (odoo_lunchClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.lunch_productFeignClient = nameBuilder.target(lunch_productFeignClient.class,"http://"+odoo_lunchClientProperties.getServiceId()+"/") ;
		}else if (odoo_lunchClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.lunch_productFeignClient = nameBuilder.target(lunch_productFeignClient.class,odoo_lunchClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ilunch_product createModel() {
		return new lunch_productImpl();
	}


    public void create(Ilunch_product lunch_product){
        Ilunch_product clientModel = lunch_productFeignClient.create((lunch_productImpl)lunch_product) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), lunch_product.getClass(), false);
        copier.copy(clientModel, lunch_product, null);
    }


    public void get(Ilunch_product lunch_product){
        Ilunch_product clientModel = lunch_productFeignClient.get(lunch_product.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), lunch_product.getClass(), false);
        copier.copy(clientModel, lunch_product, null);
    }


    public Page<Ilunch_product> fetchDefault(SearchContext context){
        Page<lunch_productImpl> page = this.lunch_productFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Ilunch_product> lunch_products){
        if(lunch_products!=null){
            List<lunch_productImpl> list = new ArrayList<lunch_productImpl>();
            for(Ilunch_product ilunch_product :lunch_products){
                list.add((lunch_productImpl)ilunch_product) ;
            }
            lunch_productFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Ilunch_product> lunch_products){
        if(lunch_products!=null){
            List<lunch_productImpl> list = new ArrayList<lunch_productImpl>();
            for(Ilunch_product ilunch_product :lunch_products){
                list.add((lunch_productImpl)ilunch_product) ;
            }
            lunch_productFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Ilunch_product> lunch_products){
        if(lunch_products!=null){
            List<lunch_productImpl> list = new ArrayList<lunch_productImpl>();
            for(Ilunch_product ilunch_product :lunch_products){
                list.add((lunch_productImpl)ilunch_product) ;
            }
            lunch_productFeignClient.removeBatch(list) ;
        }
    }


    public void update(Ilunch_product lunch_product){
        Ilunch_product clientModel = lunch_productFeignClient.update(lunch_product.getId(),(lunch_productImpl)lunch_product) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), lunch_product.getClass(), false);
        copier.copy(clientModel, lunch_product, null);
    }


    public void remove(Ilunch_product lunch_product){
        lunch_productFeignClient.remove(lunch_product.getId()) ;
    }


    public Page<Ilunch_product> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ilunch_product lunch_product){
        Ilunch_product clientModel = lunch_productFeignClient.getDraft(lunch_product.getId(),(lunch_productImpl)lunch_product) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), lunch_product.getClass(), false);
        copier.copy(clientModel, lunch_product, null);
    }



}

