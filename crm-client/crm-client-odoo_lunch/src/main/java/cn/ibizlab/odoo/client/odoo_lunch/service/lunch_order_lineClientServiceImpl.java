package cn.ibizlab.odoo.client.odoo_lunch.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ilunch_order_line;
import cn.ibizlab.odoo.client.odoo_lunch.config.odoo_lunchClientProperties;
import cn.ibizlab.odoo.core.client.service.Ilunch_order_lineClientService;
import cn.ibizlab.odoo.client.odoo_lunch.model.lunch_order_lineImpl;
import cn.ibizlab.odoo.client.odoo_lunch.feign.lunch_order_lineFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[lunch_order_line] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class lunch_order_lineClientServiceImpl implements Ilunch_order_lineClientService {

    lunch_order_lineFeignClient lunch_order_lineFeignClient;

    @Autowired
    public lunch_order_lineClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_lunchClientProperties odoo_lunchClientProperties) {
        if (odoo_lunchClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.lunch_order_lineFeignClient = nameBuilder.target(lunch_order_lineFeignClient.class,"http://"+odoo_lunchClientProperties.getServiceId()+"/") ;
		}else if (odoo_lunchClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.lunch_order_lineFeignClient = nameBuilder.target(lunch_order_lineFeignClient.class,odoo_lunchClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ilunch_order_line createModel() {
		return new lunch_order_lineImpl();
	}


    public void get(Ilunch_order_line lunch_order_line){
        Ilunch_order_line clientModel = lunch_order_lineFeignClient.get(lunch_order_line.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), lunch_order_line.getClass(), false);
        copier.copy(clientModel, lunch_order_line, null);
    }


    public void createBatch(List<Ilunch_order_line> lunch_order_lines){
        if(lunch_order_lines!=null){
            List<lunch_order_lineImpl> list = new ArrayList<lunch_order_lineImpl>();
            for(Ilunch_order_line ilunch_order_line :lunch_order_lines){
                list.add((lunch_order_lineImpl)ilunch_order_line) ;
            }
            lunch_order_lineFeignClient.createBatch(list) ;
        }
    }


    public void remove(Ilunch_order_line lunch_order_line){
        lunch_order_lineFeignClient.remove(lunch_order_line.getId()) ;
    }


    public void removeBatch(List<Ilunch_order_line> lunch_order_lines){
        if(lunch_order_lines!=null){
            List<lunch_order_lineImpl> list = new ArrayList<lunch_order_lineImpl>();
            for(Ilunch_order_line ilunch_order_line :lunch_order_lines){
                list.add((lunch_order_lineImpl)ilunch_order_line) ;
            }
            lunch_order_lineFeignClient.removeBatch(list) ;
        }
    }


    public void update(Ilunch_order_line lunch_order_line){
        Ilunch_order_line clientModel = lunch_order_lineFeignClient.update(lunch_order_line.getId(),(lunch_order_lineImpl)lunch_order_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), lunch_order_line.getClass(), false);
        copier.copy(clientModel, lunch_order_line, null);
    }


    public Page<Ilunch_order_line> fetchDefault(SearchContext context){
        Page<lunch_order_lineImpl> page = this.lunch_order_lineFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Ilunch_order_line lunch_order_line){
        Ilunch_order_line clientModel = lunch_order_lineFeignClient.create((lunch_order_lineImpl)lunch_order_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), lunch_order_line.getClass(), false);
        copier.copy(clientModel, lunch_order_line, null);
    }


    public void updateBatch(List<Ilunch_order_line> lunch_order_lines){
        if(lunch_order_lines!=null){
            List<lunch_order_lineImpl> list = new ArrayList<lunch_order_lineImpl>();
            for(Ilunch_order_line ilunch_order_line :lunch_order_lines){
                list.add((lunch_order_lineImpl)ilunch_order_line) ;
            }
            lunch_order_lineFeignClient.updateBatch(list) ;
        }
    }


    public Page<Ilunch_order_line> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ilunch_order_line lunch_order_line){
        Ilunch_order_line clientModel = lunch_order_lineFeignClient.getDraft(lunch_order_line.getId(),(lunch_order_lineImpl)lunch_order_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), lunch_order_line.getClass(), false);
        copier.copy(clientModel, lunch_order_line, null);
    }



}

