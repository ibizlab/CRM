package cn.ibizlab.odoo.client.odoo_digest.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Idigest_digest;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[digest_digest] 对象
 */
public class digest_digestImpl implements Idigest_digest,Serializable{

    /**
     * 可用字段
     */
    public String available_fields;

    @JsonIgnore
    public boolean available_fieldsDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 已订阅
     */
    public String is_subscribed;

    @JsonIgnore
    public boolean is_subscribedDirtyFlag;
    
    /**
     * 收入
     */
    public String kpi_account_total_revenue;

    @JsonIgnore
    public boolean kpi_account_total_revenueDirtyFlag;
    
    /**
     * KPI账户总收入
     */
    public Double kpi_account_total_revenue_value;

    @JsonIgnore
    public boolean kpi_account_total_revenue_valueDirtyFlag;
    
    /**
     * 所有销售
     */
    public String kpi_all_sale_total;

    @JsonIgnore
    public boolean kpi_all_sale_totalDirtyFlag;
    
    /**
     * 所有销售总价值KPI
     */
    public Double kpi_all_sale_total_value;

    @JsonIgnore
    public boolean kpi_all_sale_total_valueDirtyFlag;
    
    /**
     * 新的线索/商机
     */
    public String kpi_crm_lead_created;

    @JsonIgnore
    public boolean kpi_crm_lead_createdDirtyFlag;
    
    /**
     * KPI CRM预期收益
     */
    public Integer kpi_crm_lead_created_value;

    @JsonIgnore
    public boolean kpi_crm_lead_created_valueDirtyFlag;
    
    /**
     * 已签单商机
     */
    public String kpi_crm_opportunities_won;

    @JsonIgnore
    public boolean kpi_crm_opportunities_wonDirtyFlag;
    
    /**
     * KPI CRM签单金额
     */
    public Integer kpi_crm_opportunities_won_value;

    @JsonIgnore
    public boolean kpi_crm_opportunities_won_valueDirtyFlag;
    
    /**
     * 员工
     */
    public String kpi_hr_recruitment_new_colleagues;

    @JsonIgnore
    public boolean kpi_hr_recruitment_new_colleaguesDirtyFlag;
    
    /**
     * 人力资源新聘员工KPI指标
     */
    public Integer kpi_hr_recruitment_new_colleagues_value;

    @JsonIgnore
    public boolean kpi_hr_recruitment_new_colleagues_valueDirtyFlag;
    
    /**
     * 消息
     */
    public String kpi_mail_message_total;

    @JsonIgnore
    public boolean kpi_mail_message_totalDirtyFlag;
    
    /**
     * Kpi 邮件信息总计
     */
    public Integer kpi_mail_message_total_value;

    @JsonIgnore
    public boolean kpi_mail_message_total_valueDirtyFlag;
    
    /**
     * POS 销售
     */
    public String kpi_pos_total;

    @JsonIgnore
    public boolean kpi_pos_totalDirtyFlag;
    
    /**
     * 终端销售总额的关键绩效指标
     */
    public Double kpi_pos_total_value;

    @JsonIgnore
    public boolean kpi_pos_total_valueDirtyFlag;
    
    /**
     * 开放任务
     */
    public String kpi_project_task_opened;

    @JsonIgnore
    public boolean kpi_project_task_openedDirtyFlag;
    
    /**
     * 项目任务开放价值的关键绩效指标
     */
    public Integer kpi_project_task_opened_value;

    @JsonIgnore
    public boolean kpi_project_task_opened_valueDirtyFlag;
    
    /**
     * 已连接用户
     */
    public String kpi_res_users_connected;

    @JsonIgnore
    public boolean kpi_res_users_connectedDirtyFlag;
    
    /**
     * Kpi Res 用户连接值
     */
    public Integer kpi_res_users_connected_value;

    @JsonIgnore
    public boolean kpi_res_users_connected_valueDirtyFlag;
    
    /**
     * 电商销售
     */
    public String kpi_website_sale_total;

    @JsonIgnore
    public boolean kpi_website_sale_totalDirtyFlag;
    
    /**
     * Kpi网站销售总价值
     */
    public Double kpi_website_sale_total_value;

    @JsonIgnore
    public boolean kpi_website_sale_total_valueDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 下一发送日期
     */
    public Timestamp next_run_date;

    @JsonIgnore
    public boolean next_run_dateDirtyFlag;
    
    /**
     * 周期
     */
    public String periodicity;

    @JsonIgnore
    public boolean periodicityDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * EMail模板
     */
    public Integer template_id;

    @JsonIgnore
    public boolean template_idDirtyFlag;
    
    /**
     * EMail模板
     */
    public String template_id_text;

    @JsonIgnore
    public boolean template_id_textDirtyFlag;
    
    /**
     * 收件人
     */
    public String user_ids;

    @JsonIgnore
    public boolean user_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [可用字段]
     */
    @JsonProperty("available_fields")
    public String getAvailable_fields(){
        return this.available_fields ;
    }

    /**
     * 设置 [可用字段]
     */
    @JsonProperty("available_fields")
    public void setAvailable_fields(String  available_fields){
        this.available_fields = available_fields ;
        this.available_fieldsDirtyFlag = true ;
    }

     /**
     * 获取 [可用字段]脏标记
     */
    @JsonIgnore
    public boolean getAvailable_fieldsDirtyFlag(){
        return this.available_fieldsDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [已订阅]
     */
    @JsonProperty("is_subscribed")
    public String getIs_subscribed(){
        return this.is_subscribed ;
    }

    /**
     * 设置 [已订阅]
     */
    @JsonProperty("is_subscribed")
    public void setIs_subscribed(String  is_subscribed){
        this.is_subscribed = is_subscribed ;
        this.is_subscribedDirtyFlag = true ;
    }

     /**
     * 获取 [已订阅]脏标记
     */
    @JsonIgnore
    public boolean getIs_subscribedDirtyFlag(){
        return this.is_subscribedDirtyFlag ;
    }   

    /**
     * 获取 [收入]
     */
    @JsonProperty("kpi_account_total_revenue")
    public String getKpi_account_total_revenue(){
        return this.kpi_account_total_revenue ;
    }

    /**
     * 设置 [收入]
     */
    @JsonProperty("kpi_account_total_revenue")
    public void setKpi_account_total_revenue(String  kpi_account_total_revenue){
        this.kpi_account_total_revenue = kpi_account_total_revenue ;
        this.kpi_account_total_revenueDirtyFlag = true ;
    }

     /**
     * 获取 [收入]脏标记
     */
    @JsonIgnore
    public boolean getKpi_account_total_revenueDirtyFlag(){
        return this.kpi_account_total_revenueDirtyFlag ;
    }   

    /**
     * 获取 [KPI账户总收入]
     */
    @JsonProperty("kpi_account_total_revenue_value")
    public Double getKpi_account_total_revenue_value(){
        return this.kpi_account_total_revenue_value ;
    }

    /**
     * 设置 [KPI账户总收入]
     */
    @JsonProperty("kpi_account_total_revenue_value")
    public void setKpi_account_total_revenue_value(Double  kpi_account_total_revenue_value){
        this.kpi_account_total_revenue_value = kpi_account_total_revenue_value ;
        this.kpi_account_total_revenue_valueDirtyFlag = true ;
    }

     /**
     * 获取 [KPI账户总收入]脏标记
     */
    @JsonIgnore
    public boolean getKpi_account_total_revenue_valueDirtyFlag(){
        return this.kpi_account_total_revenue_valueDirtyFlag ;
    }   

    /**
     * 获取 [所有销售]
     */
    @JsonProperty("kpi_all_sale_total")
    public String getKpi_all_sale_total(){
        return this.kpi_all_sale_total ;
    }

    /**
     * 设置 [所有销售]
     */
    @JsonProperty("kpi_all_sale_total")
    public void setKpi_all_sale_total(String  kpi_all_sale_total){
        this.kpi_all_sale_total = kpi_all_sale_total ;
        this.kpi_all_sale_totalDirtyFlag = true ;
    }

     /**
     * 获取 [所有销售]脏标记
     */
    @JsonIgnore
    public boolean getKpi_all_sale_totalDirtyFlag(){
        return this.kpi_all_sale_totalDirtyFlag ;
    }   

    /**
     * 获取 [所有销售总价值KPI]
     */
    @JsonProperty("kpi_all_sale_total_value")
    public Double getKpi_all_sale_total_value(){
        return this.kpi_all_sale_total_value ;
    }

    /**
     * 设置 [所有销售总价值KPI]
     */
    @JsonProperty("kpi_all_sale_total_value")
    public void setKpi_all_sale_total_value(Double  kpi_all_sale_total_value){
        this.kpi_all_sale_total_value = kpi_all_sale_total_value ;
        this.kpi_all_sale_total_valueDirtyFlag = true ;
    }

     /**
     * 获取 [所有销售总价值KPI]脏标记
     */
    @JsonIgnore
    public boolean getKpi_all_sale_total_valueDirtyFlag(){
        return this.kpi_all_sale_total_valueDirtyFlag ;
    }   

    /**
     * 获取 [新的线索/商机]
     */
    @JsonProperty("kpi_crm_lead_created")
    public String getKpi_crm_lead_created(){
        return this.kpi_crm_lead_created ;
    }

    /**
     * 设置 [新的线索/商机]
     */
    @JsonProperty("kpi_crm_lead_created")
    public void setKpi_crm_lead_created(String  kpi_crm_lead_created){
        this.kpi_crm_lead_created = kpi_crm_lead_created ;
        this.kpi_crm_lead_createdDirtyFlag = true ;
    }

     /**
     * 获取 [新的线索/商机]脏标记
     */
    @JsonIgnore
    public boolean getKpi_crm_lead_createdDirtyFlag(){
        return this.kpi_crm_lead_createdDirtyFlag ;
    }   

    /**
     * 获取 [KPI CRM预期收益]
     */
    @JsonProperty("kpi_crm_lead_created_value")
    public Integer getKpi_crm_lead_created_value(){
        return this.kpi_crm_lead_created_value ;
    }

    /**
     * 设置 [KPI CRM预期收益]
     */
    @JsonProperty("kpi_crm_lead_created_value")
    public void setKpi_crm_lead_created_value(Integer  kpi_crm_lead_created_value){
        this.kpi_crm_lead_created_value = kpi_crm_lead_created_value ;
        this.kpi_crm_lead_created_valueDirtyFlag = true ;
    }

     /**
     * 获取 [KPI CRM预期收益]脏标记
     */
    @JsonIgnore
    public boolean getKpi_crm_lead_created_valueDirtyFlag(){
        return this.kpi_crm_lead_created_valueDirtyFlag ;
    }   

    /**
     * 获取 [已签单商机]
     */
    @JsonProperty("kpi_crm_opportunities_won")
    public String getKpi_crm_opportunities_won(){
        return this.kpi_crm_opportunities_won ;
    }

    /**
     * 设置 [已签单商机]
     */
    @JsonProperty("kpi_crm_opportunities_won")
    public void setKpi_crm_opportunities_won(String  kpi_crm_opportunities_won){
        this.kpi_crm_opportunities_won = kpi_crm_opportunities_won ;
        this.kpi_crm_opportunities_wonDirtyFlag = true ;
    }

     /**
     * 获取 [已签单商机]脏标记
     */
    @JsonIgnore
    public boolean getKpi_crm_opportunities_wonDirtyFlag(){
        return this.kpi_crm_opportunities_wonDirtyFlag ;
    }   

    /**
     * 获取 [KPI CRM签单金额]
     */
    @JsonProperty("kpi_crm_opportunities_won_value")
    public Integer getKpi_crm_opportunities_won_value(){
        return this.kpi_crm_opportunities_won_value ;
    }

    /**
     * 设置 [KPI CRM签单金额]
     */
    @JsonProperty("kpi_crm_opportunities_won_value")
    public void setKpi_crm_opportunities_won_value(Integer  kpi_crm_opportunities_won_value){
        this.kpi_crm_opportunities_won_value = kpi_crm_opportunities_won_value ;
        this.kpi_crm_opportunities_won_valueDirtyFlag = true ;
    }

     /**
     * 获取 [KPI CRM签单金额]脏标记
     */
    @JsonIgnore
    public boolean getKpi_crm_opportunities_won_valueDirtyFlag(){
        return this.kpi_crm_opportunities_won_valueDirtyFlag ;
    }   

    /**
     * 获取 [员工]
     */
    @JsonProperty("kpi_hr_recruitment_new_colleagues")
    public String getKpi_hr_recruitment_new_colleagues(){
        return this.kpi_hr_recruitment_new_colleagues ;
    }

    /**
     * 设置 [员工]
     */
    @JsonProperty("kpi_hr_recruitment_new_colleagues")
    public void setKpi_hr_recruitment_new_colleagues(String  kpi_hr_recruitment_new_colleagues){
        this.kpi_hr_recruitment_new_colleagues = kpi_hr_recruitment_new_colleagues ;
        this.kpi_hr_recruitment_new_colleaguesDirtyFlag = true ;
    }

     /**
     * 获取 [员工]脏标记
     */
    @JsonIgnore
    public boolean getKpi_hr_recruitment_new_colleaguesDirtyFlag(){
        return this.kpi_hr_recruitment_new_colleaguesDirtyFlag ;
    }   

    /**
     * 获取 [人力资源新聘员工KPI指标]
     */
    @JsonProperty("kpi_hr_recruitment_new_colleagues_value")
    public Integer getKpi_hr_recruitment_new_colleagues_value(){
        return this.kpi_hr_recruitment_new_colleagues_value ;
    }

    /**
     * 设置 [人力资源新聘员工KPI指标]
     */
    @JsonProperty("kpi_hr_recruitment_new_colleagues_value")
    public void setKpi_hr_recruitment_new_colleagues_value(Integer  kpi_hr_recruitment_new_colleagues_value){
        this.kpi_hr_recruitment_new_colleagues_value = kpi_hr_recruitment_new_colleagues_value ;
        this.kpi_hr_recruitment_new_colleagues_valueDirtyFlag = true ;
    }

     /**
     * 获取 [人力资源新聘员工KPI指标]脏标记
     */
    @JsonIgnore
    public boolean getKpi_hr_recruitment_new_colleagues_valueDirtyFlag(){
        return this.kpi_hr_recruitment_new_colleagues_valueDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("kpi_mail_message_total")
    public String getKpi_mail_message_total(){
        return this.kpi_mail_message_total ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("kpi_mail_message_total")
    public void setKpi_mail_message_total(String  kpi_mail_message_total){
        this.kpi_mail_message_total = kpi_mail_message_total ;
        this.kpi_mail_message_totalDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getKpi_mail_message_totalDirtyFlag(){
        return this.kpi_mail_message_totalDirtyFlag ;
    }   

    /**
     * 获取 [Kpi 邮件信息总计]
     */
    @JsonProperty("kpi_mail_message_total_value")
    public Integer getKpi_mail_message_total_value(){
        return this.kpi_mail_message_total_value ;
    }

    /**
     * 设置 [Kpi 邮件信息总计]
     */
    @JsonProperty("kpi_mail_message_total_value")
    public void setKpi_mail_message_total_value(Integer  kpi_mail_message_total_value){
        this.kpi_mail_message_total_value = kpi_mail_message_total_value ;
        this.kpi_mail_message_total_valueDirtyFlag = true ;
    }

     /**
     * 获取 [Kpi 邮件信息总计]脏标记
     */
    @JsonIgnore
    public boolean getKpi_mail_message_total_valueDirtyFlag(){
        return this.kpi_mail_message_total_valueDirtyFlag ;
    }   

    /**
     * 获取 [POS 销售]
     */
    @JsonProperty("kpi_pos_total")
    public String getKpi_pos_total(){
        return this.kpi_pos_total ;
    }

    /**
     * 设置 [POS 销售]
     */
    @JsonProperty("kpi_pos_total")
    public void setKpi_pos_total(String  kpi_pos_total){
        this.kpi_pos_total = kpi_pos_total ;
        this.kpi_pos_totalDirtyFlag = true ;
    }

     /**
     * 获取 [POS 销售]脏标记
     */
    @JsonIgnore
    public boolean getKpi_pos_totalDirtyFlag(){
        return this.kpi_pos_totalDirtyFlag ;
    }   

    /**
     * 获取 [终端销售总额的关键绩效指标]
     */
    @JsonProperty("kpi_pos_total_value")
    public Double getKpi_pos_total_value(){
        return this.kpi_pos_total_value ;
    }

    /**
     * 设置 [终端销售总额的关键绩效指标]
     */
    @JsonProperty("kpi_pos_total_value")
    public void setKpi_pos_total_value(Double  kpi_pos_total_value){
        this.kpi_pos_total_value = kpi_pos_total_value ;
        this.kpi_pos_total_valueDirtyFlag = true ;
    }

     /**
     * 获取 [终端销售总额的关键绩效指标]脏标记
     */
    @JsonIgnore
    public boolean getKpi_pos_total_valueDirtyFlag(){
        return this.kpi_pos_total_valueDirtyFlag ;
    }   

    /**
     * 获取 [开放任务]
     */
    @JsonProperty("kpi_project_task_opened")
    public String getKpi_project_task_opened(){
        return this.kpi_project_task_opened ;
    }

    /**
     * 设置 [开放任务]
     */
    @JsonProperty("kpi_project_task_opened")
    public void setKpi_project_task_opened(String  kpi_project_task_opened){
        this.kpi_project_task_opened = kpi_project_task_opened ;
        this.kpi_project_task_openedDirtyFlag = true ;
    }

     /**
     * 获取 [开放任务]脏标记
     */
    @JsonIgnore
    public boolean getKpi_project_task_openedDirtyFlag(){
        return this.kpi_project_task_openedDirtyFlag ;
    }   

    /**
     * 获取 [项目任务开放价值的关键绩效指标]
     */
    @JsonProperty("kpi_project_task_opened_value")
    public Integer getKpi_project_task_opened_value(){
        return this.kpi_project_task_opened_value ;
    }

    /**
     * 设置 [项目任务开放价值的关键绩效指标]
     */
    @JsonProperty("kpi_project_task_opened_value")
    public void setKpi_project_task_opened_value(Integer  kpi_project_task_opened_value){
        this.kpi_project_task_opened_value = kpi_project_task_opened_value ;
        this.kpi_project_task_opened_valueDirtyFlag = true ;
    }

     /**
     * 获取 [项目任务开放价值的关键绩效指标]脏标记
     */
    @JsonIgnore
    public boolean getKpi_project_task_opened_valueDirtyFlag(){
        return this.kpi_project_task_opened_valueDirtyFlag ;
    }   

    /**
     * 获取 [已连接用户]
     */
    @JsonProperty("kpi_res_users_connected")
    public String getKpi_res_users_connected(){
        return this.kpi_res_users_connected ;
    }

    /**
     * 设置 [已连接用户]
     */
    @JsonProperty("kpi_res_users_connected")
    public void setKpi_res_users_connected(String  kpi_res_users_connected){
        this.kpi_res_users_connected = kpi_res_users_connected ;
        this.kpi_res_users_connectedDirtyFlag = true ;
    }

     /**
     * 获取 [已连接用户]脏标记
     */
    @JsonIgnore
    public boolean getKpi_res_users_connectedDirtyFlag(){
        return this.kpi_res_users_connectedDirtyFlag ;
    }   

    /**
     * 获取 [Kpi Res 用户连接值]
     */
    @JsonProperty("kpi_res_users_connected_value")
    public Integer getKpi_res_users_connected_value(){
        return this.kpi_res_users_connected_value ;
    }

    /**
     * 设置 [Kpi Res 用户连接值]
     */
    @JsonProperty("kpi_res_users_connected_value")
    public void setKpi_res_users_connected_value(Integer  kpi_res_users_connected_value){
        this.kpi_res_users_connected_value = kpi_res_users_connected_value ;
        this.kpi_res_users_connected_valueDirtyFlag = true ;
    }

     /**
     * 获取 [Kpi Res 用户连接值]脏标记
     */
    @JsonIgnore
    public boolean getKpi_res_users_connected_valueDirtyFlag(){
        return this.kpi_res_users_connected_valueDirtyFlag ;
    }   

    /**
     * 获取 [电商销售]
     */
    @JsonProperty("kpi_website_sale_total")
    public String getKpi_website_sale_total(){
        return this.kpi_website_sale_total ;
    }

    /**
     * 设置 [电商销售]
     */
    @JsonProperty("kpi_website_sale_total")
    public void setKpi_website_sale_total(String  kpi_website_sale_total){
        this.kpi_website_sale_total = kpi_website_sale_total ;
        this.kpi_website_sale_totalDirtyFlag = true ;
    }

     /**
     * 获取 [电商销售]脏标记
     */
    @JsonIgnore
    public boolean getKpi_website_sale_totalDirtyFlag(){
        return this.kpi_website_sale_totalDirtyFlag ;
    }   

    /**
     * 获取 [Kpi网站销售总价值]
     */
    @JsonProperty("kpi_website_sale_total_value")
    public Double getKpi_website_sale_total_value(){
        return this.kpi_website_sale_total_value ;
    }

    /**
     * 设置 [Kpi网站销售总价值]
     */
    @JsonProperty("kpi_website_sale_total_value")
    public void setKpi_website_sale_total_value(Double  kpi_website_sale_total_value){
        this.kpi_website_sale_total_value = kpi_website_sale_total_value ;
        this.kpi_website_sale_total_valueDirtyFlag = true ;
    }

     /**
     * 获取 [Kpi网站销售总价值]脏标记
     */
    @JsonIgnore
    public boolean getKpi_website_sale_total_valueDirtyFlag(){
        return this.kpi_website_sale_total_valueDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [下一发送日期]
     */
    @JsonProperty("next_run_date")
    public Timestamp getNext_run_date(){
        return this.next_run_date ;
    }

    /**
     * 设置 [下一发送日期]
     */
    @JsonProperty("next_run_date")
    public void setNext_run_date(Timestamp  next_run_date){
        this.next_run_date = next_run_date ;
        this.next_run_dateDirtyFlag = true ;
    }

     /**
     * 获取 [下一发送日期]脏标记
     */
    @JsonIgnore
    public boolean getNext_run_dateDirtyFlag(){
        return this.next_run_dateDirtyFlag ;
    }   

    /**
     * 获取 [周期]
     */
    @JsonProperty("periodicity")
    public String getPeriodicity(){
        return this.periodicity ;
    }

    /**
     * 设置 [周期]
     */
    @JsonProperty("periodicity")
    public void setPeriodicity(String  periodicity){
        this.periodicity = periodicity ;
        this.periodicityDirtyFlag = true ;
    }

     /**
     * 获取 [周期]脏标记
     */
    @JsonIgnore
    public boolean getPeriodicityDirtyFlag(){
        return this.periodicityDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [EMail模板]
     */
    @JsonProperty("template_id")
    public Integer getTemplate_id(){
        return this.template_id ;
    }

    /**
     * 设置 [EMail模板]
     */
    @JsonProperty("template_id")
    public void setTemplate_id(Integer  template_id){
        this.template_id = template_id ;
        this.template_idDirtyFlag = true ;
    }

     /**
     * 获取 [EMail模板]脏标记
     */
    @JsonIgnore
    public boolean getTemplate_idDirtyFlag(){
        return this.template_idDirtyFlag ;
    }   

    /**
     * 获取 [EMail模板]
     */
    @JsonProperty("template_id_text")
    public String getTemplate_id_text(){
        return this.template_id_text ;
    }

    /**
     * 设置 [EMail模板]
     */
    @JsonProperty("template_id_text")
    public void setTemplate_id_text(String  template_id_text){
        this.template_id_text = template_id_text ;
        this.template_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [EMail模板]脏标记
     */
    @JsonIgnore
    public boolean getTemplate_id_textDirtyFlag(){
        return this.template_id_textDirtyFlag ;
    }   

    /**
     * 获取 [收件人]
     */
    @JsonProperty("user_ids")
    public String getUser_ids(){
        return this.user_ids ;
    }

    /**
     * 设置 [收件人]
     */
    @JsonProperty("user_ids")
    public void setUser_ids(String  user_ids){
        this.user_ids = user_ids ;
        this.user_idsDirtyFlag = true ;
    }

     /**
     * 获取 [收件人]脏标记
     */
    @JsonIgnore
    public boolean getUser_idsDirtyFlag(){
        return this.user_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
