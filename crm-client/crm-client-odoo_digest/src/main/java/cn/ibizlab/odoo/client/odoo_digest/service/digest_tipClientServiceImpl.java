package cn.ibizlab.odoo.client.odoo_digest.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Idigest_tip;
import cn.ibizlab.odoo.client.odoo_digest.config.odoo_digestClientProperties;
import cn.ibizlab.odoo.core.client.service.Idigest_tipClientService;
import cn.ibizlab.odoo.client.odoo_digest.model.digest_tipImpl;
import cn.ibizlab.odoo.client.odoo_digest.feign.digest_tipFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[digest_tip] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class digest_tipClientServiceImpl implements Idigest_tipClientService {

    digest_tipFeignClient digest_tipFeignClient;

    @Autowired
    public digest_tipClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_digestClientProperties odoo_digestClientProperties) {
        if (odoo_digestClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.digest_tipFeignClient = nameBuilder.target(digest_tipFeignClient.class,"http://"+odoo_digestClientProperties.getServiceId()+"/") ;
		}else if (odoo_digestClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.digest_tipFeignClient = nameBuilder.target(digest_tipFeignClient.class,odoo_digestClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Idigest_tip createModel() {
		return new digest_tipImpl();
	}


    public void updateBatch(List<Idigest_tip> digest_tips){
        if(digest_tips!=null){
            List<digest_tipImpl> list = new ArrayList<digest_tipImpl>();
            for(Idigest_tip idigest_tip :digest_tips){
                list.add((digest_tipImpl)idigest_tip) ;
            }
            digest_tipFeignClient.updateBatch(list) ;
        }
    }


    public Page<Idigest_tip> fetchDefault(SearchContext context){
        Page<digest_tipImpl> page = this.digest_tipFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Idigest_tip digest_tip){
        Idigest_tip clientModel = digest_tipFeignClient.get(digest_tip.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), digest_tip.getClass(), false);
        copier.copy(clientModel, digest_tip, null);
    }


    public void remove(Idigest_tip digest_tip){
        digest_tipFeignClient.remove(digest_tip.getId()) ;
    }


    public void createBatch(List<Idigest_tip> digest_tips){
        if(digest_tips!=null){
            List<digest_tipImpl> list = new ArrayList<digest_tipImpl>();
            for(Idigest_tip idigest_tip :digest_tips){
                list.add((digest_tipImpl)idigest_tip) ;
            }
            digest_tipFeignClient.createBatch(list) ;
        }
    }


    public void update(Idigest_tip digest_tip){
        Idigest_tip clientModel = digest_tipFeignClient.update(digest_tip.getId(),(digest_tipImpl)digest_tip) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), digest_tip.getClass(), false);
        copier.copy(clientModel, digest_tip, null);
    }


    public void create(Idigest_tip digest_tip){
        Idigest_tip clientModel = digest_tipFeignClient.create((digest_tipImpl)digest_tip) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), digest_tip.getClass(), false);
        copier.copy(clientModel, digest_tip, null);
    }


    public void removeBatch(List<Idigest_tip> digest_tips){
        if(digest_tips!=null){
            List<digest_tipImpl> list = new ArrayList<digest_tipImpl>();
            for(Idigest_tip idigest_tip :digest_tips){
                list.add((digest_tipImpl)idigest_tip) ;
            }
            digest_tipFeignClient.removeBatch(list) ;
        }
    }


    public Page<Idigest_tip> select(SearchContext context){
        return null ;
    }


    public void getDraft(Idigest_tip digest_tip){
        Idigest_tip clientModel = digest_tipFeignClient.getDraft(digest_tip.getId(),(digest_tipImpl)digest_tip) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), digest_tip.getClass(), false);
        copier.copy(clientModel, digest_tip, null);
    }



}

