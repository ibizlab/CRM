package cn.ibizlab.odoo.client.odoo_event.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ievent_event;
import cn.ibizlab.odoo.client.odoo_event.model.event_eventImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[event_event] 服务对象接口
 */
public interface event_eventFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_events")
    public event_eventImpl create(@RequestBody event_eventImpl event_event);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_events/{id}")
    public event_eventImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_events/fetchdefault")
    public Page<event_eventImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_events/createbatch")
    public event_eventImpl createBatch(@RequestBody List<event_eventImpl> event_events);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_events/updatebatch")
    public event_eventImpl updateBatch(@RequestBody List<event_eventImpl> event_events);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_events/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_events/{id}")
    public event_eventImpl update(@PathVariable("id") Integer id,@RequestBody event_eventImpl event_event);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_events/removebatch")
    public event_eventImpl removeBatch(@RequestBody List<event_eventImpl> event_events);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_events/select")
    public Page<event_eventImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_events/{id}/getdraft")
    public event_eventImpl getDraft(@PathVariable("id") Integer id,@RequestBody event_eventImpl event_event);



}
