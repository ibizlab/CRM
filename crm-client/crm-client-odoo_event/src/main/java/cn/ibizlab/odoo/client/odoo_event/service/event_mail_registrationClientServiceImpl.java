package cn.ibizlab.odoo.client.odoo_event.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ievent_mail_registration;
import cn.ibizlab.odoo.client.odoo_event.config.odoo_eventClientProperties;
import cn.ibizlab.odoo.core.client.service.Ievent_mail_registrationClientService;
import cn.ibizlab.odoo.client.odoo_event.model.event_mail_registrationImpl;
import cn.ibizlab.odoo.client.odoo_event.feign.event_mail_registrationFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[event_mail_registration] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class event_mail_registrationClientServiceImpl implements Ievent_mail_registrationClientService {

    event_mail_registrationFeignClient event_mail_registrationFeignClient;

    @Autowired
    public event_mail_registrationClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_eventClientProperties odoo_eventClientProperties) {
        if (odoo_eventClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.event_mail_registrationFeignClient = nameBuilder.target(event_mail_registrationFeignClient.class,"http://"+odoo_eventClientProperties.getServiceId()+"/") ;
		}else if (odoo_eventClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.event_mail_registrationFeignClient = nameBuilder.target(event_mail_registrationFeignClient.class,odoo_eventClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ievent_mail_registration createModel() {
		return new event_mail_registrationImpl();
	}


    public void update(Ievent_mail_registration event_mail_registration){
        Ievent_mail_registration clientModel = event_mail_registrationFeignClient.update(event_mail_registration.getId(),(event_mail_registrationImpl)event_mail_registration) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_mail_registration.getClass(), false);
        copier.copy(clientModel, event_mail_registration, null);
    }


    public void remove(Ievent_mail_registration event_mail_registration){
        event_mail_registrationFeignClient.remove(event_mail_registration.getId()) ;
    }


    public void createBatch(List<Ievent_mail_registration> event_mail_registrations){
        if(event_mail_registrations!=null){
            List<event_mail_registrationImpl> list = new ArrayList<event_mail_registrationImpl>();
            for(Ievent_mail_registration ievent_mail_registration :event_mail_registrations){
                list.add((event_mail_registrationImpl)ievent_mail_registration) ;
            }
            event_mail_registrationFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Ievent_mail_registration> event_mail_registrations){
        if(event_mail_registrations!=null){
            List<event_mail_registrationImpl> list = new ArrayList<event_mail_registrationImpl>();
            for(Ievent_mail_registration ievent_mail_registration :event_mail_registrations){
                list.add((event_mail_registrationImpl)ievent_mail_registration) ;
            }
            event_mail_registrationFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Ievent_mail_registration> event_mail_registrations){
        if(event_mail_registrations!=null){
            List<event_mail_registrationImpl> list = new ArrayList<event_mail_registrationImpl>();
            for(Ievent_mail_registration ievent_mail_registration :event_mail_registrations){
                list.add((event_mail_registrationImpl)ievent_mail_registration) ;
            }
            event_mail_registrationFeignClient.updateBatch(list) ;
        }
    }


    public void create(Ievent_mail_registration event_mail_registration){
        Ievent_mail_registration clientModel = event_mail_registrationFeignClient.create((event_mail_registrationImpl)event_mail_registration) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_mail_registration.getClass(), false);
        copier.copy(clientModel, event_mail_registration, null);
    }


    public Page<Ievent_mail_registration> fetchDefault(SearchContext context){
        Page<event_mail_registrationImpl> page = this.event_mail_registrationFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Ievent_mail_registration event_mail_registration){
        Ievent_mail_registration clientModel = event_mail_registrationFeignClient.get(event_mail_registration.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_mail_registration.getClass(), false);
        copier.copy(clientModel, event_mail_registration, null);
    }


    public Page<Ievent_mail_registration> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ievent_mail_registration event_mail_registration){
        Ievent_mail_registration clientModel = event_mail_registrationFeignClient.getDraft(event_mail_registration.getId(),(event_mail_registrationImpl)event_mail_registration) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_mail_registration.getClass(), false);
        copier.copy(clientModel, event_mail_registration, null);
    }



}

