package cn.ibizlab.odoo.client.odoo_event.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ievent_registration;
import cn.ibizlab.odoo.client.odoo_event.config.odoo_eventClientProperties;
import cn.ibizlab.odoo.core.client.service.Ievent_registrationClientService;
import cn.ibizlab.odoo.client.odoo_event.model.event_registrationImpl;
import cn.ibizlab.odoo.client.odoo_event.feign.event_registrationFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[event_registration] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class event_registrationClientServiceImpl implements Ievent_registrationClientService {

    event_registrationFeignClient event_registrationFeignClient;

    @Autowired
    public event_registrationClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_eventClientProperties odoo_eventClientProperties) {
        if (odoo_eventClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.event_registrationFeignClient = nameBuilder.target(event_registrationFeignClient.class,"http://"+odoo_eventClientProperties.getServiceId()+"/") ;
		}else if (odoo_eventClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.event_registrationFeignClient = nameBuilder.target(event_registrationFeignClient.class,odoo_eventClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ievent_registration createModel() {
		return new event_registrationImpl();
	}


    public void update(Ievent_registration event_registration){
        Ievent_registration clientModel = event_registrationFeignClient.update(event_registration.getId(),(event_registrationImpl)event_registration) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_registration.getClass(), false);
        copier.copy(clientModel, event_registration, null);
    }


    public void updateBatch(List<Ievent_registration> event_registrations){
        if(event_registrations!=null){
            List<event_registrationImpl> list = new ArrayList<event_registrationImpl>();
            for(Ievent_registration ievent_registration :event_registrations){
                list.add((event_registrationImpl)ievent_registration) ;
            }
            event_registrationFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Ievent_registration event_registration){
        event_registrationFeignClient.remove(event_registration.getId()) ;
    }


    public void get(Ievent_registration event_registration){
        Ievent_registration clientModel = event_registrationFeignClient.get(event_registration.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_registration.getClass(), false);
        copier.copy(clientModel, event_registration, null);
    }


    public void create(Ievent_registration event_registration){
        Ievent_registration clientModel = event_registrationFeignClient.create((event_registrationImpl)event_registration) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_registration.getClass(), false);
        copier.copy(clientModel, event_registration, null);
    }


    public Page<Ievent_registration> fetchDefault(SearchContext context){
        Page<event_registrationImpl> page = this.event_registrationFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Ievent_registration> event_registrations){
        if(event_registrations!=null){
            List<event_registrationImpl> list = new ArrayList<event_registrationImpl>();
            for(Ievent_registration ievent_registration :event_registrations){
                list.add((event_registrationImpl)ievent_registration) ;
            }
            event_registrationFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Ievent_registration> event_registrations){
        if(event_registrations!=null){
            List<event_registrationImpl> list = new ArrayList<event_registrationImpl>();
            for(Ievent_registration ievent_registration :event_registrations){
                list.add((event_registrationImpl)ievent_registration) ;
            }
            event_registrationFeignClient.removeBatch(list) ;
        }
    }


    public Page<Ievent_registration> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ievent_registration event_registration){
        Ievent_registration clientModel = event_registrationFeignClient.getDraft(event_registration.getId(),(event_registrationImpl)event_registration) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_registration.getClass(), false);
        copier.copy(clientModel, event_registration, null);
    }



}

