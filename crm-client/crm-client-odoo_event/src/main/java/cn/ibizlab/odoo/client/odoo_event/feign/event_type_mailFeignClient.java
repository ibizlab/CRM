package cn.ibizlab.odoo.client.odoo_event.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ievent_type_mail;
import cn.ibizlab.odoo.client.odoo_event.model.event_type_mailImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[event_type_mail] 服务对象接口
 */
public interface event_type_mailFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_type_mails/createbatch")
    public event_type_mailImpl createBatch(@RequestBody List<event_type_mailImpl> event_type_mails);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_type_mails/{id}")
    public event_type_mailImpl update(@PathVariable("id") Integer id,@RequestBody event_type_mailImpl event_type_mail);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_type_mails/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_type_mails/updatebatch")
    public event_type_mailImpl updateBatch(@RequestBody List<event_type_mailImpl> event_type_mails);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_type_mails/removebatch")
    public event_type_mailImpl removeBatch(@RequestBody List<event_type_mailImpl> event_type_mails);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_type_mails/fetchdefault")
    public Page<event_type_mailImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_type_mails/{id}")
    public event_type_mailImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_type_mails")
    public event_type_mailImpl create(@RequestBody event_type_mailImpl event_type_mail);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_type_mails/select")
    public Page<event_type_mailImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_type_mails/{id}/getdraft")
    public event_type_mailImpl getDraft(@PathVariable("id") Integer id,@RequestBody event_type_mailImpl event_type_mail);



}
