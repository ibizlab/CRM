package cn.ibizlab.odoo.client.odoo_event.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ievent_mail;
import cn.ibizlab.odoo.client.odoo_event.config.odoo_eventClientProperties;
import cn.ibizlab.odoo.core.client.service.Ievent_mailClientService;
import cn.ibizlab.odoo.client.odoo_event.model.event_mailImpl;
import cn.ibizlab.odoo.client.odoo_event.feign.event_mailFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[event_mail] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class event_mailClientServiceImpl implements Ievent_mailClientService {

    event_mailFeignClient event_mailFeignClient;

    @Autowired
    public event_mailClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_eventClientProperties odoo_eventClientProperties) {
        if (odoo_eventClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.event_mailFeignClient = nameBuilder.target(event_mailFeignClient.class,"http://"+odoo_eventClientProperties.getServiceId()+"/") ;
		}else if (odoo_eventClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.event_mailFeignClient = nameBuilder.target(event_mailFeignClient.class,odoo_eventClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ievent_mail createModel() {
		return new event_mailImpl();
	}


    public void removeBatch(List<Ievent_mail> event_mails){
        if(event_mails!=null){
            List<event_mailImpl> list = new ArrayList<event_mailImpl>();
            for(Ievent_mail ievent_mail :event_mails){
                list.add((event_mailImpl)ievent_mail) ;
            }
            event_mailFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Ievent_mail event_mail){
        event_mailFeignClient.remove(event_mail.getId()) ;
    }


    public void create(Ievent_mail event_mail){
        Ievent_mail clientModel = event_mailFeignClient.create((event_mailImpl)event_mail) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_mail.getClass(), false);
        copier.copy(clientModel, event_mail, null);
    }


    public void createBatch(List<Ievent_mail> event_mails){
        if(event_mails!=null){
            List<event_mailImpl> list = new ArrayList<event_mailImpl>();
            for(Ievent_mail ievent_mail :event_mails){
                list.add((event_mailImpl)ievent_mail) ;
            }
            event_mailFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Ievent_mail> event_mails){
        if(event_mails!=null){
            List<event_mailImpl> list = new ArrayList<event_mailImpl>();
            for(Ievent_mail ievent_mail :event_mails){
                list.add((event_mailImpl)ievent_mail) ;
            }
            event_mailFeignClient.updateBatch(list) ;
        }
    }


    public Page<Ievent_mail> fetchDefault(SearchContext context){
        Page<event_mailImpl> page = this.event_mailFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Ievent_mail event_mail){
        Ievent_mail clientModel = event_mailFeignClient.get(event_mail.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_mail.getClass(), false);
        copier.copy(clientModel, event_mail, null);
    }


    public void update(Ievent_mail event_mail){
        Ievent_mail clientModel = event_mailFeignClient.update(event_mail.getId(),(event_mailImpl)event_mail) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_mail.getClass(), false);
        copier.copy(clientModel, event_mail, null);
    }


    public Page<Ievent_mail> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ievent_mail event_mail){
        Ievent_mail clientModel = event_mailFeignClient.getDraft(event_mail.getId(),(event_mailImpl)event_mail) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), event_mail.getClass(), false);
        copier.copy(clientModel, event_mail, null);
    }



}

