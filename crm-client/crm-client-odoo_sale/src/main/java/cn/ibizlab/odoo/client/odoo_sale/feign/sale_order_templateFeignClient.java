package cn.ibizlab.odoo.client.odoo_sale.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Isale_order_template;
import cn.ibizlab.odoo.client.odoo_sale.model.sale_order_templateImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[sale_order_template] 服务对象接口
 */
public interface sale_order_templateFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_order_templates")
    public sale_order_templateImpl create(@RequestBody sale_order_templateImpl sale_order_template);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sale/sale_order_templates/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_sale/sale_order_templates/updatebatch")
    public sale_order_templateImpl updateBatch(@RequestBody List<sale_order_templateImpl> sale_order_templates);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_sale/sale_order_templates/{id}")
    public sale_order_templateImpl update(@PathVariable("id") Integer id,@RequestBody sale_order_templateImpl sale_order_template);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_order_templates/{id}")
    public sale_order_templateImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sale/sale_order_templates/removebatch")
    public sale_order_templateImpl removeBatch(@RequestBody List<sale_order_templateImpl> sale_order_templates);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_order_templates/fetchdefault")
    public Page<sale_order_templateImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_order_templates/createbatch")
    public sale_order_templateImpl createBatch(@RequestBody List<sale_order_templateImpl> sale_order_templates);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_order_templates/select")
    public Page<sale_order_templateImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_order_templates/{id}/getdraft")
    public sale_order_templateImpl getDraft(@PathVariable("id") Integer id,@RequestBody sale_order_templateImpl sale_order_template);



}
