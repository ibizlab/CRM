package cn.ibizlab.odoo.client.odoo_sale.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Isale_report;
import cn.ibizlab.odoo.client.odoo_sale.model.sale_reportImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[sale_report] 服务对象接口
 */
public interface sale_reportFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_reports/{id}")
    public sale_reportImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sale/sale_reports/removebatch")
    public sale_reportImpl removeBatch(@RequestBody List<sale_reportImpl> sale_reports);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_sale/sale_reports/{id}")
    public sale_reportImpl update(@PathVariable("id") Integer id,@RequestBody sale_reportImpl sale_report);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_reports/fetchdefault")
    public Page<sale_reportImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sale/sale_reports/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_reports")
    public sale_reportImpl create(@RequestBody sale_reportImpl sale_report);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_reports/createbatch")
    public sale_reportImpl createBatch(@RequestBody List<sale_reportImpl> sale_reports);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_sale/sale_reports/updatebatch")
    public sale_reportImpl updateBatch(@RequestBody List<sale_reportImpl> sale_reports);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_reports/select")
    public Page<sale_reportImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_reports/{id}/getdraft")
    public sale_reportImpl getDraft(@PathVariable("id") Integer id,@RequestBody sale_reportImpl sale_report);



}
