package cn.ibizlab.odoo.client.odoo_sale.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Isale_order_template_option;
import cn.ibizlab.odoo.client.odoo_sale.model.sale_order_template_optionImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[sale_order_template_option] 服务对象接口
 */
public interface sale_order_template_optionFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sale/sale_order_template_options/removebatch")
    public sale_order_template_optionImpl removeBatch(@RequestBody List<sale_order_template_optionImpl> sale_order_template_options);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_sale/sale_order_template_options/updatebatch")
    public sale_order_template_optionImpl updateBatch(@RequestBody List<sale_order_template_optionImpl> sale_order_template_options);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_order_template_options/createbatch")
    public sale_order_template_optionImpl createBatch(@RequestBody List<sale_order_template_optionImpl> sale_order_template_options);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sale/sale_order_template_options/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_sale/sale_order_template_options/{id}")
    public sale_order_template_optionImpl update(@PathVariable("id") Integer id,@RequestBody sale_order_template_optionImpl sale_order_template_option);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_order_template_options/fetchdefault")
    public Page<sale_order_template_optionImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_order_template_options")
    public sale_order_template_optionImpl create(@RequestBody sale_order_template_optionImpl sale_order_template_option);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_order_template_options/{id}")
    public sale_order_template_optionImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_order_template_options/select")
    public Page<sale_order_template_optionImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_order_template_options/{id}/getdraft")
    public sale_order_template_optionImpl getDraft(@PathVariable("id") Integer id,@RequestBody sale_order_template_optionImpl sale_order_template_option);



}
