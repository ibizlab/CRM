package cn.ibizlab.odoo.client.odoo_sale.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isale_advance_payment_inv;
import cn.ibizlab.odoo.client.odoo_sale.config.odoo_saleClientProperties;
import cn.ibizlab.odoo.core.client.service.Isale_advance_payment_invClientService;
import cn.ibizlab.odoo.client.odoo_sale.model.sale_advance_payment_invImpl;
import cn.ibizlab.odoo.client.odoo_sale.feign.sale_advance_payment_invFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[sale_advance_payment_inv] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class sale_advance_payment_invClientServiceImpl implements Isale_advance_payment_invClientService {

    sale_advance_payment_invFeignClient sale_advance_payment_invFeignClient;

    @Autowired
    public sale_advance_payment_invClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_saleClientProperties odoo_saleClientProperties) {
        if (odoo_saleClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.sale_advance_payment_invFeignClient = nameBuilder.target(sale_advance_payment_invFeignClient.class,"http://"+odoo_saleClientProperties.getServiceId()+"/") ;
		}else if (odoo_saleClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.sale_advance_payment_invFeignClient = nameBuilder.target(sale_advance_payment_invFeignClient.class,odoo_saleClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Isale_advance_payment_inv createModel() {
		return new sale_advance_payment_invImpl();
	}


    public void create(Isale_advance_payment_inv sale_advance_payment_inv){
        Isale_advance_payment_inv clientModel = sale_advance_payment_invFeignClient.create((sale_advance_payment_invImpl)sale_advance_payment_inv) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_advance_payment_inv.getClass(), false);
        copier.copy(clientModel, sale_advance_payment_inv, null);
    }


    public void removeBatch(List<Isale_advance_payment_inv> sale_advance_payment_invs){
        if(sale_advance_payment_invs!=null){
            List<sale_advance_payment_invImpl> list = new ArrayList<sale_advance_payment_invImpl>();
            for(Isale_advance_payment_inv isale_advance_payment_inv :sale_advance_payment_invs){
                list.add((sale_advance_payment_invImpl)isale_advance_payment_inv) ;
            }
            sale_advance_payment_invFeignClient.removeBatch(list) ;
        }
    }


    public Page<Isale_advance_payment_inv> fetchDefault(SearchContext context){
        Page<sale_advance_payment_invImpl> page = this.sale_advance_payment_invFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Isale_advance_payment_inv> sale_advance_payment_invs){
        if(sale_advance_payment_invs!=null){
            List<sale_advance_payment_invImpl> list = new ArrayList<sale_advance_payment_invImpl>();
            for(Isale_advance_payment_inv isale_advance_payment_inv :sale_advance_payment_invs){
                list.add((sale_advance_payment_invImpl)isale_advance_payment_inv) ;
            }
            sale_advance_payment_invFeignClient.updateBatch(list) ;
        }
    }


    public void get(Isale_advance_payment_inv sale_advance_payment_inv){
        Isale_advance_payment_inv clientModel = sale_advance_payment_invFeignClient.get(sale_advance_payment_inv.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_advance_payment_inv.getClass(), false);
        copier.copy(clientModel, sale_advance_payment_inv, null);
    }


    public void remove(Isale_advance_payment_inv sale_advance_payment_inv){
        sale_advance_payment_invFeignClient.remove(sale_advance_payment_inv.getId()) ;
    }


    public void update(Isale_advance_payment_inv sale_advance_payment_inv){
        Isale_advance_payment_inv clientModel = sale_advance_payment_invFeignClient.update(sale_advance_payment_inv.getId(),(sale_advance_payment_invImpl)sale_advance_payment_inv) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_advance_payment_inv.getClass(), false);
        copier.copy(clientModel, sale_advance_payment_inv, null);
    }


    public void createBatch(List<Isale_advance_payment_inv> sale_advance_payment_invs){
        if(sale_advance_payment_invs!=null){
            List<sale_advance_payment_invImpl> list = new ArrayList<sale_advance_payment_invImpl>();
            for(Isale_advance_payment_inv isale_advance_payment_inv :sale_advance_payment_invs){
                list.add((sale_advance_payment_invImpl)isale_advance_payment_inv) ;
            }
            sale_advance_payment_invFeignClient.createBatch(list) ;
        }
    }


    public Page<Isale_advance_payment_inv> select(SearchContext context){
        return null ;
    }


    public void getDraft(Isale_advance_payment_inv sale_advance_payment_inv){
        Isale_advance_payment_inv clientModel = sale_advance_payment_invFeignClient.getDraft(sale_advance_payment_inv.getId(),(sale_advance_payment_invImpl)sale_advance_payment_inv) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_advance_payment_inv.getClass(), false);
        copier.copy(clientModel, sale_advance_payment_inv, null);
    }



}

