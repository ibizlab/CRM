package cn.ibizlab.odoo.client.odoo_sale.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isale_order;
import cn.ibizlab.odoo.client.odoo_sale.config.odoo_saleClientProperties;
import cn.ibizlab.odoo.core.client.service.Isale_orderClientService;
import cn.ibizlab.odoo.client.odoo_sale.model.sale_orderImpl;
import cn.ibizlab.odoo.client.odoo_sale.feign.sale_orderFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[sale_order] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class sale_orderClientServiceImpl implements Isale_orderClientService {

    sale_orderFeignClient sale_orderFeignClient;

    @Autowired
    public sale_orderClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_saleClientProperties odoo_saleClientProperties) {
        if (odoo_saleClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.sale_orderFeignClient = nameBuilder.target(sale_orderFeignClient.class,"http://"+odoo_saleClientProperties.getServiceId()+"/") ;
		}else if (odoo_saleClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.sale_orderFeignClient = nameBuilder.target(sale_orderFeignClient.class,odoo_saleClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Isale_order createModel() {
		return new sale_orderImpl();
	}


    public void updateBatch(List<Isale_order> sale_orders){
        if(sale_orders!=null){
            List<sale_orderImpl> list = new ArrayList<sale_orderImpl>();
            for(Isale_order isale_order :sale_orders){
                list.add((sale_orderImpl)isale_order) ;
            }
            sale_orderFeignClient.updateBatch(list) ;
        }
    }


    public void update(Isale_order sale_order){
        Isale_order clientModel = sale_orderFeignClient.update(sale_order.getId(),(sale_orderImpl)sale_order) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_order.getClass(), false);
        copier.copy(clientModel, sale_order, null);
    }


    public Page<Isale_order> fetchDefault(SearchContext context){
        Page<sale_orderImpl> page = this.sale_orderFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Isale_order> sale_orders){
        if(sale_orders!=null){
            List<sale_orderImpl> list = new ArrayList<sale_orderImpl>();
            for(Isale_order isale_order :sale_orders){
                list.add((sale_orderImpl)isale_order) ;
            }
            sale_orderFeignClient.removeBatch(list) ;
        }
    }


    public void create(Isale_order sale_order){
        Isale_order clientModel = sale_orderFeignClient.create((sale_orderImpl)sale_order) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_order.getClass(), false);
        copier.copy(clientModel, sale_order, null);
    }


    public void createBatch(List<Isale_order> sale_orders){
        if(sale_orders!=null){
            List<sale_orderImpl> list = new ArrayList<sale_orderImpl>();
            for(Isale_order isale_order :sale_orders){
                list.add((sale_orderImpl)isale_order) ;
            }
            sale_orderFeignClient.createBatch(list) ;
        }
    }


    public void get(Isale_order sale_order){
        Isale_order clientModel = sale_orderFeignClient.get(sale_order.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_order.getClass(), false);
        copier.copy(clientModel, sale_order, null);
    }


    public void remove(Isale_order sale_order){
        sale_orderFeignClient.remove(sale_order.getId()) ;
    }


    public Page<Isale_order> select(SearchContext context){
        return null ;
    }


    public void save(Isale_order sale_order){
        Isale_order clientModel = sale_orderFeignClient.save(sale_order.getId(),(sale_orderImpl)sale_order) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_order.getClass(), false);
        copier.copy(clientModel, sale_order, null);
    }


    public void checkKey(Isale_order sale_order){
        Isale_order clientModel = sale_orderFeignClient.checkKey(sale_order.getId(),(sale_orderImpl)sale_order) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_order.getClass(), false);
        copier.copy(clientModel, sale_order, null);
    }


    public void getDraft(Isale_order sale_order){
        Isale_order clientModel = sale_orderFeignClient.getDraft(sale_order.getId(),(sale_orderImpl)sale_order) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sale_order.getClass(), false);
        copier.copy(clientModel, sale_order, null);
    }



}

