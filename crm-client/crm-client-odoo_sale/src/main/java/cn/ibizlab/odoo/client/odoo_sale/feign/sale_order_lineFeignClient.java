package cn.ibizlab.odoo.client.odoo_sale.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Isale_order_line;
import cn.ibizlab.odoo.client.odoo_sale.model.sale_order_lineImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[sale_order_line] 服务对象接口
 */
public interface sale_order_lineFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sale/sale_order_lines/removebatch")
    public sale_order_lineImpl removeBatch(@RequestBody List<sale_order_lineImpl> sale_order_lines);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_sale/sale_order_lines/{id}")
    public sale_order_lineImpl update(@PathVariable("id") Integer id,@RequestBody sale_order_lineImpl sale_order_line);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_order_lines/createbatch")
    public sale_order_lineImpl createBatch(@RequestBody List<sale_order_lineImpl> sale_order_lines);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sale/sale_order_lines/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_order_lines/{id}")
    public sale_order_lineImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_order_lines")
    public sale_order_lineImpl create(@RequestBody sale_order_lineImpl sale_order_line);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_sale/sale_order_lines/updatebatch")
    public sale_order_lineImpl updateBatch(@RequestBody List<sale_order_lineImpl> sale_order_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_order_lines/fetchdefault")
    public Page<sale_order_lineImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_order_lines/select")
    public Page<sale_order_lineImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_order_lines/{id}/getdraft")
    public sale_order_lineImpl getDraft(@PathVariable("id") Integer id,@RequestBody sale_order_lineImpl sale_order_line);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_order_lines/{id}/checkkey")
    public sale_order_lineImpl checkKey(@PathVariable("id") Integer id,@RequestBody sale_order_lineImpl sale_order_line);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_order_lines/{id}/save")
    public sale_order_lineImpl save(@PathVariable("id") Integer id,@RequestBody sale_order_lineImpl sale_order_line);



}
