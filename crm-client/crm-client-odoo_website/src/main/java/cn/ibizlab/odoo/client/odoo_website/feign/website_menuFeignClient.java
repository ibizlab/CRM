package cn.ibizlab.odoo.client.odoo_website.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iwebsite_menu;
import cn.ibizlab.odoo.client.odoo_website.model.website_menuImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[website_menu] 服务对象接口
 */
public interface website_menuFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_menus/{id}")
    public website_menuImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_website/website_menus/removebatch")
    public website_menuImpl removeBatch(@RequestBody List<website_menuImpl> website_menus);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_website/website_menus/updatebatch")
    public website_menuImpl updateBatch(@RequestBody List<website_menuImpl> website_menus);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_website/website_menus")
    public website_menuImpl create(@RequestBody website_menuImpl website_menu);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_website/website_menus/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_website/website_menus/{id}")
    public website_menuImpl update(@PathVariable("id") Integer id,@RequestBody website_menuImpl website_menu);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_menus/fetchdefault")
    public Page<website_menuImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_website/website_menus/createbatch")
    public website_menuImpl createBatch(@RequestBody List<website_menuImpl> website_menus);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_menus/select")
    public Page<website_menuImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_menus/{id}/getdraft")
    public website_menuImpl getDraft(@PathVariable("id") Integer id,@RequestBody website_menuImpl website_menu);



}
