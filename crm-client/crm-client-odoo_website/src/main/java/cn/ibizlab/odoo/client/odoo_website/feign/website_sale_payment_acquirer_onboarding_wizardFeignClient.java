package cn.ibizlab.odoo.client.odoo_website.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iwebsite_sale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.client.odoo_website.model.website_sale_payment_acquirer_onboarding_wizardImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[website_sale_payment_acquirer_onboarding_wizard] 服务对象接口
 */
public interface website_sale_payment_acquirer_onboarding_wizardFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_sale_payment_acquirer_onboarding_wizards/fetchdefault")
    public Page<website_sale_payment_acquirer_onboarding_wizardImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_website/website_sale_payment_acquirer_onboarding_wizards")
    public website_sale_payment_acquirer_onboarding_wizardImpl create(@RequestBody website_sale_payment_acquirer_onboarding_wizardImpl website_sale_payment_acquirer_onboarding_wizard);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_website/website_sale_payment_acquirer_onboarding_wizards/updatebatch")
    public website_sale_payment_acquirer_onboarding_wizardImpl updateBatch(@RequestBody List<website_sale_payment_acquirer_onboarding_wizardImpl> website_sale_payment_acquirer_onboarding_wizards);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_website/website_sale_payment_acquirer_onboarding_wizards/removebatch")
    public website_sale_payment_acquirer_onboarding_wizardImpl removeBatch(@RequestBody List<website_sale_payment_acquirer_onboarding_wizardImpl> website_sale_payment_acquirer_onboarding_wizards);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_sale_payment_acquirer_onboarding_wizards/{id}")
    public website_sale_payment_acquirer_onboarding_wizardImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_website/website_sale_payment_acquirer_onboarding_wizards/{id}")
    public website_sale_payment_acquirer_onboarding_wizardImpl update(@PathVariable("id") Integer id,@RequestBody website_sale_payment_acquirer_onboarding_wizardImpl website_sale_payment_acquirer_onboarding_wizard);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_website/website_sale_payment_acquirer_onboarding_wizards/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_website/website_sale_payment_acquirer_onboarding_wizards/createbatch")
    public website_sale_payment_acquirer_onboarding_wizardImpl createBatch(@RequestBody List<website_sale_payment_acquirer_onboarding_wizardImpl> website_sale_payment_acquirer_onboarding_wizards);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_sale_payment_acquirer_onboarding_wizards/select")
    public Page<website_sale_payment_acquirer_onboarding_wizardImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_sale_payment_acquirer_onboarding_wizards/{id}/getdraft")
    public website_sale_payment_acquirer_onboarding_wizardImpl getDraft(@PathVariable("id") Integer id,@RequestBody website_sale_payment_acquirer_onboarding_wizardImpl website_sale_payment_acquirer_onboarding_wizard);



}
