package cn.ibizlab.odoo.client.odoo_website.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ConditionalOnClass(odoo_websiteClientConfiguration.class)
@ConditionalOnWebApplication
@EnableConfigurationProperties(odoo_websiteClientProperties.class)
@Import({
    FeignClientsConfiguration.class
})
public class odoo_websiteAutoConfiguration {

}
