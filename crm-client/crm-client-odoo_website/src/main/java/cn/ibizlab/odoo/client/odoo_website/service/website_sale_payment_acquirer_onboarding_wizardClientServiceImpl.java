package cn.ibizlab.odoo.client.odoo_website.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iwebsite_sale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.client.odoo_website.config.odoo_websiteClientProperties;
import cn.ibizlab.odoo.core.client.service.Iwebsite_sale_payment_acquirer_onboarding_wizardClientService;
import cn.ibizlab.odoo.client.odoo_website.model.website_sale_payment_acquirer_onboarding_wizardImpl;
import cn.ibizlab.odoo.client.odoo_website.feign.website_sale_payment_acquirer_onboarding_wizardFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[website_sale_payment_acquirer_onboarding_wizard] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class website_sale_payment_acquirer_onboarding_wizardClientServiceImpl implements Iwebsite_sale_payment_acquirer_onboarding_wizardClientService {

    website_sale_payment_acquirer_onboarding_wizardFeignClient website_sale_payment_acquirer_onboarding_wizardFeignClient;

    @Autowired
    public website_sale_payment_acquirer_onboarding_wizardClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_websiteClientProperties odoo_websiteClientProperties) {
        if (odoo_websiteClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.website_sale_payment_acquirer_onboarding_wizardFeignClient = nameBuilder.target(website_sale_payment_acquirer_onboarding_wizardFeignClient.class,"http://"+odoo_websiteClientProperties.getServiceId()+"/") ;
		}else if (odoo_websiteClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.website_sale_payment_acquirer_onboarding_wizardFeignClient = nameBuilder.target(website_sale_payment_acquirer_onboarding_wizardFeignClient.class,odoo_websiteClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iwebsite_sale_payment_acquirer_onboarding_wizard createModel() {
		return new website_sale_payment_acquirer_onboarding_wizardImpl();
	}


    public Page<Iwebsite_sale_payment_acquirer_onboarding_wizard> fetchDefault(SearchContext context){
        Page<website_sale_payment_acquirer_onboarding_wizardImpl> page = this.website_sale_payment_acquirer_onboarding_wizardFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Iwebsite_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard){
        Iwebsite_sale_payment_acquirer_onboarding_wizard clientModel = website_sale_payment_acquirer_onboarding_wizardFeignClient.create((website_sale_payment_acquirer_onboarding_wizardImpl)website_sale_payment_acquirer_onboarding_wizard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), website_sale_payment_acquirer_onboarding_wizard.getClass(), false);
        copier.copy(clientModel, website_sale_payment_acquirer_onboarding_wizard, null);
    }


    public void updateBatch(List<Iwebsite_sale_payment_acquirer_onboarding_wizard> website_sale_payment_acquirer_onboarding_wizards){
        if(website_sale_payment_acquirer_onboarding_wizards!=null){
            List<website_sale_payment_acquirer_onboarding_wizardImpl> list = new ArrayList<website_sale_payment_acquirer_onboarding_wizardImpl>();
            for(Iwebsite_sale_payment_acquirer_onboarding_wizard iwebsite_sale_payment_acquirer_onboarding_wizard :website_sale_payment_acquirer_onboarding_wizards){
                list.add((website_sale_payment_acquirer_onboarding_wizardImpl)iwebsite_sale_payment_acquirer_onboarding_wizard) ;
            }
            website_sale_payment_acquirer_onboarding_wizardFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Iwebsite_sale_payment_acquirer_onboarding_wizard> website_sale_payment_acquirer_onboarding_wizards){
        if(website_sale_payment_acquirer_onboarding_wizards!=null){
            List<website_sale_payment_acquirer_onboarding_wizardImpl> list = new ArrayList<website_sale_payment_acquirer_onboarding_wizardImpl>();
            for(Iwebsite_sale_payment_acquirer_onboarding_wizard iwebsite_sale_payment_acquirer_onboarding_wizard :website_sale_payment_acquirer_onboarding_wizards){
                list.add((website_sale_payment_acquirer_onboarding_wizardImpl)iwebsite_sale_payment_acquirer_onboarding_wizard) ;
            }
            website_sale_payment_acquirer_onboarding_wizardFeignClient.removeBatch(list) ;
        }
    }


    public void get(Iwebsite_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard){
        Iwebsite_sale_payment_acquirer_onboarding_wizard clientModel = website_sale_payment_acquirer_onboarding_wizardFeignClient.get(website_sale_payment_acquirer_onboarding_wizard.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), website_sale_payment_acquirer_onboarding_wizard.getClass(), false);
        copier.copy(clientModel, website_sale_payment_acquirer_onboarding_wizard, null);
    }


    public void update(Iwebsite_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard){
        Iwebsite_sale_payment_acquirer_onboarding_wizard clientModel = website_sale_payment_acquirer_onboarding_wizardFeignClient.update(website_sale_payment_acquirer_onboarding_wizard.getId(),(website_sale_payment_acquirer_onboarding_wizardImpl)website_sale_payment_acquirer_onboarding_wizard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), website_sale_payment_acquirer_onboarding_wizard.getClass(), false);
        copier.copy(clientModel, website_sale_payment_acquirer_onboarding_wizard, null);
    }


    public void remove(Iwebsite_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard){
        website_sale_payment_acquirer_onboarding_wizardFeignClient.remove(website_sale_payment_acquirer_onboarding_wizard.getId()) ;
    }


    public void createBatch(List<Iwebsite_sale_payment_acquirer_onboarding_wizard> website_sale_payment_acquirer_onboarding_wizards){
        if(website_sale_payment_acquirer_onboarding_wizards!=null){
            List<website_sale_payment_acquirer_onboarding_wizardImpl> list = new ArrayList<website_sale_payment_acquirer_onboarding_wizardImpl>();
            for(Iwebsite_sale_payment_acquirer_onboarding_wizard iwebsite_sale_payment_acquirer_onboarding_wizard :website_sale_payment_acquirer_onboarding_wizards){
                list.add((website_sale_payment_acquirer_onboarding_wizardImpl)iwebsite_sale_payment_acquirer_onboarding_wizard) ;
            }
            website_sale_payment_acquirer_onboarding_wizardFeignClient.createBatch(list) ;
        }
    }


    public Page<Iwebsite_sale_payment_acquirer_onboarding_wizard> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iwebsite_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard){
        Iwebsite_sale_payment_acquirer_onboarding_wizard clientModel = website_sale_payment_acquirer_onboarding_wizardFeignClient.getDraft(website_sale_payment_acquirer_onboarding_wizard.getId(),(website_sale_payment_acquirer_onboarding_wizardImpl)website_sale_payment_acquirer_onboarding_wizard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), website_sale_payment_acquirer_onboarding_wizard.getClass(), false);
        copier.copy(clientModel, website_sale_payment_acquirer_onboarding_wizard, null);
    }



}

