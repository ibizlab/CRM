package cn.ibizlab.odoo.client.odoo_website.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iwebsite_published_mixin;
import cn.ibizlab.odoo.client.odoo_website.model.website_published_mixinImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[website_published_mixin] 服务对象接口
 */
public interface website_published_mixinFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_published_mixins/fetchdefault")
    public Page<website_published_mixinImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_website/website_published_mixins/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_website/website_published_mixins/updatebatch")
    public website_published_mixinImpl updateBatch(@RequestBody List<website_published_mixinImpl> website_published_mixins);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_website/website_published_mixins/createbatch")
    public website_published_mixinImpl createBatch(@RequestBody List<website_published_mixinImpl> website_published_mixins);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_website/website_published_mixins")
    public website_published_mixinImpl create(@RequestBody website_published_mixinImpl website_published_mixin);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_website/website_published_mixins/{id}")
    public website_published_mixinImpl update(@PathVariable("id") Integer id,@RequestBody website_published_mixinImpl website_published_mixin);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_published_mixins/{id}")
    public website_published_mixinImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_website/website_published_mixins/removebatch")
    public website_published_mixinImpl removeBatch(@RequestBody List<website_published_mixinImpl> website_published_mixins);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_published_mixins/select")
    public Page<website_published_mixinImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_published_mixins/{id}/getdraft")
    public website_published_mixinImpl getDraft(@PathVariable("id") Integer id,@RequestBody website_published_mixinImpl website_published_mixin);



}
