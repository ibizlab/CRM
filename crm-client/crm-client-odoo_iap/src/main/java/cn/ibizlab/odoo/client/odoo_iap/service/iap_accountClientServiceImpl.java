package cn.ibizlab.odoo.client.odoo_iap.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iiap_account;
import cn.ibizlab.odoo.client.odoo_iap.config.odoo_iapClientProperties;
import cn.ibizlab.odoo.core.client.service.Iiap_accountClientService;
import cn.ibizlab.odoo.client.odoo_iap.model.iap_accountImpl;
import cn.ibizlab.odoo.client.odoo_iap.feign.iap_accountFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[iap_account] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class iap_accountClientServiceImpl implements Iiap_accountClientService {

    iap_accountFeignClient iap_accountFeignClient;

    @Autowired
    public iap_accountClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_iapClientProperties odoo_iapClientProperties) {
        if (odoo_iapClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.iap_accountFeignClient = nameBuilder.target(iap_accountFeignClient.class,"http://"+odoo_iapClientProperties.getServiceId()+"/") ;
		}else if (odoo_iapClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.iap_accountFeignClient = nameBuilder.target(iap_accountFeignClient.class,odoo_iapClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iiap_account createModel() {
		return new iap_accountImpl();
	}


    public Page<Iiap_account> fetchDefault(SearchContext context){
        Page<iap_accountImpl> page = this.iap_accountFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Iiap_account> iap_accounts){
        if(iap_accounts!=null){
            List<iap_accountImpl> list = new ArrayList<iap_accountImpl>();
            for(Iiap_account iiap_account :iap_accounts){
                list.add((iap_accountImpl)iiap_account) ;
            }
            iap_accountFeignClient.createBatch(list) ;
        }
    }


    public void create(Iiap_account iap_account){
        Iiap_account clientModel = iap_accountFeignClient.create((iap_accountImpl)iap_account) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), iap_account.getClass(), false);
        copier.copy(clientModel, iap_account, null);
    }


    public void removeBatch(List<Iiap_account> iap_accounts){
        if(iap_accounts!=null){
            List<iap_accountImpl> list = new ArrayList<iap_accountImpl>();
            for(Iiap_account iiap_account :iap_accounts){
                list.add((iap_accountImpl)iiap_account) ;
            }
            iap_accountFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Iiap_account> iap_accounts){
        if(iap_accounts!=null){
            List<iap_accountImpl> list = new ArrayList<iap_accountImpl>();
            for(Iiap_account iiap_account :iap_accounts){
                list.add((iap_accountImpl)iiap_account) ;
            }
            iap_accountFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Iiap_account iap_account){
        iap_accountFeignClient.remove(iap_account.getId()) ;
    }


    public void update(Iiap_account iap_account){
        Iiap_account clientModel = iap_accountFeignClient.update(iap_account.getId(),(iap_accountImpl)iap_account) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), iap_account.getClass(), false);
        copier.copy(clientModel, iap_account, null);
    }


    public void get(Iiap_account iap_account){
        Iiap_account clientModel = iap_accountFeignClient.get(iap_account.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), iap_account.getClass(), false);
        copier.copy(clientModel, iap_account, null);
    }


    public Page<Iiap_account> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iiap_account iap_account){
        Iiap_account clientModel = iap_accountFeignClient.getDraft(iap_account.getId(),(iap_accountImpl)iap_account) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), iap_account.getClass(), false);
        copier.copy(clientModel, iap_account, null);
    }



}

