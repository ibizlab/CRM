package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_recruitment_stage;
import cn.ibizlab.odoo.client.odoo_hr.config.odoo_hrClientProperties;
import cn.ibizlab.odoo.core.client.service.Ihr_recruitment_stageClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_recruitment_stageImpl;
import cn.ibizlab.odoo.client.odoo_hr.feign.hr_recruitment_stageFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[hr_recruitment_stage] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class hr_recruitment_stageClientServiceImpl implements Ihr_recruitment_stageClientService {

    hr_recruitment_stageFeignClient hr_recruitment_stageFeignClient;

    @Autowired
    public hr_recruitment_stageClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_hrClientProperties odoo_hrClientProperties) {
        if (odoo_hrClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_recruitment_stageFeignClient = nameBuilder.target(hr_recruitment_stageFeignClient.class,"http://"+odoo_hrClientProperties.getServiceId()+"/") ;
		}else if (odoo_hrClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_recruitment_stageFeignClient = nameBuilder.target(hr_recruitment_stageFeignClient.class,odoo_hrClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ihr_recruitment_stage createModel() {
		return new hr_recruitment_stageImpl();
	}


    public void updateBatch(List<Ihr_recruitment_stage> hr_recruitment_stages){
        if(hr_recruitment_stages!=null){
            List<hr_recruitment_stageImpl> list = new ArrayList<hr_recruitment_stageImpl>();
            for(Ihr_recruitment_stage ihr_recruitment_stage :hr_recruitment_stages){
                list.add((hr_recruitment_stageImpl)ihr_recruitment_stage) ;
            }
            hr_recruitment_stageFeignClient.updateBatch(list) ;
        }
    }


    public Page<Ihr_recruitment_stage> fetchDefault(SearchContext context){
        Page<hr_recruitment_stageImpl> page = this.hr_recruitment_stageFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Ihr_recruitment_stage hr_recruitment_stage){
        Ihr_recruitment_stage clientModel = hr_recruitment_stageFeignClient.create((hr_recruitment_stageImpl)hr_recruitment_stage) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_recruitment_stage.getClass(), false);
        copier.copy(clientModel, hr_recruitment_stage, null);
    }


    public void remove(Ihr_recruitment_stage hr_recruitment_stage){
        hr_recruitment_stageFeignClient.remove(hr_recruitment_stage.getId()) ;
    }


    public void get(Ihr_recruitment_stage hr_recruitment_stage){
        Ihr_recruitment_stage clientModel = hr_recruitment_stageFeignClient.get(hr_recruitment_stage.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_recruitment_stage.getClass(), false);
        copier.copy(clientModel, hr_recruitment_stage, null);
    }


    public void createBatch(List<Ihr_recruitment_stage> hr_recruitment_stages){
        if(hr_recruitment_stages!=null){
            List<hr_recruitment_stageImpl> list = new ArrayList<hr_recruitment_stageImpl>();
            for(Ihr_recruitment_stage ihr_recruitment_stage :hr_recruitment_stages){
                list.add((hr_recruitment_stageImpl)ihr_recruitment_stage) ;
            }
            hr_recruitment_stageFeignClient.createBatch(list) ;
        }
    }


    public void update(Ihr_recruitment_stage hr_recruitment_stage){
        Ihr_recruitment_stage clientModel = hr_recruitment_stageFeignClient.update(hr_recruitment_stage.getId(),(hr_recruitment_stageImpl)hr_recruitment_stage) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_recruitment_stage.getClass(), false);
        copier.copy(clientModel, hr_recruitment_stage, null);
    }


    public void removeBatch(List<Ihr_recruitment_stage> hr_recruitment_stages){
        if(hr_recruitment_stages!=null){
            List<hr_recruitment_stageImpl> list = new ArrayList<hr_recruitment_stageImpl>();
            for(Ihr_recruitment_stage ihr_recruitment_stage :hr_recruitment_stages){
                list.add((hr_recruitment_stageImpl)ihr_recruitment_stage) ;
            }
            hr_recruitment_stageFeignClient.removeBatch(list) ;
        }
    }


    public Page<Ihr_recruitment_stage> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ihr_recruitment_stage hr_recruitment_stage){
        Ihr_recruitment_stage clientModel = hr_recruitment_stageFeignClient.getDraft(hr_recruitment_stage.getId(),(hr_recruitment_stageImpl)hr_recruitment_stage) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_recruitment_stage.getClass(), false);
        copier.copy(clientModel, hr_recruitment_stage, null);
    }



}

