package cn.ibizlab.odoo.client.odoo_hr.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ihr_recruitment_degree;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_recruitment_degreeImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[hr_recruitment_degree] 服务对象接口
 */
public interface hr_recruitment_degreeFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_recruitment_degrees/fetchdefault")
    public Page<hr_recruitment_degreeImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_recruitment_degrees")
    public hr_recruitment_degreeImpl create(@RequestBody hr_recruitment_degreeImpl hr_recruitment_degree);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_recruitment_degrees/updatebatch")
    public hr_recruitment_degreeImpl updateBatch(@RequestBody List<hr_recruitment_degreeImpl> hr_recruitment_degrees);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_recruitment_degrees/removebatch")
    public hr_recruitment_degreeImpl removeBatch(@RequestBody List<hr_recruitment_degreeImpl> hr_recruitment_degrees);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_recruitment_degrees/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_recruitment_degrees/{id}")
    public hr_recruitment_degreeImpl update(@PathVariable("id") Integer id,@RequestBody hr_recruitment_degreeImpl hr_recruitment_degree);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_recruitment_degrees/{id}")
    public hr_recruitment_degreeImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_recruitment_degrees/createbatch")
    public hr_recruitment_degreeImpl createBatch(@RequestBody List<hr_recruitment_degreeImpl> hr_recruitment_degrees);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_recruitment_degrees/select")
    public Page<hr_recruitment_degreeImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_recruitment_degrees/{id}/getdraft")
    public hr_recruitment_degreeImpl getDraft(@PathVariable("id") Integer id,@RequestBody hr_recruitment_degreeImpl hr_recruitment_degree);



}
