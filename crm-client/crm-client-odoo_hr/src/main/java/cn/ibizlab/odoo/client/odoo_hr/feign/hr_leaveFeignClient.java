package cn.ibizlab.odoo.client.odoo_hr.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ihr_leave;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_leaveImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[hr_leave] 服务对象接口
 */
public interface hr_leaveFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_leaves/fetchdefault")
    public Page<hr_leaveImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_leaves/createbatch")
    public hr_leaveImpl createBatch(@RequestBody List<hr_leaveImpl> hr_leaves);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_leaves/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_leaves/removebatch")
    public hr_leaveImpl removeBatch(@RequestBody List<hr_leaveImpl> hr_leaves);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_leaves/updatebatch")
    public hr_leaveImpl updateBatch(@RequestBody List<hr_leaveImpl> hr_leaves);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_leaves")
    public hr_leaveImpl create(@RequestBody hr_leaveImpl hr_leave);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_leaves/{id}")
    public hr_leaveImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_leaves/{id}")
    public hr_leaveImpl update(@PathVariable("id") Integer id,@RequestBody hr_leaveImpl hr_leave);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_leaves/select")
    public Page<hr_leaveImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_leaves/{id}/getdraft")
    public hr_leaveImpl getDraft(@PathVariable("id") Integer id,@RequestBody hr_leaveImpl hr_leave);



}
