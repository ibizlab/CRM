package cn.ibizlab.odoo.client.odoo_hr.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ihr_leave_type;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_leave_typeImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[hr_leave_type] 服务对象接口
 */
public interface hr_leave_typeFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_leave_types/updatebatch")
    public hr_leave_typeImpl updateBatch(@RequestBody List<hr_leave_typeImpl> hr_leave_types);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_leave_types/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_leave_types")
    public hr_leave_typeImpl create(@RequestBody hr_leave_typeImpl hr_leave_type);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_leave_types/fetchdefault")
    public Page<hr_leave_typeImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_leave_types/{id}")
    public hr_leave_typeImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_leave_types/{id}")
    public hr_leave_typeImpl update(@PathVariable("id") Integer id,@RequestBody hr_leave_typeImpl hr_leave_type);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_leave_types/createbatch")
    public hr_leave_typeImpl createBatch(@RequestBody List<hr_leave_typeImpl> hr_leave_types);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_leave_types/removebatch")
    public hr_leave_typeImpl removeBatch(@RequestBody List<hr_leave_typeImpl> hr_leave_types);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_leave_types/select")
    public Page<hr_leave_typeImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_leave_types/{id}/getdraft")
    public hr_leave_typeImpl getDraft(@PathVariable("id") Integer id,@RequestBody hr_leave_typeImpl hr_leave_type);



}
