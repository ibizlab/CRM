package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_job;
import cn.ibizlab.odoo.client.odoo_hr.config.odoo_hrClientProperties;
import cn.ibizlab.odoo.core.client.service.Ihr_jobClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_jobImpl;
import cn.ibizlab.odoo.client.odoo_hr.feign.hr_jobFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[hr_job] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class hr_jobClientServiceImpl implements Ihr_jobClientService {

    hr_jobFeignClient hr_jobFeignClient;

    @Autowired
    public hr_jobClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_hrClientProperties odoo_hrClientProperties) {
        if (odoo_hrClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_jobFeignClient = nameBuilder.target(hr_jobFeignClient.class,"http://"+odoo_hrClientProperties.getServiceId()+"/") ;
		}else if (odoo_hrClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_jobFeignClient = nameBuilder.target(hr_jobFeignClient.class,odoo_hrClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ihr_job createModel() {
		return new hr_jobImpl();
	}


    public void update(Ihr_job hr_job){
        Ihr_job clientModel = hr_jobFeignClient.update(hr_job.getId(),(hr_jobImpl)hr_job) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_job.getClass(), false);
        copier.copy(clientModel, hr_job, null);
    }


    public void createBatch(List<Ihr_job> hr_jobs){
        if(hr_jobs!=null){
            List<hr_jobImpl> list = new ArrayList<hr_jobImpl>();
            for(Ihr_job ihr_job :hr_jobs){
                list.add((hr_jobImpl)ihr_job) ;
            }
            hr_jobFeignClient.createBatch(list) ;
        }
    }


    public void remove(Ihr_job hr_job){
        hr_jobFeignClient.remove(hr_job.getId()) ;
    }


    public void removeBatch(List<Ihr_job> hr_jobs){
        if(hr_jobs!=null){
            List<hr_jobImpl> list = new ArrayList<hr_jobImpl>();
            for(Ihr_job ihr_job :hr_jobs){
                list.add((hr_jobImpl)ihr_job) ;
            }
            hr_jobFeignClient.removeBatch(list) ;
        }
    }


    public void create(Ihr_job hr_job){
        Ihr_job clientModel = hr_jobFeignClient.create((hr_jobImpl)hr_job) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_job.getClass(), false);
        copier.copy(clientModel, hr_job, null);
    }


    public Page<Ihr_job> fetchDefault(SearchContext context){
        Page<hr_jobImpl> page = this.hr_jobFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Ihr_job> hr_jobs){
        if(hr_jobs!=null){
            List<hr_jobImpl> list = new ArrayList<hr_jobImpl>();
            for(Ihr_job ihr_job :hr_jobs){
                list.add((hr_jobImpl)ihr_job) ;
            }
            hr_jobFeignClient.updateBatch(list) ;
        }
    }


    public void get(Ihr_job hr_job){
        Ihr_job clientModel = hr_jobFeignClient.get(hr_job.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_job.getClass(), false);
        copier.copy(clientModel, hr_job, null);
    }


    public Page<Ihr_job> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ihr_job hr_job){
        Ihr_job clientModel = hr_jobFeignClient.getDraft(hr_job.getId(),(hr_jobImpl)hr_job) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_job.getClass(), false);
        copier.copy(clientModel, hr_job, null);
    }



}

