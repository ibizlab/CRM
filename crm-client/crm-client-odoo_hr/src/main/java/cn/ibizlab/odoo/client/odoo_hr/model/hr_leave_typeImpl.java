package cn.ibizlab.odoo.client.odoo_hr.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ihr_leave_type;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[hr_leave_type] 对象
 */
public class hr_leave_typeImpl implements Ihr_leave_type,Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 模式
     */
    public String allocation_type;

    @JsonIgnore
    public boolean allocation_typeDirtyFlag;
    
    /**
     * 会议类型
     */
    public Integer categ_id;

    @JsonIgnore
    public boolean categ_idDirtyFlag;
    
    /**
     * 会议类型
     */
    public String categ_id_text;

    @JsonIgnore
    public boolean categ_id_textDirtyFlag;
    
    /**
     * 报表中的颜色
     */
    public String color_name;

    @JsonIgnore
    public boolean color_nameDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 应用双重验证
     */
    public String double_validation;

    @JsonIgnore
    public boolean double_validationDirtyFlag;
    
    /**
     * 分配天数
     */
    public Double group_days_allocation;

    @JsonIgnore
    public boolean group_days_allocationDirtyFlag;
    
    /**
     * 集团假期
     */
    public Double group_days_leave;

    @JsonIgnore
    public boolean group_days_leaveDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 休假早已使用
     */
    public Double leaves_taken;

    @JsonIgnore
    public boolean leaves_takenDirtyFlag;
    
    /**
     * 最大允许
     */
    public Double max_leaves;

    @JsonIgnore
    public boolean max_leavesDirtyFlag;
    
    /**
     * 休假类型
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 剩余休假
     */
    public Double remaining_leaves;

    @JsonIgnore
    public boolean remaining_leavesDirtyFlag;
    
    /**
     * 休假
     */
    public String request_unit;

    @JsonIgnore
    public boolean request_unitDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 请假
     */
    public String time_type;

    @JsonIgnore
    public boolean time_typeDirtyFlag;
    
    /**
     * 没付款
     */
    public String unpaid;

    @JsonIgnore
    public boolean unpaidDirtyFlag;
    
    /**
     * 有效
     */
    public String valid;

    @JsonIgnore
    public boolean validDirtyFlag;
    
    /**
     * 验证人
     */
    public String validation_type;

    @JsonIgnore
    public boolean validation_typeDirtyFlag;
    
    /**
     * 开始日期
     */
    public Timestamp validity_start;

    @JsonIgnore
    public boolean validity_startDirtyFlag;
    
    /**
     * 结束日期
     */
    public Timestamp validity_stop;

    @JsonIgnore
    public boolean validity_stopDirtyFlag;
    
    /**
     * 虚拟剩余休假
     */
    public Double virtual_remaining_leaves;

    @JsonIgnore
    public boolean virtual_remaining_leavesDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [模式]
     */
    @JsonProperty("allocation_type")
    public String getAllocation_type(){
        return this.allocation_type ;
    }

    /**
     * 设置 [模式]
     */
    @JsonProperty("allocation_type")
    public void setAllocation_type(String  allocation_type){
        this.allocation_type = allocation_type ;
        this.allocation_typeDirtyFlag = true ;
    }

     /**
     * 获取 [模式]脏标记
     */
    @JsonIgnore
    public boolean getAllocation_typeDirtyFlag(){
        return this.allocation_typeDirtyFlag ;
    }   

    /**
     * 获取 [会议类型]
     */
    @JsonProperty("categ_id")
    public Integer getCateg_id(){
        return this.categ_id ;
    }

    /**
     * 设置 [会议类型]
     */
    @JsonProperty("categ_id")
    public void setCateg_id(Integer  categ_id){
        this.categ_id = categ_id ;
        this.categ_idDirtyFlag = true ;
    }

     /**
     * 获取 [会议类型]脏标记
     */
    @JsonIgnore
    public boolean getCateg_idDirtyFlag(){
        return this.categ_idDirtyFlag ;
    }   

    /**
     * 获取 [会议类型]
     */
    @JsonProperty("categ_id_text")
    public String getCateg_id_text(){
        return this.categ_id_text ;
    }

    /**
     * 设置 [会议类型]
     */
    @JsonProperty("categ_id_text")
    public void setCateg_id_text(String  categ_id_text){
        this.categ_id_text = categ_id_text ;
        this.categ_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [会议类型]脏标记
     */
    @JsonIgnore
    public boolean getCateg_id_textDirtyFlag(){
        return this.categ_id_textDirtyFlag ;
    }   

    /**
     * 获取 [报表中的颜色]
     */
    @JsonProperty("color_name")
    public String getColor_name(){
        return this.color_name ;
    }

    /**
     * 设置 [报表中的颜色]
     */
    @JsonProperty("color_name")
    public void setColor_name(String  color_name){
        this.color_name = color_name ;
        this.color_nameDirtyFlag = true ;
    }

     /**
     * 获取 [报表中的颜色]脏标记
     */
    @JsonIgnore
    public boolean getColor_nameDirtyFlag(){
        return this.color_nameDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [应用双重验证]
     */
    @JsonProperty("double_validation")
    public String getDouble_validation(){
        return this.double_validation ;
    }

    /**
     * 设置 [应用双重验证]
     */
    @JsonProperty("double_validation")
    public void setDouble_validation(String  double_validation){
        this.double_validation = double_validation ;
        this.double_validationDirtyFlag = true ;
    }

     /**
     * 获取 [应用双重验证]脏标记
     */
    @JsonIgnore
    public boolean getDouble_validationDirtyFlag(){
        return this.double_validationDirtyFlag ;
    }   

    /**
     * 获取 [分配天数]
     */
    @JsonProperty("group_days_allocation")
    public Double getGroup_days_allocation(){
        return this.group_days_allocation ;
    }

    /**
     * 设置 [分配天数]
     */
    @JsonProperty("group_days_allocation")
    public void setGroup_days_allocation(Double  group_days_allocation){
        this.group_days_allocation = group_days_allocation ;
        this.group_days_allocationDirtyFlag = true ;
    }

     /**
     * 获取 [分配天数]脏标记
     */
    @JsonIgnore
    public boolean getGroup_days_allocationDirtyFlag(){
        return this.group_days_allocationDirtyFlag ;
    }   

    /**
     * 获取 [集团假期]
     */
    @JsonProperty("group_days_leave")
    public Double getGroup_days_leave(){
        return this.group_days_leave ;
    }

    /**
     * 设置 [集团假期]
     */
    @JsonProperty("group_days_leave")
    public void setGroup_days_leave(Double  group_days_leave){
        this.group_days_leave = group_days_leave ;
        this.group_days_leaveDirtyFlag = true ;
    }

     /**
     * 获取 [集团假期]脏标记
     */
    @JsonIgnore
    public boolean getGroup_days_leaveDirtyFlag(){
        return this.group_days_leaveDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [休假早已使用]
     */
    @JsonProperty("leaves_taken")
    public Double getLeaves_taken(){
        return this.leaves_taken ;
    }

    /**
     * 设置 [休假早已使用]
     */
    @JsonProperty("leaves_taken")
    public void setLeaves_taken(Double  leaves_taken){
        this.leaves_taken = leaves_taken ;
        this.leaves_takenDirtyFlag = true ;
    }

     /**
     * 获取 [休假早已使用]脏标记
     */
    @JsonIgnore
    public boolean getLeaves_takenDirtyFlag(){
        return this.leaves_takenDirtyFlag ;
    }   

    /**
     * 获取 [最大允许]
     */
    @JsonProperty("max_leaves")
    public Double getMax_leaves(){
        return this.max_leaves ;
    }

    /**
     * 设置 [最大允许]
     */
    @JsonProperty("max_leaves")
    public void setMax_leaves(Double  max_leaves){
        this.max_leaves = max_leaves ;
        this.max_leavesDirtyFlag = true ;
    }

     /**
     * 获取 [最大允许]脏标记
     */
    @JsonIgnore
    public boolean getMax_leavesDirtyFlag(){
        return this.max_leavesDirtyFlag ;
    }   

    /**
     * 获取 [休假类型]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [休假类型]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [休假类型]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [剩余休假]
     */
    @JsonProperty("remaining_leaves")
    public Double getRemaining_leaves(){
        return this.remaining_leaves ;
    }

    /**
     * 设置 [剩余休假]
     */
    @JsonProperty("remaining_leaves")
    public void setRemaining_leaves(Double  remaining_leaves){
        this.remaining_leaves = remaining_leaves ;
        this.remaining_leavesDirtyFlag = true ;
    }

     /**
     * 获取 [剩余休假]脏标记
     */
    @JsonIgnore
    public boolean getRemaining_leavesDirtyFlag(){
        return this.remaining_leavesDirtyFlag ;
    }   

    /**
     * 获取 [休假]
     */
    @JsonProperty("request_unit")
    public String getRequest_unit(){
        return this.request_unit ;
    }

    /**
     * 设置 [休假]
     */
    @JsonProperty("request_unit")
    public void setRequest_unit(String  request_unit){
        this.request_unit = request_unit ;
        this.request_unitDirtyFlag = true ;
    }

     /**
     * 获取 [休假]脏标记
     */
    @JsonIgnore
    public boolean getRequest_unitDirtyFlag(){
        return this.request_unitDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [请假]
     */
    @JsonProperty("time_type")
    public String getTime_type(){
        return this.time_type ;
    }

    /**
     * 设置 [请假]
     */
    @JsonProperty("time_type")
    public void setTime_type(String  time_type){
        this.time_type = time_type ;
        this.time_typeDirtyFlag = true ;
    }

     /**
     * 获取 [请假]脏标记
     */
    @JsonIgnore
    public boolean getTime_typeDirtyFlag(){
        return this.time_typeDirtyFlag ;
    }   

    /**
     * 获取 [没付款]
     */
    @JsonProperty("unpaid")
    public String getUnpaid(){
        return this.unpaid ;
    }

    /**
     * 设置 [没付款]
     */
    @JsonProperty("unpaid")
    public void setUnpaid(String  unpaid){
        this.unpaid = unpaid ;
        this.unpaidDirtyFlag = true ;
    }

     /**
     * 获取 [没付款]脏标记
     */
    @JsonIgnore
    public boolean getUnpaidDirtyFlag(){
        return this.unpaidDirtyFlag ;
    }   

    /**
     * 获取 [有效]
     */
    @JsonProperty("valid")
    public String getValid(){
        return this.valid ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("valid")
    public void setValid(String  valid){
        this.valid = valid ;
        this.validDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getValidDirtyFlag(){
        return this.validDirtyFlag ;
    }   

    /**
     * 获取 [验证人]
     */
    @JsonProperty("validation_type")
    public String getValidation_type(){
        return this.validation_type ;
    }

    /**
     * 设置 [验证人]
     */
    @JsonProperty("validation_type")
    public void setValidation_type(String  validation_type){
        this.validation_type = validation_type ;
        this.validation_typeDirtyFlag = true ;
    }

     /**
     * 获取 [验证人]脏标记
     */
    @JsonIgnore
    public boolean getValidation_typeDirtyFlag(){
        return this.validation_typeDirtyFlag ;
    }   

    /**
     * 获取 [开始日期]
     */
    @JsonProperty("validity_start")
    public Timestamp getValidity_start(){
        return this.validity_start ;
    }

    /**
     * 设置 [开始日期]
     */
    @JsonProperty("validity_start")
    public void setValidity_start(Timestamp  validity_start){
        this.validity_start = validity_start ;
        this.validity_startDirtyFlag = true ;
    }

     /**
     * 获取 [开始日期]脏标记
     */
    @JsonIgnore
    public boolean getValidity_startDirtyFlag(){
        return this.validity_startDirtyFlag ;
    }   

    /**
     * 获取 [结束日期]
     */
    @JsonProperty("validity_stop")
    public Timestamp getValidity_stop(){
        return this.validity_stop ;
    }

    /**
     * 设置 [结束日期]
     */
    @JsonProperty("validity_stop")
    public void setValidity_stop(Timestamp  validity_stop){
        this.validity_stop = validity_stop ;
        this.validity_stopDirtyFlag = true ;
    }

     /**
     * 获取 [结束日期]脏标记
     */
    @JsonIgnore
    public boolean getValidity_stopDirtyFlag(){
        return this.validity_stopDirtyFlag ;
    }   

    /**
     * 获取 [虚拟剩余休假]
     */
    @JsonProperty("virtual_remaining_leaves")
    public Double getVirtual_remaining_leaves(){
        return this.virtual_remaining_leaves ;
    }

    /**
     * 设置 [虚拟剩余休假]
     */
    @JsonProperty("virtual_remaining_leaves")
    public void setVirtual_remaining_leaves(Double  virtual_remaining_leaves){
        this.virtual_remaining_leaves = virtual_remaining_leaves ;
        this.virtual_remaining_leavesDirtyFlag = true ;
    }

     /**
     * 获取 [虚拟剩余休假]脏标记
     */
    @JsonIgnore
    public boolean getVirtual_remaining_leavesDirtyFlag(){
        return this.virtual_remaining_leavesDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
