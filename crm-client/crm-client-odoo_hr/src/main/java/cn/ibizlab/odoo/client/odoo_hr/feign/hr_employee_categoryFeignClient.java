package cn.ibizlab.odoo.client.odoo_hr.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ihr_employee_category;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_employee_categoryImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[hr_employee_category] 服务对象接口
 */
public interface hr_employee_categoryFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_employee_categories/fetchdefault")
    public Page<hr_employee_categoryImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_employee_categories/updatebatch")
    public hr_employee_categoryImpl updateBatch(@RequestBody List<hr_employee_categoryImpl> hr_employee_categories);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_employee_categories/{id}")
    public hr_employee_categoryImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_employee_categories/createbatch")
    public hr_employee_categoryImpl createBatch(@RequestBody List<hr_employee_categoryImpl> hr_employee_categories);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_employee_categories/{id}")
    public hr_employee_categoryImpl update(@PathVariable("id") Integer id,@RequestBody hr_employee_categoryImpl hr_employee_category);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_employee_categories/removebatch")
    public hr_employee_categoryImpl removeBatch(@RequestBody List<hr_employee_categoryImpl> hr_employee_categories);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_employee_categories")
    public hr_employee_categoryImpl create(@RequestBody hr_employee_categoryImpl hr_employee_category);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_employee_categories/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_employee_categories/select")
    public Page<hr_employee_categoryImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_employee_categories/{id}/getdraft")
    public hr_employee_categoryImpl getDraft(@PathVariable("id") Integer id,@RequestBody hr_employee_categoryImpl hr_employee_category);



}
