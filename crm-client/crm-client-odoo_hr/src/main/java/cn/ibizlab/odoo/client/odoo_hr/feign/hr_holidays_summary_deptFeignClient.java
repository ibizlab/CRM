package cn.ibizlab.odoo.client.odoo_hr.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ihr_holidays_summary_dept;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_holidays_summary_deptImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[hr_holidays_summary_dept] 服务对象接口
 */
public interface hr_holidays_summary_deptFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_holidays_summary_depts/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_holidays_summary_depts/fetchdefault")
    public Page<hr_holidays_summary_deptImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_holidays_summary_depts/{id}")
    public hr_holidays_summary_deptImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_holidays_summary_depts/createbatch")
    public hr_holidays_summary_deptImpl createBatch(@RequestBody List<hr_holidays_summary_deptImpl> hr_holidays_summary_depts);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_holidays_summary_depts")
    public hr_holidays_summary_deptImpl create(@RequestBody hr_holidays_summary_deptImpl hr_holidays_summary_dept);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_holidays_summary_depts/removebatch")
    public hr_holidays_summary_deptImpl removeBatch(@RequestBody List<hr_holidays_summary_deptImpl> hr_holidays_summary_depts);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_holidays_summary_depts/{id}")
    public hr_holidays_summary_deptImpl update(@PathVariable("id") Integer id,@RequestBody hr_holidays_summary_deptImpl hr_holidays_summary_dept);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_holidays_summary_depts/updatebatch")
    public hr_holidays_summary_deptImpl updateBatch(@RequestBody List<hr_holidays_summary_deptImpl> hr_holidays_summary_depts);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_holidays_summary_depts/select")
    public Page<hr_holidays_summary_deptImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_holidays_summary_depts/{id}/getdraft")
    public hr_holidays_summary_deptImpl getDraft(@PathVariable("id") Integer id,@RequestBody hr_holidays_summary_deptImpl hr_holidays_summary_dept);



}
