package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_recruitment_source;
import cn.ibizlab.odoo.client.odoo_hr.config.odoo_hrClientProperties;
import cn.ibizlab.odoo.core.client.service.Ihr_recruitment_sourceClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_recruitment_sourceImpl;
import cn.ibizlab.odoo.client.odoo_hr.feign.hr_recruitment_sourceFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[hr_recruitment_source] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class hr_recruitment_sourceClientServiceImpl implements Ihr_recruitment_sourceClientService {

    hr_recruitment_sourceFeignClient hr_recruitment_sourceFeignClient;

    @Autowired
    public hr_recruitment_sourceClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_hrClientProperties odoo_hrClientProperties) {
        if (odoo_hrClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_recruitment_sourceFeignClient = nameBuilder.target(hr_recruitment_sourceFeignClient.class,"http://"+odoo_hrClientProperties.getServiceId()+"/") ;
		}else if (odoo_hrClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_recruitment_sourceFeignClient = nameBuilder.target(hr_recruitment_sourceFeignClient.class,odoo_hrClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ihr_recruitment_source createModel() {
		return new hr_recruitment_sourceImpl();
	}


    public Page<Ihr_recruitment_source> fetchDefault(SearchContext context){
        Page<hr_recruitment_sourceImpl> page = this.hr_recruitment_sourceFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Ihr_recruitment_source> hr_recruitment_sources){
        if(hr_recruitment_sources!=null){
            List<hr_recruitment_sourceImpl> list = new ArrayList<hr_recruitment_sourceImpl>();
            for(Ihr_recruitment_source ihr_recruitment_source :hr_recruitment_sources){
                list.add((hr_recruitment_sourceImpl)ihr_recruitment_source) ;
            }
            hr_recruitment_sourceFeignClient.createBatch(list) ;
        }
    }


    public void get(Ihr_recruitment_source hr_recruitment_source){
        Ihr_recruitment_source clientModel = hr_recruitment_sourceFeignClient.get(hr_recruitment_source.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_recruitment_source.getClass(), false);
        copier.copy(clientModel, hr_recruitment_source, null);
    }


    public void update(Ihr_recruitment_source hr_recruitment_source){
        Ihr_recruitment_source clientModel = hr_recruitment_sourceFeignClient.update(hr_recruitment_source.getId(),(hr_recruitment_sourceImpl)hr_recruitment_source) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_recruitment_source.getClass(), false);
        copier.copy(clientModel, hr_recruitment_source, null);
    }


    public void remove(Ihr_recruitment_source hr_recruitment_source){
        hr_recruitment_sourceFeignClient.remove(hr_recruitment_source.getId()) ;
    }


    public void updateBatch(List<Ihr_recruitment_source> hr_recruitment_sources){
        if(hr_recruitment_sources!=null){
            List<hr_recruitment_sourceImpl> list = new ArrayList<hr_recruitment_sourceImpl>();
            for(Ihr_recruitment_source ihr_recruitment_source :hr_recruitment_sources){
                list.add((hr_recruitment_sourceImpl)ihr_recruitment_source) ;
            }
            hr_recruitment_sourceFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Ihr_recruitment_source> hr_recruitment_sources){
        if(hr_recruitment_sources!=null){
            List<hr_recruitment_sourceImpl> list = new ArrayList<hr_recruitment_sourceImpl>();
            for(Ihr_recruitment_source ihr_recruitment_source :hr_recruitment_sources){
                list.add((hr_recruitment_sourceImpl)ihr_recruitment_source) ;
            }
            hr_recruitment_sourceFeignClient.removeBatch(list) ;
        }
    }


    public void create(Ihr_recruitment_source hr_recruitment_source){
        Ihr_recruitment_source clientModel = hr_recruitment_sourceFeignClient.create((hr_recruitment_sourceImpl)hr_recruitment_source) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_recruitment_source.getClass(), false);
        copier.copy(clientModel, hr_recruitment_source, null);
    }


    public Page<Ihr_recruitment_source> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ihr_recruitment_source hr_recruitment_source){
        Ihr_recruitment_source clientModel = hr_recruitment_sourceFeignClient.getDraft(hr_recruitment_source.getId(),(hr_recruitment_sourceImpl)hr_recruitment_source) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_recruitment_source.getClass(), false);
        copier.copy(clientModel, hr_recruitment_source, null);
    }



}

