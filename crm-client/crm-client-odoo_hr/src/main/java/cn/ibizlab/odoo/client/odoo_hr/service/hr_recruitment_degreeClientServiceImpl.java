package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_recruitment_degree;
import cn.ibizlab.odoo.client.odoo_hr.config.odoo_hrClientProperties;
import cn.ibizlab.odoo.core.client.service.Ihr_recruitment_degreeClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_recruitment_degreeImpl;
import cn.ibizlab.odoo.client.odoo_hr.feign.hr_recruitment_degreeFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[hr_recruitment_degree] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class hr_recruitment_degreeClientServiceImpl implements Ihr_recruitment_degreeClientService {

    hr_recruitment_degreeFeignClient hr_recruitment_degreeFeignClient;

    @Autowired
    public hr_recruitment_degreeClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_hrClientProperties odoo_hrClientProperties) {
        if (odoo_hrClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_recruitment_degreeFeignClient = nameBuilder.target(hr_recruitment_degreeFeignClient.class,"http://"+odoo_hrClientProperties.getServiceId()+"/") ;
		}else if (odoo_hrClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_recruitment_degreeFeignClient = nameBuilder.target(hr_recruitment_degreeFeignClient.class,odoo_hrClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ihr_recruitment_degree createModel() {
		return new hr_recruitment_degreeImpl();
	}


    public Page<Ihr_recruitment_degree> fetchDefault(SearchContext context){
        Page<hr_recruitment_degreeImpl> page = this.hr_recruitment_degreeFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Ihr_recruitment_degree hr_recruitment_degree){
        Ihr_recruitment_degree clientModel = hr_recruitment_degreeFeignClient.create((hr_recruitment_degreeImpl)hr_recruitment_degree) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_recruitment_degree.getClass(), false);
        copier.copy(clientModel, hr_recruitment_degree, null);
    }


    public void updateBatch(List<Ihr_recruitment_degree> hr_recruitment_degrees){
        if(hr_recruitment_degrees!=null){
            List<hr_recruitment_degreeImpl> list = new ArrayList<hr_recruitment_degreeImpl>();
            for(Ihr_recruitment_degree ihr_recruitment_degree :hr_recruitment_degrees){
                list.add((hr_recruitment_degreeImpl)ihr_recruitment_degree) ;
            }
            hr_recruitment_degreeFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Ihr_recruitment_degree> hr_recruitment_degrees){
        if(hr_recruitment_degrees!=null){
            List<hr_recruitment_degreeImpl> list = new ArrayList<hr_recruitment_degreeImpl>();
            for(Ihr_recruitment_degree ihr_recruitment_degree :hr_recruitment_degrees){
                list.add((hr_recruitment_degreeImpl)ihr_recruitment_degree) ;
            }
            hr_recruitment_degreeFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Ihr_recruitment_degree hr_recruitment_degree){
        hr_recruitment_degreeFeignClient.remove(hr_recruitment_degree.getId()) ;
    }


    public void update(Ihr_recruitment_degree hr_recruitment_degree){
        Ihr_recruitment_degree clientModel = hr_recruitment_degreeFeignClient.update(hr_recruitment_degree.getId(),(hr_recruitment_degreeImpl)hr_recruitment_degree) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_recruitment_degree.getClass(), false);
        copier.copy(clientModel, hr_recruitment_degree, null);
    }


    public void get(Ihr_recruitment_degree hr_recruitment_degree){
        Ihr_recruitment_degree clientModel = hr_recruitment_degreeFeignClient.get(hr_recruitment_degree.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_recruitment_degree.getClass(), false);
        copier.copy(clientModel, hr_recruitment_degree, null);
    }


    public void createBatch(List<Ihr_recruitment_degree> hr_recruitment_degrees){
        if(hr_recruitment_degrees!=null){
            List<hr_recruitment_degreeImpl> list = new ArrayList<hr_recruitment_degreeImpl>();
            for(Ihr_recruitment_degree ihr_recruitment_degree :hr_recruitment_degrees){
                list.add((hr_recruitment_degreeImpl)ihr_recruitment_degree) ;
            }
            hr_recruitment_degreeFeignClient.createBatch(list) ;
        }
    }


    public Page<Ihr_recruitment_degree> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ihr_recruitment_degree hr_recruitment_degree){
        Ihr_recruitment_degree clientModel = hr_recruitment_degreeFeignClient.getDraft(hr_recruitment_degree.getId(),(hr_recruitment_degreeImpl)hr_recruitment_degree) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_recruitment_degree.getClass(), false);
        copier.copy(clientModel, hr_recruitment_degree, null);
    }



}

