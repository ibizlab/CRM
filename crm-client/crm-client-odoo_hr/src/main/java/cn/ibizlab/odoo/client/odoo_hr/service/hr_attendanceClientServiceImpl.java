package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_attendance;
import cn.ibizlab.odoo.client.odoo_hr.config.odoo_hrClientProperties;
import cn.ibizlab.odoo.core.client.service.Ihr_attendanceClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_attendanceImpl;
import cn.ibizlab.odoo.client.odoo_hr.feign.hr_attendanceFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[hr_attendance] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class hr_attendanceClientServiceImpl implements Ihr_attendanceClientService {

    hr_attendanceFeignClient hr_attendanceFeignClient;

    @Autowired
    public hr_attendanceClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_hrClientProperties odoo_hrClientProperties) {
        if (odoo_hrClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_attendanceFeignClient = nameBuilder.target(hr_attendanceFeignClient.class,"http://"+odoo_hrClientProperties.getServiceId()+"/") ;
		}else if (odoo_hrClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_attendanceFeignClient = nameBuilder.target(hr_attendanceFeignClient.class,odoo_hrClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ihr_attendance createModel() {
		return new hr_attendanceImpl();
	}


    public void update(Ihr_attendance hr_attendance){
        Ihr_attendance clientModel = hr_attendanceFeignClient.update(hr_attendance.getId(),(hr_attendanceImpl)hr_attendance) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_attendance.getClass(), false);
        copier.copy(clientModel, hr_attendance, null);
    }


    public void createBatch(List<Ihr_attendance> hr_attendances){
        if(hr_attendances!=null){
            List<hr_attendanceImpl> list = new ArrayList<hr_attendanceImpl>();
            for(Ihr_attendance ihr_attendance :hr_attendances){
                list.add((hr_attendanceImpl)ihr_attendance) ;
            }
            hr_attendanceFeignClient.createBatch(list) ;
        }
    }


    public void create(Ihr_attendance hr_attendance){
        Ihr_attendance clientModel = hr_attendanceFeignClient.create((hr_attendanceImpl)hr_attendance) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_attendance.getClass(), false);
        copier.copy(clientModel, hr_attendance, null);
    }


    public void remove(Ihr_attendance hr_attendance){
        hr_attendanceFeignClient.remove(hr_attendance.getId()) ;
    }


    public void updateBatch(List<Ihr_attendance> hr_attendances){
        if(hr_attendances!=null){
            List<hr_attendanceImpl> list = new ArrayList<hr_attendanceImpl>();
            for(Ihr_attendance ihr_attendance :hr_attendances){
                list.add((hr_attendanceImpl)ihr_attendance) ;
            }
            hr_attendanceFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Ihr_attendance> hr_attendances){
        if(hr_attendances!=null){
            List<hr_attendanceImpl> list = new ArrayList<hr_attendanceImpl>();
            for(Ihr_attendance ihr_attendance :hr_attendances){
                list.add((hr_attendanceImpl)ihr_attendance) ;
            }
            hr_attendanceFeignClient.removeBatch(list) ;
        }
    }


    public void get(Ihr_attendance hr_attendance){
        Ihr_attendance clientModel = hr_attendanceFeignClient.get(hr_attendance.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_attendance.getClass(), false);
        copier.copy(clientModel, hr_attendance, null);
    }


    public Page<Ihr_attendance> fetchDefault(SearchContext context){
        Page<hr_attendanceImpl> page = this.hr_attendanceFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Ihr_attendance> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ihr_attendance hr_attendance){
        Ihr_attendance clientModel = hr_attendanceFeignClient.getDraft(hr_attendance.getId(),(hr_attendanceImpl)hr_attendance) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_attendance.getClass(), false);
        copier.copy(clientModel, hr_attendance, null);
    }



}

