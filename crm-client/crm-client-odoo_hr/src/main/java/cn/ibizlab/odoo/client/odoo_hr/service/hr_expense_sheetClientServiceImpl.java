package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_expense_sheet;
import cn.ibizlab.odoo.client.odoo_hr.config.odoo_hrClientProperties;
import cn.ibizlab.odoo.core.client.service.Ihr_expense_sheetClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_expense_sheetImpl;
import cn.ibizlab.odoo.client.odoo_hr.feign.hr_expense_sheetFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[hr_expense_sheet] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class hr_expense_sheetClientServiceImpl implements Ihr_expense_sheetClientService {

    hr_expense_sheetFeignClient hr_expense_sheetFeignClient;

    @Autowired
    public hr_expense_sheetClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_hrClientProperties odoo_hrClientProperties) {
        if (odoo_hrClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_expense_sheetFeignClient = nameBuilder.target(hr_expense_sheetFeignClient.class,"http://"+odoo_hrClientProperties.getServiceId()+"/") ;
		}else if (odoo_hrClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.hr_expense_sheetFeignClient = nameBuilder.target(hr_expense_sheetFeignClient.class,odoo_hrClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ihr_expense_sheet createModel() {
		return new hr_expense_sheetImpl();
	}


    public void create(Ihr_expense_sheet hr_expense_sheet){
        Ihr_expense_sheet clientModel = hr_expense_sheetFeignClient.create((hr_expense_sheetImpl)hr_expense_sheet) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_expense_sheet.getClass(), false);
        copier.copy(clientModel, hr_expense_sheet, null);
    }


    public void createBatch(List<Ihr_expense_sheet> hr_expense_sheets){
        if(hr_expense_sheets!=null){
            List<hr_expense_sheetImpl> list = new ArrayList<hr_expense_sheetImpl>();
            for(Ihr_expense_sheet ihr_expense_sheet :hr_expense_sheets){
                list.add((hr_expense_sheetImpl)ihr_expense_sheet) ;
            }
            hr_expense_sheetFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Ihr_expense_sheet> hr_expense_sheets){
        if(hr_expense_sheets!=null){
            List<hr_expense_sheetImpl> list = new ArrayList<hr_expense_sheetImpl>();
            for(Ihr_expense_sheet ihr_expense_sheet :hr_expense_sheets){
                list.add((hr_expense_sheetImpl)ihr_expense_sheet) ;
            }
            hr_expense_sheetFeignClient.updateBatch(list) ;
        }
    }


    public void update(Ihr_expense_sheet hr_expense_sheet){
        Ihr_expense_sheet clientModel = hr_expense_sheetFeignClient.update(hr_expense_sheet.getId(),(hr_expense_sheetImpl)hr_expense_sheet) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_expense_sheet.getClass(), false);
        copier.copy(clientModel, hr_expense_sheet, null);
    }


    public Page<Ihr_expense_sheet> fetchDefault(SearchContext context){
        Page<hr_expense_sheetImpl> page = this.hr_expense_sheetFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Ihr_expense_sheet hr_expense_sheet){
        Ihr_expense_sheet clientModel = hr_expense_sheetFeignClient.get(hr_expense_sheet.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_expense_sheet.getClass(), false);
        copier.copy(clientModel, hr_expense_sheet, null);
    }


    public void remove(Ihr_expense_sheet hr_expense_sheet){
        hr_expense_sheetFeignClient.remove(hr_expense_sheet.getId()) ;
    }


    public void removeBatch(List<Ihr_expense_sheet> hr_expense_sheets){
        if(hr_expense_sheets!=null){
            List<hr_expense_sheetImpl> list = new ArrayList<hr_expense_sheetImpl>();
            for(Ihr_expense_sheet ihr_expense_sheet :hr_expense_sheets){
                list.add((hr_expense_sheetImpl)ihr_expense_sheet) ;
            }
            hr_expense_sheetFeignClient.removeBatch(list) ;
        }
    }


    public Page<Ihr_expense_sheet> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ihr_expense_sheet hr_expense_sheet){
        Ihr_expense_sheet clientModel = hr_expense_sheetFeignClient.getDraft(hr_expense_sheet.getId(),(hr_expense_sheetImpl)hr_expense_sheet) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), hr_expense_sheet.getClass(), false);
        copier.copy(clientModel, hr_expense_sheet, null);
    }



}

