package cn.ibizlab.odoo.client.odoo_web_tour.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iweb_tour_tour;
import cn.ibizlab.odoo.client.odoo_web_tour.model.web_tour_tourImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[web_tour_tour] 服务对象接口
 */
public interface web_tour_tourFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_web_tour/web_tour_tours/createbatch")
    public web_tour_tourImpl createBatch(@RequestBody List<web_tour_tourImpl> web_tour_tours);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_web_tour/web_tour_tours/{id}")
    public web_tour_tourImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_web_tour/web_tour_tours/removebatch")
    public web_tour_tourImpl removeBatch(@RequestBody List<web_tour_tourImpl> web_tour_tours);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_web_tour/web_tour_tours/updatebatch")
    public web_tour_tourImpl updateBatch(@RequestBody List<web_tour_tourImpl> web_tour_tours);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_web_tour/web_tour_tours/{id}")
    public web_tour_tourImpl update(@PathVariable("id") Integer id,@RequestBody web_tour_tourImpl web_tour_tour);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_web_tour/web_tour_tours/fetchdefault")
    public Page<web_tour_tourImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_web_tour/web_tour_tours/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_web_tour/web_tour_tours")
    public web_tour_tourImpl create(@RequestBody web_tour_tourImpl web_tour_tour);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_web_tour/web_tour_tours/select")
    public Page<web_tour_tourImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_web_tour/web_tour_tours/{id}/getdraft")
    public web_tour_tourImpl getDraft(@PathVariable("id") Integer id,@RequestBody web_tour_tourImpl web_tour_tour);



}
