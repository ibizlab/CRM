package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ires_partner;
import cn.ibizlab.odoo.client.odoo_base.model.res_partnerImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[res_partner] 服务对象接口
 */
public interface res_partnerFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partners/{id}")
    public res_partnerImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_partners/{id}")
    public res_partnerImpl update(@PathVariable("id") Integer id,@RequestBody res_partnerImpl res_partner);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_partners")
    public res_partnerImpl create(@RequestBody res_partnerImpl res_partner);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_partners/createbatch")
    public res_partnerImpl createBatch(@RequestBody List<res_partnerImpl> res_partners);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_partners/removebatch")
    public res_partnerImpl removeBatch(@RequestBody List<res_partnerImpl> res_partners);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_partners/updatebatch")
    public res_partnerImpl updateBatch(@RequestBody List<res_partnerImpl> res_partners);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_partners/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partners/fetchdefault")
    public Page<res_partnerImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partners/select")
    public Page<res_partnerImpl> select();


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_partners/{id}/checkkey")
    public res_partnerImpl checkKey(@PathVariable("id") Integer id,@RequestBody res_partnerImpl res_partner);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_partners/{id}/save")
    public res_partnerImpl save(@PathVariable("id") Integer id,@RequestBody res_partnerImpl res_partner);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partners/{id}/getdraft")
    public res_partnerImpl getDraft(@PathVariable("id") Integer id,@RequestBody res_partnerImpl res_partner);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partners/fetchcontacts")
    public Page<res_partnerImpl> fetchContacts(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partners/fetchcompany")
    public Page<res_partnerImpl> fetchCompany(SearchContext context);



}
