package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_language_import;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibase_language_importClientService;
import cn.ibizlab.odoo.client.odoo_base.model.base_language_importImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.base_language_importFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[base_language_import] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class base_language_importClientServiceImpl implements Ibase_language_importClientService {

    base_language_importFeignClient base_language_importFeignClient;

    @Autowired
    public base_language_importClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_language_importFeignClient = nameBuilder.target(base_language_importFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_language_importFeignClient = nameBuilder.target(base_language_importFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibase_language_import createModel() {
		return new base_language_importImpl();
	}


    public void removeBatch(List<Ibase_language_import> base_language_imports){
        if(base_language_imports!=null){
            List<base_language_importImpl> list = new ArrayList<base_language_importImpl>();
            for(Ibase_language_import ibase_language_import :base_language_imports){
                list.add((base_language_importImpl)ibase_language_import) ;
            }
            base_language_importFeignClient.removeBatch(list) ;
        }
    }


    public void create(Ibase_language_import base_language_import){
        Ibase_language_import clientModel = base_language_importFeignClient.create((base_language_importImpl)base_language_import) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_language_import.getClass(), false);
        copier.copy(clientModel, base_language_import, null);
    }


    public void update(Ibase_language_import base_language_import){
        Ibase_language_import clientModel = base_language_importFeignClient.update(base_language_import.getId(),(base_language_importImpl)base_language_import) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_language_import.getClass(), false);
        copier.copy(clientModel, base_language_import, null);
    }


    public void remove(Ibase_language_import base_language_import){
        base_language_importFeignClient.remove(base_language_import.getId()) ;
    }


    public void get(Ibase_language_import base_language_import){
        Ibase_language_import clientModel = base_language_importFeignClient.get(base_language_import.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_language_import.getClass(), false);
        copier.copy(clientModel, base_language_import, null);
    }


    public void createBatch(List<Ibase_language_import> base_language_imports){
        if(base_language_imports!=null){
            List<base_language_importImpl> list = new ArrayList<base_language_importImpl>();
            for(Ibase_language_import ibase_language_import :base_language_imports){
                list.add((base_language_importImpl)ibase_language_import) ;
            }
            base_language_importFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Ibase_language_import> base_language_imports){
        if(base_language_imports!=null){
            List<base_language_importImpl> list = new ArrayList<base_language_importImpl>();
            for(Ibase_language_import ibase_language_import :base_language_imports){
                list.add((base_language_importImpl)ibase_language_import) ;
            }
            base_language_importFeignClient.updateBatch(list) ;
        }
    }


    public Page<Ibase_language_import> fetchDefault(SearchContext context){
        Page<base_language_importImpl> page = this.base_language_importFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Ibase_language_import> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibase_language_import base_language_import){
        Ibase_language_import clientModel = base_language_importFeignClient.getDraft(base_language_import.getId(),(base_language_importImpl)base_language_import) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_language_import.getClass(), false);
        copier.copy(clientModel, base_language_import, null);
    }



}

