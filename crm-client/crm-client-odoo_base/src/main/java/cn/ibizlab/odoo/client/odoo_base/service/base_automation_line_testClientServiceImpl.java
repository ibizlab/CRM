package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_automation_line_test;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibase_automation_line_testClientService;
import cn.ibizlab.odoo.client.odoo_base.model.base_automation_line_testImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.base_automation_line_testFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[base_automation_line_test] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class base_automation_line_testClientServiceImpl implements Ibase_automation_line_testClientService {

    base_automation_line_testFeignClient base_automation_line_testFeignClient;

    @Autowired
    public base_automation_line_testClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_automation_line_testFeignClient = nameBuilder.target(base_automation_line_testFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_automation_line_testFeignClient = nameBuilder.target(base_automation_line_testFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibase_automation_line_test createModel() {
		return new base_automation_line_testImpl();
	}


    public void removeBatch(List<Ibase_automation_line_test> base_automation_line_tests){
        if(base_automation_line_tests!=null){
            List<base_automation_line_testImpl> list = new ArrayList<base_automation_line_testImpl>();
            for(Ibase_automation_line_test ibase_automation_line_test :base_automation_line_tests){
                list.add((base_automation_line_testImpl)ibase_automation_line_test) ;
            }
            base_automation_line_testFeignClient.removeBatch(list) ;
        }
    }


    public Page<Ibase_automation_line_test> fetchDefault(SearchContext context){
        Page<base_automation_line_testImpl> page = this.base_automation_line_testFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Ibase_automation_line_test> base_automation_line_tests){
        if(base_automation_line_tests!=null){
            List<base_automation_line_testImpl> list = new ArrayList<base_automation_line_testImpl>();
            for(Ibase_automation_line_test ibase_automation_line_test :base_automation_line_tests){
                list.add((base_automation_line_testImpl)ibase_automation_line_test) ;
            }
            base_automation_line_testFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Ibase_automation_line_test base_automation_line_test){
        base_automation_line_testFeignClient.remove(base_automation_line_test.getId()) ;
    }


    public void createBatch(List<Ibase_automation_line_test> base_automation_line_tests){
        if(base_automation_line_tests!=null){
            List<base_automation_line_testImpl> list = new ArrayList<base_automation_line_testImpl>();
            for(Ibase_automation_line_test ibase_automation_line_test :base_automation_line_tests){
                list.add((base_automation_line_testImpl)ibase_automation_line_test) ;
            }
            base_automation_line_testFeignClient.createBatch(list) ;
        }
    }


    public void get(Ibase_automation_line_test base_automation_line_test){
        Ibase_automation_line_test clientModel = base_automation_line_testFeignClient.get(base_automation_line_test.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_automation_line_test.getClass(), false);
        copier.copy(clientModel, base_automation_line_test, null);
    }


    public void create(Ibase_automation_line_test base_automation_line_test){
        Ibase_automation_line_test clientModel = base_automation_line_testFeignClient.create((base_automation_line_testImpl)base_automation_line_test) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_automation_line_test.getClass(), false);
        copier.copy(clientModel, base_automation_line_test, null);
    }


    public void update(Ibase_automation_line_test base_automation_line_test){
        Ibase_automation_line_test clientModel = base_automation_line_testFeignClient.update(base_automation_line_test.getId(),(base_automation_line_testImpl)base_automation_line_test) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_automation_line_test.getClass(), false);
        copier.copy(clientModel, base_automation_line_test, null);
    }


    public Page<Ibase_automation_line_test> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibase_automation_line_test base_automation_line_test){
        Ibase_automation_line_test clientModel = base_automation_line_testFeignClient.getDraft(base_automation_line_test.getId(),(base_automation_line_testImpl)base_automation_line_test) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_automation_line_test.getClass(), false);
        copier.copy(clientModel, base_automation_line_test, null);
    }



}

