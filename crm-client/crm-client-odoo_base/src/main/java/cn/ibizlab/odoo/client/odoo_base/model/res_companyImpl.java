package cn.ibizlab.odoo.client.odoo_base.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ires_company;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[res_company] 对象
 */
public class res_companyImpl implements Ires_company,Serializable{

    /**
     * 银行核销阈值
     */
    public Timestamp account_bank_reconciliation_start;

    @JsonIgnore
    public boolean account_bank_reconciliation_startDirtyFlag;
    
    /**
     * 处于会计面板的状态
     */
    public String account_dashboard_onboarding_state;

    @JsonIgnore
    public boolean account_dashboard_onboarding_stateDirtyFlag;
    
    /**
     * 处于会计发票面板的状态
     */
    public String account_invoice_onboarding_state;

    @JsonIgnore
    public boolean account_invoice_onboarding_stateDirtyFlag;
    
    /**
     * 科目号码
     */
    public String account_no;

    @JsonIgnore
    public boolean account_noDirtyFlag;
    
    /**
     * 有待被确认的发票步骤的状态
     */
    public String account_onboarding_invoice_layout_state;

    @JsonIgnore
    public boolean account_onboarding_invoice_layout_stateDirtyFlag;
    
    /**
     * 有待被确认的报价单步骤的状态
     */
    public String account_onboarding_sale_tax_state;

    @JsonIgnore
    public boolean account_onboarding_sale_tax_stateDirtyFlag;
    
    /**
     * 有待被确认的样品报价单步骤的状态
     */
    public String account_onboarding_sample_invoice_state;

    @JsonIgnore
    public boolean account_onboarding_sample_invoice_stateDirtyFlag;
    
    /**
     * 期初日期
     */
    public Timestamp account_opening_date;

    @JsonIgnore
    public boolean account_opening_dateDirtyFlag;
    
    /**
     * 期初日记账
     */
    public Integer account_opening_journal_id;

    @JsonIgnore
    public boolean account_opening_journal_idDirtyFlag;
    
    /**
     * 期初日记账分录
     */
    public Integer account_opening_move_id;

    @JsonIgnore
    public boolean account_opening_move_idDirtyFlag;
    
    /**
     * 期初日记账分录
     */
    public String account_opening_move_id_text;

    @JsonIgnore
    public boolean account_opening_move_id_textDirtyFlag;
    
    /**
     * 默认进项税
     */
    public Integer account_purchase_tax_id;

    @JsonIgnore
    public boolean account_purchase_tax_idDirtyFlag;
    
    /**
     * 默认进项税
     */
    public String account_purchase_tax_id_text;

    @JsonIgnore
    public boolean account_purchase_tax_id_textDirtyFlag;
    
    /**
     * 默认销售税
     */
    public Integer account_sale_tax_id;

    @JsonIgnore
    public boolean account_sale_tax_idDirtyFlag;
    
    /**
     * 默认销售税
     */
    public String account_sale_tax_id_text;

    @JsonIgnore
    public boolean account_sale_tax_id_textDirtyFlag;
    
    /**
     * 有待被确认的银行数据步骤的状态
     */
    public String account_setup_bank_data_state;

    @JsonIgnore
    public boolean account_setup_bank_data_stateDirtyFlag;
    
    /**
     * 科目状态
     */
    public String account_setup_coa_state;

    @JsonIgnore
    public boolean account_setup_coa_stateDirtyFlag;
    
    /**
     * 有待被确认的会计年度步骤的状态
     */
    public String account_setup_fy_data_state;

    @JsonIgnore
    public boolean account_setup_fy_data_stateDirtyFlag;
    
    /**
     * 使用anglo-saxon会计
     */
    public String anglo_saxon_accounting;

    @JsonIgnore
    public boolean anglo_saxon_accountingDirtyFlag;
    
    /**
     * 银行科目的前缀
     */
    public String bank_account_code_prefix;

    @JsonIgnore
    public boolean bank_account_code_prefixDirtyFlag;
    
    /**
     * 银行账户
     */
    public String bank_ids;

    @JsonIgnore
    public boolean bank_idsDirtyFlag;
    
    /**
     * 银行日记账
     */
    public String bank_journal_ids;

    @JsonIgnore
    public boolean bank_journal_idsDirtyFlag;
    
    /**
     * 公司状态
     */
    public String base_onboarding_company_state;

    @JsonIgnore
    public boolean base_onboarding_company_stateDirtyFlag;
    
    /**
     * 现金科目的前缀
     */
    public String cash_account_code_prefix;

    @JsonIgnore
    public boolean cash_account_code_prefixDirtyFlag;
    
    /**
     * 预设邮件
     */
    public String catchall;

    @JsonIgnore
    public boolean catchallDirtyFlag;
    
    /**
     * 表模板
     */
    public Integer chart_template_id;

    @JsonIgnore
    public boolean chart_template_idDirtyFlag;
    
    /**
     * 表模板
     */
    public String chart_template_id_text;

    @JsonIgnore
    public boolean chart_template_id_textDirtyFlag;
    
    /**
     * 下级公司
     */
    public String child_ids;

    @JsonIgnore
    public boolean child_idsDirtyFlag;
    
    /**
     * 城市
     */
    public String city;

    @JsonIgnore
    public boolean cityDirtyFlag;
    
    /**
     * 公司注册
     */
    public String company_registry;

    @JsonIgnore
    public boolean company_registryDirtyFlag;
    
    /**
     * 国家/地区
     */
    public Integer country_id;

    @JsonIgnore
    public boolean country_idDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 汇兑损益
     */
    public Integer currency_exchange_journal_id;

    @JsonIgnore
    public boolean currency_exchange_journal_idDirtyFlag;
    
    /**
     * 汇兑损益
     */
    public String currency_exchange_journal_id_text;

    @JsonIgnore
    public boolean currency_exchange_journal_id_textDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 币种
     */
    public String currency_id_text;

    @JsonIgnore
    public boolean currency_id_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * EMail
     */
    public String email;

    @JsonIgnore
    public boolean emailDirtyFlag;
    
    /**
     * 预计会计科目表
     */
    public String expects_chart_of_accounts;

    @JsonIgnore
    public boolean expects_chart_of_accountsDirtyFlag;
    
    /**
     * 汇率损失科目
     */
    public Integer expense_currency_exchange_account_id;

    @JsonIgnore
    public boolean expense_currency_exchange_account_idDirtyFlag;
    
    /**
     * 文档模板
     */
    public Integer external_report_layout_id;

    @JsonIgnore
    public boolean external_report_layout_idDirtyFlag;
    
    /**
     * 会计年度的最后一天
     */
    public Integer fiscalyear_last_day;

    @JsonIgnore
    public boolean fiscalyear_last_dayDirtyFlag;
    
    /**
     * 会计年度的最后一个月
     */
    public String fiscalyear_last_month;

    @JsonIgnore
    public boolean fiscalyear_last_monthDirtyFlag;
    
    /**
     * 锁定日期
     */
    public Timestamp fiscalyear_lock_date;

    @JsonIgnore
    public boolean fiscalyear_lock_dateDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 汇率增益科目
     */
    public Integer income_currency_exchange_account_id;

    @JsonIgnore
    public boolean income_currency_exchange_account_idDirtyFlag;
    
    /**
     * 默认国际贸易术语
     */
    public Integer incoterm_id;

    @JsonIgnore
    public boolean incoterm_idDirtyFlag;
    
    /**
     * 默认国际贸易术语
     */
    public String incoterm_id_text;

    @JsonIgnore
    public boolean incoterm_id_textDirtyFlag;
    
    /**
     * 内部中转位置
     */
    public Integer internal_transit_location_id;

    @JsonIgnore
    public boolean internal_transit_location_idDirtyFlag;
    
    /**
     * 内部中转位置
     */
    public String internal_transit_location_id_text;

    @JsonIgnore
    public boolean internal_transit_location_id_textDirtyFlag;
    
    /**
     * 默认邮件
     */
    public String invoice_is_email;

    @JsonIgnore
    public boolean invoice_is_emailDirtyFlag;
    
    /**
     * 通过默认值打印
     */
    public String invoice_is_print;

    @JsonIgnore
    public boolean invoice_is_printDirtyFlag;
    
    /**
     * 默认以信件发送
     */
    public String invoice_is_snailmail;

    @JsonIgnore
    public boolean invoice_is_snailmailDirtyFlag;
    
    /**
     * 默认信息类型
     */
    public String invoice_reference_type;

    @JsonIgnore
    public boolean invoice_reference_typeDirtyFlag;
    
    /**
     * 公司 Logo
     */
    public byte[] logo;

    @JsonIgnore
    public boolean logoDirtyFlag;
    
    /**
     * 网页徽标
     */
    public byte[] logo_web;

    @JsonIgnore
    public boolean logo_webDirtyFlag;
    
    /**
     * 制造提前期(日)
     */
    public Double manufacturing_lead;

    @JsonIgnore
    public boolean manufacturing_leadDirtyFlag;
    
    /**
     * 公司名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 命名规则
     */
    public Integer nomenclature_id;

    @JsonIgnore
    public boolean nomenclature_idDirtyFlag;
    
    /**
     * 逾期追款消息
     */
    public String overdue_msg;

    @JsonIgnore
    public boolean overdue_msgDirtyFlag;
    
    /**
     * 纸张格式
     */
    public Integer paperformat_id;

    @JsonIgnore
    public boolean paperformat_idDirtyFlag;
    
    /**
     * 上级公司
     */
    public Integer parent_id;

    @JsonIgnore
    public boolean parent_idDirtyFlag;
    
    /**
     * 上级公司
     */
    public String parent_id_text;

    @JsonIgnore
    public boolean parent_id_textDirtyFlag;
    
    /**
     * 公司数据库ID
     */
    public Integer partner_gid;

    @JsonIgnore
    public boolean partner_gidDirtyFlag;
    
    /**
     * 业务伙伴
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 入职支付收单机构的状态
     */
    public String payment_acquirer_onboarding_state;

    @JsonIgnore
    public boolean payment_acquirer_onboarding_stateDirtyFlag;
    
    /**
     * 选择付款方式
     */
    public String payment_onboarding_payment_method;

    @JsonIgnore
    public boolean payment_onboarding_payment_methodDirtyFlag;
    
    /**
     * 非顾问的锁定日期
     */
    public Timestamp period_lock_date;

    @JsonIgnore
    public boolean period_lock_dateDirtyFlag;
    
    /**
     * 电话
     */
    public String phone;

    @JsonIgnore
    public boolean phoneDirtyFlag;
    
    /**
     * 在线支付
     */
    public String portal_confirmation_pay;

    @JsonIgnore
    public boolean portal_confirmation_payDirtyFlag;
    
    /**
     * 在线签名
     */
    public String portal_confirmation_sign;

    @JsonIgnore
    public boolean portal_confirmation_signDirtyFlag;
    
    /**
     * 批准等级
     */
    public String po_double_validation;

    @JsonIgnore
    public boolean po_double_validationDirtyFlag;
    
    /**
     * 再次验证金额
     */
    public Double po_double_validation_amount;

    @JsonIgnore
    public boolean po_double_validation_amountDirtyFlag;
    
    /**
     * 采购提前时间
     */
    public Double po_lead;

    @JsonIgnore
    public boolean po_leadDirtyFlag;
    
    /**
     * 销售订单修改
     */
    public String po_lock;

    @JsonIgnore
    public boolean po_lockDirtyFlag;
    
    /**
     * 连接在一起的库存移动的日期变化传播的最小差值。
     */
    public Integer propagation_minimum_delta;

    @JsonIgnore
    public boolean propagation_minimum_deltaDirtyFlag;
    
    /**
     * 库存计价的入库科目
     */
    public Integer property_stock_account_input_categ_id;

    @JsonIgnore
    public boolean property_stock_account_input_categ_idDirtyFlag;
    
    /**
     * 库存计价的入库科目
     */
    public String property_stock_account_input_categ_id_text;

    @JsonIgnore
    public boolean property_stock_account_input_categ_id_textDirtyFlag;
    
    /**
     * 库存计价的出货科目
     */
    public Integer property_stock_account_output_categ_id;

    @JsonIgnore
    public boolean property_stock_account_output_categ_idDirtyFlag;
    
    /**
     * 库存计价的出货科目
     */
    public String property_stock_account_output_categ_id_text;

    @JsonIgnore
    public boolean property_stock_account_output_categ_id_textDirtyFlag;
    
    /**
     * 库存计价的科目模板
     */
    public Integer property_stock_valuation_account_id;

    @JsonIgnore
    public boolean property_stock_valuation_account_idDirtyFlag;
    
    /**
     * 库存计价的科目模板
     */
    public String property_stock_valuation_account_id_text;

    @JsonIgnore
    public boolean property_stock_valuation_account_id_textDirtyFlag;
    
    /**
     * 显示SEPA QR码
     */
    public String qr_code;

    @JsonIgnore
    public boolean qr_codeDirtyFlag;
    
    /**
     * 默认报价有效期（日）
     */
    public Integer quotation_validity_days;

    @JsonIgnore
    public boolean quotation_validity_daysDirtyFlag;
    
    /**
     * 报表页脚
     */
    public String report_footer;

    @JsonIgnore
    public boolean report_footerDirtyFlag;
    
    /**
     * 公司口号
     */
    public String report_header;

    @JsonIgnore
    public boolean report_headerDirtyFlag;
    
    /**
     * 默认工作时间
     */
    public Integer resource_calendar_id;

    @JsonIgnore
    public boolean resource_calendar_idDirtyFlag;
    
    /**
     * 工作时间
     */
    public String resource_calendar_ids;

    @JsonIgnore
    public boolean resource_calendar_idsDirtyFlag;
    
    /**
     * 默认工作时间
     */
    public String resource_calendar_id_text;

    @JsonIgnore
    public boolean resource_calendar_id_textDirtyFlag;
    
    /**
     * 默认条款和条件
     */
    public String sale_note;

    @JsonIgnore
    public boolean sale_noteDirtyFlag;
    
    /**
     * 有待被确认的订单步骤的状态
     */
    public String sale_onboarding_order_confirmation_state;

    @JsonIgnore
    public boolean sale_onboarding_order_confirmation_stateDirtyFlag;
    
    /**
     * 请选择付款方式
     */
    public String sale_onboarding_payment_method;

    @JsonIgnore
    public boolean sale_onboarding_payment_methodDirtyFlag;
    
    /**
     * 有待被确认的样品报价单步骤的状态
     */
    public String sale_onboarding_sample_quotation_state;

    @JsonIgnore
    public boolean sale_onboarding_sample_quotation_stateDirtyFlag;
    
    /**
     * 正进行销售面板的状态
     */
    public String sale_quotation_onboarding_state;

    @JsonIgnore
    public boolean sale_quotation_onboarding_stateDirtyFlag;
    
    /**
     * 销售安全天数
     */
    public Double security_lead;

    @JsonIgnore
    public boolean security_leadDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 颜色
     */
    public String snailmail_color;

    @JsonIgnore
    public boolean snailmail_colorDirtyFlag;
    
    /**
     * 双面
     */
    public String snailmail_duplex;

    @JsonIgnore
    public boolean snailmail_duplexDirtyFlag;
    
    /**
     * 脸书账号
     */
    public String social_facebook;

    @JsonIgnore
    public boolean social_facebookDirtyFlag;
    
    /**
     * GitHub账户
     */
    public String social_github;

    @JsonIgnore
    public boolean social_githubDirtyFlag;
    
    /**
     * Google+账户
     */
    public String social_googleplus;

    @JsonIgnore
    public boolean social_googleplusDirtyFlag;
    
    /**
     * Instagram 账号
     */
    public String social_instagram;

    @JsonIgnore
    public boolean social_instagramDirtyFlag;
    
    /**
     * 领英账号
     */
    public String social_linkedin;

    @JsonIgnore
    public boolean social_linkedinDirtyFlag;
    
    /**
     * Twitter账号
     */
    public String social_twitter;

    @JsonIgnore
    public boolean social_twitterDirtyFlag;
    
    /**
     * Youtube账号
     */
    public String social_youtube;

    @JsonIgnore
    public boolean social_youtubeDirtyFlag;
    
    /**
     * 状态
     */
    public Integer state_id;

    @JsonIgnore
    public boolean state_idDirtyFlag;
    
    /**
     * 街道
     */
    public String street;

    @JsonIgnore
    public boolean streetDirtyFlag;
    
    /**
     * 街道 2
     */
    public String street2;

    @JsonIgnore
    public boolean street2DirtyFlag;
    
    /**
     * 税率计算的舍入方法
     */
    public String tax_calculation_rounding_method;

    @JsonIgnore
    public boolean tax_calculation_rounding_methodDirtyFlag;
    
    /**
     * 现金收付制日记账
     */
    public Integer tax_cash_basis_journal_id;

    @JsonIgnore
    public boolean tax_cash_basis_journal_idDirtyFlag;
    
    /**
     * 现金收付制日记账
     */
    public String tax_cash_basis_journal_id_text;

    @JsonIgnore
    public boolean tax_cash_basis_journal_id_textDirtyFlag;
    
    /**
     * 使用现金收付制
     */
    public String tax_exigibility;

    @JsonIgnore
    public boolean tax_exigibilityDirtyFlag;
    
    /**
     * 转账帐户的前缀
     */
    public String transfer_account_code_prefix;

    @JsonIgnore
    public boolean transfer_account_code_prefixDirtyFlag;
    
    /**
     * 银行间转账科目
     */
    public Integer transfer_account_id;

    @JsonIgnore
    public boolean transfer_account_idDirtyFlag;
    
    /**
     * 银行间转账科目
     */
    public String transfer_account_id_text;

    @JsonIgnore
    public boolean transfer_account_id_textDirtyFlag;
    
    /**
     * 接受的用户
     */
    public String user_ids;

    @JsonIgnore
    public boolean user_idsDirtyFlag;
    
    /**
     * 税号
     */
    public String vat;

    @JsonIgnore
    public boolean vatDirtyFlag;
    
    /**
     * 网站
     */
    public String website;

    @JsonIgnore
    public boolean websiteDirtyFlag;
    
    /**
     * 网站销售状态入职付款收单机构步骤
     */
    public String website_sale_onboarding_payment_acquirer_state;

    @JsonIgnore
    public boolean website_sale_onboarding_payment_acquirer_stateDirtyFlag;
    
    /**
     * 招聘网站主题一步完成
     */
    public String website_theme_onboarding_done;

    @JsonIgnore
    public boolean website_theme_onboarding_doneDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 邮政编码
     */
    public String zip;

    @JsonIgnore
    public boolean zipDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [银行核销阈值]
     */
    @JsonProperty("account_bank_reconciliation_start")
    public Timestamp getAccount_bank_reconciliation_start(){
        return this.account_bank_reconciliation_start ;
    }

    /**
     * 设置 [银行核销阈值]
     */
    @JsonProperty("account_bank_reconciliation_start")
    public void setAccount_bank_reconciliation_start(Timestamp  account_bank_reconciliation_start){
        this.account_bank_reconciliation_start = account_bank_reconciliation_start ;
        this.account_bank_reconciliation_startDirtyFlag = true ;
    }

     /**
     * 获取 [银行核销阈值]脏标记
     */
    @JsonIgnore
    public boolean getAccount_bank_reconciliation_startDirtyFlag(){
        return this.account_bank_reconciliation_startDirtyFlag ;
    }   

    /**
     * 获取 [处于会计面板的状态]
     */
    @JsonProperty("account_dashboard_onboarding_state")
    public String getAccount_dashboard_onboarding_state(){
        return this.account_dashboard_onboarding_state ;
    }

    /**
     * 设置 [处于会计面板的状态]
     */
    @JsonProperty("account_dashboard_onboarding_state")
    public void setAccount_dashboard_onboarding_state(String  account_dashboard_onboarding_state){
        this.account_dashboard_onboarding_state = account_dashboard_onboarding_state ;
        this.account_dashboard_onboarding_stateDirtyFlag = true ;
    }

     /**
     * 获取 [处于会计面板的状态]脏标记
     */
    @JsonIgnore
    public boolean getAccount_dashboard_onboarding_stateDirtyFlag(){
        return this.account_dashboard_onboarding_stateDirtyFlag ;
    }   

    /**
     * 获取 [处于会计发票面板的状态]
     */
    @JsonProperty("account_invoice_onboarding_state")
    public String getAccount_invoice_onboarding_state(){
        return this.account_invoice_onboarding_state ;
    }

    /**
     * 设置 [处于会计发票面板的状态]
     */
    @JsonProperty("account_invoice_onboarding_state")
    public void setAccount_invoice_onboarding_state(String  account_invoice_onboarding_state){
        this.account_invoice_onboarding_state = account_invoice_onboarding_state ;
        this.account_invoice_onboarding_stateDirtyFlag = true ;
    }

     /**
     * 获取 [处于会计发票面板的状态]脏标记
     */
    @JsonIgnore
    public boolean getAccount_invoice_onboarding_stateDirtyFlag(){
        return this.account_invoice_onboarding_stateDirtyFlag ;
    }   

    /**
     * 获取 [科目号码]
     */
    @JsonProperty("account_no")
    public String getAccount_no(){
        return this.account_no ;
    }

    /**
     * 设置 [科目号码]
     */
    @JsonProperty("account_no")
    public void setAccount_no(String  account_no){
        this.account_no = account_no ;
        this.account_noDirtyFlag = true ;
    }

     /**
     * 获取 [科目号码]脏标记
     */
    @JsonIgnore
    public boolean getAccount_noDirtyFlag(){
        return this.account_noDirtyFlag ;
    }   

    /**
     * 获取 [有待被确认的发票步骤的状态]
     */
    @JsonProperty("account_onboarding_invoice_layout_state")
    public String getAccount_onboarding_invoice_layout_state(){
        return this.account_onboarding_invoice_layout_state ;
    }

    /**
     * 设置 [有待被确认的发票步骤的状态]
     */
    @JsonProperty("account_onboarding_invoice_layout_state")
    public void setAccount_onboarding_invoice_layout_state(String  account_onboarding_invoice_layout_state){
        this.account_onboarding_invoice_layout_state = account_onboarding_invoice_layout_state ;
        this.account_onboarding_invoice_layout_stateDirtyFlag = true ;
    }

     /**
     * 获取 [有待被确认的发票步骤的状态]脏标记
     */
    @JsonIgnore
    public boolean getAccount_onboarding_invoice_layout_stateDirtyFlag(){
        return this.account_onboarding_invoice_layout_stateDirtyFlag ;
    }   

    /**
     * 获取 [有待被确认的报价单步骤的状态]
     */
    @JsonProperty("account_onboarding_sale_tax_state")
    public String getAccount_onboarding_sale_tax_state(){
        return this.account_onboarding_sale_tax_state ;
    }

    /**
     * 设置 [有待被确认的报价单步骤的状态]
     */
    @JsonProperty("account_onboarding_sale_tax_state")
    public void setAccount_onboarding_sale_tax_state(String  account_onboarding_sale_tax_state){
        this.account_onboarding_sale_tax_state = account_onboarding_sale_tax_state ;
        this.account_onboarding_sale_tax_stateDirtyFlag = true ;
    }

     /**
     * 获取 [有待被确认的报价单步骤的状态]脏标记
     */
    @JsonIgnore
    public boolean getAccount_onboarding_sale_tax_stateDirtyFlag(){
        return this.account_onboarding_sale_tax_stateDirtyFlag ;
    }   

    /**
     * 获取 [有待被确认的样品报价单步骤的状态]
     */
    @JsonProperty("account_onboarding_sample_invoice_state")
    public String getAccount_onboarding_sample_invoice_state(){
        return this.account_onboarding_sample_invoice_state ;
    }

    /**
     * 设置 [有待被确认的样品报价单步骤的状态]
     */
    @JsonProperty("account_onboarding_sample_invoice_state")
    public void setAccount_onboarding_sample_invoice_state(String  account_onboarding_sample_invoice_state){
        this.account_onboarding_sample_invoice_state = account_onboarding_sample_invoice_state ;
        this.account_onboarding_sample_invoice_stateDirtyFlag = true ;
    }

     /**
     * 获取 [有待被确认的样品报价单步骤的状态]脏标记
     */
    @JsonIgnore
    public boolean getAccount_onboarding_sample_invoice_stateDirtyFlag(){
        return this.account_onboarding_sample_invoice_stateDirtyFlag ;
    }   

    /**
     * 获取 [期初日期]
     */
    @JsonProperty("account_opening_date")
    public Timestamp getAccount_opening_date(){
        return this.account_opening_date ;
    }

    /**
     * 设置 [期初日期]
     */
    @JsonProperty("account_opening_date")
    public void setAccount_opening_date(Timestamp  account_opening_date){
        this.account_opening_date = account_opening_date ;
        this.account_opening_dateDirtyFlag = true ;
    }

     /**
     * 获取 [期初日期]脏标记
     */
    @JsonIgnore
    public boolean getAccount_opening_dateDirtyFlag(){
        return this.account_opening_dateDirtyFlag ;
    }   

    /**
     * 获取 [期初日记账]
     */
    @JsonProperty("account_opening_journal_id")
    public Integer getAccount_opening_journal_id(){
        return this.account_opening_journal_id ;
    }

    /**
     * 设置 [期初日记账]
     */
    @JsonProperty("account_opening_journal_id")
    public void setAccount_opening_journal_id(Integer  account_opening_journal_id){
        this.account_opening_journal_id = account_opening_journal_id ;
        this.account_opening_journal_idDirtyFlag = true ;
    }

     /**
     * 获取 [期初日记账]脏标记
     */
    @JsonIgnore
    public boolean getAccount_opening_journal_idDirtyFlag(){
        return this.account_opening_journal_idDirtyFlag ;
    }   

    /**
     * 获取 [期初日记账分录]
     */
    @JsonProperty("account_opening_move_id")
    public Integer getAccount_opening_move_id(){
        return this.account_opening_move_id ;
    }

    /**
     * 设置 [期初日记账分录]
     */
    @JsonProperty("account_opening_move_id")
    public void setAccount_opening_move_id(Integer  account_opening_move_id){
        this.account_opening_move_id = account_opening_move_id ;
        this.account_opening_move_idDirtyFlag = true ;
    }

     /**
     * 获取 [期初日记账分录]脏标记
     */
    @JsonIgnore
    public boolean getAccount_opening_move_idDirtyFlag(){
        return this.account_opening_move_idDirtyFlag ;
    }   

    /**
     * 获取 [期初日记账分录]
     */
    @JsonProperty("account_opening_move_id_text")
    public String getAccount_opening_move_id_text(){
        return this.account_opening_move_id_text ;
    }

    /**
     * 设置 [期初日记账分录]
     */
    @JsonProperty("account_opening_move_id_text")
    public void setAccount_opening_move_id_text(String  account_opening_move_id_text){
        this.account_opening_move_id_text = account_opening_move_id_text ;
        this.account_opening_move_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [期初日记账分录]脏标记
     */
    @JsonIgnore
    public boolean getAccount_opening_move_id_textDirtyFlag(){
        return this.account_opening_move_id_textDirtyFlag ;
    }   

    /**
     * 获取 [默认进项税]
     */
    @JsonProperty("account_purchase_tax_id")
    public Integer getAccount_purchase_tax_id(){
        return this.account_purchase_tax_id ;
    }

    /**
     * 设置 [默认进项税]
     */
    @JsonProperty("account_purchase_tax_id")
    public void setAccount_purchase_tax_id(Integer  account_purchase_tax_id){
        this.account_purchase_tax_id = account_purchase_tax_id ;
        this.account_purchase_tax_idDirtyFlag = true ;
    }

     /**
     * 获取 [默认进项税]脏标记
     */
    @JsonIgnore
    public boolean getAccount_purchase_tax_idDirtyFlag(){
        return this.account_purchase_tax_idDirtyFlag ;
    }   

    /**
     * 获取 [默认进项税]
     */
    @JsonProperty("account_purchase_tax_id_text")
    public String getAccount_purchase_tax_id_text(){
        return this.account_purchase_tax_id_text ;
    }

    /**
     * 设置 [默认进项税]
     */
    @JsonProperty("account_purchase_tax_id_text")
    public void setAccount_purchase_tax_id_text(String  account_purchase_tax_id_text){
        this.account_purchase_tax_id_text = account_purchase_tax_id_text ;
        this.account_purchase_tax_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [默认进项税]脏标记
     */
    @JsonIgnore
    public boolean getAccount_purchase_tax_id_textDirtyFlag(){
        return this.account_purchase_tax_id_textDirtyFlag ;
    }   

    /**
     * 获取 [默认销售税]
     */
    @JsonProperty("account_sale_tax_id")
    public Integer getAccount_sale_tax_id(){
        return this.account_sale_tax_id ;
    }

    /**
     * 设置 [默认销售税]
     */
    @JsonProperty("account_sale_tax_id")
    public void setAccount_sale_tax_id(Integer  account_sale_tax_id){
        this.account_sale_tax_id = account_sale_tax_id ;
        this.account_sale_tax_idDirtyFlag = true ;
    }

     /**
     * 获取 [默认销售税]脏标记
     */
    @JsonIgnore
    public boolean getAccount_sale_tax_idDirtyFlag(){
        return this.account_sale_tax_idDirtyFlag ;
    }   

    /**
     * 获取 [默认销售税]
     */
    @JsonProperty("account_sale_tax_id_text")
    public String getAccount_sale_tax_id_text(){
        return this.account_sale_tax_id_text ;
    }

    /**
     * 设置 [默认销售税]
     */
    @JsonProperty("account_sale_tax_id_text")
    public void setAccount_sale_tax_id_text(String  account_sale_tax_id_text){
        this.account_sale_tax_id_text = account_sale_tax_id_text ;
        this.account_sale_tax_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [默认销售税]脏标记
     */
    @JsonIgnore
    public boolean getAccount_sale_tax_id_textDirtyFlag(){
        return this.account_sale_tax_id_textDirtyFlag ;
    }   

    /**
     * 获取 [有待被确认的银行数据步骤的状态]
     */
    @JsonProperty("account_setup_bank_data_state")
    public String getAccount_setup_bank_data_state(){
        return this.account_setup_bank_data_state ;
    }

    /**
     * 设置 [有待被确认的银行数据步骤的状态]
     */
    @JsonProperty("account_setup_bank_data_state")
    public void setAccount_setup_bank_data_state(String  account_setup_bank_data_state){
        this.account_setup_bank_data_state = account_setup_bank_data_state ;
        this.account_setup_bank_data_stateDirtyFlag = true ;
    }

     /**
     * 获取 [有待被确认的银行数据步骤的状态]脏标记
     */
    @JsonIgnore
    public boolean getAccount_setup_bank_data_stateDirtyFlag(){
        return this.account_setup_bank_data_stateDirtyFlag ;
    }   

    /**
     * 获取 [科目状态]
     */
    @JsonProperty("account_setup_coa_state")
    public String getAccount_setup_coa_state(){
        return this.account_setup_coa_state ;
    }

    /**
     * 设置 [科目状态]
     */
    @JsonProperty("account_setup_coa_state")
    public void setAccount_setup_coa_state(String  account_setup_coa_state){
        this.account_setup_coa_state = account_setup_coa_state ;
        this.account_setup_coa_stateDirtyFlag = true ;
    }

     /**
     * 获取 [科目状态]脏标记
     */
    @JsonIgnore
    public boolean getAccount_setup_coa_stateDirtyFlag(){
        return this.account_setup_coa_stateDirtyFlag ;
    }   

    /**
     * 获取 [有待被确认的会计年度步骤的状态]
     */
    @JsonProperty("account_setup_fy_data_state")
    public String getAccount_setup_fy_data_state(){
        return this.account_setup_fy_data_state ;
    }

    /**
     * 设置 [有待被确认的会计年度步骤的状态]
     */
    @JsonProperty("account_setup_fy_data_state")
    public void setAccount_setup_fy_data_state(String  account_setup_fy_data_state){
        this.account_setup_fy_data_state = account_setup_fy_data_state ;
        this.account_setup_fy_data_stateDirtyFlag = true ;
    }

     /**
     * 获取 [有待被确认的会计年度步骤的状态]脏标记
     */
    @JsonIgnore
    public boolean getAccount_setup_fy_data_stateDirtyFlag(){
        return this.account_setup_fy_data_stateDirtyFlag ;
    }   

    /**
     * 获取 [使用anglo-saxon会计]
     */
    @JsonProperty("anglo_saxon_accounting")
    public String getAnglo_saxon_accounting(){
        return this.anglo_saxon_accounting ;
    }

    /**
     * 设置 [使用anglo-saxon会计]
     */
    @JsonProperty("anglo_saxon_accounting")
    public void setAnglo_saxon_accounting(String  anglo_saxon_accounting){
        this.anglo_saxon_accounting = anglo_saxon_accounting ;
        this.anglo_saxon_accountingDirtyFlag = true ;
    }

     /**
     * 获取 [使用anglo-saxon会计]脏标记
     */
    @JsonIgnore
    public boolean getAnglo_saxon_accountingDirtyFlag(){
        return this.anglo_saxon_accountingDirtyFlag ;
    }   

    /**
     * 获取 [银行科目的前缀]
     */
    @JsonProperty("bank_account_code_prefix")
    public String getBank_account_code_prefix(){
        return this.bank_account_code_prefix ;
    }

    /**
     * 设置 [银行科目的前缀]
     */
    @JsonProperty("bank_account_code_prefix")
    public void setBank_account_code_prefix(String  bank_account_code_prefix){
        this.bank_account_code_prefix = bank_account_code_prefix ;
        this.bank_account_code_prefixDirtyFlag = true ;
    }

     /**
     * 获取 [银行科目的前缀]脏标记
     */
    @JsonIgnore
    public boolean getBank_account_code_prefixDirtyFlag(){
        return this.bank_account_code_prefixDirtyFlag ;
    }   

    /**
     * 获取 [银行账户]
     */
    @JsonProperty("bank_ids")
    public String getBank_ids(){
        return this.bank_ids ;
    }

    /**
     * 设置 [银行账户]
     */
    @JsonProperty("bank_ids")
    public void setBank_ids(String  bank_ids){
        this.bank_ids = bank_ids ;
        this.bank_idsDirtyFlag = true ;
    }

     /**
     * 获取 [银行账户]脏标记
     */
    @JsonIgnore
    public boolean getBank_idsDirtyFlag(){
        return this.bank_idsDirtyFlag ;
    }   

    /**
     * 获取 [银行日记账]
     */
    @JsonProperty("bank_journal_ids")
    public String getBank_journal_ids(){
        return this.bank_journal_ids ;
    }

    /**
     * 设置 [银行日记账]
     */
    @JsonProperty("bank_journal_ids")
    public void setBank_journal_ids(String  bank_journal_ids){
        this.bank_journal_ids = bank_journal_ids ;
        this.bank_journal_idsDirtyFlag = true ;
    }

     /**
     * 获取 [银行日记账]脏标记
     */
    @JsonIgnore
    public boolean getBank_journal_idsDirtyFlag(){
        return this.bank_journal_idsDirtyFlag ;
    }   

    /**
     * 获取 [公司状态]
     */
    @JsonProperty("base_onboarding_company_state")
    public String getBase_onboarding_company_state(){
        return this.base_onboarding_company_state ;
    }

    /**
     * 设置 [公司状态]
     */
    @JsonProperty("base_onboarding_company_state")
    public void setBase_onboarding_company_state(String  base_onboarding_company_state){
        this.base_onboarding_company_state = base_onboarding_company_state ;
        this.base_onboarding_company_stateDirtyFlag = true ;
    }

     /**
     * 获取 [公司状态]脏标记
     */
    @JsonIgnore
    public boolean getBase_onboarding_company_stateDirtyFlag(){
        return this.base_onboarding_company_stateDirtyFlag ;
    }   

    /**
     * 获取 [现金科目的前缀]
     */
    @JsonProperty("cash_account_code_prefix")
    public String getCash_account_code_prefix(){
        return this.cash_account_code_prefix ;
    }

    /**
     * 设置 [现金科目的前缀]
     */
    @JsonProperty("cash_account_code_prefix")
    public void setCash_account_code_prefix(String  cash_account_code_prefix){
        this.cash_account_code_prefix = cash_account_code_prefix ;
        this.cash_account_code_prefixDirtyFlag = true ;
    }

     /**
     * 获取 [现金科目的前缀]脏标记
     */
    @JsonIgnore
    public boolean getCash_account_code_prefixDirtyFlag(){
        return this.cash_account_code_prefixDirtyFlag ;
    }   

    /**
     * 获取 [预设邮件]
     */
    @JsonProperty("catchall")
    public String getCatchall(){
        return this.catchall ;
    }

    /**
     * 设置 [预设邮件]
     */
    @JsonProperty("catchall")
    public void setCatchall(String  catchall){
        this.catchall = catchall ;
        this.catchallDirtyFlag = true ;
    }

     /**
     * 获取 [预设邮件]脏标记
     */
    @JsonIgnore
    public boolean getCatchallDirtyFlag(){
        return this.catchallDirtyFlag ;
    }   

    /**
     * 获取 [表模板]
     */
    @JsonProperty("chart_template_id")
    public Integer getChart_template_id(){
        return this.chart_template_id ;
    }

    /**
     * 设置 [表模板]
     */
    @JsonProperty("chart_template_id")
    public void setChart_template_id(Integer  chart_template_id){
        this.chart_template_id = chart_template_id ;
        this.chart_template_idDirtyFlag = true ;
    }

     /**
     * 获取 [表模板]脏标记
     */
    @JsonIgnore
    public boolean getChart_template_idDirtyFlag(){
        return this.chart_template_idDirtyFlag ;
    }   

    /**
     * 获取 [表模板]
     */
    @JsonProperty("chart_template_id_text")
    public String getChart_template_id_text(){
        return this.chart_template_id_text ;
    }

    /**
     * 设置 [表模板]
     */
    @JsonProperty("chart_template_id_text")
    public void setChart_template_id_text(String  chart_template_id_text){
        this.chart_template_id_text = chart_template_id_text ;
        this.chart_template_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [表模板]脏标记
     */
    @JsonIgnore
    public boolean getChart_template_id_textDirtyFlag(){
        return this.chart_template_id_textDirtyFlag ;
    }   

    /**
     * 获取 [下级公司]
     */
    @JsonProperty("child_ids")
    public String getChild_ids(){
        return this.child_ids ;
    }

    /**
     * 设置 [下级公司]
     */
    @JsonProperty("child_ids")
    public void setChild_ids(String  child_ids){
        this.child_ids = child_ids ;
        this.child_idsDirtyFlag = true ;
    }

     /**
     * 获取 [下级公司]脏标记
     */
    @JsonIgnore
    public boolean getChild_idsDirtyFlag(){
        return this.child_idsDirtyFlag ;
    }   

    /**
     * 获取 [城市]
     */
    @JsonProperty("city")
    public String getCity(){
        return this.city ;
    }

    /**
     * 设置 [城市]
     */
    @JsonProperty("city")
    public void setCity(String  city){
        this.city = city ;
        this.cityDirtyFlag = true ;
    }

     /**
     * 获取 [城市]脏标记
     */
    @JsonIgnore
    public boolean getCityDirtyFlag(){
        return this.cityDirtyFlag ;
    }   

    /**
     * 获取 [公司注册]
     */
    @JsonProperty("company_registry")
    public String getCompany_registry(){
        return this.company_registry ;
    }

    /**
     * 设置 [公司注册]
     */
    @JsonProperty("company_registry")
    public void setCompany_registry(String  company_registry){
        this.company_registry = company_registry ;
        this.company_registryDirtyFlag = true ;
    }

     /**
     * 获取 [公司注册]脏标记
     */
    @JsonIgnore
    public boolean getCompany_registryDirtyFlag(){
        return this.company_registryDirtyFlag ;
    }   

    /**
     * 获取 [国家/地区]
     */
    @JsonProperty("country_id")
    public Integer getCountry_id(){
        return this.country_id ;
    }

    /**
     * 设置 [国家/地区]
     */
    @JsonProperty("country_id")
    public void setCountry_id(Integer  country_id){
        this.country_id = country_id ;
        this.country_idDirtyFlag = true ;
    }

     /**
     * 获取 [国家/地区]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idDirtyFlag(){
        return this.country_idDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [汇兑损益]
     */
    @JsonProperty("currency_exchange_journal_id")
    public Integer getCurrency_exchange_journal_id(){
        return this.currency_exchange_journal_id ;
    }

    /**
     * 设置 [汇兑损益]
     */
    @JsonProperty("currency_exchange_journal_id")
    public void setCurrency_exchange_journal_id(Integer  currency_exchange_journal_id){
        this.currency_exchange_journal_id = currency_exchange_journal_id ;
        this.currency_exchange_journal_idDirtyFlag = true ;
    }

     /**
     * 获取 [汇兑损益]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_exchange_journal_idDirtyFlag(){
        return this.currency_exchange_journal_idDirtyFlag ;
    }   

    /**
     * 获取 [汇兑损益]
     */
    @JsonProperty("currency_exchange_journal_id_text")
    public String getCurrency_exchange_journal_id_text(){
        return this.currency_exchange_journal_id_text ;
    }

    /**
     * 设置 [汇兑损益]
     */
    @JsonProperty("currency_exchange_journal_id_text")
    public void setCurrency_exchange_journal_id_text(String  currency_exchange_journal_id_text){
        this.currency_exchange_journal_id_text = currency_exchange_journal_id_text ;
        this.currency_exchange_journal_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [汇兑损益]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_exchange_journal_id_textDirtyFlag(){
        return this.currency_exchange_journal_id_textDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return this.currency_id_text ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return this.currency_id_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [EMail]
     */
    @JsonProperty("email")
    public String getEmail(){
        return this.email ;
    }

    /**
     * 设置 [EMail]
     */
    @JsonProperty("email")
    public void setEmail(String  email){
        this.email = email ;
        this.emailDirtyFlag = true ;
    }

     /**
     * 获取 [EMail]脏标记
     */
    @JsonIgnore
    public boolean getEmailDirtyFlag(){
        return this.emailDirtyFlag ;
    }   

    /**
     * 获取 [预计会计科目表]
     */
    @JsonProperty("expects_chart_of_accounts")
    public String getExpects_chart_of_accounts(){
        return this.expects_chart_of_accounts ;
    }

    /**
     * 设置 [预计会计科目表]
     */
    @JsonProperty("expects_chart_of_accounts")
    public void setExpects_chart_of_accounts(String  expects_chart_of_accounts){
        this.expects_chart_of_accounts = expects_chart_of_accounts ;
        this.expects_chart_of_accountsDirtyFlag = true ;
    }

     /**
     * 获取 [预计会计科目表]脏标记
     */
    @JsonIgnore
    public boolean getExpects_chart_of_accountsDirtyFlag(){
        return this.expects_chart_of_accountsDirtyFlag ;
    }   

    /**
     * 获取 [汇率损失科目]
     */
    @JsonProperty("expense_currency_exchange_account_id")
    public Integer getExpense_currency_exchange_account_id(){
        return this.expense_currency_exchange_account_id ;
    }

    /**
     * 设置 [汇率损失科目]
     */
    @JsonProperty("expense_currency_exchange_account_id")
    public void setExpense_currency_exchange_account_id(Integer  expense_currency_exchange_account_id){
        this.expense_currency_exchange_account_id = expense_currency_exchange_account_id ;
        this.expense_currency_exchange_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [汇率损失科目]脏标记
     */
    @JsonIgnore
    public boolean getExpense_currency_exchange_account_idDirtyFlag(){
        return this.expense_currency_exchange_account_idDirtyFlag ;
    }   

    /**
     * 获取 [文档模板]
     */
    @JsonProperty("external_report_layout_id")
    public Integer getExternal_report_layout_id(){
        return this.external_report_layout_id ;
    }

    /**
     * 设置 [文档模板]
     */
    @JsonProperty("external_report_layout_id")
    public void setExternal_report_layout_id(Integer  external_report_layout_id){
        this.external_report_layout_id = external_report_layout_id ;
        this.external_report_layout_idDirtyFlag = true ;
    }

     /**
     * 获取 [文档模板]脏标记
     */
    @JsonIgnore
    public boolean getExternal_report_layout_idDirtyFlag(){
        return this.external_report_layout_idDirtyFlag ;
    }   

    /**
     * 获取 [会计年度的最后一天]
     */
    @JsonProperty("fiscalyear_last_day")
    public Integer getFiscalyear_last_day(){
        return this.fiscalyear_last_day ;
    }

    /**
     * 设置 [会计年度的最后一天]
     */
    @JsonProperty("fiscalyear_last_day")
    public void setFiscalyear_last_day(Integer  fiscalyear_last_day){
        this.fiscalyear_last_day = fiscalyear_last_day ;
        this.fiscalyear_last_dayDirtyFlag = true ;
    }

     /**
     * 获取 [会计年度的最后一天]脏标记
     */
    @JsonIgnore
    public boolean getFiscalyear_last_dayDirtyFlag(){
        return this.fiscalyear_last_dayDirtyFlag ;
    }   

    /**
     * 获取 [会计年度的最后一个月]
     */
    @JsonProperty("fiscalyear_last_month")
    public String getFiscalyear_last_month(){
        return this.fiscalyear_last_month ;
    }

    /**
     * 设置 [会计年度的最后一个月]
     */
    @JsonProperty("fiscalyear_last_month")
    public void setFiscalyear_last_month(String  fiscalyear_last_month){
        this.fiscalyear_last_month = fiscalyear_last_month ;
        this.fiscalyear_last_monthDirtyFlag = true ;
    }

     /**
     * 获取 [会计年度的最后一个月]脏标记
     */
    @JsonIgnore
    public boolean getFiscalyear_last_monthDirtyFlag(){
        return this.fiscalyear_last_monthDirtyFlag ;
    }   

    /**
     * 获取 [锁定日期]
     */
    @JsonProperty("fiscalyear_lock_date")
    public Timestamp getFiscalyear_lock_date(){
        return this.fiscalyear_lock_date ;
    }

    /**
     * 设置 [锁定日期]
     */
    @JsonProperty("fiscalyear_lock_date")
    public void setFiscalyear_lock_date(Timestamp  fiscalyear_lock_date){
        this.fiscalyear_lock_date = fiscalyear_lock_date ;
        this.fiscalyear_lock_dateDirtyFlag = true ;
    }

     /**
     * 获取 [锁定日期]脏标记
     */
    @JsonIgnore
    public boolean getFiscalyear_lock_dateDirtyFlag(){
        return this.fiscalyear_lock_dateDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [汇率增益科目]
     */
    @JsonProperty("income_currency_exchange_account_id")
    public Integer getIncome_currency_exchange_account_id(){
        return this.income_currency_exchange_account_id ;
    }

    /**
     * 设置 [汇率增益科目]
     */
    @JsonProperty("income_currency_exchange_account_id")
    public void setIncome_currency_exchange_account_id(Integer  income_currency_exchange_account_id){
        this.income_currency_exchange_account_id = income_currency_exchange_account_id ;
        this.income_currency_exchange_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [汇率增益科目]脏标记
     */
    @JsonIgnore
    public boolean getIncome_currency_exchange_account_idDirtyFlag(){
        return this.income_currency_exchange_account_idDirtyFlag ;
    }   

    /**
     * 获取 [默认国际贸易术语]
     */
    @JsonProperty("incoterm_id")
    public Integer getIncoterm_id(){
        return this.incoterm_id ;
    }

    /**
     * 设置 [默认国际贸易术语]
     */
    @JsonProperty("incoterm_id")
    public void setIncoterm_id(Integer  incoterm_id){
        this.incoterm_id = incoterm_id ;
        this.incoterm_idDirtyFlag = true ;
    }

     /**
     * 获取 [默认国际贸易术语]脏标记
     */
    @JsonIgnore
    public boolean getIncoterm_idDirtyFlag(){
        return this.incoterm_idDirtyFlag ;
    }   

    /**
     * 获取 [默认国际贸易术语]
     */
    @JsonProperty("incoterm_id_text")
    public String getIncoterm_id_text(){
        return this.incoterm_id_text ;
    }

    /**
     * 设置 [默认国际贸易术语]
     */
    @JsonProperty("incoterm_id_text")
    public void setIncoterm_id_text(String  incoterm_id_text){
        this.incoterm_id_text = incoterm_id_text ;
        this.incoterm_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [默认国际贸易术语]脏标记
     */
    @JsonIgnore
    public boolean getIncoterm_id_textDirtyFlag(){
        return this.incoterm_id_textDirtyFlag ;
    }   

    /**
     * 获取 [内部中转位置]
     */
    @JsonProperty("internal_transit_location_id")
    public Integer getInternal_transit_location_id(){
        return this.internal_transit_location_id ;
    }

    /**
     * 设置 [内部中转位置]
     */
    @JsonProperty("internal_transit_location_id")
    public void setInternal_transit_location_id(Integer  internal_transit_location_id){
        this.internal_transit_location_id = internal_transit_location_id ;
        this.internal_transit_location_idDirtyFlag = true ;
    }

     /**
     * 获取 [内部中转位置]脏标记
     */
    @JsonIgnore
    public boolean getInternal_transit_location_idDirtyFlag(){
        return this.internal_transit_location_idDirtyFlag ;
    }   

    /**
     * 获取 [内部中转位置]
     */
    @JsonProperty("internal_transit_location_id_text")
    public String getInternal_transit_location_id_text(){
        return this.internal_transit_location_id_text ;
    }

    /**
     * 设置 [内部中转位置]
     */
    @JsonProperty("internal_transit_location_id_text")
    public void setInternal_transit_location_id_text(String  internal_transit_location_id_text){
        this.internal_transit_location_id_text = internal_transit_location_id_text ;
        this.internal_transit_location_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [内部中转位置]脏标记
     */
    @JsonIgnore
    public boolean getInternal_transit_location_id_textDirtyFlag(){
        return this.internal_transit_location_id_textDirtyFlag ;
    }   

    /**
     * 获取 [默认邮件]
     */
    @JsonProperty("invoice_is_email")
    public String getInvoice_is_email(){
        return this.invoice_is_email ;
    }

    /**
     * 设置 [默认邮件]
     */
    @JsonProperty("invoice_is_email")
    public void setInvoice_is_email(String  invoice_is_email){
        this.invoice_is_email = invoice_is_email ;
        this.invoice_is_emailDirtyFlag = true ;
    }

     /**
     * 获取 [默认邮件]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_is_emailDirtyFlag(){
        return this.invoice_is_emailDirtyFlag ;
    }   

    /**
     * 获取 [通过默认值打印]
     */
    @JsonProperty("invoice_is_print")
    public String getInvoice_is_print(){
        return this.invoice_is_print ;
    }

    /**
     * 设置 [通过默认值打印]
     */
    @JsonProperty("invoice_is_print")
    public void setInvoice_is_print(String  invoice_is_print){
        this.invoice_is_print = invoice_is_print ;
        this.invoice_is_printDirtyFlag = true ;
    }

     /**
     * 获取 [通过默认值打印]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_is_printDirtyFlag(){
        return this.invoice_is_printDirtyFlag ;
    }   

    /**
     * 获取 [默认以信件发送]
     */
    @JsonProperty("invoice_is_snailmail")
    public String getInvoice_is_snailmail(){
        return this.invoice_is_snailmail ;
    }

    /**
     * 设置 [默认以信件发送]
     */
    @JsonProperty("invoice_is_snailmail")
    public void setInvoice_is_snailmail(String  invoice_is_snailmail){
        this.invoice_is_snailmail = invoice_is_snailmail ;
        this.invoice_is_snailmailDirtyFlag = true ;
    }

     /**
     * 获取 [默认以信件发送]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_is_snailmailDirtyFlag(){
        return this.invoice_is_snailmailDirtyFlag ;
    }   

    /**
     * 获取 [默认信息类型]
     */
    @JsonProperty("invoice_reference_type")
    public String getInvoice_reference_type(){
        return this.invoice_reference_type ;
    }

    /**
     * 设置 [默认信息类型]
     */
    @JsonProperty("invoice_reference_type")
    public void setInvoice_reference_type(String  invoice_reference_type){
        this.invoice_reference_type = invoice_reference_type ;
        this.invoice_reference_typeDirtyFlag = true ;
    }

     /**
     * 获取 [默认信息类型]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_reference_typeDirtyFlag(){
        return this.invoice_reference_typeDirtyFlag ;
    }   

    /**
     * 获取 [公司 Logo]
     */
    @JsonProperty("logo")
    public byte[] getLogo(){
        return this.logo ;
    }

    /**
     * 设置 [公司 Logo]
     */
    @JsonProperty("logo")
    public void setLogo(byte[]  logo){
        this.logo = logo ;
        this.logoDirtyFlag = true ;
    }

     /**
     * 获取 [公司 Logo]脏标记
     */
    @JsonIgnore
    public boolean getLogoDirtyFlag(){
        return this.logoDirtyFlag ;
    }   

    /**
     * 获取 [网页徽标]
     */
    @JsonProperty("logo_web")
    public byte[] getLogo_web(){
        return this.logo_web ;
    }

    /**
     * 设置 [网页徽标]
     */
    @JsonProperty("logo_web")
    public void setLogo_web(byte[]  logo_web){
        this.logo_web = logo_web ;
        this.logo_webDirtyFlag = true ;
    }

     /**
     * 获取 [网页徽标]脏标记
     */
    @JsonIgnore
    public boolean getLogo_webDirtyFlag(){
        return this.logo_webDirtyFlag ;
    }   

    /**
     * 获取 [制造提前期(日)]
     */
    @JsonProperty("manufacturing_lead")
    public Double getManufacturing_lead(){
        return this.manufacturing_lead ;
    }

    /**
     * 设置 [制造提前期(日)]
     */
    @JsonProperty("manufacturing_lead")
    public void setManufacturing_lead(Double  manufacturing_lead){
        this.manufacturing_lead = manufacturing_lead ;
        this.manufacturing_leadDirtyFlag = true ;
    }

     /**
     * 获取 [制造提前期(日)]脏标记
     */
    @JsonIgnore
    public boolean getManufacturing_leadDirtyFlag(){
        return this.manufacturing_leadDirtyFlag ;
    }   

    /**
     * 获取 [公司名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [公司名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [公司名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [命名规则]
     */
    @JsonProperty("nomenclature_id")
    public Integer getNomenclature_id(){
        return this.nomenclature_id ;
    }

    /**
     * 设置 [命名规则]
     */
    @JsonProperty("nomenclature_id")
    public void setNomenclature_id(Integer  nomenclature_id){
        this.nomenclature_id = nomenclature_id ;
        this.nomenclature_idDirtyFlag = true ;
    }

     /**
     * 获取 [命名规则]脏标记
     */
    @JsonIgnore
    public boolean getNomenclature_idDirtyFlag(){
        return this.nomenclature_idDirtyFlag ;
    }   

    /**
     * 获取 [逾期追款消息]
     */
    @JsonProperty("overdue_msg")
    public String getOverdue_msg(){
        return this.overdue_msg ;
    }

    /**
     * 设置 [逾期追款消息]
     */
    @JsonProperty("overdue_msg")
    public void setOverdue_msg(String  overdue_msg){
        this.overdue_msg = overdue_msg ;
        this.overdue_msgDirtyFlag = true ;
    }

     /**
     * 获取 [逾期追款消息]脏标记
     */
    @JsonIgnore
    public boolean getOverdue_msgDirtyFlag(){
        return this.overdue_msgDirtyFlag ;
    }   

    /**
     * 获取 [纸张格式]
     */
    @JsonProperty("paperformat_id")
    public Integer getPaperformat_id(){
        return this.paperformat_id ;
    }

    /**
     * 设置 [纸张格式]
     */
    @JsonProperty("paperformat_id")
    public void setPaperformat_id(Integer  paperformat_id){
        this.paperformat_id = paperformat_id ;
        this.paperformat_idDirtyFlag = true ;
    }

     /**
     * 获取 [纸张格式]脏标记
     */
    @JsonIgnore
    public boolean getPaperformat_idDirtyFlag(){
        return this.paperformat_idDirtyFlag ;
    }   

    /**
     * 获取 [上级公司]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return this.parent_id ;
    }

    /**
     * 设置 [上级公司]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

     /**
     * 获取 [上级公司]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return this.parent_idDirtyFlag ;
    }   

    /**
     * 获取 [上级公司]
     */
    @JsonProperty("parent_id_text")
    public String getParent_id_text(){
        return this.parent_id_text ;
    }

    /**
     * 设置 [上级公司]
     */
    @JsonProperty("parent_id_text")
    public void setParent_id_text(String  parent_id_text){
        this.parent_id_text = parent_id_text ;
        this.parent_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [上级公司]脏标记
     */
    @JsonIgnore
    public boolean getParent_id_textDirtyFlag(){
        return this.parent_id_textDirtyFlag ;
    }   

    /**
     * 获取 [公司数据库ID]
     */
    @JsonProperty("partner_gid")
    public Integer getPartner_gid(){
        return this.partner_gid ;
    }

    /**
     * 设置 [公司数据库ID]
     */
    @JsonProperty("partner_gid")
    public void setPartner_gid(Integer  partner_gid){
        this.partner_gid = partner_gid ;
        this.partner_gidDirtyFlag = true ;
    }

     /**
     * 获取 [公司数据库ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_gidDirtyFlag(){
        return this.partner_gidDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [入职支付收单机构的状态]
     */
    @JsonProperty("payment_acquirer_onboarding_state")
    public String getPayment_acquirer_onboarding_state(){
        return this.payment_acquirer_onboarding_state ;
    }

    /**
     * 设置 [入职支付收单机构的状态]
     */
    @JsonProperty("payment_acquirer_onboarding_state")
    public void setPayment_acquirer_onboarding_state(String  payment_acquirer_onboarding_state){
        this.payment_acquirer_onboarding_state = payment_acquirer_onboarding_state ;
        this.payment_acquirer_onboarding_stateDirtyFlag = true ;
    }

     /**
     * 获取 [入职支付收单机构的状态]脏标记
     */
    @JsonIgnore
    public boolean getPayment_acquirer_onboarding_stateDirtyFlag(){
        return this.payment_acquirer_onboarding_stateDirtyFlag ;
    }   

    /**
     * 获取 [选择付款方式]
     */
    @JsonProperty("payment_onboarding_payment_method")
    public String getPayment_onboarding_payment_method(){
        return this.payment_onboarding_payment_method ;
    }

    /**
     * 设置 [选择付款方式]
     */
    @JsonProperty("payment_onboarding_payment_method")
    public void setPayment_onboarding_payment_method(String  payment_onboarding_payment_method){
        this.payment_onboarding_payment_method = payment_onboarding_payment_method ;
        this.payment_onboarding_payment_methodDirtyFlag = true ;
    }

     /**
     * 获取 [选择付款方式]脏标记
     */
    @JsonIgnore
    public boolean getPayment_onboarding_payment_methodDirtyFlag(){
        return this.payment_onboarding_payment_methodDirtyFlag ;
    }   

    /**
     * 获取 [非顾问的锁定日期]
     */
    @JsonProperty("period_lock_date")
    public Timestamp getPeriod_lock_date(){
        return this.period_lock_date ;
    }

    /**
     * 设置 [非顾问的锁定日期]
     */
    @JsonProperty("period_lock_date")
    public void setPeriod_lock_date(Timestamp  period_lock_date){
        this.period_lock_date = period_lock_date ;
        this.period_lock_dateDirtyFlag = true ;
    }

     /**
     * 获取 [非顾问的锁定日期]脏标记
     */
    @JsonIgnore
    public boolean getPeriod_lock_dateDirtyFlag(){
        return this.period_lock_dateDirtyFlag ;
    }   

    /**
     * 获取 [电话]
     */
    @JsonProperty("phone")
    public String getPhone(){
        return this.phone ;
    }

    /**
     * 设置 [电话]
     */
    @JsonProperty("phone")
    public void setPhone(String  phone){
        this.phone = phone ;
        this.phoneDirtyFlag = true ;
    }

     /**
     * 获取 [电话]脏标记
     */
    @JsonIgnore
    public boolean getPhoneDirtyFlag(){
        return this.phoneDirtyFlag ;
    }   

    /**
     * 获取 [在线支付]
     */
    @JsonProperty("portal_confirmation_pay")
    public String getPortal_confirmation_pay(){
        return this.portal_confirmation_pay ;
    }

    /**
     * 设置 [在线支付]
     */
    @JsonProperty("portal_confirmation_pay")
    public void setPortal_confirmation_pay(String  portal_confirmation_pay){
        this.portal_confirmation_pay = portal_confirmation_pay ;
        this.portal_confirmation_payDirtyFlag = true ;
    }

     /**
     * 获取 [在线支付]脏标记
     */
    @JsonIgnore
    public boolean getPortal_confirmation_payDirtyFlag(){
        return this.portal_confirmation_payDirtyFlag ;
    }   

    /**
     * 获取 [在线签名]
     */
    @JsonProperty("portal_confirmation_sign")
    public String getPortal_confirmation_sign(){
        return this.portal_confirmation_sign ;
    }

    /**
     * 设置 [在线签名]
     */
    @JsonProperty("portal_confirmation_sign")
    public void setPortal_confirmation_sign(String  portal_confirmation_sign){
        this.portal_confirmation_sign = portal_confirmation_sign ;
        this.portal_confirmation_signDirtyFlag = true ;
    }

     /**
     * 获取 [在线签名]脏标记
     */
    @JsonIgnore
    public boolean getPortal_confirmation_signDirtyFlag(){
        return this.portal_confirmation_signDirtyFlag ;
    }   

    /**
     * 获取 [批准等级]
     */
    @JsonProperty("po_double_validation")
    public String getPo_double_validation(){
        return this.po_double_validation ;
    }

    /**
     * 设置 [批准等级]
     */
    @JsonProperty("po_double_validation")
    public void setPo_double_validation(String  po_double_validation){
        this.po_double_validation = po_double_validation ;
        this.po_double_validationDirtyFlag = true ;
    }

     /**
     * 获取 [批准等级]脏标记
     */
    @JsonIgnore
    public boolean getPo_double_validationDirtyFlag(){
        return this.po_double_validationDirtyFlag ;
    }   

    /**
     * 获取 [再次验证金额]
     */
    @JsonProperty("po_double_validation_amount")
    public Double getPo_double_validation_amount(){
        return this.po_double_validation_amount ;
    }

    /**
     * 设置 [再次验证金额]
     */
    @JsonProperty("po_double_validation_amount")
    public void setPo_double_validation_amount(Double  po_double_validation_amount){
        this.po_double_validation_amount = po_double_validation_amount ;
        this.po_double_validation_amountDirtyFlag = true ;
    }

     /**
     * 获取 [再次验证金额]脏标记
     */
    @JsonIgnore
    public boolean getPo_double_validation_amountDirtyFlag(){
        return this.po_double_validation_amountDirtyFlag ;
    }   

    /**
     * 获取 [采购提前时间]
     */
    @JsonProperty("po_lead")
    public Double getPo_lead(){
        return this.po_lead ;
    }

    /**
     * 设置 [采购提前时间]
     */
    @JsonProperty("po_lead")
    public void setPo_lead(Double  po_lead){
        this.po_lead = po_lead ;
        this.po_leadDirtyFlag = true ;
    }

     /**
     * 获取 [采购提前时间]脏标记
     */
    @JsonIgnore
    public boolean getPo_leadDirtyFlag(){
        return this.po_leadDirtyFlag ;
    }   

    /**
     * 获取 [销售订单修改]
     */
    @JsonProperty("po_lock")
    public String getPo_lock(){
        return this.po_lock ;
    }

    /**
     * 设置 [销售订单修改]
     */
    @JsonProperty("po_lock")
    public void setPo_lock(String  po_lock){
        this.po_lock = po_lock ;
        this.po_lockDirtyFlag = true ;
    }

     /**
     * 获取 [销售订单修改]脏标记
     */
    @JsonIgnore
    public boolean getPo_lockDirtyFlag(){
        return this.po_lockDirtyFlag ;
    }   

    /**
     * 获取 [连接在一起的库存移动的日期变化传播的最小差值。]
     */
    @JsonProperty("propagation_minimum_delta")
    public Integer getPropagation_minimum_delta(){
        return this.propagation_minimum_delta ;
    }

    /**
     * 设置 [连接在一起的库存移动的日期变化传播的最小差值。]
     */
    @JsonProperty("propagation_minimum_delta")
    public void setPropagation_minimum_delta(Integer  propagation_minimum_delta){
        this.propagation_minimum_delta = propagation_minimum_delta ;
        this.propagation_minimum_deltaDirtyFlag = true ;
    }

     /**
     * 获取 [连接在一起的库存移动的日期变化传播的最小差值。]脏标记
     */
    @JsonIgnore
    public boolean getPropagation_minimum_deltaDirtyFlag(){
        return this.propagation_minimum_deltaDirtyFlag ;
    }   

    /**
     * 获取 [库存计价的入库科目]
     */
    @JsonProperty("property_stock_account_input_categ_id")
    public Integer getProperty_stock_account_input_categ_id(){
        return this.property_stock_account_input_categ_id ;
    }

    /**
     * 设置 [库存计价的入库科目]
     */
    @JsonProperty("property_stock_account_input_categ_id")
    public void setProperty_stock_account_input_categ_id(Integer  property_stock_account_input_categ_id){
        this.property_stock_account_input_categ_id = property_stock_account_input_categ_id ;
        this.property_stock_account_input_categ_idDirtyFlag = true ;
    }

     /**
     * 获取 [库存计价的入库科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_account_input_categ_idDirtyFlag(){
        return this.property_stock_account_input_categ_idDirtyFlag ;
    }   

    /**
     * 获取 [库存计价的入库科目]
     */
    @JsonProperty("property_stock_account_input_categ_id_text")
    public String getProperty_stock_account_input_categ_id_text(){
        return this.property_stock_account_input_categ_id_text ;
    }

    /**
     * 设置 [库存计价的入库科目]
     */
    @JsonProperty("property_stock_account_input_categ_id_text")
    public void setProperty_stock_account_input_categ_id_text(String  property_stock_account_input_categ_id_text){
        this.property_stock_account_input_categ_id_text = property_stock_account_input_categ_id_text ;
        this.property_stock_account_input_categ_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [库存计价的入库科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_account_input_categ_id_textDirtyFlag(){
        return this.property_stock_account_input_categ_id_textDirtyFlag ;
    }   

    /**
     * 获取 [库存计价的出货科目]
     */
    @JsonProperty("property_stock_account_output_categ_id")
    public Integer getProperty_stock_account_output_categ_id(){
        return this.property_stock_account_output_categ_id ;
    }

    /**
     * 设置 [库存计价的出货科目]
     */
    @JsonProperty("property_stock_account_output_categ_id")
    public void setProperty_stock_account_output_categ_id(Integer  property_stock_account_output_categ_id){
        this.property_stock_account_output_categ_id = property_stock_account_output_categ_id ;
        this.property_stock_account_output_categ_idDirtyFlag = true ;
    }

     /**
     * 获取 [库存计价的出货科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_account_output_categ_idDirtyFlag(){
        return this.property_stock_account_output_categ_idDirtyFlag ;
    }   

    /**
     * 获取 [库存计价的出货科目]
     */
    @JsonProperty("property_stock_account_output_categ_id_text")
    public String getProperty_stock_account_output_categ_id_text(){
        return this.property_stock_account_output_categ_id_text ;
    }

    /**
     * 设置 [库存计价的出货科目]
     */
    @JsonProperty("property_stock_account_output_categ_id_text")
    public void setProperty_stock_account_output_categ_id_text(String  property_stock_account_output_categ_id_text){
        this.property_stock_account_output_categ_id_text = property_stock_account_output_categ_id_text ;
        this.property_stock_account_output_categ_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [库存计价的出货科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_account_output_categ_id_textDirtyFlag(){
        return this.property_stock_account_output_categ_id_textDirtyFlag ;
    }   

    /**
     * 获取 [库存计价的科目模板]
     */
    @JsonProperty("property_stock_valuation_account_id")
    public Integer getProperty_stock_valuation_account_id(){
        return this.property_stock_valuation_account_id ;
    }

    /**
     * 设置 [库存计价的科目模板]
     */
    @JsonProperty("property_stock_valuation_account_id")
    public void setProperty_stock_valuation_account_id(Integer  property_stock_valuation_account_id){
        this.property_stock_valuation_account_id = property_stock_valuation_account_id ;
        this.property_stock_valuation_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [库存计价的科目模板]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_valuation_account_idDirtyFlag(){
        return this.property_stock_valuation_account_idDirtyFlag ;
    }   

    /**
     * 获取 [库存计价的科目模板]
     */
    @JsonProperty("property_stock_valuation_account_id_text")
    public String getProperty_stock_valuation_account_id_text(){
        return this.property_stock_valuation_account_id_text ;
    }

    /**
     * 设置 [库存计价的科目模板]
     */
    @JsonProperty("property_stock_valuation_account_id_text")
    public void setProperty_stock_valuation_account_id_text(String  property_stock_valuation_account_id_text){
        this.property_stock_valuation_account_id_text = property_stock_valuation_account_id_text ;
        this.property_stock_valuation_account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [库存计价的科目模板]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_valuation_account_id_textDirtyFlag(){
        return this.property_stock_valuation_account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [显示SEPA QR码]
     */
    @JsonProperty("qr_code")
    public String getQr_code(){
        return this.qr_code ;
    }

    /**
     * 设置 [显示SEPA QR码]
     */
    @JsonProperty("qr_code")
    public void setQr_code(String  qr_code){
        this.qr_code = qr_code ;
        this.qr_codeDirtyFlag = true ;
    }

     /**
     * 获取 [显示SEPA QR码]脏标记
     */
    @JsonIgnore
    public boolean getQr_codeDirtyFlag(){
        return this.qr_codeDirtyFlag ;
    }   

    /**
     * 获取 [默认报价有效期（日）]
     */
    @JsonProperty("quotation_validity_days")
    public Integer getQuotation_validity_days(){
        return this.quotation_validity_days ;
    }

    /**
     * 设置 [默认报价有效期（日）]
     */
    @JsonProperty("quotation_validity_days")
    public void setQuotation_validity_days(Integer  quotation_validity_days){
        this.quotation_validity_days = quotation_validity_days ;
        this.quotation_validity_daysDirtyFlag = true ;
    }

     /**
     * 获取 [默认报价有效期（日）]脏标记
     */
    @JsonIgnore
    public boolean getQuotation_validity_daysDirtyFlag(){
        return this.quotation_validity_daysDirtyFlag ;
    }   

    /**
     * 获取 [报表页脚]
     */
    @JsonProperty("report_footer")
    public String getReport_footer(){
        return this.report_footer ;
    }

    /**
     * 设置 [报表页脚]
     */
    @JsonProperty("report_footer")
    public void setReport_footer(String  report_footer){
        this.report_footer = report_footer ;
        this.report_footerDirtyFlag = true ;
    }

     /**
     * 获取 [报表页脚]脏标记
     */
    @JsonIgnore
    public boolean getReport_footerDirtyFlag(){
        return this.report_footerDirtyFlag ;
    }   

    /**
     * 获取 [公司口号]
     */
    @JsonProperty("report_header")
    public String getReport_header(){
        return this.report_header ;
    }

    /**
     * 设置 [公司口号]
     */
    @JsonProperty("report_header")
    public void setReport_header(String  report_header){
        this.report_header = report_header ;
        this.report_headerDirtyFlag = true ;
    }

     /**
     * 获取 [公司口号]脏标记
     */
    @JsonIgnore
    public boolean getReport_headerDirtyFlag(){
        return this.report_headerDirtyFlag ;
    }   

    /**
     * 获取 [默认工作时间]
     */
    @JsonProperty("resource_calendar_id")
    public Integer getResource_calendar_id(){
        return this.resource_calendar_id ;
    }

    /**
     * 设置 [默认工作时间]
     */
    @JsonProperty("resource_calendar_id")
    public void setResource_calendar_id(Integer  resource_calendar_id){
        this.resource_calendar_id = resource_calendar_id ;
        this.resource_calendar_idDirtyFlag = true ;
    }

     /**
     * 获取 [默认工作时间]脏标记
     */
    @JsonIgnore
    public boolean getResource_calendar_idDirtyFlag(){
        return this.resource_calendar_idDirtyFlag ;
    }   

    /**
     * 获取 [工作时间]
     */
    @JsonProperty("resource_calendar_ids")
    public String getResource_calendar_ids(){
        return this.resource_calendar_ids ;
    }

    /**
     * 设置 [工作时间]
     */
    @JsonProperty("resource_calendar_ids")
    public void setResource_calendar_ids(String  resource_calendar_ids){
        this.resource_calendar_ids = resource_calendar_ids ;
        this.resource_calendar_idsDirtyFlag = true ;
    }

     /**
     * 获取 [工作时间]脏标记
     */
    @JsonIgnore
    public boolean getResource_calendar_idsDirtyFlag(){
        return this.resource_calendar_idsDirtyFlag ;
    }   

    /**
     * 获取 [默认工作时间]
     */
    @JsonProperty("resource_calendar_id_text")
    public String getResource_calendar_id_text(){
        return this.resource_calendar_id_text ;
    }

    /**
     * 设置 [默认工作时间]
     */
    @JsonProperty("resource_calendar_id_text")
    public void setResource_calendar_id_text(String  resource_calendar_id_text){
        this.resource_calendar_id_text = resource_calendar_id_text ;
        this.resource_calendar_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [默认工作时间]脏标记
     */
    @JsonIgnore
    public boolean getResource_calendar_id_textDirtyFlag(){
        return this.resource_calendar_id_textDirtyFlag ;
    }   

    /**
     * 获取 [默认条款和条件]
     */
    @JsonProperty("sale_note")
    public String getSale_note(){
        return this.sale_note ;
    }

    /**
     * 设置 [默认条款和条件]
     */
    @JsonProperty("sale_note")
    public void setSale_note(String  sale_note){
        this.sale_note = sale_note ;
        this.sale_noteDirtyFlag = true ;
    }

     /**
     * 获取 [默认条款和条件]脏标记
     */
    @JsonIgnore
    public boolean getSale_noteDirtyFlag(){
        return this.sale_noteDirtyFlag ;
    }   

    /**
     * 获取 [有待被确认的订单步骤的状态]
     */
    @JsonProperty("sale_onboarding_order_confirmation_state")
    public String getSale_onboarding_order_confirmation_state(){
        return this.sale_onboarding_order_confirmation_state ;
    }

    /**
     * 设置 [有待被确认的订单步骤的状态]
     */
    @JsonProperty("sale_onboarding_order_confirmation_state")
    public void setSale_onboarding_order_confirmation_state(String  sale_onboarding_order_confirmation_state){
        this.sale_onboarding_order_confirmation_state = sale_onboarding_order_confirmation_state ;
        this.sale_onboarding_order_confirmation_stateDirtyFlag = true ;
    }

     /**
     * 获取 [有待被确认的订单步骤的状态]脏标记
     */
    @JsonIgnore
    public boolean getSale_onboarding_order_confirmation_stateDirtyFlag(){
        return this.sale_onboarding_order_confirmation_stateDirtyFlag ;
    }   

    /**
     * 获取 [请选择付款方式]
     */
    @JsonProperty("sale_onboarding_payment_method")
    public String getSale_onboarding_payment_method(){
        return this.sale_onboarding_payment_method ;
    }

    /**
     * 设置 [请选择付款方式]
     */
    @JsonProperty("sale_onboarding_payment_method")
    public void setSale_onboarding_payment_method(String  sale_onboarding_payment_method){
        this.sale_onboarding_payment_method = sale_onboarding_payment_method ;
        this.sale_onboarding_payment_methodDirtyFlag = true ;
    }

     /**
     * 获取 [请选择付款方式]脏标记
     */
    @JsonIgnore
    public boolean getSale_onboarding_payment_methodDirtyFlag(){
        return this.sale_onboarding_payment_methodDirtyFlag ;
    }   

    /**
     * 获取 [有待被确认的样品报价单步骤的状态]
     */
    @JsonProperty("sale_onboarding_sample_quotation_state")
    public String getSale_onboarding_sample_quotation_state(){
        return this.sale_onboarding_sample_quotation_state ;
    }

    /**
     * 设置 [有待被确认的样品报价单步骤的状态]
     */
    @JsonProperty("sale_onboarding_sample_quotation_state")
    public void setSale_onboarding_sample_quotation_state(String  sale_onboarding_sample_quotation_state){
        this.sale_onboarding_sample_quotation_state = sale_onboarding_sample_quotation_state ;
        this.sale_onboarding_sample_quotation_stateDirtyFlag = true ;
    }

     /**
     * 获取 [有待被确认的样品报价单步骤的状态]脏标记
     */
    @JsonIgnore
    public boolean getSale_onboarding_sample_quotation_stateDirtyFlag(){
        return this.sale_onboarding_sample_quotation_stateDirtyFlag ;
    }   

    /**
     * 获取 [正进行销售面板的状态]
     */
    @JsonProperty("sale_quotation_onboarding_state")
    public String getSale_quotation_onboarding_state(){
        return this.sale_quotation_onboarding_state ;
    }

    /**
     * 设置 [正进行销售面板的状态]
     */
    @JsonProperty("sale_quotation_onboarding_state")
    public void setSale_quotation_onboarding_state(String  sale_quotation_onboarding_state){
        this.sale_quotation_onboarding_state = sale_quotation_onboarding_state ;
        this.sale_quotation_onboarding_stateDirtyFlag = true ;
    }

     /**
     * 获取 [正进行销售面板的状态]脏标记
     */
    @JsonIgnore
    public boolean getSale_quotation_onboarding_stateDirtyFlag(){
        return this.sale_quotation_onboarding_stateDirtyFlag ;
    }   

    /**
     * 获取 [销售安全天数]
     */
    @JsonProperty("security_lead")
    public Double getSecurity_lead(){
        return this.security_lead ;
    }

    /**
     * 设置 [销售安全天数]
     */
    @JsonProperty("security_lead")
    public void setSecurity_lead(Double  security_lead){
        this.security_lead = security_lead ;
        this.security_leadDirtyFlag = true ;
    }

     /**
     * 获取 [销售安全天数]脏标记
     */
    @JsonIgnore
    public boolean getSecurity_leadDirtyFlag(){
        return this.security_leadDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [颜色]
     */
    @JsonProperty("snailmail_color")
    public String getSnailmail_color(){
        return this.snailmail_color ;
    }

    /**
     * 设置 [颜色]
     */
    @JsonProperty("snailmail_color")
    public void setSnailmail_color(String  snailmail_color){
        this.snailmail_color = snailmail_color ;
        this.snailmail_colorDirtyFlag = true ;
    }

     /**
     * 获取 [颜色]脏标记
     */
    @JsonIgnore
    public boolean getSnailmail_colorDirtyFlag(){
        return this.snailmail_colorDirtyFlag ;
    }   

    /**
     * 获取 [双面]
     */
    @JsonProperty("snailmail_duplex")
    public String getSnailmail_duplex(){
        return this.snailmail_duplex ;
    }

    /**
     * 设置 [双面]
     */
    @JsonProperty("snailmail_duplex")
    public void setSnailmail_duplex(String  snailmail_duplex){
        this.snailmail_duplex = snailmail_duplex ;
        this.snailmail_duplexDirtyFlag = true ;
    }

     /**
     * 获取 [双面]脏标记
     */
    @JsonIgnore
    public boolean getSnailmail_duplexDirtyFlag(){
        return this.snailmail_duplexDirtyFlag ;
    }   

    /**
     * 获取 [脸书账号]
     */
    @JsonProperty("social_facebook")
    public String getSocial_facebook(){
        return this.social_facebook ;
    }

    /**
     * 设置 [脸书账号]
     */
    @JsonProperty("social_facebook")
    public void setSocial_facebook(String  social_facebook){
        this.social_facebook = social_facebook ;
        this.social_facebookDirtyFlag = true ;
    }

     /**
     * 获取 [脸书账号]脏标记
     */
    @JsonIgnore
    public boolean getSocial_facebookDirtyFlag(){
        return this.social_facebookDirtyFlag ;
    }   

    /**
     * 获取 [GitHub账户]
     */
    @JsonProperty("social_github")
    public String getSocial_github(){
        return this.social_github ;
    }

    /**
     * 设置 [GitHub账户]
     */
    @JsonProperty("social_github")
    public void setSocial_github(String  social_github){
        this.social_github = social_github ;
        this.social_githubDirtyFlag = true ;
    }

     /**
     * 获取 [GitHub账户]脏标记
     */
    @JsonIgnore
    public boolean getSocial_githubDirtyFlag(){
        return this.social_githubDirtyFlag ;
    }   

    /**
     * 获取 [Google+账户]
     */
    @JsonProperty("social_googleplus")
    public String getSocial_googleplus(){
        return this.social_googleplus ;
    }

    /**
     * 设置 [Google+账户]
     */
    @JsonProperty("social_googleplus")
    public void setSocial_googleplus(String  social_googleplus){
        this.social_googleplus = social_googleplus ;
        this.social_googleplusDirtyFlag = true ;
    }

     /**
     * 获取 [Google+账户]脏标记
     */
    @JsonIgnore
    public boolean getSocial_googleplusDirtyFlag(){
        return this.social_googleplusDirtyFlag ;
    }   

    /**
     * 获取 [Instagram 账号]
     */
    @JsonProperty("social_instagram")
    public String getSocial_instagram(){
        return this.social_instagram ;
    }

    /**
     * 设置 [Instagram 账号]
     */
    @JsonProperty("social_instagram")
    public void setSocial_instagram(String  social_instagram){
        this.social_instagram = social_instagram ;
        this.social_instagramDirtyFlag = true ;
    }

     /**
     * 获取 [Instagram 账号]脏标记
     */
    @JsonIgnore
    public boolean getSocial_instagramDirtyFlag(){
        return this.social_instagramDirtyFlag ;
    }   

    /**
     * 获取 [领英账号]
     */
    @JsonProperty("social_linkedin")
    public String getSocial_linkedin(){
        return this.social_linkedin ;
    }

    /**
     * 设置 [领英账号]
     */
    @JsonProperty("social_linkedin")
    public void setSocial_linkedin(String  social_linkedin){
        this.social_linkedin = social_linkedin ;
        this.social_linkedinDirtyFlag = true ;
    }

     /**
     * 获取 [领英账号]脏标记
     */
    @JsonIgnore
    public boolean getSocial_linkedinDirtyFlag(){
        return this.social_linkedinDirtyFlag ;
    }   

    /**
     * 获取 [Twitter账号]
     */
    @JsonProperty("social_twitter")
    public String getSocial_twitter(){
        return this.social_twitter ;
    }

    /**
     * 设置 [Twitter账号]
     */
    @JsonProperty("social_twitter")
    public void setSocial_twitter(String  social_twitter){
        this.social_twitter = social_twitter ;
        this.social_twitterDirtyFlag = true ;
    }

     /**
     * 获取 [Twitter账号]脏标记
     */
    @JsonIgnore
    public boolean getSocial_twitterDirtyFlag(){
        return this.social_twitterDirtyFlag ;
    }   

    /**
     * 获取 [Youtube账号]
     */
    @JsonProperty("social_youtube")
    public String getSocial_youtube(){
        return this.social_youtube ;
    }

    /**
     * 设置 [Youtube账号]
     */
    @JsonProperty("social_youtube")
    public void setSocial_youtube(String  social_youtube){
        this.social_youtube = social_youtube ;
        this.social_youtubeDirtyFlag = true ;
    }

     /**
     * 获取 [Youtube账号]脏标记
     */
    @JsonIgnore
    public boolean getSocial_youtubeDirtyFlag(){
        return this.social_youtubeDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state_id")
    public Integer getState_id(){
        return this.state_id ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state_id")
    public void setState_id(Integer  state_id){
        this.state_id = state_id ;
        this.state_idDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getState_idDirtyFlag(){
        return this.state_idDirtyFlag ;
    }   

    /**
     * 获取 [街道]
     */
    @JsonProperty("street")
    public String getStreet(){
        return this.street ;
    }

    /**
     * 设置 [街道]
     */
    @JsonProperty("street")
    public void setStreet(String  street){
        this.street = street ;
        this.streetDirtyFlag = true ;
    }

     /**
     * 获取 [街道]脏标记
     */
    @JsonIgnore
    public boolean getStreetDirtyFlag(){
        return this.streetDirtyFlag ;
    }   

    /**
     * 获取 [街道 2]
     */
    @JsonProperty("street2")
    public String getStreet2(){
        return this.street2 ;
    }

    /**
     * 设置 [街道 2]
     */
    @JsonProperty("street2")
    public void setStreet2(String  street2){
        this.street2 = street2 ;
        this.street2DirtyFlag = true ;
    }

     /**
     * 获取 [街道 2]脏标记
     */
    @JsonIgnore
    public boolean getStreet2DirtyFlag(){
        return this.street2DirtyFlag ;
    }   

    /**
     * 获取 [税率计算的舍入方法]
     */
    @JsonProperty("tax_calculation_rounding_method")
    public String getTax_calculation_rounding_method(){
        return this.tax_calculation_rounding_method ;
    }

    /**
     * 设置 [税率计算的舍入方法]
     */
    @JsonProperty("tax_calculation_rounding_method")
    public void setTax_calculation_rounding_method(String  tax_calculation_rounding_method){
        this.tax_calculation_rounding_method = tax_calculation_rounding_method ;
        this.tax_calculation_rounding_methodDirtyFlag = true ;
    }

     /**
     * 获取 [税率计算的舍入方法]脏标记
     */
    @JsonIgnore
    public boolean getTax_calculation_rounding_methodDirtyFlag(){
        return this.tax_calculation_rounding_methodDirtyFlag ;
    }   

    /**
     * 获取 [现金收付制日记账]
     */
    @JsonProperty("tax_cash_basis_journal_id")
    public Integer getTax_cash_basis_journal_id(){
        return this.tax_cash_basis_journal_id ;
    }

    /**
     * 设置 [现金收付制日记账]
     */
    @JsonProperty("tax_cash_basis_journal_id")
    public void setTax_cash_basis_journal_id(Integer  tax_cash_basis_journal_id){
        this.tax_cash_basis_journal_id = tax_cash_basis_journal_id ;
        this.tax_cash_basis_journal_idDirtyFlag = true ;
    }

     /**
     * 获取 [现金收付制日记账]脏标记
     */
    @JsonIgnore
    public boolean getTax_cash_basis_journal_idDirtyFlag(){
        return this.tax_cash_basis_journal_idDirtyFlag ;
    }   

    /**
     * 获取 [现金收付制日记账]
     */
    @JsonProperty("tax_cash_basis_journal_id_text")
    public String getTax_cash_basis_journal_id_text(){
        return this.tax_cash_basis_journal_id_text ;
    }

    /**
     * 设置 [现金收付制日记账]
     */
    @JsonProperty("tax_cash_basis_journal_id_text")
    public void setTax_cash_basis_journal_id_text(String  tax_cash_basis_journal_id_text){
        this.tax_cash_basis_journal_id_text = tax_cash_basis_journal_id_text ;
        this.tax_cash_basis_journal_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [现金收付制日记账]脏标记
     */
    @JsonIgnore
    public boolean getTax_cash_basis_journal_id_textDirtyFlag(){
        return this.tax_cash_basis_journal_id_textDirtyFlag ;
    }   

    /**
     * 获取 [使用现金收付制]
     */
    @JsonProperty("tax_exigibility")
    public String getTax_exigibility(){
        return this.tax_exigibility ;
    }

    /**
     * 设置 [使用现金收付制]
     */
    @JsonProperty("tax_exigibility")
    public void setTax_exigibility(String  tax_exigibility){
        this.tax_exigibility = tax_exigibility ;
        this.tax_exigibilityDirtyFlag = true ;
    }

     /**
     * 获取 [使用现金收付制]脏标记
     */
    @JsonIgnore
    public boolean getTax_exigibilityDirtyFlag(){
        return this.tax_exigibilityDirtyFlag ;
    }   

    /**
     * 获取 [转账帐户的前缀]
     */
    @JsonProperty("transfer_account_code_prefix")
    public String getTransfer_account_code_prefix(){
        return this.transfer_account_code_prefix ;
    }

    /**
     * 设置 [转账帐户的前缀]
     */
    @JsonProperty("transfer_account_code_prefix")
    public void setTransfer_account_code_prefix(String  transfer_account_code_prefix){
        this.transfer_account_code_prefix = transfer_account_code_prefix ;
        this.transfer_account_code_prefixDirtyFlag = true ;
    }

     /**
     * 获取 [转账帐户的前缀]脏标记
     */
    @JsonIgnore
    public boolean getTransfer_account_code_prefixDirtyFlag(){
        return this.transfer_account_code_prefixDirtyFlag ;
    }   

    /**
     * 获取 [银行间转账科目]
     */
    @JsonProperty("transfer_account_id")
    public Integer getTransfer_account_id(){
        return this.transfer_account_id ;
    }

    /**
     * 设置 [银行间转账科目]
     */
    @JsonProperty("transfer_account_id")
    public void setTransfer_account_id(Integer  transfer_account_id){
        this.transfer_account_id = transfer_account_id ;
        this.transfer_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [银行间转账科目]脏标记
     */
    @JsonIgnore
    public boolean getTransfer_account_idDirtyFlag(){
        return this.transfer_account_idDirtyFlag ;
    }   

    /**
     * 获取 [银行间转账科目]
     */
    @JsonProperty("transfer_account_id_text")
    public String getTransfer_account_id_text(){
        return this.transfer_account_id_text ;
    }

    /**
     * 设置 [银行间转账科目]
     */
    @JsonProperty("transfer_account_id_text")
    public void setTransfer_account_id_text(String  transfer_account_id_text){
        this.transfer_account_id_text = transfer_account_id_text ;
        this.transfer_account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [银行间转账科目]脏标记
     */
    @JsonIgnore
    public boolean getTransfer_account_id_textDirtyFlag(){
        return this.transfer_account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [接受的用户]
     */
    @JsonProperty("user_ids")
    public String getUser_ids(){
        return this.user_ids ;
    }

    /**
     * 设置 [接受的用户]
     */
    @JsonProperty("user_ids")
    public void setUser_ids(String  user_ids){
        this.user_ids = user_ids ;
        this.user_idsDirtyFlag = true ;
    }

     /**
     * 获取 [接受的用户]脏标记
     */
    @JsonIgnore
    public boolean getUser_idsDirtyFlag(){
        return this.user_idsDirtyFlag ;
    }   

    /**
     * 获取 [税号]
     */
    @JsonProperty("vat")
    public String getVat(){
        return this.vat ;
    }

    /**
     * 设置 [税号]
     */
    @JsonProperty("vat")
    public void setVat(String  vat){
        this.vat = vat ;
        this.vatDirtyFlag = true ;
    }

     /**
     * 获取 [税号]脏标记
     */
    @JsonIgnore
    public boolean getVatDirtyFlag(){
        return this.vatDirtyFlag ;
    }   

    /**
     * 获取 [网站]
     */
    @JsonProperty("website")
    public String getWebsite(){
        return this.website ;
    }

    /**
     * 设置 [网站]
     */
    @JsonProperty("website")
    public void setWebsite(String  website){
        this.website = website ;
        this.websiteDirtyFlag = true ;
    }

     /**
     * 获取 [网站]脏标记
     */
    @JsonIgnore
    public boolean getWebsiteDirtyFlag(){
        return this.websiteDirtyFlag ;
    }   

    /**
     * 获取 [网站销售状态入职付款收单机构步骤]
     */
    @JsonProperty("website_sale_onboarding_payment_acquirer_state")
    public String getWebsite_sale_onboarding_payment_acquirer_state(){
        return this.website_sale_onboarding_payment_acquirer_state ;
    }

    /**
     * 设置 [网站销售状态入职付款收单机构步骤]
     */
    @JsonProperty("website_sale_onboarding_payment_acquirer_state")
    public void setWebsite_sale_onboarding_payment_acquirer_state(String  website_sale_onboarding_payment_acquirer_state){
        this.website_sale_onboarding_payment_acquirer_state = website_sale_onboarding_payment_acquirer_state ;
        this.website_sale_onboarding_payment_acquirer_stateDirtyFlag = true ;
    }

     /**
     * 获取 [网站销售状态入职付款收单机构步骤]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_sale_onboarding_payment_acquirer_stateDirtyFlag(){
        return this.website_sale_onboarding_payment_acquirer_stateDirtyFlag ;
    }   

    /**
     * 获取 [招聘网站主题一步完成]
     */
    @JsonProperty("website_theme_onboarding_done")
    public String getWebsite_theme_onboarding_done(){
        return this.website_theme_onboarding_done ;
    }

    /**
     * 设置 [招聘网站主题一步完成]
     */
    @JsonProperty("website_theme_onboarding_done")
    public void setWebsite_theme_onboarding_done(String  website_theme_onboarding_done){
        this.website_theme_onboarding_done = website_theme_onboarding_done ;
        this.website_theme_onboarding_doneDirtyFlag = true ;
    }

     /**
     * 获取 [招聘网站主题一步完成]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_theme_onboarding_doneDirtyFlag(){
        return this.website_theme_onboarding_doneDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [邮政编码]
     */
    @JsonProperty("zip")
    public String getZip(){
        return this.zip ;
    }

    /**
     * 设置 [邮政编码]
     */
    @JsonProperty("zip")
    public void setZip(String  zip){
        this.zip = zip ;
        this.zipDirtyFlag = true ;
    }

     /**
     * 获取 [邮政编码]脏标记
     */
    @JsonIgnore
    public boolean getZipDirtyFlag(){
        return this.zipDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
