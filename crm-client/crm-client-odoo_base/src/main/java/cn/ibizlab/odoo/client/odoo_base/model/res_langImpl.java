package cn.ibizlab.odoo.client.odoo_base.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ires_lang;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[res_lang] 对象
 */
public class res_langImpl implements Ires_lang,Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 地区代码
     */
    public String code;

    @JsonIgnore
    public boolean codeDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 日期格式
     */
    public String date_format;

    @JsonIgnore
    public boolean date_formatDirtyFlag;
    
    /**
     * 小数分割符
     */
    public String decimal_point;

    @JsonIgnore
    public boolean decimal_pointDirtyFlag;
    
    /**
     * 方向
     */
    public String direction;

    @JsonIgnore
    public boolean directionDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 分割符格式
     */
    public String grouping;

    @JsonIgnore
    public boolean groupingDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * ISO代码
     */
    public String iso_code;

    @JsonIgnore
    public boolean iso_codeDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 千位分隔符
     */
    public String thousands_sep;

    @JsonIgnore
    public boolean thousands_sepDirtyFlag;
    
    /**
     * 时间格式
     */
    public String time_format;

    @JsonIgnore
    public boolean time_formatDirtyFlag;
    
    /**
     * 可翻译
     */
    public String translatable;

    @JsonIgnore
    public boolean translatableDirtyFlag;
    
    /**
     * 一个星期的第一天是
     */
    public String week_start;

    @JsonIgnore
    public boolean week_startDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [地区代码]
     */
    @JsonProperty("code")
    public String getCode(){
        return this.code ;
    }

    /**
     * 设置 [地区代码]
     */
    @JsonProperty("code")
    public void setCode(String  code){
        this.code = code ;
        this.codeDirtyFlag = true ;
    }

     /**
     * 获取 [地区代码]脏标记
     */
    @JsonIgnore
    public boolean getCodeDirtyFlag(){
        return this.codeDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [日期格式]
     */
    @JsonProperty("date_format")
    public String getDate_format(){
        return this.date_format ;
    }

    /**
     * 设置 [日期格式]
     */
    @JsonProperty("date_format")
    public void setDate_format(String  date_format){
        this.date_format = date_format ;
        this.date_formatDirtyFlag = true ;
    }

     /**
     * 获取 [日期格式]脏标记
     */
    @JsonIgnore
    public boolean getDate_formatDirtyFlag(){
        return this.date_formatDirtyFlag ;
    }   

    /**
     * 获取 [小数分割符]
     */
    @JsonProperty("decimal_point")
    public String getDecimal_point(){
        return this.decimal_point ;
    }

    /**
     * 设置 [小数分割符]
     */
    @JsonProperty("decimal_point")
    public void setDecimal_point(String  decimal_point){
        this.decimal_point = decimal_point ;
        this.decimal_pointDirtyFlag = true ;
    }

     /**
     * 获取 [小数分割符]脏标记
     */
    @JsonIgnore
    public boolean getDecimal_pointDirtyFlag(){
        return this.decimal_pointDirtyFlag ;
    }   

    /**
     * 获取 [方向]
     */
    @JsonProperty("direction")
    public String getDirection(){
        return this.direction ;
    }

    /**
     * 设置 [方向]
     */
    @JsonProperty("direction")
    public void setDirection(String  direction){
        this.direction = direction ;
        this.directionDirtyFlag = true ;
    }

     /**
     * 获取 [方向]脏标记
     */
    @JsonIgnore
    public boolean getDirectionDirtyFlag(){
        return this.directionDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [分割符格式]
     */
    @JsonProperty("grouping")
    public String getGrouping(){
        return this.grouping ;
    }

    /**
     * 设置 [分割符格式]
     */
    @JsonProperty("grouping")
    public void setGrouping(String  grouping){
        this.grouping = grouping ;
        this.groupingDirtyFlag = true ;
    }

     /**
     * 获取 [分割符格式]脏标记
     */
    @JsonIgnore
    public boolean getGroupingDirtyFlag(){
        return this.groupingDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [ISO代码]
     */
    @JsonProperty("iso_code")
    public String getIso_code(){
        return this.iso_code ;
    }

    /**
     * 设置 [ISO代码]
     */
    @JsonProperty("iso_code")
    public void setIso_code(String  iso_code){
        this.iso_code = iso_code ;
        this.iso_codeDirtyFlag = true ;
    }

     /**
     * 获取 [ISO代码]脏标记
     */
    @JsonIgnore
    public boolean getIso_codeDirtyFlag(){
        return this.iso_codeDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [千位分隔符]
     */
    @JsonProperty("thousands_sep")
    public String getThousands_sep(){
        return this.thousands_sep ;
    }

    /**
     * 设置 [千位分隔符]
     */
    @JsonProperty("thousands_sep")
    public void setThousands_sep(String  thousands_sep){
        this.thousands_sep = thousands_sep ;
        this.thousands_sepDirtyFlag = true ;
    }

     /**
     * 获取 [千位分隔符]脏标记
     */
    @JsonIgnore
    public boolean getThousands_sepDirtyFlag(){
        return this.thousands_sepDirtyFlag ;
    }   

    /**
     * 获取 [时间格式]
     */
    @JsonProperty("time_format")
    public String getTime_format(){
        return this.time_format ;
    }

    /**
     * 设置 [时间格式]
     */
    @JsonProperty("time_format")
    public void setTime_format(String  time_format){
        this.time_format = time_format ;
        this.time_formatDirtyFlag = true ;
    }

     /**
     * 获取 [时间格式]脏标记
     */
    @JsonIgnore
    public boolean getTime_formatDirtyFlag(){
        return this.time_formatDirtyFlag ;
    }   

    /**
     * 获取 [可翻译]
     */
    @JsonProperty("translatable")
    public String getTranslatable(){
        return this.translatable ;
    }

    /**
     * 设置 [可翻译]
     */
    @JsonProperty("translatable")
    public void setTranslatable(String  translatable){
        this.translatable = translatable ;
        this.translatableDirtyFlag = true ;
    }

     /**
     * 获取 [可翻译]脏标记
     */
    @JsonIgnore
    public boolean getTranslatableDirtyFlag(){
        return this.translatableDirtyFlag ;
    }   

    /**
     * 获取 [一个星期的第一天是]
     */
    @JsonProperty("week_start")
    public String getWeek_start(){
        return this.week_start ;
    }

    /**
     * 设置 [一个星期的第一天是]
     */
    @JsonProperty("week_start")
    public void setWeek_start(String  week_start){
        this.week_start = week_start ;
        this.week_startDirtyFlag = true ;
    }

     /**
     * 获取 [一个星期的第一天是]脏标记
     */
    @JsonIgnore
    public boolean getWeek_startDirtyFlag(){
        return this.week_startDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
