package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibase_module_update;
import cn.ibizlab.odoo.client.odoo_base.model.base_module_updateImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[base_module_update] 服务对象接口
 */
public interface base_module_updateFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_module_updates/updatebatch")
    public base_module_updateImpl updateBatch(@RequestBody List<base_module_updateImpl> base_module_updates);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_module_updates/{id}")
    public base_module_updateImpl update(@PathVariable("id") Integer id,@RequestBody base_module_updateImpl base_module_update);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_module_updates/createbatch")
    public base_module_updateImpl createBatch(@RequestBody List<base_module_updateImpl> base_module_updates);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_module_updates")
    public base_module_updateImpl create(@RequestBody base_module_updateImpl base_module_update);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_module_updates/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_module_updates/fetchdefault")
    public Page<base_module_updateImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_module_updates/{id}")
    public base_module_updateImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_module_updates/removebatch")
    public base_module_updateImpl removeBatch(@RequestBody List<base_module_updateImpl> base_module_updates);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_module_updates/select")
    public Page<base_module_updateImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_module_updates/{id}/getdraft")
    public base_module_updateImpl getDraft(@PathVariable("id") Integer id,@RequestBody base_module_updateImpl base_module_update);



}
