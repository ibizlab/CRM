package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ires_partner_title;
import cn.ibizlab.odoo.client.odoo_base.model.res_partner_titleImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[res_partner_title] 服务对象接口
 */
public interface res_partner_titleFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_partner_titles/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_partner_titles/updatebatch")
    public res_partner_titleImpl updateBatch(@RequestBody List<res_partner_titleImpl> res_partner_titles);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_partner_titles/{id}")
    public res_partner_titleImpl update(@PathVariable("id") Integer id,@RequestBody res_partner_titleImpl res_partner_title);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partner_titles/fetchdefault")
    public Page<res_partner_titleImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_partner_titles")
    public res_partner_titleImpl create(@RequestBody res_partner_titleImpl res_partner_title);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partner_titles/{id}")
    public res_partner_titleImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_partner_titles/createbatch")
    public res_partner_titleImpl createBatch(@RequestBody List<res_partner_titleImpl> res_partner_titles);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_partner_titles/removebatch")
    public res_partner_titleImpl removeBatch(@RequestBody List<res_partner_titleImpl> res_partner_titles);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partner_titles/select")
    public Page<res_partner_titleImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partner_titles/{id}/getdraft")
    public res_partner_titleImpl getDraft(@PathVariable("id") Integer id,@RequestBody res_partner_titleImpl res_partner_title);



}
