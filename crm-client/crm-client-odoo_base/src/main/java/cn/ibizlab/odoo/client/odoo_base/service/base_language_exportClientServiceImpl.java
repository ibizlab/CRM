package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_language_export;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibase_language_exportClientService;
import cn.ibizlab.odoo.client.odoo_base.model.base_language_exportImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.base_language_exportFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[base_language_export] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class base_language_exportClientServiceImpl implements Ibase_language_exportClientService {

    base_language_exportFeignClient base_language_exportFeignClient;

    @Autowired
    public base_language_exportClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_language_exportFeignClient = nameBuilder.target(base_language_exportFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_language_exportFeignClient = nameBuilder.target(base_language_exportFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibase_language_export createModel() {
		return new base_language_exportImpl();
	}


    public void create(Ibase_language_export base_language_export){
        Ibase_language_export clientModel = base_language_exportFeignClient.create((base_language_exportImpl)base_language_export) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_language_export.getClass(), false);
        copier.copy(clientModel, base_language_export, null);
    }


    public void removeBatch(List<Ibase_language_export> base_language_exports){
        if(base_language_exports!=null){
            List<base_language_exportImpl> list = new ArrayList<base_language_exportImpl>();
            for(Ibase_language_export ibase_language_export :base_language_exports){
                list.add((base_language_exportImpl)ibase_language_export) ;
            }
            base_language_exportFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Ibase_language_export> base_language_exports){
        if(base_language_exports!=null){
            List<base_language_exportImpl> list = new ArrayList<base_language_exportImpl>();
            for(Ibase_language_export ibase_language_export :base_language_exports){
                list.add((base_language_exportImpl)ibase_language_export) ;
            }
            base_language_exportFeignClient.createBatch(list) ;
        }
    }


    public Page<Ibase_language_export> fetchDefault(SearchContext context){
        Page<base_language_exportImpl> page = this.base_language_exportFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Ibase_language_export base_language_export){
        Ibase_language_export clientModel = base_language_exportFeignClient.get(base_language_export.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_language_export.getClass(), false);
        copier.copy(clientModel, base_language_export, null);
    }


    public void update(Ibase_language_export base_language_export){
        Ibase_language_export clientModel = base_language_exportFeignClient.update(base_language_export.getId(),(base_language_exportImpl)base_language_export) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_language_export.getClass(), false);
        copier.copy(clientModel, base_language_export, null);
    }


    public void remove(Ibase_language_export base_language_export){
        base_language_exportFeignClient.remove(base_language_export.getId()) ;
    }


    public void updateBatch(List<Ibase_language_export> base_language_exports){
        if(base_language_exports!=null){
            List<base_language_exportImpl> list = new ArrayList<base_language_exportImpl>();
            for(Ibase_language_export ibase_language_export :base_language_exports){
                list.add((base_language_exportImpl)ibase_language_export) ;
            }
            base_language_exportFeignClient.updateBatch(list) ;
        }
    }


    public Page<Ibase_language_export> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibase_language_export base_language_export){
        Ibase_language_export clientModel = base_language_exportFeignClient.getDraft(base_language_export.getId(),(base_language_exportImpl)base_language_export) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_language_export.getClass(), false);
        copier.copy(clientModel, base_language_export, null);
    }



}

