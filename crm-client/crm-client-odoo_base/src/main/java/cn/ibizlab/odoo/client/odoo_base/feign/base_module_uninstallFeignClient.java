package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibase_module_uninstall;
import cn.ibizlab.odoo.client.odoo_base.model.base_module_uninstallImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[base_module_uninstall] 服务对象接口
 */
public interface base_module_uninstallFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_module_uninstalls/{id}")
    public base_module_uninstallImpl update(@PathVariable("id") Integer id,@RequestBody base_module_uninstallImpl base_module_uninstall);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_module_uninstalls/updatebatch")
    public base_module_uninstallImpl updateBatch(@RequestBody List<base_module_uninstallImpl> base_module_uninstalls);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_module_uninstalls/createbatch")
    public base_module_uninstallImpl createBatch(@RequestBody List<base_module_uninstallImpl> base_module_uninstalls);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_module_uninstalls/{id}")
    public base_module_uninstallImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_module_uninstalls/removebatch")
    public base_module_uninstallImpl removeBatch(@RequestBody List<base_module_uninstallImpl> base_module_uninstalls);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_module_uninstalls/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_module_uninstalls")
    public base_module_uninstallImpl create(@RequestBody base_module_uninstallImpl base_module_uninstall);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_module_uninstalls/fetchdefault")
    public Page<base_module_uninstallImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_module_uninstalls/select")
    public Page<base_module_uninstallImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_module_uninstalls/{id}/getdraft")
    public base_module_uninstallImpl getDraft(@PathVariable("id") Integer id,@RequestBody base_module_uninstallImpl base_module_uninstall);



}
