package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_automation;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibase_automationClientService;
import cn.ibizlab.odoo.client.odoo_base.model.base_automationImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.base_automationFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[base_automation] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class base_automationClientServiceImpl implements Ibase_automationClientService {

    base_automationFeignClient base_automationFeignClient;

    @Autowired
    public base_automationClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_automationFeignClient = nameBuilder.target(base_automationFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_automationFeignClient = nameBuilder.target(base_automationFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibase_automation createModel() {
		return new base_automationImpl();
	}


    public void removeBatch(List<Ibase_automation> base_automations){
        if(base_automations!=null){
            List<base_automationImpl> list = new ArrayList<base_automationImpl>();
            for(Ibase_automation ibase_automation :base_automations){
                list.add((base_automationImpl)ibase_automation) ;
            }
            base_automationFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Ibase_automation> base_automations){
        if(base_automations!=null){
            List<base_automationImpl> list = new ArrayList<base_automationImpl>();
            for(Ibase_automation ibase_automation :base_automations){
                list.add((base_automationImpl)ibase_automation) ;
            }
            base_automationFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Ibase_automation> base_automations){
        if(base_automations!=null){
            List<base_automationImpl> list = new ArrayList<base_automationImpl>();
            for(Ibase_automation ibase_automation :base_automations){
                list.add((base_automationImpl)ibase_automation) ;
            }
            base_automationFeignClient.createBatch(list) ;
        }
    }


    public void create(Ibase_automation base_automation){
        Ibase_automation clientModel = base_automationFeignClient.create((base_automationImpl)base_automation) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_automation.getClass(), false);
        copier.copy(clientModel, base_automation, null);
    }


    public void get(Ibase_automation base_automation){
        Ibase_automation clientModel = base_automationFeignClient.get(base_automation.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_automation.getClass(), false);
        copier.copy(clientModel, base_automation, null);
    }


    public void remove(Ibase_automation base_automation){
        base_automationFeignClient.remove(base_automation.getId()) ;
    }


    public void update(Ibase_automation base_automation){
        Ibase_automation clientModel = base_automationFeignClient.update(base_automation.getId(),(base_automationImpl)base_automation) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_automation.getClass(), false);
        copier.copy(clientModel, base_automation, null);
    }


    public Page<Ibase_automation> fetchDefault(SearchContext context){
        Page<base_automationImpl> page = this.base_automationFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Ibase_automation> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibase_automation base_automation){
        Ibase_automation clientModel = base_automationFeignClient.getDraft(base_automation.getId(),(base_automationImpl)base_automation) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_automation.getClass(), false);
        copier.copy(clientModel, base_automation, null);
    }



}

