package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_module_uninstall;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibase_module_uninstallClientService;
import cn.ibizlab.odoo.client.odoo_base.model.base_module_uninstallImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.base_module_uninstallFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[base_module_uninstall] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class base_module_uninstallClientServiceImpl implements Ibase_module_uninstallClientService {

    base_module_uninstallFeignClient base_module_uninstallFeignClient;

    @Autowired
    public base_module_uninstallClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_module_uninstallFeignClient = nameBuilder.target(base_module_uninstallFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_module_uninstallFeignClient = nameBuilder.target(base_module_uninstallFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibase_module_uninstall createModel() {
		return new base_module_uninstallImpl();
	}


    public void update(Ibase_module_uninstall base_module_uninstall){
        Ibase_module_uninstall clientModel = base_module_uninstallFeignClient.update(base_module_uninstall.getId(),(base_module_uninstallImpl)base_module_uninstall) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_module_uninstall.getClass(), false);
        copier.copy(clientModel, base_module_uninstall, null);
    }


    public void updateBatch(List<Ibase_module_uninstall> base_module_uninstalls){
        if(base_module_uninstalls!=null){
            List<base_module_uninstallImpl> list = new ArrayList<base_module_uninstallImpl>();
            for(Ibase_module_uninstall ibase_module_uninstall :base_module_uninstalls){
                list.add((base_module_uninstallImpl)ibase_module_uninstall) ;
            }
            base_module_uninstallFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Ibase_module_uninstall> base_module_uninstalls){
        if(base_module_uninstalls!=null){
            List<base_module_uninstallImpl> list = new ArrayList<base_module_uninstallImpl>();
            for(Ibase_module_uninstall ibase_module_uninstall :base_module_uninstalls){
                list.add((base_module_uninstallImpl)ibase_module_uninstall) ;
            }
            base_module_uninstallFeignClient.createBatch(list) ;
        }
    }


    public void get(Ibase_module_uninstall base_module_uninstall){
        Ibase_module_uninstall clientModel = base_module_uninstallFeignClient.get(base_module_uninstall.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_module_uninstall.getClass(), false);
        copier.copy(clientModel, base_module_uninstall, null);
    }


    public void removeBatch(List<Ibase_module_uninstall> base_module_uninstalls){
        if(base_module_uninstalls!=null){
            List<base_module_uninstallImpl> list = new ArrayList<base_module_uninstallImpl>();
            for(Ibase_module_uninstall ibase_module_uninstall :base_module_uninstalls){
                list.add((base_module_uninstallImpl)ibase_module_uninstall) ;
            }
            base_module_uninstallFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Ibase_module_uninstall base_module_uninstall){
        base_module_uninstallFeignClient.remove(base_module_uninstall.getId()) ;
    }


    public void create(Ibase_module_uninstall base_module_uninstall){
        Ibase_module_uninstall clientModel = base_module_uninstallFeignClient.create((base_module_uninstallImpl)base_module_uninstall) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_module_uninstall.getClass(), false);
        copier.copy(clientModel, base_module_uninstall, null);
    }


    public Page<Ibase_module_uninstall> fetchDefault(SearchContext context){
        Page<base_module_uninstallImpl> page = this.base_module_uninstallFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Ibase_module_uninstall> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibase_module_uninstall base_module_uninstall){
        Ibase_module_uninstall clientModel = base_module_uninstallFeignClient.getDraft(base_module_uninstall.getId(),(base_module_uninstallImpl)base_module_uninstall) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_module_uninstall.getClass(), false);
        copier.copy(clientModel, base_module_uninstall, null);
    }



}

