package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ires_partner_category;
import cn.ibizlab.odoo.client.odoo_base.model.res_partner_categoryImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[res_partner_category] 服务对象接口
 */
public interface res_partner_categoryFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partner_categories/{id}")
    public res_partner_categoryImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_partner_categories/createbatch")
    public res_partner_categoryImpl createBatch(@RequestBody List<res_partner_categoryImpl> res_partner_categories);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_partner_categories/updatebatch")
    public res_partner_categoryImpl updateBatch(@RequestBody List<res_partner_categoryImpl> res_partner_categories);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partner_categories/fetchdefault")
    public Page<res_partner_categoryImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_partner_categories/{id}")
    public res_partner_categoryImpl update(@PathVariable("id") Integer id,@RequestBody res_partner_categoryImpl res_partner_category);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_partner_categories/removebatch")
    public res_partner_categoryImpl removeBatch(@RequestBody List<res_partner_categoryImpl> res_partner_categories);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_partner_categories")
    public res_partner_categoryImpl create(@RequestBody res_partner_categoryImpl res_partner_category);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_partner_categories/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partner_categories/select")
    public Page<res_partner_categoryImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partner_categories/{id}/getdraft")
    public res_partner_categoryImpl getDraft(@PathVariable("id") Integer id,@RequestBody res_partner_categoryImpl res_partner_category);



}
