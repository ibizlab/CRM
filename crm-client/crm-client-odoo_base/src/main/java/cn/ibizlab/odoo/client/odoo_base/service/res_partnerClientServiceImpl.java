package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_partner;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ires_partnerClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_partnerImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.res_partnerFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[res_partner] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class res_partnerClientServiceImpl implements Ires_partnerClientService {

    res_partnerFeignClient res_partnerFeignClient;

    @Autowired
    public res_partnerClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_partnerFeignClient = nameBuilder.target(res_partnerFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_partnerFeignClient = nameBuilder.target(res_partnerFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ires_partner createModel() {
		return new res_partnerImpl();
	}


    public void get(Ires_partner res_partner){
        Ires_partner clientModel = res_partnerFeignClient.get(res_partner.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_partner.getClass(), false);
        copier.copy(clientModel, res_partner, null);
    }


    public void update(Ires_partner res_partner){
        Ires_partner clientModel = res_partnerFeignClient.update(res_partner.getId(),(res_partnerImpl)res_partner) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_partner.getClass(), false);
        copier.copy(clientModel, res_partner, null);
    }


    public void create(Ires_partner res_partner){
        Ires_partner clientModel = res_partnerFeignClient.create((res_partnerImpl)res_partner) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_partner.getClass(), false);
        copier.copy(clientModel, res_partner, null);
    }


    public void createBatch(List<Ires_partner> res_partners){
        if(res_partners!=null){
            List<res_partnerImpl> list = new ArrayList<res_partnerImpl>();
            for(Ires_partner ires_partner :res_partners){
                list.add((res_partnerImpl)ires_partner) ;
            }
            res_partnerFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Ires_partner> res_partners){
        if(res_partners!=null){
            List<res_partnerImpl> list = new ArrayList<res_partnerImpl>();
            for(Ires_partner ires_partner :res_partners){
                list.add((res_partnerImpl)ires_partner) ;
            }
            res_partnerFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Ires_partner> res_partners){
        if(res_partners!=null){
            List<res_partnerImpl> list = new ArrayList<res_partnerImpl>();
            for(Ires_partner ires_partner :res_partners){
                list.add((res_partnerImpl)ires_partner) ;
            }
            res_partnerFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Ires_partner res_partner){
        res_partnerFeignClient.remove(res_partner.getId()) ;
    }


    public Page<Ires_partner> fetchDefault(SearchContext context){
        Page<res_partnerImpl> page = this.res_partnerFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Ires_partner> select(SearchContext context){
        return null ;
    }


    public void checkKey(Ires_partner res_partner){
        Ires_partner clientModel = res_partnerFeignClient.checkKey(res_partner.getId(),(res_partnerImpl)res_partner) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_partner.getClass(), false);
        copier.copy(clientModel, res_partner, null);
    }


    public void save(Ires_partner res_partner){
        Ires_partner clientModel = res_partnerFeignClient.save(res_partner.getId(),(res_partnerImpl)res_partner) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_partner.getClass(), false);
        copier.copy(clientModel, res_partner, null);
    }


    public void getDraft(Ires_partner res_partner){
        Ires_partner clientModel = res_partnerFeignClient.getDraft(res_partner.getId(),(res_partnerImpl)res_partner) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_partner.getClass(), false);
        copier.copy(clientModel, res_partner, null);
    }


    public Page<Ires_partner> fetchContacts(SearchContext context){
        Page<res_partnerImpl> page = this.res_partnerFeignClient.fetchContacts(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Ires_partner> fetchCompany(SearchContext context){
        Page<res_partnerImpl> page = this.res_partnerFeignClient.fetchCompany(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }



}

