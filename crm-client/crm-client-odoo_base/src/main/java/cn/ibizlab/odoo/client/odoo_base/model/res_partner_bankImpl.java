package cn.ibizlab.odoo.client.odoo_base.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ires_partner_bank;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[res_partner_bank] 对象
 */
public class res_partner_bankImpl implements Ires_partner_bank,Serializable{

    /**
     * 账户持有人名称
     */
    public String acc_holder_name;

    @JsonIgnore
    public boolean acc_holder_nameDirtyFlag;
    
    /**
     * 账户号码
     */
    public String acc_number;

    @JsonIgnore
    public boolean acc_numberDirtyFlag;
    
    /**
     * 类型
     */
    public String acc_type;

    @JsonIgnore
    public boolean acc_typeDirtyFlag;
    
    /**
     * 银行识别代码
     */
    public String bank_bic;

    @JsonIgnore
    public boolean bank_bicDirtyFlag;
    
    /**
     * 银行
     */
    public Integer bank_id;

    @JsonIgnore
    public boolean bank_idDirtyFlag;
    
    /**
     * 名称
     */
    public String bank_name;

    @JsonIgnore
    public boolean bank_nameDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 币种
     */
    public String currency_id_text;

    @JsonIgnore
    public boolean currency_id_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 会计日记账
     */
    public String journal_id;

    @JsonIgnore
    public boolean journal_idDirtyFlag;
    
    /**
     * 账户持有人
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 账户持有人
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * ‎有所有必需的参数‎
     */
    public String qr_code_valid;

    @JsonIgnore
    public boolean qr_code_validDirtyFlag;
    
    /**
     * 核对银行账号
     */
    public String sanitized_acc_number;

    @JsonIgnore
    public boolean sanitized_acc_numberDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [账户持有人名称]
     */
    @JsonProperty("acc_holder_name")
    public String getAcc_holder_name(){
        return this.acc_holder_name ;
    }

    /**
     * 设置 [账户持有人名称]
     */
    @JsonProperty("acc_holder_name")
    public void setAcc_holder_name(String  acc_holder_name){
        this.acc_holder_name = acc_holder_name ;
        this.acc_holder_nameDirtyFlag = true ;
    }

     /**
     * 获取 [账户持有人名称]脏标记
     */
    @JsonIgnore
    public boolean getAcc_holder_nameDirtyFlag(){
        return this.acc_holder_nameDirtyFlag ;
    }   

    /**
     * 获取 [账户号码]
     */
    @JsonProperty("acc_number")
    public String getAcc_number(){
        return this.acc_number ;
    }

    /**
     * 设置 [账户号码]
     */
    @JsonProperty("acc_number")
    public void setAcc_number(String  acc_number){
        this.acc_number = acc_number ;
        this.acc_numberDirtyFlag = true ;
    }

     /**
     * 获取 [账户号码]脏标记
     */
    @JsonIgnore
    public boolean getAcc_numberDirtyFlag(){
        return this.acc_numberDirtyFlag ;
    }   

    /**
     * 获取 [类型]
     */
    @JsonProperty("acc_type")
    public String getAcc_type(){
        return this.acc_type ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("acc_type")
    public void setAcc_type(String  acc_type){
        this.acc_type = acc_type ;
        this.acc_typeDirtyFlag = true ;
    }

     /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getAcc_typeDirtyFlag(){
        return this.acc_typeDirtyFlag ;
    }   

    /**
     * 获取 [银行识别代码]
     */
    @JsonProperty("bank_bic")
    public String getBank_bic(){
        return this.bank_bic ;
    }

    /**
     * 设置 [银行识别代码]
     */
    @JsonProperty("bank_bic")
    public void setBank_bic(String  bank_bic){
        this.bank_bic = bank_bic ;
        this.bank_bicDirtyFlag = true ;
    }

     /**
     * 获取 [银行识别代码]脏标记
     */
    @JsonIgnore
    public boolean getBank_bicDirtyFlag(){
        return this.bank_bicDirtyFlag ;
    }   

    /**
     * 获取 [银行]
     */
    @JsonProperty("bank_id")
    public Integer getBank_id(){
        return this.bank_id ;
    }

    /**
     * 设置 [银行]
     */
    @JsonProperty("bank_id")
    public void setBank_id(Integer  bank_id){
        this.bank_id = bank_id ;
        this.bank_idDirtyFlag = true ;
    }

     /**
     * 获取 [银行]脏标记
     */
    @JsonIgnore
    public boolean getBank_idDirtyFlag(){
        return this.bank_idDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("bank_name")
    public String getBank_name(){
        return this.bank_name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("bank_name")
    public void setBank_name(String  bank_name){
        this.bank_name = bank_name ;
        this.bank_nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getBank_nameDirtyFlag(){
        return this.bank_nameDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return this.currency_id_text ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return this.currency_id_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [会计日记账]
     */
    @JsonProperty("journal_id")
    public String getJournal_id(){
        return this.journal_id ;
    }

    /**
     * 设置 [会计日记账]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(String  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

     /**
     * 获取 [会计日记账]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return this.journal_idDirtyFlag ;
    }   

    /**
     * 获取 [账户持有人]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [账户持有人]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [账户持有人]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [账户持有人]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [账户持有人]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [账户持有人]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [‎有所有必需的参数‎]
     */
    @JsonProperty("qr_code_valid")
    public String getQr_code_valid(){
        return this.qr_code_valid ;
    }

    /**
     * 设置 [‎有所有必需的参数‎]
     */
    @JsonProperty("qr_code_valid")
    public void setQr_code_valid(String  qr_code_valid){
        this.qr_code_valid = qr_code_valid ;
        this.qr_code_validDirtyFlag = true ;
    }

     /**
     * 获取 [‎有所有必需的参数‎]脏标记
     */
    @JsonIgnore
    public boolean getQr_code_validDirtyFlag(){
        return this.qr_code_validDirtyFlag ;
    }   

    /**
     * 获取 [核对银行账号]
     */
    @JsonProperty("sanitized_acc_number")
    public String getSanitized_acc_number(){
        return this.sanitized_acc_number ;
    }

    /**
     * 设置 [核对银行账号]
     */
    @JsonProperty("sanitized_acc_number")
    public void setSanitized_acc_number(String  sanitized_acc_number){
        this.sanitized_acc_number = sanitized_acc_number ;
        this.sanitized_acc_numberDirtyFlag = true ;
    }

     /**
     * 获取 [核对银行账号]脏标记
     */
    @JsonIgnore
    public boolean getSanitized_acc_numberDirtyFlag(){
        return this.sanitized_acc_numberDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
