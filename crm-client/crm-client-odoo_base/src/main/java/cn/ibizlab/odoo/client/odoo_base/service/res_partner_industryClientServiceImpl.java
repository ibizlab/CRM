package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_partner_industry;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ires_partner_industryClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_partner_industryImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.res_partner_industryFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[res_partner_industry] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class res_partner_industryClientServiceImpl implements Ires_partner_industryClientService {

    res_partner_industryFeignClient res_partner_industryFeignClient;

    @Autowired
    public res_partner_industryClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_partner_industryFeignClient = nameBuilder.target(res_partner_industryFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_partner_industryFeignClient = nameBuilder.target(res_partner_industryFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ires_partner_industry createModel() {
		return new res_partner_industryImpl();
	}


    public void create(Ires_partner_industry res_partner_industry){
        Ires_partner_industry clientModel = res_partner_industryFeignClient.create((res_partner_industryImpl)res_partner_industry) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_partner_industry.getClass(), false);
        copier.copy(clientModel, res_partner_industry, null);
    }


    public void updateBatch(List<Ires_partner_industry> res_partner_industries){
        if(res_partner_industries!=null){
            List<res_partner_industryImpl> list = new ArrayList<res_partner_industryImpl>();
            for(Ires_partner_industry ires_partner_industry :res_partner_industries){
                list.add((res_partner_industryImpl)ires_partner_industry) ;
            }
            res_partner_industryFeignClient.updateBatch(list) ;
        }
    }


    public void update(Ires_partner_industry res_partner_industry){
        Ires_partner_industry clientModel = res_partner_industryFeignClient.update(res_partner_industry.getId(),(res_partner_industryImpl)res_partner_industry) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_partner_industry.getClass(), false);
        copier.copy(clientModel, res_partner_industry, null);
    }


    public void createBatch(List<Ires_partner_industry> res_partner_industries){
        if(res_partner_industries!=null){
            List<res_partner_industryImpl> list = new ArrayList<res_partner_industryImpl>();
            for(Ires_partner_industry ires_partner_industry :res_partner_industries){
                list.add((res_partner_industryImpl)ires_partner_industry) ;
            }
            res_partner_industryFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Ires_partner_industry> res_partner_industries){
        if(res_partner_industries!=null){
            List<res_partner_industryImpl> list = new ArrayList<res_partner_industryImpl>();
            for(Ires_partner_industry ires_partner_industry :res_partner_industries){
                list.add((res_partner_industryImpl)ires_partner_industry) ;
            }
            res_partner_industryFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Ires_partner_industry res_partner_industry){
        res_partner_industryFeignClient.remove(res_partner_industry.getId()) ;
    }


    public Page<Ires_partner_industry> fetchDefault(SearchContext context){
        Page<res_partner_industryImpl> page = this.res_partner_industryFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Ires_partner_industry res_partner_industry){
        Ires_partner_industry clientModel = res_partner_industryFeignClient.get(res_partner_industry.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_partner_industry.getClass(), false);
        copier.copy(clientModel, res_partner_industry, null);
    }


    public Page<Ires_partner_industry> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ires_partner_industry res_partner_industry){
        Ires_partner_industry clientModel = res_partner_industryFeignClient.getDraft(res_partner_industry.getId(),(res_partner_industryImpl)res_partner_industry) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_partner_industry.getClass(), false);
        copier.copy(clientModel, res_partner_industry, null);
    }



}

