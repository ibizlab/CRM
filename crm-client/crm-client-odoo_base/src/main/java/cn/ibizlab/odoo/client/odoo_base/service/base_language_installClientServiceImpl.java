package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_language_install;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibase_language_installClientService;
import cn.ibizlab.odoo.client.odoo_base.model.base_language_installImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.base_language_installFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[base_language_install] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class base_language_installClientServiceImpl implements Ibase_language_installClientService {

    base_language_installFeignClient base_language_installFeignClient;

    @Autowired
    public base_language_installClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_language_installFeignClient = nameBuilder.target(base_language_installFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_language_installFeignClient = nameBuilder.target(base_language_installFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibase_language_install createModel() {
		return new base_language_installImpl();
	}


    public void updateBatch(List<Ibase_language_install> base_language_installs){
        if(base_language_installs!=null){
            List<base_language_installImpl> list = new ArrayList<base_language_installImpl>();
            for(Ibase_language_install ibase_language_install :base_language_installs){
                list.add((base_language_installImpl)ibase_language_install) ;
            }
            base_language_installFeignClient.updateBatch(list) ;
        }
    }


    public void update(Ibase_language_install base_language_install){
        Ibase_language_install clientModel = base_language_installFeignClient.update(base_language_install.getId(),(base_language_installImpl)base_language_install) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_language_install.getClass(), false);
        copier.copy(clientModel, base_language_install, null);
    }


    public Page<Ibase_language_install> fetchDefault(SearchContext context){
        Page<base_language_installImpl> page = this.base_language_installFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Ibase_language_install> base_language_installs){
        if(base_language_installs!=null){
            List<base_language_installImpl> list = new ArrayList<base_language_installImpl>();
            for(Ibase_language_install ibase_language_install :base_language_installs){
                list.add((base_language_installImpl)ibase_language_install) ;
            }
            base_language_installFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Ibase_language_install> base_language_installs){
        if(base_language_installs!=null){
            List<base_language_installImpl> list = new ArrayList<base_language_installImpl>();
            for(Ibase_language_install ibase_language_install :base_language_installs){
                list.add((base_language_installImpl)ibase_language_install) ;
            }
            base_language_installFeignClient.createBatch(list) ;
        }
    }


    public void create(Ibase_language_install base_language_install){
        Ibase_language_install clientModel = base_language_installFeignClient.create((base_language_installImpl)base_language_install) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_language_install.getClass(), false);
        copier.copy(clientModel, base_language_install, null);
    }


    public void remove(Ibase_language_install base_language_install){
        base_language_installFeignClient.remove(base_language_install.getId()) ;
    }


    public void get(Ibase_language_install base_language_install){
        Ibase_language_install clientModel = base_language_installFeignClient.get(base_language_install.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_language_install.getClass(), false);
        copier.copy(clientModel, base_language_install, null);
    }


    public Page<Ibase_language_install> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibase_language_install base_language_install){
        Ibase_language_install clientModel = base_language_installFeignClient.getDraft(base_language_install.getId(),(base_language_installImpl)base_language_install) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_language_install.getClass(), false);
        copier.copy(clientModel, base_language_install, null);
    }



}

