package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ires_country_group;
import cn.ibizlab.odoo.client.odoo_base.model.res_country_groupImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[res_country_group] 服务对象接口
 */
public interface res_country_groupFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_country_groups/fetchdefault")
    public Page<res_country_groupImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_country_groups/{id}")
    public res_country_groupImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_country_groups/{id}")
    public res_country_groupImpl update(@PathVariable("id") Integer id,@RequestBody res_country_groupImpl res_country_group);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_country_groups/removebatch")
    public res_country_groupImpl removeBatch(@RequestBody List<res_country_groupImpl> res_country_groups);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_country_groups/createbatch")
    public res_country_groupImpl createBatch(@RequestBody List<res_country_groupImpl> res_country_groups);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_country_groups/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_country_groups/updatebatch")
    public res_country_groupImpl updateBatch(@RequestBody List<res_country_groupImpl> res_country_groups);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_country_groups")
    public res_country_groupImpl create(@RequestBody res_country_groupImpl res_country_group);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_country_groups/select")
    public Page<res_country_groupImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_country_groups/{id}/getdraft")
    public res_country_groupImpl getDraft(@PathVariable("id") Integer id,@RequestBody res_country_groupImpl res_country_group);



}
