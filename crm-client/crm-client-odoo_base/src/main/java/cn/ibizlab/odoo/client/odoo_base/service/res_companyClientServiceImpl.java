package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_company;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ires_companyClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_companyImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.res_companyFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[res_company] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class res_companyClientServiceImpl implements Ires_companyClientService {

    res_companyFeignClient res_companyFeignClient;

    @Autowired
    public res_companyClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_companyFeignClient = nameBuilder.target(res_companyFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.res_companyFeignClient = nameBuilder.target(res_companyFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ires_company createModel() {
		return new res_companyImpl();
	}


    public void removeBatch(List<Ires_company> res_companies){
        if(res_companies!=null){
            List<res_companyImpl> list = new ArrayList<res_companyImpl>();
            for(Ires_company ires_company :res_companies){
                list.add((res_companyImpl)ires_company) ;
            }
            res_companyFeignClient.removeBatch(list) ;
        }
    }


    public void create(Ires_company res_company){
        Ires_company clientModel = res_companyFeignClient.create((res_companyImpl)res_company) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_company.getClass(), false);
        copier.copy(clientModel, res_company, null);
    }


    public void remove(Ires_company res_company){
        res_companyFeignClient.remove(res_company.getId()) ;
    }


    public void updateBatch(List<Ires_company> res_companies){
        if(res_companies!=null){
            List<res_companyImpl> list = new ArrayList<res_companyImpl>();
            for(Ires_company ires_company :res_companies){
                list.add((res_companyImpl)ires_company) ;
            }
            res_companyFeignClient.updateBatch(list) ;
        }
    }


    public void update(Ires_company res_company){
        Ires_company clientModel = res_companyFeignClient.update(res_company.getId(),(res_companyImpl)res_company) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_company.getClass(), false);
        copier.copy(clientModel, res_company, null);
    }


    public Page<Ires_company> fetchDefault(SearchContext context){
        Page<res_companyImpl> page = this.res_companyFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Ires_company> res_companies){
        if(res_companies!=null){
            List<res_companyImpl> list = new ArrayList<res_companyImpl>();
            for(Ires_company ires_company :res_companies){
                list.add((res_companyImpl)ires_company) ;
            }
            res_companyFeignClient.createBatch(list) ;
        }
    }


    public void get(Ires_company res_company){
        Ires_company clientModel = res_companyFeignClient.get(res_company.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_company.getClass(), false);
        copier.copy(clientModel, res_company, null);
    }


    public Page<Ires_company> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ires_company res_company){
        Ires_company clientModel = res_companyFeignClient.getDraft(res_company.getId(),(res_companyImpl)res_company) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), res_company.getClass(), false);
        copier.copy(clientModel, res_company, null);
    }



}

