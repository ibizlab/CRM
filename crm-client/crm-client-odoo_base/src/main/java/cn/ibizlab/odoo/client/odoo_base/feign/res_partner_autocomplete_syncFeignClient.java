package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ires_partner_autocomplete_sync;
import cn.ibizlab.odoo.client.odoo_base.model.res_partner_autocomplete_syncImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[res_partner_autocomplete_sync] 服务对象接口
 */
public interface res_partner_autocomplete_syncFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_partner_autocomplete_syncs/{id}")
    public res_partner_autocomplete_syncImpl update(@PathVariable("id") Integer id,@RequestBody res_partner_autocomplete_syncImpl res_partner_autocomplete_sync);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_partner_autocomplete_syncs/updatebatch")
    public res_partner_autocomplete_syncImpl updateBatch(@RequestBody List<res_partner_autocomplete_syncImpl> res_partner_autocomplete_syncs);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partner_autocomplete_syncs/{id}")
    public res_partner_autocomplete_syncImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_partner_autocomplete_syncs")
    public res_partner_autocomplete_syncImpl create(@RequestBody res_partner_autocomplete_syncImpl res_partner_autocomplete_sync);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_partner_autocomplete_syncs/createbatch")
    public res_partner_autocomplete_syncImpl createBatch(@RequestBody List<res_partner_autocomplete_syncImpl> res_partner_autocomplete_syncs);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partner_autocomplete_syncs/fetchdefault")
    public Page<res_partner_autocomplete_syncImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_partner_autocomplete_syncs/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_partner_autocomplete_syncs/removebatch")
    public res_partner_autocomplete_syncImpl removeBatch(@RequestBody List<res_partner_autocomplete_syncImpl> res_partner_autocomplete_syncs);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partner_autocomplete_syncs/select")
    public Page<res_partner_autocomplete_syncImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partner_autocomplete_syncs/{id}/getdraft")
    public res_partner_autocomplete_syncImpl getDraft(@PathVariable("id") Integer id,@RequestBody res_partner_autocomplete_syncImpl res_partner_autocomplete_sync);



}
