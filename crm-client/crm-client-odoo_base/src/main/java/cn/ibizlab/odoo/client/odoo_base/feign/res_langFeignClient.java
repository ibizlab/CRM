package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ires_lang;
import cn.ibizlab.odoo.client.odoo_base.model.res_langImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[res_lang] 服务对象接口
 */
public interface res_langFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_langs/{id}")
    public res_langImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_langs/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_langs/{id}")
    public res_langImpl update(@PathVariable("id") Integer id,@RequestBody res_langImpl res_lang);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_langs/updatebatch")
    public res_langImpl updateBatch(@RequestBody List<res_langImpl> res_langs);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_langs/fetchdefault")
    public Page<res_langImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_langs/removebatch")
    public res_langImpl removeBatch(@RequestBody List<res_langImpl> res_langs);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_langs")
    public res_langImpl create(@RequestBody res_langImpl res_lang);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_langs/createbatch")
    public res_langImpl createBatch(@RequestBody List<res_langImpl> res_langs);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_langs/select")
    public Page<res_langImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_langs/{id}/getdraft")
    public res_langImpl getDraft(@PathVariable("id") Integer id,@RequestBody res_langImpl res_lang);



}
