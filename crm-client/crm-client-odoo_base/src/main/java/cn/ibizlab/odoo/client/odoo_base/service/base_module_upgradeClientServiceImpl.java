package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_module_upgrade;
import cn.ibizlab.odoo.client.odoo_base.config.odoo_baseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibase_module_upgradeClientService;
import cn.ibizlab.odoo.client.odoo_base.model.base_module_upgradeImpl;
import cn.ibizlab.odoo.client.odoo_base.feign.base_module_upgradeFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[base_module_upgrade] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class base_module_upgradeClientServiceImpl implements Ibase_module_upgradeClientService {

    base_module_upgradeFeignClient base_module_upgradeFeignClient;

    @Autowired
    public base_module_upgradeClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_baseClientProperties odoo_baseClientProperties) {
        if (odoo_baseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_module_upgradeFeignClient = nameBuilder.target(base_module_upgradeFeignClient.class,"http://"+odoo_baseClientProperties.getServiceId()+"/") ;
		}else if (odoo_baseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_module_upgradeFeignClient = nameBuilder.target(base_module_upgradeFeignClient.class,odoo_baseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibase_module_upgrade createModel() {
		return new base_module_upgradeImpl();
	}


    public void update(Ibase_module_upgrade base_module_upgrade){
        Ibase_module_upgrade clientModel = base_module_upgradeFeignClient.update(base_module_upgrade.getId(),(base_module_upgradeImpl)base_module_upgrade) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_module_upgrade.getClass(), false);
        copier.copy(clientModel, base_module_upgrade, null);
    }


    public void createBatch(List<Ibase_module_upgrade> base_module_upgrades){
        if(base_module_upgrades!=null){
            List<base_module_upgradeImpl> list = new ArrayList<base_module_upgradeImpl>();
            for(Ibase_module_upgrade ibase_module_upgrade :base_module_upgrades){
                list.add((base_module_upgradeImpl)ibase_module_upgrade) ;
            }
            base_module_upgradeFeignClient.createBatch(list) ;
        }
    }


    public void remove(Ibase_module_upgrade base_module_upgrade){
        base_module_upgradeFeignClient.remove(base_module_upgrade.getId()) ;
    }


    public void get(Ibase_module_upgrade base_module_upgrade){
        Ibase_module_upgrade clientModel = base_module_upgradeFeignClient.get(base_module_upgrade.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_module_upgrade.getClass(), false);
        copier.copy(clientModel, base_module_upgrade, null);
    }


    public Page<Ibase_module_upgrade> fetchDefault(SearchContext context){
        Page<base_module_upgradeImpl> page = this.base_module_upgradeFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Ibase_module_upgrade> base_module_upgrades){
        if(base_module_upgrades!=null){
            List<base_module_upgradeImpl> list = new ArrayList<base_module_upgradeImpl>();
            for(Ibase_module_upgrade ibase_module_upgrade :base_module_upgrades){
                list.add((base_module_upgradeImpl)ibase_module_upgrade) ;
            }
            base_module_upgradeFeignClient.removeBatch(list) ;
        }
    }


    public void create(Ibase_module_upgrade base_module_upgrade){
        Ibase_module_upgrade clientModel = base_module_upgradeFeignClient.create((base_module_upgradeImpl)base_module_upgrade) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_module_upgrade.getClass(), false);
        copier.copy(clientModel, base_module_upgrade, null);
    }


    public void updateBatch(List<Ibase_module_upgrade> base_module_upgrades){
        if(base_module_upgrades!=null){
            List<base_module_upgradeImpl> list = new ArrayList<base_module_upgradeImpl>();
            for(Ibase_module_upgrade ibase_module_upgrade :base_module_upgrades){
                list.add((base_module_upgradeImpl)ibase_module_upgrade) ;
            }
            base_module_upgradeFeignClient.updateBatch(list) ;
        }
    }


    public Page<Ibase_module_upgrade> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibase_module_upgrade base_module_upgrade){
        Ibase_module_upgrade clientModel = base_module_upgradeFeignClient.getDraft(base_module_upgrade.getId(),(base_module_upgradeImpl)base_module_upgrade) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_module_upgrade.getClass(), false);
        copier.copy(clientModel, base_module_upgrade, null);
    }



}

