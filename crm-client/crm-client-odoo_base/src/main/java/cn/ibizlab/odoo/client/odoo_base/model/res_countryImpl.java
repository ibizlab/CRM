package cn.ibizlab.odoo.client.odoo_base.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ires_country;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[res_country] 对象
 */
public class res_countryImpl implements Ires_country,Serializable{

    /**
     * 报表布局
     */
    public String address_format;

    @JsonIgnore
    public boolean address_formatDirtyFlag;
    
    /**
     * 输入视图
     */
    public Integer address_view_id;

    @JsonIgnore
    public boolean address_view_idDirtyFlag;
    
    /**
     * 国家/地区代码
     */
    public String code;

    @JsonIgnore
    public boolean codeDirtyFlag;
    
    /**
     * 国家/地区分组
     */
    public String country_group_ids;

    @JsonIgnore
    public boolean country_group_idsDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 币种
     */
    public String currency_id_text;

    @JsonIgnore
    public boolean currency_id_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 图像
     */
    public byte[] image;

    @JsonIgnore
    public boolean imageDirtyFlag;
    
    /**
     * 国家/地区名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 客户姓名位置
     */
    public String name_position;

    @JsonIgnore
    public boolean name_positionDirtyFlag;
    
    /**
     * 国家/地区长途区号
     */
    public Integer phone_code;

    @JsonIgnore
    public boolean phone_codeDirtyFlag;
    
    /**
     * 省份
     */
    public String state_ids;

    @JsonIgnore
    public boolean state_idsDirtyFlag;
    
    /**
     * 增值税标签
     */
    public String vat_label;

    @JsonIgnore
    public boolean vat_labelDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [报表布局]
     */
    @JsonProperty("address_format")
    public String getAddress_format(){
        return this.address_format ;
    }

    /**
     * 设置 [报表布局]
     */
    @JsonProperty("address_format")
    public void setAddress_format(String  address_format){
        this.address_format = address_format ;
        this.address_formatDirtyFlag = true ;
    }

     /**
     * 获取 [报表布局]脏标记
     */
    @JsonIgnore
    public boolean getAddress_formatDirtyFlag(){
        return this.address_formatDirtyFlag ;
    }   

    /**
     * 获取 [输入视图]
     */
    @JsonProperty("address_view_id")
    public Integer getAddress_view_id(){
        return this.address_view_id ;
    }

    /**
     * 设置 [输入视图]
     */
    @JsonProperty("address_view_id")
    public void setAddress_view_id(Integer  address_view_id){
        this.address_view_id = address_view_id ;
        this.address_view_idDirtyFlag = true ;
    }

     /**
     * 获取 [输入视图]脏标记
     */
    @JsonIgnore
    public boolean getAddress_view_idDirtyFlag(){
        return this.address_view_idDirtyFlag ;
    }   

    /**
     * 获取 [国家/地区代码]
     */
    @JsonProperty("code")
    public String getCode(){
        return this.code ;
    }

    /**
     * 设置 [国家/地区代码]
     */
    @JsonProperty("code")
    public void setCode(String  code){
        this.code = code ;
        this.codeDirtyFlag = true ;
    }

     /**
     * 获取 [国家/地区代码]脏标记
     */
    @JsonIgnore
    public boolean getCodeDirtyFlag(){
        return this.codeDirtyFlag ;
    }   

    /**
     * 获取 [国家/地区分组]
     */
    @JsonProperty("country_group_ids")
    public String getCountry_group_ids(){
        return this.country_group_ids ;
    }

    /**
     * 设置 [国家/地区分组]
     */
    @JsonProperty("country_group_ids")
    public void setCountry_group_ids(String  country_group_ids){
        this.country_group_ids = country_group_ids ;
        this.country_group_idsDirtyFlag = true ;
    }

     /**
     * 获取 [国家/地区分组]脏标记
     */
    @JsonIgnore
    public boolean getCountry_group_idsDirtyFlag(){
        return this.country_group_idsDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return this.currency_id_text ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return this.currency_id_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [图像]
     */
    @JsonProperty("image")
    public byte[] getImage(){
        return this.image ;
    }

    /**
     * 设置 [图像]
     */
    @JsonProperty("image")
    public void setImage(byte[]  image){
        this.image = image ;
        this.imageDirtyFlag = true ;
    }

     /**
     * 获取 [图像]脏标记
     */
    @JsonIgnore
    public boolean getImageDirtyFlag(){
        return this.imageDirtyFlag ;
    }   

    /**
     * 获取 [国家/地区名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [国家/地区名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [国家/地区名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [客户姓名位置]
     */
    @JsonProperty("name_position")
    public String getName_position(){
        return this.name_position ;
    }

    /**
     * 设置 [客户姓名位置]
     */
    @JsonProperty("name_position")
    public void setName_position(String  name_position){
        this.name_position = name_position ;
        this.name_positionDirtyFlag = true ;
    }

     /**
     * 获取 [客户姓名位置]脏标记
     */
    @JsonIgnore
    public boolean getName_positionDirtyFlag(){
        return this.name_positionDirtyFlag ;
    }   

    /**
     * 获取 [国家/地区长途区号]
     */
    @JsonProperty("phone_code")
    public Integer getPhone_code(){
        return this.phone_code ;
    }

    /**
     * 设置 [国家/地区长途区号]
     */
    @JsonProperty("phone_code")
    public void setPhone_code(Integer  phone_code){
        this.phone_code = phone_code ;
        this.phone_codeDirtyFlag = true ;
    }

     /**
     * 获取 [国家/地区长途区号]脏标记
     */
    @JsonIgnore
    public boolean getPhone_codeDirtyFlag(){
        return this.phone_codeDirtyFlag ;
    }   

    /**
     * 获取 [省份]
     */
    @JsonProperty("state_ids")
    public String getState_ids(){
        return this.state_ids ;
    }

    /**
     * 设置 [省份]
     */
    @JsonProperty("state_ids")
    public void setState_ids(String  state_ids){
        this.state_ids = state_ids ;
        this.state_idsDirtyFlag = true ;
    }

     /**
     * 获取 [省份]脏标记
     */
    @JsonIgnore
    public boolean getState_idsDirtyFlag(){
        return this.state_idsDirtyFlag ;
    }   

    /**
     * 获取 [增值税标签]
     */
    @JsonProperty("vat_label")
    public String getVat_label(){
        return this.vat_label ;
    }

    /**
     * 设置 [增值税标签]
     */
    @JsonProperty("vat_label")
    public void setVat_label(String  vat_label){
        this.vat_label = vat_label ;
        this.vat_labelDirtyFlag = true ;
    }

     /**
     * 获取 [增值税标签]脏标记
     */
    @JsonIgnore
    public boolean getVat_labelDirtyFlag(){
        return this.vat_labelDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
