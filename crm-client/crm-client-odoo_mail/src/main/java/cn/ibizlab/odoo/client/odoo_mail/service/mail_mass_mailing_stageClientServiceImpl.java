package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_stage;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_mass_mailing_stageClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mass_mailing_stageImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_mass_mailing_stageFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_mass_mailing_stage] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_mass_mailing_stageClientServiceImpl implements Imail_mass_mailing_stageClientService {

    mail_mass_mailing_stageFeignClient mail_mass_mailing_stageFeignClient;

    @Autowired
    public mail_mass_mailing_stageClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_mass_mailing_stageFeignClient = nameBuilder.target(mail_mass_mailing_stageFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_mass_mailing_stageFeignClient = nameBuilder.target(mail_mass_mailing_stageFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_mass_mailing_stage createModel() {
		return new mail_mass_mailing_stageImpl();
	}


    public void create(Imail_mass_mailing_stage mail_mass_mailing_stage){
        Imail_mass_mailing_stage clientModel = mail_mass_mailing_stageFeignClient.create((mail_mass_mailing_stageImpl)mail_mass_mailing_stage) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing_stage.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing_stage, null);
    }


    public void updateBatch(List<Imail_mass_mailing_stage> mail_mass_mailing_stages){
        if(mail_mass_mailing_stages!=null){
            List<mail_mass_mailing_stageImpl> list = new ArrayList<mail_mass_mailing_stageImpl>();
            for(Imail_mass_mailing_stage imail_mass_mailing_stage :mail_mass_mailing_stages){
                list.add((mail_mass_mailing_stageImpl)imail_mass_mailing_stage) ;
            }
            mail_mass_mailing_stageFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Imail_mass_mailing_stage> mail_mass_mailing_stages){
        if(mail_mass_mailing_stages!=null){
            List<mail_mass_mailing_stageImpl> list = new ArrayList<mail_mass_mailing_stageImpl>();
            for(Imail_mass_mailing_stage imail_mass_mailing_stage :mail_mass_mailing_stages){
                list.add((mail_mass_mailing_stageImpl)imail_mass_mailing_stage) ;
            }
            mail_mass_mailing_stageFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Imail_mass_mailing_stage> mail_mass_mailing_stages){
        if(mail_mass_mailing_stages!=null){
            List<mail_mass_mailing_stageImpl> list = new ArrayList<mail_mass_mailing_stageImpl>();
            for(Imail_mass_mailing_stage imail_mass_mailing_stage :mail_mass_mailing_stages){
                list.add((mail_mass_mailing_stageImpl)imail_mass_mailing_stage) ;
            }
            mail_mass_mailing_stageFeignClient.removeBatch(list) ;
        }
    }


    public void update(Imail_mass_mailing_stage mail_mass_mailing_stage){
        Imail_mass_mailing_stage clientModel = mail_mass_mailing_stageFeignClient.update(mail_mass_mailing_stage.getId(),(mail_mass_mailing_stageImpl)mail_mass_mailing_stage) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing_stage.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing_stage, null);
    }


    public void remove(Imail_mass_mailing_stage mail_mass_mailing_stage){
        mail_mass_mailing_stageFeignClient.remove(mail_mass_mailing_stage.getId()) ;
    }


    public void get(Imail_mass_mailing_stage mail_mass_mailing_stage){
        Imail_mass_mailing_stage clientModel = mail_mass_mailing_stageFeignClient.get(mail_mass_mailing_stage.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing_stage.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing_stage, null);
    }


    public Page<Imail_mass_mailing_stage> fetchDefault(SearchContext context){
        Page<mail_mass_mailing_stageImpl> page = this.mail_mass_mailing_stageFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Imail_mass_mailing_stage> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_mass_mailing_stage mail_mass_mailing_stage){
        Imail_mass_mailing_stage clientModel = mail_mass_mailing_stageFeignClient.getDraft(mail_mass_mailing_stage.getId(),(mail_mass_mailing_stageImpl)mail_mass_mailing_stage) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing_stage.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing_stage, null);
    }



}

