package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_activity_type;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_activity_typeClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_activity_typeImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_activity_typeFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_activity_type] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_activity_typeClientServiceImpl implements Imail_activity_typeClientService {

    mail_activity_typeFeignClient mail_activity_typeFeignClient;

    @Autowired
    public mail_activity_typeClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_activity_typeFeignClient = nameBuilder.target(mail_activity_typeFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_activity_typeFeignClient = nameBuilder.target(mail_activity_typeFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_activity_type createModel() {
		return new mail_activity_typeImpl();
	}


    public Page<Imail_activity_type> fetchDefault(SearchContext context){
        Page<mail_activity_typeImpl> page = this.mail_activity_typeFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Imail_activity_type mail_activity_type){
        Imail_activity_type clientModel = mail_activity_typeFeignClient.update(mail_activity_type.getId(),(mail_activity_typeImpl)mail_activity_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_activity_type.getClass(), false);
        copier.copy(clientModel, mail_activity_type, null);
    }


    public void create(Imail_activity_type mail_activity_type){
        Imail_activity_type clientModel = mail_activity_typeFeignClient.create((mail_activity_typeImpl)mail_activity_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_activity_type.getClass(), false);
        copier.copy(clientModel, mail_activity_type, null);
    }


    public void get(Imail_activity_type mail_activity_type){
        Imail_activity_type clientModel = mail_activity_typeFeignClient.get(mail_activity_type.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_activity_type.getClass(), false);
        copier.copy(clientModel, mail_activity_type, null);
    }


    public void updateBatch(List<Imail_activity_type> mail_activity_types){
        if(mail_activity_types!=null){
            List<mail_activity_typeImpl> list = new ArrayList<mail_activity_typeImpl>();
            for(Imail_activity_type imail_activity_type :mail_activity_types){
                list.add((mail_activity_typeImpl)imail_activity_type) ;
            }
            mail_activity_typeFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Imail_activity_type> mail_activity_types){
        if(mail_activity_types!=null){
            List<mail_activity_typeImpl> list = new ArrayList<mail_activity_typeImpl>();
            for(Imail_activity_type imail_activity_type :mail_activity_types){
                list.add((mail_activity_typeImpl)imail_activity_type) ;
            }
            mail_activity_typeFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Imail_activity_type mail_activity_type){
        mail_activity_typeFeignClient.remove(mail_activity_type.getId()) ;
    }


    public void createBatch(List<Imail_activity_type> mail_activity_types){
        if(mail_activity_types!=null){
            List<mail_activity_typeImpl> list = new ArrayList<mail_activity_typeImpl>();
            for(Imail_activity_type imail_activity_type :mail_activity_types){
                list.add((mail_activity_typeImpl)imail_activity_type) ;
            }
            mail_activity_typeFeignClient.createBatch(list) ;
        }
    }


    public Page<Imail_activity_type> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_activity_type mail_activity_type){
        Imail_activity_type clientModel = mail_activity_typeFeignClient.getDraft(mail_activity_type.getId(),(mail_activity_typeImpl)mail_activity_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_activity_type.getClass(), false);
        copier.copy(clientModel, mail_activity_type, null);
    }


    public void checkKey(Imail_activity_type mail_activity_type){
        Imail_activity_type clientModel = mail_activity_typeFeignClient.checkKey(mail_activity_type.getId(),(mail_activity_typeImpl)mail_activity_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_activity_type.getClass(), false);
        copier.copy(clientModel, mail_activity_type, null);
    }


    public void save(Imail_activity_type mail_activity_type){
        Imail_activity_type clientModel = mail_activity_typeFeignClient.save(mail_activity_type.getId(),(mail_activity_typeImpl)mail_activity_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_activity_type.getClass(), false);
        copier.copy(clientModel, mail_activity_type, null);
    }



}

