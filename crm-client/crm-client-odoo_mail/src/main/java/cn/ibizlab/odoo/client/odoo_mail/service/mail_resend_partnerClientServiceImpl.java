package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_resend_partner;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_resend_partnerClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_resend_partnerImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_resend_partnerFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_resend_partner] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_resend_partnerClientServiceImpl implements Imail_resend_partnerClientService {

    mail_resend_partnerFeignClient mail_resend_partnerFeignClient;

    @Autowired
    public mail_resend_partnerClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_resend_partnerFeignClient = nameBuilder.target(mail_resend_partnerFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_resend_partnerFeignClient = nameBuilder.target(mail_resend_partnerFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_resend_partner createModel() {
		return new mail_resend_partnerImpl();
	}


    public void get(Imail_resend_partner mail_resend_partner){
        Imail_resend_partner clientModel = mail_resend_partnerFeignClient.get(mail_resend_partner.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_resend_partner.getClass(), false);
        copier.copy(clientModel, mail_resend_partner, null);
    }


    public void update(Imail_resend_partner mail_resend_partner){
        Imail_resend_partner clientModel = mail_resend_partnerFeignClient.update(mail_resend_partner.getId(),(mail_resend_partnerImpl)mail_resend_partner) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_resend_partner.getClass(), false);
        copier.copy(clientModel, mail_resend_partner, null);
    }


    public void updateBatch(List<Imail_resend_partner> mail_resend_partners){
        if(mail_resend_partners!=null){
            List<mail_resend_partnerImpl> list = new ArrayList<mail_resend_partnerImpl>();
            for(Imail_resend_partner imail_resend_partner :mail_resend_partners){
                list.add((mail_resend_partnerImpl)imail_resend_partner) ;
            }
            mail_resend_partnerFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Imail_resend_partner> mail_resend_partners){
        if(mail_resend_partners!=null){
            List<mail_resend_partnerImpl> list = new ArrayList<mail_resend_partnerImpl>();
            for(Imail_resend_partner imail_resend_partner :mail_resend_partners){
                list.add((mail_resend_partnerImpl)imail_resend_partner) ;
            }
            mail_resend_partnerFeignClient.removeBatch(list) ;
        }
    }


    public Page<Imail_resend_partner> fetchDefault(SearchContext context){
        Page<mail_resend_partnerImpl> page = this.mail_resend_partnerFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Imail_resend_partner mail_resend_partner){
        Imail_resend_partner clientModel = mail_resend_partnerFeignClient.create((mail_resend_partnerImpl)mail_resend_partner) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_resend_partner.getClass(), false);
        copier.copy(clientModel, mail_resend_partner, null);
    }


    public void createBatch(List<Imail_resend_partner> mail_resend_partners){
        if(mail_resend_partners!=null){
            List<mail_resend_partnerImpl> list = new ArrayList<mail_resend_partnerImpl>();
            for(Imail_resend_partner imail_resend_partner :mail_resend_partners){
                list.add((mail_resend_partnerImpl)imail_resend_partner) ;
            }
            mail_resend_partnerFeignClient.createBatch(list) ;
        }
    }


    public void remove(Imail_resend_partner mail_resend_partner){
        mail_resend_partnerFeignClient.remove(mail_resend_partner.getId()) ;
    }


    public Page<Imail_resend_partner> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_resend_partner mail_resend_partner){
        Imail_resend_partner clientModel = mail_resend_partnerFeignClient.getDraft(mail_resend_partner.getId(),(mail_resend_partnerImpl)mail_resend_partner) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_resend_partner.getClass(), false);
        copier.copy(clientModel, mail_resend_partner, null);
    }



}

