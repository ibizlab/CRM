package cn.ibizlab.odoo.client.odoo_mail.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Imail_tracking_value;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[mail_tracking_value] 对象
 */
public class mail_tracking_valueImpl implements Imail_tracking_value,Serializable{

    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 更改的字段
     */
    public String field;

    @JsonIgnore
    public boolean fieldDirtyFlag;
    
    /**
     * 字段说明
     */
    public String field_desc;

    @JsonIgnore
    public boolean field_descDirtyFlag;
    
    /**
     * 字段类型
     */
    public String field_type;

    @JsonIgnore
    public boolean field_typeDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 邮件消息ID
     */
    public Integer mail_message_id;

    @JsonIgnore
    public boolean mail_message_idDirtyFlag;
    
    /**
     * 新字符值
     */
    public String new_value_char;

    @JsonIgnore
    public boolean new_value_charDirtyFlag;
    
    /**
     * 新日期时间值
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp new_value_datetime;

    @JsonIgnore
    public boolean new_value_datetimeDirtyFlag;
    
    /**
     * 新浮点值
     */
    public Double new_value_float;

    @JsonIgnore
    public boolean new_value_floatDirtyFlag;
    
    /**
     * 新整数值
     */
    public Integer new_value_integer;

    @JsonIgnore
    public boolean new_value_integerDirtyFlag;
    
    /**
     * 新货币值
     */
    public Double new_value_monetary;

    @JsonIgnore
    public boolean new_value_monetaryDirtyFlag;
    
    /**
     * 新文本值
     */
    public String new_value_text;

    @JsonIgnore
    public boolean new_value_textDirtyFlag;
    
    /**
     * 旧字符值
     */
    public String old_value_char;

    @JsonIgnore
    public boolean old_value_charDirtyFlag;
    
    /**
     * 旧日期时间值
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp old_value_datetime;

    @JsonIgnore
    public boolean old_value_datetimeDirtyFlag;
    
    /**
     * 旧浮点值
     */
    public Double old_value_float;

    @JsonIgnore
    public boolean old_value_floatDirtyFlag;
    
    /**
     * 旧整数值
     */
    public Integer old_value_integer;

    @JsonIgnore
    public boolean old_value_integerDirtyFlag;
    
    /**
     * 旧货币值
     */
    public Double old_value_monetary;

    @JsonIgnore
    public boolean old_value_monetaryDirtyFlag;
    
    /**
     * 旧文本值
     */
    public String old_value_text;

    @JsonIgnore
    public boolean old_value_textDirtyFlag;
    
    /**
     * 跟踪字段次序
     */
    public Integer track_sequence;

    @JsonIgnore
    public boolean track_sequenceDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [更改的字段]
     */
    @JsonProperty("field")
    public String getField(){
        return this.field ;
    }

    /**
     * 设置 [更改的字段]
     */
    @JsonProperty("field")
    public void setField(String  field){
        this.field = field ;
        this.fieldDirtyFlag = true ;
    }

     /**
     * 获取 [更改的字段]脏标记
     */
    @JsonIgnore
    public boolean getFieldDirtyFlag(){
        return this.fieldDirtyFlag ;
    }   

    /**
     * 获取 [字段说明]
     */
    @JsonProperty("field_desc")
    public String getField_desc(){
        return this.field_desc ;
    }

    /**
     * 设置 [字段说明]
     */
    @JsonProperty("field_desc")
    public void setField_desc(String  field_desc){
        this.field_desc = field_desc ;
        this.field_descDirtyFlag = true ;
    }

     /**
     * 获取 [字段说明]脏标记
     */
    @JsonIgnore
    public boolean getField_descDirtyFlag(){
        return this.field_descDirtyFlag ;
    }   

    /**
     * 获取 [字段类型]
     */
    @JsonProperty("field_type")
    public String getField_type(){
        return this.field_type ;
    }

    /**
     * 设置 [字段类型]
     */
    @JsonProperty("field_type")
    public void setField_type(String  field_type){
        this.field_type = field_type ;
        this.field_typeDirtyFlag = true ;
    }

     /**
     * 获取 [字段类型]脏标记
     */
    @JsonIgnore
    public boolean getField_typeDirtyFlag(){
        return this.field_typeDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [邮件消息ID]
     */
    @JsonProperty("mail_message_id")
    public Integer getMail_message_id(){
        return this.mail_message_id ;
    }

    /**
     * 设置 [邮件消息ID]
     */
    @JsonProperty("mail_message_id")
    public void setMail_message_id(Integer  mail_message_id){
        this.mail_message_id = mail_message_id ;
        this.mail_message_idDirtyFlag = true ;
    }

     /**
     * 获取 [邮件消息ID]脏标记
     */
    @JsonIgnore
    public boolean getMail_message_idDirtyFlag(){
        return this.mail_message_idDirtyFlag ;
    }   

    /**
     * 获取 [新字符值]
     */
    @JsonProperty("new_value_char")
    public String getNew_value_char(){
        return this.new_value_char ;
    }

    /**
     * 设置 [新字符值]
     */
    @JsonProperty("new_value_char")
    public void setNew_value_char(String  new_value_char){
        this.new_value_char = new_value_char ;
        this.new_value_charDirtyFlag = true ;
    }

     /**
     * 获取 [新字符值]脏标记
     */
    @JsonIgnore
    public boolean getNew_value_charDirtyFlag(){
        return this.new_value_charDirtyFlag ;
    }   

    /**
     * 获取 [新日期时间值]
     */
    @JsonProperty("new_value_datetime")
    public Timestamp getNew_value_datetime(){
        return this.new_value_datetime ;
    }

    /**
     * 设置 [新日期时间值]
     */
    @JsonProperty("new_value_datetime")
    public void setNew_value_datetime(Timestamp  new_value_datetime){
        this.new_value_datetime = new_value_datetime ;
        this.new_value_datetimeDirtyFlag = true ;
    }

     /**
     * 获取 [新日期时间值]脏标记
     */
    @JsonIgnore
    public boolean getNew_value_datetimeDirtyFlag(){
        return this.new_value_datetimeDirtyFlag ;
    }   

    /**
     * 获取 [新浮点值]
     */
    @JsonProperty("new_value_float")
    public Double getNew_value_float(){
        return this.new_value_float ;
    }

    /**
     * 设置 [新浮点值]
     */
    @JsonProperty("new_value_float")
    public void setNew_value_float(Double  new_value_float){
        this.new_value_float = new_value_float ;
        this.new_value_floatDirtyFlag = true ;
    }

     /**
     * 获取 [新浮点值]脏标记
     */
    @JsonIgnore
    public boolean getNew_value_floatDirtyFlag(){
        return this.new_value_floatDirtyFlag ;
    }   

    /**
     * 获取 [新整数值]
     */
    @JsonProperty("new_value_integer")
    public Integer getNew_value_integer(){
        return this.new_value_integer ;
    }

    /**
     * 设置 [新整数值]
     */
    @JsonProperty("new_value_integer")
    public void setNew_value_integer(Integer  new_value_integer){
        this.new_value_integer = new_value_integer ;
        this.new_value_integerDirtyFlag = true ;
    }

     /**
     * 获取 [新整数值]脏标记
     */
    @JsonIgnore
    public boolean getNew_value_integerDirtyFlag(){
        return this.new_value_integerDirtyFlag ;
    }   

    /**
     * 获取 [新货币值]
     */
    @JsonProperty("new_value_monetary")
    public Double getNew_value_monetary(){
        return this.new_value_monetary ;
    }

    /**
     * 设置 [新货币值]
     */
    @JsonProperty("new_value_monetary")
    public void setNew_value_monetary(Double  new_value_monetary){
        this.new_value_monetary = new_value_monetary ;
        this.new_value_monetaryDirtyFlag = true ;
    }

     /**
     * 获取 [新货币值]脏标记
     */
    @JsonIgnore
    public boolean getNew_value_monetaryDirtyFlag(){
        return this.new_value_monetaryDirtyFlag ;
    }   

    /**
     * 获取 [新文本值]
     */
    @JsonProperty("new_value_text")
    public String getNew_value_text(){
        return this.new_value_text ;
    }

    /**
     * 设置 [新文本值]
     */
    @JsonProperty("new_value_text")
    public void setNew_value_text(String  new_value_text){
        this.new_value_text = new_value_text ;
        this.new_value_textDirtyFlag = true ;
    }

     /**
     * 获取 [新文本值]脏标记
     */
    @JsonIgnore
    public boolean getNew_value_textDirtyFlag(){
        return this.new_value_textDirtyFlag ;
    }   

    /**
     * 获取 [旧字符值]
     */
    @JsonProperty("old_value_char")
    public String getOld_value_char(){
        return this.old_value_char ;
    }

    /**
     * 设置 [旧字符值]
     */
    @JsonProperty("old_value_char")
    public void setOld_value_char(String  old_value_char){
        this.old_value_char = old_value_char ;
        this.old_value_charDirtyFlag = true ;
    }

     /**
     * 获取 [旧字符值]脏标记
     */
    @JsonIgnore
    public boolean getOld_value_charDirtyFlag(){
        return this.old_value_charDirtyFlag ;
    }   

    /**
     * 获取 [旧日期时间值]
     */
    @JsonProperty("old_value_datetime")
    public Timestamp getOld_value_datetime(){
        return this.old_value_datetime ;
    }

    /**
     * 设置 [旧日期时间值]
     */
    @JsonProperty("old_value_datetime")
    public void setOld_value_datetime(Timestamp  old_value_datetime){
        this.old_value_datetime = old_value_datetime ;
        this.old_value_datetimeDirtyFlag = true ;
    }

     /**
     * 获取 [旧日期时间值]脏标记
     */
    @JsonIgnore
    public boolean getOld_value_datetimeDirtyFlag(){
        return this.old_value_datetimeDirtyFlag ;
    }   

    /**
     * 获取 [旧浮点值]
     */
    @JsonProperty("old_value_float")
    public Double getOld_value_float(){
        return this.old_value_float ;
    }

    /**
     * 设置 [旧浮点值]
     */
    @JsonProperty("old_value_float")
    public void setOld_value_float(Double  old_value_float){
        this.old_value_float = old_value_float ;
        this.old_value_floatDirtyFlag = true ;
    }

     /**
     * 获取 [旧浮点值]脏标记
     */
    @JsonIgnore
    public boolean getOld_value_floatDirtyFlag(){
        return this.old_value_floatDirtyFlag ;
    }   

    /**
     * 获取 [旧整数值]
     */
    @JsonProperty("old_value_integer")
    public Integer getOld_value_integer(){
        return this.old_value_integer ;
    }

    /**
     * 设置 [旧整数值]
     */
    @JsonProperty("old_value_integer")
    public void setOld_value_integer(Integer  old_value_integer){
        this.old_value_integer = old_value_integer ;
        this.old_value_integerDirtyFlag = true ;
    }

     /**
     * 获取 [旧整数值]脏标记
     */
    @JsonIgnore
    public boolean getOld_value_integerDirtyFlag(){
        return this.old_value_integerDirtyFlag ;
    }   

    /**
     * 获取 [旧货币值]
     */
    @JsonProperty("old_value_monetary")
    public Double getOld_value_monetary(){
        return this.old_value_monetary ;
    }

    /**
     * 设置 [旧货币值]
     */
    @JsonProperty("old_value_monetary")
    public void setOld_value_monetary(Double  old_value_monetary){
        this.old_value_monetary = old_value_monetary ;
        this.old_value_monetaryDirtyFlag = true ;
    }

     /**
     * 获取 [旧货币值]脏标记
     */
    @JsonIgnore
    public boolean getOld_value_monetaryDirtyFlag(){
        return this.old_value_monetaryDirtyFlag ;
    }   

    /**
     * 获取 [旧文本值]
     */
    @JsonProperty("old_value_text")
    public String getOld_value_text(){
        return this.old_value_text ;
    }

    /**
     * 设置 [旧文本值]
     */
    @JsonProperty("old_value_text")
    public void setOld_value_text(String  old_value_text){
        this.old_value_text = old_value_text ;
        this.old_value_textDirtyFlag = true ;
    }

     /**
     * 获取 [旧文本值]脏标记
     */
    @JsonIgnore
    public boolean getOld_value_textDirtyFlag(){
        return this.old_value_textDirtyFlag ;
    }   

    /**
     * 获取 [跟踪字段次序]
     */
    @JsonProperty("track_sequence")
    public Integer getTrack_sequence(){
        return this.track_sequence ;
    }

    /**
     * 设置 [跟踪字段次序]
     */
    @JsonProperty("track_sequence")
    public void setTrack_sequence(Integer  track_sequence){
        this.track_sequence = track_sequence ;
        this.track_sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [跟踪字段次序]脏标记
     */
    @JsonIgnore
    public boolean getTrack_sequenceDirtyFlag(){
        return this.track_sequenceDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
