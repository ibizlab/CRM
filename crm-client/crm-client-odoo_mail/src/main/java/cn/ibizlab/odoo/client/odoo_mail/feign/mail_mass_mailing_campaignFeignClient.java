package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_campaign;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mass_mailing_campaignImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_mass_mailing_campaign] 服务对象接口
 */
public interface mail_mass_mailing_campaignFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailing_campaigns/{id}")
    public mail_mass_mailing_campaignImpl update(@PathVariable("id") Integer id,@RequestBody mail_mass_mailing_campaignImpl mail_mass_mailing_campaign);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailing_campaigns/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_campaigns/fetchdefault")
    public Page<mail_mass_mailing_campaignImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailing_campaigns/createbatch")
    public mail_mass_mailing_campaignImpl createBatch(@RequestBody List<mail_mass_mailing_campaignImpl> mail_mass_mailing_campaigns);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailing_campaigns/updatebatch")
    public mail_mass_mailing_campaignImpl updateBatch(@RequestBody List<mail_mass_mailing_campaignImpl> mail_mass_mailing_campaigns);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_campaigns/{id}")
    public mail_mass_mailing_campaignImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailing_campaigns")
    public mail_mass_mailing_campaignImpl create(@RequestBody mail_mass_mailing_campaignImpl mail_mass_mailing_campaign);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailing_campaigns/removebatch")
    public mail_mass_mailing_campaignImpl removeBatch(@RequestBody List<mail_mass_mailing_campaignImpl> mail_mass_mailing_campaigns);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_campaigns/select")
    public Page<mail_mass_mailing_campaignImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_campaigns/{id}/getdraft")
    public mail_mass_mailing_campaignImpl getDraft(@PathVariable("id") Integer id,@RequestBody mail_mass_mailing_campaignImpl mail_mass_mailing_campaign);



}
