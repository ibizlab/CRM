package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_contact;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_mass_mailing_contactClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mass_mailing_contactImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_mass_mailing_contactFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_mass_mailing_contact] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_mass_mailing_contactClientServiceImpl implements Imail_mass_mailing_contactClientService {

    mail_mass_mailing_contactFeignClient mail_mass_mailing_contactFeignClient;

    @Autowired
    public mail_mass_mailing_contactClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_mass_mailing_contactFeignClient = nameBuilder.target(mail_mass_mailing_contactFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_mass_mailing_contactFeignClient = nameBuilder.target(mail_mass_mailing_contactFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_mass_mailing_contact createModel() {
		return new mail_mass_mailing_contactImpl();
	}


    public Page<Imail_mass_mailing_contact> fetchDefault(SearchContext context){
        Page<mail_mass_mailing_contactImpl> page = this.mail_mass_mailing_contactFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Imail_mass_mailing_contact> mail_mass_mailing_contacts){
        if(mail_mass_mailing_contacts!=null){
            List<mail_mass_mailing_contactImpl> list = new ArrayList<mail_mass_mailing_contactImpl>();
            for(Imail_mass_mailing_contact imail_mass_mailing_contact :mail_mass_mailing_contacts){
                list.add((mail_mass_mailing_contactImpl)imail_mass_mailing_contact) ;
            }
            mail_mass_mailing_contactFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Imail_mass_mailing_contact> mail_mass_mailing_contacts){
        if(mail_mass_mailing_contacts!=null){
            List<mail_mass_mailing_contactImpl> list = new ArrayList<mail_mass_mailing_contactImpl>();
            for(Imail_mass_mailing_contact imail_mass_mailing_contact :mail_mass_mailing_contacts){
                list.add((mail_mass_mailing_contactImpl)imail_mass_mailing_contact) ;
            }
            mail_mass_mailing_contactFeignClient.updateBatch(list) ;
        }
    }


    public void create(Imail_mass_mailing_contact mail_mass_mailing_contact){
        Imail_mass_mailing_contact clientModel = mail_mass_mailing_contactFeignClient.create((mail_mass_mailing_contactImpl)mail_mass_mailing_contact) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing_contact.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing_contact, null);
    }


    public void createBatch(List<Imail_mass_mailing_contact> mail_mass_mailing_contacts){
        if(mail_mass_mailing_contacts!=null){
            List<mail_mass_mailing_contactImpl> list = new ArrayList<mail_mass_mailing_contactImpl>();
            for(Imail_mass_mailing_contact imail_mass_mailing_contact :mail_mass_mailing_contacts){
                list.add((mail_mass_mailing_contactImpl)imail_mass_mailing_contact) ;
            }
            mail_mass_mailing_contactFeignClient.createBatch(list) ;
        }
    }


    public void get(Imail_mass_mailing_contact mail_mass_mailing_contact){
        Imail_mass_mailing_contact clientModel = mail_mass_mailing_contactFeignClient.get(mail_mass_mailing_contact.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing_contact.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing_contact, null);
    }


    public void update(Imail_mass_mailing_contact mail_mass_mailing_contact){
        Imail_mass_mailing_contact clientModel = mail_mass_mailing_contactFeignClient.update(mail_mass_mailing_contact.getId(),(mail_mass_mailing_contactImpl)mail_mass_mailing_contact) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing_contact.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing_contact, null);
    }


    public void remove(Imail_mass_mailing_contact mail_mass_mailing_contact){
        mail_mass_mailing_contactFeignClient.remove(mail_mass_mailing_contact.getId()) ;
    }


    public Page<Imail_mass_mailing_contact> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_mass_mailing_contact mail_mass_mailing_contact){
        Imail_mass_mailing_contact clientModel = mail_mass_mailing_contactFeignClient.getDraft(mail_mass_mailing_contact.getId(),(mail_mass_mailing_contactImpl)mail_mass_mailing_contact) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing_contact.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing_contact, null);
    }



}

