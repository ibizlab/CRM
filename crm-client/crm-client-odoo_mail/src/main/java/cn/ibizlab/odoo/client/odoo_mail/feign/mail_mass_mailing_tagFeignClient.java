package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_tag;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mass_mailing_tagImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_mass_mailing_tag] 服务对象接口
 */
public interface mail_mass_mailing_tagFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailing_tags/removebatch")
    public mail_mass_mailing_tagImpl removeBatch(@RequestBody List<mail_mass_mailing_tagImpl> mail_mass_mailing_tags);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailing_tags/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_tags/fetchdefault")
    public Page<mail_mass_mailing_tagImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailing_tags/updatebatch")
    public mail_mass_mailing_tagImpl updateBatch(@RequestBody List<mail_mass_mailing_tagImpl> mail_mass_mailing_tags);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailing_tags")
    public mail_mass_mailing_tagImpl create(@RequestBody mail_mass_mailing_tagImpl mail_mass_mailing_tag);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailing_tags/{id}")
    public mail_mass_mailing_tagImpl update(@PathVariable("id") Integer id,@RequestBody mail_mass_mailing_tagImpl mail_mass_mailing_tag);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailing_tags/createbatch")
    public mail_mass_mailing_tagImpl createBatch(@RequestBody List<mail_mass_mailing_tagImpl> mail_mass_mailing_tags);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_tags/{id}")
    public mail_mass_mailing_tagImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_tags/select")
    public Page<mail_mass_mailing_tagImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_tags/{id}/getdraft")
    public mail_mass_mailing_tagImpl getDraft(@PathVariable("id") Integer id,@RequestBody mail_mass_mailing_tagImpl mail_mass_mailing_tag);



}
