package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_blacklist_mixin;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_blacklist_mixinClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_blacklist_mixinImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_blacklist_mixinFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_blacklist_mixin] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_blacklist_mixinClientServiceImpl implements Imail_blacklist_mixinClientService {

    mail_blacklist_mixinFeignClient mail_blacklist_mixinFeignClient;

    @Autowired
    public mail_blacklist_mixinClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_blacklist_mixinFeignClient = nameBuilder.target(mail_blacklist_mixinFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_blacklist_mixinFeignClient = nameBuilder.target(mail_blacklist_mixinFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_blacklist_mixin createModel() {
		return new mail_blacklist_mixinImpl();
	}


    public void create(Imail_blacklist_mixin mail_blacklist_mixin){
        Imail_blacklist_mixin clientModel = mail_blacklist_mixinFeignClient.create((mail_blacklist_mixinImpl)mail_blacklist_mixin) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_blacklist_mixin.getClass(), false);
        copier.copy(clientModel, mail_blacklist_mixin, null);
    }


    public Page<Imail_blacklist_mixin> fetchDefault(SearchContext context){
        Page<mail_blacklist_mixinImpl> page = this.mail_blacklist_mixinFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Imail_blacklist_mixin> mail_blacklist_mixins){
        if(mail_blacklist_mixins!=null){
            List<mail_blacklist_mixinImpl> list = new ArrayList<mail_blacklist_mixinImpl>();
            for(Imail_blacklist_mixin imail_blacklist_mixin :mail_blacklist_mixins){
                list.add((mail_blacklist_mixinImpl)imail_blacklist_mixin) ;
            }
            mail_blacklist_mixinFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Imail_blacklist_mixin> mail_blacklist_mixins){
        if(mail_blacklist_mixins!=null){
            List<mail_blacklist_mixinImpl> list = new ArrayList<mail_blacklist_mixinImpl>();
            for(Imail_blacklist_mixin imail_blacklist_mixin :mail_blacklist_mixins){
                list.add((mail_blacklist_mixinImpl)imail_blacklist_mixin) ;
            }
            mail_blacklist_mixinFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Imail_blacklist_mixin mail_blacklist_mixin){
        mail_blacklist_mixinFeignClient.remove(mail_blacklist_mixin.getId()) ;
    }


    public void removeBatch(List<Imail_blacklist_mixin> mail_blacklist_mixins){
        if(mail_blacklist_mixins!=null){
            List<mail_blacklist_mixinImpl> list = new ArrayList<mail_blacklist_mixinImpl>();
            for(Imail_blacklist_mixin imail_blacklist_mixin :mail_blacklist_mixins){
                list.add((mail_blacklist_mixinImpl)imail_blacklist_mixin) ;
            }
            mail_blacklist_mixinFeignClient.removeBatch(list) ;
        }
    }


    public void update(Imail_blacklist_mixin mail_blacklist_mixin){
        Imail_blacklist_mixin clientModel = mail_blacklist_mixinFeignClient.update(mail_blacklist_mixin.getId(),(mail_blacklist_mixinImpl)mail_blacklist_mixin) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_blacklist_mixin.getClass(), false);
        copier.copy(clientModel, mail_blacklist_mixin, null);
    }


    public void get(Imail_blacklist_mixin mail_blacklist_mixin){
        Imail_blacklist_mixin clientModel = mail_blacklist_mixinFeignClient.get(mail_blacklist_mixin.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_blacklist_mixin.getClass(), false);
        copier.copy(clientModel, mail_blacklist_mixin, null);
    }


    public Page<Imail_blacklist_mixin> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_blacklist_mixin mail_blacklist_mixin){
        Imail_blacklist_mixin clientModel = mail_blacklist_mixinFeignClient.getDraft(mail_blacklist_mixin.getId(),(mail_blacklist_mixinImpl)mail_blacklist_mixin) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_blacklist_mixin.getClass(), false);
        copier.copy(clientModel, mail_blacklist_mixin, null);
    }



}

