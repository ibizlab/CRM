package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_message;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_messageImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_message] 服务对象接口
 */
public interface mail_messageFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_messages/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_messages/updatebatch")
    public mail_messageImpl updateBatch(@RequestBody List<mail_messageImpl> mail_messages);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_messages")
    public mail_messageImpl create(@RequestBody mail_messageImpl mail_message);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_messages/createbatch")
    public mail_messageImpl createBatch(@RequestBody List<mail_messageImpl> mail_messages);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_messages/fetchdefault")
    public Page<mail_messageImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_messages/{id}")
    public mail_messageImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_messages/removebatch")
    public mail_messageImpl removeBatch(@RequestBody List<mail_messageImpl> mail_messages);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_messages/{id}")
    public mail_messageImpl update(@PathVariable("id") Integer id,@RequestBody mail_messageImpl mail_message);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_messages/select")
    public Page<mail_messageImpl> select();


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_messages/{id}/checkkey")
    public mail_messageImpl checkKey(@PathVariable("id") Integer id,@RequestBody mail_messageImpl mail_message);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_messages/{id}/getdraft")
    public mail_messageImpl getDraft(@PathVariable("id") Integer id,@RequestBody mail_messageImpl mail_message);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_messages/{id}/save")
    public mail_messageImpl save(@PathVariable("id") Integer id,@RequestBody mail_messageImpl mail_message);



}
