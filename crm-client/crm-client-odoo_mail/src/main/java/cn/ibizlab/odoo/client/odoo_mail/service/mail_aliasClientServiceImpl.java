package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_alias;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_aliasClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_aliasImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_aliasFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_alias] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_aliasClientServiceImpl implements Imail_aliasClientService {

    mail_aliasFeignClient mail_aliasFeignClient;

    @Autowired
    public mail_aliasClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_aliasFeignClient = nameBuilder.target(mail_aliasFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_aliasFeignClient = nameBuilder.target(mail_aliasFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_alias createModel() {
		return new mail_aliasImpl();
	}


    public Page<Imail_alias> fetchDefault(SearchContext context){
        Page<mail_aliasImpl> page = this.mail_aliasFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Imail_alias> mail_aliases){
        if(mail_aliases!=null){
            List<mail_aliasImpl> list = new ArrayList<mail_aliasImpl>();
            for(Imail_alias imail_alias :mail_aliases){
                list.add((mail_aliasImpl)imail_alias) ;
            }
            mail_aliasFeignClient.removeBatch(list) ;
        }
    }


    public void update(Imail_alias mail_alias){
        Imail_alias clientModel = mail_aliasFeignClient.update(mail_alias.getId(),(mail_aliasImpl)mail_alias) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_alias.getClass(), false);
        copier.copy(clientModel, mail_alias, null);
    }


    public void updateBatch(List<Imail_alias> mail_aliases){
        if(mail_aliases!=null){
            List<mail_aliasImpl> list = new ArrayList<mail_aliasImpl>();
            for(Imail_alias imail_alias :mail_aliases){
                list.add((mail_aliasImpl)imail_alias) ;
            }
            mail_aliasFeignClient.updateBatch(list) ;
        }
    }


    public void get(Imail_alias mail_alias){
        Imail_alias clientModel = mail_aliasFeignClient.get(mail_alias.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_alias.getClass(), false);
        copier.copy(clientModel, mail_alias, null);
    }


    public void remove(Imail_alias mail_alias){
        mail_aliasFeignClient.remove(mail_alias.getId()) ;
    }


    public void create(Imail_alias mail_alias){
        Imail_alias clientModel = mail_aliasFeignClient.create((mail_aliasImpl)mail_alias) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_alias.getClass(), false);
        copier.copy(clientModel, mail_alias, null);
    }


    public void createBatch(List<Imail_alias> mail_aliases){
        if(mail_aliases!=null){
            List<mail_aliasImpl> list = new ArrayList<mail_aliasImpl>();
            for(Imail_alias imail_alias :mail_aliases){
                list.add((mail_aliasImpl)imail_alias) ;
            }
            mail_aliasFeignClient.createBatch(list) ;
        }
    }


    public Page<Imail_alias> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_alias mail_alias){
        Imail_alias clientModel = mail_aliasFeignClient.getDraft(mail_alias.getId(),(mail_aliasImpl)mail_alias) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_alias.getClass(), false);
        copier.copy(clientModel, mail_alias, null);
    }



}

