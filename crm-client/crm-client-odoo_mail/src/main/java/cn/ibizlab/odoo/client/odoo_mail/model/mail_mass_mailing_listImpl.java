package cn.ibizlab.odoo.client.odoo_mail.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_list;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[mail_mass_mailing_list] 对象
 */
public class mail_mass_mailing_listImpl implements Imail_mass_mailing_list,Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 邮件列表
     */
    public String contact_ids;

    @JsonIgnore
    public boolean contact_idsDirtyFlag;
    
    /**
     * 联系人人数
     */
    public Integer contact_nbr;

    @JsonIgnore
    public boolean contact_nbrDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 公开
     */
    public String is_public;

    @JsonIgnore
    public boolean is_publicDirtyFlag;
    
    /**
     * 邮件列表
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 网站弹出内容
     */
    public String popup_content;

    @JsonIgnore
    public boolean popup_contentDirtyFlag;
    
    /**
     * 网站弹出重新定向网址
     */
    public String popup_redirect_url;

    @JsonIgnore
    public boolean popup_redirect_urlDirtyFlag;
    
    /**
     * 订阅信息
     */
    public String subscription_contact_ids;

    @JsonIgnore
    public boolean subscription_contact_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [邮件列表]
     */
    @JsonProperty("contact_ids")
    public String getContact_ids(){
        return this.contact_ids ;
    }

    /**
     * 设置 [邮件列表]
     */
    @JsonProperty("contact_ids")
    public void setContact_ids(String  contact_ids){
        this.contact_ids = contact_ids ;
        this.contact_idsDirtyFlag = true ;
    }

     /**
     * 获取 [邮件列表]脏标记
     */
    @JsonIgnore
    public boolean getContact_idsDirtyFlag(){
        return this.contact_idsDirtyFlag ;
    }   

    /**
     * 获取 [联系人人数]
     */
    @JsonProperty("contact_nbr")
    public Integer getContact_nbr(){
        return this.contact_nbr ;
    }

    /**
     * 设置 [联系人人数]
     */
    @JsonProperty("contact_nbr")
    public void setContact_nbr(Integer  contact_nbr){
        this.contact_nbr = contact_nbr ;
        this.contact_nbrDirtyFlag = true ;
    }

     /**
     * 获取 [联系人人数]脏标记
     */
    @JsonIgnore
    public boolean getContact_nbrDirtyFlag(){
        return this.contact_nbrDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [公开]
     */
    @JsonProperty("is_public")
    public String getIs_public(){
        return this.is_public ;
    }

    /**
     * 设置 [公开]
     */
    @JsonProperty("is_public")
    public void setIs_public(String  is_public){
        this.is_public = is_public ;
        this.is_publicDirtyFlag = true ;
    }

     /**
     * 获取 [公开]脏标记
     */
    @JsonIgnore
    public boolean getIs_publicDirtyFlag(){
        return this.is_publicDirtyFlag ;
    }   

    /**
     * 获取 [邮件列表]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [邮件列表]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [邮件列表]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [网站弹出内容]
     */
    @JsonProperty("popup_content")
    public String getPopup_content(){
        return this.popup_content ;
    }

    /**
     * 设置 [网站弹出内容]
     */
    @JsonProperty("popup_content")
    public void setPopup_content(String  popup_content){
        this.popup_content = popup_content ;
        this.popup_contentDirtyFlag = true ;
    }

     /**
     * 获取 [网站弹出内容]脏标记
     */
    @JsonIgnore
    public boolean getPopup_contentDirtyFlag(){
        return this.popup_contentDirtyFlag ;
    }   

    /**
     * 获取 [网站弹出重新定向网址]
     */
    @JsonProperty("popup_redirect_url")
    public String getPopup_redirect_url(){
        return this.popup_redirect_url ;
    }

    /**
     * 设置 [网站弹出重新定向网址]
     */
    @JsonProperty("popup_redirect_url")
    public void setPopup_redirect_url(String  popup_redirect_url){
        this.popup_redirect_url = popup_redirect_url ;
        this.popup_redirect_urlDirtyFlag = true ;
    }

     /**
     * 获取 [网站弹出重新定向网址]脏标记
     */
    @JsonIgnore
    public boolean getPopup_redirect_urlDirtyFlag(){
        return this.popup_redirect_urlDirtyFlag ;
    }   

    /**
     * 获取 [订阅信息]
     */
    @JsonProperty("subscription_contact_ids")
    public String getSubscription_contact_ids(){
        return this.subscription_contact_ids ;
    }

    /**
     * 设置 [订阅信息]
     */
    @JsonProperty("subscription_contact_ids")
    public void setSubscription_contact_ids(String  subscription_contact_ids){
        this.subscription_contact_ids = subscription_contact_ids ;
        this.subscription_contact_idsDirtyFlag = true ;
    }

     /**
     * 获取 [订阅信息]脏标记
     */
    @JsonIgnore
    public boolean getSubscription_contact_idsDirtyFlag(){
        return this.subscription_contact_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
