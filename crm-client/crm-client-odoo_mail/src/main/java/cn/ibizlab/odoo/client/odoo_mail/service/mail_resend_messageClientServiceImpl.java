package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_resend_message;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_resend_messageClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_resend_messageImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_resend_messageFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_resend_message] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_resend_messageClientServiceImpl implements Imail_resend_messageClientService {

    mail_resend_messageFeignClient mail_resend_messageFeignClient;

    @Autowired
    public mail_resend_messageClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_resend_messageFeignClient = nameBuilder.target(mail_resend_messageFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_resend_messageFeignClient = nameBuilder.target(mail_resend_messageFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_resend_message createModel() {
		return new mail_resend_messageImpl();
	}


    public void updateBatch(List<Imail_resend_message> mail_resend_messages){
        if(mail_resend_messages!=null){
            List<mail_resend_messageImpl> list = new ArrayList<mail_resend_messageImpl>();
            for(Imail_resend_message imail_resend_message :mail_resend_messages){
                list.add((mail_resend_messageImpl)imail_resend_message) ;
            }
            mail_resend_messageFeignClient.updateBatch(list) ;
        }
    }


    public void update(Imail_resend_message mail_resend_message){
        Imail_resend_message clientModel = mail_resend_messageFeignClient.update(mail_resend_message.getId(),(mail_resend_messageImpl)mail_resend_message) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_resend_message.getClass(), false);
        copier.copy(clientModel, mail_resend_message, null);
    }


    public void remove(Imail_resend_message mail_resend_message){
        mail_resend_messageFeignClient.remove(mail_resend_message.getId()) ;
    }


    public void createBatch(List<Imail_resend_message> mail_resend_messages){
        if(mail_resend_messages!=null){
            List<mail_resend_messageImpl> list = new ArrayList<mail_resend_messageImpl>();
            for(Imail_resend_message imail_resend_message :mail_resend_messages){
                list.add((mail_resend_messageImpl)imail_resend_message) ;
            }
            mail_resend_messageFeignClient.createBatch(list) ;
        }
    }


    public void get(Imail_resend_message mail_resend_message){
        Imail_resend_message clientModel = mail_resend_messageFeignClient.get(mail_resend_message.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_resend_message.getClass(), false);
        copier.copy(clientModel, mail_resend_message, null);
    }


    public Page<Imail_resend_message> fetchDefault(SearchContext context){
        Page<mail_resend_messageImpl> page = this.mail_resend_messageFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Imail_resend_message> mail_resend_messages){
        if(mail_resend_messages!=null){
            List<mail_resend_messageImpl> list = new ArrayList<mail_resend_messageImpl>();
            for(Imail_resend_message imail_resend_message :mail_resend_messages){
                list.add((mail_resend_messageImpl)imail_resend_message) ;
            }
            mail_resend_messageFeignClient.removeBatch(list) ;
        }
    }


    public void create(Imail_resend_message mail_resend_message){
        Imail_resend_message clientModel = mail_resend_messageFeignClient.create((mail_resend_messageImpl)mail_resend_message) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_resend_message.getClass(), false);
        copier.copy(clientModel, mail_resend_message, null);
    }


    public Page<Imail_resend_message> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_resend_message mail_resend_message){
        Imail_resend_message clientModel = mail_resend_messageFeignClient.getDraft(mail_resend_message.getId(),(mail_resend_messageImpl)mail_resend_message) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_resend_message.getClass(), false);
        copier.copy(clientModel, mail_resend_message, null);
    }



}

