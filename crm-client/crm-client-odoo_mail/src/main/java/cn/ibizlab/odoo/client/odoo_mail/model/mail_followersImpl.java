package cn.ibizlab.odoo.client.odoo_mail.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Imail_followers;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[mail_followers] 对象
 */
public class mail_followersImpl implements Imail_followers,Serializable{

    /**
     * 监听器
     */
    public Integer channel_id;

    @JsonIgnore
    public boolean channel_idDirtyFlag;
    
    /**
     * 监听器
     */
    public String channel_id_text;

    @JsonIgnore
    public boolean channel_id_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 相关的业务伙伴
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 相关的业务伙伴
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 相关文档编号
     */
    public Integer res_id;

    @JsonIgnore
    public boolean res_idDirtyFlag;
    
    /**
     * 相关的文档模型名称
     */
    public String res_model;

    @JsonIgnore
    public boolean res_modelDirtyFlag;
    
    /**
     * 子类型
     */
    public String subtype_ids;

    @JsonIgnore
    public boolean subtype_idsDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [监听器]
     */
    @JsonProperty("channel_id")
    public Integer getChannel_id(){
        return this.channel_id ;
    }

    /**
     * 设置 [监听器]
     */
    @JsonProperty("channel_id")
    public void setChannel_id(Integer  channel_id){
        this.channel_id = channel_id ;
        this.channel_idDirtyFlag = true ;
    }

     /**
     * 获取 [监听器]脏标记
     */
    @JsonIgnore
    public boolean getChannel_idDirtyFlag(){
        return this.channel_idDirtyFlag ;
    }   

    /**
     * 获取 [监听器]
     */
    @JsonProperty("channel_id_text")
    public String getChannel_id_text(){
        return this.channel_id_text ;
    }

    /**
     * 设置 [监听器]
     */
    @JsonProperty("channel_id_text")
    public void setChannel_id_text(String  channel_id_text){
        this.channel_id_text = channel_id_text ;
        this.channel_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [监听器]脏标记
     */
    @JsonIgnore
    public boolean getChannel_id_textDirtyFlag(){
        return this.channel_id_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [相关的业务伙伴]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [相关的业务伙伴]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [相关的业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [相关的业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [相关的业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [相关的业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [相关文档编号]
     */
    @JsonProperty("res_id")
    public Integer getRes_id(){
        return this.res_id ;
    }

    /**
     * 设置 [相关文档编号]
     */
    @JsonProperty("res_id")
    public void setRes_id(Integer  res_id){
        this.res_id = res_id ;
        this.res_idDirtyFlag = true ;
    }

     /**
     * 获取 [相关文档编号]脏标记
     */
    @JsonIgnore
    public boolean getRes_idDirtyFlag(){
        return this.res_idDirtyFlag ;
    }   

    /**
     * 获取 [相关的文档模型名称]
     */
    @JsonProperty("res_model")
    public String getRes_model(){
        return this.res_model ;
    }

    /**
     * 设置 [相关的文档模型名称]
     */
    @JsonProperty("res_model")
    public void setRes_model(String  res_model){
        this.res_model = res_model ;
        this.res_modelDirtyFlag = true ;
    }

     /**
     * 获取 [相关的文档模型名称]脏标记
     */
    @JsonIgnore
    public boolean getRes_modelDirtyFlag(){
        return this.res_modelDirtyFlag ;
    }   

    /**
     * 获取 [子类型]
     */
    @JsonProperty("subtype_ids")
    public String getSubtype_ids(){
        return this.subtype_ids ;
    }

    /**
     * 设置 [子类型]
     */
    @JsonProperty("subtype_ids")
    public void setSubtype_ids(String  subtype_ids){
        this.subtype_ids = subtype_ids ;
        this.subtype_idsDirtyFlag = true ;
    }

     /**
     * 获取 [子类型]脏标记
     */
    @JsonIgnore
    public boolean getSubtype_idsDirtyFlag(){
        return this.subtype_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
