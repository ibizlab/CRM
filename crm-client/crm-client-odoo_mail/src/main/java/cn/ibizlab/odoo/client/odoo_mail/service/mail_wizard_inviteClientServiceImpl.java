package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_wizard_invite;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_wizard_inviteClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_wizard_inviteImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_wizard_inviteFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_wizard_invite] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_wizard_inviteClientServiceImpl implements Imail_wizard_inviteClientService {

    mail_wizard_inviteFeignClient mail_wizard_inviteFeignClient;

    @Autowired
    public mail_wizard_inviteClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_wizard_inviteFeignClient = nameBuilder.target(mail_wizard_inviteFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_wizard_inviteFeignClient = nameBuilder.target(mail_wizard_inviteFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_wizard_invite createModel() {
		return new mail_wizard_inviteImpl();
	}


    public Page<Imail_wizard_invite> fetchDefault(SearchContext context){
        Page<mail_wizard_inviteImpl> page = this.mail_wizard_inviteFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Imail_wizard_invite mail_wizard_invite){
        Imail_wizard_invite clientModel = mail_wizard_inviteFeignClient.update(mail_wizard_invite.getId(),(mail_wizard_inviteImpl)mail_wizard_invite) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_wizard_invite.getClass(), false);
        copier.copy(clientModel, mail_wizard_invite, null);
    }


    public void updateBatch(List<Imail_wizard_invite> mail_wizard_invites){
        if(mail_wizard_invites!=null){
            List<mail_wizard_inviteImpl> list = new ArrayList<mail_wizard_inviteImpl>();
            for(Imail_wizard_invite imail_wizard_invite :mail_wizard_invites){
                list.add((mail_wizard_inviteImpl)imail_wizard_invite) ;
            }
            mail_wizard_inviteFeignClient.updateBatch(list) ;
        }
    }


    public void create(Imail_wizard_invite mail_wizard_invite){
        Imail_wizard_invite clientModel = mail_wizard_inviteFeignClient.create((mail_wizard_inviteImpl)mail_wizard_invite) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_wizard_invite.getClass(), false);
        copier.copy(clientModel, mail_wizard_invite, null);
    }


    public void removeBatch(List<Imail_wizard_invite> mail_wizard_invites){
        if(mail_wizard_invites!=null){
            List<mail_wizard_inviteImpl> list = new ArrayList<mail_wizard_inviteImpl>();
            for(Imail_wizard_invite imail_wizard_invite :mail_wizard_invites){
                list.add((mail_wizard_inviteImpl)imail_wizard_invite) ;
            }
            mail_wizard_inviteFeignClient.removeBatch(list) ;
        }
    }


    public void get(Imail_wizard_invite mail_wizard_invite){
        Imail_wizard_invite clientModel = mail_wizard_inviteFeignClient.get(mail_wizard_invite.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_wizard_invite.getClass(), false);
        copier.copy(clientModel, mail_wizard_invite, null);
    }


    public void remove(Imail_wizard_invite mail_wizard_invite){
        mail_wizard_inviteFeignClient.remove(mail_wizard_invite.getId()) ;
    }


    public void createBatch(List<Imail_wizard_invite> mail_wizard_invites){
        if(mail_wizard_invites!=null){
            List<mail_wizard_inviteImpl> list = new ArrayList<mail_wizard_inviteImpl>();
            for(Imail_wizard_invite imail_wizard_invite :mail_wizard_invites){
                list.add((mail_wizard_inviteImpl)imail_wizard_invite) ;
            }
            mail_wizard_inviteFeignClient.createBatch(list) ;
        }
    }


    public Page<Imail_wizard_invite> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_wizard_invite mail_wizard_invite){
        Imail_wizard_invite clientModel = mail_wizard_inviteFeignClient.getDraft(mail_wizard_invite.getId(),(mail_wizard_inviteImpl)mail_wizard_invite) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_wizard_invite.getClass(), false);
        copier.copy(clientModel, mail_wizard_invite, null);
    }



}

