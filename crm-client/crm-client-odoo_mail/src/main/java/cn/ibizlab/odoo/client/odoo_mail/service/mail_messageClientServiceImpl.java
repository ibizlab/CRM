package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_message;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_messageClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_messageImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_messageFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_message] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_messageClientServiceImpl implements Imail_messageClientService {

    mail_messageFeignClient mail_messageFeignClient;

    @Autowired
    public mail_messageClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_messageFeignClient = nameBuilder.target(mail_messageFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_messageFeignClient = nameBuilder.target(mail_messageFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_message createModel() {
		return new mail_messageImpl();
	}


    public void remove(Imail_message mail_message){
        mail_messageFeignClient.remove(mail_message.getId()) ;
    }


    public void updateBatch(List<Imail_message> mail_messages){
        if(mail_messages!=null){
            List<mail_messageImpl> list = new ArrayList<mail_messageImpl>();
            for(Imail_message imail_message :mail_messages){
                list.add((mail_messageImpl)imail_message) ;
            }
            mail_messageFeignClient.updateBatch(list) ;
        }
    }


    public void create(Imail_message mail_message){
        Imail_message clientModel = mail_messageFeignClient.create((mail_messageImpl)mail_message) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_message.getClass(), false);
        copier.copy(clientModel, mail_message, null);
    }


    public void createBatch(List<Imail_message> mail_messages){
        if(mail_messages!=null){
            List<mail_messageImpl> list = new ArrayList<mail_messageImpl>();
            for(Imail_message imail_message :mail_messages){
                list.add((mail_messageImpl)imail_message) ;
            }
            mail_messageFeignClient.createBatch(list) ;
        }
    }


    public Page<Imail_message> fetchDefault(SearchContext context){
        Page<mail_messageImpl> page = this.mail_messageFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Imail_message mail_message){
        Imail_message clientModel = mail_messageFeignClient.get(mail_message.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_message.getClass(), false);
        copier.copy(clientModel, mail_message, null);
    }


    public void removeBatch(List<Imail_message> mail_messages){
        if(mail_messages!=null){
            List<mail_messageImpl> list = new ArrayList<mail_messageImpl>();
            for(Imail_message imail_message :mail_messages){
                list.add((mail_messageImpl)imail_message) ;
            }
            mail_messageFeignClient.removeBatch(list) ;
        }
    }


    public void update(Imail_message mail_message){
        Imail_message clientModel = mail_messageFeignClient.update(mail_message.getId(),(mail_messageImpl)mail_message) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_message.getClass(), false);
        copier.copy(clientModel, mail_message, null);
    }


    public Page<Imail_message> select(SearchContext context){
        return null ;
    }


    public void checkKey(Imail_message mail_message){
        Imail_message clientModel = mail_messageFeignClient.checkKey(mail_message.getId(),(mail_messageImpl)mail_message) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_message.getClass(), false);
        copier.copy(clientModel, mail_message, null);
    }


    public void getDraft(Imail_message mail_message){
        Imail_message clientModel = mail_messageFeignClient.getDraft(mail_message.getId(),(mail_messageImpl)mail_message) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_message.getClass(), false);
        copier.copy(clientModel, mail_message, null);
    }


    public void save(Imail_message mail_message){
        Imail_message clientModel = mail_messageFeignClient.save(mail_message.getId(),(mail_messageImpl)mail_message) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_message.getClass(), false);
        copier.copy(clientModel, mail_message, null);
    }



}

