package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_bot;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_botClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_botImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_botFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_bot] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_botClientServiceImpl implements Imail_botClientService {

    mail_botFeignClient mail_botFeignClient;

    @Autowired
    public mail_botClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_botFeignClient = nameBuilder.target(mail_botFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_botFeignClient = nameBuilder.target(mail_botFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_bot createModel() {
		return new mail_botImpl();
	}


    public void update(Imail_bot mail_bot){
        Imail_bot clientModel = mail_botFeignClient.update(mail_bot.getId(),(mail_botImpl)mail_bot) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_bot.getClass(), false);
        copier.copy(clientModel, mail_bot, null);
    }


    public void create(Imail_bot mail_bot){
        Imail_bot clientModel = mail_botFeignClient.create((mail_botImpl)mail_bot) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_bot.getClass(), false);
        copier.copy(clientModel, mail_bot, null);
    }


    public void createBatch(List<Imail_bot> mail_bots){
        if(mail_bots!=null){
            List<mail_botImpl> list = new ArrayList<mail_botImpl>();
            for(Imail_bot imail_bot :mail_bots){
                list.add((mail_botImpl)imail_bot) ;
            }
            mail_botFeignClient.createBatch(list) ;
        }
    }


    public Page<Imail_bot> fetchDefault(SearchContext context){
        Page<mail_botImpl> page = this.mail_botFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Imail_bot mail_bot){
        Imail_bot clientModel = mail_botFeignClient.get(mail_bot.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_bot.getClass(), false);
        copier.copy(clientModel, mail_bot, null);
    }


    public void removeBatch(List<Imail_bot> mail_bots){
        if(mail_bots!=null){
            List<mail_botImpl> list = new ArrayList<mail_botImpl>();
            for(Imail_bot imail_bot :mail_bots){
                list.add((mail_botImpl)imail_bot) ;
            }
            mail_botFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Imail_bot mail_bot){
        mail_botFeignClient.remove(mail_bot.getId()) ;
    }


    public void updateBatch(List<Imail_bot> mail_bots){
        if(mail_bots!=null){
            List<mail_botImpl> list = new ArrayList<mail_botImpl>();
            for(Imail_bot imail_bot :mail_bots){
                list.add((mail_botImpl)imail_bot) ;
            }
            mail_botFeignClient.updateBatch(list) ;
        }
    }


    public Page<Imail_bot> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_bot mail_bot){
        Imail_bot clientModel = mail_botFeignClient.getDraft(mail_bot.getId(),(mail_botImpl)mail_bot) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_bot.getClass(), false);
        copier.copy(clientModel, mail_bot, null);
    }



}

