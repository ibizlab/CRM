package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_activity;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_activityClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_activityImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_activityFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_activity] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_activityClientServiceImpl implements Imail_activityClientService {

    mail_activityFeignClient mail_activityFeignClient;

    @Autowired
    public mail_activityClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_activityFeignClient = nameBuilder.target(mail_activityFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_activityFeignClient = nameBuilder.target(mail_activityFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_activity createModel() {
		return new mail_activityImpl();
	}


    public void removeBatch(List<Imail_activity> mail_activities){
        if(mail_activities!=null){
            List<mail_activityImpl> list = new ArrayList<mail_activityImpl>();
            for(Imail_activity imail_activity :mail_activities){
                list.add((mail_activityImpl)imail_activity) ;
            }
            mail_activityFeignClient.removeBatch(list) ;
        }
    }


    public void get(Imail_activity mail_activity){
        Imail_activity clientModel = mail_activityFeignClient.get(mail_activity.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_activity.getClass(), false);
        copier.copy(clientModel, mail_activity, null);
    }


    public void remove(Imail_activity mail_activity){
        mail_activityFeignClient.remove(mail_activity.getId()) ;
    }


    public void createBatch(List<Imail_activity> mail_activities){
        if(mail_activities!=null){
            List<mail_activityImpl> list = new ArrayList<mail_activityImpl>();
            for(Imail_activity imail_activity :mail_activities){
                list.add((mail_activityImpl)imail_activity) ;
            }
            mail_activityFeignClient.createBatch(list) ;
        }
    }


    public void update(Imail_activity mail_activity){
        Imail_activity clientModel = mail_activityFeignClient.update(mail_activity.getId(),(mail_activityImpl)mail_activity) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_activity.getClass(), false);
        copier.copy(clientModel, mail_activity, null);
    }


    public void create(Imail_activity mail_activity){
        Imail_activity clientModel = mail_activityFeignClient.create((mail_activityImpl)mail_activity) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_activity.getClass(), false);
        copier.copy(clientModel, mail_activity, null);
    }


    public void updateBatch(List<Imail_activity> mail_activities){
        if(mail_activities!=null){
            List<mail_activityImpl> list = new ArrayList<mail_activityImpl>();
            for(Imail_activity imail_activity :mail_activities){
                list.add((mail_activityImpl)imail_activity) ;
            }
            mail_activityFeignClient.updateBatch(list) ;
        }
    }


    public Page<Imail_activity> fetchDefault(SearchContext context){
        Page<mail_activityImpl> page = this.mail_activityFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Imail_activity> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_activity mail_activity){
        Imail_activity clientModel = mail_activityFeignClient.getDraft(mail_activity.getId(),(mail_activityImpl)mail_activity) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_activity.getClass(), false);
        copier.copy(clientModel, mail_activity, null);
    }


    public void checkKey(Imail_activity mail_activity){
        Imail_activity clientModel = mail_activityFeignClient.checkKey(mail_activity.getId(),(mail_activityImpl)mail_activity) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_activity.getClass(), false);
        copier.copy(clientModel, mail_activity, null);
    }


    public void save(Imail_activity mail_activity){
        Imail_activity clientModel = mail_activityFeignClient.save(mail_activity.getId(),(mail_activityImpl)mail_activity) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_activity.getClass(), false);
        copier.copy(clientModel, mail_activity, null);
    }



}

