package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_channel_partner;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_channel_partnerImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_channel_partner] 服务对象接口
 */
public interface mail_channel_partnerFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_channel_partners/updatebatch")
    public mail_channel_partnerImpl updateBatch(@RequestBody List<mail_channel_partnerImpl> mail_channel_partners);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_channel_partners/removebatch")
    public mail_channel_partnerImpl removeBatch(@RequestBody List<mail_channel_partnerImpl> mail_channel_partners);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_channel_partners/createbatch")
    public mail_channel_partnerImpl createBatch(@RequestBody List<mail_channel_partnerImpl> mail_channel_partners);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_channel_partners")
    public mail_channel_partnerImpl create(@RequestBody mail_channel_partnerImpl mail_channel_partner);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_channel_partners/{id}")
    public mail_channel_partnerImpl update(@PathVariable("id") Integer id,@RequestBody mail_channel_partnerImpl mail_channel_partner);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_channel_partners/fetchdefault")
    public Page<mail_channel_partnerImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_channel_partners/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_channel_partners/{id}")
    public mail_channel_partnerImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_channel_partners/select")
    public Page<mail_channel_partnerImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_channel_partners/{id}/getdraft")
    public mail_channel_partnerImpl getDraft(@PathVariable("id") Integer id,@RequestBody mail_channel_partnerImpl mail_channel_partner);



}
