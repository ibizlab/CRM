package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_message_subtype;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_message_subtypeClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_message_subtypeImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_message_subtypeFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_message_subtype] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_message_subtypeClientServiceImpl implements Imail_message_subtypeClientService {

    mail_message_subtypeFeignClient mail_message_subtypeFeignClient;

    @Autowired
    public mail_message_subtypeClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_message_subtypeFeignClient = nameBuilder.target(mail_message_subtypeFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_message_subtypeFeignClient = nameBuilder.target(mail_message_subtypeFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_message_subtype createModel() {
		return new mail_message_subtypeImpl();
	}


    public void updateBatch(List<Imail_message_subtype> mail_message_subtypes){
        if(mail_message_subtypes!=null){
            List<mail_message_subtypeImpl> list = new ArrayList<mail_message_subtypeImpl>();
            for(Imail_message_subtype imail_message_subtype :mail_message_subtypes){
                list.add((mail_message_subtypeImpl)imail_message_subtype) ;
            }
            mail_message_subtypeFeignClient.updateBatch(list) ;
        }
    }


    public void get(Imail_message_subtype mail_message_subtype){
        Imail_message_subtype clientModel = mail_message_subtypeFeignClient.get(mail_message_subtype.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_message_subtype.getClass(), false);
        copier.copy(clientModel, mail_message_subtype, null);
    }


    public void update(Imail_message_subtype mail_message_subtype){
        Imail_message_subtype clientModel = mail_message_subtypeFeignClient.update(mail_message_subtype.getId(),(mail_message_subtypeImpl)mail_message_subtype) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_message_subtype.getClass(), false);
        copier.copy(clientModel, mail_message_subtype, null);
    }


    public void remove(Imail_message_subtype mail_message_subtype){
        mail_message_subtypeFeignClient.remove(mail_message_subtype.getId()) ;
    }


    public void removeBatch(List<Imail_message_subtype> mail_message_subtypes){
        if(mail_message_subtypes!=null){
            List<mail_message_subtypeImpl> list = new ArrayList<mail_message_subtypeImpl>();
            for(Imail_message_subtype imail_message_subtype :mail_message_subtypes){
                list.add((mail_message_subtypeImpl)imail_message_subtype) ;
            }
            mail_message_subtypeFeignClient.removeBatch(list) ;
        }
    }


    public void create(Imail_message_subtype mail_message_subtype){
        Imail_message_subtype clientModel = mail_message_subtypeFeignClient.create((mail_message_subtypeImpl)mail_message_subtype) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_message_subtype.getClass(), false);
        copier.copy(clientModel, mail_message_subtype, null);
    }


    public void createBatch(List<Imail_message_subtype> mail_message_subtypes){
        if(mail_message_subtypes!=null){
            List<mail_message_subtypeImpl> list = new ArrayList<mail_message_subtypeImpl>();
            for(Imail_message_subtype imail_message_subtype :mail_message_subtypes){
                list.add((mail_message_subtypeImpl)imail_message_subtype) ;
            }
            mail_message_subtypeFeignClient.createBatch(list) ;
        }
    }


    public Page<Imail_message_subtype> fetchDefault(SearchContext context){
        Page<mail_message_subtypeImpl> page = this.mail_message_subtypeFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Imail_message_subtype> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_message_subtype mail_message_subtype){
        Imail_message_subtype clientModel = mail_message_subtypeFeignClient.getDraft(mail_message_subtype.getId(),(mail_message_subtypeImpl)mail_message_subtype) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_message_subtype.getClass(), false);
        copier.copy(clientModel, mail_message_subtype, null);
    }



}

