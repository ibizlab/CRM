package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_blacklist;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_blacklistClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_blacklistImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_blacklistFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_blacklist] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_blacklistClientServiceImpl implements Imail_blacklistClientService {

    mail_blacklistFeignClient mail_blacklistFeignClient;

    @Autowired
    public mail_blacklistClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_blacklistFeignClient = nameBuilder.target(mail_blacklistFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_blacklistFeignClient = nameBuilder.target(mail_blacklistFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_blacklist createModel() {
		return new mail_blacklistImpl();
	}


    public Page<Imail_blacklist> fetchDefault(SearchContext context){
        Page<mail_blacklistImpl> page = this.mail_blacklistFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Imail_blacklist> mail_blacklists){
        if(mail_blacklists!=null){
            List<mail_blacklistImpl> list = new ArrayList<mail_blacklistImpl>();
            for(Imail_blacklist imail_blacklist :mail_blacklists){
                list.add((mail_blacklistImpl)imail_blacklist) ;
            }
            mail_blacklistFeignClient.createBatch(list) ;
        }
    }


    public void get(Imail_blacklist mail_blacklist){
        Imail_blacklist clientModel = mail_blacklistFeignClient.get(mail_blacklist.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_blacklist.getClass(), false);
        copier.copy(clientModel, mail_blacklist, null);
    }


    public void removeBatch(List<Imail_blacklist> mail_blacklists){
        if(mail_blacklists!=null){
            List<mail_blacklistImpl> list = new ArrayList<mail_blacklistImpl>();
            for(Imail_blacklist imail_blacklist :mail_blacklists){
                list.add((mail_blacklistImpl)imail_blacklist) ;
            }
            mail_blacklistFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Imail_blacklist mail_blacklist){
        mail_blacklistFeignClient.remove(mail_blacklist.getId()) ;
    }


    public void update(Imail_blacklist mail_blacklist){
        Imail_blacklist clientModel = mail_blacklistFeignClient.update(mail_blacklist.getId(),(mail_blacklistImpl)mail_blacklist) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_blacklist.getClass(), false);
        copier.copy(clientModel, mail_blacklist, null);
    }


    public void create(Imail_blacklist mail_blacklist){
        Imail_blacklist clientModel = mail_blacklistFeignClient.create((mail_blacklistImpl)mail_blacklist) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_blacklist.getClass(), false);
        copier.copy(clientModel, mail_blacklist, null);
    }


    public void updateBatch(List<Imail_blacklist> mail_blacklists){
        if(mail_blacklists!=null){
            List<mail_blacklistImpl> list = new ArrayList<mail_blacklistImpl>();
            for(Imail_blacklist imail_blacklist :mail_blacklists){
                list.add((mail_blacklistImpl)imail_blacklist) ;
            }
            mail_blacklistFeignClient.updateBatch(list) ;
        }
    }


    public Page<Imail_blacklist> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_blacklist mail_blacklist){
        Imail_blacklist clientModel = mail_blacklistFeignClient.getDraft(mail_blacklist.getId(),(mail_blacklistImpl)mail_blacklist) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_blacklist.getClass(), false);
        copier.copy(clientModel, mail_blacklist, null);
    }



}

