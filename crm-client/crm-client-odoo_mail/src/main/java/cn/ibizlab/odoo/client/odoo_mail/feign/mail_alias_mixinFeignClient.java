package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_alias_mixin;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_alias_mixinImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_alias_mixin] 服务对象接口
 */
public interface mail_alias_mixinFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_alias_mixins/updatebatch")
    public mail_alias_mixinImpl updateBatch(@RequestBody List<mail_alias_mixinImpl> mail_alias_mixins);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_alias_mixins/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_alias_mixins/fetchdefault")
    public Page<mail_alias_mixinImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_alias_mixins/removebatch")
    public mail_alias_mixinImpl removeBatch(@RequestBody List<mail_alias_mixinImpl> mail_alias_mixins);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_alias_mixins/{id}")
    public mail_alias_mixinImpl update(@PathVariable("id") Integer id,@RequestBody mail_alias_mixinImpl mail_alias_mixin);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_alias_mixins")
    public mail_alias_mixinImpl create(@RequestBody mail_alias_mixinImpl mail_alias_mixin);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_alias_mixins/{id}")
    public mail_alias_mixinImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_alias_mixins/createbatch")
    public mail_alias_mixinImpl createBatch(@RequestBody List<mail_alias_mixinImpl> mail_alias_mixins);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_alias_mixins/select")
    public Page<mail_alias_mixinImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_alias_mixins/{id}/getdraft")
    public mail_alias_mixinImpl getDraft(@PathVariable("id") Integer id,@RequestBody mail_alias_mixinImpl mail_alias_mixin);



}
