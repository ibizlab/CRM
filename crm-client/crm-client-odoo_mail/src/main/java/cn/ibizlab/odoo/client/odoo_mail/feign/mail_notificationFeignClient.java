package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_notification;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_notificationImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_notification] 服务对象接口
 */
public interface mail_notificationFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_notifications/createbatch")
    public mail_notificationImpl createBatch(@RequestBody List<mail_notificationImpl> mail_notifications);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_notifications/updatebatch")
    public mail_notificationImpl updateBatch(@RequestBody List<mail_notificationImpl> mail_notifications);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_notifications")
    public mail_notificationImpl create(@RequestBody mail_notificationImpl mail_notification);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_notifications/fetchdefault")
    public Page<mail_notificationImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_notifications/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_notifications/{id}")
    public mail_notificationImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_notifications/{id}")
    public mail_notificationImpl update(@PathVariable("id") Integer id,@RequestBody mail_notificationImpl mail_notification);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_notifications/removebatch")
    public mail_notificationImpl removeBatch(@RequestBody List<mail_notificationImpl> mail_notifications);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_notifications/select")
    public Page<mail_notificationImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_notifications/{id}/getdraft")
    public mail_notificationImpl getDraft(@PathVariable("id") Integer id,@RequestBody mail_notificationImpl mail_notification);



}
