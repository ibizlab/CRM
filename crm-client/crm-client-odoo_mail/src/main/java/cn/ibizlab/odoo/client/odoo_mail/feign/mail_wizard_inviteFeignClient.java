package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_wizard_invite;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_wizard_inviteImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_wizard_invite] 服务对象接口
 */
public interface mail_wizard_inviteFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_wizard_invites/fetchdefault")
    public Page<mail_wizard_inviteImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_wizard_invites/{id}")
    public mail_wizard_inviteImpl update(@PathVariable("id") Integer id,@RequestBody mail_wizard_inviteImpl mail_wizard_invite);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_wizard_invites/updatebatch")
    public mail_wizard_inviteImpl updateBatch(@RequestBody List<mail_wizard_inviteImpl> mail_wizard_invites);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_wizard_invites")
    public mail_wizard_inviteImpl create(@RequestBody mail_wizard_inviteImpl mail_wizard_invite);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_wizard_invites/removebatch")
    public mail_wizard_inviteImpl removeBatch(@RequestBody List<mail_wizard_inviteImpl> mail_wizard_invites);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_wizard_invites/{id}")
    public mail_wizard_inviteImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_wizard_invites/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_wizard_invites/createbatch")
    public mail_wizard_inviteImpl createBatch(@RequestBody List<mail_wizard_inviteImpl> mail_wizard_invites);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_wizard_invites/select")
    public Page<mail_wizard_inviteImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_wizard_invites/{id}/getdraft")
    public mail_wizard_inviteImpl getDraft(@PathVariable("id") Integer id,@RequestBody mail_wizard_inviteImpl mail_wizard_invite);



}
