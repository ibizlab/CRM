package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_blacklist_mixin;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_blacklist_mixinImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_blacklist_mixin] 服务对象接口
 */
public interface mail_blacklist_mixinFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_blacklist_mixins")
    public mail_blacklist_mixinImpl create(@RequestBody mail_blacklist_mixinImpl mail_blacklist_mixin);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_blacklist_mixins/fetchdefault")
    public Page<mail_blacklist_mixinImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_blacklist_mixins/createbatch")
    public mail_blacklist_mixinImpl createBatch(@RequestBody List<mail_blacklist_mixinImpl> mail_blacklist_mixins);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_blacklist_mixins/updatebatch")
    public mail_blacklist_mixinImpl updateBatch(@RequestBody List<mail_blacklist_mixinImpl> mail_blacklist_mixins);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_blacklist_mixins/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_blacklist_mixins/removebatch")
    public mail_blacklist_mixinImpl removeBatch(@RequestBody List<mail_blacklist_mixinImpl> mail_blacklist_mixins);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_blacklist_mixins/{id}")
    public mail_blacklist_mixinImpl update(@PathVariable("id") Integer id,@RequestBody mail_blacklist_mixinImpl mail_blacklist_mixin);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_blacklist_mixins/{id}")
    public mail_blacklist_mixinImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_blacklist_mixins/select")
    public Page<mail_blacklist_mixinImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_blacklist_mixins/{id}/getdraft")
    public mail_blacklist_mixinImpl getDraft(@PathVariable("id") Integer id,@RequestBody mail_blacklist_mixinImpl mail_blacklist_mixin);



}
