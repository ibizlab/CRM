package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_campaign;
import cn.ibizlab.odoo.client.odoo_mail.config.odoo_mailClientProperties;
import cn.ibizlab.odoo.core.client.service.Imail_mass_mailing_campaignClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mass_mailing_campaignImpl;
import cn.ibizlab.odoo.client.odoo_mail.feign.mail_mass_mailing_campaignFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mail_mass_mailing_campaign] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mail_mass_mailing_campaignClientServiceImpl implements Imail_mass_mailing_campaignClientService {

    mail_mass_mailing_campaignFeignClient mail_mass_mailing_campaignFeignClient;

    @Autowired
    public mail_mass_mailing_campaignClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mailClientProperties odoo_mailClientProperties) {
        if (odoo_mailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_mass_mailing_campaignFeignClient = nameBuilder.target(mail_mass_mailing_campaignFeignClient.class,"http://"+odoo_mailClientProperties.getServiceId()+"/") ;
		}else if (odoo_mailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mail_mass_mailing_campaignFeignClient = nameBuilder.target(mail_mass_mailing_campaignFeignClient.class,odoo_mailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imail_mass_mailing_campaign createModel() {
		return new mail_mass_mailing_campaignImpl();
	}


    public void update(Imail_mass_mailing_campaign mail_mass_mailing_campaign){
        Imail_mass_mailing_campaign clientModel = mail_mass_mailing_campaignFeignClient.update(mail_mass_mailing_campaign.getId(),(mail_mass_mailing_campaignImpl)mail_mass_mailing_campaign) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing_campaign.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing_campaign, null);
    }


    public void remove(Imail_mass_mailing_campaign mail_mass_mailing_campaign){
        mail_mass_mailing_campaignFeignClient.remove(mail_mass_mailing_campaign.getId()) ;
    }


    public Page<Imail_mass_mailing_campaign> fetchDefault(SearchContext context){
        Page<mail_mass_mailing_campaignImpl> page = this.mail_mass_mailing_campaignFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Imail_mass_mailing_campaign> mail_mass_mailing_campaigns){
        if(mail_mass_mailing_campaigns!=null){
            List<mail_mass_mailing_campaignImpl> list = new ArrayList<mail_mass_mailing_campaignImpl>();
            for(Imail_mass_mailing_campaign imail_mass_mailing_campaign :mail_mass_mailing_campaigns){
                list.add((mail_mass_mailing_campaignImpl)imail_mass_mailing_campaign) ;
            }
            mail_mass_mailing_campaignFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Imail_mass_mailing_campaign> mail_mass_mailing_campaigns){
        if(mail_mass_mailing_campaigns!=null){
            List<mail_mass_mailing_campaignImpl> list = new ArrayList<mail_mass_mailing_campaignImpl>();
            for(Imail_mass_mailing_campaign imail_mass_mailing_campaign :mail_mass_mailing_campaigns){
                list.add((mail_mass_mailing_campaignImpl)imail_mass_mailing_campaign) ;
            }
            mail_mass_mailing_campaignFeignClient.updateBatch(list) ;
        }
    }


    public void get(Imail_mass_mailing_campaign mail_mass_mailing_campaign){
        Imail_mass_mailing_campaign clientModel = mail_mass_mailing_campaignFeignClient.get(mail_mass_mailing_campaign.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing_campaign.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing_campaign, null);
    }


    public void create(Imail_mass_mailing_campaign mail_mass_mailing_campaign){
        Imail_mass_mailing_campaign clientModel = mail_mass_mailing_campaignFeignClient.create((mail_mass_mailing_campaignImpl)mail_mass_mailing_campaign) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing_campaign.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing_campaign, null);
    }


    public void removeBatch(List<Imail_mass_mailing_campaign> mail_mass_mailing_campaigns){
        if(mail_mass_mailing_campaigns!=null){
            List<mail_mass_mailing_campaignImpl> list = new ArrayList<mail_mass_mailing_campaignImpl>();
            for(Imail_mass_mailing_campaign imail_mass_mailing_campaign :mail_mass_mailing_campaigns){
                list.add((mail_mass_mailing_campaignImpl)imail_mass_mailing_campaign) ;
            }
            mail_mass_mailing_campaignFeignClient.removeBatch(list) ;
        }
    }


    public Page<Imail_mass_mailing_campaign> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imail_mass_mailing_campaign mail_mass_mailing_campaign){
        Imail_mass_mailing_campaign clientModel = mail_mass_mailing_campaignFeignClient.getDraft(mail_mass_mailing_campaign.getId(),(mail_mass_mailing_campaignImpl)mail_mass_mailing_campaign) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mail_mass_mailing_campaign.getClass(), false);
        copier.copy(clientModel, mail_mass_mailing_campaign, null);
    }



}

