package cn.ibizlab.odoo.client.odoo_payment.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ipayment_transaction;
import cn.ibizlab.odoo.client.odoo_payment.config.odoo_paymentClientProperties;
import cn.ibizlab.odoo.core.client.service.Ipayment_transactionClientService;
import cn.ibizlab.odoo.client.odoo_payment.model.payment_transactionImpl;
import cn.ibizlab.odoo.client.odoo_payment.feign.payment_transactionFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[payment_transaction] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class payment_transactionClientServiceImpl implements Ipayment_transactionClientService {

    payment_transactionFeignClient payment_transactionFeignClient;

    @Autowired
    public payment_transactionClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_paymentClientProperties odoo_paymentClientProperties) {
        if (odoo_paymentClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.payment_transactionFeignClient = nameBuilder.target(payment_transactionFeignClient.class,"http://"+odoo_paymentClientProperties.getServiceId()+"/") ;
		}else if (odoo_paymentClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.payment_transactionFeignClient = nameBuilder.target(payment_transactionFeignClient.class,odoo_paymentClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ipayment_transaction createModel() {
		return new payment_transactionImpl();
	}


    public void removeBatch(List<Ipayment_transaction> payment_transactions){
        if(payment_transactions!=null){
            List<payment_transactionImpl> list = new ArrayList<payment_transactionImpl>();
            for(Ipayment_transaction ipayment_transaction :payment_transactions){
                list.add((payment_transactionImpl)ipayment_transaction) ;
            }
            payment_transactionFeignClient.removeBatch(list) ;
        }
    }


    public void create(Ipayment_transaction payment_transaction){
        Ipayment_transaction clientModel = payment_transactionFeignClient.create((payment_transactionImpl)payment_transaction) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), payment_transaction.getClass(), false);
        copier.copy(clientModel, payment_transaction, null);
    }


    public Page<Ipayment_transaction> fetchDefault(SearchContext context){
        Page<payment_transactionImpl> page = this.payment_transactionFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Ipayment_transaction> payment_transactions){
        if(payment_transactions!=null){
            List<payment_transactionImpl> list = new ArrayList<payment_transactionImpl>();
            for(Ipayment_transaction ipayment_transaction :payment_transactions){
                list.add((payment_transactionImpl)ipayment_transaction) ;
            }
            payment_transactionFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Ipayment_transaction> payment_transactions){
        if(payment_transactions!=null){
            List<payment_transactionImpl> list = new ArrayList<payment_transactionImpl>();
            for(Ipayment_transaction ipayment_transaction :payment_transactions){
                list.add((payment_transactionImpl)ipayment_transaction) ;
            }
            payment_transactionFeignClient.updateBatch(list) ;
        }
    }


    public void get(Ipayment_transaction payment_transaction){
        Ipayment_transaction clientModel = payment_transactionFeignClient.get(payment_transaction.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), payment_transaction.getClass(), false);
        copier.copy(clientModel, payment_transaction, null);
    }


    public void remove(Ipayment_transaction payment_transaction){
        payment_transactionFeignClient.remove(payment_transaction.getId()) ;
    }


    public void update(Ipayment_transaction payment_transaction){
        Ipayment_transaction clientModel = payment_transactionFeignClient.update(payment_transaction.getId(),(payment_transactionImpl)payment_transaction) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), payment_transaction.getClass(), false);
        copier.copy(clientModel, payment_transaction, null);
    }


    public Page<Ipayment_transaction> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ipayment_transaction payment_transaction){
        Ipayment_transaction clientModel = payment_transactionFeignClient.getDraft(payment_transaction.getId(),(payment_transactionImpl)payment_transaction) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), payment_transaction.getClass(), false);
        copier.copy(clientModel, payment_transaction, null);
    }



}

