package cn.ibizlab.odoo.client.odoo_payment.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ipayment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.client.odoo_payment.config.odoo_paymentClientProperties;
import cn.ibizlab.odoo.core.client.service.Ipayment_acquirer_onboarding_wizardClientService;
import cn.ibizlab.odoo.client.odoo_payment.model.payment_acquirer_onboarding_wizardImpl;
import cn.ibizlab.odoo.client.odoo_payment.feign.payment_acquirer_onboarding_wizardFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[payment_acquirer_onboarding_wizard] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class payment_acquirer_onboarding_wizardClientServiceImpl implements Ipayment_acquirer_onboarding_wizardClientService {

    payment_acquirer_onboarding_wizardFeignClient payment_acquirer_onboarding_wizardFeignClient;

    @Autowired
    public payment_acquirer_onboarding_wizardClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_paymentClientProperties odoo_paymentClientProperties) {
        if (odoo_paymentClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.payment_acquirer_onboarding_wizardFeignClient = nameBuilder.target(payment_acquirer_onboarding_wizardFeignClient.class,"http://"+odoo_paymentClientProperties.getServiceId()+"/") ;
		}else if (odoo_paymentClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.payment_acquirer_onboarding_wizardFeignClient = nameBuilder.target(payment_acquirer_onboarding_wizardFeignClient.class,odoo_paymentClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ipayment_acquirer_onboarding_wizard createModel() {
		return new payment_acquirer_onboarding_wizardImpl();
	}


    public void updateBatch(List<Ipayment_acquirer_onboarding_wizard> payment_acquirer_onboarding_wizards){
        if(payment_acquirer_onboarding_wizards!=null){
            List<payment_acquirer_onboarding_wizardImpl> list = new ArrayList<payment_acquirer_onboarding_wizardImpl>();
            for(Ipayment_acquirer_onboarding_wizard ipayment_acquirer_onboarding_wizard :payment_acquirer_onboarding_wizards){
                list.add((payment_acquirer_onboarding_wizardImpl)ipayment_acquirer_onboarding_wizard) ;
            }
            payment_acquirer_onboarding_wizardFeignClient.updateBatch(list) ;
        }
    }


    public void create(Ipayment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard){
        Ipayment_acquirer_onboarding_wizard clientModel = payment_acquirer_onboarding_wizardFeignClient.create((payment_acquirer_onboarding_wizardImpl)payment_acquirer_onboarding_wizard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), payment_acquirer_onboarding_wizard.getClass(), false);
        copier.copy(clientModel, payment_acquirer_onboarding_wizard, null);
    }


    public Page<Ipayment_acquirer_onboarding_wizard> fetchDefault(SearchContext context){
        Page<payment_acquirer_onboarding_wizardImpl> page = this.payment_acquirer_onboarding_wizardFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Ipayment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard){
        Ipayment_acquirer_onboarding_wizard clientModel = payment_acquirer_onboarding_wizardFeignClient.update(payment_acquirer_onboarding_wizard.getId(),(payment_acquirer_onboarding_wizardImpl)payment_acquirer_onboarding_wizard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), payment_acquirer_onboarding_wizard.getClass(), false);
        copier.copy(clientModel, payment_acquirer_onboarding_wizard, null);
    }


    public void createBatch(List<Ipayment_acquirer_onboarding_wizard> payment_acquirer_onboarding_wizards){
        if(payment_acquirer_onboarding_wizards!=null){
            List<payment_acquirer_onboarding_wizardImpl> list = new ArrayList<payment_acquirer_onboarding_wizardImpl>();
            for(Ipayment_acquirer_onboarding_wizard ipayment_acquirer_onboarding_wizard :payment_acquirer_onboarding_wizards){
                list.add((payment_acquirer_onboarding_wizardImpl)ipayment_acquirer_onboarding_wizard) ;
            }
            payment_acquirer_onboarding_wizardFeignClient.createBatch(list) ;
        }
    }


    public void get(Ipayment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard){
        Ipayment_acquirer_onboarding_wizard clientModel = payment_acquirer_onboarding_wizardFeignClient.get(payment_acquirer_onboarding_wizard.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), payment_acquirer_onboarding_wizard.getClass(), false);
        copier.copy(clientModel, payment_acquirer_onboarding_wizard, null);
    }


    public void remove(Ipayment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard){
        payment_acquirer_onboarding_wizardFeignClient.remove(payment_acquirer_onboarding_wizard.getId()) ;
    }


    public void removeBatch(List<Ipayment_acquirer_onboarding_wizard> payment_acquirer_onboarding_wizards){
        if(payment_acquirer_onboarding_wizards!=null){
            List<payment_acquirer_onboarding_wizardImpl> list = new ArrayList<payment_acquirer_onboarding_wizardImpl>();
            for(Ipayment_acquirer_onboarding_wizard ipayment_acquirer_onboarding_wizard :payment_acquirer_onboarding_wizards){
                list.add((payment_acquirer_onboarding_wizardImpl)ipayment_acquirer_onboarding_wizard) ;
            }
            payment_acquirer_onboarding_wizardFeignClient.removeBatch(list) ;
        }
    }


    public Page<Ipayment_acquirer_onboarding_wizard> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ipayment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard){
        Ipayment_acquirer_onboarding_wizard clientModel = payment_acquirer_onboarding_wizardFeignClient.getDraft(payment_acquirer_onboarding_wizard.getId(),(payment_acquirer_onboarding_wizardImpl)payment_acquirer_onboarding_wizard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), payment_acquirer_onboarding_wizard.getClass(), false);
        copier.copy(clientModel, payment_acquirer_onboarding_wizard, null);
    }



}

