package cn.ibizlab.odoo.client.odoo_payment.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ipayment_acquirer;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[payment_acquirer] 对象
 */
public class payment_acquirerImpl implements Ipayment_acquirer,Serializable{

    /**
     * 批准支持的机制
     */
    public String authorize_implemented;

    @JsonIgnore
    public boolean authorize_implementedDirtyFlag;
    
    /**
     * 取消消息
     */
    public String cancel_msg;

    @JsonIgnore
    public boolean cancel_msgDirtyFlag;
    
    /**
     * 手动获取金额
     */
    public String capture_manually;

    @JsonIgnore
    public boolean capture_manuallyDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 国家
     */
    public String country_ids;

    @JsonIgnore
    public boolean country_idsDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 说明
     */
    public String description;

    @JsonIgnore
    public boolean descriptionDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 完成的信息
     */
    public String done_msg;

    @JsonIgnore
    public boolean done_msgDirtyFlag;
    
    /**
     * 环境
     */
    public String environment;

    @JsonIgnore
    public boolean environmentDirtyFlag;
    
    /**
     * 错误消息
     */
    public String error_msg;

    @JsonIgnore
    public boolean error_msgDirtyFlag;
    
    /**
     * 添加额外的费用
     */
    public String fees_active;

    @JsonIgnore
    public boolean fees_activeDirtyFlag;
    
    /**
     * 国内固定费用
     */
    public Double fees_dom_fixed;

    @JsonIgnore
    public boolean fees_dom_fixedDirtyFlag;
    
    /**
     * 动态内部费用(百分比)
     */
    public Double fees_dom_var;

    @JsonIgnore
    public boolean fees_dom_varDirtyFlag;
    
    /**
     * 支持费用计算
     */
    public String fees_implemented;

    @JsonIgnore
    public boolean fees_implementedDirtyFlag;
    
    /**
     * 固定的手续费
     */
    public Double fees_int_fixed;

    @JsonIgnore
    public boolean fees_int_fixedDirtyFlag;
    
    /**
     * 可变的交易费用（百分比）
     */
    public Double fees_int_var;

    @JsonIgnore
    public boolean fees_int_varDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 图像
     */
    public byte[] image;

    @JsonIgnore
    public boolean imageDirtyFlag;
    
    /**
     * 中等尺寸图像
     */
    public byte[] image_medium;

    @JsonIgnore
    public boolean image_mediumDirtyFlag;
    
    /**
     * 小尺寸图像
     */
    public byte[] image_small;

    @JsonIgnore
    public boolean image_smallDirtyFlag;
    
    /**
     * 未收款
     */
    public String inbound_payment_method_ids;

    @JsonIgnore
    public boolean inbound_payment_method_idsDirtyFlag;
    
    /**
     * 付款日记账
     */
    public Integer journal_id;

    @JsonIgnore
    public boolean journal_idDirtyFlag;
    
    /**
     * 付款日记账
     */
    public String journal_id_text;

    @JsonIgnore
    public boolean journal_id_textDirtyFlag;
    
    /**
     * 对应模块
     */
    public Integer module_id;

    @JsonIgnore
    public boolean module_idDirtyFlag;
    
    /**
     * 安装状态
     */
    public String module_state;

    @JsonIgnore
    public boolean module_stateDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 立即支付
     */
    public String payment_flow;

    @JsonIgnore
    public boolean payment_flowDirtyFlag;
    
    /**
     * 支持的支付图标
     */
    public String payment_icon_ids;

    @JsonIgnore
    public boolean payment_icon_idsDirtyFlag;
    
    /**
     * 待定消息
     */
    public String pending_msg;

    @JsonIgnore
    public boolean pending_msgDirtyFlag;
    
    /**
     * 感谢留言
     */
    public String post_msg;

    @JsonIgnore
    public boolean post_msgDirtyFlag;
    
    /**
     * 帮助信息
     */
    public String pre_msg;

    @JsonIgnore
    public boolean pre_msgDirtyFlag;
    
    /**
     * 服务商
     */
    public String provider;

    @JsonIgnore
    public boolean providerDirtyFlag;
    
    /**
     * 使用SEPA QR 二维码
     */
    public String qr_code;

    @JsonIgnore
    public boolean qr_codeDirtyFlag;
    
    /**
     * S2S表单模板
     */
    public Integer registration_view_template_id;

    @JsonIgnore
    public boolean registration_view_template_idDirtyFlag;
    
    /**
     * 保存卡
     */
    public String save_token;

    @JsonIgnore
    public boolean save_tokenDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 交流
     */
    public String so_reference_type;

    @JsonIgnore
    public boolean so_reference_typeDirtyFlag;
    
    /**
     * 特定国家/地区
     */
    public String specific_countries;

    @JsonIgnore
    public boolean specific_countriesDirtyFlag;
    
    /**
     * 支持保存卡片资料
     */
    public String token_implemented;

    @JsonIgnore
    public boolean token_implementedDirtyFlag;
    
    /**
     * 窗体按钮模板
     */
    public Integer view_template_id;

    @JsonIgnore
    public boolean view_template_idDirtyFlag;
    
    /**
     * 网站
     */
    public Integer website_id;

    @JsonIgnore
    public boolean website_idDirtyFlag;
    
    /**
     * 在门户/网站可见
     */
    public String website_published;

    @JsonIgnore
    public boolean website_publishedDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [批准支持的机制]
     */
    @JsonProperty("authorize_implemented")
    public String getAuthorize_implemented(){
        return this.authorize_implemented ;
    }

    /**
     * 设置 [批准支持的机制]
     */
    @JsonProperty("authorize_implemented")
    public void setAuthorize_implemented(String  authorize_implemented){
        this.authorize_implemented = authorize_implemented ;
        this.authorize_implementedDirtyFlag = true ;
    }

     /**
     * 获取 [批准支持的机制]脏标记
     */
    @JsonIgnore
    public boolean getAuthorize_implementedDirtyFlag(){
        return this.authorize_implementedDirtyFlag ;
    }   

    /**
     * 获取 [取消消息]
     */
    @JsonProperty("cancel_msg")
    public String getCancel_msg(){
        return this.cancel_msg ;
    }

    /**
     * 设置 [取消消息]
     */
    @JsonProperty("cancel_msg")
    public void setCancel_msg(String  cancel_msg){
        this.cancel_msg = cancel_msg ;
        this.cancel_msgDirtyFlag = true ;
    }

     /**
     * 获取 [取消消息]脏标记
     */
    @JsonIgnore
    public boolean getCancel_msgDirtyFlag(){
        return this.cancel_msgDirtyFlag ;
    }   

    /**
     * 获取 [手动获取金额]
     */
    @JsonProperty("capture_manually")
    public String getCapture_manually(){
        return this.capture_manually ;
    }

    /**
     * 设置 [手动获取金额]
     */
    @JsonProperty("capture_manually")
    public void setCapture_manually(String  capture_manually){
        this.capture_manually = capture_manually ;
        this.capture_manuallyDirtyFlag = true ;
    }

     /**
     * 获取 [手动获取金额]脏标记
     */
    @JsonIgnore
    public boolean getCapture_manuallyDirtyFlag(){
        return this.capture_manuallyDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [国家]
     */
    @JsonProperty("country_ids")
    public String getCountry_ids(){
        return this.country_ids ;
    }

    /**
     * 设置 [国家]
     */
    @JsonProperty("country_ids")
    public void setCountry_ids(String  country_ids){
        this.country_ids = country_ids ;
        this.country_idsDirtyFlag = true ;
    }

     /**
     * 获取 [国家]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idsDirtyFlag(){
        return this.country_idsDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [说明]
     */
    @JsonProperty("description")
    public String getDescription(){
        return this.description ;
    }

    /**
     * 设置 [说明]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [说明]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return this.descriptionDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [完成的信息]
     */
    @JsonProperty("done_msg")
    public String getDone_msg(){
        return this.done_msg ;
    }

    /**
     * 设置 [完成的信息]
     */
    @JsonProperty("done_msg")
    public void setDone_msg(String  done_msg){
        this.done_msg = done_msg ;
        this.done_msgDirtyFlag = true ;
    }

     /**
     * 获取 [完成的信息]脏标记
     */
    @JsonIgnore
    public boolean getDone_msgDirtyFlag(){
        return this.done_msgDirtyFlag ;
    }   

    /**
     * 获取 [环境]
     */
    @JsonProperty("environment")
    public String getEnvironment(){
        return this.environment ;
    }

    /**
     * 设置 [环境]
     */
    @JsonProperty("environment")
    public void setEnvironment(String  environment){
        this.environment = environment ;
        this.environmentDirtyFlag = true ;
    }

     /**
     * 获取 [环境]脏标记
     */
    @JsonIgnore
    public boolean getEnvironmentDirtyFlag(){
        return this.environmentDirtyFlag ;
    }   

    /**
     * 获取 [错误消息]
     */
    @JsonProperty("error_msg")
    public String getError_msg(){
        return this.error_msg ;
    }

    /**
     * 设置 [错误消息]
     */
    @JsonProperty("error_msg")
    public void setError_msg(String  error_msg){
        this.error_msg = error_msg ;
        this.error_msgDirtyFlag = true ;
    }

     /**
     * 获取 [错误消息]脏标记
     */
    @JsonIgnore
    public boolean getError_msgDirtyFlag(){
        return this.error_msgDirtyFlag ;
    }   

    /**
     * 获取 [添加额外的费用]
     */
    @JsonProperty("fees_active")
    public String getFees_active(){
        return this.fees_active ;
    }

    /**
     * 设置 [添加额外的费用]
     */
    @JsonProperty("fees_active")
    public void setFees_active(String  fees_active){
        this.fees_active = fees_active ;
        this.fees_activeDirtyFlag = true ;
    }

     /**
     * 获取 [添加额外的费用]脏标记
     */
    @JsonIgnore
    public boolean getFees_activeDirtyFlag(){
        return this.fees_activeDirtyFlag ;
    }   

    /**
     * 获取 [国内固定费用]
     */
    @JsonProperty("fees_dom_fixed")
    public Double getFees_dom_fixed(){
        return this.fees_dom_fixed ;
    }

    /**
     * 设置 [国内固定费用]
     */
    @JsonProperty("fees_dom_fixed")
    public void setFees_dom_fixed(Double  fees_dom_fixed){
        this.fees_dom_fixed = fees_dom_fixed ;
        this.fees_dom_fixedDirtyFlag = true ;
    }

     /**
     * 获取 [国内固定费用]脏标记
     */
    @JsonIgnore
    public boolean getFees_dom_fixedDirtyFlag(){
        return this.fees_dom_fixedDirtyFlag ;
    }   

    /**
     * 获取 [动态内部费用(百分比)]
     */
    @JsonProperty("fees_dom_var")
    public Double getFees_dom_var(){
        return this.fees_dom_var ;
    }

    /**
     * 设置 [动态内部费用(百分比)]
     */
    @JsonProperty("fees_dom_var")
    public void setFees_dom_var(Double  fees_dom_var){
        this.fees_dom_var = fees_dom_var ;
        this.fees_dom_varDirtyFlag = true ;
    }

     /**
     * 获取 [动态内部费用(百分比)]脏标记
     */
    @JsonIgnore
    public boolean getFees_dom_varDirtyFlag(){
        return this.fees_dom_varDirtyFlag ;
    }   

    /**
     * 获取 [支持费用计算]
     */
    @JsonProperty("fees_implemented")
    public String getFees_implemented(){
        return this.fees_implemented ;
    }

    /**
     * 设置 [支持费用计算]
     */
    @JsonProperty("fees_implemented")
    public void setFees_implemented(String  fees_implemented){
        this.fees_implemented = fees_implemented ;
        this.fees_implementedDirtyFlag = true ;
    }

     /**
     * 获取 [支持费用计算]脏标记
     */
    @JsonIgnore
    public boolean getFees_implementedDirtyFlag(){
        return this.fees_implementedDirtyFlag ;
    }   

    /**
     * 获取 [固定的手续费]
     */
    @JsonProperty("fees_int_fixed")
    public Double getFees_int_fixed(){
        return this.fees_int_fixed ;
    }

    /**
     * 设置 [固定的手续费]
     */
    @JsonProperty("fees_int_fixed")
    public void setFees_int_fixed(Double  fees_int_fixed){
        this.fees_int_fixed = fees_int_fixed ;
        this.fees_int_fixedDirtyFlag = true ;
    }

     /**
     * 获取 [固定的手续费]脏标记
     */
    @JsonIgnore
    public boolean getFees_int_fixedDirtyFlag(){
        return this.fees_int_fixedDirtyFlag ;
    }   

    /**
     * 获取 [可变的交易费用（百分比）]
     */
    @JsonProperty("fees_int_var")
    public Double getFees_int_var(){
        return this.fees_int_var ;
    }

    /**
     * 设置 [可变的交易费用（百分比）]
     */
    @JsonProperty("fees_int_var")
    public void setFees_int_var(Double  fees_int_var){
        this.fees_int_var = fees_int_var ;
        this.fees_int_varDirtyFlag = true ;
    }

     /**
     * 获取 [可变的交易费用（百分比）]脏标记
     */
    @JsonIgnore
    public boolean getFees_int_varDirtyFlag(){
        return this.fees_int_varDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [图像]
     */
    @JsonProperty("image")
    public byte[] getImage(){
        return this.image ;
    }

    /**
     * 设置 [图像]
     */
    @JsonProperty("image")
    public void setImage(byte[]  image){
        this.image = image ;
        this.imageDirtyFlag = true ;
    }

     /**
     * 获取 [图像]脏标记
     */
    @JsonIgnore
    public boolean getImageDirtyFlag(){
        return this.imageDirtyFlag ;
    }   

    /**
     * 获取 [中等尺寸图像]
     */
    @JsonProperty("image_medium")
    public byte[] getImage_medium(){
        return this.image_medium ;
    }

    /**
     * 设置 [中等尺寸图像]
     */
    @JsonProperty("image_medium")
    public void setImage_medium(byte[]  image_medium){
        this.image_medium = image_medium ;
        this.image_mediumDirtyFlag = true ;
    }

     /**
     * 获取 [中等尺寸图像]脏标记
     */
    @JsonIgnore
    public boolean getImage_mediumDirtyFlag(){
        return this.image_mediumDirtyFlag ;
    }   

    /**
     * 获取 [小尺寸图像]
     */
    @JsonProperty("image_small")
    public byte[] getImage_small(){
        return this.image_small ;
    }

    /**
     * 设置 [小尺寸图像]
     */
    @JsonProperty("image_small")
    public void setImage_small(byte[]  image_small){
        this.image_small = image_small ;
        this.image_smallDirtyFlag = true ;
    }

     /**
     * 获取 [小尺寸图像]脏标记
     */
    @JsonIgnore
    public boolean getImage_smallDirtyFlag(){
        return this.image_smallDirtyFlag ;
    }   

    /**
     * 获取 [未收款]
     */
    @JsonProperty("inbound_payment_method_ids")
    public String getInbound_payment_method_ids(){
        return this.inbound_payment_method_ids ;
    }

    /**
     * 设置 [未收款]
     */
    @JsonProperty("inbound_payment_method_ids")
    public void setInbound_payment_method_ids(String  inbound_payment_method_ids){
        this.inbound_payment_method_ids = inbound_payment_method_ids ;
        this.inbound_payment_method_idsDirtyFlag = true ;
    }

     /**
     * 获取 [未收款]脏标记
     */
    @JsonIgnore
    public boolean getInbound_payment_method_idsDirtyFlag(){
        return this.inbound_payment_method_idsDirtyFlag ;
    }   

    /**
     * 获取 [付款日记账]
     */
    @JsonProperty("journal_id")
    public Integer getJournal_id(){
        return this.journal_id ;
    }

    /**
     * 设置 [付款日记账]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(Integer  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

     /**
     * 获取 [付款日记账]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return this.journal_idDirtyFlag ;
    }   

    /**
     * 获取 [付款日记账]
     */
    @JsonProperty("journal_id_text")
    public String getJournal_id_text(){
        return this.journal_id_text ;
    }

    /**
     * 设置 [付款日记账]
     */
    @JsonProperty("journal_id_text")
    public void setJournal_id_text(String  journal_id_text){
        this.journal_id_text = journal_id_text ;
        this.journal_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [付款日记账]脏标记
     */
    @JsonIgnore
    public boolean getJournal_id_textDirtyFlag(){
        return this.journal_id_textDirtyFlag ;
    }   

    /**
     * 获取 [对应模块]
     */
    @JsonProperty("module_id")
    public Integer getModule_id(){
        return this.module_id ;
    }

    /**
     * 设置 [对应模块]
     */
    @JsonProperty("module_id")
    public void setModule_id(Integer  module_id){
        this.module_id = module_id ;
        this.module_idDirtyFlag = true ;
    }

     /**
     * 获取 [对应模块]脏标记
     */
    @JsonIgnore
    public boolean getModule_idDirtyFlag(){
        return this.module_idDirtyFlag ;
    }   

    /**
     * 获取 [安装状态]
     */
    @JsonProperty("module_state")
    public String getModule_state(){
        return this.module_state ;
    }

    /**
     * 设置 [安装状态]
     */
    @JsonProperty("module_state")
    public void setModule_state(String  module_state){
        this.module_state = module_state ;
        this.module_stateDirtyFlag = true ;
    }

     /**
     * 获取 [安装状态]脏标记
     */
    @JsonIgnore
    public boolean getModule_stateDirtyFlag(){
        return this.module_stateDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [立即支付]
     */
    @JsonProperty("payment_flow")
    public String getPayment_flow(){
        return this.payment_flow ;
    }

    /**
     * 设置 [立即支付]
     */
    @JsonProperty("payment_flow")
    public void setPayment_flow(String  payment_flow){
        this.payment_flow = payment_flow ;
        this.payment_flowDirtyFlag = true ;
    }

     /**
     * 获取 [立即支付]脏标记
     */
    @JsonIgnore
    public boolean getPayment_flowDirtyFlag(){
        return this.payment_flowDirtyFlag ;
    }   

    /**
     * 获取 [支持的支付图标]
     */
    @JsonProperty("payment_icon_ids")
    public String getPayment_icon_ids(){
        return this.payment_icon_ids ;
    }

    /**
     * 设置 [支持的支付图标]
     */
    @JsonProperty("payment_icon_ids")
    public void setPayment_icon_ids(String  payment_icon_ids){
        this.payment_icon_ids = payment_icon_ids ;
        this.payment_icon_idsDirtyFlag = true ;
    }

     /**
     * 获取 [支持的支付图标]脏标记
     */
    @JsonIgnore
    public boolean getPayment_icon_idsDirtyFlag(){
        return this.payment_icon_idsDirtyFlag ;
    }   

    /**
     * 获取 [待定消息]
     */
    @JsonProperty("pending_msg")
    public String getPending_msg(){
        return this.pending_msg ;
    }

    /**
     * 设置 [待定消息]
     */
    @JsonProperty("pending_msg")
    public void setPending_msg(String  pending_msg){
        this.pending_msg = pending_msg ;
        this.pending_msgDirtyFlag = true ;
    }

     /**
     * 获取 [待定消息]脏标记
     */
    @JsonIgnore
    public boolean getPending_msgDirtyFlag(){
        return this.pending_msgDirtyFlag ;
    }   

    /**
     * 获取 [感谢留言]
     */
    @JsonProperty("post_msg")
    public String getPost_msg(){
        return this.post_msg ;
    }

    /**
     * 设置 [感谢留言]
     */
    @JsonProperty("post_msg")
    public void setPost_msg(String  post_msg){
        this.post_msg = post_msg ;
        this.post_msgDirtyFlag = true ;
    }

     /**
     * 获取 [感谢留言]脏标记
     */
    @JsonIgnore
    public boolean getPost_msgDirtyFlag(){
        return this.post_msgDirtyFlag ;
    }   

    /**
     * 获取 [帮助信息]
     */
    @JsonProperty("pre_msg")
    public String getPre_msg(){
        return this.pre_msg ;
    }

    /**
     * 设置 [帮助信息]
     */
    @JsonProperty("pre_msg")
    public void setPre_msg(String  pre_msg){
        this.pre_msg = pre_msg ;
        this.pre_msgDirtyFlag = true ;
    }

     /**
     * 获取 [帮助信息]脏标记
     */
    @JsonIgnore
    public boolean getPre_msgDirtyFlag(){
        return this.pre_msgDirtyFlag ;
    }   

    /**
     * 获取 [服务商]
     */
    @JsonProperty("provider")
    public String getProvider(){
        return this.provider ;
    }

    /**
     * 设置 [服务商]
     */
    @JsonProperty("provider")
    public void setProvider(String  provider){
        this.provider = provider ;
        this.providerDirtyFlag = true ;
    }

     /**
     * 获取 [服务商]脏标记
     */
    @JsonIgnore
    public boolean getProviderDirtyFlag(){
        return this.providerDirtyFlag ;
    }   

    /**
     * 获取 [使用SEPA QR 二维码]
     */
    @JsonProperty("qr_code")
    public String getQr_code(){
        return this.qr_code ;
    }

    /**
     * 设置 [使用SEPA QR 二维码]
     */
    @JsonProperty("qr_code")
    public void setQr_code(String  qr_code){
        this.qr_code = qr_code ;
        this.qr_codeDirtyFlag = true ;
    }

     /**
     * 获取 [使用SEPA QR 二维码]脏标记
     */
    @JsonIgnore
    public boolean getQr_codeDirtyFlag(){
        return this.qr_codeDirtyFlag ;
    }   

    /**
     * 获取 [S2S表单模板]
     */
    @JsonProperty("registration_view_template_id")
    public Integer getRegistration_view_template_id(){
        return this.registration_view_template_id ;
    }

    /**
     * 设置 [S2S表单模板]
     */
    @JsonProperty("registration_view_template_id")
    public void setRegistration_view_template_id(Integer  registration_view_template_id){
        this.registration_view_template_id = registration_view_template_id ;
        this.registration_view_template_idDirtyFlag = true ;
    }

     /**
     * 获取 [S2S表单模板]脏标记
     */
    @JsonIgnore
    public boolean getRegistration_view_template_idDirtyFlag(){
        return this.registration_view_template_idDirtyFlag ;
    }   

    /**
     * 获取 [保存卡]
     */
    @JsonProperty("save_token")
    public String getSave_token(){
        return this.save_token ;
    }

    /**
     * 设置 [保存卡]
     */
    @JsonProperty("save_token")
    public void setSave_token(String  save_token){
        this.save_token = save_token ;
        this.save_tokenDirtyFlag = true ;
    }

     /**
     * 获取 [保存卡]脏标记
     */
    @JsonIgnore
    public boolean getSave_tokenDirtyFlag(){
        return this.save_tokenDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [交流]
     */
    @JsonProperty("so_reference_type")
    public String getSo_reference_type(){
        return this.so_reference_type ;
    }

    /**
     * 设置 [交流]
     */
    @JsonProperty("so_reference_type")
    public void setSo_reference_type(String  so_reference_type){
        this.so_reference_type = so_reference_type ;
        this.so_reference_typeDirtyFlag = true ;
    }

     /**
     * 获取 [交流]脏标记
     */
    @JsonIgnore
    public boolean getSo_reference_typeDirtyFlag(){
        return this.so_reference_typeDirtyFlag ;
    }   

    /**
     * 获取 [特定国家/地区]
     */
    @JsonProperty("specific_countries")
    public String getSpecific_countries(){
        return this.specific_countries ;
    }

    /**
     * 设置 [特定国家/地区]
     */
    @JsonProperty("specific_countries")
    public void setSpecific_countries(String  specific_countries){
        this.specific_countries = specific_countries ;
        this.specific_countriesDirtyFlag = true ;
    }

     /**
     * 获取 [特定国家/地区]脏标记
     */
    @JsonIgnore
    public boolean getSpecific_countriesDirtyFlag(){
        return this.specific_countriesDirtyFlag ;
    }   

    /**
     * 获取 [支持保存卡片资料]
     */
    @JsonProperty("token_implemented")
    public String getToken_implemented(){
        return this.token_implemented ;
    }

    /**
     * 设置 [支持保存卡片资料]
     */
    @JsonProperty("token_implemented")
    public void setToken_implemented(String  token_implemented){
        this.token_implemented = token_implemented ;
        this.token_implementedDirtyFlag = true ;
    }

     /**
     * 获取 [支持保存卡片资料]脏标记
     */
    @JsonIgnore
    public boolean getToken_implementedDirtyFlag(){
        return this.token_implementedDirtyFlag ;
    }   

    /**
     * 获取 [窗体按钮模板]
     */
    @JsonProperty("view_template_id")
    public Integer getView_template_id(){
        return this.view_template_id ;
    }

    /**
     * 设置 [窗体按钮模板]
     */
    @JsonProperty("view_template_id")
    public void setView_template_id(Integer  view_template_id){
        this.view_template_id = view_template_id ;
        this.view_template_idDirtyFlag = true ;
    }

     /**
     * 获取 [窗体按钮模板]脏标记
     */
    @JsonIgnore
    public boolean getView_template_idDirtyFlag(){
        return this.view_template_idDirtyFlag ;
    }   

    /**
     * 获取 [网站]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return this.website_id ;
    }

    /**
     * 设置 [网站]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

     /**
     * 获取 [网站]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return this.website_idDirtyFlag ;
    }   

    /**
     * 获取 [在门户/网站可见]
     */
    @JsonProperty("website_published")
    public String getWebsite_published(){
        return this.website_published ;
    }

    /**
     * 设置 [在门户/网站可见]
     */
    @JsonProperty("website_published")
    public void setWebsite_published(String  website_published){
        this.website_published = website_published ;
        this.website_publishedDirtyFlag = true ;
    }

     /**
     * 获取 [在门户/网站可见]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_publishedDirtyFlag(){
        return this.website_publishedDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
