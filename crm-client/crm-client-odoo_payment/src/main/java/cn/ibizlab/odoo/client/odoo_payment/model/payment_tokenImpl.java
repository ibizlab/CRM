package cn.ibizlab.odoo.client.odoo_payment.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ipayment_token;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[payment_token] 对象
 */
public class payment_tokenImpl implements Ipayment_token,Serializable{

    /**
     * 收单方帐号
     */
    public Integer acquirer_id;

    @JsonIgnore
    public boolean acquirer_idDirtyFlag;
    
    /**
     * 收单方帐号
     */
    public String acquirer_id_text;

    @JsonIgnore
    public boolean acquirer_id_textDirtyFlag;
    
    /**
     * 收单方参照
     */
    public String acquirer_ref;

    @JsonIgnore
    public boolean acquirer_refDirtyFlag;
    
    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 业务伙伴
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 业务伙伴
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 付款交易
     */
    public String payment_ids;

    @JsonIgnore
    public boolean payment_idsDirtyFlag;
    
    /**
     * 简名
     */
    public String short_name;

    @JsonIgnore
    public boolean short_nameDirtyFlag;
    
    /**
     * 已验证
     */
    public String verified;

    @JsonIgnore
    public boolean verifiedDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [收单方帐号]
     */
    @JsonProperty("acquirer_id")
    public Integer getAcquirer_id(){
        return this.acquirer_id ;
    }

    /**
     * 设置 [收单方帐号]
     */
    @JsonProperty("acquirer_id")
    public void setAcquirer_id(Integer  acquirer_id){
        this.acquirer_id = acquirer_id ;
        this.acquirer_idDirtyFlag = true ;
    }

     /**
     * 获取 [收单方帐号]脏标记
     */
    @JsonIgnore
    public boolean getAcquirer_idDirtyFlag(){
        return this.acquirer_idDirtyFlag ;
    }   

    /**
     * 获取 [收单方帐号]
     */
    @JsonProperty("acquirer_id_text")
    public String getAcquirer_id_text(){
        return this.acquirer_id_text ;
    }

    /**
     * 设置 [收单方帐号]
     */
    @JsonProperty("acquirer_id_text")
    public void setAcquirer_id_text(String  acquirer_id_text){
        this.acquirer_id_text = acquirer_id_text ;
        this.acquirer_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [收单方帐号]脏标记
     */
    @JsonIgnore
    public boolean getAcquirer_id_textDirtyFlag(){
        return this.acquirer_id_textDirtyFlag ;
    }   

    /**
     * 获取 [收单方参照]
     */
    @JsonProperty("acquirer_ref")
    public String getAcquirer_ref(){
        return this.acquirer_ref ;
    }

    /**
     * 设置 [收单方参照]
     */
    @JsonProperty("acquirer_ref")
    public void setAcquirer_ref(String  acquirer_ref){
        this.acquirer_ref = acquirer_ref ;
        this.acquirer_refDirtyFlag = true ;
    }

     /**
     * 获取 [收单方参照]脏标记
     */
    @JsonIgnore
    public boolean getAcquirer_refDirtyFlag(){
        return this.acquirer_refDirtyFlag ;
    }   

    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [付款交易]
     */
    @JsonProperty("payment_ids")
    public String getPayment_ids(){
        return this.payment_ids ;
    }

    /**
     * 设置 [付款交易]
     */
    @JsonProperty("payment_ids")
    public void setPayment_ids(String  payment_ids){
        this.payment_ids = payment_ids ;
        this.payment_idsDirtyFlag = true ;
    }

     /**
     * 获取 [付款交易]脏标记
     */
    @JsonIgnore
    public boolean getPayment_idsDirtyFlag(){
        return this.payment_idsDirtyFlag ;
    }   

    /**
     * 获取 [简名]
     */
    @JsonProperty("short_name")
    public String getShort_name(){
        return this.short_name ;
    }

    /**
     * 设置 [简名]
     */
    @JsonProperty("short_name")
    public void setShort_name(String  short_name){
        this.short_name = short_name ;
        this.short_nameDirtyFlag = true ;
    }

     /**
     * 获取 [简名]脏标记
     */
    @JsonIgnore
    public boolean getShort_nameDirtyFlag(){
        return this.short_nameDirtyFlag ;
    }   

    /**
     * 获取 [已验证]
     */
    @JsonProperty("verified")
    public String getVerified(){
        return this.verified ;
    }

    /**
     * 设置 [已验证]
     */
    @JsonProperty("verified")
    public void setVerified(String  verified){
        this.verified = verified ;
        this.verifiedDirtyFlag = true ;
    }

     /**
     * 获取 [已验证]脏标记
     */
    @JsonIgnore
    public boolean getVerifiedDirtyFlag(){
        return this.verifiedDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
