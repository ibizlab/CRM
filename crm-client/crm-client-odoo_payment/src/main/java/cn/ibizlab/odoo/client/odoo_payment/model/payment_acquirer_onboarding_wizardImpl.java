package cn.ibizlab.odoo.client.odoo_payment.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ipayment_acquirer_onboarding_wizard;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[payment_acquirer_onboarding_wizard] 对象
 */
public class payment_acquirer_onboarding_wizardImpl implements Ipayment_acquirer_onboarding_wizard,Serializable{

    /**
     * 账户号码
     */
    public String acc_number;

    @JsonIgnore
    public boolean acc_numberDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 银行名称
     */
    public String journal_name;

    @JsonIgnore
    public boolean journal_nameDirtyFlag;
    
    /**
     * 方法
     */
    public String manual_name;

    @JsonIgnore
    public boolean manual_nameDirtyFlag;
    
    /**
     * 支付说明
     */
    public String manual_post_msg;

    @JsonIgnore
    public boolean manual_post_msgDirtyFlag;
    
    /**
     * 付款方法
     */
    public String payment_method;

    @JsonIgnore
    public boolean payment_methodDirtyFlag;
    
    /**
     * PayPal EMailID
     */
    public String paypal_email_account;

    @JsonIgnore
    public boolean paypal_email_accountDirtyFlag;
    
    /**
     * Paypal 付款数据传输标记
     */
    public String paypal_pdt_token;

    @JsonIgnore
    public boolean paypal_pdt_tokenDirtyFlag;
    
    /**
     * Paypal 销售商 ID
     */
    public String paypal_seller_account;

    @JsonIgnore
    public boolean paypal_seller_accountDirtyFlag;
    
    /**
     * Stripe 公钥
     */
    public String stripe_publishable_key;

    @JsonIgnore
    public boolean stripe_publishable_keyDirtyFlag;
    
    /**
     * Stripe 密钥
     */
    public String stripe_secret_key;

    @JsonIgnore
    public boolean stripe_secret_keyDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [账户号码]
     */
    @JsonProperty("acc_number")
    public String getAcc_number(){
        return this.acc_number ;
    }

    /**
     * 设置 [账户号码]
     */
    @JsonProperty("acc_number")
    public void setAcc_number(String  acc_number){
        this.acc_number = acc_number ;
        this.acc_numberDirtyFlag = true ;
    }

     /**
     * 获取 [账户号码]脏标记
     */
    @JsonIgnore
    public boolean getAcc_numberDirtyFlag(){
        return this.acc_numberDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [银行名称]
     */
    @JsonProperty("journal_name")
    public String getJournal_name(){
        return this.journal_name ;
    }

    /**
     * 设置 [银行名称]
     */
    @JsonProperty("journal_name")
    public void setJournal_name(String  journal_name){
        this.journal_name = journal_name ;
        this.journal_nameDirtyFlag = true ;
    }

     /**
     * 获取 [银行名称]脏标记
     */
    @JsonIgnore
    public boolean getJournal_nameDirtyFlag(){
        return this.journal_nameDirtyFlag ;
    }   

    /**
     * 获取 [方法]
     */
    @JsonProperty("manual_name")
    public String getManual_name(){
        return this.manual_name ;
    }

    /**
     * 设置 [方法]
     */
    @JsonProperty("manual_name")
    public void setManual_name(String  manual_name){
        this.manual_name = manual_name ;
        this.manual_nameDirtyFlag = true ;
    }

     /**
     * 获取 [方法]脏标记
     */
    @JsonIgnore
    public boolean getManual_nameDirtyFlag(){
        return this.manual_nameDirtyFlag ;
    }   

    /**
     * 获取 [支付说明]
     */
    @JsonProperty("manual_post_msg")
    public String getManual_post_msg(){
        return this.manual_post_msg ;
    }

    /**
     * 设置 [支付说明]
     */
    @JsonProperty("manual_post_msg")
    public void setManual_post_msg(String  manual_post_msg){
        this.manual_post_msg = manual_post_msg ;
        this.manual_post_msgDirtyFlag = true ;
    }

     /**
     * 获取 [支付说明]脏标记
     */
    @JsonIgnore
    public boolean getManual_post_msgDirtyFlag(){
        return this.manual_post_msgDirtyFlag ;
    }   

    /**
     * 获取 [付款方法]
     */
    @JsonProperty("payment_method")
    public String getPayment_method(){
        return this.payment_method ;
    }

    /**
     * 设置 [付款方法]
     */
    @JsonProperty("payment_method")
    public void setPayment_method(String  payment_method){
        this.payment_method = payment_method ;
        this.payment_methodDirtyFlag = true ;
    }

     /**
     * 获取 [付款方法]脏标记
     */
    @JsonIgnore
    public boolean getPayment_methodDirtyFlag(){
        return this.payment_methodDirtyFlag ;
    }   

    /**
     * 获取 [PayPal EMailID]
     */
    @JsonProperty("paypal_email_account")
    public String getPaypal_email_account(){
        return this.paypal_email_account ;
    }

    /**
     * 设置 [PayPal EMailID]
     */
    @JsonProperty("paypal_email_account")
    public void setPaypal_email_account(String  paypal_email_account){
        this.paypal_email_account = paypal_email_account ;
        this.paypal_email_accountDirtyFlag = true ;
    }

     /**
     * 获取 [PayPal EMailID]脏标记
     */
    @JsonIgnore
    public boolean getPaypal_email_accountDirtyFlag(){
        return this.paypal_email_accountDirtyFlag ;
    }   

    /**
     * 获取 [Paypal 付款数据传输标记]
     */
    @JsonProperty("paypal_pdt_token")
    public String getPaypal_pdt_token(){
        return this.paypal_pdt_token ;
    }

    /**
     * 设置 [Paypal 付款数据传输标记]
     */
    @JsonProperty("paypal_pdt_token")
    public void setPaypal_pdt_token(String  paypal_pdt_token){
        this.paypal_pdt_token = paypal_pdt_token ;
        this.paypal_pdt_tokenDirtyFlag = true ;
    }

     /**
     * 获取 [Paypal 付款数据传输标记]脏标记
     */
    @JsonIgnore
    public boolean getPaypal_pdt_tokenDirtyFlag(){
        return this.paypal_pdt_tokenDirtyFlag ;
    }   

    /**
     * 获取 [Paypal 销售商 ID]
     */
    @JsonProperty("paypal_seller_account")
    public String getPaypal_seller_account(){
        return this.paypal_seller_account ;
    }

    /**
     * 设置 [Paypal 销售商 ID]
     */
    @JsonProperty("paypal_seller_account")
    public void setPaypal_seller_account(String  paypal_seller_account){
        this.paypal_seller_account = paypal_seller_account ;
        this.paypal_seller_accountDirtyFlag = true ;
    }

     /**
     * 获取 [Paypal 销售商 ID]脏标记
     */
    @JsonIgnore
    public boolean getPaypal_seller_accountDirtyFlag(){
        return this.paypal_seller_accountDirtyFlag ;
    }   

    /**
     * 获取 [Stripe 公钥]
     */
    @JsonProperty("stripe_publishable_key")
    public String getStripe_publishable_key(){
        return this.stripe_publishable_key ;
    }

    /**
     * 设置 [Stripe 公钥]
     */
    @JsonProperty("stripe_publishable_key")
    public void setStripe_publishable_key(String  stripe_publishable_key){
        this.stripe_publishable_key = stripe_publishable_key ;
        this.stripe_publishable_keyDirtyFlag = true ;
    }

     /**
     * 获取 [Stripe 公钥]脏标记
     */
    @JsonIgnore
    public boolean getStripe_publishable_keyDirtyFlag(){
        return this.stripe_publishable_keyDirtyFlag ;
    }   

    /**
     * 获取 [Stripe 密钥]
     */
    @JsonProperty("stripe_secret_key")
    public String getStripe_secret_key(){
        return this.stripe_secret_key ;
    }

    /**
     * 设置 [Stripe 密钥]
     */
    @JsonProperty("stripe_secret_key")
    public void setStripe_secret_key(String  stripe_secret_key){
        this.stripe_secret_key = stripe_secret_key ;
        this.stripe_secret_keyDirtyFlag = true ;
    }

     /**
     * 获取 [Stripe 密钥]脏标记
     */
    @JsonIgnore
    public boolean getStripe_secret_keyDirtyFlag(){
        return this.stripe_secret_keyDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
