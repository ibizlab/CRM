package cn.ibizlab.odoo.client.odoo_fetchmail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ifetchmail_server;
import cn.ibizlab.odoo.client.odoo_fetchmail.model.fetchmail_serverImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[fetchmail_server] 服务对象接口
 */
public interface fetchmail_serverFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fetchmail/fetchmail_servers/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_fetchmail/fetchmail_servers/createbatch")
    public fetchmail_serverImpl createBatch(@RequestBody List<fetchmail_serverImpl> fetchmail_servers);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_fetchmail/fetchmail_servers")
    public fetchmail_serverImpl create(@RequestBody fetchmail_serverImpl fetchmail_server);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_fetchmail/fetchmail_servers/{id}")
    public fetchmail_serverImpl update(@PathVariable("id") Integer id,@RequestBody fetchmail_serverImpl fetchmail_server);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fetchmail/fetchmail_servers/fetchdefault")
    public Page<fetchmail_serverImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fetchmail/fetchmail_servers/removebatch")
    public fetchmail_serverImpl removeBatch(@RequestBody List<fetchmail_serverImpl> fetchmail_servers);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_fetchmail/fetchmail_servers/updatebatch")
    public fetchmail_serverImpl updateBatch(@RequestBody List<fetchmail_serverImpl> fetchmail_servers);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fetchmail/fetchmail_servers/{id}")
    public fetchmail_serverImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fetchmail/fetchmail_servers/select")
    public Page<fetchmail_serverImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fetchmail/fetchmail_servers/{id}/getdraft")
    public fetchmail_serverImpl getDraft(@PathVariable("id") Integer id,@RequestBody fetchmail_serverImpl fetchmail_server);



}
