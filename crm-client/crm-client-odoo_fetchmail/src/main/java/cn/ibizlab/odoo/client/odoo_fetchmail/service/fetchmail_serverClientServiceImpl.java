package cn.ibizlab.odoo.client.odoo_fetchmail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ifetchmail_server;
import cn.ibizlab.odoo.client.odoo_fetchmail.config.odoo_fetchmailClientProperties;
import cn.ibizlab.odoo.core.client.service.Ifetchmail_serverClientService;
import cn.ibizlab.odoo.client.odoo_fetchmail.model.fetchmail_serverImpl;
import cn.ibizlab.odoo.client.odoo_fetchmail.feign.fetchmail_serverFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[fetchmail_server] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class fetchmail_serverClientServiceImpl implements Ifetchmail_serverClientService {

    fetchmail_serverFeignClient fetchmail_serverFeignClient;

    @Autowired
    public fetchmail_serverClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_fetchmailClientProperties odoo_fetchmailClientProperties) {
        if (odoo_fetchmailClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.fetchmail_serverFeignClient = nameBuilder.target(fetchmail_serverFeignClient.class,"http://"+odoo_fetchmailClientProperties.getServiceId()+"/") ;
		}else if (odoo_fetchmailClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.fetchmail_serverFeignClient = nameBuilder.target(fetchmail_serverFeignClient.class,odoo_fetchmailClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ifetchmail_server createModel() {
		return new fetchmail_serverImpl();
	}


    public void remove(Ifetchmail_server fetchmail_server){
        fetchmail_serverFeignClient.remove(fetchmail_server.getId()) ;
    }


    public void createBatch(List<Ifetchmail_server> fetchmail_servers){
        if(fetchmail_servers!=null){
            List<fetchmail_serverImpl> list = new ArrayList<fetchmail_serverImpl>();
            for(Ifetchmail_server ifetchmail_server :fetchmail_servers){
                list.add((fetchmail_serverImpl)ifetchmail_server) ;
            }
            fetchmail_serverFeignClient.createBatch(list) ;
        }
    }


    public void create(Ifetchmail_server fetchmail_server){
        Ifetchmail_server clientModel = fetchmail_serverFeignClient.create((fetchmail_serverImpl)fetchmail_server) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fetchmail_server.getClass(), false);
        copier.copy(clientModel, fetchmail_server, null);
    }


    public void update(Ifetchmail_server fetchmail_server){
        Ifetchmail_server clientModel = fetchmail_serverFeignClient.update(fetchmail_server.getId(),(fetchmail_serverImpl)fetchmail_server) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fetchmail_server.getClass(), false);
        copier.copy(clientModel, fetchmail_server, null);
    }


    public Page<Ifetchmail_server> fetchDefault(SearchContext context){
        Page<fetchmail_serverImpl> page = this.fetchmail_serverFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Ifetchmail_server> fetchmail_servers){
        if(fetchmail_servers!=null){
            List<fetchmail_serverImpl> list = new ArrayList<fetchmail_serverImpl>();
            for(Ifetchmail_server ifetchmail_server :fetchmail_servers){
                list.add((fetchmail_serverImpl)ifetchmail_server) ;
            }
            fetchmail_serverFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Ifetchmail_server> fetchmail_servers){
        if(fetchmail_servers!=null){
            List<fetchmail_serverImpl> list = new ArrayList<fetchmail_serverImpl>();
            for(Ifetchmail_server ifetchmail_server :fetchmail_servers){
                list.add((fetchmail_serverImpl)ifetchmail_server) ;
            }
            fetchmail_serverFeignClient.updateBatch(list) ;
        }
    }


    public void get(Ifetchmail_server fetchmail_server){
        Ifetchmail_server clientModel = fetchmail_serverFeignClient.get(fetchmail_server.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fetchmail_server.getClass(), false);
        copier.copy(clientModel, fetchmail_server, null);
    }


    public Page<Ifetchmail_server> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ifetchmail_server fetchmail_server){
        Ifetchmail_server clientModel = fetchmail_serverFeignClient.getDraft(fetchmail_server.getId(),(fetchmail_serverImpl)fetchmail_server) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fetchmail_server.getClass(), false);
        copier.copy(clientModel, fetchmail_server, null);
    }



}

