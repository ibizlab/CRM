package cn.ibizlab.odoo.client.odoo_utm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iutm_medium;
import cn.ibizlab.odoo.client.odoo_utm.config.odoo_utmClientProperties;
import cn.ibizlab.odoo.core.client.service.Iutm_mediumClientService;
import cn.ibizlab.odoo.client.odoo_utm.model.utm_mediumImpl;
import cn.ibizlab.odoo.client.odoo_utm.feign.utm_mediumFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[utm_medium] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class utm_mediumClientServiceImpl implements Iutm_mediumClientService {

    utm_mediumFeignClient utm_mediumFeignClient;

    @Autowired
    public utm_mediumClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_utmClientProperties odoo_utmClientProperties) {
        if (odoo_utmClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.utm_mediumFeignClient = nameBuilder.target(utm_mediumFeignClient.class,"http://"+odoo_utmClientProperties.getServiceId()+"/") ;
		}else if (odoo_utmClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.utm_mediumFeignClient = nameBuilder.target(utm_mediumFeignClient.class,odoo_utmClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iutm_medium createModel() {
		return new utm_mediumImpl();
	}


    public void removeBatch(List<Iutm_medium> utm_media){
        if(utm_media!=null){
            List<utm_mediumImpl> list = new ArrayList<utm_mediumImpl>();
            for(Iutm_medium iutm_medium :utm_media){
                list.add((utm_mediumImpl)iutm_medium) ;
            }
            utm_mediumFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Iutm_medium> utm_media){
        if(utm_media!=null){
            List<utm_mediumImpl> list = new ArrayList<utm_mediumImpl>();
            for(Iutm_medium iutm_medium :utm_media){
                list.add((utm_mediumImpl)iutm_medium) ;
            }
            utm_mediumFeignClient.createBatch(list) ;
        }
    }


    public void create(Iutm_medium utm_medium){
        Iutm_medium clientModel = utm_mediumFeignClient.create((utm_mediumImpl)utm_medium) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), utm_medium.getClass(), false);
        copier.copy(clientModel, utm_medium, null);
    }


    public void remove(Iutm_medium utm_medium){
        utm_mediumFeignClient.remove(utm_medium.getId()) ;
    }


    public void updateBatch(List<Iutm_medium> utm_media){
        if(utm_media!=null){
            List<utm_mediumImpl> list = new ArrayList<utm_mediumImpl>();
            for(Iutm_medium iutm_medium :utm_media){
                list.add((utm_mediumImpl)iutm_medium) ;
            }
            utm_mediumFeignClient.updateBatch(list) ;
        }
    }


    public void update(Iutm_medium utm_medium){
        Iutm_medium clientModel = utm_mediumFeignClient.update(utm_medium.getId(),(utm_mediumImpl)utm_medium) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), utm_medium.getClass(), false);
        copier.copy(clientModel, utm_medium, null);
    }


    public void get(Iutm_medium utm_medium){
        Iutm_medium clientModel = utm_mediumFeignClient.get(utm_medium.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), utm_medium.getClass(), false);
        copier.copy(clientModel, utm_medium, null);
    }


    public Page<Iutm_medium> fetchDefault(SearchContext context){
        Page<utm_mediumImpl> page = this.utm_mediumFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Iutm_medium> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iutm_medium utm_medium){
        Iutm_medium clientModel = utm_mediumFeignClient.getDraft(utm_medium.getId(),(utm_mediumImpl)utm_medium) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), utm_medium.getClass(), false);
        copier.copy(clientModel, utm_medium, null);
    }



}

