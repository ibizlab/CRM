package cn.ibizlab.odoo.client.odoo_utm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iutm_mixin;
import cn.ibizlab.odoo.client.odoo_utm.config.odoo_utmClientProperties;
import cn.ibizlab.odoo.core.client.service.Iutm_mixinClientService;
import cn.ibizlab.odoo.client.odoo_utm.model.utm_mixinImpl;
import cn.ibizlab.odoo.client.odoo_utm.feign.utm_mixinFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[utm_mixin] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class utm_mixinClientServiceImpl implements Iutm_mixinClientService {

    utm_mixinFeignClient utm_mixinFeignClient;

    @Autowired
    public utm_mixinClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_utmClientProperties odoo_utmClientProperties) {
        if (odoo_utmClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.utm_mixinFeignClient = nameBuilder.target(utm_mixinFeignClient.class,"http://"+odoo_utmClientProperties.getServiceId()+"/") ;
		}else if (odoo_utmClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.utm_mixinFeignClient = nameBuilder.target(utm_mixinFeignClient.class,odoo_utmClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iutm_mixin createModel() {
		return new utm_mixinImpl();
	}


    public void createBatch(List<Iutm_mixin> utm_mixins){
        if(utm_mixins!=null){
            List<utm_mixinImpl> list = new ArrayList<utm_mixinImpl>();
            for(Iutm_mixin iutm_mixin :utm_mixins){
                list.add((utm_mixinImpl)iutm_mixin) ;
            }
            utm_mixinFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Iutm_mixin> utm_mixins){
        if(utm_mixins!=null){
            List<utm_mixinImpl> list = new ArrayList<utm_mixinImpl>();
            for(Iutm_mixin iutm_mixin :utm_mixins){
                list.add((utm_mixinImpl)iutm_mixin) ;
            }
            utm_mixinFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Iutm_mixin utm_mixin){
        utm_mixinFeignClient.remove(utm_mixin.getId()) ;
    }


    public void updateBatch(List<Iutm_mixin> utm_mixins){
        if(utm_mixins!=null){
            List<utm_mixinImpl> list = new ArrayList<utm_mixinImpl>();
            for(Iutm_mixin iutm_mixin :utm_mixins){
                list.add((utm_mixinImpl)iutm_mixin) ;
            }
            utm_mixinFeignClient.updateBatch(list) ;
        }
    }


    public void create(Iutm_mixin utm_mixin){
        Iutm_mixin clientModel = utm_mixinFeignClient.create((utm_mixinImpl)utm_mixin) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), utm_mixin.getClass(), false);
        copier.copy(clientModel, utm_mixin, null);
    }


    public void get(Iutm_mixin utm_mixin){
        Iutm_mixin clientModel = utm_mixinFeignClient.get(utm_mixin.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), utm_mixin.getClass(), false);
        copier.copy(clientModel, utm_mixin, null);
    }


    public Page<Iutm_mixin> fetchDefault(SearchContext context){
        Page<utm_mixinImpl> page = this.utm_mixinFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Iutm_mixin utm_mixin){
        Iutm_mixin clientModel = utm_mixinFeignClient.update(utm_mixin.getId(),(utm_mixinImpl)utm_mixin) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), utm_mixin.getClass(), false);
        copier.copy(clientModel, utm_mixin, null);
    }


    public Page<Iutm_mixin> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iutm_mixin utm_mixin){
        Iutm_mixin clientModel = utm_mixinFeignClient.getDraft(utm_mixin.getId(),(utm_mixinImpl)utm_mixin) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), utm_mixin.getClass(), false);
        copier.copy(clientModel, utm_mixin, null);
    }



}

