package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_warehouse_orderpoint;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_warehouse_orderpointClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_warehouse_orderpointImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_warehouse_orderpointFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_warehouse_orderpoint] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_warehouse_orderpointClientServiceImpl implements Istock_warehouse_orderpointClientService {

    stock_warehouse_orderpointFeignClient stock_warehouse_orderpointFeignClient;

    @Autowired
    public stock_warehouse_orderpointClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_warehouse_orderpointFeignClient = nameBuilder.target(stock_warehouse_orderpointFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_warehouse_orderpointFeignClient = nameBuilder.target(stock_warehouse_orderpointFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_warehouse_orderpoint createModel() {
		return new stock_warehouse_orderpointImpl();
	}


    public void createBatch(List<Istock_warehouse_orderpoint> stock_warehouse_orderpoints){
        if(stock_warehouse_orderpoints!=null){
            List<stock_warehouse_orderpointImpl> list = new ArrayList<stock_warehouse_orderpointImpl>();
            for(Istock_warehouse_orderpoint istock_warehouse_orderpoint :stock_warehouse_orderpoints){
                list.add((stock_warehouse_orderpointImpl)istock_warehouse_orderpoint) ;
            }
            stock_warehouse_orderpointFeignClient.createBatch(list) ;
        }
    }


    public void remove(Istock_warehouse_orderpoint stock_warehouse_orderpoint){
        stock_warehouse_orderpointFeignClient.remove(stock_warehouse_orderpoint.getId()) ;
    }


    public void updateBatch(List<Istock_warehouse_orderpoint> stock_warehouse_orderpoints){
        if(stock_warehouse_orderpoints!=null){
            List<stock_warehouse_orderpointImpl> list = new ArrayList<stock_warehouse_orderpointImpl>();
            for(Istock_warehouse_orderpoint istock_warehouse_orderpoint :stock_warehouse_orderpoints){
                list.add((stock_warehouse_orderpointImpl)istock_warehouse_orderpoint) ;
            }
            stock_warehouse_orderpointFeignClient.updateBatch(list) ;
        }
    }


    public void get(Istock_warehouse_orderpoint stock_warehouse_orderpoint){
        Istock_warehouse_orderpoint clientModel = stock_warehouse_orderpointFeignClient.get(stock_warehouse_orderpoint.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_warehouse_orderpoint.getClass(), false);
        copier.copy(clientModel, stock_warehouse_orderpoint, null);
    }


    public void removeBatch(List<Istock_warehouse_orderpoint> stock_warehouse_orderpoints){
        if(stock_warehouse_orderpoints!=null){
            List<stock_warehouse_orderpointImpl> list = new ArrayList<stock_warehouse_orderpointImpl>();
            for(Istock_warehouse_orderpoint istock_warehouse_orderpoint :stock_warehouse_orderpoints){
                list.add((stock_warehouse_orderpointImpl)istock_warehouse_orderpoint) ;
            }
            stock_warehouse_orderpointFeignClient.removeBatch(list) ;
        }
    }


    public void update(Istock_warehouse_orderpoint stock_warehouse_orderpoint){
        Istock_warehouse_orderpoint clientModel = stock_warehouse_orderpointFeignClient.update(stock_warehouse_orderpoint.getId(),(stock_warehouse_orderpointImpl)stock_warehouse_orderpoint) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_warehouse_orderpoint.getClass(), false);
        copier.copy(clientModel, stock_warehouse_orderpoint, null);
    }


    public void create(Istock_warehouse_orderpoint stock_warehouse_orderpoint){
        Istock_warehouse_orderpoint clientModel = stock_warehouse_orderpointFeignClient.create((stock_warehouse_orderpointImpl)stock_warehouse_orderpoint) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_warehouse_orderpoint.getClass(), false);
        copier.copy(clientModel, stock_warehouse_orderpoint, null);
    }


    public Page<Istock_warehouse_orderpoint> fetchDefault(SearchContext context){
        Page<stock_warehouse_orderpointImpl> page = this.stock_warehouse_orderpointFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Istock_warehouse_orderpoint> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_warehouse_orderpoint stock_warehouse_orderpoint){
        Istock_warehouse_orderpoint clientModel = stock_warehouse_orderpointFeignClient.getDraft(stock_warehouse_orderpoint.getId(),(stock_warehouse_orderpointImpl)stock_warehouse_orderpoint) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_warehouse_orderpoint.getClass(), false);
        copier.copy(clientModel, stock_warehouse_orderpoint, null);
    }



}

