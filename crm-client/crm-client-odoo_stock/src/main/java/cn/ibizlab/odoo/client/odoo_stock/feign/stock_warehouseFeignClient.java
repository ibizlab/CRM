package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_warehouse;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_warehouseImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_warehouse] 服务对象接口
 */
public interface stock_warehouseFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_warehouses/updatebatch")
    public stock_warehouseImpl updateBatch(@RequestBody List<stock_warehouseImpl> stock_warehouses);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_warehouses/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warehouses/fetchdefault")
    public Page<stock_warehouseImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_warehouses/{id}")
    public stock_warehouseImpl update(@PathVariable("id") Integer id,@RequestBody stock_warehouseImpl stock_warehouse);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_warehouses")
    public stock_warehouseImpl create(@RequestBody stock_warehouseImpl stock_warehouse);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_warehouses/createbatch")
    public stock_warehouseImpl createBatch(@RequestBody List<stock_warehouseImpl> stock_warehouses);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_warehouses/removebatch")
    public stock_warehouseImpl removeBatch(@RequestBody List<stock_warehouseImpl> stock_warehouses);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warehouses/{id}")
    public stock_warehouseImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warehouses/select")
    public Page<stock_warehouseImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warehouses/{id}/getdraft")
    public stock_warehouseImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_warehouseImpl stock_warehouse);



}
