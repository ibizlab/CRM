package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_inventory_line;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_inventory_lineImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_inventory_line] 服务对象接口
 */
public interface stock_inventory_lineFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_inventory_lines/updatebatch")
    public stock_inventory_lineImpl updateBatch(@RequestBody List<stock_inventory_lineImpl> stock_inventory_lines);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_inventory_lines/removebatch")
    public stock_inventory_lineImpl removeBatch(@RequestBody List<stock_inventory_lineImpl> stock_inventory_lines);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_inventory_lines/createbatch")
    public stock_inventory_lineImpl createBatch(@RequestBody List<stock_inventory_lineImpl> stock_inventory_lines);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_inventory_lines/{id}")
    public stock_inventory_lineImpl update(@PathVariable("id") Integer id,@RequestBody stock_inventory_lineImpl stock_inventory_line);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_inventory_lines")
    public stock_inventory_lineImpl create(@RequestBody stock_inventory_lineImpl stock_inventory_line);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_inventory_lines/fetchdefault")
    public Page<stock_inventory_lineImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_inventory_lines/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_inventory_lines/{id}")
    public stock_inventory_lineImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_inventory_lines/select")
    public Page<stock_inventory_lineImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_inventory_lines/{id}/getdraft")
    public stock_inventory_lineImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_inventory_lineImpl stock_inventory_line);



}
