package cn.ibizlab.odoo.client.odoo_stock.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Istock_package_level;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[stock_package_level] 对象
 */
public class stock_package_levelImpl implements Istock_package_level,Serializable{

    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 完成
     */
    public String is_done;

    @JsonIgnore
    public boolean is_doneDirtyFlag;
    
    /**
     * 是新鲜包裹
     */
    public String is_fresh_package;

    @JsonIgnore
    public boolean is_fresh_packageDirtyFlag;
    
    /**
     * 至
     */
    public Integer location_dest_id;

    @JsonIgnore
    public boolean location_dest_idDirtyFlag;
    
    /**
     * 至
     */
    public String location_dest_id_text;

    @JsonIgnore
    public boolean location_dest_id_textDirtyFlag;
    
    /**
     * 从
     */
    public Integer location_id;

    @JsonIgnore
    public boolean location_idDirtyFlag;
    
    /**
     * 移动
     */
    public String move_ids;

    @JsonIgnore
    public boolean move_idsDirtyFlag;
    
    /**
     * 凭证明细
     */
    public String move_line_ids;

    @JsonIgnore
    public boolean move_line_idsDirtyFlag;
    
    /**
     * 包裹
     */
    public Integer package_id;

    @JsonIgnore
    public boolean package_idDirtyFlag;
    
    /**
     * 包裹
     */
    public String package_id_text;

    @JsonIgnore
    public boolean package_id_textDirtyFlag;
    
    /**
     * 分拣
     */
    public Integer picking_id;

    @JsonIgnore
    public boolean picking_idDirtyFlag;
    
    /**
     * 分拣
     */
    public String picking_id_text;

    @JsonIgnore
    public boolean picking_id_textDirtyFlag;
    
    /**
     * 源位置
     */
    public Integer picking_source_location;

    @JsonIgnore
    public boolean picking_source_locationDirtyFlag;
    
    /**
     * 显示批次M2O
     */
    public String show_lots_m2o;

    @JsonIgnore
    public boolean show_lots_m2oDirtyFlag;
    
    /**
     * 显示批次文本
     */
    public String show_lots_text;

    @JsonIgnore
    public boolean show_lots_textDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [完成]
     */
    @JsonProperty("is_done")
    public String getIs_done(){
        return this.is_done ;
    }

    /**
     * 设置 [完成]
     */
    @JsonProperty("is_done")
    public void setIs_done(String  is_done){
        this.is_done = is_done ;
        this.is_doneDirtyFlag = true ;
    }

     /**
     * 获取 [完成]脏标记
     */
    @JsonIgnore
    public boolean getIs_doneDirtyFlag(){
        return this.is_doneDirtyFlag ;
    }   

    /**
     * 获取 [是新鲜包裹]
     */
    @JsonProperty("is_fresh_package")
    public String getIs_fresh_package(){
        return this.is_fresh_package ;
    }

    /**
     * 设置 [是新鲜包裹]
     */
    @JsonProperty("is_fresh_package")
    public void setIs_fresh_package(String  is_fresh_package){
        this.is_fresh_package = is_fresh_package ;
        this.is_fresh_packageDirtyFlag = true ;
    }

     /**
     * 获取 [是新鲜包裹]脏标记
     */
    @JsonIgnore
    public boolean getIs_fresh_packageDirtyFlag(){
        return this.is_fresh_packageDirtyFlag ;
    }   

    /**
     * 获取 [至]
     */
    @JsonProperty("location_dest_id")
    public Integer getLocation_dest_id(){
        return this.location_dest_id ;
    }

    /**
     * 设置 [至]
     */
    @JsonProperty("location_dest_id")
    public void setLocation_dest_id(Integer  location_dest_id){
        this.location_dest_id = location_dest_id ;
        this.location_dest_idDirtyFlag = true ;
    }

     /**
     * 获取 [至]脏标记
     */
    @JsonIgnore
    public boolean getLocation_dest_idDirtyFlag(){
        return this.location_dest_idDirtyFlag ;
    }   

    /**
     * 获取 [至]
     */
    @JsonProperty("location_dest_id_text")
    public String getLocation_dest_id_text(){
        return this.location_dest_id_text ;
    }

    /**
     * 设置 [至]
     */
    @JsonProperty("location_dest_id_text")
    public void setLocation_dest_id_text(String  location_dest_id_text){
        this.location_dest_id_text = location_dest_id_text ;
        this.location_dest_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [至]脏标记
     */
    @JsonIgnore
    public boolean getLocation_dest_id_textDirtyFlag(){
        return this.location_dest_id_textDirtyFlag ;
    }   

    /**
     * 获取 [从]
     */
    @JsonProperty("location_id")
    public Integer getLocation_id(){
        return this.location_id ;
    }

    /**
     * 设置 [从]
     */
    @JsonProperty("location_id")
    public void setLocation_id(Integer  location_id){
        this.location_id = location_id ;
        this.location_idDirtyFlag = true ;
    }

     /**
     * 获取 [从]脏标记
     */
    @JsonIgnore
    public boolean getLocation_idDirtyFlag(){
        return this.location_idDirtyFlag ;
    }   

    /**
     * 获取 [移动]
     */
    @JsonProperty("move_ids")
    public String getMove_ids(){
        return this.move_ids ;
    }

    /**
     * 设置 [移动]
     */
    @JsonProperty("move_ids")
    public void setMove_ids(String  move_ids){
        this.move_ids = move_ids ;
        this.move_idsDirtyFlag = true ;
    }

     /**
     * 获取 [移动]脏标记
     */
    @JsonIgnore
    public boolean getMove_idsDirtyFlag(){
        return this.move_idsDirtyFlag ;
    }   

    /**
     * 获取 [凭证明细]
     */
    @JsonProperty("move_line_ids")
    public String getMove_line_ids(){
        return this.move_line_ids ;
    }

    /**
     * 设置 [凭证明细]
     */
    @JsonProperty("move_line_ids")
    public void setMove_line_ids(String  move_line_ids){
        this.move_line_ids = move_line_ids ;
        this.move_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [凭证明细]脏标记
     */
    @JsonIgnore
    public boolean getMove_line_idsDirtyFlag(){
        return this.move_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [包裹]
     */
    @JsonProperty("package_id")
    public Integer getPackage_id(){
        return this.package_id ;
    }

    /**
     * 设置 [包裹]
     */
    @JsonProperty("package_id")
    public void setPackage_id(Integer  package_id){
        this.package_id = package_id ;
        this.package_idDirtyFlag = true ;
    }

     /**
     * 获取 [包裹]脏标记
     */
    @JsonIgnore
    public boolean getPackage_idDirtyFlag(){
        return this.package_idDirtyFlag ;
    }   

    /**
     * 获取 [包裹]
     */
    @JsonProperty("package_id_text")
    public String getPackage_id_text(){
        return this.package_id_text ;
    }

    /**
     * 设置 [包裹]
     */
    @JsonProperty("package_id_text")
    public void setPackage_id_text(String  package_id_text){
        this.package_id_text = package_id_text ;
        this.package_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [包裹]脏标记
     */
    @JsonIgnore
    public boolean getPackage_id_textDirtyFlag(){
        return this.package_id_textDirtyFlag ;
    }   

    /**
     * 获取 [分拣]
     */
    @JsonProperty("picking_id")
    public Integer getPicking_id(){
        return this.picking_id ;
    }

    /**
     * 设置 [分拣]
     */
    @JsonProperty("picking_id")
    public void setPicking_id(Integer  picking_id){
        this.picking_id = picking_id ;
        this.picking_idDirtyFlag = true ;
    }

     /**
     * 获取 [分拣]脏标记
     */
    @JsonIgnore
    public boolean getPicking_idDirtyFlag(){
        return this.picking_idDirtyFlag ;
    }   

    /**
     * 获取 [分拣]
     */
    @JsonProperty("picking_id_text")
    public String getPicking_id_text(){
        return this.picking_id_text ;
    }

    /**
     * 设置 [分拣]
     */
    @JsonProperty("picking_id_text")
    public void setPicking_id_text(String  picking_id_text){
        this.picking_id_text = picking_id_text ;
        this.picking_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [分拣]脏标记
     */
    @JsonIgnore
    public boolean getPicking_id_textDirtyFlag(){
        return this.picking_id_textDirtyFlag ;
    }   

    /**
     * 获取 [源位置]
     */
    @JsonProperty("picking_source_location")
    public Integer getPicking_source_location(){
        return this.picking_source_location ;
    }

    /**
     * 设置 [源位置]
     */
    @JsonProperty("picking_source_location")
    public void setPicking_source_location(Integer  picking_source_location){
        this.picking_source_location = picking_source_location ;
        this.picking_source_locationDirtyFlag = true ;
    }

     /**
     * 获取 [源位置]脏标记
     */
    @JsonIgnore
    public boolean getPicking_source_locationDirtyFlag(){
        return this.picking_source_locationDirtyFlag ;
    }   

    /**
     * 获取 [显示批次M2O]
     */
    @JsonProperty("show_lots_m2o")
    public String getShow_lots_m2o(){
        return this.show_lots_m2o ;
    }

    /**
     * 设置 [显示批次M2O]
     */
    @JsonProperty("show_lots_m2o")
    public void setShow_lots_m2o(String  show_lots_m2o){
        this.show_lots_m2o = show_lots_m2o ;
        this.show_lots_m2oDirtyFlag = true ;
    }

     /**
     * 获取 [显示批次M2O]脏标记
     */
    @JsonIgnore
    public boolean getShow_lots_m2oDirtyFlag(){
        return this.show_lots_m2oDirtyFlag ;
    }   

    /**
     * 获取 [显示批次文本]
     */
    @JsonProperty("show_lots_text")
    public String getShow_lots_text(){
        return this.show_lots_text ;
    }

    /**
     * 设置 [显示批次文本]
     */
    @JsonProperty("show_lots_text")
    public void setShow_lots_text(String  show_lots_text){
        this.show_lots_text = show_lots_text ;
        this.show_lots_textDirtyFlag = true ;
    }

     /**
     * 获取 [显示批次文本]脏标记
     */
    @JsonIgnore
    public boolean getShow_lots_textDirtyFlag(){
        return this.show_lots_textDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
