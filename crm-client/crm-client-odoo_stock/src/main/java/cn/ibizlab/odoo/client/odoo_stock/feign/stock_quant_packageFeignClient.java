package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_quant_package;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_quant_packageImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_quant_package] 服务对象接口
 */
public interface stock_quant_packageFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_quant_packages/removebatch")
    public stock_quant_packageImpl removeBatch(@RequestBody List<stock_quant_packageImpl> stock_quant_packages);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_quant_packages/updatebatch")
    public stock_quant_packageImpl updateBatch(@RequestBody List<stock_quant_packageImpl> stock_quant_packages);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_quant_packages/{id}")
    public stock_quant_packageImpl update(@PathVariable("id") Integer id,@RequestBody stock_quant_packageImpl stock_quant_package);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_quant_packages/{id}")
    public stock_quant_packageImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_quant_packages/fetchdefault")
    public Page<stock_quant_packageImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_quant_packages")
    public stock_quant_packageImpl create(@RequestBody stock_quant_packageImpl stock_quant_package);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_quant_packages/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_quant_packages/createbatch")
    public stock_quant_packageImpl createBatch(@RequestBody List<stock_quant_packageImpl> stock_quant_packages);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_quant_packages/select")
    public Page<stock_quant_packageImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_quant_packages/{id}/getdraft")
    public stock_quant_packageImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_quant_packageImpl stock_quant_package);



}
