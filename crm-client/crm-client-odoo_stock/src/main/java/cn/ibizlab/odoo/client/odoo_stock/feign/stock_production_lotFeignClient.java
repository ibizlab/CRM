package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_production_lot;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_production_lotImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_production_lot] 服务对象接口
 */
public interface stock_production_lotFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_production_lots/{id}")
    public stock_production_lotImpl update(@PathVariable("id") Integer id,@RequestBody stock_production_lotImpl stock_production_lot);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_production_lots/updatebatch")
    public stock_production_lotImpl updateBatch(@RequestBody List<stock_production_lotImpl> stock_production_lots);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_production_lots/createbatch")
    public stock_production_lotImpl createBatch(@RequestBody List<stock_production_lotImpl> stock_production_lots);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_production_lots/fetchdefault")
    public Page<stock_production_lotImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_production_lots/{id}")
    public stock_production_lotImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_production_lots/removebatch")
    public stock_production_lotImpl removeBatch(@RequestBody List<stock_production_lotImpl> stock_production_lots);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_production_lots")
    public stock_production_lotImpl create(@RequestBody stock_production_lotImpl stock_production_lot);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_production_lots/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_production_lots/select")
    public Page<stock_production_lotImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_production_lots/{id}/getdraft")
    public stock_production_lotImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_production_lotImpl stock_production_lot);



}
