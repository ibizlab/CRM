package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_return_picking;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_return_pickingImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_return_picking] 服务对象接口
 */
public interface stock_return_pickingFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_return_pickings/updatebatch")
    public stock_return_pickingImpl updateBatch(@RequestBody List<stock_return_pickingImpl> stock_return_pickings);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_return_pickings/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_return_pickings/fetchdefault")
    public Page<stock_return_pickingImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_return_pickings/removebatch")
    public stock_return_pickingImpl removeBatch(@RequestBody List<stock_return_pickingImpl> stock_return_pickings);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_return_pickings/createbatch")
    public stock_return_pickingImpl createBatch(@RequestBody List<stock_return_pickingImpl> stock_return_pickings);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_return_pickings")
    public stock_return_pickingImpl create(@RequestBody stock_return_pickingImpl stock_return_picking);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_return_pickings/{id}")
    public stock_return_pickingImpl update(@PathVariable("id") Integer id,@RequestBody stock_return_pickingImpl stock_return_picking);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_return_pickings/{id}")
    public stock_return_pickingImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_return_pickings/select")
    public Page<stock_return_pickingImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_return_pickings/{id}/getdraft")
    public stock_return_pickingImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_return_pickingImpl stock_return_picking);



}
