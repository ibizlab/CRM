package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_track_confirmation;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_track_confirmationClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_track_confirmationImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_track_confirmationFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_track_confirmation] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_track_confirmationClientServiceImpl implements Istock_track_confirmationClientService {

    stock_track_confirmationFeignClient stock_track_confirmationFeignClient;

    @Autowired
    public stock_track_confirmationClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_track_confirmationFeignClient = nameBuilder.target(stock_track_confirmationFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_track_confirmationFeignClient = nameBuilder.target(stock_track_confirmationFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_track_confirmation createModel() {
		return new stock_track_confirmationImpl();
	}


    public void updateBatch(List<Istock_track_confirmation> stock_track_confirmations){
        if(stock_track_confirmations!=null){
            List<stock_track_confirmationImpl> list = new ArrayList<stock_track_confirmationImpl>();
            for(Istock_track_confirmation istock_track_confirmation :stock_track_confirmations){
                list.add((stock_track_confirmationImpl)istock_track_confirmation) ;
            }
            stock_track_confirmationFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Istock_track_confirmation> stock_track_confirmations){
        if(stock_track_confirmations!=null){
            List<stock_track_confirmationImpl> list = new ArrayList<stock_track_confirmationImpl>();
            for(Istock_track_confirmation istock_track_confirmation :stock_track_confirmations){
                list.add((stock_track_confirmationImpl)istock_track_confirmation) ;
            }
            stock_track_confirmationFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Istock_track_confirmation> stock_track_confirmations){
        if(stock_track_confirmations!=null){
            List<stock_track_confirmationImpl> list = new ArrayList<stock_track_confirmationImpl>();
            for(Istock_track_confirmation istock_track_confirmation :stock_track_confirmations){
                list.add((stock_track_confirmationImpl)istock_track_confirmation) ;
            }
            stock_track_confirmationFeignClient.removeBatch(list) ;
        }
    }


    public Page<Istock_track_confirmation> fetchDefault(SearchContext context){
        Page<stock_track_confirmationImpl> page = this.stock_track_confirmationFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Istock_track_confirmation stock_track_confirmation){
        Istock_track_confirmation clientModel = stock_track_confirmationFeignClient.create((stock_track_confirmationImpl)stock_track_confirmation) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_track_confirmation.getClass(), false);
        copier.copy(clientModel, stock_track_confirmation, null);
    }


    public void update(Istock_track_confirmation stock_track_confirmation){
        Istock_track_confirmation clientModel = stock_track_confirmationFeignClient.update(stock_track_confirmation.getId(),(stock_track_confirmationImpl)stock_track_confirmation) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_track_confirmation.getClass(), false);
        copier.copy(clientModel, stock_track_confirmation, null);
    }


    public void get(Istock_track_confirmation stock_track_confirmation){
        Istock_track_confirmation clientModel = stock_track_confirmationFeignClient.get(stock_track_confirmation.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_track_confirmation.getClass(), false);
        copier.copy(clientModel, stock_track_confirmation, null);
    }


    public void remove(Istock_track_confirmation stock_track_confirmation){
        stock_track_confirmationFeignClient.remove(stock_track_confirmation.getId()) ;
    }


    public Page<Istock_track_confirmation> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_track_confirmation stock_track_confirmation){
        Istock_track_confirmation clientModel = stock_track_confirmationFeignClient.getDraft(stock_track_confirmation.getId(),(stock_track_confirmationImpl)stock_track_confirmation) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_track_confirmation.getClass(), false);
        copier.copy(clientModel, stock_track_confirmation, null);
    }



}

