package cn.ibizlab.odoo.client.odoo_stock.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Istock_location_route;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[stock_location_route] 对象
 */
public class stock_location_routeImpl implements Istock_location_route,Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 产品类别
     */
    public String categ_ids;

    @JsonIgnore
    public boolean categ_idsDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 路线
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 可应用于产品类别
     */
    public String product_categ_selectable;

    @JsonIgnore
    public boolean product_categ_selectableDirtyFlag;
    
    /**
     * 产品
     */
    public String product_ids;

    @JsonIgnore
    public boolean product_idsDirtyFlag;
    
    /**
     * 可应用于产品
     */
    public String product_selectable;

    @JsonIgnore
    public boolean product_selectableDirtyFlag;
    
    /**
     * 规则
     */
    public String rule_ids;

    @JsonIgnore
    public boolean rule_idsDirtyFlag;
    
    /**
     * 在销售订单行上可选
     */
    public String sale_selectable;

    @JsonIgnore
    public boolean sale_selectableDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 供应的仓库
     */
    public Integer supplied_wh_id;

    @JsonIgnore
    public boolean supplied_wh_idDirtyFlag;
    
    /**
     * 供应的仓库
     */
    public String supplied_wh_id_text;

    @JsonIgnore
    public boolean supplied_wh_id_textDirtyFlag;
    
    /**
     * 供应仓库
     */
    public Integer supplier_wh_id;

    @JsonIgnore
    public boolean supplier_wh_idDirtyFlag;
    
    /**
     * 供应仓库
     */
    public String supplier_wh_id_text;

    @JsonIgnore
    public boolean supplier_wh_id_textDirtyFlag;
    
    /**
     * 仓库
     */
    public String warehouse_ids;

    @JsonIgnore
    public boolean warehouse_idsDirtyFlag;
    
    /**
     * 可应用于仓库
     */
    public String warehouse_selectable;

    @JsonIgnore
    public boolean warehouse_selectableDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [产品类别]
     */
    @JsonProperty("categ_ids")
    public String getCateg_ids(){
        return this.categ_ids ;
    }

    /**
     * 设置 [产品类别]
     */
    @JsonProperty("categ_ids")
    public void setCateg_ids(String  categ_ids){
        this.categ_ids = categ_ids ;
        this.categ_idsDirtyFlag = true ;
    }

     /**
     * 获取 [产品类别]脏标记
     */
    @JsonIgnore
    public boolean getCateg_idsDirtyFlag(){
        return this.categ_idsDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [路线]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [路线]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [路线]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [可应用于产品类别]
     */
    @JsonProperty("product_categ_selectable")
    public String getProduct_categ_selectable(){
        return this.product_categ_selectable ;
    }

    /**
     * 设置 [可应用于产品类别]
     */
    @JsonProperty("product_categ_selectable")
    public void setProduct_categ_selectable(String  product_categ_selectable){
        this.product_categ_selectable = product_categ_selectable ;
        this.product_categ_selectableDirtyFlag = true ;
    }

     /**
     * 获取 [可应用于产品类别]脏标记
     */
    @JsonIgnore
    public boolean getProduct_categ_selectableDirtyFlag(){
        return this.product_categ_selectableDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_ids")
    public String getProduct_ids(){
        return this.product_ids ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_ids")
    public void setProduct_ids(String  product_ids){
        this.product_ids = product_ids ;
        this.product_idsDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idsDirtyFlag(){
        return this.product_idsDirtyFlag ;
    }   

    /**
     * 获取 [可应用于产品]
     */
    @JsonProperty("product_selectable")
    public String getProduct_selectable(){
        return this.product_selectable ;
    }

    /**
     * 设置 [可应用于产品]
     */
    @JsonProperty("product_selectable")
    public void setProduct_selectable(String  product_selectable){
        this.product_selectable = product_selectable ;
        this.product_selectableDirtyFlag = true ;
    }

     /**
     * 获取 [可应用于产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_selectableDirtyFlag(){
        return this.product_selectableDirtyFlag ;
    }   

    /**
     * 获取 [规则]
     */
    @JsonProperty("rule_ids")
    public String getRule_ids(){
        return this.rule_ids ;
    }

    /**
     * 设置 [规则]
     */
    @JsonProperty("rule_ids")
    public void setRule_ids(String  rule_ids){
        this.rule_ids = rule_ids ;
        this.rule_idsDirtyFlag = true ;
    }

     /**
     * 获取 [规则]脏标记
     */
    @JsonIgnore
    public boolean getRule_idsDirtyFlag(){
        return this.rule_idsDirtyFlag ;
    }   

    /**
     * 获取 [在销售订单行上可选]
     */
    @JsonProperty("sale_selectable")
    public String getSale_selectable(){
        return this.sale_selectable ;
    }

    /**
     * 设置 [在销售订单行上可选]
     */
    @JsonProperty("sale_selectable")
    public void setSale_selectable(String  sale_selectable){
        this.sale_selectable = sale_selectable ;
        this.sale_selectableDirtyFlag = true ;
    }

     /**
     * 获取 [在销售订单行上可选]脏标记
     */
    @JsonIgnore
    public boolean getSale_selectableDirtyFlag(){
        return this.sale_selectableDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [供应的仓库]
     */
    @JsonProperty("supplied_wh_id")
    public Integer getSupplied_wh_id(){
        return this.supplied_wh_id ;
    }

    /**
     * 设置 [供应的仓库]
     */
    @JsonProperty("supplied_wh_id")
    public void setSupplied_wh_id(Integer  supplied_wh_id){
        this.supplied_wh_id = supplied_wh_id ;
        this.supplied_wh_idDirtyFlag = true ;
    }

     /**
     * 获取 [供应的仓库]脏标记
     */
    @JsonIgnore
    public boolean getSupplied_wh_idDirtyFlag(){
        return this.supplied_wh_idDirtyFlag ;
    }   

    /**
     * 获取 [供应的仓库]
     */
    @JsonProperty("supplied_wh_id_text")
    public String getSupplied_wh_id_text(){
        return this.supplied_wh_id_text ;
    }

    /**
     * 设置 [供应的仓库]
     */
    @JsonProperty("supplied_wh_id_text")
    public void setSupplied_wh_id_text(String  supplied_wh_id_text){
        this.supplied_wh_id_text = supplied_wh_id_text ;
        this.supplied_wh_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [供应的仓库]脏标记
     */
    @JsonIgnore
    public boolean getSupplied_wh_id_textDirtyFlag(){
        return this.supplied_wh_id_textDirtyFlag ;
    }   

    /**
     * 获取 [供应仓库]
     */
    @JsonProperty("supplier_wh_id")
    public Integer getSupplier_wh_id(){
        return this.supplier_wh_id ;
    }

    /**
     * 设置 [供应仓库]
     */
    @JsonProperty("supplier_wh_id")
    public void setSupplier_wh_id(Integer  supplier_wh_id){
        this.supplier_wh_id = supplier_wh_id ;
        this.supplier_wh_idDirtyFlag = true ;
    }

     /**
     * 获取 [供应仓库]脏标记
     */
    @JsonIgnore
    public boolean getSupplier_wh_idDirtyFlag(){
        return this.supplier_wh_idDirtyFlag ;
    }   

    /**
     * 获取 [供应仓库]
     */
    @JsonProperty("supplier_wh_id_text")
    public String getSupplier_wh_id_text(){
        return this.supplier_wh_id_text ;
    }

    /**
     * 设置 [供应仓库]
     */
    @JsonProperty("supplier_wh_id_text")
    public void setSupplier_wh_id_text(String  supplier_wh_id_text){
        this.supplier_wh_id_text = supplier_wh_id_text ;
        this.supplier_wh_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [供应仓库]脏标记
     */
    @JsonIgnore
    public boolean getSupplier_wh_id_textDirtyFlag(){
        return this.supplier_wh_id_textDirtyFlag ;
    }   

    /**
     * 获取 [仓库]
     */
    @JsonProperty("warehouse_ids")
    public String getWarehouse_ids(){
        return this.warehouse_ids ;
    }

    /**
     * 设置 [仓库]
     */
    @JsonProperty("warehouse_ids")
    public void setWarehouse_ids(String  warehouse_ids){
        this.warehouse_ids = warehouse_ids ;
        this.warehouse_idsDirtyFlag = true ;
    }

     /**
     * 获取 [仓库]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_idsDirtyFlag(){
        return this.warehouse_idsDirtyFlag ;
    }   

    /**
     * 获取 [可应用于仓库]
     */
    @JsonProperty("warehouse_selectable")
    public String getWarehouse_selectable(){
        return this.warehouse_selectable ;
    }

    /**
     * 设置 [可应用于仓库]
     */
    @JsonProperty("warehouse_selectable")
    public void setWarehouse_selectable(String  warehouse_selectable){
        this.warehouse_selectable = warehouse_selectable ;
        this.warehouse_selectableDirtyFlag = true ;
    }

     /**
     * 获取 [可应用于仓库]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_selectableDirtyFlag(){
        return this.warehouse_selectableDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
