package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_inventory_line;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_inventory_lineClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_inventory_lineImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_inventory_lineFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_inventory_line] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_inventory_lineClientServiceImpl implements Istock_inventory_lineClientService {

    stock_inventory_lineFeignClient stock_inventory_lineFeignClient;

    @Autowired
    public stock_inventory_lineClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_inventory_lineFeignClient = nameBuilder.target(stock_inventory_lineFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_inventory_lineFeignClient = nameBuilder.target(stock_inventory_lineFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_inventory_line createModel() {
		return new stock_inventory_lineImpl();
	}


    public void updateBatch(List<Istock_inventory_line> stock_inventory_lines){
        if(stock_inventory_lines!=null){
            List<stock_inventory_lineImpl> list = new ArrayList<stock_inventory_lineImpl>();
            for(Istock_inventory_line istock_inventory_line :stock_inventory_lines){
                list.add((stock_inventory_lineImpl)istock_inventory_line) ;
            }
            stock_inventory_lineFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Istock_inventory_line> stock_inventory_lines){
        if(stock_inventory_lines!=null){
            List<stock_inventory_lineImpl> list = new ArrayList<stock_inventory_lineImpl>();
            for(Istock_inventory_line istock_inventory_line :stock_inventory_lines){
                list.add((stock_inventory_lineImpl)istock_inventory_line) ;
            }
            stock_inventory_lineFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Istock_inventory_line> stock_inventory_lines){
        if(stock_inventory_lines!=null){
            List<stock_inventory_lineImpl> list = new ArrayList<stock_inventory_lineImpl>();
            for(Istock_inventory_line istock_inventory_line :stock_inventory_lines){
                list.add((stock_inventory_lineImpl)istock_inventory_line) ;
            }
            stock_inventory_lineFeignClient.createBatch(list) ;
        }
    }


    public void update(Istock_inventory_line stock_inventory_line){
        Istock_inventory_line clientModel = stock_inventory_lineFeignClient.update(stock_inventory_line.getId(),(stock_inventory_lineImpl)stock_inventory_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_inventory_line.getClass(), false);
        copier.copy(clientModel, stock_inventory_line, null);
    }


    public void create(Istock_inventory_line stock_inventory_line){
        Istock_inventory_line clientModel = stock_inventory_lineFeignClient.create((stock_inventory_lineImpl)stock_inventory_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_inventory_line.getClass(), false);
        copier.copy(clientModel, stock_inventory_line, null);
    }


    public Page<Istock_inventory_line> fetchDefault(SearchContext context){
        Page<stock_inventory_lineImpl> page = this.stock_inventory_lineFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Istock_inventory_line stock_inventory_line){
        stock_inventory_lineFeignClient.remove(stock_inventory_line.getId()) ;
    }


    public void get(Istock_inventory_line stock_inventory_line){
        Istock_inventory_line clientModel = stock_inventory_lineFeignClient.get(stock_inventory_line.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_inventory_line.getClass(), false);
        copier.copy(clientModel, stock_inventory_line, null);
    }


    public Page<Istock_inventory_line> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_inventory_line stock_inventory_line){
        Istock_inventory_line clientModel = stock_inventory_lineFeignClient.getDraft(stock_inventory_line.getId(),(stock_inventory_lineImpl)stock_inventory_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_inventory_line.getClass(), false);
        copier.copy(clientModel, stock_inventory_line, null);
    }



}

