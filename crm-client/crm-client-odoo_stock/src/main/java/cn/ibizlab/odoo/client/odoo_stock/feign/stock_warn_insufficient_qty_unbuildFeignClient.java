package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_warn_insufficient_qty_unbuild;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_warn_insufficient_qty_unbuildImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_warn_insufficient_qty_unbuild] 服务对象接口
 */
public interface stock_warn_insufficient_qty_unbuildFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_warn_insufficient_qty_unbuilds/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_warn_insufficient_qty_unbuilds/updatebatch")
    public stock_warn_insufficient_qty_unbuildImpl updateBatch(@RequestBody List<stock_warn_insufficient_qty_unbuildImpl> stock_warn_insufficient_qty_unbuilds);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warn_insufficient_qty_unbuilds/{id}")
    public stock_warn_insufficient_qty_unbuildImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_warn_insufficient_qty_unbuilds/removebatch")
    public stock_warn_insufficient_qty_unbuildImpl removeBatch(@RequestBody List<stock_warn_insufficient_qty_unbuildImpl> stock_warn_insufficient_qty_unbuilds);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_warn_insufficient_qty_unbuilds")
    public stock_warn_insufficient_qty_unbuildImpl create(@RequestBody stock_warn_insufficient_qty_unbuildImpl stock_warn_insufficient_qty_unbuild);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warn_insufficient_qty_unbuilds/fetchdefault")
    public Page<stock_warn_insufficient_qty_unbuildImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_warn_insufficient_qty_unbuilds/createbatch")
    public stock_warn_insufficient_qty_unbuildImpl createBatch(@RequestBody List<stock_warn_insufficient_qty_unbuildImpl> stock_warn_insufficient_qty_unbuilds);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_warn_insufficient_qty_unbuilds/{id}")
    public stock_warn_insufficient_qty_unbuildImpl update(@PathVariable("id") Integer id,@RequestBody stock_warn_insufficient_qty_unbuildImpl stock_warn_insufficient_qty_unbuild);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warn_insufficient_qty_unbuilds/select")
    public Page<stock_warn_insufficient_qty_unbuildImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warn_insufficient_qty_unbuilds/{id}/getdraft")
    public stock_warn_insufficient_qty_unbuildImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_warn_insufficient_qty_unbuildImpl stock_warn_insufficient_qty_unbuild);



}
