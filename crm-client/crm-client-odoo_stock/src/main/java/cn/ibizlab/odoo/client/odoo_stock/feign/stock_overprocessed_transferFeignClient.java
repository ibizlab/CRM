package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_overprocessed_transfer;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_overprocessed_transferImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_overprocessed_transfer] 服务对象接口
 */
public interface stock_overprocessed_transferFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_overprocessed_transfers")
    public stock_overprocessed_transferImpl create(@RequestBody stock_overprocessed_transferImpl stock_overprocessed_transfer);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_overprocessed_transfers/{id}")
    public stock_overprocessed_transferImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_overprocessed_transfers/removebatch")
    public stock_overprocessed_transferImpl removeBatch(@RequestBody List<stock_overprocessed_transferImpl> stock_overprocessed_transfers);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_overprocessed_transfers/{id}")
    public stock_overprocessed_transferImpl update(@PathVariable("id") Integer id,@RequestBody stock_overprocessed_transferImpl stock_overprocessed_transfer);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_overprocessed_transfers/updatebatch")
    public stock_overprocessed_transferImpl updateBatch(@RequestBody List<stock_overprocessed_transferImpl> stock_overprocessed_transfers);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_overprocessed_transfers/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_overprocessed_transfers/createbatch")
    public stock_overprocessed_transferImpl createBatch(@RequestBody List<stock_overprocessed_transferImpl> stock_overprocessed_transfers);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_overprocessed_transfers/fetchdefault")
    public Page<stock_overprocessed_transferImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_overprocessed_transfers/select")
    public Page<stock_overprocessed_transferImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_overprocessed_transfers/{id}/getdraft")
    public stock_overprocessed_transferImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_overprocessed_transferImpl stock_overprocessed_transfer);



}
