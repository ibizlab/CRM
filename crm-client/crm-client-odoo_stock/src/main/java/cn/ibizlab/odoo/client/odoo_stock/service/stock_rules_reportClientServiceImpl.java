package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_rules_report;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_rules_reportClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_rules_reportImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_rules_reportFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_rules_report] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_rules_reportClientServiceImpl implements Istock_rules_reportClientService {

    stock_rules_reportFeignClient stock_rules_reportFeignClient;

    @Autowired
    public stock_rules_reportClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_rules_reportFeignClient = nameBuilder.target(stock_rules_reportFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_rules_reportFeignClient = nameBuilder.target(stock_rules_reportFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_rules_report createModel() {
		return new stock_rules_reportImpl();
	}


    public Page<Istock_rules_report> fetchDefault(SearchContext context){
        Page<stock_rules_reportImpl> page = this.stock_rules_reportFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Istock_rules_report> stock_rules_reports){
        if(stock_rules_reports!=null){
            List<stock_rules_reportImpl> list = new ArrayList<stock_rules_reportImpl>();
            for(Istock_rules_report istock_rules_report :stock_rules_reports){
                list.add((stock_rules_reportImpl)istock_rules_report) ;
            }
            stock_rules_reportFeignClient.createBatch(list) ;
        }
    }


    public void update(Istock_rules_report stock_rules_report){
        Istock_rules_report clientModel = stock_rules_reportFeignClient.update(stock_rules_report.getId(),(stock_rules_reportImpl)stock_rules_report) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_rules_report.getClass(), false);
        copier.copy(clientModel, stock_rules_report, null);
    }


    public void removeBatch(List<Istock_rules_report> stock_rules_reports){
        if(stock_rules_reports!=null){
            List<stock_rules_reportImpl> list = new ArrayList<stock_rules_reportImpl>();
            for(Istock_rules_report istock_rules_report :stock_rules_reports){
                list.add((stock_rules_reportImpl)istock_rules_report) ;
            }
            stock_rules_reportFeignClient.removeBatch(list) ;
        }
    }


    public void get(Istock_rules_report stock_rules_report){
        Istock_rules_report clientModel = stock_rules_reportFeignClient.get(stock_rules_report.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_rules_report.getClass(), false);
        copier.copy(clientModel, stock_rules_report, null);
    }


    public void updateBatch(List<Istock_rules_report> stock_rules_reports){
        if(stock_rules_reports!=null){
            List<stock_rules_reportImpl> list = new ArrayList<stock_rules_reportImpl>();
            for(Istock_rules_report istock_rules_report :stock_rules_reports){
                list.add((stock_rules_reportImpl)istock_rules_report) ;
            }
            stock_rules_reportFeignClient.updateBatch(list) ;
        }
    }


    public void create(Istock_rules_report stock_rules_report){
        Istock_rules_report clientModel = stock_rules_reportFeignClient.create((stock_rules_reportImpl)stock_rules_report) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_rules_report.getClass(), false);
        copier.copy(clientModel, stock_rules_report, null);
    }


    public void remove(Istock_rules_report stock_rules_report){
        stock_rules_reportFeignClient.remove(stock_rules_report.getId()) ;
    }


    public Page<Istock_rules_report> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_rules_report stock_rules_report){
        Istock_rules_report clientModel = stock_rules_reportFeignClient.getDraft(stock_rules_report.getId(),(stock_rules_reportImpl)stock_rules_report) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_rules_report.getClass(), false);
        copier.copy(clientModel, stock_rules_report, null);
    }



}

