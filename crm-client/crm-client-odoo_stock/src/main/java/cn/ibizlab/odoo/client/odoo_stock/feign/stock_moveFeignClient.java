package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_move;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_moveImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_move] 服务对象接口
 */
public interface stock_moveFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_moves/createbatch")
    public stock_moveImpl createBatch(@RequestBody List<stock_moveImpl> stock_moves);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_moves/updatebatch")
    public stock_moveImpl updateBatch(@RequestBody List<stock_moveImpl> stock_moves);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_moves/{id}")
    public stock_moveImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_moves")
    public stock_moveImpl create(@RequestBody stock_moveImpl stock_move);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_moves/{id}")
    public stock_moveImpl update(@PathVariable("id") Integer id,@RequestBody stock_moveImpl stock_move);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_moves/removebatch")
    public stock_moveImpl removeBatch(@RequestBody List<stock_moveImpl> stock_moves);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_moves/fetchdefault")
    public Page<stock_moveImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_moves/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_moves/select")
    public Page<stock_moveImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_moves/{id}/getdraft")
    public stock_moveImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_moveImpl stock_move);



}
