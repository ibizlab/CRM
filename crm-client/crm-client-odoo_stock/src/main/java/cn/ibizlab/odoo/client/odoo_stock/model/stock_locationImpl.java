package cn.ibizlab.odoo.client.odoo_stock.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Istock_location;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[stock_location] 对象
 */
public class stock_locationImpl implements Istock_location,Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 条码
     */
    public String barcode;

    @JsonIgnore
    public boolean barcodeDirtyFlag;
    
    /**
     * 包含
     */
    public String child_ids;

    @JsonIgnore
    public boolean child_idsDirtyFlag;
    
    /**
     * 额外的信息
     */
    public String comment;

    @JsonIgnore
    public boolean commentDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 完整的位置名称
     */
    public String complete_name;

    @JsonIgnore
    public boolean complete_nameDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 上级位置
     */
    public Integer location_id;

    @JsonIgnore
    public boolean location_idDirtyFlag;
    
    /**
     * 上级位置
     */
    public String location_id_text;

    @JsonIgnore
    public boolean location_id_textDirtyFlag;
    
    /**
     * 位置名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 父级路径
     */
    public String parent_path;

    @JsonIgnore
    public boolean parent_pathDirtyFlag;
    
    /**
     * 所有者
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 所有者
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 通道(X)
     */
    public Integer posx;

    @JsonIgnore
    public boolean posxDirtyFlag;
    
    /**
     * 货架(Y)
     */
    public Integer posy;

    @JsonIgnore
    public boolean posyDirtyFlag;
    
    /**
     * 高度(Z)
     */
    public Integer posz;

    @JsonIgnore
    public boolean poszDirtyFlag;
    
    /**
     * 上架策略
     */
    public Integer putaway_strategy_id;

    @JsonIgnore
    public boolean putaway_strategy_idDirtyFlag;
    
    /**
     * 上架策略
     */
    public String putaway_strategy_id_text;

    @JsonIgnore
    public boolean putaway_strategy_id_textDirtyFlag;
    
    /**
     * 即时库存
     */
    public String quant_ids;

    @JsonIgnore
    public boolean quant_idsDirtyFlag;
    
    /**
     * 下架策略
     */
    public Integer removal_strategy_id;

    @JsonIgnore
    public boolean removal_strategy_idDirtyFlag;
    
    /**
     * 下架策略
     */
    public String removal_strategy_id_text;

    @JsonIgnore
    public boolean removal_strategy_id_textDirtyFlag;
    
    /**
     * 是一个退回位置？
     */
    public String return_location;

    @JsonIgnore
    public boolean return_locationDirtyFlag;
    
    /**
     * 是一个报废位置？
     */
    public String scrap_location;

    @JsonIgnore
    public boolean scrap_locationDirtyFlag;
    
    /**
     * 位置类型
     */
    public String usage;

    @JsonIgnore
    public boolean usageDirtyFlag;
    
    /**
     * 库存计价科目（入向）
     */
    public Integer valuation_in_account_id;

    @JsonIgnore
    public boolean valuation_in_account_idDirtyFlag;
    
    /**
     * 库存计价科目（入向）
     */
    public String valuation_in_account_id_text;

    @JsonIgnore
    public boolean valuation_in_account_id_textDirtyFlag;
    
    /**
     * 库存计价科目（出向）
     */
    public Integer valuation_out_account_id;

    @JsonIgnore
    public boolean valuation_out_account_idDirtyFlag;
    
    /**
     * 库存计价科目（出向）
     */
    public String valuation_out_account_id_text;

    @JsonIgnore
    public boolean valuation_out_account_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [条码]
     */
    @JsonProperty("barcode")
    public String getBarcode(){
        return this.barcode ;
    }

    /**
     * 设置 [条码]
     */
    @JsonProperty("barcode")
    public void setBarcode(String  barcode){
        this.barcode = barcode ;
        this.barcodeDirtyFlag = true ;
    }

     /**
     * 获取 [条码]脏标记
     */
    @JsonIgnore
    public boolean getBarcodeDirtyFlag(){
        return this.barcodeDirtyFlag ;
    }   

    /**
     * 获取 [包含]
     */
    @JsonProperty("child_ids")
    public String getChild_ids(){
        return this.child_ids ;
    }

    /**
     * 设置 [包含]
     */
    @JsonProperty("child_ids")
    public void setChild_ids(String  child_ids){
        this.child_ids = child_ids ;
        this.child_idsDirtyFlag = true ;
    }

     /**
     * 获取 [包含]脏标记
     */
    @JsonIgnore
    public boolean getChild_idsDirtyFlag(){
        return this.child_idsDirtyFlag ;
    }   

    /**
     * 获取 [额外的信息]
     */
    @JsonProperty("comment")
    public String getComment(){
        return this.comment ;
    }

    /**
     * 设置 [额外的信息]
     */
    @JsonProperty("comment")
    public void setComment(String  comment){
        this.comment = comment ;
        this.commentDirtyFlag = true ;
    }

     /**
     * 获取 [额外的信息]脏标记
     */
    @JsonIgnore
    public boolean getCommentDirtyFlag(){
        return this.commentDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [完整的位置名称]
     */
    @JsonProperty("complete_name")
    public String getComplete_name(){
        return this.complete_name ;
    }

    /**
     * 设置 [完整的位置名称]
     */
    @JsonProperty("complete_name")
    public void setComplete_name(String  complete_name){
        this.complete_name = complete_name ;
        this.complete_nameDirtyFlag = true ;
    }

     /**
     * 获取 [完整的位置名称]脏标记
     */
    @JsonIgnore
    public boolean getComplete_nameDirtyFlag(){
        return this.complete_nameDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [上级位置]
     */
    @JsonProperty("location_id")
    public Integer getLocation_id(){
        return this.location_id ;
    }

    /**
     * 设置 [上级位置]
     */
    @JsonProperty("location_id")
    public void setLocation_id(Integer  location_id){
        this.location_id = location_id ;
        this.location_idDirtyFlag = true ;
    }

     /**
     * 获取 [上级位置]脏标记
     */
    @JsonIgnore
    public boolean getLocation_idDirtyFlag(){
        return this.location_idDirtyFlag ;
    }   

    /**
     * 获取 [上级位置]
     */
    @JsonProperty("location_id_text")
    public String getLocation_id_text(){
        return this.location_id_text ;
    }

    /**
     * 设置 [上级位置]
     */
    @JsonProperty("location_id_text")
    public void setLocation_id_text(String  location_id_text){
        this.location_id_text = location_id_text ;
        this.location_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [上级位置]脏标记
     */
    @JsonIgnore
    public boolean getLocation_id_textDirtyFlag(){
        return this.location_id_textDirtyFlag ;
    }   

    /**
     * 获取 [位置名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [位置名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [位置名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [父级路径]
     */
    @JsonProperty("parent_path")
    public String getParent_path(){
        return this.parent_path ;
    }

    /**
     * 设置 [父级路径]
     */
    @JsonProperty("parent_path")
    public void setParent_path(String  parent_path){
        this.parent_path = parent_path ;
        this.parent_pathDirtyFlag = true ;
    }

     /**
     * 获取 [父级路径]脏标记
     */
    @JsonIgnore
    public boolean getParent_pathDirtyFlag(){
        return this.parent_pathDirtyFlag ;
    }   

    /**
     * 获取 [所有者]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [所有者]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [所有者]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [所有者]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [所有者]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [所有者]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [通道(X)]
     */
    @JsonProperty("posx")
    public Integer getPosx(){
        return this.posx ;
    }

    /**
     * 设置 [通道(X)]
     */
    @JsonProperty("posx")
    public void setPosx(Integer  posx){
        this.posx = posx ;
        this.posxDirtyFlag = true ;
    }

     /**
     * 获取 [通道(X)]脏标记
     */
    @JsonIgnore
    public boolean getPosxDirtyFlag(){
        return this.posxDirtyFlag ;
    }   

    /**
     * 获取 [货架(Y)]
     */
    @JsonProperty("posy")
    public Integer getPosy(){
        return this.posy ;
    }

    /**
     * 设置 [货架(Y)]
     */
    @JsonProperty("posy")
    public void setPosy(Integer  posy){
        this.posy = posy ;
        this.posyDirtyFlag = true ;
    }

     /**
     * 获取 [货架(Y)]脏标记
     */
    @JsonIgnore
    public boolean getPosyDirtyFlag(){
        return this.posyDirtyFlag ;
    }   

    /**
     * 获取 [高度(Z)]
     */
    @JsonProperty("posz")
    public Integer getPosz(){
        return this.posz ;
    }

    /**
     * 设置 [高度(Z)]
     */
    @JsonProperty("posz")
    public void setPosz(Integer  posz){
        this.posz = posz ;
        this.poszDirtyFlag = true ;
    }

     /**
     * 获取 [高度(Z)]脏标记
     */
    @JsonIgnore
    public boolean getPoszDirtyFlag(){
        return this.poszDirtyFlag ;
    }   

    /**
     * 获取 [上架策略]
     */
    @JsonProperty("putaway_strategy_id")
    public Integer getPutaway_strategy_id(){
        return this.putaway_strategy_id ;
    }

    /**
     * 设置 [上架策略]
     */
    @JsonProperty("putaway_strategy_id")
    public void setPutaway_strategy_id(Integer  putaway_strategy_id){
        this.putaway_strategy_id = putaway_strategy_id ;
        this.putaway_strategy_idDirtyFlag = true ;
    }

     /**
     * 获取 [上架策略]脏标记
     */
    @JsonIgnore
    public boolean getPutaway_strategy_idDirtyFlag(){
        return this.putaway_strategy_idDirtyFlag ;
    }   

    /**
     * 获取 [上架策略]
     */
    @JsonProperty("putaway_strategy_id_text")
    public String getPutaway_strategy_id_text(){
        return this.putaway_strategy_id_text ;
    }

    /**
     * 设置 [上架策略]
     */
    @JsonProperty("putaway_strategy_id_text")
    public void setPutaway_strategy_id_text(String  putaway_strategy_id_text){
        this.putaway_strategy_id_text = putaway_strategy_id_text ;
        this.putaway_strategy_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [上架策略]脏标记
     */
    @JsonIgnore
    public boolean getPutaway_strategy_id_textDirtyFlag(){
        return this.putaway_strategy_id_textDirtyFlag ;
    }   

    /**
     * 获取 [即时库存]
     */
    @JsonProperty("quant_ids")
    public String getQuant_ids(){
        return this.quant_ids ;
    }

    /**
     * 设置 [即时库存]
     */
    @JsonProperty("quant_ids")
    public void setQuant_ids(String  quant_ids){
        this.quant_ids = quant_ids ;
        this.quant_idsDirtyFlag = true ;
    }

     /**
     * 获取 [即时库存]脏标记
     */
    @JsonIgnore
    public boolean getQuant_idsDirtyFlag(){
        return this.quant_idsDirtyFlag ;
    }   

    /**
     * 获取 [下架策略]
     */
    @JsonProperty("removal_strategy_id")
    public Integer getRemoval_strategy_id(){
        return this.removal_strategy_id ;
    }

    /**
     * 设置 [下架策略]
     */
    @JsonProperty("removal_strategy_id")
    public void setRemoval_strategy_id(Integer  removal_strategy_id){
        this.removal_strategy_id = removal_strategy_id ;
        this.removal_strategy_idDirtyFlag = true ;
    }

     /**
     * 获取 [下架策略]脏标记
     */
    @JsonIgnore
    public boolean getRemoval_strategy_idDirtyFlag(){
        return this.removal_strategy_idDirtyFlag ;
    }   

    /**
     * 获取 [下架策略]
     */
    @JsonProperty("removal_strategy_id_text")
    public String getRemoval_strategy_id_text(){
        return this.removal_strategy_id_text ;
    }

    /**
     * 设置 [下架策略]
     */
    @JsonProperty("removal_strategy_id_text")
    public void setRemoval_strategy_id_text(String  removal_strategy_id_text){
        this.removal_strategy_id_text = removal_strategy_id_text ;
        this.removal_strategy_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [下架策略]脏标记
     */
    @JsonIgnore
    public boolean getRemoval_strategy_id_textDirtyFlag(){
        return this.removal_strategy_id_textDirtyFlag ;
    }   

    /**
     * 获取 [是一个退回位置？]
     */
    @JsonProperty("return_location")
    public String getReturn_location(){
        return this.return_location ;
    }

    /**
     * 设置 [是一个退回位置？]
     */
    @JsonProperty("return_location")
    public void setReturn_location(String  return_location){
        this.return_location = return_location ;
        this.return_locationDirtyFlag = true ;
    }

     /**
     * 获取 [是一个退回位置？]脏标记
     */
    @JsonIgnore
    public boolean getReturn_locationDirtyFlag(){
        return this.return_locationDirtyFlag ;
    }   

    /**
     * 获取 [是一个报废位置？]
     */
    @JsonProperty("scrap_location")
    public String getScrap_location(){
        return this.scrap_location ;
    }

    /**
     * 设置 [是一个报废位置？]
     */
    @JsonProperty("scrap_location")
    public void setScrap_location(String  scrap_location){
        this.scrap_location = scrap_location ;
        this.scrap_locationDirtyFlag = true ;
    }

     /**
     * 获取 [是一个报废位置？]脏标记
     */
    @JsonIgnore
    public boolean getScrap_locationDirtyFlag(){
        return this.scrap_locationDirtyFlag ;
    }   

    /**
     * 获取 [位置类型]
     */
    @JsonProperty("usage")
    public String getUsage(){
        return this.usage ;
    }

    /**
     * 设置 [位置类型]
     */
    @JsonProperty("usage")
    public void setUsage(String  usage){
        this.usage = usage ;
        this.usageDirtyFlag = true ;
    }

     /**
     * 获取 [位置类型]脏标记
     */
    @JsonIgnore
    public boolean getUsageDirtyFlag(){
        return this.usageDirtyFlag ;
    }   

    /**
     * 获取 [库存计价科目（入向）]
     */
    @JsonProperty("valuation_in_account_id")
    public Integer getValuation_in_account_id(){
        return this.valuation_in_account_id ;
    }

    /**
     * 设置 [库存计价科目（入向）]
     */
    @JsonProperty("valuation_in_account_id")
    public void setValuation_in_account_id(Integer  valuation_in_account_id){
        this.valuation_in_account_id = valuation_in_account_id ;
        this.valuation_in_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [库存计价科目（入向）]脏标记
     */
    @JsonIgnore
    public boolean getValuation_in_account_idDirtyFlag(){
        return this.valuation_in_account_idDirtyFlag ;
    }   

    /**
     * 获取 [库存计价科目（入向）]
     */
    @JsonProperty("valuation_in_account_id_text")
    public String getValuation_in_account_id_text(){
        return this.valuation_in_account_id_text ;
    }

    /**
     * 设置 [库存计价科目（入向）]
     */
    @JsonProperty("valuation_in_account_id_text")
    public void setValuation_in_account_id_text(String  valuation_in_account_id_text){
        this.valuation_in_account_id_text = valuation_in_account_id_text ;
        this.valuation_in_account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [库存计价科目（入向）]脏标记
     */
    @JsonIgnore
    public boolean getValuation_in_account_id_textDirtyFlag(){
        return this.valuation_in_account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [库存计价科目（出向）]
     */
    @JsonProperty("valuation_out_account_id")
    public Integer getValuation_out_account_id(){
        return this.valuation_out_account_id ;
    }

    /**
     * 设置 [库存计价科目（出向）]
     */
    @JsonProperty("valuation_out_account_id")
    public void setValuation_out_account_id(Integer  valuation_out_account_id){
        this.valuation_out_account_id = valuation_out_account_id ;
        this.valuation_out_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [库存计价科目（出向）]脏标记
     */
    @JsonIgnore
    public boolean getValuation_out_account_idDirtyFlag(){
        return this.valuation_out_account_idDirtyFlag ;
    }   

    /**
     * 获取 [库存计价科目（出向）]
     */
    @JsonProperty("valuation_out_account_id_text")
    public String getValuation_out_account_id_text(){
        return this.valuation_out_account_id_text ;
    }

    /**
     * 设置 [库存计价科目（出向）]
     */
    @JsonProperty("valuation_out_account_id_text")
    public void setValuation_out_account_id_text(String  valuation_out_account_id_text){
        this.valuation_out_account_id_text = valuation_out_account_id_text ;
        this.valuation_out_account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [库存计价科目（出向）]脏标记
     */
    @JsonIgnore
    public boolean getValuation_out_account_id_textDirtyFlag(){
        return this.valuation_out_account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
