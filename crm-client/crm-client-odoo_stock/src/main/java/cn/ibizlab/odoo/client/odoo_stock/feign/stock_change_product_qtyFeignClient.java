package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_change_product_qty;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_change_product_qtyImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_change_product_qty] 服务对象接口
 */
public interface stock_change_product_qtyFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_change_product_qties/createbatch")
    public stock_change_product_qtyImpl createBatch(@RequestBody List<stock_change_product_qtyImpl> stock_change_product_qties);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_change_product_qties/removebatch")
    public stock_change_product_qtyImpl removeBatch(@RequestBody List<stock_change_product_qtyImpl> stock_change_product_qties);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_change_product_qties/{id}")
    public stock_change_product_qtyImpl update(@PathVariable("id") Integer id,@RequestBody stock_change_product_qtyImpl stock_change_product_qty);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_change_product_qties/fetchdefault")
    public Page<stock_change_product_qtyImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_change_product_qties/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_change_product_qties/updatebatch")
    public stock_change_product_qtyImpl updateBatch(@RequestBody List<stock_change_product_qtyImpl> stock_change_product_qties);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_change_product_qties/{id}")
    public stock_change_product_qtyImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_change_product_qties")
    public stock_change_product_qtyImpl create(@RequestBody stock_change_product_qtyImpl stock_change_product_qty);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_change_product_qties/select")
    public Page<stock_change_product_qtyImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_change_product_qties/{id}/getdraft")
    public stock_change_product_qtyImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_change_product_qtyImpl stock_change_product_qty);



}
