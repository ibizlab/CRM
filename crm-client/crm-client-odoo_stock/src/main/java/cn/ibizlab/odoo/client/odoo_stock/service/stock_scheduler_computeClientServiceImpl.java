package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_scheduler_compute;
import cn.ibizlab.odoo.client.odoo_stock.config.odoo_stockClientProperties;
import cn.ibizlab.odoo.core.client.service.Istock_scheduler_computeClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_scheduler_computeImpl;
import cn.ibizlab.odoo.client.odoo_stock.feign.stock_scheduler_computeFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[stock_scheduler_compute] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class stock_scheduler_computeClientServiceImpl implements Istock_scheduler_computeClientService {

    stock_scheduler_computeFeignClient stock_scheduler_computeFeignClient;

    @Autowired
    public stock_scheduler_computeClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_stockClientProperties odoo_stockClientProperties) {
        if (odoo_stockClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_scheduler_computeFeignClient = nameBuilder.target(stock_scheduler_computeFeignClient.class,"http://"+odoo_stockClientProperties.getServiceId()+"/") ;
		}else if (odoo_stockClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.stock_scheduler_computeFeignClient = nameBuilder.target(stock_scheduler_computeFeignClient.class,odoo_stockClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Istock_scheduler_compute createModel() {
		return new stock_scheduler_computeImpl();
	}


    public Page<Istock_scheduler_compute> fetchDefault(SearchContext context){
        Page<stock_scheduler_computeImpl> page = this.stock_scheduler_computeFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Istock_scheduler_compute> stock_scheduler_computes){
        if(stock_scheduler_computes!=null){
            List<stock_scheduler_computeImpl> list = new ArrayList<stock_scheduler_computeImpl>();
            for(Istock_scheduler_compute istock_scheduler_compute :stock_scheduler_computes){
                list.add((stock_scheduler_computeImpl)istock_scheduler_compute) ;
            }
            stock_scheduler_computeFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Istock_scheduler_compute> stock_scheduler_computes){
        if(stock_scheduler_computes!=null){
            List<stock_scheduler_computeImpl> list = new ArrayList<stock_scheduler_computeImpl>();
            for(Istock_scheduler_compute istock_scheduler_compute :stock_scheduler_computes){
                list.add((stock_scheduler_computeImpl)istock_scheduler_compute) ;
            }
            stock_scheduler_computeFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Istock_scheduler_compute> stock_scheduler_computes){
        if(stock_scheduler_computes!=null){
            List<stock_scheduler_computeImpl> list = new ArrayList<stock_scheduler_computeImpl>();
            for(Istock_scheduler_compute istock_scheduler_compute :stock_scheduler_computes){
                list.add((stock_scheduler_computeImpl)istock_scheduler_compute) ;
            }
            stock_scheduler_computeFeignClient.createBatch(list) ;
        }
    }


    public void create(Istock_scheduler_compute stock_scheduler_compute){
        Istock_scheduler_compute clientModel = stock_scheduler_computeFeignClient.create((stock_scheduler_computeImpl)stock_scheduler_compute) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_scheduler_compute.getClass(), false);
        copier.copy(clientModel, stock_scheduler_compute, null);
    }


    public void get(Istock_scheduler_compute stock_scheduler_compute){
        Istock_scheduler_compute clientModel = stock_scheduler_computeFeignClient.get(stock_scheduler_compute.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_scheduler_compute.getClass(), false);
        copier.copy(clientModel, stock_scheduler_compute, null);
    }


    public void remove(Istock_scheduler_compute stock_scheduler_compute){
        stock_scheduler_computeFeignClient.remove(stock_scheduler_compute.getId()) ;
    }


    public void update(Istock_scheduler_compute stock_scheduler_compute){
        Istock_scheduler_compute clientModel = stock_scheduler_computeFeignClient.update(stock_scheduler_compute.getId(),(stock_scheduler_computeImpl)stock_scheduler_compute) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_scheduler_compute.getClass(), false);
        copier.copy(clientModel, stock_scheduler_compute, null);
    }


    public Page<Istock_scheduler_compute> select(SearchContext context){
        return null ;
    }


    public void getDraft(Istock_scheduler_compute stock_scheduler_compute){
        Istock_scheduler_compute clientModel = stock_scheduler_computeFeignClient.getDraft(stock_scheduler_compute.getId(),(stock_scheduler_computeImpl)stock_scheduler_compute) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), stock_scheduler_compute.getClass(), false);
        copier.copy(clientModel, stock_scheduler_compute, null);
    }



}

