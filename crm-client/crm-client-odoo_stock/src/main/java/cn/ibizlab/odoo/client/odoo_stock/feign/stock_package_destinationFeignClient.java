package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_package_destination;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_package_destinationImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_package_destination] 服务对象接口
 */
public interface stock_package_destinationFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_package_destinations/fetchdefault")
    public Page<stock_package_destinationImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_package_destinations/{id}")
    public stock_package_destinationImpl update(@PathVariable("id") Integer id,@RequestBody stock_package_destinationImpl stock_package_destination);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_package_destinations/removebatch")
    public stock_package_destinationImpl removeBatch(@RequestBody List<stock_package_destinationImpl> stock_package_destinations);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_package_destinations/{id}")
    public stock_package_destinationImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_package_destinations")
    public stock_package_destinationImpl create(@RequestBody stock_package_destinationImpl stock_package_destination);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_package_destinations/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_package_destinations/updatebatch")
    public stock_package_destinationImpl updateBatch(@RequestBody List<stock_package_destinationImpl> stock_package_destinations);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_package_destinations/createbatch")
    public stock_package_destinationImpl createBatch(@RequestBody List<stock_package_destinationImpl> stock_package_destinations);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_package_destinations/select")
    public Page<stock_package_destinationImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_package_destinations/{id}/getdraft")
    public stock_package_destinationImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_package_destinationImpl stock_package_destination);



}
