package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_traceability_report;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_traceability_reportImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_traceability_report] 服务对象接口
 */
public interface stock_traceability_reportFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_traceability_reports")
    public stock_traceability_reportImpl create(@RequestBody stock_traceability_reportImpl stock_traceability_report);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_traceability_reports/{id}")
    public stock_traceability_reportImpl update(@PathVariable("id") Integer id,@RequestBody stock_traceability_reportImpl stock_traceability_report);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_traceability_reports/removebatch")
    public stock_traceability_reportImpl removeBatch(@RequestBody List<stock_traceability_reportImpl> stock_traceability_reports);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_traceability_reports/createbatch")
    public stock_traceability_reportImpl createBatch(@RequestBody List<stock_traceability_reportImpl> stock_traceability_reports);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_traceability_reports/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_traceability_reports/updatebatch")
    public stock_traceability_reportImpl updateBatch(@RequestBody List<stock_traceability_reportImpl> stock_traceability_reports);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_traceability_reports/{id}")
    public stock_traceability_reportImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_traceability_reports/fetchdefault")
    public Page<stock_traceability_reportImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_traceability_reports/select")
    public Page<stock_traceability_reportImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_traceability_reports/{id}/getdraft")
    public stock_traceability_reportImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_traceability_reportImpl stock_traceability_report);



}
