package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_warn_insufficient_qty_scrap;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_warn_insufficient_qty_scrapImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_warn_insufficient_qty_scrap] 服务对象接口
 */
public interface stock_warn_insufficient_qty_scrapFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warn_insufficient_qty_scraps/fetchdefault")
    public Page<stock_warn_insufficient_qty_scrapImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_warn_insufficient_qty_scraps/updatebatch")
    public stock_warn_insufficient_qty_scrapImpl updateBatch(@RequestBody List<stock_warn_insufficient_qty_scrapImpl> stock_warn_insufficient_qty_scraps);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_warn_insufficient_qty_scraps")
    public stock_warn_insufficient_qty_scrapImpl create(@RequestBody stock_warn_insufficient_qty_scrapImpl stock_warn_insufficient_qty_scrap);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_warn_insufficient_qty_scraps/createbatch")
    public stock_warn_insufficient_qty_scrapImpl createBatch(@RequestBody List<stock_warn_insufficient_qty_scrapImpl> stock_warn_insufficient_qty_scraps);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_warn_insufficient_qty_scraps/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_warn_insufficient_qty_scraps/removebatch")
    public stock_warn_insufficient_qty_scrapImpl removeBatch(@RequestBody List<stock_warn_insufficient_qty_scrapImpl> stock_warn_insufficient_qty_scraps);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_warn_insufficient_qty_scraps/{id}")
    public stock_warn_insufficient_qty_scrapImpl update(@PathVariable("id") Integer id,@RequestBody stock_warn_insufficient_qty_scrapImpl stock_warn_insufficient_qty_scrap);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warn_insufficient_qty_scraps/{id}")
    public stock_warn_insufficient_qty_scrapImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warn_insufficient_qty_scraps/select")
    public Page<stock_warn_insufficient_qty_scrapImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warn_insufficient_qty_scraps/{id}/getdraft")
    public stock_warn_insufficient_qty_scrapImpl getDraft(@PathVariable("id") Integer id,@RequestBody stock_warn_insufficient_qty_scrapImpl stock_warn_insufficient_qty_scrap);



}
