package cn.ibizlab.odoo.client.odoo_gamification.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Igamification_challenge_line;
import cn.ibizlab.odoo.client.odoo_gamification.model.gamification_challenge_lineImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[gamification_challenge_line] 服务对象接口
 */
public interface gamification_challenge_lineFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_gamification/gamification_challenge_lines")
    public gamification_challenge_lineImpl create(@RequestBody gamification_challenge_lineImpl gamification_challenge_line);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_gamification/gamification_challenge_lines/{id}")
    public gamification_challenge_lineImpl update(@PathVariable("id") Integer id,@RequestBody gamification_challenge_lineImpl gamification_challenge_line);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_gamification/gamification_challenge_lines/updatebatch")
    public gamification_challenge_lineImpl updateBatch(@RequestBody List<gamification_challenge_lineImpl> gamification_challenge_lines);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_gamification/gamification_challenge_lines/removebatch")
    public gamification_challenge_lineImpl removeBatch(@RequestBody List<gamification_challenge_lineImpl> gamification_challenge_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_challenge_lines/fetchdefault")
    public Page<gamification_challenge_lineImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_challenge_lines/{id}")
    public gamification_challenge_lineImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_gamification/gamification_challenge_lines/createbatch")
    public gamification_challenge_lineImpl createBatch(@RequestBody List<gamification_challenge_lineImpl> gamification_challenge_lines);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_gamification/gamification_challenge_lines/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_challenge_lines/select")
    public Page<gamification_challenge_lineImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_challenge_lines/{id}/getdraft")
    public gamification_challenge_lineImpl getDraft(@PathVariable("id") Integer id,@RequestBody gamification_challenge_lineImpl gamification_challenge_line);



}
