package cn.ibizlab.odoo.client.odoo_gamification.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Igamification_challenge_line;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[gamification_challenge_line] 对象
 */
public class gamification_challenge_lineImpl implements Igamification_challenge_line,Serializable{

    /**
     * 挑战
     */
    public Integer challenge_id;

    @JsonIgnore
    public boolean challenge_idDirtyFlag;
    
    /**
     * 挑战
     */
    public String challenge_id_text;

    @JsonIgnore
    public boolean challenge_id_textDirtyFlag;
    
    /**
     * 目标绩效
     */
    public String condition;

    @JsonIgnore
    public boolean conditionDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 后缀
     */
    public String definition_full_suffix;

    @JsonIgnore
    public boolean definition_full_suffixDirtyFlag;
    
    /**
     * 目标定义
     */
    public Integer definition_id;

    @JsonIgnore
    public boolean definition_idDirtyFlag;
    
    /**
     * 货币
     */
    public String definition_monetary;

    @JsonIgnore
    public boolean definition_monetaryDirtyFlag;
    
    /**
     * 单位
     */
    public String definition_suffix;

    @JsonIgnore
    public boolean definition_suffixDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 要达到的目标值
     */
    public Double target_goal;

    @JsonIgnore
    public boolean target_goalDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [挑战]
     */
    @JsonProperty("challenge_id")
    public Integer getChallenge_id(){
        return this.challenge_id ;
    }

    /**
     * 设置 [挑战]
     */
    @JsonProperty("challenge_id")
    public void setChallenge_id(Integer  challenge_id){
        this.challenge_id = challenge_id ;
        this.challenge_idDirtyFlag = true ;
    }

     /**
     * 获取 [挑战]脏标记
     */
    @JsonIgnore
    public boolean getChallenge_idDirtyFlag(){
        return this.challenge_idDirtyFlag ;
    }   

    /**
     * 获取 [挑战]
     */
    @JsonProperty("challenge_id_text")
    public String getChallenge_id_text(){
        return this.challenge_id_text ;
    }

    /**
     * 设置 [挑战]
     */
    @JsonProperty("challenge_id_text")
    public void setChallenge_id_text(String  challenge_id_text){
        this.challenge_id_text = challenge_id_text ;
        this.challenge_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [挑战]脏标记
     */
    @JsonIgnore
    public boolean getChallenge_id_textDirtyFlag(){
        return this.challenge_id_textDirtyFlag ;
    }   

    /**
     * 获取 [目标绩效]
     */
    @JsonProperty("condition")
    public String getCondition(){
        return this.condition ;
    }

    /**
     * 设置 [目标绩效]
     */
    @JsonProperty("condition")
    public void setCondition(String  condition){
        this.condition = condition ;
        this.conditionDirtyFlag = true ;
    }

     /**
     * 获取 [目标绩效]脏标记
     */
    @JsonIgnore
    public boolean getConditionDirtyFlag(){
        return this.conditionDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [后缀]
     */
    @JsonProperty("definition_full_suffix")
    public String getDefinition_full_suffix(){
        return this.definition_full_suffix ;
    }

    /**
     * 设置 [后缀]
     */
    @JsonProperty("definition_full_suffix")
    public void setDefinition_full_suffix(String  definition_full_suffix){
        this.definition_full_suffix = definition_full_suffix ;
        this.definition_full_suffixDirtyFlag = true ;
    }

     /**
     * 获取 [后缀]脏标记
     */
    @JsonIgnore
    public boolean getDefinition_full_suffixDirtyFlag(){
        return this.definition_full_suffixDirtyFlag ;
    }   

    /**
     * 获取 [目标定义]
     */
    @JsonProperty("definition_id")
    public Integer getDefinition_id(){
        return this.definition_id ;
    }

    /**
     * 设置 [目标定义]
     */
    @JsonProperty("definition_id")
    public void setDefinition_id(Integer  definition_id){
        this.definition_id = definition_id ;
        this.definition_idDirtyFlag = true ;
    }

     /**
     * 获取 [目标定义]脏标记
     */
    @JsonIgnore
    public boolean getDefinition_idDirtyFlag(){
        return this.definition_idDirtyFlag ;
    }   

    /**
     * 获取 [货币]
     */
    @JsonProperty("definition_monetary")
    public String getDefinition_monetary(){
        return this.definition_monetary ;
    }

    /**
     * 设置 [货币]
     */
    @JsonProperty("definition_monetary")
    public void setDefinition_monetary(String  definition_monetary){
        this.definition_monetary = definition_monetary ;
        this.definition_monetaryDirtyFlag = true ;
    }

     /**
     * 获取 [货币]脏标记
     */
    @JsonIgnore
    public boolean getDefinition_monetaryDirtyFlag(){
        return this.definition_monetaryDirtyFlag ;
    }   

    /**
     * 获取 [单位]
     */
    @JsonProperty("definition_suffix")
    public String getDefinition_suffix(){
        return this.definition_suffix ;
    }

    /**
     * 设置 [单位]
     */
    @JsonProperty("definition_suffix")
    public void setDefinition_suffix(String  definition_suffix){
        this.definition_suffix = definition_suffix ;
        this.definition_suffixDirtyFlag = true ;
    }

     /**
     * 获取 [单位]脏标记
     */
    @JsonIgnore
    public boolean getDefinition_suffixDirtyFlag(){
        return this.definition_suffixDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [要达到的目标值]
     */
    @JsonProperty("target_goal")
    public Double getTarget_goal(){
        return this.target_goal ;
    }

    /**
     * 设置 [要达到的目标值]
     */
    @JsonProperty("target_goal")
    public void setTarget_goal(Double  target_goal){
        this.target_goal = target_goal ;
        this.target_goalDirtyFlag = true ;
    }

     /**
     * 获取 [要达到的目标值]脏标记
     */
    @JsonIgnore
    public boolean getTarget_goalDirtyFlag(){
        return this.target_goalDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
