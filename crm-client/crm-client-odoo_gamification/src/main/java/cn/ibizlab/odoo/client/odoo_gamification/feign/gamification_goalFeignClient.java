package cn.ibizlab.odoo.client.odoo_gamification.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Igamification_goal;
import cn.ibizlab.odoo.client.odoo_gamification.model.gamification_goalImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[gamification_goal] 服务对象接口
 */
public interface gamification_goalFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_goals/fetchdefault")
    public Page<gamification_goalImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_gamification/gamification_goals/updatebatch")
    public gamification_goalImpl updateBatch(@RequestBody List<gamification_goalImpl> gamification_goals);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_gamification/gamification_goals/removebatch")
    public gamification_goalImpl removeBatch(@RequestBody List<gamification_goalImpl> gamification_goals);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_goals/{id}")
    public gamification_goalImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_gamification/gamification_goals/createbatch")
    public gamification_goalImpl createBatch(@RequestBody List<gamification_goalImpl> gamification_goals);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_gamification/gamification_goals/{id}")
    public gamification_goalImpl update(@PathVariable("id") Integer id,@RequestBody gamification_goalImpl gamification_goal);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_gamification/gamification_goals")
    public gamification_goalImpl create(@RequestBody gamification_goalImpl gamification_goal);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_gamification/gamification_goals/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_goals/select")
    public Page<gamification_goalImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_goals/{id}/getdraft")
    public gamification_goalImpl getDraft(@PathVariable("id") Integer id,@RequestBody gamification_goalImpl gamification_goal);



}
