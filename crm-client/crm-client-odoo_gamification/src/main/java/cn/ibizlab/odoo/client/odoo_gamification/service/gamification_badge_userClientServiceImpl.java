package cn.ibizlab.odoo.client.odoo_gamification.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Igamification_badge_user;
import cn.ibizlab.odoo.client.odoo_gamification.config.odoo_gamificationClientProperties;
import cn.ibizlab.odoo.core.client.service.Igamification_badge_userClientService;
import cn.ibizlab.odoo.client.odoo_gamification.model.gamification_badge_userImpl;
import cn.ibizlab.odoo.client.odoo_gamification.feign.gamification_badge_userFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[gamification_badge_user] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class gamification_badge_userClientServiceImpl implements Igamification_badge_userClientService {

    gamification_badge_userFeignClient gamification_badge_userFeignClient;

    @Autowired
    public gamification_badge_userClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_gamificationClientProperties odoo_gamificationClientProperties) {
        if (odoo_gamificationClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.gamification_badge_userFeignClient = nameBuilder.target(gamification_badge_userFeignClient.class,"http://"+odoo_gamificationClientProperties.getServiceId()+"/") ;
		}else if (odoo_gamificationClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.gamification_badge_userFeignClient = nameBuilder.target(gamification_badge_userFeignClient.class,odoo_gamificationClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Igamification_badge_user createModel() {
		return new gamification_badge_userImpl();
	}


    public void createBatch(List<Igamification_badge_user> gamification_badge_users){
        if(gamification_badge_users!=null){
            List<gamification_badge_userImpl> list = new ArrayList<gamification_badge_userImpl>();
            for(Igamification_badge_user igamification_badge_user :gamification_badge_users){
                list.add((gamification_badge_userImpl)igamification_badge_user) ;
            }
            gamification_badge_userFeignClient.createBatch(list) ;
        }
    }


    public void get(Igamification_badge_user gamification_badge_user){
        Igamification_badge_user clientModel = gamification_badge_userFeignClient.get(gamification_badge_user.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_badge_user.getClass(), false);
        copier.copy(clientModel, gamification_badge_user, null);
    }


    public void updateBatch(List<Igamification_badge_user> gamification_badge_users){
        if(gamification_badge_users!=null){
            List<gamification_badge_userImpl> list = new ArrayList<gamification_badge_userImpl>();
            for(Igamification_badge_user igamification_badge_user :gamification_badge_users){
                list.add((gamification_badge_userImpl)igamification_badge_user) ;
            }
            gamification_badge_userFeignClient.updateBatch(list) ;
        }
    }


    public void update(Igamification_badge_user gamification_badge_user){
        Igamification_badge_user clientModel = gamification_badge_userFeignClient.update(gamification_badge_user.getId(),(gamification_badge_userImpl)gamification_badge_user) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_badge_user.getClass(), false);
        copier.copy(clientModel, gamification_badge_user, null);
    }


    public void removeBatch(List<Igamification_badge_user> gamification_badge_users){
        if(gamification_badge_users!=null){
            List<gamification_badge_userImpl> list = new ArrayList<gamification_badge_userImpl>();
            for(Igamification_badge_user igamification_badge_user :gamification_badge_users){
                list.add((gamification_badge_userImpl)igamification_badge_user) ;
            }
            gamification_badge_userFeignClient.removeBatch(list) ;
        }
    }


    public void create(Igamification_badge_user gamification_badge_user){
        Igamification_badge_user clientModel = gamification_badge_userFeignClient.create((gamification_badge_userImpl)gamification_badge_user) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_badge_user.getClass(), false);
        copier.copy(clientModel, gamification_badge_user, null);
    }


    public Page<Igamification_badge_user> fetchDefault(SearchContext context){
        Page<gamification_badge_userImpl> page = this.gamification_badge_userFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Igamification_badge_user gamification_badge_user){
        gamification_badge_userFeignClient.remove(gamification_badge_user.getId()) ;
    }


    public Page<Igamification_badge_user> select(SearchContext context){
        return null ;
    }


    public void getDraft(Igamification_badge_user gamification_badge_user){
        Igamification_badge_user clientModel = gamification_badge_userFeignClient.getDraft(gamification_badge_user.getId(),(gamification_badge_userImpl)gamification_badge_user) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_badge_user.getClass(), false);
        copier.copy(clientModel, gamification_badge_user, null);
    }



}

