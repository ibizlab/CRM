package cn.ibizlab.odoo.client.odoo_gamification.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Igamification_goal_wizard;
import cn.ibizlab.odoo.client.odoo_gamification.model.gamification_goal_wizardImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[gamification_goal_wizard] 服务对象接口
 */
public interface gamification_goal_wizardFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_gamification/gamification_goal_wizards/removebatch")
    public gamification_goal_wizardImpl removeBatch(@RequestBody List<gamification_goal_wizardImpl> gamification_goal_wizards);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_gamification/gamification_goal_wizards/{id}")
    public gamification_goal_wizardImpl update(@PathVariable("id") Integer id,@RequestBody gamification_goal_wizardImpl gamification_goal_wizard);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_goal_wizards/fetchdefault")
    public Page<gamification_goal_wizardImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_gamification/gamification_goal_wizards")
    public gamification_goal_wizardImpl create(@RequestBody gamification_goal_wizardImpl gamification_goal_wizard);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_goal_wizards/{id}")
    public gamification_goal_wizardImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_gamification/gamification_goal_wizards/createbatch")
    public gamification_goal_wizardImpl createBatch(@RequestBody List<gamification_goal_wizardImpl> gamification_goal_wizards);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_gamification/gamification_goal_wizards/updatebatch")
    public gamification_goal_wizardImpl updateBatch(@RequestBody List<gamification_goal_wizardImpl> gamification_goal_wizards);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_gamification/gamification_goal_wizards/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_goal_wizards/select")
    public Page<gamification_goal_wizardImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_goal_wizards/{id}/getdraft")
    public gamification_goal_wizardImpl getDraft(@PathVariable("id") Integer id,@RequestBody gamification_goal_wizardImpl gamification_goal_wizard);



}
