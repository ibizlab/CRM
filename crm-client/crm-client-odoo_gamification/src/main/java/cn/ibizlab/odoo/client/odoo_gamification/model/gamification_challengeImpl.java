package cn.ibizlab.odoo.client.odoo_gamification.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Igamification_challenge;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[gamification_challenge] 对象
 */
public class gamification_challengeImpl implements Igamification_challenge,Serializable{

    /**
     * 出现在
     */
    public String category;

    @JsonIgnore
    public boolean categoryDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 说明
     */
    public String description;

    @JsonIgnore
    public boolean descriptionDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 结束日期
     */
    public Timestamp end_date;

    @JsonIgnore
    public boolean end_dateDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 建议用户
     */
    public String invited_user_ids;

    @JsonIgnore
    public boolean invited_user_idsDirtyFlag;
    
    /**
     * 最新报告日期
     */
    public Timestamp last_report_date;

    @JsonIgnore
    public boolean last_report_dateDirtyFlag;
    
    /**
     * 明细行
     */
    public String line_ids;

    @JsonIgnore
    public boolean line_idsDirtyFlag;
    
    /**
     * 负责人
     */
    public Integer manager_id;

    @JsonIgnore
    public boolean manager_idDirtyFlag;
    
    /**
     * 负责人
     */
    public String manager_id_text;

    @JsonIgnore
    public boolean manager_id_textDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 错误数
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 消息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 需要激活
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 行动数量
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 挑战名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 下次报告日期
     */
    public Timestamp next_report_date;

    @JsonIgnore
    public boolean next_report_dateDirtyFlag;
    
    /**
     * 周期
     */
    public String period;

    @JsonIgnore
    public boolean periodDirtyFlag;
    
    /**
     * 未更新的手动目标稍后将被提醒
     */
    public Integer remind_update_delay;

    @JsonIgnore
    public boolean remind_update_delayDirtyFlag;
    
    /**
     * 报告的频率
     */
    public String report_message_frequency;

    @JsonIgnore
    public boolean report_message_frequencyDirtyFlag;
    
    /**
     * 抄送
     */
    public Integer report_message_group_id;

    @JsonIgnore
    public boolean report_message_group_idDirtyFlag;
    
    /**
     * 抄送
     */
    public String report_message_group_id_text;

    @JsonIgnore
    public boolean report_message_group_id_textDirtyFlag;
    
    /**
     * 报告模板
     */
    public Integer report_template_id;

    @JsonIgnore
    public boolean report_template_idDirtyFlag;
    
    /**
     * 报告模板
     */
    public String report_template_id_text;

    @JsonIgnore
    public boolean report_template_id_textDirtyFlag;
    
    /**
     * 奖励未达成目标的最优者?
     */
    public String reward_failure;

    @JsonIgnore
    public boolean reward_failureDirtyFlag;
    
    /**
     * 第一位用户
     */
    public Integer reward_first_id;

    @JsonIgnore
    public boolean reward_first_idDirtyFlag;
    
    /**
     * 第一位用户
     */
    public String reward_first_id_text;

    @JsonIgnore
    public boolean reward_first_id_textDirtyFlag;
    
    /**
     * 每位获得成功的用户
     */
    public Integer reward_id;

    @JsonIgnore
    public boolean reward_idDirtyFlag;
    
    /**
     * 每位获得成功的用户
     */
    public String reward_id_text;

    @JsonIgnore
    public boolean reward_id_textDirtyFlag;
    
    /**
     * 每完成一个目标就马上奖励
     */
    public String reward_realtime;

    @JsonIgnore
    public boolean reward_realtimeDirtyFlag;
    
    /**
     * 第二位用户
     */
    public Integer reward_second_id;

    @JsonIgnore
    public boolean reward_second_idDirtyFlag;
    
    /**
     * 第二位用户
     */
    public String reward_second_id_text;

    @JsonIgnore
    public boolean reward_second_id_textDirtyFlag;
    
    /**
     * 第三位用户
     */
    public Integer reward_third_id;

    @JsonIgnore
    public boolean reward_third_idDirtyFlag;
    
    /**
     * 第三位用户
     */
    public String reward_third_id_text;

    @JsonIgnore
    public boolean reward_third_id_textDirtyFlag;
    
    /**
     * 开始日期
     */
    public Timestamp start_date;

    @JsonIgnore
    public boolean start_dateDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 用户领域
     */
    public String user_domain;

    @JsonIgnore
    public boolean user_domainDirtyFlag;
    
    /**
     * 用户
     */
    public String user_ids;

    @JsonIgnore
    public boolean user_idsDirtyFlag;
    
    /**
     * 显示模式
     */
    public String visibility_mode;

    @JsonIgnore
    public boolean visibility_modeDirtyFlag;
    
    /**
     * 网站消息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [出现在]
     */
    @JsonProperty("category")
    public String getCategory(){
        return this.category ;
    }

    /**
     * 设置 [出现在]
     */
    @JsonProperty("category")
    public void setCategory(String  category){
        this.category = category ;
        this.categoryDirtyFlag = true ;
    }

     /**
     * 获取 [出现在]脏标记
     */
    @JsonIgnore
    public boolean getCategoryDirtyFlag(){
        return this.categoryDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [说明]
     */
    @JsonProperty("description")
    public String getDescription(){
        return this.description ;
    }

    /**
     * 设置 [说明]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [说明]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return this.descriptionDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [结束日期]
     */
    @JsonProperty("end_date")
    public Timestamp getEnd_date(){
        return this.end_date ;
    }

    /**
     * 设置 [结束日期]
     */
    @JsonProperty("end_date")
    public void setEnd_date(Timestamp  end_date){
        this.end_date = end_date ;
        this.end_dateDirtyFlag = true ;
    }

     /**
     * 获取 [结束日期]脏标记
     */
    @JsonIgnore
    public boolean getEnd_dateDirtyFlag(){
        return this.end_dateDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [建议用户]
     */
    @JsonProperty("invited_user_ids")
    public String getInvited_user_ids(){
        return this.invited_user_ids ;
    }

    /**
     * 设置 [建议用户]
     */
    @JsonProperty("invited_user_ids")
    public void setInvited_user_ids(String  invited_user_ids){
        this.invited_user_ids = invited_user_ids ;
        this.invited_user_idsDirtyFlag = true ;
    }

     /**
     * 获取 [建议用户]脏标记
     */
    @JsonIgnore
    public boolean getInvited_user_idsDirtyFlag(){
        return this.invited_user_idsDirtyFlag ;
    }   

    /**
     * 获取 [最新报告日期]
     */
    @JsonProperty("last_report_date")
    public Timestamp getLast_report_date(){
        return this.last_report_date ;
    }

    /**
     * 设置 [最新报告日期]
     */
    @JsonProperty("last_report_date")
    public void setLast_report_date(Timestamp  last_report_date){
        this.last_report_date = last_report_date ;
        this.last_report_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最新报告日期]脏标记
     */
    @JsonIgnore
    public boolean getLast_report_dateDirtyFlag(){
        return this.last_report_dateDirtyFlag ;
    }   

    /**
     * 获取 [明细行]
     */
    @JsonProperty("line_ids")
    public String getLine_ids(){
        return this.line_ids ;
    }

    /**
     * 设置 [明细行]
     */
    @JsonProperty("line_ids")
    public void setLine_ids(String  line_ids){
        this.line_ids = line_ids ;
        this.line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [明细行]脏标记
     */
    @JsonIgnore
    public boolean getLine_idsDirtyFlag(){
        return this.line_idsDirtyFlag ;
    }   

    /**
     * 获取 [负责人]
     */
    @JsonProperty("manager_id")
    public Integer getManager_id(){
        return this.manager_id ;
    }

    /**
     * 设置 [负责人]
     */
    @JsonProperty("manager_id")
    public void setManager_id(Integer  manager_id){
        this.manager_id = manager_id ;
        this.manager_idDirtyFlag = true ;
    }

     /**
     * 获取 [负责人]脏标记
     */
    @JsonIgnore
    public boolean getManager_idDirtyFlag(){
        return this.manager_idDirtyFlag ;
    }   

    /**
     * 获取 [负责人]
     */
    @JsonProperty("manager_id_text")
    public String getManager_id_text(){
        return this.manager_id_text ;
    }

    /**
     * 设置 [负责人]
     */
    @JsonProperty("manager_id_text")
    public void setManager_id_text(String  manager_id_text){
        this.manager_id_text = manager_id_text ;
        this.manager_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [负责人]脏标记
     */
    @JsonIgnore
    public boolean getManager_id_textDirtyFlag(){
        return this.manager_id_textDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [错误数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [需要激活]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要激活]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [需要激活]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [行动数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [挑战名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [挑战名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [挑战名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [下次报告日期]
     */
    @JsonProperty("next_report_date")
    public Timestamp getNext_report_date(){
        return this.next_report_date ;
    }

    /**
     * 设置 [下次报告日期]
     */
    @JsonProperty("next_report_date")
    public void setNext_report_date(Timestamp  next_report_date){
        this.next_report_date = next_report_date ;
        this.next_report_dateDirtyFlag = true ;
    }

     /**
     * 获取 [下次报告日期]脏标记
     */
    @JsonIgnore
    public boolean getNext_report_dateDirtyFlag(){
        return this.next_report_dateDirtyFlag ;
    }   

    /**
     * 获取 [周期]
     */
    @JsonProperty("period")
    public String getPeriod(){
        return this.period ;
    }

    /**
     * 设置 [周期]
     */
    @JsonProperty("period")
    public void setPeriod(String  period){
        this.period = period ;
        this.periodDirtyFlag = true ;
    }

     /**
     * 获取 [周期]脏标记
     */
    @JsonIgnore
    public boolean getPeriodDirtyFlag(){
        return this.periodDirtyFlag ;
    }   

    /**
     * 获取 [未更新的手动目标稍后将被提醒]
     */
    @JsonProperty("remind_update_delay")
    public Integer getRemind_update_delay(){
        return this.remind_update_delay ;
    }

    /**
     * 设置 [未更新的手动目标稍后将被提醒]
     */
    @JsonProperty("remind_update_delay")
    public void setRemind_update_delay(Integer  remind_update_delay){
        this.remind_update_delay = remind_update_delay ;
        this.remind_update_delayDirtyFlag = true ;
    }

     /**
     * 获取 [未更新的手动目标稍后将被提醒]脏标记
     */
    @JsonIgnore
    public boolean getRemind_update_delayDirtyFlag(){
        return this.remind_update_delayDirtyFlag ;
    }   

    /**
     * 获取 [报告的频率]
     */
    @JsonProperty("report_message_frequency")
    public String getReport_message_frequency(){
        return this.report_message_frequency ;
    }

    /**
     * 设置 [报告的频率]
     */
    @JsonProperty("report_message_frequency")
    public void setReport_message_frequency(String  report_message_frequency){
        this.report_message_frequency = report_message_frequency ;
        this.report_message_frequencyDirtyFlag = true ;
    }

     /**
     * 获取 [报告的频率]脏标记
     */
    @JsonIgnore
    public boolean getReport_message_frequencyDirtyFlag(){
        return this.report_message_frequencyDirtyFlag ;
    }   

    /**
     * 获取 [抄送]
     */
    @JsonProperty("report_message_group_id")
    public Integer getReport_message_group_id(){
        return this.report_message_group_id ;
    }

    /**
     * 设置 [抄送]
     */
    @JsonProperty("report_message_group_id")
    public void setReport_message_group_id(Integer  report_message_group_id){
        this.report_message_group_id = report_message_group_id ;
        this.report_message_group_idDirtyFlag = true ;
    }

     /**
     * 获取 [抄送]脏标记
     */
    @JsonIgnore
    public boolean getReport_message_group_idDirtyFlag(){
        return this.report_message_group_idDirtyFlag ;
    }   

    /**
     * 获取 [抄送]
     */
    @JsonProperty("report_message_group_id_text")
    public String getReport_message_group_id_text(){
        return this.report_message_group_id_text ;
    }

    /**
     * 设置 [抄送]
     */
    @JsonProperty("report_message_group_id_text")
    public void setReport_message_group_id_text(String  report_message_group_id_text){
        this.report_message_group_id_text = report_message_group_id_text ;
        this.report_message_group_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [抄送]脏标记
     */
    @JsonIgnore
    public boolean getReport_message_group_id_textDirtyFlag(){
        return this.report_message_group_id_textDirtyFlag ;
    }   

    /**
     * 获取 [报告模板]
     */
    @JsonProperty("report_template_id")
    public Integer getReport_template_id(){
        return this.report_template_id ;
    }

    /**
     * 设置 [报告模板]
     */
    @JsonProperty("report_template_id")
    public void setReport_template_id(Integer  report_template_id){
        this.report_template_id = report_template_id ;
        this.report_template_idDirtyFlag = true ;
    }

     /**
     * 获取 [报告模板]脏标记
     */
    @JsonIgnore
    public boolean getReport_template_idDirtyFlag(){
        return this.report_template_idDirtyFlag ;
    }   

    /**
     * 获取 [报告模板]
     */
    @JsonProperty("report_template_id_text")
    public String getReport_template_id_text(){
        return this.report_template_id_text ;
    }

    /**
     * 设置 [报告模板]
     */
    @JsonProperty("report_template_id_text")
    public void setReport_template_id_text(String  report_template_id_text){
        this.report_template_id_text = report_template_id_text ;
        this.report_template_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [报告模板]脏标记
     */
    @JsonIgnore
    public boolean getReport_template_id_textDirtyFlag(){
        return this.report_template_id_textDirtyFlag ;
    }   

    /**
     * 获取 [奖励未达成目标的最优者?]
     */
    @JsonProperty("reward_failure")
    public String getReward_failure(){
        return this.reward_failure ;
    }

    /**
     * 设置 [奖励未达成目标的最优者?]
     */
    @JsonProperty("reward_failure")
    public void setReward_failure(String  reward_failure){
        this.reward_failure = reward_failure ;
        this.reward_failureDirtyFlag = true ;
    }

     /**
     * 获取 [奖励未达成目标的最优者?]脏标记
     */
    @JsonIgnore
    public boolean getReward_failureDirtyFlag(){
        return this.reward_failureDirtyFlag ;
    }   

    /**
     * 获取 [第一位用户]
     */
    @JsonProperty("reward_first_id")
    public Integer getReward_first_id(){
        return this.reward_first_id ;
    }

    /**
     * 设置 [第一位用户]
     */
    @JsonProperty("reward_first_id")
    public void setReward_first_id(Integer  reward_first_id){
        this.reward_first_id = reward_first_id ;
        this.reward_first_idDirtyFlag = true ;
    }

     /**
     * 获取 [第一位用户]脏标记
     */
    @JsonIgnore
    public boolean getReward_first_idDirtyFlag(){
        return this.reward_first_idDirtyFlag ;
    }   

    /**
     * 获取 [第一位用户]
     */
    @JsonProperty("reward_first_id_text")
    public String getReward_first_id_text(){
        return this.reward_first_id_text ;
    }

    /**
     * 设置 [第一位用户]
     */
    @JsonProperty("reward_first_id_text")
    public void setReward_first_id_text(String  reward_first_id_text){
        this.reward_first_id_text = reward_first_id_text ;
        this.reward_first_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [第一位用户]脏标记
     */
    @JsonIgnore
    public boolean getReward_first_id_textDirtyFlag(){
        return this.reward_first_id_textDirtyFlag ;
    }   

    /**
     * 获取 [每位获得成功的用户]
     */
    @JsonProperty("reward_id")
    public Integer getReward_id(){
        return this.reward_id ;
    }

    /**
     * 设置 [每位获得成功的用户]
     */
    @JsonProperty("reward_id")
    public void setReward_id(Integer  reward_id){
        this.reward_id = reward_id ;
        this.reward_idDirtyFlag = true ;
    }

     /**
     * 获取 [每位获得成功的用户]脏标记
     */
    @JsonIgnore
    public boolean getReward_idDirtyFlag(){
        return this.reward_idDirtyFlag ;
    }   

    /**
     * 获取 [每位获得成功的用户]
     */
    @JsonProperty("reward_id_text")
    public String getReward_id_text(){
        return this.reward_id_text ;
    }

    /**
     * 设置 [每位获得成功的用户]
     */
    @JsonProperty("reward_id_text")
    public void setReward_id_text(String  reward_id_text){
        this.reward_id_text = reward_id_text ;
        this.reward_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [每位获得成功的用户]脏标记
     */
    @JsonIgnore
    public boolean getReward_id_textDirtyFlag(){
        return this.reward_id_textDirtyFlag ;
    }   

    /**
     * 获取 [每完成一个目标就马上奖励]
     */
    @JsonProperty("reward_realtime")
    public String getReward_realtime(){
        return this.reward_realtime ;
    }

    /**
     * 设置 [每完成一个目标就马上奖励]
     */
    @JsonProperty("reward_realtime")
    public void setReward_realtime(String  reward_realtime){
        this.reward_realtime = reward_realtime ;
        this.reward_realtimeDirtyFlag = true ;
    }

     /**
     * 获取 [每完成一个目标就马上奖励]脏标记
     */
    @JsonIgnore
    public boolean getReward_realtimeDirtyFlag(){
        return this.reward_realtimeDirtyFlag ;
    }   

    /**
     * 获取 [第二位用户]
     */
    @JsonProperty("reward_second_id")
    public Integer getReward_second_id(){
        return this.reward_second_id ;
    }

    /**
     * 设置 [第二位用户]
     */
    @JsonProperty("reward_second_id")
    public void setReward_second_id(Integer  reward_second_id){
        this.reward_second_id = reward_second_id ;
        this.reward_second_idDirtyFlag = true ;
    }

     /**
     * 获取 [第二位用户]脏标记
     */
    @JsonIgnore
    public boolean getReward_second_idDirtyFlag(){
        return this.reward_second_idDirtyFlag ;
    }   

    /**
     * 获取 [第二位用户]
     */
    @JsonProperty("reward_second_id_text")
    public String getReward_second_id_text(){
        return this.reward_second_id_text ;
    }

    /**
     * 设置 [第二位用户]
     */
    @JsonProperty("reward_second_id_text")
    public void setReward_second_id_text(String  reward_second_id_text){
        this.reward_second_id_text = reward_second_id_text ;
        this.reward_second_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [第二位用户]脏标记
     */
    @JsonIgnore
    public boolean getReward_second_id_textDirtyFlag(){
        return this.reward_second_id_textDirtyFlag ;
    }   

    /**
     * 获取 [第三位用户]
     */
    @JsonProperty("reward_third_id")
    public Integer getReward_third_id(){
        return this.reward_third_id ;
    }

    /**
     * 设置 [第三位用户]
     */
    @JsonProperty("reward_third_id")
    public void setReward_third_id(Integer  reward_third_id){
        this.reward_third_id = reward_third_id ;
        this.reward_third_idDirtyFlag = true ;
    }

     /**
     * 获取 [第三位用户]脏标记
     */
    @JsonIgnore
    public boolean getReward_third_idDirtyFlag(){
        return this.reward_third_idDirtyFlag ;
    }   

    /**
     * 获取 [第三位用户]
     */
    @JsonProperty("reward_third_id_text")
    public String getReward_third_id_text(){
        return this.reward_third_id_text ;
    }

    /**
     * 设置 [第三位用户]
     */
    @JsonProperty("reward_third_id_text")
    public void setReward_third_id_text(String  reward_third_id_text){
        this.reward_third_id_text = reward_third_id_text ;
        this.reward_third_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [第三位用户]脏标记
     */
    @JsonIgnore
    public boolean getReward_third_id_textDirtyFlag(){
        return this.reward_third_id_textDirtyFlag ;
    }   

    /**
     * 获取 [开始日期]
     */
    @JsonProperty("start_date")
    public Timestamp getStart_date(){
        return this.start_date ;
    }

    /**
     * 设置 [开始日期]
     */
    @JsonProperty("start_date")
    public void setStart_date(Timestamp  start_date){
        this.start_date = start_date ;
        this.start_dateDirtyFlag = true ;
    }

     /**
     * 获取 [开始日期]脏标记
     */
    @JsonIgnore
    public boolean getStart_dateDirtyFlag(){
        return this.start_dateDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [用户领域]
     */
    @JsonProperty("user_domain")
    public String getUser_domain(){
        return this.user_domain ;
    }

    /**
     * 设置 [用户领域]
     */
    @JsonProperty("user_domain")
    public void setUser_domain(String  user_domain){
        this.user_domain = user_domain ;
        this.user_domainDirtyFlag = true ;
    }

     /**
     * 获取 [用户领域]脏标记
     */
    @JsonIgnore
    public boolean getUser_domainDirtyFlag(){
        return this.user_domainDirtyFlag ;
    }   

    /**
     * 获取 [用户]
     */
    @JsonProperty("user_ids")
    public String getUser_ids(){
        return this.user_ids ;
    }

    /**
     * 设置 [用户]
     */
    @JsonProperty("user_ids")
    public void setUser_ids(String  user_ids){
        this.user_ids = user_ids ;
        this.user_idsDirtyFlag = true ;
    }

     /**
     * 获取 [用户]脏标记
     */
    @JsonIgnore
    public boolean getUser_idsDirtyFlag(){
        return this.user_idsDirtyFlag ;
    }   

    /**
     * 获取 [显示模式]
     */
    @JsonProperty("visibility_mode")
    public String getVisibility_mode(){
        return this.visibility_mode ;
    }

    /**
     * 设置 [显示模式]
     */
    @JsonProperty("visibility_mode")
    public void setVisibility_mode(String  visibility_mode){
        this.visibility_mode = visibility_mode ;
        this.visibility_modeDirtyFlag = true ;
    }

     /**
     * 获取 [显示模式]脏标记
     */
    @JsonIgnore
    public boolean getVisibility_modeDirtyFlag(){
        return this.visibility_modeDirtyFlag ;
    }   

    /**
     * 获取 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站消息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
