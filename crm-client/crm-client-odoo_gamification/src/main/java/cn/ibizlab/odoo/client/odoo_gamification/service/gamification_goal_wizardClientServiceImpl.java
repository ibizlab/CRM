package cn.ibizlab.odoo.client.odoo_gamification.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Igamification_goal_wizard;
import cn.ibizlab.odoo.client.odoo_gamification.config.odoo_gamificationClientProperties;
import cn.ibizlab.odoo.core.client.service.Igamification_goal_wizardClientService;
import cn.ibizlab.odoo.client.odoo_gamification.model.gamification_goal_wizardImpl;
import cn.ibizlab.odoo.client.odoo_gamification.feign.gamification_goal_wizardFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[gamification_goal_wizard] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class gamification_goal_wizardClientServiceImpl implements Igamification_goal_wizardClientService {

    gamification_goal_wizardFeignClient gamification_goal_wizardFeignClient;

    @Autowired
    public gamification_goal_wizardClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_gamificationClientProperties odoo_gamificationClientProperties) {
        if (odoo_gamificationClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.gamification_goal_wizardFeignClient = nameBuilder.target(gamification_goal_wizardFeignClient.class,"http://"+odoo_gamificationClientProperties.getServiceId()+"/") ;
		}else if (odoo_gamificationClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.gamification_goal_wizardFeignClient = nameBuilder.target(gamification_goal_wizardFeignClient.class,odoo_gamificationClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Igamification_goal_wizard createModel() {
		return new gamification_goal_wizardImpl();
	}


    public void removeBatch(List<Igamification_goal_wizard> gamification_goal_wizards){
        if(gamification_goal_wizards!=null){
            List<gamification_goal_wizardImpl> list = new ArrayList<gamification_goal_wizardImpl>();
            for(Igamification_goal_wizard igamification_goal_wizard :gamification_goal_wizards){
                list.add((gamification_goal_wizardImpl)igamification_goal_wizard) ;
            }
            gamification_goal_wizardFeignClient.removeBatch(list) ;
        }
    }


    public void update(Igamification_goal_wizard gamification_goal_wizard){
        Igamification_goal_wizard clientModel = gamification_goal_wizardFeignClient.update(gamification_goal_wizard.getId(),(gamification_goal_wizardImpl)gamification_goal_wizard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_goal_wizard.getClass(), false);
        copier.copy(clientModel, gamification_goal_wizard, null);
    }


    public Page<Igamification_goal_wizard> fetchDefault(SearchContext context){
        Page<gamification_goal_wizardImpl> page = this.gamification_goal_wizardFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Igamification_goal_wizard gamification_goal_wizard){
        Igamification_goal_wizard clientModel = gamification_goal_wizardFeignClient.create((gamification_goal_wizardImpl)gamification_goal_wizard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_goal_wizard.getClass(), false);
        copier.copy(clientModel, gamification_goal_wizard, null);
    }


    public void get(Igamification_goal_wizard gamification_goal_wizard){
        Igamification_goal_wizard clientModel = gamification_goal_wizardFeignClient.get(gamification_goal_wizard.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_goal_wizard.getClass(), false);
        copier.copy(clientModel, gamification_goal_wizard, null);
    }


    public void createBatch(List<Igamification_goal_wizard> gamification_goal_wizards){
        if(gamification_goal_wizards!=null){
            List<gamification_goal_wizardImpl> list = new ArrayList<gamification_goal_wizardImpl>();
            for(Igamification_goal_wizard igamification_goal_wizard :gamification_goal_wizards){
                list.add((gamification_goal_wizardImpl)igamification_goal_wizard) ;
            }
            gamification_goal_wizardFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Igamification_goal_wizard> gamification_goal_wizards){
        if(gamification_goal_wizards!=null){
            List<gamification_goal_wizardImpl> list = new ArrayList<gamification_goal_wizardImpl>();
            for(Igamification_goal_wizard igamification_goal_wizard :gamification_goal_wizards){
                list.add((gamification_goal_wizardImpl)igamification_goal_wizard) ;
            }
            gamification_goal_wizardFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Igamification_goal_wizard gamification_goal_wizard){
        gamification_goal_wizardFeignClient.remove(gamification_goal_wizard.getId()) ;
    }


    public Page<Igamification_goal_wizard> select(SearchContext context){
        return null ;
    }


    public void getDraft(Igamification_goal_wizard gamification_goal_wizard){
        Igamification_goal_wizard clientModel = gamification_goal_wizardFeignClient.getDraft(gamification_goal_wizard.getId(),(gamification_goal_wizardImpl)gamification_goal_wizard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_goal_wizard.getClass(), false);
        copier.copy(clientModel, gamification_goal_wizard, null);
    }



}

