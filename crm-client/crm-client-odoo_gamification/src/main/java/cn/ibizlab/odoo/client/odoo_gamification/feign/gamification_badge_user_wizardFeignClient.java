package cn.ibizlab.odoo.client.odoo_gamification.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Igamification_badge_user_wizard;
import cn.ibizlab.odoo.client.odoo_gamification.model.gamification_badge_user_wizardImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[gamification_badge_user_wizard] 服务对象接口
 */
public interface gamification_badge_user_wizardFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_gamification/gamification_badge_user_wizards/updatebatch")
    public gamification_badge_user_wizardImpl updateBatch(@RequestBody List<gamification_badge_user_wizardImpl> gamification_badge_user_wizards);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_gamification/gamification_badge_user_wizards/removebatch")
    public gamification_badge_user_wizardImpl removeBatch(@RequestBody List<gamification_badge_user_wizardImpl> gamification_badge_user_wizards);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_badge_user_wizards/{id}")
    public gamification_badge_user_wizardImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_gamification/gamification_badge_user_wizards/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_gamification/gamification_badge_user_wizards")
    public gamification_badge_user_wizardImpl create(@RequestBody gamification_badge_user_wizardImpl gamification_badge_user_wizard);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_gamification/gamification_badge_user_wizards/{id}")
    public gamification_badge_user_wizardImpl update(@PathVariable("id") Integer id,@RequestBody gamification_badge_user_wizardImpl gamification_badge_user_wizard);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_badge_user_wizards/fetchdefault")
    public Page<gamification_badge_user_wizardImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_gamification/gamification_badge_user_wizards/createbatch")
    public gamification_badge_user_wizardImpl createBatch(@RequestBody List<gamification_badge_user_wizardImpl> gamification_badge_user_wizards);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_badge_user_wizards/select")
    public Page<gamification_badge_user_wizardImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_badge_user_wizards/{id}/getdraft")
    public gamification_badge_user_wizardImpl getDraft(@PathVariable("id") Integer id,@RequestBody gamification_badge_user_wizardImpl gamification_badge_user_wizard);



}
