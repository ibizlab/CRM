package cn.ibizlab.odoo.client.odoo_gamification.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Igamification_badge_user;
import cn.ibizlab.odoo.client.odoo_gamification.model.gamification_badge_userImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[gamification_badge_user] 服务对象接口
 */
public interface gamification_badge_userFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_gamification/gamification_badge_users/createbatch")
    public gamification_badge_userImpl createBatch(@RequestBody List<gamification_badge_userImpl> gamification_badge_users);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_badge_users/{id}")
    public gamification_badge_userImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_gamification/gamification_badge_users/updatebatch")
    public gamification_badge_userImpl updateBatch(@RequestBody List<gamification_badge_userImpl> gamification_badge_users);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_gamification/gamification_badge_users/{id}")
    public gamification_badge_userImpl update(@PathVariable("id") Integer id,@RequestBody gamification_badge_userImpl gamification_badge_user);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_gamification/gamification_badge_users/removebatch")
    public gamification_badge_userImpl removeBatch(@RequestBody List<gamification_badge_userImpl> gamification_badge_users);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_gamification/gamification_badge_users")
    public gamification_badge_userImpl create(@RequestBody gamification_badge_userImpl gamification_badge_user);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_badge_users/fetchdefault")
    public Page<gamification_badge_userImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_gamification/gamification_badge_users/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_badge_users/select")
    public Page<gamification_badge_userImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_badge_users/{id}/getdraft")
    public gamification_badge_userImpl getDraft(@PathVariable("id") Integer id,@RequestBody gamification_badge_userImpl gamification_badge_user);



}
