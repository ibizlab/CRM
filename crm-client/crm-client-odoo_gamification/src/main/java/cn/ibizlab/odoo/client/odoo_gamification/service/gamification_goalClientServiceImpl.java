package cn.ibizlab.odoo.client.odoo_gamification.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Igamification_goal;
import cn.ibizlab.odoo.client.odoo_gamification.config.odoo_gamificationClientProperties;
import cn.ibizlab.odoo.core.client.service.Igamification_goalClientService;
import cn.ibizlab.odoo.client.odoo_gamification.model.gamification_goalImpl;
import cn.ibizlab.odoo.client.odoo_gamification.feign.gamification_goalFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[gamification_goal] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class gamification_goalClientServiceImpl implements Igamification_goalClientService {

    gamification_goalFeignClient gamification_goalFeignClient;

    @Autowired
    public gamification_goalClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_gamificationClientProperties odoo_gamificationClientProperties) {
        if (odoo_gamificationClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.gamification_goalFeignClient = nameBuilder.target(gamification_goalFeignClient.class,"http://"+odoo_gamificationClientProperties.getServiceId()+"/") ;
		}else if (odoo_gamificationClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.gamification_goalFeignClient = nameBuilder.target(gamification_goalFeignClient.class,odoo_gamificationClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Igamification_goal createModel() {
		return new gamification_goalImpl();
	}


    public Page<Igamification_goal> fetchDefault(SearchContext context){
        Page<gamification_goalImpl> page = this.gamification_goalFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Igamification_goal> gamification_goals){
        if(gamification_goals!=null){
            List<gamification_goalImpl> list = new ArrayList<gamification_goalImpl>();
            for(Igamification_goal igamification_goal :gamification_goals){
                list.add((gamification_goalImpl)igamification_goal) ;
            }
            gamification_goalFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Igamification_goal> gamification_goals){
        if(gamification_goals!=null){
            List<gamification_goalImpl> list = new ArrayList<gamification_goalImpl>();
            for(Igamification_goal igamification_goal :gamification_goals){
                list.add((gamification_goalImpl)igamification_goal) ;
            }
            gamification_goalFeignClient.removeBatch(list) ;
        }
    }


    public void get(Igamification_goal gamification_goal){
        Igamification_goal clientModel = gamification_goalFeignClient.get(gamification_goal.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_goal.getClass(), false);
        copier.copy(clientModel, gamification_goal, null);
    }


    public void createBatch(List<Igamification_goal> gamification_goals){
        if(gamification_goals!=null){
            List<gamification_goalImpl> list = new ArrayList<gamification_goalImpl>();
            for(Igamification_goal igamification_goal :gamification_goals){
                list.add((gamification_goalImpl)igamification_goal) ;
            }
            gamification_goalFeignClient.createBatch(list) ;
        }
    }


    public void update(Igamification_goal gamification_goal){
        Igamification_goal clientModel = gamification_goalFeignClient.update(gamification_goal.getId(),(gamification_goalImpl)gamification_goal) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_goal.getClass(), false);
        copier.copy(clientModel, gamification_goal, null);
    }


    public void create(Igamification_goal gamification_goal){
        Igamification_goal clientModel = gamification_goalFeignClient.create((gamification_goalImpl)gamification_goal) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_goal.getClass(), false);
        copier.copy(clientModel, gamification_goal, null);
    }


    public void remove(Igamification_goal gamification_goal){
        gamification_goalFeignClient.remove(gamification_goal.getId()) ;
    }


    public Page<Igamification_goal> select(SearchContext context){
        return null ;
    }


    public void getDraft(Igamification_goal gamification_goal){
        Igamification_goal clientModel = gamification_goalFeignClient.getDraft(gamification_goal.getId(),(gamification_goalImpl)gamification_goal) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_goal.getClass(), false);
        copier.copy(clientModel, gamification_goal, null);
    }



}

