package cn.ibizlab.odoo.client.odoo_gamification.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Igamification_badge_user_wizard;
import cn.ibizlab.odoo.client.odoo_gamification.config.odoo_gamificationClientProperties;
import cn.ibizlab.odoo.core.client.service.Igamification_badge_user_wizardClientService;
import cn.ibizlab.odoo.client.odoo_gamification.model.gamification_badge_user_wizardImpl;
import cn.ibizlab.odoo.client.odoo_gamification.feign.gamification_badge_user_wizardFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[gamification_badge_user_wizard] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class gamification_badge_user_wizardClientServiceImpl implements Igamification_badge_user_wizardClientService {

    gamification_badge_user_wizardFeignClient gamification_badge_user_wizardFeignClient;

    @Autowired
    public gamification_badge_user_wizardClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_gamificationClientProperties odoo_gamificationClientProperties) {
        if (odoo_gamificationClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.gamification_badge_user_wizardFeignClient = nameBuilder.target(gamification_badge_user_wizardFeignClient.class,"http://"+odoo_gamificationClientProperties.getServiceId()+"/") ;
		}else if (odoo_gamificationClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.gamification_badge_user_wizardFeignClient = nameBuilder.target(gamification_badge_user_wizardFeignClient.class,odoo_gamificationClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Igamification_badge_user_wizard createModel() {
		return new gamification_badge_user_wizardImpl();
	}


    public void updateBatch(List<Igamification_badge_user_wizard> gamification_badge_user_wizards){
        if(gamification_badge_user_wizards!=null){
            List<gamification_badge_user_wizardImpl> list = new ArrayList<gamification_badge_user_wizardImpl>();
            for(Igamification_badge_user_wizard igamification_badge_user_wizard :gamification_badge_user_wizards){
                list.add((gamification_badge_user_wizardImpl)igamification_badge_user_wizard) ;
            }
            gamification_badge_user_wizardFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Igamification_badge_user_wizard> gamification_badge_user_wizards){
        if(gamification_badge_user_wizards!=null){
            List<gamification_badge_user_wizardImpl> list = new ArrayList<gamification_badge_user_wizardImpl>();
            for(Igamification_badge_user_wizard igamification_badge_user_wizard :gamification_badge_user_wizards){
                list.add((gamification_badge_user_wizardImpl)igamification_badge_user_wizard) ;
            }
            gamification_badge_user_wizardFeignClient.removeBatch(list) ;
        }
    }


    public void get(Igamification_badge_user_wizard gamification_badge_user_wizard){
        Igamification_badge_user_wizard clientModel = gamification_badge_user_wizardFeignClient.get(gamification_badge_user_wizard.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_badge_user_wizard.getClass(), false);
        copier.copy(clientModel, gamification_badge_user_wizard, null);
    }


    public void remove(Igamification_badge_user_wizard gamification_badge_user_wizard){
        gamification_badge_user_wizardFeignClient.remove(gamification_badge_user_wizard.getId()) ;
    }


    public void create(Igamification_badge_user_wizard gamification_badge_user_wizard){
        Igamification_badge_user_wizard clientModel = gamification_badge_user_wizardFeignClient.create((gamification_badge_user_wizardImpl)gamification_badge_user_wizard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_badge_user_wizard.getClass(), false);
        copier.copy(clientModel, gamification_badge_user_wizard, null);
    }


    public void update(Igamification_badge_user_wizard gamification_badge_user_wizard){
        Igamification_badge_user_wizard clientModel = gamification_badge_user_wizardFeignClient.update(gamification_badge_user_wizard.getId(),(gamification_badge_user_wizardImpl)gamification_badge_user_wizard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_badge_user_wizard.getClass(), false);
        copier.copy(clientModel, gamification_badge_user_wizard, null);
    }


    public Page<Igamification_badge_user_wizard> fetchDefault(SearchContext context){
        Page<gamification_badge_user_wizardImpl> page = this.gamification_badge_user_wizardFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Igamification_badge_user_wizard> gamification_badge_user_wizards){
        if(gamification_badge_user_wizards!=null){
            List<gamification_badge_user_wizardImpl> list = new ArrayList<gamification_badge_user_wizardImpl>();
            for(Igamification_badge_user_wizard igamification_badge_user_wizard :gamification_badge_user_wizards){
                list.add((gamification_badge_user_wizardImpl)igamification_badge_user_wizard) ;
            }
            gamification_badge_user_wizardFeignClient.createBatch(list) ;
        }
    }


    public Page<Igamification_badge_user_wizard> select(SearchContext context){
        return null ;
    }


    public void getDraft(Igamification_badge_user_wizard gamification_badge_user_wizard){
        Igamification_badge_user_wizard clientModel = gamification_badge_user_wizardFeignClient.getDraft(gamification_badge_user_wizard.getId(),(gamification_badge_user_wizardImpl)gamification_badge_user_wizard) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), gamification_badge_user_wizard.getClass(), false);
        copier.copy(clientModel, gamification_badge_user_wizard, null);
    }



}

