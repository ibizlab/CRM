package cn.ibizlab.odoo.client.odoo_maintenance.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imaintenance_team;
import cn.ibizlab.odoo.client.odoo_maintenance.config.odoo_maintenanceClientProperties;
import cn.ibizlab.odoo.core.client.service.Imaintenance_teamClientService;
import cn.ibizlab.odoo.client.odoo_maintenance.model.maintenance_teamImpl;
import cn.ibizlab.odoo.client.odoo_maintenance.feign.maintenance_teamFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[maintenance_team] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class maintenance_teamClientServiceImpl implements Imaintenance_teamClientService {

    maintenance_teamFeignClient maintenance_teamFeignClient;

    @Autowired
    public maintenance_teamClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_maintenanceClientProperties odoo_maintenanceClientProperties) {
        if (odoo_maintenanceClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.maintenance_teamFeignClient = nameBuilder.target(maintenance_teamFeignClient.class,"http://"+odoo_maintenanceClientProperties.getServiceId()+"/") ;
		}else if (odoo_maintenanceClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.maintenance_teamFeignClient = nameBuilder.target(maintenance_teamFeignClient.class,odoo_maintenanceClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imaintenance_team createModel() {
		return new maintenance_teamImpl();
	}


    public void get(Imaintenance_team maintenance_team){
        Imaintenance_team clientModel = maintenance_teamFeignClient.get(maintenance_team.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), maintenance_team.getClass(), false);
        copier.copy(clientModel, maintenance_team, null);
    }


    public void remove(Imaintenance_team maintenance_team){
        maintenance_teamFeignClient.remove(maintenance_team.getId()) ;
    }


    public Page<Imaintenance_team> fetchDefault(SearchContext context){
        Page<maintenance_teamImpl> page = this.maintenance_teamFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Imaintenance_team maintenance_team){
        Imaintenance_team clientModel = maintenance_teamFeignClient.update(maintenance_team.getId(),(maintenance_teamImpl)maintenance_team) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), maintenance_team.getClass(), false);
        copier.copy(clientModel, maintenance_team, null);
    }


    public void createBatch(List<Imaintenance_team> maintenance_teams){
        if(maintenance_teams!=null){
            List<maintenance_teamImpl> list = new ArrayList<maintenance_teamImpl>();
            for(Imaintenance_team imaintenance_team :maintenance_teams){
                list.add((maintenance_teamImpl)imaintenance_team) ;
            }
            maintenance_teamFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Imaintenance_team> maintenance_teams){
        if(maintenance_teams!=null){
            List<maintenance_teamImpl> list = new ArrayList<maintenance_teamImpl>();
            for(Imaintenance_team imaintenance_team :maintenance_teams){
                list.add((maintenance_teamImpl)imaintenance_team) ;
            }
            maintenance_teamFeignClient.updateBatch(list) ;
        }
    }


    public void create(Imaintenance_team maintenance_team){
        Imaintenance_team clientModel = maintenance_teamFeignClient.create((maintenance_teamImpl)maintenance_team) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), maintenance_team.getClass(), false);
        copier.copy(clientModel, maintenance_team, null);
    }


    public void removeBatch(List<Imaintenance_team> maintenance_teams){
        if(maintenance_teams!=null){
            List<maintenance_teamImpl> list = new ArrayList<maintenance_teamImpl>();
            for(Imaintenance_team imaintenance_team :maintenance_teams){
                list.add((maintenance_teamImpl)imaintenance_team) ;
            }
            maintenance_teamFeignClient.removeBatch(list) ;
        }
    }


    public Page<Imaintenance_team> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imaintenance_team maintenance_team){
        Imaintenance_team clientModel = maintenance_teamFeignClient.getDraft(maintenance_team.getId(),(maintenance_teamImpl)maintenance_team) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), maintenance_team.getClass(), false);
        copier.copy(clientModel, maintenance_team, null);
    }



}

