package cn.ibizlab.odoo.client.odoo_maintenance.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.service.odoo.maintenance")
@Data
public class odoo_maintenanceClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

	private String serviceUrl ;
    	
    private String serviceId ;

}
