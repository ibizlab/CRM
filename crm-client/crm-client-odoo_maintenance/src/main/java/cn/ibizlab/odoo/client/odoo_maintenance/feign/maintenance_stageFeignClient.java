package cn.ibizlab.odoo.client.odoo_maintenance.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imaintenance_stage;
import cn.ibizlab.odoo.client.odoo_maintenance.model.maintenance_stageImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[maintenance_stage] 服务对象接口
 */
public interface maintenance_stageFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_maintenance/maintenance_stages/fetchdefault")
    public Page<maintenance_stageImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_maintenance/maintenance_stages/{id}")
    public maintenance_stageImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_maintenance/maintenance_stages")
    public maintenance_stageImpl create(@RequestBody maintenance_stageImpl maintenance_stage);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_maintenance/maintenance_stages/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_maintenance/maintenance_stages/{id}")
    public maintenance_stageImpl update(@PathVariable("id") Integer id,@RequestBody maintenance_stageImpl maintenance_stage);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_maintenance/maintenance_stages/removebatch")
    public maintenance_stageImpl removeBatch(@RequestBody List<maintenance_stageImpl> maintenance_stages);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_maintenance/maintenance_stages/updatebatch")
    public maintenance_stageImpl updateBatch(@RequestBody List<maintenance_stageImpl> maintenance_stages);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_maintenance/maintenance_stages/createbatch")
    public maintenance_stageImpl createBatch(@RequestBody List<maintenance_stageImpl> maintenance_stages);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_maintenance/maintenance_stages/select")
    public Page<maintenance_stageImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_maintenance/maintenance_stages/{id}/getdraft")
    public maintenance_stageImpl getDraft(@PathVariable("id") Integer id,@RequestBody maintenance_stageImpl maintenance_stage);



}
