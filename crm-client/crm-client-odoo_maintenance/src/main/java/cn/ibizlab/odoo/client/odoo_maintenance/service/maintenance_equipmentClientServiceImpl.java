package cn.ibizlab.odoo.client.odoo_maintenance.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imaintenance_equipment;
import cn.ibizlab.odoo.client.odoo_maintenance.config.odoo_maintenanceClientProperties;
import cn.ibizlab.odoo.core.client.service.Imaintenance_equipmentClientService;
import cn.ibizlab.odoo.client.odoo_maintenance.model.maintenance_equipmentImpl;
import cn.ibizlab.odoo.client.odoo_maintenance.feign.maintenance_equipmentFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[maintenance_equipment] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class maintenance_equipmentClientServiceImpl implements Imaintenance_equipmentClientService {

    maintenance_equipmentFeignClient maintenance_equipmentFeignClient;

    @Autowired
    public maintenance_equipmentClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_maintenanceClientProperties odoo_maintenanceClientProperties) {
        if (odoo_maintenanceClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.maintenance_equipmentFeignClient = nameBuilder.target(maintenance_equipmentFeignClient.class,"http://"+odoo_maintenanceClientProperties.getServiceId()+"/") ;
		}else if (odoo_maintenanceClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.maintenance_equipmentFeignClient = nameBuilder.target(maintenance_equipmentFeignClient.class,odoo_maintenanceClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imaintenance_equipment createModel() {
		return new maintenance_equipmentImpl();
	}


    public void remove(Imaintenance_equipment maintenance_equipment){
        maintenance_equipmentFeignClient.remove(maintenance_equipment.getId()) ;
    }


    public void removeBatch(List<Imaintenance_equipment> maintenance_equipments){
        if(maintenance_equipments!=null){
            List<maintenance_equipmentImpl> list = new ArrayList<maintenance_equipmentImpl>();
            for(Imaintenance_equipment imaintenance_equipment :maintenance_equipments){
                list.add((maintenance_equipmentImpl)imaintenance_equipment) ;
            }
            maintenance_equipmentFeignClient.removeBatch(list) ;
        }
    }


    public Page<Imaintenance_equipment> fetchDefault(SearchContext context){
        Page<maintenance_equipmentImpl> page = this.maintenance_equipmentFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Imaintenance_equipment maintenance_equipment){
        Imaintenance_equipment clientModel = maintenance_equipmentFeignClient.create((maintenance_equipmentImpl)maintenance_equipment) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), maintenance_equipment.getClass(), false);
        copier.copy(clientModel, maintenance_equipment, null);
    }


    public void updateBatch(List<Imaintenance_equipment> maintenance_equipments){
        if(maintenance_equipments!=null){
            List<maintenance_equipmentImpl> list = new ArrayList<maintenance_equipmentImpl>();
            for(Imaintenance_equipment imaintenance_equipment :maintenance_equipments){
                list.add((maintenance_equipmentImpl)imaintenance_equipment) ;
            }
            maintenance_equipmentFeignClient.updateBatch(list) ;
        }
    }


    public void update(Imaintenance_equipment maintenance_equipment){
        Imaintenance_equipment clientModel = maintenance_equipmentFeignClient.update(maintenance_equipment.getId(),(maintenance_equipmentImpl)maintenance_equipment) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), maintenance_equipment.getClass(), false);
        copier.copy(clientModel, maintenance_equipment, null);
    }


    public void get(Imaintenance_equipment maintenance_equipment){
        Imaintenance_equipment clientModel = maintenance_equipmentFeignClient.get(maintenance_equipment.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), maintenance_equipment.getClass(), false);
        copier.copy(clientModel, maintenance_equipment, null);
    }


    public void createBatch(List<Imaintenance_equipment> maintenance_equipments){
        if(maintenance_equipments!=null){
            List<maintenance_equipmentImpl> list = new ArrayList<maintenance_equipmentImpl>();
            for(Imaintenance_equipment imaintenance_equipment :maintenance_equipments){
                list.add((maintenance_equipmentImpl)imaintenance_equipment) ;
            }
            maintenance_equipmentFeignClient.createBatch(list) ;
        }
    }


    public Page<Imaintenance_equipment> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imaintenance_equipment maintenance_equipment){
        Imaintenance_equipment clientModel = maintenance_equipmentFeignClient.getDraft(maintenance_equipment.getId(),(maintenance_equipmentImpl)maintenance_equipment) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), maintenance_equipment.getClass(), false);
        copier.copy(clientModel, maintenance_equipment, null);
    }



}

