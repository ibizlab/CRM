package cn.ibizlab.odoo.client.odoo_maintenance.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Imaintenance_team;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[maintenance_team] 对象
 */
public class maintenance_teamImpl implements Imaintenance_team,Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 颜色索引
     */
    public Integer color;

    @JsonIgnore
    public boolean colorDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 设备
     */
    public String equipment_ids;

    @JsonIgnore
    public boolean equipment_idsDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 团队成员
     */
    public String member_ids;

    @JsonIgnore
    public boolean member_idsDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 请求
     */
    public String request_ids;

    @JsonIgnore
    public boolean request_idsDirtyFlag;
    
    /**
     * 请求数量
     */
    public Integer todo_request_count;

    @JsonIgnore
    public boolean todo_request_countDirtyFlag;
    
    /**
     * 已阻止请求的数量
     */
    public Integer todo_request_count_block;

    @JsonIgnore
    public boolean todo_request_count_blockDirtyFlag;
    
    /**
     * 已计划请求的数量
     */
    public Integer todo_request_count_date;

    @JsonIgnore
    public boolean todo_request_count_dateDirtyFlag;
    
    /**
     * 高优先级的请求数量
     */
    public Integer todo_request_count_high_priority;

    @JsonIgnore
    public boolean todo_request_count_high_priorityDirtyFlag;
    
    /**
     * 未计划请求的数量
     */
    public Integer todo_request_count_unscheduled;

    @JsonIgnore
    public boolean todo_request_count_unscheduledDirtyFlag;
    
    /**
     * 请求
     */
    public String todo_request_ids;

    @JsonIgnore
    public boolean todo_request_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [颜色索引]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return this.color ;
    }

    /**
     * 设置 [颜色索引]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

     /**
     * 获取 [颜色索引]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return this.colorDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [设备]
     */
    @JsonProperty("equipment_ids")
    public String getEquipment_ids(){
        return this.equipment_ids ;
    }

    /**
     * 设置 [设备]
     */
    @JsonProperty("equipment_ids")
    public void setEquipment_ids(String  equipment_ids){
        this.equipment_ids = equipment_ids ;
        this.equipment_idsDirtyFlag = true ;
    }

     /**
     * 获取 [设备]脏标记
     */
    @JsonIgnore
    public boolean getEquipment_idsDirtyFlag(){
        return this.equipment_idsDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [团队成员]
     */
    @JsonProperty("member_ids")
    public String getMember_ids(){
        return this.member_ids ;
    }

    /**
     * 设置 [团队成员]
     */
    @JsonProperty("member_ids")
    public void setMember_ids(String  member_ids){
        this.member_ids = member_ids ;
        this.member_idsDirtyFlag = true ;
    }

     /**
     * 获取 [团队成员]脏标记
     */
    @JsonIgnore
    public boolean getMember_idsDirtyFlag(){
        return this.member_idsDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [请求]
     */
    @JsonProperty("request_ids")
    public String getRequest_ids(){
        return this.request_ids ;
    }

    /**
     * 设置 [请求]
     */
    @JsonProperty("request_ids")
    public void setRequest_ids(String  request_ids){
        this.request_ids = request_ids ;
        this.request_idsDirtyFlag = true ;
    }

     /**
     * 获取 [请求]脏标记
     */
    @JsonIgnore
    public boolean getRequest_idsDirtyFlag(){
        return this.request_idsDirtyFlag ;
    }   

    /**
     * 获取 [请求数量]
     */
    @JsonProperty("todo_request_count")
    public Integer getTodo_request_count(){
        return this.todo_request_count ;
    }

    /**
     * 设置 [请求数量]
     */
    @JsonProperty("todo_request_count")
    public void setTodo_request_count(Integer  todo_request_count){
        this.todo_request_count = todo_request_count ;
        this.todo_request_countDirtyFlag = true ;
    }

     /**
     * 获取 [请求数量]脏标记
     */
    @JsonIgnore
    public boolean getTodo_request_countDirtyFlag(){
        return this.todo_request_countDirtyFlag ;
    }   

    /**
     * 获取 [已阻止请求的数量]
     */
    @JsonProperty("todo_request_count_block")
    public Integer getTodo_request_count_block(){
        return this.todo_request_count_block ;
    }

    /**
     * 设置 [已阻止请求的数量]
     */
    @JsonProperty("todo_request_count_block")
    public void setTodo_request_count_block(Integer  todo_request_count_block){
        this.todo_request_count_block = todo_request_count_block ;
        this.todo_request_count_blockDirtyFlag = true ;
    }

     /**
     * 获取 [已阻止请求的数量]脏标记
     */
    @JsonIgnore
    public boolean getTodo_request_count_blockDirtyFlag(){
        return this.todo_request_count_blockDirtyFlag ;
    }   

    /**
     * 获取 [已计划请求的数量]
     */
    @JsonProperty("todo_request_count_date")
    public Integer getTodo_request_count_date(){
        return this.todo_request_count_date ;
    }

    /**
     * 设置 [已计划请求的数量]
     */
    @JsonProperty("todo_request_count_date")
    public void setTodo_request_count_date(Integer  todo_request_count_date){
        this.todo_request_count_date = todo_request_count_date ;
        this.todo_request_count_dateDirtyFlag = true ;
    }

     /**
     * 获取 [已计划请求的数量]脏标记
     */
    @JsonIgnore
    public boolean getTodo_request_count_dateDirtyFlag(){
        return this.todo_request_count_dateDirtyFlag ;
    }   

    /**
     * 获取 [高优先级的请求数量]
     */
    @JsonProperty("todo_request_count_high_priority")
    public Integer getTodo_request_count_high_priority(){
        return this.todo_request_count_high_priority ;
    }

    /**
     * 设置 [高优先级的请求数量]
     */
    @JsonProperty("todo_request_count_high_priority")
    public void setTodo_request_count_high_priority(Integer  todo_request_count_high_priority){
        this.todo_request_count_high_priority = todo_request_count_high_priority ;
        this.todo_request_count_high_priorityDirtyFlag = true ;
    }

     /**
     * 获取 [高优先级的请求数量]脏标记
     */
    @JsonIgnore
    public boolean getTodo_request_count_high_priorityDirtyFlag(){
        return this.todo_request_count_high_priorityDirtyFlag ;
    }   

    /**
     * 获取 [未计划请求的数量]
     */
    @JsonProperty("todo_request_count_unscheduled")
    public Integer getTodo_request_count_unscheduled(){
        return this.todo_request_count_unscheduled ;
    }

    /**
     * 设置 [未计划请求的数量]
     */
    @JsonProperty("todo_request_count_unscheduled")
    public void setTodo_request_count_unscheduled(Integer  todo_request_count_unscheduled){
        this.todo_request_count_unscheduled = todo_request_count_unscheduled ;
        this.todo_request_count_unscheduledDirtyFlag = true ;
    }

     /**
     * 获取 [未计划请求的数量]脏标记
     */
    @JsonIgnore
    public boolean getTodo_request_count_unscheduledDirtyFlag(){
        return this.todo_request_count_unscheduledDirtyFlag ;
    }   

    /**
     * 获取 [请求]
     */
    @JsonProperty("todo_request_ids")
    public String getTodo_request_ids(){
        return this.todo_request_ids ;
    }

    /**
     * 设置 [请求]
     */
    @JsonProperty("todo_request_ids")
    public void setTodo_request_ids(String  todo_request_ids){
        this.todo_request_ids = todo_request_ids ;
        this.todo_request_idsDirtyFlag = true ;
    }

     /**
     * 获取 [请求]脏标记
     */
    @JsonIgnore
    public boolean getTodo_request_idsDirtyFlag(){
        return this.todo_request_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
