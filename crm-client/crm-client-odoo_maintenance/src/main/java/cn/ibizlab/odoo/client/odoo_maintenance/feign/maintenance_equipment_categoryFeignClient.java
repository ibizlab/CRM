package cn.ibizlab.odoo.client.odoo_maintenance.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imaintenance_equipment_category;
import cn.ibizlab.odoo.client.odoo_maintenance.model.maintenance_equipment_categoryImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[maintenance_equipment_category] 服务对象接口
 */
public interface maintenance_equipment_categoryFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_maintenance/maintenance_equipment_categories/createbatch")
    public maintenance_equipment_categoryImpl createBatch(@RequestBody List<maintenance_equipment_categoryImpl> maintenance_equipment_categories);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_maintenance/maintenance_equipment_categories")
    public maintenance_equipment_categoryImpl create(@RequestBody maintenance_equipment_categoryImpl maintenance_equipment_category);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_maintenance/maintenance_equipment_categories/{id}")
    public maintenance_equipment_categoryImpl update(@PathVariable("id") Integer id,@RequestBody maintenance_equipment_categoryImpl maintenance_equipment_category);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_maintenance/maintenance_equipment_categories/{id}")
    public maintenance_equipment_categoryImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_maintenance/maintenance_equipment_categories/updatebatch")
    public maintenance_equipment_categoryImpl updateBatch(@RequestBody List<maintenance_equipment_categoryImpl> maintenance_equipment_categories);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_maintenance/maintenance_equipment_categories/removebatch")
    public maintenance_equipment_categoryImpl removeBatch(@RequestBody List<maintenance_equipment_categoryImpl> maintenance_equipment_categories);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_maintenance/maintenance_equipment_categories/fetchdefault")
    public Page<maintenance_equipment_categoryImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_maintenance/maintenance_equipment_categories/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_maintenance/maintenance_equipment_categories/select")
    public Page<maintenance_equipment_categoryImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_maintenance/maintenance_equipment_categories/{id}/getdraft")
    public maintenance_equipment_categoryImpl getDraft(@PathVariable("id") Integer id,@RequestBody maintenance_equipment_categoryImpl maintenance_equipment_category);



}
