package cn.ibizlab.odoo.client.odoo_maintenance.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imaintenance_request;
import cn.ibizlab.odoo.client.odoo_maintenance.config.odoo_maintenanceClientProperties;
import cn.ibizlab.odoo.core.client.service.Imaintenance_requestClientService;
import cn.ibizlab.odoo.client.odoo_maintenance.model.maintenance_requestImpl;
import cn.ibizlab.odoo.client.odoo_maintenance.feign.maintenance_requestFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[maintenance_request] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class maintenance_requestClientServiceImpl implements Imaintenance_requestClientService {

    maintenance_requestFeignClient maintenance_requestFeignClient;

    @Autowired
    public maintenance_requestClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_maintenanceClientProperties odoo_maintenanceClientProperties) {
        if (odoo_maintenanceClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.maintenance_requestFeignClient = nameBuilder.target(maintenance_requestFeignClient.class,"http://"+odoo_maintenanceClientProperties.getServiceId()+"/") ;
		}else if (odoo_maintenanceClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.maintenance_requestFeignClient = nameBuilder.target(maintenance_requestFeignClient.class,odoo_maintenanceClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imaintenance_request createModel() {
		return new maintenance_requestImpl();
	}


    public void updateBatch(List<Imaintenance_request> maintenance_requests){
        if(maintenance_requests!=null){
            List<maintenance_requestImpl> list = new ArrayList<maintenance_requestImpl>();
            for(Imaintenance_request imaintenance_request :maintenance_requests){
                list.add((maintenance_requestImpl)imaintenance_request) ;
            }
            maintenance_requestFeignClient.updateBatch(list) ;
        }
    }


    public void create(Imaintenance_request maintenance_request){
        Imaintenance_request clientModel = maintenance_requestFeignClient.create((maintenance_requestImpl)maintenance_request) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), maintenance_request.getClass(), false);
        copier.copy(clientModel, maintenance_request, null);
    }


    public void remove(Imaintenance_request maintenance_request){
        maintenance_requestFeignClient.remove(maintenance_request.getId()) ;
    }


    public void createBatch(List<Imaintenance_request> maintenance_requests){
        if(maintenance_requests!=null){
            List<maintenance_requestImpl> list = new ArrayList<maintenance_requestImpl>();
            for(Imaintenance_request imaintenance_request :maintenance_requests){
                list.add((maintenance_requestImpl)imaintenance_request) ;
            }
            maintenance_requestFeignClient.createBatch(list) ;
        }
    }


    public void get(Imaintenance_request maintenance_request){
        Imaintenance_request clientModel = maintenance_requestFeignClient.get(maintenance_request.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), maintenance_request.getClass(), false);
        copier.copy(clientModel, maintenance_request, null);
    }


    public Page<Imaintenance_request> fetchDefault(SearchContext context){
        Page<maintenance_requestImpl> page = this.maintenance_requestFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Imaintenance_request maintenance_request){
        Imaintenance_request clientModel = maintenance_requestFeignClient.update(maintenance_request.getId(),(maintenance_requestImpl)maintenance_request) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), maintenance_request.getClass(), false);
        copier.copy(clientModel, maintenance_request, null);
    }


    public void removeBatch(List<Imaintenance_request> maintenance_requests){
        if(maintenance_requests!=null){
            List<maintenance_requestImpl> list = new ArrayList<maintenance_requestImpl>();
            for(Imaintenance_request imaintenance_request :maintenance_requests){
                list.add((maintenance_requestImpl)imaintenance_request) ;
            }
            maintenance_requestFeignClient.removeBatch(list) ;
        }
    }


    public Page<Imaintenance_request> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imaintenance_request maintenance_request){
        Imaintenance_request clientModel = maintenance_requestFeignClient.getDraft(maintenance_request.getId(),(maintenance_requestImpl)maintenance_request) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), maintenance_request.getClass(), false);
        copier.copy(clientModel, maintenance_request, null);
    }



}

