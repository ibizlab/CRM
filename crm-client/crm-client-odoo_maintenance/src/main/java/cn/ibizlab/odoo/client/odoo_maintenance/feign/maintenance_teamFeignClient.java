package cn.ibizlab.odoo.client.odoo_maintenance.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imaintenance_team;
import cn.ibizlab.odoo.client.odoo_maintenance.model.maintenance_teamImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[maintenance_team] 服务对象接口
 */
public interface maintenance_teamFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_maintenance/maintenance_teams/{id}")
    public maintenance_teamImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_maintenance/maintenance_teams/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_maintenance/maintenance_teams/fetchdefault")
    public Page<maintenance_teamImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_maintenance/maintenance_teams/{id}")
    public maintenance_teamImpl update(@PathVariable("id") Integer id,@RequestBody maintenance_teamImpl maintenance_team);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_maintenance/maintenance_teams/createbatch")
    public maintenance_teamImpl createBatch(@RequestBody List<maintenance_teamImpl> maintenance_teams);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_maintenance/maintenance_teams/updatebatch")
    public maintenance_teamImpl updateBatch(@RequestBody List<maintenance_teamImpl> maintenance_teams);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_maintenance/maintenance_teams")
    public maintenance_teamImpl create(@RequestBody maintenance_teamImpl maintenance_team);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_maintenance/maintenance_teams/removebatch")
    public maintenance_teamImpl removeBatch(@RequestBody List<maintenance_teamImpl> maintenance_teams);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_maintenance/maintenance_teams/select")
    public Page<maintenance_teamImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_maintenance/maintenance_teams/{id}/getdraft")
    public maintenance_teamImpl getDraft(@PathVariable("id") Integer id,@RequestBody maintenance_teamImpl maintenance_team);



}
