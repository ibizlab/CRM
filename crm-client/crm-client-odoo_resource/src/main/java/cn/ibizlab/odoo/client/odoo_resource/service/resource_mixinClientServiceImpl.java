package cn.ibizlab.odoo.client.odoo_resource.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iresource_mixin;
import cn.ibizlab.odoo.client.odoo_resource.config.odoo_resourceClientProperties;
import cn.ibizlab.odoo.core.client.service.Iresource_mixinClientService;
import cn.ibizlab.odoo.client.odoo_resource.model.resource_mixinImpl;
import cn.ibizlab.odoo.client.odoo_resource.feign.resource_mixinFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[resource_mixin] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class resource_mixinClientServiceImpl implements Iresource_mixinClientService {

    resource_mixinFeignClient resource_mixinFeignClient;

    @Autowired
    public resource_mixinClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_resourceClientProperties odoo_resourceClientProperties) {
        if (odoo_resourceClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.resource_mixinFeignClient = nameBuilder.target(resource_mixinFeignClient.class,"http://"+odoo_resourceClientProperties.getServiceId()+"/") ;
		}else if (odoo_resourceClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.resource_mixinFeignClient = nameBuilder.target(resource_mixinFeignClient.class,odoo_resourceClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iresource_mixin createModel() {
		return new resource_mixinImpl();
	}


    public void create(Iresource_mixin resource_mixin){
        Iresource_mixin clientModel = resource_mixinFeignClient.create((resource_mixinImpl)resource_mixin) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), resource_mixin.getClass(), false);
        copier.copy(clientModel, resource_mixin, null);
    }


    public void createBatch(List<Iresource_mixin> resource_mixins){
        if(resource_mixins!=null){
            List<resource_mixinImpl> list = new ArrayList<resource_mixinImpl>();
            for(Iresource_mixin iresource_mixin :resource_mixins){
                list.add((resource_mixinImpl)iresource_mixin) ;
            }
            resource_mixinFeignClient.createBatch(list) ;
        }
    }


    public Page<Iresource_mixin> fetchDefault(SearchContext context){
        Page<resource_mixinImpl> page = this.resource_mixinFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Iresource_mixin resource_mixin){
        resource_mixinFeignClient.remove(resource_mixin.getId()) ;
    }


    public void removeBatch(List<Iresource_mixin> resource_mixins){
        if(resource_mixins!=null){
            List<resource_mixinImpl> list = new ArrayList<resource_mixinImpl>();
            for(Iresource_mixin iresource_mixin :resource_mixins){
                list.add((resource_mixinImpl)iresource_mixin) ;
            }
            resource_mixinFeignClient.removeBatch(list) ;
        }
    }


    public void get(Iresource_mixin resource_mixin){
        Iresource_mixin clientModel = resource_mixinFeignClient.get(resource_mixin.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), resource_mixin.getClass(), false);
        copier.copy(clientModel, resource_mixin, null);
    }


    public void update(Iresource_mixin resource_mixin){
        Iresource_mixin clientModel = resource_mixinFeignClient.update(resource_mixin.getId(),(resource_mixinImpl)resource_mixin) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), resource_mixin.getClass(), false);
        copier.copy(clientModel, resource_mixin, null);
    }


    public void updateBatch(List<Iresource_mixin> resource_mixins){
        if(resource_mixins!=null){
            List<resource_mixinImpl> list = new ArrayList<resource_mixinImpl>();
            for(Iresource_mixin iresource_mixin :resource_mixins){
                list.add((resource_mixinImpl)iresource_mixin) ;
            }
            resource_mixinFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iresource_mixin> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iresource_mixin resource_mixin){
        Iresource_mixin clientModel = resource_mixinFeignClient.getDraft(resource_mixin.getId(),(resource_mixinImpl)resource_mixin) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), resource_mixin.getClass(), false);
        copier.copy(clientModel, resource_mixin, null);
    }



}

