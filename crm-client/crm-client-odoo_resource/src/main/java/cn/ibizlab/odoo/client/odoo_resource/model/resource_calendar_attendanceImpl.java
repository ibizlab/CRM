package cn.ibizlab.odoo.client.odoo_resource.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iresource_calendar_attendance;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[resource_calendar_attendance] 对象
 */
public class resource_calendar_attendanceImpl implements Iresource_calendar_attendance,Serializable{

    /**
     * 资源的日历
     */
    public Integer calendar_id;

    @JsonIgnore
    public boolean calendar_idDirtyFlag;
    
    /**
     * 资源的日历
     */
    public String calendar_id_text;

    @JsonIgnore
    public boolean calendar_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 起始日期
     */
    public Timestamp date_from;

    @JsonIgnore
    public boolean date_fromDirtyFlag;
    
    /**
     * 结束日期
     */
    public Timestamp date_to;

    @JsonIgnore
    public boolean date_toDirtyFlag;
    
    /**
     * 星期
     */
    public String dayofweek;

    @JsonIgnore
    public boolean dayofweekDirtyFlag;
    
    /**
     * 日期
     */
    public String day_period;

    @JsonIgnore
    public boolean day_periodDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 工作起始
     */
    public Double hour_from;

    @JsonIgnore
    public boolean hour_fromDirtyFlag;
    
    /**
     * 工作截止
     */
    public Double hour_to;

    @JsonIgnore
    public boolean hour_toDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [资源的日历]
     */
    @JsonProperty("calendar_id")
    public Integer getCalendar_id(){
        return this.calendar_id ;
    }

    /**
     * 设置 [资源的日历]
     */
    @JsonProperty("calendar_id")
    public void setCalendar_id(Integer  calendar_id){
        this.calendar_id = calendar_id ;
        this.calendar_idDirtyFlag = true ;
    }

     /**
     * 获取 [资源的日历]脏标记
     */
    @JsonIgnore
    public boolean getCalendar_idDirtyFlag(){
        return this.calendar_idDirtyFlag ;
    }   

    /**
     * 获取 [资源的日历]
     */
    @JsonProperty("calendar_id_text")
    public String getCalendar_id_text(){
        return this.calendar_id_text ;
    }

    /**
     * 设置 [资源的日历]
     */
    @JsonProperty("calendar_id_text")
    public void setCalendar_id_text(String  calendar_id_text){
        this.calendar_id_text = calendar_id_text ;
        this.calendar_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [资源的日历]脏标记
     */
    @JsonIgnore
    public boolean getCalendar_id_textDirtyFlag(){
        return this.calendar_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [起始日期]
     */
    @JsonProperty("date_from")
    public Timestamp getDate_from(){
        return this.date_from ;
    }

    /**
     * 设置 [起始日期]
     */
    @JsonProperty("date_from")
    public void setDate_from(Timestamp  date_from){
        this.date_from = date_from ;
        this.date_fromDirtyFlag = true ;
    }

     /**
     * 获取 [起始日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_fromDirtyFlag(){
        return this.date_fromDirtyFlag ;
    }   

    /**
     * 获取 [结束日期]
     */
    @JsonProperty("date_to")
    public Timestamp getDate_to(){
        return this.date_to ;
    }

    /**
     * 设置 [结束日期]
     */
    @JsonProperty("date_to")
    public void setDate_to(Timestamp  date_to){
        this.date_to = date_to ;
        this.date_toDirtyFlag = true ;
    }

     /**
     * 获取 [结束日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_toDirtyFlag(){
        return this.date_toDirtyFlag ;
    }   

    /**
     * 获取 [星期]
     */
    @JsonProperty("dayofweek")
    public String getDayofweek(){
        return this.dayofweek ;
    }

    /**
     * 设置 [星期]
     */
    @JsonProperty("dayofweek")
    public void setDayofweek(String  dayofweek){
        this.dayofweek = dayofweek ;
        this.dayofweekDirtyFlag = true ;
    }

     /**
     * 获取 [星期]脏标记
     */
    @JsonIgnore
    public boolean getDayofweekDirtyFlag(){
        return this.dayofweekDirtyFlag ;
    }   

    /**
     * 获取 [日期]
     */
    @JsonProperty("day_period")
    public String getDay_period(){
        return this.day_period ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("day_period")
    public void setDay_period(String  day_period){
        this.day_period = day_period ;
        this.day_periodDirtyFlag = true ;
    }

     /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDay_periodDirtyFlag(){
        return this.day_periodDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [工作起始]
     */
    @JsonProperty("hour_from")
    public Double getHour_from(){
        return this.hour_from ;
    }

    /**
     * 设置 [工作起始]
     */
    @JsonProperty("hour_from")
    public void setHour_from(Double  hour_from){
        this.hour_from = hour_from ;
        this.hour_fromDirtyFlag = true ;
    }

     /**
     * 获取 [工作起始]脏标记
     */
    @JsonIgnore
    public boolean getHour_fromDirtyFlag(){
        return this.hour_fromDirtyFlag ;
    }   

    /**
     * 获取 [工作截止]
     */
    @JsonProperty("hour_to")
    public Double getHour_to(){
        return this.hour_to ;
    }

    /**
     * 设置 [工作截止]
     */
    @JsonProperty("hour_to")
    public void setHour_to(Double  hour_to){
        this.hour_to = hour_to ;
        this.hour_toDirtyFlag = true ;
    }

     /**
     * 获取 [工作截止]脏标记
     */
    @JsonIgnore
    public boolean getHour_toDirtyFlag(){
        return this.hour_toDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
