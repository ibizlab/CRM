package cn.ibizlab.odoo.client.odoo_resource.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iresource_calendar;
import cn.ibizlab.odoo.client.odoo_resource.model.resource_calendarImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[resource_calendar] 服务对象接口
 */
public interface resource_calendarFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_resource/resource_calendars/{id}")
    public resource_calendarImpl update(@PathVariable("id") Integer id,@RequestBody resource_calendarImpl resource_calendar);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_resource/resource_calendars/removebatch")
    public resource_calendarImpl removeBatch(@RequestBody List<resource_calendarImpl> resource_calendars);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_resource/resource_calendars/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_calendars/fetchdefault")
    public Page<resource_calendarImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_calendars/{id}")
    public resource_calendarImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_resource/resource_calendars/createbatch")
    public resource_calendarImpl createBatch(@RequestBody List<resource_calendarImpl> resource_calendars);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_resource/resource_calendars/updatebatch")
    public resource_calendarImpl updateBatch(@RequestBody List<resource_calendarImpl> resource_calendars);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_resource/resource_calendars")
    public resource_calendarImpl create(@RequestBody resource_calendarImpl resource_calendar);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_calendars/select")
    public Page<resource_calendarImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_calendars/{id}/getdraft")
    public resource_calendarImpl getDraft(@PathVariable("id") Integer id,@RequestBody resource_calendarImpl resource_calendar);



}
