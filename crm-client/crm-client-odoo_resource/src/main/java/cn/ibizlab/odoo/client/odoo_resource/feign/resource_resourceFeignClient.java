package cn.ibizlab.odoo.client.odoo_resource.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iresource_resource;
import cn.ibizlab.odoo.client.odoo_resource.model.resource_resourceImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[resource_resource] 服务对象接口
 */
public interface resource_resourceFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_resource/resource_resources/updatebatch")
    public resource_resourceImpl updateBatch(@RequestBody List<resource_resourceImpl> resource_resources);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_resource/resource_resources/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_resource/resource_resources/removebatch")
    public resource_resourceImpl removeBatch(@RequestBody List<resource_resourceImpl> resource_resources);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_resources/{id}")
    public resource_resourceImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_resource/resource_resources/createbatch")
    public resource_resourceImpl createBatch(@RequestBody List<resource_resourceImpl> resource_resources);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_resources/fetchdefault")
    public Page<resource_resourceImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_resource/resource_resources/{id}")
    public resource_resourceImpl update(@PathVariable("id") Integer id,@RequestBody resource_resourceImpl resource_resource);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_resource/resource_resources")
    public resource_resourceImpl create(@RequestBody resource_resourceImpl resource_resource);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_resources/select")
    public Page<resource_resourceImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_resources/{id}/getdraft")
    public resource_resourceImpl getDraft(@PathVariable("id") Integer id,@RequestBody resource_resourceImpl resource_resource);



}
