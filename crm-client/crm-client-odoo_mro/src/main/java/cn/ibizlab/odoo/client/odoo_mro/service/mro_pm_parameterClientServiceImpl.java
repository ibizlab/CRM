package cn.ibizlab.odoo.client.odoo_mro.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imro_pm_parameter;
import cn.ibizlab.odoo.client.odoo_mro.config.odoo_mroClientProperties;
import cn.ibizlab.odoo.core.client.service.Imro_pm_parameterClientService;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_pm_parameterImpl;
import cn.ibizlab.odoo.client.odoo_mro.feign.mro_pm_parameterFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mro_pm_parameter] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mro_pm_parameterClientServiceImpl implements Imro_pm_parameterClientService {

    mro_pm_parameterFeignClient mro_pm_parameterFeignClient;

    @Autowired
    public mro_pm_parameterClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mroClientProperties odoo_mroClientProperties) {
        if (odoo_mroClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mro_pm_parameterFeignClient = nameBuilder.target(mro_pm_parameterFeignClient.class,"http://"+odoo_mroClientProperties.getServiceId()+"/") ;
		}else if (odoo_mroClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mro_pm_parameterFeignClient = nameBuilder.target(mro_pm_parameterFeignClient.class,odoo_mroClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imro_pm_parameter createModel() {
		return new mro_pm_parameterImpl();
	}


    public Page<Imro_pm_parameter> fetchDefault(SearchContext context){
        Page<mro_pm_parameterImpl> page = this.mro_pm_parameterFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Imro_pm_parameter> mro_pm_parameters){
        if(mro_pm_parameters!=null){
            List<mro_pm_parameterImpl> list = new ArrayList<mro_pm_parameterImpl>();
            for(Imro_pm_parameter imro_pm_parameter :mro_pm_parameters){
                list.add((mro_pm_parameterImpl)imro_pm_parameter) ;
            }
            mro_pm_parameterFeignClient.removeBatch(list) ;
        }
    }


    public void get(Imro_pm_parameter mro_pm_parameter){
        Imro_pm_parameter clientModel = mro_pm_parameterFeignClient.get(mro_pm_parameter.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_pm_parameter.getClass(), false);
        copier.copy(clientModel, mro_pm_parameter, null);
    }


    public void create(Imro_pm_parameter mro_pm_parameter){
        Imro_pm_parameter clientModel = mro_pm_parameterFeignClient.create((mro_pm_parameterImpl)mro_pm_parameter) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_pm_parameter.getClass(), false);
        copier.copy(clientModel, mro_pm_parameter, null);
    }


    public void updateBatch(List<Imro_pm_parameter> mro_pm_parameters){
        if(mro_pm_parameters!=null){
            List<mro_pm_parameterImpl> list = new ArrayList<mro_pm_parameterImpl>();
            for(Imro_pm_parameter imro_pm_parameter :mro_pm_parameters){
                list.add((mro_pm_parameterImpl)imro_pm_parameter) ;
            }
            mro_pm_parameterFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Imro_pm_parameter> mro_pm_parameters){
        if(mro_pm_parameters!=null){
            List<mro_pm_parameterImpl> list = new ArrayList<mro_pm_parameterImpl>();
            for(Imro_pm_parameter imro_pm_parameter :mro_pm_parameters){
                list.add((mro_pm_parameterImpl)imro_pm_parameter) ;
            }
            mro_pm_parameterFeignClient.createBatch(list) ;
        }
    }


    public void remove(Imro_pm_parameter mro_pm_parameter){
        mro_pm_parameterFeignClient.remove(mro_pm_parameter.getId()) ;
    }


    public void update(Imro_pm_parameter mro_pm_parameter){
        Imro_pm_parameter clientModel = mro_pm_parameterFeignClient.update(mro_pm_parameter.getId(),(mro_pm_parameterImpl)mro_pm_parameter) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_pm_parameter.getClass(), false);
        copier.copy(clientModel, mro_pm_parameter, null);
    }


    public Page<Imro_pm_parameter> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imro_pm_parameter mro_pm_parameter){
        Imro_pm_parameter clientModel = mro_pm_parameterFeignClient.getDraft(mro_pm_parameter.getId(),(mro_pm_parameterImpl)mro_pm_parameter) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_pm_parameter.getClass(), false);
        copier.copy(clientModel, mro_pm_parameter, null);
    }



}

