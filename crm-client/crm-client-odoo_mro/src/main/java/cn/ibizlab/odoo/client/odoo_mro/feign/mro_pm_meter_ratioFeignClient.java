package cn.ibizlab.odoo.client.odoo_mro.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imro_pm_meter_ratio;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_pm_meter_ratioImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mro_pm_meter_ratio] 服务对象接口
 */
public interface mro_pm_meter_ratioFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_meter_ratios/{id}")
    public mro_pm_meter_ratioImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_meter_ratios")
    public mro_pm_meter_ratioImpl create(@RequestBody mro_pm_meter_ratioImpl mro_pm_meter_ratio);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_meter_ratios/{id}")
    public mro_pm_meter_ratioImpl update(@PathVariable("id") Integer id,@RequestBody mro_pm_meter_ratioImpl mro_pm_meter_ratio);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_meter_ratios/removebatch")
    public mro_pm_meter_ratioImpl removeBatch(@RequestBody List<mro_pm_meter_ratioImpl> mro_pm_meter_ratios);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_meter_ratios/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_meter_ratios/updatebatch")
    public mro_pm_meter_ratioImpl updateBatch(@RequestBody List<mro_pm_meter_ratioImpl> mro_pm_meter_ratios);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_meter_ratios/createbatch")
    public mro_pm_meter_ratioImpl createBatch(@RequestBody List<mro_pm_meter_ratioImpl> mro_pm_meter_ratios);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_meter_ratios/fetchdefault")
    public Page<mro_pm_meter_ratioImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_meter_ratios/select")
    public Page<mro_pm_meter_ratioImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_meter_ratios/{id}/getdraft")
    public mro_pm_meter_ratioImpl getDraft(@PathVariable("id") Integer id,@RequestBody mro_pm_meter_ratioImpl mro_pm_meter_ratio);



}
