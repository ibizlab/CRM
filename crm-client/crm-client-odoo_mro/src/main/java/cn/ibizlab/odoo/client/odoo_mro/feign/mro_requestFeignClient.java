package cn.ibizlab.odoo.client.odoo_mro.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imro_request;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_requestImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mro_request] 服务对象接口
 */
public interface mro_requestFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_requests/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_requests")
    public mro_requestImpl create(@RequestBody mro_requestImpl mro_request);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_requests/removebatch")
    public mro_requestImpl removeBatch(@RequestBody List<mro_requestImpl> mro_requests);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_requests/{id}")
    public mro_requestImpl update(@PathVariable("id") Integer id,@RequestBody mro_requestImpl mro_request);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_requests/createbatch")
    public mro_requestImpl createBatch(@RequestBody List<mro_requestImpl> mro_requests);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_requests/updatebatch")
    public mro_requestImpl updateBatch(@RequestBody List<mro_requestImpl> mro_requests);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_requests/fetchdefault")
    public Page<mro_requestImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_requests/{id}")
    public mro_requestImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_requests/select")
    public Page<mro_requestImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_requests/{id}/getdraft")
    public mro_requestImpl getDraft(@PathVariable("id") Integer id,@RequestBody mro_requestImpl mro_request);



}
