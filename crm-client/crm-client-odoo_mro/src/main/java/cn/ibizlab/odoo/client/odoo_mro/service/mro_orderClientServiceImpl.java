package cn.ibizlab.odoo.client.odoo_mro.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imro_order;
import cn.ibizlab.odoo.client.odoo_mro.config.odoo_mroClientProperties;
import cn.ibizlab.odoo.core.client.service.Imro_orderClientService;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_orderImpl;
import cn.ibizlab.odoo.client.odoo_mro.feign.mro_orderFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mro_order] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mro_orderClientServiceImpl implements Imro_orderClientService {

    mro_orderFeignClient mro_orderFeignClient;

    @Autowired
    public mro_orderClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mroClientProperties odoo_mroClientProperties) {
        if (odoo_mroClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mro_orderFeignClient = nameBuilder.target(mro_orderFeignClient.class,"http://"+odoo_mroClientProperties.getServiceId()+"/") ;
		}else if (odoo_mroClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mro_orderFeignClient = nameBuilder.target(mro_orderFeignClient.class,odoo_mroClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imro_order createModel() {
		return new mro_orderImpl();
	}


    public void update(Imro_order mro_order){
        Imro_order clientModel = mro_orderFeignClient.update(mro_order.getId(),(mro_orderImpl)mro_order) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_order.getClass(), false);
        copier.copy(clientModel, mro_order, null);
    }


    public void createBatch(List<Imro_order> mro_orders){
        if(mro_orders!=null){
            List<mro_orderImpl> list = new ArrayList<mro_orderImpl>();
            for(Imro_order imro_order :mro_orders){
                list.add((mro_orderImpl)imro_order) ;
            }
            mro_orderFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Imro_order> mro_orders){
        if(mro_orders!=null){
            List<mro_orderImpl> list = new ArrayList<mro_orderImpl>();
            for(Imro_order imro_order :mro_orders){
                list.add((mro_orderImpl)imro_order) ;
            }
            mro_orderFeignClient.removeBatch(list) ;
        }
    }


    public void create(Imro_order mro_order){
        Imro_order clientModel = mro_orderFeignClient.create((mro_orderImpl)mro_order) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_order.getClass(), false);
        copier.copy(clientModel, mro_order, null);
    }


    public void updateBatch(List<Imro_order> mro_orders){
        if(mro_orders!=null){
            List<mro_orderImpl> list = new ArrayList<mro_orderImpl>();
            for(Imro_order imro_order :mro_orders){
                list.add((mro_orderImpl)imro_order) ;
            }
            mro_orderFeignClient.updateBatch(list) ;
        }
    }


    public void get(Imro_order mro_order){
        Imro_order clientModel = mro_orderFeignClient.get(mro_order.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_order.getClass(), false);
        copier.copy(clientModel, mro_order, null);
    }


    public void remove(Imro_order mro_order){
        mro_orderFeignClient.remove(mro_order.getId()) ;
    }


    public Page<Imro_order> fetchDefault(SearchContext context){
        Page<mro_orderImpl> page = this.mro_orderFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Imro_order> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imro_order mro_order){
        Imro_order clientModel = mro_orderFeignClient.getDraft(mro_order.getId(),(mro_orderImpl)mro_order) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_order.getClass(), false);
        copier.copy(clientModel, mro_order, null);
    }



}

