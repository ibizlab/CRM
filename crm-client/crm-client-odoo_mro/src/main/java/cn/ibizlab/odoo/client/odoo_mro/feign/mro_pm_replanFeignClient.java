package cn.ibizlab.odoo.client.odoo_mro.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imro_pm_replan;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_pm_replanImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mro_pm_replan] 服务对象接口
 */
public interface mro_pm_replanFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_replans/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_replans/{id}")
    public mro_pm_replanImpl update(@PathVariable("id") Integer id,@RequestBody mro_pm_replanImpl mro_pm_replan);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_replans/{id}")
    public mro_pm_replanImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_replans/updatebatch")
    public mro_pm_replanImpl updateBatch(@RequestBody List<mro_pm_replanImpl> mro_pm_replans);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_replans/fetchdefault")
    public Page<mro_pm_replanImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_replans/createbatch")
    public mro_pm_replanImpl createBatch(@RequestBody List<mro_pm_replanImpl> mro_pm_replans);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_replans/removebatch")
    public mro_pm_replanImpl removeBatch(@RequestBody List<mro_pm_replanImpl> mro_pm_replans);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_replans")
    public mro_pm_replanImpl create(@RequestBody mro_pm_replanImpl mro_pm_replan);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_replans/select")
    public Page<mro_pm_replanImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_replans/{id}/getdraft")
    public mro_pm_replanImpl getDraft(@PathVariable("id") Integer id,@RequestBody mro_pm_replanImpl mro_pm_replan);



}
