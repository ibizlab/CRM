package cn.ibizlab.odoo.client.odoo_mro.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imro_pm_replan;
import cn.ibizlab.odoo.client.odoo_mro.config.odoo_mroClientProperties;
import cn.ibizlab.odoo.core.client.service.Imro_pm_replanClientService;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_pm_replanImpl;
import cn.ibizlab.odoo.client.odoo_mro.feign.mro_pm_replanFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mro_pm_replan] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mro_pm_replanClientServiceImpl implements Imro_pm_replanClientService {

    mro_pm_replanFeignClient mro_pm_replanFeignClient;

    @Autowired
    public mro_pm_replanClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mroClientProperties odoo_mroClientProperties) {
        if (odoo_mroClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mro_pm_replanFeignClient = nameBuilder.target(mro_pm_replanFeignClient.class,"http://"+odoo_mroClientProperties.getServiceId()+"/") ;
		}else if (odoo_mroClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mro_pm_replanFeignClient = nameBuilder.target(mro_pm_replanFeignClient.class,odoo_mroClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imro_pm_replan createModel() {
		return new mro_pm_replanImpl();
	}


    public void remove(Imro_pm_replan mro_pm_replan){
        mro_pm_replanFeignClient.remove(mro_pm_replan.getId()) ;
    }


    public void update(Imro_pm_replan mro_pm_replan){
        Imro_pm_replan clientModel = mro_pm_replanFeignClient.update(mro_pm_replan.getId(),(mro_pm_replanImpl)mro_pm_replan) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_pm_replan.getClass(), false);
        copier.copy(clientModel, mro_pm_replan, null);
    }


    public void get(Imro_pm_replan mro_pm_replan){
        Imro_pm_replan clientModel = mro_pm_replanFeignClient.get(mro_pm_replan.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_pm_replan.getClass(), false);
        copier.copy(clientModel, mro_pm_replan, null);
    }


    public void updateBatch(List<Imro_pm_replan> mro_pm_replans){
        if(mro_pm_replans!=null){
            List<mro_pm_replanImpl> list = new ArrayList<mro_pm_replanImpl>();
            for(Imro_pm_replan imro_pm_replan :mro_pm_replans){
                list.add((mro_pm_replanImpl)imro_pm_replan) ;
            }
            mro_pm_replanFeignClient.updateBatch(list) ;
        }
    }


    public Page<Imro_pm_replan> fetchDefault(SearchContext context){
        Page<mro_pm_replanImpl> page = this.mro_pm_replanFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Imro_pm_replan> mro_pm_replans){
        if(mro_pm_replans!=null){
            List<mro_pm_replanImpl> list = new ArrayList<mro_pm_replanImpl>();
            for(Imro_pm_replan imro_pm_replan :mro_pm_replans){
                list.add((mro_pm_replanImpl)imro_pm_replan) ;
            }
            mro_pm_replanFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Imro_pm_replan> mro_pm_replans){
        if(mro_pm_replans!=null){
            List<mro_pm_replanImpl> list = new ArrayList<mro_pm_replanImpl>();
            for(Imro_pm_replan imro_pm_replan :mro_pm_replans){
                list.add((mro_pm_replanImpl)imro_pm_replan) ;
            }
            mro_pm_replanFeignClient.removeBatch(list) ;
        }
    }


    public void create(Imro_pm_replan mro_pm_replan){
        Imro_pm_replan clientModel = mro_pm_replanFeignClient.create((mro_pm_replanImpl)mro_pm_replan) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_pm_replan.getClass(), false);
        copier.copy(clientModel, mro_pm_replan, null);
    }


    public Page<Imro_pm_replan> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imro_pm_replan mro_pm_replan){
        Imro_pm_replan clientModel = mro_pm_replanFeignClient.getDraft(mro_pm_replan.getId(),(mro_pm_replanImpl)mro_pm_replan) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_pm_replan.getClass(), false);
        copier.copy(clientModel, mro_pm_replan, null);
    }



}

