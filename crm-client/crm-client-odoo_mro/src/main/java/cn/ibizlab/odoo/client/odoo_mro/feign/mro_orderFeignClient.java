package cn.ibizlab.odoo.client.odoo_mro.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imro_order;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_orderImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mro_order] 服务对象接口
 */
public interface mro_orderFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_orders/{id}")
    public mro_orderImpl update(@PathVariable("id") Integer id,@RequestBody mro_orderImpl mro_order);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_orders/createbatch")
    public mro_orderImpl createBatch(@RequestBody List<mro_orderImpl> mro_orders);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_orders/removebatch")
    public mro_orderImpl removeBatch(@RequestBody List<mro_orderImpl> mro_orders);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_orders")
    public mro_orderImpl create(@RequestBody mro_orderImpl mro_order);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_orders/updatebatch")
    public mro_orderImpl updateBatch(@RequestBody List<mro_orderImpl> mro_orders);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_orders/{id}")
    public mro_orderImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_orders/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_orders/fetchdefault")
    public Page<mro_orderImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_orders/select")
    public Page<mro_orderImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_orders/{id}/getdraft")
    public mro_orderImpl getDraft(@PathVariable("id") Integer id,@RequestBody mro_orderImpl mro_order);



}
