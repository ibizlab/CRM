package cn.ibizlab.odoo.client.odoo_mro.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imro_workorder;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_workorderImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mro_workorder] 服务对象接口
 */
public interface mro_workorderFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_workorders")
    public mro_workorderImpl create(@RequestBody mro_workorderImpl mro_workorder);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_workorders/{id}")
    public mro_workorderImpl update(@PathVariable("id") Integer id,@RequestBody mro_workorderImpl mro_workorder);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_workorders/{id}")
    public mro_workorderImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_workorders/removebatch")
    public mro_workorderImpl removeBatch(@RequestBody List<mro_workorderImpl> mro_workorders);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_workorders/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_workorders/updatebatch")
    public mro_workorderImpl updateBatch(@RequestBody List<mro_workorderImpl> mro_workorders);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_workorders/createbatch")
    public mro_workorderImpl createBatch(@RequestBody List<mro_workorderImpl> mro_workorders);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_workorders/fetchdefault")
    public Page<mro_workorderImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_workorders/select")
    public Page<mro_workorderImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_workorders/{id}/getdraft")
    public mro_workorderImpl getDraft(@PathVariable("id") Integer id,@RequestBody mro_workorderImpl mro_workorder);



}
