package cn.ibizlab.odoo.client.odoo_mro.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imro_workorder;
import cn.ibizlab.odoo.client.odoo_mro.config.odoo_mroClientProperties;
import cn.ibizlab.odoo.core.client.service.Imro_workorderClientService;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_workorderImpl;
import cn.ibizlab.odoo.client.odoo_mro.feign.mro_workorderFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mro_workorder] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mro_workorderClientServiceImpl implements Imro_workorderClientService {

    mro_workorderFeignClient mro_workorderFeignClient;

    @Autowired
    public mro_workorderClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mroClientProperties odoo_mroClientProperties) {
        if (odoo_mroClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mro_workorderFeignClient = nameBuilder.target(mro_workorderFeignClient.class,"http://"+odoo_mroClientProperties.getServiceId()+"/") ;
		}else if (odoo_mroClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mro_workorderFeignClient = nameBuilder.target(mro_workorderFeignClient.class,odoo_mroClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imro_workorder createModel() {
		return new mro_workorderImpl();
	}


    public void create(Imro_workorder mro_workorder){
        Imro_workorder clientModel = mro_workorderFeignClient.create((mro_workorderImpl)mro_workorder) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_workorder.getClass(), false);
        copier.copy(clientModel, mro_workorder, null);
    }


    public void update(Imro_workorder mro_workorder){
        Imro_workorder clientModel = mro_workorderFeignClient.update(mro_workorder.getId(),(mro_workorderImpl)mro_workorder) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_workorder.getClass(), false);
        copier.copy(clientModel, mro_workorder, null);
    }


    public void get(Imro_workorder mro_workorder){
        Imro_workorder clientModel = mro_workorderFeignClient.get(mro_workorder.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_workorder.getClass(), false);
        copier.copy(clientModel, mro_workorder, null);
    }


    public void removeBatch(List<Imro_workorder> mro_workorders){
        if(mro_workorders!=null){
            List<mro_workorderImpl> list = new ArrayList<mro_workorderImpl>();
            for(Imro_workorder imro_workorder :mro_workorders){
                list.add((mro_workorderImpl)imro_workorder) ;
            }
            mro_workorderFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Imro_workorder mro_workorder){
        mro_workorderFeignClient.remove(mro_workorder.getId()) ;
    }


    public void updateBatch(List<Imro_workorder> mro_workorders){
        if(mro_workorders!=null){
            List<mro_workorderImpl> list = new ArrayList<mro_workorderImpl>();
            for(Imro_workorder imro_workorder :mro_workorders){
                list.add((mro_workorderImpl)imro_workorder) ;
            }
            mro_workorderFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Imro_workorder> mro_workorders){
        if(mro_workorders!=null){
            List<mro_workorderImpl> list = new ArrayList<mro_workorderImpl>();
            for(Imro_workorder imro_workorder :mro_workorders){
                list.add((mro_workorderImpl)imro_workorder) ;
            }
            mro_workorderFeignClient.createBatch(list) ;
        }
    }


    public Page<Imro_workorder> fetchDefault(SearchContext context){
        Page<mro_workorderImpl> page = this.mro_workorderFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Imro_workorder> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imro_workorder mro_workorder){
        Imro_workorder clientModel = mro_workorderFeignClient.getDraft(mro_workorder.getId(),(mro_workorderImpl)mro_workorder) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mro_workorder.getClass(), false);
        copier.copy(clientModel, mro_workorder, null);
    }



}

