package cn.ibizlab.odoo.client.odoo_mro.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Imro_order;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[mro_order] 对象
 */
public class mro_orderImpl implements Imro_order,Serializable{

    /**
     * Asset
     */
    public Integer asset_id;

    @JsonIgnore
    public boolean asset_idDirtyFlag;
    
    /**
     * Asset
     */
    public String asset_id_text;

    @JsonIgnore
    public boolean asset_id_textDirtyFlag;
    
    /**
     * Asset Category
     */
    public String category_ids;

    @JsonIgnore
    public boolean category_idsDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * Execution Date
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_execution;

    @JsonIgnore
    public boolean date_executionDirtyFlag;
    
    /**
     * Planned Date
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_planned;

    @JsonIgnore
    public boolean date_plannedDirtyFlag;
    
    /**
     * 计划日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_scheduled;

    @JsonIgnore
    public boolean date_scheduledDirtyFlag;
    
    /**
     * 说明
     */
    public String description;

    @JsonIgnore
    public boolean descriptionDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * Documentation Description
     */
    public String documentation_description;

    @JsonIgnore
    public boolean documentation_descriptionDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * Labor Description
     */
    public String labor_description;

    @JsonIgnore
    public boolean labor_descriptionDirtyFlag;
    
    /**
     * 保养类型
     */
    public String maintenance_type;

    @JsonIgnore
    public boolean maintenance_typeDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 错误个数
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 消息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 前置操作
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 操作次数
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 编号
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * Operations Description
     */
    public String operations_description;

    @JsonIgnore
    public boolean operations_descriptionDirtyFlag;
    
    /**
     * 源文档
     */
    public String origin;

    @JsonIgnore
    public boolean originDirtyFlag;
    
    /**
     * Planned parts
     */
    public String parts_lines;

    @JsonIgnore
    public boolean parts_linesDirtyFlag;
    
    /**
     * Parts Moved Lines
     */
    public String parts_moved_lines;

    @JsonIgnore
    public boolean parts_moved_linesDirtyFlag;
    
    /**
     * Parts Move Lines
     */
    public String parts_move_lines;

    @JsonIgnore
    public boolean parts_move_linesDirtyFlag;
    
    /**
     * Parts Ready Lines
     */
    public String parts_ready_lines;

    @JsonIgnore
    public boolean parts_ready_linesDirtyFlag;
    
    /**
     * Problem Description
     */
    public String problem_description;

    @JsonIgnore
    public boolean problem_descriptionDirtyFlag;
    
    /**
     * Procurement group
     */
    public Integer procurement_group_id;

    @JsonIgnore
    public boolean procurement_group_idDirtyFlag;
    
    /**
     * 请求
     */
    public Integer request_id;

    @JsonIgnore
    public boolean request_idDirtyFlag;
    
    /**
     * 请求
     */
    public String request_id_text;

    @JsonIgnore
    public boolean request_id_textDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * Task
     */
    public Integer task_id;

    @JsonIgnore
    public boolean task_idDirtyFlag;
    
    /**
     * Task
     */
    public String task_id_text;

    @JsonIgnore
    public boolean task_id_textDirtyFlag;
    
    /**
     * Tools Description
     */
    public String tools_description;

    @JsonIgnore
    public boolean tools_descriptionDirtyFlag;
    
    /**
     * 负责人
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 负责人
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 网站消息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 工单
     */
    public Integer wo_id;

    @JsonIgnore
    public boolean wo_idDirtyFlag;
    
    /**
     * 工单
     */
    public String wo_id_text;

    @JsonIgnore
    public boolean wo_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [Asset]
     */
    @JsonProperty("asset_id")
    public Integer getAsset_id(){
        return this.asset_id ;
    }

    /**
     * 设置 [Asset]
     */
    @JsonProperty("asset_id")
    public void setAsset_id(Integer  asset_id){
        this.asset_id = asset_id ;
        this.asset_idDirtyFlag = true ;
    }

     /**
     * 获取 [Asset]脏标记
     */
    @JsonIgnore
    public boolean getAsset_idDirtyFlag(){
        return this.asset_idDirtyFlag ;
    }   

    /**
     * 获取 [Asset]
     */
    @JsonProperty("asset_id_text")
    public String getAsset_id_text(){
        return this.asset_id_text ;
    }

    /**
     * 设置 [Asset]
     */
    @JsonProperty("asset_id_text")
    public void setAsset_id_text(String  asset_id_text){
        this.asset_id_text = asset_id_text ;
        this.asset_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [Asset]脏标记
     */
    @JsonIgnore
    public boolean getAsset_id_textDirtyFlag(){
        return this.asset_id_textDirtyFlag ;
    }   

    /**
     * 获取 [Asset Category]
     */
    @JsonProperty("category_ids")
    public String getCategory_ids(){
        return this.category_ids ;
    }

    /**
     * 设置 [Asset Category]
     */
    @JsonProperty("category_ids")
    public void setCategory_ids(String  category_ids){
        this.category_ids = category_ids ;
        this.category_idsDirtyFlag = true ;
    }

     /**
     * 获取 [Asset Category]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idsDirtyFlag(){
        return this.category_idsDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [Execution Date]
     */
    @JsonProperty("date_execution")
    public Timestamp getDate_execution(){
        return this.date_execution ;
    }

    /**
     * 设置 [Execution Date]
     */
    @JsonProperty("date_execution")
    public void setDate_execution(Timestamp  date_execution){
        this.date_execution = date_execution ;
        this.date_executionDirtyFlag = true ;
    }

     /**
     * 获取 [Execution Date]脏标记
     */
    @JsonIgnore
    public boolean getDate_executionDirtyFlag(){
        return this.date_executionDirtyFlag ;
    }   

    /**
     * 获取 [Planned Date]
     */
    @JsonProperty("date_planned")
    public Timestamp getDate_planned(){
        return this.date_planned ;
    }

    /**
     * 设置 [Planned Date]
     */
    @JsonProperty("date_planned")
    public void setDate_planned(Timestamp  date_planned){
        this.date_planned = date_planned ;
        this.date_plannedDirtyFlag = true ;
    }

     /**
     * 获取 [Planned Date]脏标记
     */
    @JsonIgnore
    public boolean getDate_plannedDirtyFlag(){
        return this.date_plannedDirtyFlag ;
    }   

    /**
     * 获取 [计划日期]
     */
    @JsonProperty("date_scheduled")
    public Timestamp getDate_scheduled(){
        return this.date_scheduled ;
    }

    /**
     * 设置 [计划日期]
     */
    @JsonProperty("date_scheduled")
    public void setDate_scheduled(Timestamp  date_scheduled){
        this.date_scheduled = date_scheduled ;
        this.date_scheduledDirtyFlag = true ;
    }

     /**
     * 获取 [计划日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_scheduledDirtyFlag(){
        return this.date_scheduledDirtyFlag ;
    }   

    /**
     * 获取 [说明]
     */
    @JsonProperty("description")
    public String getDescription(){
        return this.description ;
    }

    /**
     * 设置 [说明]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [说明]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return this.descriptionDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [Documentation Description]
     */
    @JsonProperty("documentation_description")
    public String getDocumentation_description(){
        return this.documentation_description ;
    }

    /**
     * 设置 [Documentation Description]
     */
    @JsonProperty("documentation_description")
    public void setDocumentation_description(String  documentation_description){
        this.documentation_description = documentation_description ;
        this.documentation_descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [Documentation Description]脏标记
     */
    @JsonIgnore
    public boolean getDocumentation_descriptionDirtyFlag(){
        return this.documentation_descriptionDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [Labor Description]
     */
    @JsonProperty("labor_description")
    public String getLabor_description(){
        return this.labor_description ;
    }

    /**
     * 设置 [Labor Description]
     */
    @JsonProperty("labor_description")
    public void setLabor_description(String  labor_description){
        this.labor_description = labor_description ;
        this.labor_descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [Labor Description]脏标记
     */
    @JsonIgnore
    public boolean getLabor_descriptionDirtyFlag(){
        return this.labor_descriptionDirtyFlag ;
    }   

    /**
     * 获取 [保养类型]
     */
    @JsonProperty("maintenance_type")
    public String getMaintenance_type(){
        return this.maintenance_type ;
    }

    /**
     * 设置 [保养类型]
     */
    @JsonProperty("maintenance_type")
    public void setMaintenance_type(String  maintenance_type){
        this.maintenance_type = maintenance_type ;
        this.maintenance_typeDirtyFlag = true ;
    }

     /**
     * 获取 [保养类型]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_typeDirtyFlag(){
        return this.maintenance_typeDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [错误个数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误个数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [错误个数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [前置操作]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [前置操作]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [前置操作]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [操作次数]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [操作次数]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [操作次数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [编号]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [编号]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [编号]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [Operations Description]
     */
    @JsonProperty("operations_description")
    public String getOperations_description(){
        return this.operations_description ;
    }

    /**
     * 设置 [Operations Description]
     */
    @JsonProperty("operations_description")
    public void setOperations_description(String  operations_description){
        this.operations_description = operations_description ;
        this.operations_descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [Operations Description]脏标记
     */
    @JsonIgnore
    public boolean getOperations_descriptionDirtyFlag(){
        return this.operations_descriptionDirtyFlag ;
    }   

    /**
     * 获取 [源文档]
     */
    @JsonProperty("origin")
    public String getOrigin(){
        return this.origin ;
    }

    /**
     * 设置 [源文档]
     */
    @JsonProperty("origin")
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.originDirtyFlag = true ;
    }

     /**
     * 获取 [源文档]脏标记
     */
    @JsonIgnore
    public boolean getOriginDirtyFlag(){
        return this.originDirtyFlag ;
    }   

    /**
     * 获取 [Planned parts]
     */
    @JsonProperty("parts_lines")
    public String getParts_lines(){
        return this.parts_lines ;
    }

    /**
     * 设置 [Planned parts]
     */
    @JsonProperty("parts_lines")
    public void setParts_lines(String  parts_lines){
        this.parts_lines = parts_lines ;
        this.parts_linesDirtyFlag = true ;
    }

     /**
     * 获取 [Planned parts]脏标记
     */
    @JsonIgnore
    public boolean getParts_linesDirtyFlag(){
        return this.parts_linesDirtyFlag ;
    }   

    /**
     * 获取 [Parts Moved Lines]
     */
    @JsonProperty("parts_moved_lines")
    public String getParts_moved_lines(){
        return this.parts_moved_lines ;
    }

    /**
     * 设置 [Parts Moved Lines]
     */
    @JsonProperty("parts_moved_lines")
    public void setParts_moved_lines(String  parts_moved_lines){
        this.parts_moved_lines = parts_moved_lines ;
        this.parts_moved_linesDirtyFlag = true ;
    }

     /**
     * 获取 [Parts Moved Lines]脏标记
     */
    @JsonIgnore
    public boolean getParts_moved_linesDirtyFlag(){
        return this.parts_moved_linesDirtyFlag ;
    }   

    /**
     * 获取 [Parts Move Lines]
     */
    @JsonProperty("parts_move_lines")
    public String getParts_move_lines(){
        return this.parts_move_lines ;
    }

    /**
     * 设置 [Parts Move Lines]
     */
    @JsonProperty("parts_move_lines")
    public void setParts_move_lines(String  parts_move_lines){
        this.parts_move_lines = parts_move_lines ;
        this.parts_move_linesDirtyFlag = true ;
    }

     /**
     * 获取 [Parts Move Lines]脏标记
     */
    @JsonIgnore
    public boolean getParts_move_linesDirtyFlag(){
        return this.parts_move_linesDirtyFlag ;
    }   

    /**
     * 获取 [Parts Ready Lines]
     */
    @JsonProperty("parts_ready_lines")
    public String getParts_ready_lines(){
        return this.parts_ready_lines ;
    }

    /**
     * 设置 [Parts Ready Lines]
     */
    @JsonProperty("parts_ready_lines")
    public void setParts_ready_lines(String  parts_ready_lines){
        this.parts_ready_lines = parts_ready_lines ;
        this.parts_ready_linesDirtyFlag = true ;
    }

     /**
     * 获取 [Parts Ready Lines]脏标记
     */
    @JsonIgnore
    public boolean getParts_ready_linesDirtyFlag(){
        return this.parts_ready_linesDirtyFlag ;
    }   

    /**
     * 获取 [Problem Description]
     */
    @JsonProperty("problem_description")
    public String getProblem_description(){
        return this.problem_description ;
    }

    /**
     * 设置 [Problem Description]
     */
    @JsonProperty("problem_description")
    public void setProblem_description(String  problem_description){
        this.problem_description = problem_description ;
        this.problem_descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [Problem Description]脏标记
     */
    @JsonIgnore
    public boolean getProblem_descriptionDirtyFlag(){
        return this.problem_descriptionDirtyFlag ;
    }   

    /**
     * 获取 [Procurement group]
     */
    @JsonProperty("procurement_group_id")
    public Integer getProcurement_group_id(){
        return this.procurement_group_id ;
    }

    /**
     * 设置 [Procurement group]
     */
    @JsonProperty("procurement_group_id")
    public void setProcurement_group_id(Integer  procurement_group_id){
        this.procurement_group_id = procurement_group_id ;
        this.procurement_group_idDirtyFlag = true ;
    }

     /**
     * 获取 [Procurement group]脏标记
     */
    @JsonIgnore
    public boolean getProcurement_group_idDirtyFlag(){
        return this.procurement_group_idDirtyFlag ;
    }   

    /**
     * 获取 [请求]
     */
    @JsonProperty("request_id")
    public Integer getRequest_id(){
        return this.request_id ;
    }

    /**
     * 设置 [请求]
     */
    @JsonProperty("request_id")
    public void setRequest_id(Integer  request_id){
        this.request_id = request_id ;
        this.request_idDirtyFlag = true ;
    }

     /**
     * 获取 [请求]脏标记
     */
    @JsonIgnore
    public boolean getRequest_idDirtyFlag(){
        return this.request_idDirtyFlag ;
    }   

    /**
     * 获取 [请求]
     */
    @JsonProperty("request_id_text")
    public String getRequest_id_text(){
        return this.request_id_text ;
    }

    /**
     * 设置 [请求]
     */
    @JsonProperty("request_id_text")
    public void setRequest_id_text(String  request_id_text){
        this.request_id_text = request_id_text ;
        this.request_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [请求]脏标记
     */
    @JsonIgnore
    public boolean getRequest_id_textDirtyFlag(){
        return this.request_id_textDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [Task]
     */
    @JsonProperty("task_id")
    public Integer getTask_id(){
        return this.task_id ;
    }

    /**
     * 设置 [Task]
     */
    @JsonProperty("task_id")
    public void setTask_id(Integer  task_id){
        this.task_id = task_id ;
        this.task_idDirtyFlag = true ;
    }

     /**
     * 获取 [Task]脏标记
     */
    @JsonIgnore
    public boolean getTask_idDirtyFlag(){
        return this.task_idDirtyFlag ;
    }   

    /**
     * 获取 [Task]
     */
    @JsonProperty("task_id_text")
    public String getTask_id_text(){
        return this.task_id_text ;
    }

    /**
     * 设置 [Task]
     */
    @JsonProperty("task_id_text")
    public void setTask_id_text(String  task_id_text){
        this.task_id_text = task_id_text ;
        this.task_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [Task]脏标记
     */
    @JsonIgnore
    public boolean getTask_id_textDirtyFlag(){
        return this.task_id_textDirtyFlag ;
    }   

    /**
     * 获取 [Tools Description]
     */
    @JsonProperty("tools_description")
    public String getTools_description(){
        return this.tools_description ;
    }

    /**
     * 设置 [Tools Description]
     */
    @JsonProperty("tools_description")
    public void setTools_description(String  tools_description){
        this.tools_description = tools_description ;
        this.tools_descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [Tools Description]脏标记
     */
    @JsonIgnore
    public boolean getTools_descriptionDirtyFlag(){
        return this.tools_descriptionDirtyFlag ;
    }   

    /**
     * 获取 [负责人]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [负责人]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [负责人]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [负责人]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [负责人]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [负责人]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站消息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [工单]
     */
    @JsonProperty("wo_id")
    public Integer getWo_id(){
        return this.wo_id ;
    }

    /**
     * 设置 [工单]
     */
    @JsonProperty("wo_id")
    public void setWo_id(Integer  wo_id){
        this.wo_id = wo_id ;
        this.wo_idDirtyFlag = true ;
    }

     /**
     * 获取 [工单]脏标记
     */
    @JsonIgnore
    public boolean getWo_idDirtyFlag(){
        return this.wo_idDirtyFlag ;
    }   

    /**
     * 获取 [工单]
     */
    @JsonProperty("wo_id_text")
    public String getWo_id_text(){
        return this.wo_id_text ;
    }

    /**
     * 设置 [工单]
     */
    @JsonProperty("wo_id_text")
    public void setWo_id_text(String  wo_id_text){
        this.wo_id_text = wo_id_text ;
        this.wo_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [工单]脏标记
     */
    @JsonIgnore
    public boolean getWo_id_textDirtyFlag(){
        return this.wo_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
