package cn.ibizlab.odoo.client.odoo_project.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproject_task;
import cn.ibizlab.odoo.client.odoo_project.config.odoo_projectClientProperties;
import cn.ibizlab.odoo.core.client.service.Iproject_taskClientService;
import cn.ibizlab.odoo.client.odoo_project.model.project_taskImpl;
import cn.ibizlab.odoo.client.odoo_project.feign.project_taskFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[project_task] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class project_taskClientServiceImpl implements Iproject_taskClientService {

    project_taskFeignClient project_taskFeignClient;

    @Autowired
    public project_taskClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_projectClientProperties odoo_projectClientProperties) {
        if (odoo_projectClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.project_taskFeignClient = nameBuilder.target(project_taskFeignClient.class,"http://"+odoo_projectClientProperties.getServiceId()+"/") ;
		}else if (odoo_projectClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.project_taskFeignClient = nameBuilder.target(project_taskFeignClient.class,odoo_projectClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iproject_task createModel() {
		return new project_taskImpl();
	}


    public void updateBatch(List<Iproject_task> project_tasks){
        if(project_tasks!=null){
            List<project_taskImpl> list = new ArrayList<project_taskImpl>();
            for(Iproject_task iproject_task :project_tasks){
                list.add((project_taskImpl)iproject_task) ;
            }
            project_taskFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Iproject_task> project_tasks){
        if(project_tasks!=null){
            List<project_taskImpl> list = new ArrayList<project_taskImpl>();
            for(Iproject_task iproject_task :project_tasks){
                list.add((project_taskImpl)iproject_task) ;
            }
            project_taskFeignClient.createBatch(list) ;
        }
    }


    public void create(Iproject_task project_task){
        Iproject_task clientModel = project_taskFeignClient.create((project_taskImpl)project_task) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), project_task.getClass(), false);
        copier.copy(clientModel, project_task, null);
    }


    public void remove(Iproject_task project_task){
        project_taskFeignClient.remove(project_task.getId()) ;
    }


    public void get(Iproject_task project_task){
        Iproject_task clientModel = project_taskFeignClient.get(project_task.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), project_task.getClass(), false);
        copier.copy(clientModel, project_task, null);
    }


    public Page<Iproject_task> fetchDefault(SearchContext context){
        Page<project_taskImpl> page = this.project_taskFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Iproject_task project_task){
        Iproject_task clientModel = project_taskFeignClient.update(project_task.getId(),(project_taskImpl)project_task) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), project_task.getClass(), false);
        copier.copy(clientModel, project_task, null);
    }


    public void removeBatch(List<Iproject_task> project_tasks){
        if(project_tasks!=null){
            List<project_taskImpl> list = new ArrayList<project_taskImpl>();
            for(Iproject_task iproject_task :project_tasks){
                list.add((project_taskImpl)iproject_task) ;
            }
            project_taskFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iproject_task> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iproject_task project_task){
        Iproject_task clientModel = project_taskFeignClient.getDraft(project_task.getId(),(project_taskImpl)project_task) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), project_task.getClass(), false);
        copier.copy(clientModel, project_task, null);
    }



}

