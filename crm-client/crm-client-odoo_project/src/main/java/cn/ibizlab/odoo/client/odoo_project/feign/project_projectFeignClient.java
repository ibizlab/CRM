package cn.ibizlab.odoo.client.odoo_project.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iproject_project;
import cn.ibizlab.odoo.client.odoo_project.model.project_projectImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[project_project] 服务对象接口
 */
public interface project_projectFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_project/project_projects/fetchdefault")
    public Page<project_projectImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_project/project_projects/updatebatch")
    public project_projectImpl updateBatch(@RequestBody List<project_projectImpl> project_projects);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_project/project_projects/createbatch")
    public project_projectImpl createBatch(@RequestBody List<project_projectImpl> project_projects);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_project/project_projects/removebatch")
    public project_projectImpl removeBatch(@RequestBody List<project_projectImpl> project_projects);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_project/project_projects/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_project/project_projects")
    public project_projectImpl create(@RequestBody project_projectImpl project_project);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_project/project_projects/{id}")
    public project_projectImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_project/project_projects/{id}")
    public project_projectImpl update(@PathVariable("id") Integer id,@RequestBody project_projectImpl project_project);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_project/project_projects/select")
    public Page<project_projectImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_project/project_projects/{id}/getdraft")
    public project_projectImpl getDraft(@PathVariable("id") Integer id,@RequestBody project_projectImpl project_project);



}
