package cn.ibizlab.odoo.client.odoo_project.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproject_tags;
import cn.ibizlab.odoo.client.odoo_project.config.odoo_projectClientProperties;
import cn.ibizlab.odoo.core.client.service.Iproject_tagsClientService;
import cn.ibizlab.odoo.client.odoo_project.model.project_tagsImpl;
import cn.ibizlab.odoo.client.odoo_project.feign.project_tagsFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[project_tags] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class project_tagsClientServiceImpl implements Iproject_tagsClientService {

    project_tagsFeignClient project_tagsFeignClient;

    @Autowired
    public project_tagsClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_projectClientProperties odoo_projectClientProperties) {
        if (odoo_projectClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.project_tagsFeignClient = nameBuilder.target(project_tagsFeignClient.class,"http://"+odoo_projectClientProperties.getServiceId()+"/") ;
		}else if (odoo_projectClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.project_tagsFeignClient = nameBuilder.target(project_tagsFeignClient.class,odoo_projectClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iproject_tags createModel() {
		return new project_tagsImpl();
	}


    public Page<Iproject_tags> fetchDefault(SearchContext context){
        Page<project_tagsImpl> page = this.project_tagsFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Iproject_tags project_tags){
        project_tagsFeignClient.remove(project_tags.getId()) ;
    }


    public void update(Iproject_tags project_tags){
        Iproject_tags clientModel = project_tagsFeignClient.update(project_tags.getId(),(project_tagsImpl)project_tags) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), project_tags.getClass(), false);
        copier.copy(clientModel, project_tags, null);
    }


    public void removeBatch(List<Iproject_tags> project_tags){
        if(project_tags!=null){
            List<project_tagsImpl> list = new ArrayList<project_tagsImpl>();
            for(Iproject_tags iproject_tags :project_tags){
                list.add((project_tagsImpl)iproject_tags) ;
            }
            project_tagsFeignClient.removeBatch(list) ;
        }
    }


    public void create(Iproject_tags project_tags){
        Iproject_tags clientModel = project_tagsFeignClient.create((project_tagsImpl)project_tags) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), project_tags.getClass(), false);
        copier.copy(clientModel, project_tags, null);
    }


    public void createBatch(List<Iproject_tags> project_tags){
        if(project_tags!=null){
            List<project_tagsImpl> list = new ArrayList<project_tagsImpl>();
            for(Iproject_tags iproject_tags :project_tags){
                list.add((project_tagsImpl)iproject_tags) ;
            }
            project_tagsFeignClient.createBatch(list) ;
        }
    }


    public void get(Iproject_tags project_tags){
        Iproject_tags clientModel = project_tagsFeignClient.get(project_tags.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), project_tags.getClass(), false);
        copier.copy(clientModel, project_tags, null);
    }


    public void updateBatch(List<Iproject_tags> project_tags){
        if(project_tags!=null){
            List<project_tagsImpl> list = new ArrayList<project_tagsImpl>();
            for(Iproject_tags iproject_tags :project_tags){
                list.add((project_tagsImpl)iproject_tags) ;
            }
            project_tagsFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iproject_tags> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iproject_tags project_tags){
        Iproject_tags clientModel = project_tagsFeignClient.getDraft(project_tags.getId(),(project_tagsImpl)project_tags) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), project_tags.getClass(), false);
        copier.copy(clientModel, project_tags, null);
    }



}

