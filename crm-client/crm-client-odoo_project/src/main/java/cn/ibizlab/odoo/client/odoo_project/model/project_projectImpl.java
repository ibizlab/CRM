package cn.ibizlab.odoo.client.odoo_project.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iproject_project;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[project_project] 对象
 */
public class project_projectImpl implements Iproject_project,Serializable{

    /**
     * 安全令牌
     */
    public String access_token;

    @JsonIgnore
    public boolean access_tokenDirtyFlag;
    
    /**
     * 门户访问网址
     */
    public String access_url;

    @JsonIgnore
    public boolean access_urlDirtyFlag;
    
    /**
     * 访问警告
     */
    public String access_warning;

    @JsonIgnore
    public boolean access_warningDirtyFlag;
    
    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 别名联系人安全
     */
    public String alias_contact;

    @JsonIgnore
    public boolean alias_contactDirtyFlag;
    
    /**
     * 默认值
     */
    public String alias_defaults;

    @JsonIgnore
    public boolean alias_defaultsDirtyFlag;
    
    /**
     * 别名网域
     */
    public String alias_domain;

    @JsonIgnore
    public boolean alias_domainDirtyFlag;
    
    /**
     * 记录线索ID
     */
    public Integer alias_force_thread_id;

    @JsonIgnore
    public boolean alias_force_thread_idDirtyFlag;
    
    /**
     * 别名
     */
    public Integer alias_id;

    @JsonIgnore
    public boolean alias_idDirtyFlag;
    
    /**
     * 别名的模型
     */
    public Integer alias_model_id;

    @JsonIgnore
    public boolean alias_model_idDirtyFlag;
    
    /**
     * 别名
     */
    public String alias_name;

    @JsonIgnore
    public boolean alias_nameDirtyFlag;
    
    /**
     * 上级模型
     */
    public Integer alias_parent_model_id;

    @JsonIgnore
    public boolean alias_parent_model_idDirtyFlag;
    
    /**
     * 上级记录线程ID
     */
    public Integer alias_parent_thread_id;

    @JsonIgnore
    public boolean alias_parent_thread_idDirtyFlag;
    
    /**
     * 所有者
     */
    public Integer alias_user_id;

    @JsonIgnore
    public boolean alias_user_idDirtyFlag;
    
    /**
     * 颜色索引
     */
    public Integer color;

    @JsonIgnore
    public boolean colorDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 过期日期
     */
    public Timestamp date;

    @JsonIgnore
    public boolean dateDirtyFlag;
    
    /**
     * 开始日期
     */
    public Timestamp date_start;

    @JsonIgnore
    public boolean date_startDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer doc_count;

    @JsonIgnore
    public boolean doc_countDirtyFlag;
    
    /**
     * 会员
     */
    public String favorite_user_ids;

    @JsonIgnore
    public boolean favorite_user_idsDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 在仪表板显示项目
     */
    public String is_favorite;

    @JsonIgnore
    public boolean is_favoriteDirtyFlag;
    
    /**
     * 用任务来
     */
    public String label_tasks;

    @JsonIgnore
    public boolean label_tasksDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 错误数
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 消息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 是关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 需要采取行动
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 行动数量
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 客户
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 客户
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 好 % 项目
     */
    public Integer percentage_satisfaction_project;

    @JsonIgnore
    public boolean percentage_satisfaction_projectDirtyFlag;
    
    /**
     * 好 % 任务
     */
    public Integer percentage_satisfaction_task;

    @JsonIgnore
    public boolean percentage_satisfaction_taskDirtyFlag;
    
    /**
     * 公开评级
     */
    public String portal_show_rating;

    @JsonIgnore
    public boolean portal_show_ratingDirtyFlag;
    
    /**
     * 隐私
     */
    public String privacy_visibility;

    @JsonIgnore
    public boolean privacy_visibilityDirtyFlag;
    
    /**
     * 评级请求截止日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp rating_request_deadline;

    @JsonIgnore
    public boolean rating_request_deadlineDirtyFlag;
    
    /**
     * 客户点评
     */
    public String rating_status;

    @JsonIgnore
    public boolean rating_statusDirtyFlag;
    
    /**
     * 点评频率
     */
    public String rating_status_period;

    @JsonIgnore
    public boolean rating_status_periodDirtyFlag;
    
    /**
     * 工作时间
     */
    public Integer resource_calendar_id;

    @JsonIgnore
    public boolean resource_calendar_idDirtyFlag;
    
    /**
     * 工作时间
     */
    public String resource_calendar_id_text;

    @JsonIgnore
    public boolean resource_calendar_id_textDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 子任务项目
     */
    public Integer subtask_project_id;

    @JsonIgnore
    public boolean subtask_project_idDirtyFlag;
    
    /**
     * 子任务项目
     */
    public String subtask_project_id_text;

    @JsonIgnore
    public boolean subtask_project_id_textDirtyFlag;
    
    /**
     * 任务活动
     */
    public String tasks;

    @JsonIgnore
    public boolean tasksDirtyFlag;
    
    /**
     * 任务统计
     */
    public Integer task_count;

    @JsonIgnore
    public boolean task_countDirtyFlag;
    
    /**
     * 任务
     */
    public String task_ids;

    @JsonIgnore
    public boolean task_idsDirtyFlag;
    
    /**
     * 任务阶段
     */
    public String type_ids;

    @JsonIgnore
    public boolean type_idsDirtyFlag;
    
    /**
     * 项目管理员
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 项目管理员
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 网站信息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [安全令牌]
     */
    @JsonProperty("access_token")
    public String getAccess_token(){
        return this.access_token ;
    }

    /**
     * 设置 [安全令牌]
     */
    @JsonProperty("access_token")
    public void setAccess_token(String  access_token){
        this.access_token = access_token ;
        this.access_tokenDirtyFlag = true ;
    }

     /**
     * 获取 [安全令牌]脏标记
     */
    @JsonIgnore
    public boolean getAccess_tokenDirtyFlag(){
        return this.access_tokenDirtyFlag ;
    }   

    /**
     * 获取 [门户访问网址]
     */
    @JsonProperty("access_url")
    public String getAccess_url(){
        return this.access_url ;
    }

    /**
     * 设置 [门户访问网址]
     */
    @JsonProperty("access_url")
    public void setAccess_url(String  access_url){
        this.access_url = access_url ;
        this.access_urlDirtyFlag = true ;
    }

     /**
     * 获取 [门户访问网址]脏标记
     */
    @JsonIgnore
    public boolean getAccess_urlDirtyFlag(){
        return this.access_urlDirtyFlag ;
    }   

    /**
     * 获取 [访问警告]
     */
    @JsonProperty("access_warning")
    public String getAccess_warning(){
        return this.access_warning ;
    }

    /**
     * 设置 [访问警告]
     */
    @JsonProperty("access_warning")
    public void setAccess_warning(String  access_warning){
        this.access_warning = access_warning ;
        this.access_warningDirtyFlag = true ;
    }

     /**
     * 获取 [访问警告]脏标记
     */
    @JsonIgnore
    public boolean getAccess_warningDirtyFlag(){
        return this.access_warningDirtyFlag ;
    }   

    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [别名联系人安全]
     */
    @JsonProperty("alias_contact")
    public String getAlias_contact(){
        return this.alias_contact ;
    }

    /**
     * 设置 [别名联系人安全]
     */
    @JsonProperty("alias_contact")
    public void setAlias_contact(String  alias_contact){
        this.alias_contact = alias_contact ;
        this.alias_contactDirtyFlag = true ;
    }

     /**
     * 获取 [别名联系人安全]脏标记
     */
    @JsonIgnore
    public boolean getAlias_contactDirtyFlag(){
        return this.alias_contactDirtyFlag ;
    }   

    /**
     * 获取 [默认值]
     */
    @JsonProperty("alias_defaults")
    public String getAlias_defaults(){
        return this.alias_defaults ;
    }

    /**
     * 设置 [默认值]
     */
    @JsonProperty("alias_defaults")
    public void setAlias_defaults(String  alias_defaults){
        this.alias_defaults = alias_defaults ;
        this.alias_defaultsDirtyFlag = true ;
    }

     /**
     * 获取 [默认值]脏标记
     */
    @JsonIgnore
    public boolean getAlias_defaultsDirtyFlag(){
        return this.alias_defaultsDirtyFlag ;
    }   

    /**
     * 获取 [别名网域]
     */
    @JsonProperty("alias_domain")
    public String getAlias_domain(){
        return this.alias_domain ;
    }

    /**
     * 设置 [别名网域]
     */
    @JsonProperty("alias_domain")
    public void setAlias_domain(String  alias_domain){
        this.alias_domain = alias_domain ;
        this.alias_domainDirtyFlag = true ;
    }

     /**
     * 获取 [别名网域]脏标记
     */
    @JsonIgnore
    public boolean getAlias_domainDirtyFlag(){
        return this.alias_domainDirtyFlag ;
    }   

    /**
     * 获取 [记录线索ID]
     */
    @JsonProperty("alias_force_thread_id")
    public Integer getAlias_force_thread_id(){
        return this.alias_force_thread_id ;
    }

    /**
     * 设置 [记录线索ID]
     */
    @JsonProperty("alias_force_thread_id")
    public void setAlias_force_thread_id(Integer  alias_force_thread_id){
        this.alias_force_thread_id = alias_force_thread_id ;
        this.alias_force_thread_idDirtyFlag = true ;
    }

     /**
     * 获取 [记录线索ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_force_thread_idDirtyFlag(){
        return this.alias_force_thread_idDirtyFlag ;
    }   

    /**
     * 获取 [别名]
     */
    @JsonProperty("alias_id")
    public Integer getAlias_id(){
        return this.alias_id ;
    }

    /**
     * 设置 [别名]
     */
    @JsonProperty("alias_id")
    public void setAlias_id(Integer  alias_id){
        this.alias_id = alias_id ;
        this.alias_idDirtyFlag = true ;
    }

     /**
     * 获取 [别名]脏标记
     */
    @JsonIgnore
    public boolean getAlias_idDirtyFlag(){
        return this.alias_idDirtyFlag ;
    }   

    /**
     * 获取 [别名的模型]
     */
    @JsonProperty("alias_model_id")
    public Integer getAlias_model_id(){
        return this.alias_model_id ;
    }

    /**
     * 设置 [别名的模型]
     */
    @JsonProperty("alias_model_id")
    public void setAlias_model_id(Integer  alias_model_id){
        this.alias_model_id = alias_model_id ;
        this.alias_model_idDirtyFlag = true ;
    }

     /**
     * 获取 [别名的模型]脏标记
     */
    @JsonIgnore
    public boolean getAlias_model_idDirtyFlag(){
        return this.alias_model_idDirtyFlag ;
    }   

    /**
     * 获取 [别名]
     */
    @JsonProperty("alias_name")
    public String getAlias_name(){
        return this.alias_name ;
    }

    /**
     * 设置 [别名]
     */
    @JsonProperty("alias_name")
    public void setAlias_name(String  alias_name){
        this.alias_name = alias_name ;
        this.alias_nameDirtyFlag = true ;
    }

     /**
     * 获取 [别名]脏标记
     */
    @JsonIgnore
    public boolean getAlias_nameDirtyFlag(){
        return this.alias_nameDirtyFlag ;
    }   

    /**
     * 获取 [上级模型]
     */
    @JsonProperty("alias_parent_model_id")
    public Integer getAlias_parent_model_id(){
        return this.alias_parent_model_id ;
    }

    /**
     * 设置 [上级模型]
     */
    @JsonProperty("alias_parent_model_id")
    public void setAlias_parent_model_id(Integer  alias_parent_model_id){
        this.alias_parent_model_id = alias_parent_model_id ;
        this.alias_parent_model_idDirtyFlag = true ;
    }

     /**
     * 获取 [上级模型]脏标记
     */
    @JsonIgnore
    public boolean getAlias_parent_model_idDirtyFlag(){
        return this.alias_parent_model_idDirtyFlag ;
    }   

    /**
     * 获取 [上级记录线程ID]
     */
    @JsonProperty("alias_parent_thread_id")
    public Integer getAlias_parent_thread_id(){
        return this.alias_parent_thread_id ;
    }

    /**
     * 设置 [上级记录线程ID]
     */
    @JsonProperty("alias_parent_thread_id")
    public void setAlias_parent_thread_id(Integer  alias_parent_thread_id){
        this.alias_parent_thread_id = alias_parent_thread_id ;
        this.alias_parent_thread_idDirtyFlag = true ;
    }

     /**
     * 获取 [上级记录线程ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_parent_thread_idDirtyFlag(){
        return this.alias_parent_thread_idDirtyFlag ;
    }   

    /**
     * 获取 [所有者]
     */
    @JsonProperty("alias_user_id")
    public Integer getAlias_user_id(){
        return this.alias_user_id ;
    }

    /**
     * 设置 [所有者]
     */
    @JsonProperty("alias_user_id")
    public void setAlias_user_id(Integer  alias_user_id){
        this.alias_user_id = alias_user_id ;
        this.alias_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [所有者]脏标记
     */
    @JsonIgnore
    public boolean getAlias_user_idDirtyFlag(){
        return this.alias_user_idDirtyFlag ;
    }   

    /**
     * 获取 [颜色索引]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return this.color ;
    }

    /**
     * 设置 [颜色索引]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

     /**
     * 获取 [颜色索引]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return this.colorDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [过期日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [过期日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

     /**
     * 获取 [过期日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }   

    /**
     * 获取 [开始日期]
     */
    @JsonProperty("date_start")
    public Timestamp getDate_start(){
        return this.date_start ;
    }

    /**
     * 设置 [开始日期]
     */
    @JsonProperty("date_start")
    public void setDate_start(Timestamp  date_start){
        this.date_start = date_start ;
        this.date_startDirtyFlag = true ;
    }

     /**
     * 获取 [开始日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_startDirtyFlag(){
        return this.date_startDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("doc_count")
    public Integer getDoc_count(){
        return this.doc_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("doc_count")
    public void setDoc_count(Integer  doc_count){
        this.doc_count = doc_count ;
        this.doc_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getDoc_countDirtyFlag(){
        return this.doc_countDirtyFlag ;
    }   

    /**
     * 获取 [会员]
     */
    @JsonProperty("favorite_user_ids")
    public String getFavorite_user_ids(){
        return this.favorite_user_ids ;
    }

    /**
     * 设置 [会员]
     */
    @JsonProperty("favorite_user_ids")
    public void setFavorite_user_ids(String  favorite_user_ids){
        this.favorite_user_ids = favorite_user_ids ;
        this.favorite_user_idsDirtyFlag = true ;
    }

     /**
     * 获取 [会员]脏标记
     */
    @JsonIgnore
    public boolean getFavorite_user_idsDirtyFlag(){
        return this.favorite_user_idsDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [在仪表板显示项目]
     */
    @JsonProperty("is_favorite")
    public String getIs_favorite(){
        return this.is_favorite ;
    }

    /**
     * 设置 [在仪表板显示项目]
     */
    @JsonProperty("is_favorite")
    public void setIs_favorite(String  is_favorite){
        this.is_favorite = is_favorite ;
        this.is_favoriteDirtyFlag = true ;
    }

     /**
     * 获取 [在仪表板显示项目]脏标记
     */
    @JsonIgnore
    public boolean getIs_favoriteDirtyFlag(){
        return this.is_favoriteDirtyFlag ;
    }   

    /**
     * 获取 [用任务来]
     */
    @JsonProperty("label_tasks")
    public String getLabel_tasks(){
        return this.label_tasks ;
    }

    /**
     * 设置 [用任务来]
     */
    @JsonProperty("label_tasks")
    public void setLabel_tasks(String  label_tasks){
        this.label_tasks = label_tasks ;
        this.label_tasksDirtyFlag = true ;
    }

     /**
     * 获取 [用任务来]脏标记
     */
    @JsonIgnore
    public boolean getLabel_tasksDirtyFlag(){
        return this.label_tasksDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [错误数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [是关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [需要采取行动]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [行动数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [客户]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [客户]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [客户]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [客户]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [客户]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [客户]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [好 % 项目]
     */
    @JsonProperty("percentage_satisfaction_project")
    public Integer getPercentage_satisfaction_project(){
        return this.percentage_satisfaction_project ;
    }

    /**
     * 设置 [好 % 项目]
     */
    @JsonProperty("percentage_satisfaction_project")
    public void setPercentage_satisfaction_project(Integer  percentage_satisfaction_project){
        this.percentage_satisfaction_project = percentage_satisfaction_project ;
        this.percentage_satisfaction_projectDirtyFlag = true ;
    }

     /**
     * 获取 [好 % 项目]脏标记
     */
    @JsonIgnore
    public boolean getPercentage_satisfaction_projectDirtyFlag(){
        return this.percentage_satisfaction_projectDirtyFlag ;
    }   

    /**
     * 获取 [好 % 任务]
     */
    @JsonProperty("percentage_satisfaction_task")
    public Integer getPercentage_satisfaction_task(){
        return this.percentage_satisfaction_task ;
    }

    /**
     * 设置 [好 % 任务]
     */
    @JsonProperty("percentage_satisfaction_task")
    public void setPercentage_satisfaction_task(Integer  percentage_satisfaction_task){
        this.percentage_satisfaction_task = percentage_satisfaction_task ;
        this.percentage_satisfaction_taskDirtyFlag = true ;
    }

     /**
     * 获取 [好 % 任务]脏标记
     */
    @JsonIgnore
    public boolean getPercentage_satisfaction_taskDirtyFlag(){
        return this.percentage_satisfaction_taskDirtyFlag ;
    }   

    /**
     * 获取 [公开评级]
     */
    @JsonProperty("portal_show_rating")
    public String getPortal_show_rating(){
        return this.portal_show_rating ;
    }

    /**
     * 设置 [公开评级]
     */
    @JsonProperty("portal_show_rating")
    public void setPortal_show_rating(String  portal_show_rating){
        this.portal_show_rating = portal_show_rating ;
        this.portal_show_ratingDirtyFlag = true ;
    }

     /**
     * 获取 [公开评级]脏标记
     */
    @JsonIgnore
    public boolean getPortal_show_ratingDirtyFlag(){
        return this.portal_show_ratingDirtyFlag ;
    }   

    /**
     * 获取 [隐私]
     */
    @JsonProperty("privacy_visibility")
    public String getPrivacy_visibility(){
        return this.privacy_visibility ;
    }

    /**
     * 设置 [隐私]
     */
    @JsonProperty("privacy_visibility")
    public void setPrivacy_visibility(String  privacy_visibility){
        this.privacy_visibility = privacy_visibility ;
        this.privacy_visibilityDirtyFlag = true ;
    }

     /**
     * 获取 [隐私]脏标记
     */
    @JsonIgnore
    public boolean getPrivacy_visibilityDirtyFlag(){
        return this.privacy_visibilityDirtyFlag ;
    }   

    /**
     * 获取 [评级请求截止日期]
     */
    @JsonProperty("rating_request_deadline")
    public Timestamp getRating_request_deadline(){
        return this.rating_request_deadline ;
    }

    /**
     * 设置 [评级请求截止日期]
     */
    @JsonProperty("rating_request_deadline")
    public void setRating_request_deadline(Timestamp  rating_request_deadline){
        this.rating_request_deadline = rating_request_deadline ;
        this.rating_request_deadlineDirtyFlag = true ;
    }

     /**
     * 获取 [评级请求截止日期]脏标记
     */
    @JsonIgnore
    public boolean getRating_request_deadlineDirtyFlag(){
        return this.rating_request_deadlineDirtyFlag ;
    }   

    /**
     * 获取 [客户点评]
     */
    @JsonProperty("rating_status")
    public String getRating_status(){
        return this.rating_status ;
    }

    /**
     * 设置 [客户点评]
     */
    @JsonProperty("rating_status")
    public void setRating_status(String  rating_status){
        this.rating_status = rating_status ;
        this.rating_statusDirtyFlag = true ;
    }

     /**
     * 获取 [客户点评]脏标记
     */
    @JsonIgnore
    public boolean getRating_statusDirtyFlag(){
        return this.rating_statusDirtyFlag ;
    }   

    /**
     * 获取 [点评频率]
     */
    @JsonProperty("rating_status_period")
    public String getRating_status_period(){
        return this.rating_status_period ;
    }

    /**
     * 设置 [点评频率]
     */
    @JsonProperty("rating_status_period")
    public void setRating_status_period(String  rating_status_period){
        this.rating_status_period = rating_status_period ;
        this.rating_status_periodDirtyFlag = true ;
    }

     /**
     * 获取 [点评频率]脏标记
     */
    @JsonIgnore
    public boolean getRating_status_periodDirtyFlag(){
        return this.rating_status_periodDirtyFlag ;
    }   

    /**
     * 获取 [工作时间]
     */
    @JsonProperty("resource_calendar_id")
    public Integer getResource_calendar_id(){
        return this.resource_calendar_id ;
    }

    /**
     * 设置 [工作时间]
     */
    @JsonProperty("resource_calendar_id")
    public void setResource_calendar_id(Integer  resource_calendar_id){
        this.resource_calendar_id = resource_calendar_id ;
        this.resource_calendar_idDirtyFlag = true ;
    }

     /**
     * 获取 [工作时间]脏标记
     */
    @JsonIgnore
    public boolean getResource_calendar_idDirtyFlag(){
        return this.resource_calendar_idDirtyFlag ;
    }   

    /**
     * 获取 [工作时间]
     */
    @JsonProperty("resource_calendar_id_text")
    public String getResource_calendar_id_text(){
        return this.resource_calendar_id_text ;
    }

    /**
     * 设置 [工作时间]
     */
    @JsonProperty("resource_calendar_id_text")
    public void setResource_calendar_id_text(String  resource_calendar_id_text){
        this.resource_calendar_id_text = resource_calendar_id_text ;
        this.resource_calendar_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [工作时间]脏标记
     */
    @JsonIgnore
    public boolean getResource_calendar_id_textDirtyFlag(){
        return this.resource_calendar_id_textDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [子任务项目]
     */
    @JsonProperty("subtask_project_id")
    public Integer getSubtask_project_id(){
        return this.subtask_project_id ;
    }

    /**
     * 设置 [子任务项目]
     */
    @JsonProperty("subtask_project_id")
    public void setSubtask_project_id(Integer  subtask_project_id){
        this.subtask_project_id = subtask_project_id ;
        this.subtask_project_idDirtyFlag = true ;
    }

     /**
     * 获取 [子任务项目]脏标记
     */
    @JsonIgnore
    public boolean getSubtask_project_idDirtyFlag(){
        return this.subtask_project_idDirtyFlag ;
    }   

    /**
     * 获取 [子任务项目]
     */
    @JsonProperty("subtask_project_id_text")
    public String getSubtask_project_id_text(){
        return this.subtask_project_id_text ;
    }

    /**
     * 设置 [子任务项目]
     */
    @JsonProperty("subtask_project_id_text")
    public void setSubtask_project_id_text(String  subtask_project_id_text){
        this.subtask_project_id_text = subtask_project_id_text ;
        this.subtask_project_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [子任务项目]脏标记
     */
    @JsonIgnore
    public boolean getSubtask_project_id_textDirtyFlag(){
        return this.subtask_project_id_textDirtyFlag ;
    }   

    /**
     * 获取 [任务活动]
     */
    @JsonProperty("tasks")
    public String getTasks(){
        return this.tasks ;
    }

    /**
     * 设置 [任务活动]
     */
    @JsonProperty("tasks")
    public void setTasks(String  tasks){
        this.tasks = tasks ;
        this.tasksDirtyFlag = true ;
    }

     /**
     * 获取 [任务活动]脏标记
     */
    @JsonIgnore
    public boolean getTasksDirtyFlag(){
        return this.tasksDirtyFlag ;
    }   

    /**
     * 获取 [任务统计]
     */
    @JsonProperty("task_count")
    public Integer getTask_count(){
        return this.task_count ;
    }

    /**
     * 设置 [任务统计]
     */
    @JsonProperty("task_count")
    public void setTask_count(Integer  task_count){
        this.task_count = task_count ;
        this.task_countDirtyFlag = true ;
    }

     /**
     * 获取 [任务统计]脏标记
     */
    @JsonIgnore
    public boolean getTask_countDirtyFlag(){
        return this.task_countDirtyFlag ;
    }   

    /**
     * 获取 [任务]
     */
    @JsonProperty("task_ids")
    public String getTask_ids(){
        return this.task_ids ;
    }

    /**
     * 设置 [任务]
     */
    @JsonProperty("task_ids")
    public void setTask_ids(String  task_ids){
        this.task_ids = task_ids ;
        this.task_idsDirtyFlag = true ;
    }

     /**
     * 获取 [任务]脏标记
     */
    @JsonIgnore
    public boolean getTask_idsDirtyFlag(){
        return this.task_idsDirtyFlag ;
    }   

    /**
     * 获取 [任务阶段]
     */
    @JsonProperty("type_ids")
    public String getType_ids(){
        return this.type_ids ;
    }

    /**
     * 设置 [任务阶段]
     */
    @JsonProperty("type_ids")
    public void setType_ids(String  type_ids){
        this.type_ids = type_ids ;
        this.type_idsDirtyFlag = true ;
    }

     /**
     * 获取 [任务阶段]脏标记
     */
    @JsonIgnore
    public boolean getType_idsDirtyFlag(){
        return this.type_idsDirtyFlag ;
    }   

    /**
     * 获取 [项目管理员]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [项目管理员]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [项目管理员]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [项目管理员]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [项目管理员]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [项目管理员]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站信息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
