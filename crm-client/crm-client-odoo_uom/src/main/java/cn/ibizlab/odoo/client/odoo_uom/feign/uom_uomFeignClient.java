package cn.ibizlab.odoo.client.odoo_uom.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iuom_uom;
import cn.ibizlab.odoo.client.odoo_uom.model.uom_uomImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[uom_uom] 服务对象接口
 */
public interface uom_uomFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_uom/uom_uoms/createbatch")
    public uom_uomImpl createBatch(@RequestBody List<uom_uomImpl> uom_uoms);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_uom/uom_uoms/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_uom/uom_uoms/updatebatch")
    public uom_uomImpl updateBatch(@RequestBody List<uom_uomImpl> uom_uoms);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_uom/uom_uoms")
    public uom_uomImpl create(@RequestBody uom_uomImpl uom_uom);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_uom/uom_uoms/fetchdefault")
    public Page<uom_uomImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_uom/uom_uoms/{id}")
    public uom_uomImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_uom/uom_uoms/removebatch")
    public uom_uomImpl removeBatch(@RequestBody List<uom_uomImpl> uom_uoms);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_uom/uom_uoms/{id}")
    public uom_uomImpl update(@PathVariable("id") Integer id,@RequestBody uom_uomImpl uom_uom);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_uom/uom_uoms/select")
    public Page<uom_uomImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_uom/uom_uoms/{id}/getdraft")
    public uom_uomImpl getDraft(@PathVariable("id") Integer id,@RequestBody uom_uomImpl uom_uom);



}
