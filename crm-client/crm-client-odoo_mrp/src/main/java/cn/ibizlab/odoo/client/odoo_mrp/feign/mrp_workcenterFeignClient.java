package cn.ibizlab.odoo.client.odoo_mrp.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imrp_workcenter;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_workcenterImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mrp_workcenter] 服务对象接口
 */
public interface mrp_workcenterFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_workcenters/updatebatch")
    public mrp_workcenterImpl updateBatch(@RequestBody List<mrp_workcenterImpl> mrp_workcenters);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_workcenters/fetchdefault")
    public Page<mrp_workcenterImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_workcenters/createbatch")
    public mrp_workcenterImpl createBatch(@RequestBody List<mrp_workcenterImpl> mrp_workcenters);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_workcenters")
    public mrp_workcenterImpl create(@RequestBody mrp_workcenterImpl mrp_workcenter);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_workcenters/removebatch")
    public mrp_workcenterImpl removeBatch(@RequestBody List<mrp_workcenterImpl> mrp_workcenters);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_workcenters/{id}")
    public mrp_workcenterImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_workcenters/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_workcenters/{id}")
    public mrp_workcenterImpl update(@PathVariable("id") Integer id,@RequestBody mrp_workcenterImpl mrp_workcenter);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_workcenters/select")
    public Page<mrp_workcenterImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_workcenters/{id}/getdraft")
    public mrp_workcenterImpl getDraft(@PathVariable("id") Integer id,@RequestBody mrp_workcenterImpl mrp_workcenter);



}
