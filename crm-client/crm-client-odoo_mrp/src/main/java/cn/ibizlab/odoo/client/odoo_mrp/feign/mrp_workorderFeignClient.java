package cn.ibizlab.odoo.client.odoo_mrp.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imrp_workorder;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_workorderImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mrp_workorder] 服务对象接口
 */
public interface mrp_workorderFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_workorders/updatebatch")
    public mrp_workorderImpl updateBatch(@RequestBody List<mrp_workorderImpl> mrp_workorders);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_workorders/removebatch")
    public mrp_workorderImpl removeBatch(@RequestBody List<mrp_workorderImpl> mrp_workorders);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_workorders/fetchdefault")
    public Page<mrp_workorderImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_workorders")
    public mrp_workorderImpl create(@RequestBody mrp_workorderImpl mrp_workorder);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_workorders/createbatch")
    public mrp_workorderImpl createBatch(@RequestBody List<mrp_workorderImpl> mrp_workorders);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_workorders/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_workorders/{id}")
    public mrp_workorderImpl update(@PathVariable("id") Integer id,@RequestBody mrp_workorderImpl mrp_workorder);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_workorders/{id}")
    public mrp_workorderImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_workorders/select")
    public Page<mrp_workorderImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_workorders/{id}/getdraft")
    public mrp_workorderImpl getDraft(@PathVariable("id") Integer id,@RequestBody mrp_workorderImpl mrp_workorder);



}
