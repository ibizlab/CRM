package cn.ibizlab.odoo.client.odoo_mrp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imrp_bom_line;
import cn.ibizlab.odoo.client.odoo_mrp.config.odoo_mrpClientProperties;
import cn.ibizlab.odoo.core.client.service.Imrp_bom_lineClientService;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_bom_lineImpl;
import cn.ibizlab.odoo.client.odoo_mrp.feign.mrp_bom_lineFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mrp_bom_line] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mrp_bom_lineClientServiceImpl implements Imrp_bom_lineClientService {

    mrp_bom_lineFeignClient mrp_bom_lineFeignClient;

    @Autowired
    public mrp_bom_lineClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mrpClientProperties odoo_mrpClientProperties) {
        if (odoo_mrpClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mrp_bom_lineFeignClient = nameBuilder.target(mrp_bom_lineFeignClient.class,"http://"+odoo_mrpClientProperties.getServiceId()+"/") ;
		}else if (odoo_mrpClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mrp_bom_lineFeignClient = nameBuilder.target(mrp_bom_lineFeignClient.class,odoo_mrpClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imrp_bom_line createModel() {
		return new mrp_bom_lineImpl();
	}


    public void updateBatch(List<Imrp_bom_line> mrp_bom_lines){
        if(mrp_bom_lines!=null){
            List<mrp_bom_lineImpl> list = new ArrayList<mrp_bom_lineImpl>();
            for(Imrp_bom_line imrp_bom_line :mrp_bom_lines){
                list.add((mrp_bom_lineImpl)imrp_bom_line) ;
            }
            mrp_bom_lineFeignClient.updateBatch(list) ;
        }
    }


    public Page<Imrp_bom_line> fetchDefault(SearchContext context){
        Page<mrp_bom_lineImpl> page = this.mrp_bom_lineFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Imrp_bom_line> mrp_bom_lines){
        if(mrp_bom_lines!=null){
            List<mrp_bom_lineImpl> list = new ArrayList<mrp_bom_lineImpl>();
            for(Imrp_bom_line imrp_bom_line :mrp_bom_lines){
                list.add((mrp_bom_lineImpl)imrp_bom_line) ;
            }
            mrp_bom_lineFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Imrp_bom_line> mrp_bom_lines){
        if(mrp_bom_lines!=null){
            List<mrp_bom_lineImpl> list = new ArrayList<mrp_bom_lineImpl>();
            for(Imrp_bom_line imrp_bom_line :mrp_bom_lines){
                list.add((mrp_bom_lineImpl)imrp_bom_line) ;
            }
            mrp_bom_lineFeignClient.removeBatch(list) ;
        }
    }


    public void update(Imrp_bom_line mrp_bom_line){
        Imrp_bom_line clientModel = mrp_bom_lineFeignClient.update(mrp_bom_line.getId(),(mrp_bom_lineImpl)mrp_bom_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_bom_line.getClass(), false);
        copier.copy(clientModel, mrp_bom_line, null);
    }


    public void get(Imrp_bom_line mrp_bom_line){
        Imrp_bom_line clientModel = mrp_bom_lineFeignClient.get(mrp_bom_line.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_bom_line.getClass(), false);
        copier.copy(clientModel, mrp_bom_line, null);
    }


    public void remove(Imrp_bom_line mrp_bom_line){
        mrp_bom_lineFeignClient.remove(mrp_bom_line.getId()) ;
    }


    public void create(Imrp_bom_line mrp_bom_line){
        Imrp_bom_line clientModel = mrp_bom_lineFeignClient.create((mrp_bom_lineImpl)mrp_bom_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_bom_line.getClass(), false);
        copier.copy(clientModel, mrp_bom_line, null);
    }


    public Page<Imrp_bom_line> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imrp_bom_line mrp_bom_line){
        Imrp_bom_line clientModel = mrp_bom_lineFeignClient.getDraft(mrp_bom_line.getId(),(mrp_bom_lineImpl)mrp_bom_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_bom_line.getClass(), false);
        copier.copy(clientModel, mrp_bom_line, null);
    }



}

