package cn.ibizlab.odoo.client.odoo_mrp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imrp_workcenter_productivity;
import cn.ibizlab.odoo.client.odoo_mrp.config.odoo_mrpClientProperties;
import cn.ibizlab.odoo.core.client.service.Imrp_workcenter_productivityClientService;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_workcenter_productivityImpl;
import cn.ibizlab.odoo.client.odoo_mrp.feign.mrp_workcenter_productivityFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mrp_workcenter_productivity] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mrp_workcenter_productivityClientServiceImpl implements Imrp_workcenter_productivityClientService {

    mrp_workcenter_productivityFeignClient mrp_workcenter_productivityFeignClient;

    @Autowired
    public mrp_workcenter_productivityClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mrpClientProperties odoo_mrpClientProperties) {
        if (odoo_mrpClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mrp_workcenter_productivityFeignClient = nameBuilder.target(mrp_workcenter_productivityFeignClient.class,"http://"+odoo_mrpClientProperties.getServiceId()+"/") ;
		}else if (odoo_mrpClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mrp_workcenter_productivityFeignClient = nameBuilder.target(mrp_workcenter_productivityFeignClient.class,odoo_mrpClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imrp_workcenter_productivity createModel() {
		return new mrp_workcenter_productivityImpl();
	}


    public void updateBatch(List<Imrp_workcenter_productivity> mrp_workcenter_productivities){
        if(mrp_workcenter_productivities!=null){
            List<mrp_workcenter_productivityImpl> list = new ArrayList<mrp_workcenter_productivityImpl>();
            for(Imrp_workcenter_productivity imrp_workcenter_productivity :mrp_workcenter_productivities){
                list.add((mrp_workcenter_productivityImpl)imrp_workcenter_productivity) ;
            }
            mrp_workcenter_productivityFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Imrp_workcenter_productivity mrp_workcenter_productivity){
        mrp_workcenter_productivityFeignClient.remove(mrp_workcenter_productivity.getId()) ;
    }


    public void update(Imrp_workcenter_productivity mrp_workcenter_productivity){
        Imrp_workcenter_productivity clientModel = mrp_workcenter_productivityFeignClient.update(mrp_workcenter_productivity.getId(),(mrp_workcenter_productivityImpl)mrp_workcenter_productivity) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_workcenter_productivity.getClass(), false);
        copier.copy(clientModel, mrp_workcenter_productivity, null);
    }


    public Page<Imrp_workcenter_productivity> fetchDefault(SearchContext context){
        Page<mrp_workcenter_productivityImpl> page = this.mrp_workcenter_productivityFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Imrp_workcenter_productivity mrp_workcenter_productivity){
        Imrp_workcenter_productivity clientModel = mrp_workcenter_productivityFeignClient.create((mrp_workcenter_productivityImpl)mrp_workcenter_productivity) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_workcenter_productivity.getClass(), false);
        copier.copy(clientModel, mrp_workcenter_productivity, null);
    }


    public void createBatch(List<Imrp_workcenter_productivity> mrp_workcenter_productivities){
        if(mrp_workcenter_productivities!=null){
            List<mrp_workcenter_productivityImpl> list = new ArrayList<mrp_workcenter_productivityImpl>();
            for(Imrp_workcenter_productivity imrp_workcenter_productivity :mrp_workcenter_productivities){
                list.add((mrp_workcenter_productivityImpl)imrp_workcenter_productivity) ;
            }
            mrp_workcenter_productivityFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Imrp_workcenter_productivity> mrp_workcenter_productivities){
        if(mrp_workcenter_productivities!=null){
            List<mrp_workcenter_productivityImpl> list = new ArrayList<mrp_workcenter_productivityImpl>();
            for(Imrp_workcenter_productivity imrp_workcenter_productivity :mrp_workcenter_productivities){
                list.add((mrp_workcenter_productivityImpl)imrp_workcenter_productivity) ;
            }
            mrp_workcenter_productivityFeignClient.removeBatch(list) ;
        }
    }


    public void get(Imrp_workcenter_productivity mrp_workcenter_productivity){
        Imrp_workcenter_productivity clientModel = mrp_workcenter_productivityFeignClient.get(mrp_workcenter_productivity.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_workcenter_productivity.getClass(), false);
        copier.copy(clientModel, mrp_workcenter_productivity, null);
    }


    public Page<Imrp_workcenter_productivity> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imrp_workcenter_productivity mrp_workcenter_productivity){
        Imrp_workcenter_productivity clientModel = mrp_workcenter_productivityFeignClient.getDraft(mrp_workcenter_productivity.getId(),(mrp_workcenter_productivityImpl)mrp_workcenter_productivity) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_workcenter_productivity.getClass(), false);
        copier.copy(clientModel, mrp_workcenter_productivity, null);
    }



}

