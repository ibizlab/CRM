package cn.ibizlab.odoo.client.odoo_mrp.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imrp_bom;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_bomImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mrp_bom] 服务对象接口
 */
public interface mrp_bomFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_boms")
    public mrp_bomImpl create(@RequestBody mrp_bomImpl mrp_bom);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_boms/fetchdefault")
    public Page<mrp_bomImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_boms/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_boms/{id}")
    public mrp_bomImpl update(@PathVariable("id") Integer id,@RequestBody mrp_bomImpl mrp_bom);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_boms/{id}")
    public mrp_bomImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_boms/createbatch")
    public mrp_bomImpl createBatch(@RequestBody List<mrp_bomImpl> mrp_boms);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_boms/updatebatch")
    public mrp_bomImpl updateBatch(@RequestBody List<mrp_bomImpl> mrp_boms);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_boms/removebatch")
    public mrp_bomImpl removeBatch(@RequestBody List<mrp_bomImpl> mrp_boms);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_boms/select")
    public Page<mrp_bomImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_boms/{id}/getdraft")
    public mrp_bomImpl getDraft(@PathVariable("id") Integer id,@RequestBody mrp_bomImpl mrp_bom);



}
