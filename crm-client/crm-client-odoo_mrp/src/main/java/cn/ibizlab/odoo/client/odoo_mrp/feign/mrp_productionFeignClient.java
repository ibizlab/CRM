package cn.ibizlab.odoo.client.odoo_mrp.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imrp_production;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_productionImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mrp_production] 服务对象接口
 */
public interface mrp_productionFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_productions/removebatch")
    public mrp_productionImpl removeBatch(@RequestBody List<mrp_productionImpl> mrp_productions);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_productions/createbatch")
    public mrp_productionImpl createBatch(@RequestBody List<mrp_productionImpl> mrp_productions);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_productions")
    public mrp_productionImpl create(@RequestBody mrp_productionImpl mrp_production);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_productions/fetchdefault")
    public Page<mrp_productionImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_productions/updatebatch")
    public mrp_productionImpl updateBatch(@RequestBody List<mrp_productionImpl> mrp_productions);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_productions/{id}")
    public mrp_productionImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_productions/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_productions/{id}")
    public mrp_productionImpl update(@PathVariable("id") Integer id,@RequestBody mrp_productionImpl mrp_production);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_productions/select")
    public Page<mrp_productionImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_productions/{id}/getdraft")
    public mrp_productionImpl getDraft(@PathVariable("id") Integer id,@RequestBody mrp_productionImpl mrp_production);



}
