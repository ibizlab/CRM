package cn.ibizlab.odoo.client.odoo_mrp.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imrp_workcenter_productivity_loss_type;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_workcenter_productivity_loss_typeImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mrp_workcenter_productivity_loss_type] 服务对象接口
 */
public interface mrp_workcenter_productivity_loss_typeFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_workcenter_productivity_loss_types/createbatch")
    public mrp_workcenter_productivity_loss_typeImpl createBatch(@RequestBody List<mrp_workcenter_productivity_loss_typeImpl> mrp_workcenter_productivity_loss_types);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_workcenter_productivity_loss_types")
    public mrp_workcenter_productivity_loss_typeImpl create(@RequestBody mrp_workcenter_productivity_loss_typeImpl mrp_workcenter_productivity_loss_type);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_workcenter_productivity_loss_types/removebatch")
    public mrp_workcenter_productivity_loss_typeImpl removeBatch(@RequestBody List<mrp_workcenter_productivity_loss_typeImpl> mrp_workcenter_productivity_loss_types);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_workcenter_productivity_loss_types/updatebatch")
    public mrp_workcenter_productivity_loss_typeImpl updateBatch(@RequestBody List<mrp_workcenter_productivity_loss_typeImpl> mrp_workcenter_productivity_loss_types);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_workcenter_productivity_loss_types/{id}")
    public mrp_workcenter_productivity_loss_typeImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_workcenter_productivity_loss_types/{id}")
    public mrp_workcenter_productivity_loss_typeImpl update(@PathVariable("id") Integer id,@RequestBody mrp_workcenter_productivity_loss_typeImpl mrp_workcenter_productivity_loss_type);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_workcenter_productivity_loss_types/fetchdefault")
    public Page<mrp_workcenter_productivity_loss_typeImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_workcenter_productivity_loss_types/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_workcenter_productivity_loss_types/select")
    public Page<mrp_workcenter_productivity_loss_typeImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_workcenter_productivity_loss_types/{id}/getdraft")
    public mrp_workcenter_productivity_loss_typeImpl getDraft(@PathVariable("id") Integer id,@RequestBody mrp_workcenter_productivity_loss_typeImpl mrp_workcenter_productivity_loss_type);



}
