package cn.ibizlab.odoo.client.odoo_mrp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imrp_unbuild;
import cn.ibizlab.odoo.client.odoo_mrp.config.odoo_mrpClientProperties;
import cn.ibizlab.odoo.core.client.service.Imrp_unbuildClientService;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_unbuildImpl;
import cn.ibizlab.odoo.client.odoo_mrp.feign.mrp_unbuildFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[mrp_unbuild] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class mrp_unbuildClientServiceImpl implements Imrp_unbuildClientService {

    mrp_unbuildFeignClient mrp_unbuildFeignClient;

    @Autowired
    public mrp_unbuildClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_mrpClientProperties odoo_mrpClientProperties) {
        if (odoo_mrpClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mrp_unbuildFeignClient = nameBuilder.target(mrp_unbuildFeignClient.class,"http://"+odoo_mrpClientProperties.getServiceId()+"/") ;
		}else if (odoo_mrpClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.mrp_unbuildFeignClient = nameBuilder.target(mrp_unbuildFeignClient.class,odoo_mrpClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Imrp_unbuild createModel() {
		return new mrp_unbuildImpl();
	}


    public void createBatch(List<Imrp_unbuild> mrp_unbuilds){
        if(mrp_unbuilds!=null){
            List<mrp_unbuildImpl> list = new ArrayList<mrp_unbuildImpl>();
            for(Imrp_unbuild imrp_unbuild :mrp_unbuilds){
                list.add((mrp_unbuildImpl)imrp_unbuild) ;
            }
            mrp_unbuildFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Imrp_unbuild> mrp_unbuilds){
        if(mrp_unbuilds!=null){
            List<mrp_unbuildImpl> list = new ArrayList<mrp_unbuildImpl>();
            for(Imrp_unbuild imrp_unbuild :mrp_unbuilds){
                list.add((mrp_unbuildImpl)imrp_unbuild) ;
            }
            mrp_unbuildFeignClient.updateBatch(list) ;
        }
    }


    public void create(Imrp_unbuild mrp_unbuild){
        Imrp_unbuild clientModel = mrp_unbuildFeignClient.create((mrp_unbuildImpl)mrp_unbuild) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_unbuild.getClass(), false);
        copier.copy(clientModel, mrp_unbuild, null);
    }


    public void removeBatch(List<Imrp_unbuild> mrp_unbuilds){
        if(mrp_unbuilds!=null){
            List<mrp_unbuildImpl> list = new ArrayList<mrp_unbuildImpl>();
            for(Imrp_unbuild imrp_unbuild :mrp_unbuilds){
                list.add((mrp_unbuildImpl)imrp_unbuild) ;
            }
            mrp_unbuildFeignClient.removeBatch(list) ;
        }
    }


    public void update(Imrp_unbuild mrp_unbuild){
        Imrp_unbuild clientModel = mrp_unbuildFeignClient.update(mrp_unbuild.getId(),(mrp_unbuildImpl)mrp_unbuild) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_unbuild.getClass(), false);
        copier.copy(clientModel, mrp_unbuild, null);
    }


    public Page<Imrp_unbuild> fetchDefault(SearchContext context){
        Page<mrp_unbuildImpl> page = this.mrp_unbuildFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Imrp_unbuild mrp_unbuild){
        mrp_unbuildFeignClient.remove(mrp_unbuild.getId()) ;
    }


    public void get(Imrp_unbuild mrp_unbuild){
        Imrp_unbuild clientModel = mrp_unbuildFeignClient.get(mrp_unbuild.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_unbuild.getClass(), false);
        copier.copy(clientModel, mrp_unbuild, null);
    }


    public Page<Imrp_unbuild> select(SearchContext context){
        return null ;
    }


    public void getDraft(Imrp_unbuild mrp_unbuild){
        Imrp_unbuild clientModel = mrp_unbuildFeignClient.getDraft(mrp_unbuild.getId(),(mrp_unbuildImpl)mrp_unbuild) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), mrp_unbuild.getClass(), false);
        copier.copy(clientModel, mrp_unbuild, null);
    }



}

