package cn.ibizlab.odoo.client.odoo_product.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iproduct_price_list;
import cn.ibizlab.odoo.client.odoo_product.model.product_price_listImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[product_price_list] 服务对象接口
 */
public interface product_price_listFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_price_lists/createbatch")
    public product_price_listImpl createBatch(@RequestBody List<product_price_listImpl> product_price_lists);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_price_lists")
    public product_price_listImpl create(@RequestBody product_price_listImpl product_price_list);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_price_lists/updatebatch")
    public product_price_listImpl updateBatch(@RequestBody List<product_price_listImpl> product_price_lists);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_price_lists/{id}")
    public product_price_listImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_price_lists/{id}")
    public product_price_listImpl update(@PathVariable("id") Integer id,@RequestBody product_price_listImpl product_price_list);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_price_lists/fetchdefault")
    public Page<product_price_listImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_price_lists/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_price_lists/removebatch")
    public product_price_listImpl removeBatch(@RequestBody List<product_price_listImpl> product_price_lists);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_price_lists/select")
    public Page<product_price_listImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_price_lists/{id}/getdraft")
    public product_price_listImpl getDraft(@PathVariable("id") Integer id,@RequestBody product_price_listImpl product_price_list);



}
