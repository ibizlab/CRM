package cn.ibizlab.odoo.client.odoo_product.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iproduct_attribute;
import cn.ibizlab.odoo.client.odoo_product.model.product_attributeImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[product_attribute] 服务对象接口
 */
public interface product_attributeFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_attributes/{id}")
    public product_attributeImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_attributes/updatebatch")
    public product_attributeImpl updateBatch(@RequestBody List<product_attributeImpl> product_attributes);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_attributes/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_attributes/fetchdefault")
    public Page<product_attributeImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_attributes/{id}")
    public product_attributeImpl update(@PathVariable("id") Integer id,@RequestBody product_attributeImpl product_attribute);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_attributes/createbatch")
    public product_attributeImpl createBatch(@RequestBody List<product_attributeImpl> product_attributes);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_attributes")
    public product_attributeImpl create(@RequestBody product_attributeImpl product_attribute);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_attributes/removebatch")
    public product_attributeImpl removeBatch(@RequestBody List<product_attributeImpl> product_attributes);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_attributes/select")
    public Page<product_attributeImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_attributes/{id}/getdraft")
    public product_attributeImpl getDraft(@PathVariable("id") Integer id,@RequestBody product_attributeImpl product_attribute);



}
