package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_template_attribute_exclusion;
import cn.ibizlab.odoo.client.odoo_product.config.odoo_productClientProperties;
import cn.ibizlab.odoo.core.client.service.Iproduct_template_attribute_exclusionClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_template_attribute_exclusionImpl;
import cn.ibizlab.odoo.client.odoo_product.feign.product_template_attribute_exclusionFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[product_template_attribute_exclusion] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class product_template_attribute_exclusionClientServiceImpl implements Iproduct_template_attribute_exclusionClientService {

    product_template_attribute_exclusionFeignClient product_template_attribute_exclusionFeignClient;

    @Autowired
    public product_template_attribute_exclusionClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_productClientProperties odoo_productClientProperties) {
        if (odoo_productClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_template_attribute_exclusionFeignClient = nameBuilder.target(product_template_attribute_exclusionFeignClient.class,"http://"+odoo_productClientProperties.getServiceId()+"/") ;
		}else if (odoo_productClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_template_attribute_exclusionFeignClient = nameBuilder.target(product_template_attribute_exclusionFeignClient.class,odoo_productClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iproduct_template_attribute_exclusion createModel() {
		return new product_template_attribute_exclusionImpl();
	}


    public void updateBatch(List<Iproduct_template_attribute_exclusion> product_template_attribute_exclusions){
        if(product_template_attribute_exclusions!=null){
            List<product_template_attribute_exclusionImpl> list = new ArrayList<product_template_attribute_exclusionImpl>();
            for(Iproduct_template_attribute_exclusion iproduct_template_attribute_exclusion :product_template_attribute_exclusions){
                list.add((product_template_attribute_exclusionImpl)iproduct_template_attribute_exclusion) ;
            }
            product_template_attribute_exclusionFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iproduct_template_attribute_exclusion> fetchDefault(SearchContext context){
        Page<product_template_attribute_exclusionImpl> page = this.product_template_attribute_exclusionFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Iproduct_template_attribute_exclusion product_template_attribute_exclusion){
        Iproduct_template_attribute_exclusion clientModel = product_template_attribute_exclusionFeignClient.get(product_template_attribute_exclusion.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_template_attribute_exclusion.getClass(), false);
        copier.copy(clientModel, product_template_attribute_exclusion, null);
    }


    public void create(Iproduct_template_attribute_exclusion product_template_attribute_exclusion){
        Iproduct_template_attribute_exclusion clientModel = product_template_attribute_exclusionFeignClient.create((product_template_attribute_exclusionImpl)product_template_attribute_exclusion) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_template_attribute_exclusion.getClass(), false);
        copier.copy(clientModel, product_template_attribute_exclusion, null);
    }


    public void update(Iproduct_template_attribute_exclusion product_template_attribute_exclusion){
        Iproduct_template_attribute_exclusion clientModel = product_template_attribute_exclusionFeignClient.update(product_template_attribute_exclusion.getId(),(product_template_attribute_exclusionImpl)product_template_attribute_exclusion) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_template_attribute_exclusion.getClass(), false);
        copier.copy(clientModel, product_template_attribute_exclusion, null);
    }


    public void remove(Iproduct_template_attribute_exclusion product_template_attribute_exclusion){
        product_template_attribute_exclusionFeignClient.remove(product_template_attribute_exclusion.getId()) ;
    }


    public void removeBatch(List<Iproduct_template_attribute_exclusion> product_template_attribute_exclusions){
        if(product_template_attribute_exclusions!=null){
            List<product_template_attribute_exclusionImpl> list = new ArrayList<product_template_attribute_exclusionImpl>();
            for(Iproduct_template_attribute_exclusion iproduct_template_attribute_exclusion :product_template_attribute_exclusions){
                list.add((product_template_attribute_exclusionImpl)iproduct_template_attribute_exclusion) ;
            }
            product_template_attribute_exclusionFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Iproduct_template_attribute_exclusion> product_template_attribute_exclusions){
        if(product_template_attribute_exclusions!=null){
            List<product_template_attribute_exclusionImpl> list = new ArrayList<product_template_attribute_exclusionImpl>();
            for(Iproduct_template_attribute_exclusion iproduct_template_attribute_exclusion :product_template_attribute_exclusions){
                list.add((product_template_attribute_exclusionImpl)iproduct_template_attribute_exclusion) ;
            }
            product_template_attribute_exclusionFeignClient.createBatch(list) ;
        }
    }


    public Page<Iproduct_template_attribute_exclusion> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iproduct_template_attribute_exclusion product_template_attribute_exclusion){
        Iproduct_template_attribute_exclusion clientModel = product_template_attribute_exclusionFeignClient.getDraft(product_template_attribute_exclusion.getId(),(product_template_attribute_exclusionImpl)product_template_attribute_exclusion) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_template_attribute_exclusion.getClass(), false);
        copier.copy(clientModel, product_template_attribute_exclusion, null);
    }



}

