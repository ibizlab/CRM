package cn.ibizlab.odoo.client.odoo_product.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iproduct_price_list;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[product_price_list] 对象
 */
public class product_price_listImpl implements Iproduct_price_list,Serializable{

    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 价格表
     */
    public Integer price_list;

    @JsonIgnore
    public boolean price_listDirtyFlag;
    
    /**
     * 价格表
     */
    public String price_list_text;

    @JsonIgnore
    public boolean price_list_textDirtyFlag;
    
    /**
     * 数量-1
     */
    public Integer qty1;

    @JsonIgnore
    public boolean qty1DirtyFlag;
    
    /**
     * 数量-2
     */
    public Integer qty2;

    @JsonIgnore
    public boolean qty2DirtyFlag;
    
    /**
     * 数量-3
     */
    public Integer qty3;

    @JsonIgnore
    public boolean qty3DirtyFlag;
    
    /**
     * 数量-4
     */
    public Integer qty4;

    @JsonIgnore
    public boolean qty4DirtyFlag;
    
    /**
     * 数量-5
     */
    public Integer qty5;

    @JsonIgnore
    public boolean qty5DirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [价格表]
     */
    @JsonProperty("price_list")
    public Integer getPrice_list(){
        return this.price_list ;
    }

    /**
     * 设置 [价格表]
     */
    @JsonProperty("price_list")
    public void setPrice_list(Integer  price_list){
        this.price_list = price_list ;
        this.price_listDirtyFlag = true ;
    }

     /**
     * 获取 [价格表]脏标记
     */
    @JsonIgnore
    public boolean getPrice_listDirtyFlag(){
        return this.price_listDirtyFlag ;
    }   

    /**
     * 获取 [价格表]
     */
    @JsonProperty("price_list_text")
    public String getPrice_list_text(){
        return this.price_list_text ;
    }

    /**
     * 设置 [价格表]
     */
    @JsonProperty("price_list_text")
    public void setPrice_list_text(String  price_list_text){
        this.price_list_text = price_list_text ;
        this.price_list_textDirtyFlag = true ;
    }

     /**
     * 获取 [价格表]脏标记
     */
    @JsonIgnore
    public boolean getPrice_list_textDirtyFlag(){
        return this.price_list_textDirtyFlag ;
    }   

    /**
     * 获取 [数量-1]
     */
    @JsonProperty("qty1")
    public Integer getQty1(){
        return this.qty1 ;
    }

    /**
     * 设置 [数量-1]
     */
    @JsonProperty("qty1")
    public void setQty1(Integer  qty1){
        this.qty1 = qty1 ;
        this.qty1DirtyFlag = true ;
    }

     /**
     * 获取 [数量-1]脏标记
     */
    @JsonIgnore
    public boolean getQty1DirtyFlag(){
        return this.qty1DirtyFlag ;
    }   

    /**
     * 获取 [数量-2]
     */
    @JsonProperty("qty2")
    public Integer getQty2(){
        return this.qty2 ;
    }

    /**
     * 设置 [数量-2]
     */
    @JsonProperty("qty2")
    public void setQty2(Integer  qty2){
        this.qty2 = qty2 ;
        this.qty2DirtyFlag = true ;
    }

     /**
     * 获取 [数量-2]脏标记
     */
    @JsonIgnore
    public boolean getQty2DirtyFlag(){
        return this.qty2DirtyFlag ;
    }   

    /**
     * 获取 [数量-3]
     */
    @JsonProperty("qty3")
    public Integer getQty3(){
        return this.qty3 ;
    }

    /**
     * 设置 [数量-3]
     */
    @JsonProperty("qty3")
    public void setQty3(Integer  qty3){
        this.qty3 = qty3 ;
        this.qty3DirtyFlag = true ;
    }

     /**
     * 获取 [数量-3]脏标记
     */
    @JsonIgnore
    public boolean getQty3DirtyFlag(){
        return this.qty3DirtyFlag ;
    }   

    /**
     * 获取 [数量-4]
     */
    @JsonProperty("qty4")
    public Integer getQty4(){
        return this.qty4 ;
    }

    /**
     * 设置 [数量-4]
     */
    @JsonProperty("qty4")
    public void setQty4(Integer  qty4){
        this.qty4 = qty4 ;
        this.qty4DirtyFlag = true ;
    }

     /**
     * 获取 [数量-4]脏标记
     */
    @JsonIgnore
    public boolean getQty4DirtyFlag(){
        return this.qty4DirtyFlag ;
    }   

    /**
     * 获取 [数量-5]
     */
    @JsonProperty("qty5")
    public Integer getQty5(){
        return this.qty5 ;
    }

    /**
     * 设置 [数量-5]
     */
    @JsonProperty("qty5")
    public void setQty5(Integer  qty5){
        this.qty5 = qty5 ;
        this.qty5DirtyFlag = true ;
    }

     /**
     * 获取 [数量-5]脏标记
     */
    @JsonIgnore
    public boolean getQty5DirtyFlag(){
        return this.qty5DirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
