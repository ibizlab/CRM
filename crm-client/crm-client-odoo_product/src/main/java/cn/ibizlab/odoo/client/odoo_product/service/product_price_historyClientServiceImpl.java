package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_price_history;
import cn.ibizlab.odoo.client.odoo_product.config.odoo_productClientProperties;
import cn.ibizlab.odoo.core.client.service.Iproduct_price_historyClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_price_historyImpl;
import cn.ibizlab.odoo.client.odoo_product.feign.product_price_historyFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[product_price_history] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class product_price_historyClientServiceImpl implements Iproduct_price_historyClientService {

    product_price_historyFeignClient product_price_historyFeignClient;

    @Autowired
    public product_price_historyClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_productClientProperties odoo_productClientProperties) {
        if (odoo_productClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_price_historyFeignClient = nameBuilder.target(product_price_historyFeignClient.class,"http://"+odoo_productClientProperties.getServiceId()+"/") ;
		}else if (odoo_productClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_price_historyFeignClient = nameBuilder.target(product_price_historyFeignClient.class,odoo_productClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iproduct_price_history createModel() {
		return new product_price_historyImpl();
	}


    public Page<Iproduct_price_history> fetchDefault(SearchContext context){
        Page<product_price_historyImpl> page = this.product_price_historyFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Iproduct_price_history> product_price_histories){
        if(product_price_histories!=null){
            List<product_price_historyImpl> list = new ArrayList<product_price_historyImpl>();
            for(Iproduct_price_history iproduct_price_history :product_price_histories){
                list.add((product_price_historyImpl)iproduct_price_history) ;
            }
            product_price_historyFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Iproduct_price_history> product_price_histories){
        if(product_price_histories!=null){
            List<product_price_historyImpl> list = new ArrayList<product_price_historyImpl>();
            for(Iproduct_price_history iproduct_price_history :product_price_histories){
                list.add((product_price_historyImpl)iproduct_price_history) ;
            }
            product_price_historyFeignClient.createBatch(list) ;
        }
    }


    public void remove(Iproduct_price_history product_price_history){
        product_price_historyFeignClient.remove(product_price_history.getId()) ;
    }


    public void create(Iproduct_price_history product_price_history){
        Iproduct_price_history clientModel = product_price_historyFeignClient.create((product_price_historyImpl)product_price_history) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_price_history.getClass(), false);
        copier.copy(clientModel, product_price_history, null);
    }


    public void get(Iproduct_price_history product_price_history){
        Iproduct_price_history clientModel = product_price_historyFeignClient.get(product_price_history.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_price_history.getClass(), false);
        copier.copy(clientModel, product_price_history, null);
    }


    public void update(Iproduct_price_history product_price_history){
        Iproduct_price_history clientModel = product_price_historyFeignClient.update(product_price_history.getId(),(product_price_historyImpl)product_price_history) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_price_history.getClass(), false);
        copier.copy(clientModel, product_price_history, null);
    }


    public void removeBatch(List<Iproduct_price_history> product_price_histories){
        if(product_price_histories!=null){
            List<product_price_historyImpl> list = new ArrayList<product_price_historyImpl>();
            for(Iproduct_price_history iproduct_price_history :product_price_histories){
                list.add((product_price_historyImpl)iproduct_price_history) ;
            }
            product_price_historyFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iproduct_price_history> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iproduct_price_history product_price_history){
        Iproduct_price_history clientModel = product_price_historyFeignClient.getDraft(product_price_history.getId(),(product_price_historyImpl)product_price_history) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_price_history.getClass(), false);
        copier.copy(clientModel, product_price_history, null);
    }



}

