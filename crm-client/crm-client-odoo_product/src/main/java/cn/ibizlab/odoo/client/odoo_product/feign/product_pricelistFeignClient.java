package cn.ibizlab.odoo.client.odoo_product.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iproduct_pricelist;
import cn.ibizlab.odoo.client.odoo_product.model.product_pricelistImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[product_pricelist] 服务对象接口
 */
public interface product_pricelistFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_pricelists/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_pricelists/{id}")
    public product_pricelistImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_pricelists/fetchdefault")
    public Page<product_pricelistImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_pricelists/createbatch")
    public product_pricelistImpl createBatch(@RequestBody List<product_pricelistImpl> product_pricelists);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_pricelists/updatebatch")
    public product_pricelistImpl updateBatch(@RequestBody List<product_pricelistImpl> product_pricelists);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_pricelists/removebatch")
    public product_pricelistImpl removeBatch(@RequestBody List<product_pricelistImpl> product_pricelists);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_pricelists")
    public product_pricelistImpl create(@RequestBody product_pricelistImpl product_pricelist);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_pricelists/{id}")
    public product_pricelistImpl update(@PathVariable("id") Integer id,@RequestBody product_pricelistImpl product_pricelist);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_pricelists/select")
    public Page<product_pricelistImpl> select();


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_pricelists/{id}/save")
    public product_pricelistImpl save(@PathVariable("id") Integer id,@RequestBody product_pricelistImpl product_pricelist);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_pricelists/{id}/getdraft")
    public product_pricelistImpl getDraft(@PathVariable("id") Integer id,@RequestBody product_pricelistImpl product_pricelist);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_pricelists/{id}/checkkey")
    public product_pricelistImpl checkKey(@PathVariable("id") Integer id,@RequestBody product_pricelistImpl product_pricelist);



}
