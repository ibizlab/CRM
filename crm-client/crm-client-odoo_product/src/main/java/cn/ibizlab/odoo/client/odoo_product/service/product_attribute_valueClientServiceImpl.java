package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_attribute_value;
import cn.ibizlab.odoo.client.odoo_product.config.odoo_productClientProperties;
import cn.ibizlab.odoo.core.client.service.Iproduct_attribute_valueClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_attribute_valueImpl;
import cn.ibizlab.odoo.client.odoo_product.feign.product_attribute_valueFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[product_attribute_value] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class product_attribute_valueClientServiceImpl implements Iproduct_attribute_valueClientService {

    product_attribute_valueFeignClient product_attribute_valueFeignClient;

    @Autowired
    public product_attribute_valueClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_productClientProperties odoo_productClientProperties) {
        if (odoo_productClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_attribute_valueFeignClient = nameBuilder.target(product_attribute_valueFeignClient.class,"http://"+odoo_productClientProperties.getServiceId()+"/") ;
		}else if (odoo_productClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_attribute_valueFeignClient = nameBuilder.target(product_attribute_valueFeignClient.class,odoo_productClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iproduct_attribute_value createModel() {
		return new product_attribute_valueImpl();
	}


    public void update(Iproduct_attribute_value product_attribute_value){
        Iproduct_attribute_value clientModel = product_attribute_valueFeignClient.update(product_attribute_value.getId(),(product_attribute_valueImpl)product_attribute_value) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_attribute_value.getClass(), false);
        copier.copy(clientModel, product_attribute_value, null);
    }


    public void removeBatch(List<Iproduct_attribute_value> product_attribute_values){
        if(product_attribute_values!=null){
            List<product_attribute_valueImpl> list = new ArrayList<product_attribute_valueImpl>();
            for(Iproduct_attribute_value iproduct_attribute_value :product_attribute_values){
                list.add((product_attribute_valueImpl)iproduct_attribute_value) ;
            }
            product_attribute_valueFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Iproduct_attribute_value> product_attribute_values){
        if(product_attribute_values!=null){
            List<product_attribute_valueImpl> list = new ArrayList<product_attribute_valueImpl>();
            for(Iproduct_attribute_value iproduct_attribute_value :product_attribute_values){
                list.add((product_attribute_valueImpl)iproduct_attribute_value) ;
            }
            product_attribute_valueFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Iproduct_attribute_value product_attribute_value){
        product_attribute_valueFeignClient.remove(product_attribute_value.getId()) ;
    }


    public void createBatch(List<Iproduct_attribute_value> product_attribute_values){
        if(product_attribute_values!=null){
            List<product_attribute_valueImpl> list = new ArrayList<product_attribute_valueImpl>();
            for(Iproduct_attribute_value iproduct_attribute_value :product_attribute_values){
                list.add((product_attribute_valueImpl)iproduct_attribute_value) ;
            }
            product_attribute_valueFeignClient.createBatch(list) ;
        }
    }


    public void get(Iproduct_attribute_value product_attribute_value){
        Iproduct_attribute_value clientModel = product_attribute_valueFeignClient.get(product_attribute_value.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_attribute_value.getClass(), false);
        copier.copy(clientModel, product_attribute_value, null);
    }


    public void create(Iproduct_attribute_value product_attribute_value){
        Iproduct_attribute_value clientModel = product_attribute_valueFeignClient.create((product_attribute_valueImpl)product_attribute_value) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_attribute_value.getClass(), false);
        copier.copy(clientModel, product_attribute_value, null);
    }


    public Page<Iproduct_attribute_value> fetchDefault(SearchContext context){
        Page<product_attribute_valueImpl> page = this.product_attribute_valueFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Iproduct_attribute_value> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iproduct_attribute_value product_attribute_value){
        Iproduct_attribute_value clientModel = product_attribute_valueFeignClient.getDraft(product_attribute_value.getId(),(product_attribute_valueImpl)product_attribute_value) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_attribute_value.getClass(), false);
        copier.copy(clientModel, product_attribute_value, null);
    }



}

