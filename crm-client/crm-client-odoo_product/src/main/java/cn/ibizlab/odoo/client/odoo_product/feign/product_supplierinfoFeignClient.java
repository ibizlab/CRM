package cn.ibizlab.odoo.client.odoo_product.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iproduct_supplierinfo;
import cn.ibizlab.odoo.client.odoo_product.model.product_supplierinfoImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[product_supplierinfo] 服务对象接口
 */
public interface product_supplierinfoFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_supplierinfos/fetchdefault")
    public Page<product_supplierinfoImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_supplierinfos/createbatch")
    public product_supplierinfoImpl createBatch(@RequestBody List<product_supplierinfoImpl> product_supplierinfos);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_supplierinfos/{id}")
    public product_supplierinfoImpl update(@PathVariable("id") Integer id,@RequestBody product_supplierinfoImpl product_supplierinfo);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_supplierinfos/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_supplierinfos/removebatch")
    public product_supplierinfoImpl removeBatch(@RequestBody List<product_supplierinfoImpl> product_supplierinfos);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_supplierinfos/updatebatch")
    public product_supplierinfoImpl updateBatch(@RequestBody List<product_supplierinfoImpl> product_supplierinfos);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_supplierinfos")
    public product_supplierinfoImpl create(@RequestBody product_supplierinfoImpl product_supplierinfo);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_supplierinfos/{id}")
    public product_supplierinfoImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_supplierinfos/select")
    public Page<product_supplierinfoImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_supplierinfos/{id}/getdraft")
    public product_supplierinfoImpl getDraft(@PathVariable("id") Integer id,@RequestBody product_supplierinfoImpl product_supplierinfo);



}
