package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_image;
import cn.ibizlab.odoo.client.odoo_product.config.odoo_productClientProperties;
import cn.ibizlab.odoo.core.client.service.Iproduct_imageClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_imageImpl;
import cn.ibizlab.odoo.client.odoo_product.feign.product_imageFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[product_image] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class product_imageClientServiceImpl implements Iproduct_imageClientService {

    product_imageFeignClient product_imageFeignClient;

    @Autowired
    public product_imageClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_productClientProperties odoo_productClientProperties) {
        if (odoo_productClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_imageFeignClient = nameBuilder.target(product_imageFeignClient.class,"http://"+odoo_productClientProperties.getServiceId()+"/") ;
		}else if (odoo_productClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_imageFeignClient = nameBuilder.target(product_imageFeignClient.class,odoo_productClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iproduct_image createModel() {
		return new product_imageImpl();
	}


    public void create(Iproduct_image product_image){
        Iproduct_image clientModel = product_imageFeignClient.create((product_imageImpl)product_image) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_image.getClass(), false);
        copier.copy(clientModel, product_image, null);
    }


    public void remove(Iproduct_image product_image){
        product_imageFeignClient.remove(product_image.getId()) ;
    }


    public void update(Iproduct_image product_image){
        Iproduct_image clientModel = product_imageFeignClient.update(product_image.getId(),(product_imageImpl)product_image) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_image.getClass(), false);
        copier.copy(clientModel, product_image, null);
    }


    public void updateBatch(List<Iproduct_image> product_images){
        if(product_images!=null){
            List<product_imageImpl> list = new ArrayList<product_imageImpl>();
            for(Iproduct_image iproduct_image :product_images){
                list.add((product_imageImpl)iproduct_image) ;
            }
            product_imageFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iproduct_image> fetchDefault(SearchContext context){
        Page<product_imageImpl> page = this.product_imageFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Iproduct_image> product_images){
        if(product_images!=null){
            List<product_imageImpl> list = new ArrayList<product_imageImpl>();
            for(Iproduct_image iproduct_image :product_images){
                list.add((product_imageImpl)iproduct_image) ;
            }
            product_imageFeignClient.removeBatch(list) ;
        }
    }


    public void get(Iproduct_image product_image){
        Iproduct_image clientModel = product_imageFeignClient.get(product_image.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_image.getClass(), false);
        copier.copy(clientModel, product_image, null);
    }


    public void createBatch(List<Iproduct_image> product_images){
        if(product_images!=null){
            List<product_imageImpl> list = new ArrayList<product_imageImpl>();
            for(Iproduct_image iproduct_image :product_images){
                list.add((product_imageImpl)iproduct_image) ;
            }
            product_imageFeignClient.createBatch(list) ;
        }
    }


    public Page<Iproduct_image> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iproduct_image product_image){
        Iproduct_image clientModel = product_imageFeignClient.getDraft(product_image.getId(),(product_imageImpl)product_image) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_image.getClass(), false);
        copier.copy(clientModel, product_image, null);
    }



}

