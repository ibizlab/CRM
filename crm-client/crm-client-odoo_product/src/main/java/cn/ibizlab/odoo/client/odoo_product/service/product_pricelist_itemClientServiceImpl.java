package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_pricelist_item;
import cn.ibizlab.odoo.client.odoo_product.config.odoo_productClientProperties;
import cn.ibizlab.odoo.core.client.service.Iproduct_pricelist_itemClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_pricelist_itemImpl;
import cn.ibizlab.odoo.client.odoo_product.feign.product_pricelist_itemFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[product_pricelist_item] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class product_pricelist_itemClientServiceImpl implements Iproduct_pricelist_itemClientService {

    product_pricelist_itemFeignClient product_pricelist_itemFeignClient;

    @Autowired
    public product_pricelist_itemClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_productClientProperties odoo_productClientProperties) {
        if (odoo_productClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_pricelist_itemFeignClient = nameBuilder.target(product_pricelist_itemFeignClient.class,"http://"+odoo_productClientProperties.getServiceId()+"/") ;
		}else if (odoo_productClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_pricelist_itemFeignClient = nameBuilder.target(product_pricelist_itemFeignClient.class,odoo_productClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iproduct_pricelist_item createModel() {
		return new product_pricelist_itemImpl();
	}


    public void removeBatch(List<Iproduct_pricelist_item> product_pricelist_items){
        if(product_pricelist_items!=null){
            List<product_pricelist_itemImpl> list = new ArrayList<product_pricelist_itemImpl>();
            for(Iproduct_pricelist_item iproduct_pricelist_item :product_pricelist_items){
                list.add((product_pricelist_itemImpl)iproduct_pricelist_item) ;
            }
            product_pricelist_itemFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Iproduct_pricelist_item product_pricelist_item){
        product_pricelist_itemFeignClient.remove(product_pricelist_item.getId()) ;
    }


    public void update(Iproduct_pricelist_item product_pricelist_item){
        Iproduct_pricelist_item clientModel = product_pricelist_itemFeignClient.update(product_pricelist_item.getId(),(product_pricelist_itemImpl)product_pricelist_item) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_pricelist_item.getClass(), false);
        copier.copy(clientModel, product_pricelist_item, null);
    }


    public void get(Iproduct_pricelist_item product_pricelist_item){
        Iproduct_pricelist_item clientModel = product_pricelist_itemFeignClient.get(product_pricelist_item.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_pricelist_item.getClass(), false);
        copier.copy(clientModel, product_pricelist_item, null);
    }


    public void create(Iproduct_pricelist_item product_pricelist_item){
        Iproduct_pricelist_item clientModel = product_pricelist_itemFeignClient.create((product_pricelist_itemImpl)product_pricelist_item) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_pricelist_item.getClass(), false);
        copier.copy(clientModel, product_pricelist_item, null);
    }


    public void updateBatch(List<Iproduct_pricelist_item> product_pricelist_items){
        if(product_pricelist_items!=null){
            List<product_pricelist_itemImpl> list = new ArrayList<product_pricelist_itemImpl>();
            for(Iproduct_pricelist_item iproduct_pricelist_item :product_pricelist_items){
                list.add((product_pricelist_itemImpl)iproduct_pricelist_item) ;
            }
            product_pricelist_itemFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iproduct_pricelist_item> fetchDefault(SearchContext context){
        Page<product_pricelist_itemImpl> page = this.product_pricelist_itemFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Iproduct_pricelist_item> product_pricelist_items){
        if(product_pricelist_items!=null){
            List<product_pricelist_itemImpl> list = new ArrayList<product_pricelist_itemImpl>();
            for(Iproduct_pricelist_item iproduct_pricelist_item :product_pricelist_items){
                list.add((product_pricelist_itemImpl)iproduct_pricelist_item) ;
            }
            product_pricelist_itemFeignClient.createBatch(list) ;
        }
    }


    public Page<Iproduct_pricelist_item> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iproduct_pricelist_item product_pricelist_item){
        Iproduct_pricelist_item clientModel = product_pricelist_itemFeignClient.getDraft(product_pricelist_item.getId(),(product_pricelist_itemImpl)product_pricelist_item) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_pricelist_item.getClass(), false);
        copier.copy(clientModel, product_pricelist_item, null);
    }


    public void checkKey(Iproduct_pricelist_item product_pricelist_item){
        Iproduct_pricelist_item clientModel = product_pricelist_itemFeignClient.checkKey(product_pricelist_item.getId(),(product_pricelist_itemImpl)product_pricelist_item) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_pricelist_item.getClass(), false);
        copier.copy(clientModel, product_pricelist_item, null);
    }


    public void save(Iproduct_pricelist_item product_pricelist_item){
        Iproduct_pricelist_item clientModel = product_pricelist_itemFeignClient.save(product_pricelist_item.getId(),(product_pricelist_itemImpl)product_pricelist_item) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_pricelist_item.getClass(), false);
        copier.copy(clientModel, product_pricelist_item, null);
    }



}

