package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_attribute;
import cn.ibizlab.odoo.client.odoo_product.config.odoo_productClientProperties;
import cn.ibizlab.odoo.core.client.service.Iproduct_attributeClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_attributeImpl;
import cn.ibizlab.odoo.client.odoo_product.feign.product_attributeFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[product_attribute] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class product_attributeClientServiceImpl implements Iproduct_attributeClientService {

    product_attributeFeignClient product_attributeFeignClient;

    @Autowired
    public product_attributeClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_productClientProperties odoo_productClientProperties) {
        if (odoo_productClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_attributeFeignClient = nameBuilder.target(product_attributeFeignClient.class,"http://"+odoo_productClientProperties.getServiceId()+"/") ;
		}else if (odoo_productClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_attributeFeignClient = nameBuilder.target(product_attributeFeignClient.class,odoo_productClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iproduct_attribute createModel() {
		return new product_attributeImpl();
	}


    public void get(Iproduct_attribute product_attribute){
        Iproduct_attribute clientModel = product_attributeFeignClient.get(product_attribute.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_attribute.getClass(), false);
        copier.copy(clientModel, product_attribute, null);
    }


    public void updateBatch(List<Iproduct_attribute> product_attributes){
        if(product_attributes!=null){
            List<product_attributeImpl> list = new ArrayList<product_attributeImpl>();
            for(Iproduct_attribute iproduct_attribute :product_attributes){
                list.add((product_attributeImpl)iproduct_attribute) ;
            }
            product_attributeFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Iproduct_attribute product_attribute){
        product_attributeFeignClient.remove(product_attribute.getId()) ;
    }


    public Page<Iproduct_attribute> fetchDefault(SearchContext context){
        Page<product_attributeImpl> page = this.product_attributeFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Iproduct_attribute product_attribute){
        Iproduct_attribute clientModel = product_attributeFeignClient.update(product_attribute.getId(),(product_attributeImpl)product_attribute) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_attribute.getClass(), false);
        copier.copy(clientModel, product_attribute, null);
    }


    public void createBatch(List<Iproduct_attribute> product_attributes){
        if(product_attributes!=null){
            List<product_attributeImpl> list = new ArrayList<product_attributeImpl>();
            for(Iproduct_attribute iproduct_attribute :product_attributes){
                list.add((product_attributeImpl)iproduct_attribute) ;
            }
            product_attributeFeignClient.createBatch(list) ;
        }
    }


    public void create(Iproduct_attribute product_attribute){
        Iproduct_attribute clientModel = product_attributeFeignClient.create((product_attributeImpl)product_attribute) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_attribute.getClass(), false);
        copier.copy(clientModel, product_attribute, null);
    }


    public void removeBatch(List<Iproduct_attribute> product_attributes){
        if(product_attributes!=null){
            List<product_attributeImpl> list = new ArrayList<product_attributeImpl>();
            for(Iproduct_attribute iproduct_attribute :product_attributes){
                list.add((product_attributeImpl)iproduct_attribute) ;
            }
            product_attributeFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iproduct_attribute> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iproduct_attribute product_attribute){
        Iproduct_attribute clientModel = product_attributeFeignClient.getDraft(product_attribute.getId(),(product_attributeImpl)product_attribute) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_attribute.getClass(), false);
        copier.copy(clientModel, product_attribute, null);
    }



}

