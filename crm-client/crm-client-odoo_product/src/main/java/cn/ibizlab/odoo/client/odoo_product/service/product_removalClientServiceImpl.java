package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_removal;
import cn.ibizlab.odoo.client.odoo_product.config.odoo_productClientProperties;
import cn.ibizlab.odoo.core.client.service.Iproduct_removalClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_removalImpl;
import cn.ibizlab.odoo.client.odoo_product.feign.product_removalFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[product_removal] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class product_removalClientServiceImpl implements Iproduct_removalClientService {

    product_removalFeignClient product_removalFeignClient;

    @Autowired
    public product_removalClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_productClientProperties odoo_productClientProperties) {
        if (odoo_productClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_removalFeignClient = nameBuilder.target(product_removalFeignClient.class,"http://"+odoo_productClientProperties.getServiceId()+"/") ;
		}else if (odoo_productClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_removalFeignClient = nameBuilder.target(product_removalFeignClient.class,odoo_productClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iproduct_removal createModel() {
		return new product_removalImpl();
	}


    public void remove(Iproduct_removal product_removal){
        product_removalFeignClient.remove(product_removal.getId()) ;
    }


    public Page<Iproduct_removal> fetchDefault(SearchContext context){
        Page<product_removalImpl> page = this.product_removalFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Iproduct_removal> product_removals){
        if(product_removals!=null){
            List<product_removalImpl> list = new ArrayList<product_removalImpl>();
            for(Iproduct_removal iproduct_removal :product_removals){
                list.add((product_removalImpl)iproduct_removal) ;
            }
            product_removalFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Iproduct_removal> product_removals){
        if(product_removals!=null){
            List<product_removalImpl> list = new ArrayList<product_removalImpl>();
            for(Iproduct_removal iproduct_removal :product_removals){
                list.add((product_removalImpl)iproduct_removal) ;
            }
            product_removalFeignClient.createBatch(list) ;
        }
    }


    public void create(Iproduct_removal product_removal){
        Iproduct_removal clientModel = product_removalFeignClient.create((product_removalImpl)product_removal) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_removal.getClass(), false);
        copier.copy(clientModel, product_removal, null);
    }


    public void updateBatch(List<Iproduct_removal> product_removals){
        if(product_removals!=null){
            List<product_removalImpl> list = new ArrayList<product_removalImpl>();
            for(Iproduct_removal iproduct_removal :product_removals){
                list.add((product_removalImpl)iproduct_removal) ;
            }
            product_removalFeignClient.updateBatch(list) ;
        }
    }


    public void get(Iproduct_removal product_removal){
        Iproduct_removal clientModel = product_removalFeignClient.get(product_removal.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_removal.getClass(), false);
        copier.copy(clientModel, product_removal, null);
    }


    public void update(Iproduct_removal product_removal){
        Iproduct_removal clientModel = product_removalFeignClient.update(product_removal.getId(),(product_removalImpl)product_removal) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_removal.getClass(), false);
        copier.copy(clientModel, product_removal, null);
    }


    public Page<Iproduct_removal> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iproduct_removal product_removal){
        Iproduct_removal clientModel = product_removalFeignClient.getDraft(product_removal.getId(),(product_removalImpl)product_removal) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_removal.getClass(), false);
        copier.copy(clientModel, product_removal, null);
    }



}

