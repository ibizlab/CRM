package cn.ibizlab.odoo.client.odoo_product.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iproduct_template_attribute_value;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[product_template_attribute_value] 对象
 */
public class product_template_attribute_valueImpl implements Iproduct_template_attribute_value,Serializable{

    /**
     * 属性
     */
    public Integer attribute_id;

    @JsonIgnore
    public boolean attribute_idDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 排除
     */
    public String exclude_for;

    @JsonIgnore
    public boolean exclude_forDirtyFlag;
    
    /**
     * HTML 颜色索引
     */
    public String html_color;

    @JsonIgnore
    public boolean html_colorDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 是自定义值
     */
    public String is_custom;

    @JsonIgnore
    public boolean is_customDirtyFlag;
    
    /**
     * 值
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 额外属性价格
     */
    public Double price_extra;

    @JsonIgnore
    public boolean price_extraDirtyFlag;
    
    /**
     * 属性值
     */
    public Integer product_attribute_value_id;

    @JsonIgnore
    public boolean product_attribute_value_idDirtyFlag;
    
    /**
     * 产品模板
     */
    public Integer product_tmpl_id;

    @JsonIgnore
    public boolean product_tmpl_idDirtyFlag;
    
    /**
     * 产品模板
     */
    public String product_tmpl_id_text;

    @JsonIgnore
    public boolean product_tmpl_id_textDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [属性]
     */
    @JsonProperty("attribute_id")
    public Integer getAttribute_id(){
        return this.attribute_id ;
    }

    /**
     * 设置 [属性]
     */
    @JsonProperty("attribute_id")
    public void setAttribute_id(Integer  attribute_id){
        this.attribute_id = attribute_id ;
        this.attribute_idDirtyFlag = true ;
    }

     /**
     * 获取 [属性]脏标记
     */
    @JsonIgnore
    public boolean getAttribute_idDirtyFlag(){
        return this.attribute_idDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [排除]
     */
    @JsonProperty("exclude_for")
    public String getExclude_for(){
        return this.exclude_for ;
    }

    /**
     * 设置 [排除]
     */
    @JsonProperty("exclude_for")
    public void setExclude_for(String  exclude_for){
        this.exclude_for = exclude_for ;
        this.exclude_forDirtyFlag = true ;
    }

     /**
     * 获取 [排除]脏标记
     */
    @JsonIgnore
    public boolean getExclude_forDirtyFlag(){
        return this.exclude_forDirtyFlag ;
    }   

    /**
     * 获取 [HTML 颜色索引]
     */
    @JsonProperty("html_color")
    public String getHtml_color(){
        return this.html_color ;
    }

    /**
     * 设置 [HTML 颜色索引]
     */
    @JsonProperty("html_color")
    public void setHtml_color(String  html_color){
        this.html_color = html_color ;
        this.html_colorDirtyFlag = true ;
    }

     /**
     * 获取 [HTML 颜色索引]脏标记
     */
    @JsonIgnore
    public boolean getHtml_colorDirtyFlag(){
        return this.html_colorDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [是自定义值]
     */
    @JsonProperty("is_custom")
    public String getIs_custom(){
        return this.is_custom ;
    }

    /**
     * 设置 [是自定义值]
     */
    @JsonProperty("is_custom")
    public void setIs_custom(String  is_custom){
        this.is_custom = is_custom ;
        this.is_customDirtyFlag = true ;
    }

     /**
     * 获取 [是自定义值]脏标记
     */
    @JsonIgnore
    public boolean getIs_customDirtyFlag(){
        return this.is_customDirtyFlag ;
    }   

    /**
     * 获取 [值]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [值]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [值]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [额外属性价格]
     */
    @JsonProperty("price_extra")
    public Double getPrice_extra(){
        return this.price_extra ;
    }

    /**
     * 设置 [额外属性价格]
     */
    @JsonProperty("price_extra")
    public void setPrice_extra(Double  price_extra){
        this.price_extra = price_extra ;
        this.price_extraDirtyFlag = true ;
    }

     /**
     * 获取 [额外属性价格]脏标记
     */
    @JsonIgnore
    public boolean getPrice_extraDirtyFlag(){
        return this.price_extraDirtyFlag ;
    }   

    /**
     * 获取 [属性值]
     */
    @JsonProperty("product_attribute_value_id")
    public Integer getProduct_attribute_value_id(){
        return this.product_attribute_value_id ;
    }

    /**
     * 设置 [属性值]
     */
    @JsonProperty("product_attribute_value_id")
    public void setProduct_attribute_value_id(Integer  product_attribute_value_id){
        this.product_attribute_value_id = product_attribute_value_id ;
        this.product_attribute_value_idDirtyFlag = true ;
    }

     /**
     * 获取 [属性值]脏标记
     */
    @JsonIgnore
    public boolean getProduct_attribute_value_idDirtyFlag(){
        return this.product_attribute_value_idDirtyFlag ;
    }   

    /**
     * 获取 [产品模板]
     */
    @JsonProperty("product_tmpl_id")
    public Integer getProduct_tmpl_id(){
        return this.product_tmpl_id ;
    }

    /**
     * 设置 [产品模板]
     */
    @JsonProperty("product_tmpl_id")
    public void setProduct_tmpl_id(Integer  product_tmpl_id){
        this.product_tmpl_id = product_tmpl_id ;
        this.product_tmpl_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品模板]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_idDirtyFlag(){
        return this.product_tmpl_idDirtyFlag ;
    }   

    /**
     * 获取 [产品模板]
     */
    @JsonProperty("product_tmpl_id_text")
    public String getProduct_tmpl_id_text(){
        return this.product_tmpl_id_text ;
    }

    /**
     * 设置 [产品模板]
     */
    @JsonProperty("product_tmpl_id_text")
    public void setProduct_tmpl_id_text(String  product_tmpl_id_text){
        this.product_tmpl_id_text = product_tmpl_id_text ;
        this.product_tmpl_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品模板]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_id_textDirtyFlag(){
        return this.product_tmpl_id_textDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
