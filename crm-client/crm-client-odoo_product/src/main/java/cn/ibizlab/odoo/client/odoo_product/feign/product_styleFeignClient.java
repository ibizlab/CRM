package cn.ibizlab.odoo.client.odoo_product.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iproduct_style;
import cn.ibizlab.odoo.client.odoo_product.model.product_styleImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[product_style] 服务对象接口
 */
public interface product_styleFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_styles/{id}")
    public product_styleImpl update(@PathVariable("id") Integer id,@RequestBody product_styleImpl product_style);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_styles/createbatch")
    public product_styleImpl createBatch(@RequestBody List<product_styleImpl> product_styles);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_styles/fetchdefault")
    public Page<product_styleImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_styles")
    public product_styleImpl create(@RequestBody product_styleImpl product_style);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_styles/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_styles/removebatch")
    public product_styleImpl removeBatch(@RequestBody List<product_styleImpl> product_styles);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_styles/{id}")
    public product_styleImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_styles/updatebatch")
    public product_styleImpl updateBatch(@RequestBody List<product_styleImpl> product_styles);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_styles/select")
    public Page<product_styleImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_styles/{id}/getdraft")
    public product_styleImpl getDraft(@PathVariable("id") Integer id,@RequestBody product_styleImpl product_style);



}
