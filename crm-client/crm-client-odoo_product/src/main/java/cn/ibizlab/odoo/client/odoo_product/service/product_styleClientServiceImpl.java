package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_style;
import cn.ibizlab.odoo.client.odoo_product.config.odoo_productClientProperties;
import cn.ibizlab.odoo.core.client.service.Iproduct_styleClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_styleImpl;
import cn.ibizlab.odoo.client.odoo_product.feign.product_styleFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[product_style] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class product_styleClientServiceImpl implements Iproduct_styleClientService {

    product_styleFeignClient product_styleFeignClient;

    @Autowired
    public product_styleClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_productClientProperties odoo_productClientProperties) {
        if (odoo_productClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_styleFeignClient = nameBuilder.target(product_styleFeignClient.class,"http://"+odoo_productClientProperties.getServiceId()+"/") ;
		}else if (odoo_productClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.product_styleFeignClient = nameBuilder.target(product_styleFeignClient.class,odoo_productClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iproduct_style createModel() {
		return new product_styleImpl();
	}


    public void update(Iproduct_style product_style){
        Iproduct_style clientModel = product_styleFeignClient.update(product_style.getId(),(product_styleImpl)product_style) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_style.getClass(), false);
        copier.copy(clientModel, product_style, null);
    }


    public void createBatch(List<Iproduct_style> product_styles){
        if(product_styles!=null){
            List<product_styleImpl> list = new ArrayList<product_styleImpl>();
            for(Iproduct_style iproduct_style :product_styles){
                list.add((product_styleImpl)iproduct_style) ;
            }
            product_styleFeignClient.createBatch(list) ;
        }
    }


    public Page<Iproduct_style> fetchDefault(SearchContext context){
        Page<product_styleImpl> page = this.product_styleFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Iproduct_style product_style){
        Iproduct_style clientModel = product_styleFeignClient.create((product_styleImpl)product_style) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_style.getClass(), false);
        copier.copy(clientModel, product_style, null);
    }


    public void remove(Iproduct_style product_style){
        product_styleFeignClient.remove(product_style.getId()) ;
    }


    public void removeBatch(List<Iproduct_style> product_styles){
        if(product_styles!=null){
            List<product_styleImpl> list = new ArrayList<product_styleImpl>();
            for(Iproduct_style iproduct_style :product_styles){
                list.add((product_styleImpl)iproduct_style) ;
            }
            product_styleFeignClient.removeBatch(list) ;
        }
    }


    public void get(Iproduct_style product_style){
        Iproduct_style clientModel = product_styleFeignClient.get(product_style.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_style.getClass(), false);
        copier.copy(clientModel, product_style, null);
    }


    public void updateBatch(List<Iproduct_style> product_styles){
        if(product_styles!=null){
            List<product_styleImpl> list = new ArrayList<product_styleImpl>();
            for(Iproduct_style iproduct_style :product_styles){
                list.add((product_styleImpl)iproduct_style) ;
            }
            product_styleFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iproduct_style> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iproduct_style product_style){
        Iproduct_style clientModel = product_styleFeignClient.getDraft(product_style.getId(),(product_styleImpl)product_style) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), product_style.getClass(), false);
        copier.copy(clientModel, product_style, null);
    }



}

