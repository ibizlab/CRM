package cn.ibizlab.odoo.client.odoo_bus.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibus_bus;
import cn.ibizlab.odoo.client.odoo_bus.model.bus_busImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[bus_bus] 服务对象接口
 */
public interface bus_busFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_bus/bus_buses/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_bus/bus_buses/{id}")
    public bus_busImpl update(@PathVariable("id") Integer id,@RequestBody bus_busImpl bus_bus);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_bus/bus_buses")
    public bus_busImpl create(@RequestBody bus_busImpl bus_bus);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_bus/bus_buses/createbatch")
    public bus_busImpl createBatch(@RequestBody List<bus_busImpl> bus_buses);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_bus/bus_buses/{id}")
    public bus_busImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_bus/bus_buses/fetchdefault")
    public Page<bus_busImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_bus/bus_buses/updatebatch")
    public bus_busImpl updateBatch(@RequestBody List<bus_busImpl> bus_buses);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_bus/bus_buses/removebatch")
    public bus_busImpl removeBatch(@RequestBody List<bus_busImpl> bus_buses);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_bus/bus_buses/select")
    public Page<bus_busImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_bus/bus_buses/{id}/getdraft")
    public bus_busImpl getDraft(@PathVariable("id") Integer id,@RequestBody bus_busImpl bus_bus);



}
