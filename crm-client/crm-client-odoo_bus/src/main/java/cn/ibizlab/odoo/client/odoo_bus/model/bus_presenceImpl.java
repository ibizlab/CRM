package cn.ibizlab.odoo.client.odoo_bus.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ibus_presence;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[bus_presence] 对象
 */
public class bus_presenceImpl implements Ibus_presence,Serializable{

    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 最后在线
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp last_poll;

    @JsonIgnore
    public boolean last_pollDirtyFlag;
    
    /**
     * 最后登录
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp last_presence;

    @JsonIgnore
    public boolean last_presenceDirtyFlag;
    
    /**
     * IM的状态
     */
    public String status;

    @JsonIgnore
    public boolean statusDirtyFlag;
    
    /**
     * 用户
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 用户
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 最后修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [最后在线]
     */
    @JsonProperty("last_poll")
    public Timestamp getLast_poll(){
        return this.last_poll ;
    }

    /**
     * 设置 [最后在线]
     */
    @JsonProperty("last_poll")
    public void setLast_poll(Timestamp  last_poll){
        this.last_poll = last_poll ;
        this.last_pollDirtyFlag = true ;
    }

     /**
     * 获取 [最后在线]脏标记
     */
    @JsonIgnore
    public boolean getLast_pollDirtyFlag(){
        return this.last_pollDirtyFlag ;
    }   

    /**
     * 获取 [最后登录]
     */
    @JsonProperty("last_presence")
    public Timestamp getLast_presence(){
        return this.last_presence ;
    }

    /**
     * 设置 [最后登录]
     */
    @JsonProperty("last_presence")
    public void setLast_presence(Timestamp  last_presence){
        this.last_presence = last_presence ;
        this.last_presenceDirtyFlag = true ;
    }

     /**
     * 获取 [最后登录]脏标记
     */
    @JsonIgnore
    public boolean getLast_presenceDirtyFlag(){
        return this.last_presenceDirtyFlag ;
    }   

    /**
     * 获取 [IM的状态]
     */
    @JsonProperty("status")
    public String getStatus(){
        return this.status ;
    }

    /**
     * 设置 [IM的状态]
     */
    @JsonProperty("status")
    public void setStatus(String  status){
        this.status = status ;
        this.statusDirtyFlag = true ;
    }

     /**
     * 获取 [IM的状态]脏标记
     */
    @JsonIgnore
    public boolean getStatusDirtyFlag(){
        return this.statusDirtyFlag ;
    }   

    /**
     * 获取 [用户]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [用户]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [用户]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [用户]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [用户]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [用户]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改时间]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改时间]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改时间]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
