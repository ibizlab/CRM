package cn.ibizlab.odoo.client.odoo_bus.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibus_bus;
import cn.ibizlab.odoo.client.odoo_bus.config.odoo_busClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibus_busClientService;
import cn.ibizlab.odoo.client.odoo_bus.model.bus_busImpl;
import cn.ibizlab.odoo.client.odoo_bus.feign.bus_busFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[bus_bus] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class bus_busClientServiceImpl implements Ibus_busClientService {

    bus_busFeignClient bus_busFeignClient;

    @Autowired
    public bus_busClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_busClientProperties odoo_busClientProperties) {
        if (odoo_busClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.bus_busFeignClient = nameBuilder.target(bus_busFeignClient.class,"http://"+odoo_busClientProperties.getServiceId()+"/") ;
		}else if (odoo_busClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.bus_busFeignClient = nameBuilder.target(bus_busFeignClient.class,odoo_busClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibus_bus createModel() {
		return new bus_busImpl();
	}


    public void remove(Ibus_bus bus_bus){
        bus_busFeignClient.remove(bus_bus.getId()) ;
    }


    public void update(Ibus_bus bus_bus){
        Ibus_bus clientModel = bus_busFeignClient.update(bus_bus.getId(),(bus_busImpl)bus_bus) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), bus_bus.getClass(), false);
        copier.copy(clientModel, bus_bus, null);
    }


    public void create(Ibus_bus bus_bus){
        Ibus_bus clientModel = bus_busFeignClient.create((bus_busImpl)bus_bus) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), bus_bus.getClass(), false);
        copier.copy(clientModel, bus_bus, null);
    }


    public void createBatch(List<Ibus_bus> bus_buses){
        if(bus_buses!=null){
            List<bus_busImpl> list = new ArrayList<bus_busImpl>();
            for(Ibus_bus ibus_bus :bus_buses){
                list.add((bus_busImpl)ibus_bus) ;
            }
            bus_busFeignClient.createBatch(list) ;
        }
    }


    public void get(Ibus_bus bus_bus){
        Ibus_bus clientModel = bus_busFeignClient.get(bus_bus.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), bus_bus.getClass(), false);
        copier.copy(clientModel, bus_bus, null);
    }


    public Page<Ibus_bus> fetchDefault(SearchContext context){
        Page<bus_busImpl> page = this.bus_busFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Ibus_bus> bus_buses){
        if(bus_buses!=null){
            List<bus_busImpl> list = new ArrayList<bus_busImpl>();
            for(Ibus_bus ibus_bus :bus_buses){
                list.add((bus_busImpl)ibus_bus) ;
            }
            bus_busFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Ibus_bus> bus_buses){
        if(bus_buses!=null){
            List<bus_busImpl> list = new ArrayList<bus_busImpl>();
            for(Ibus_bus ibus_bus :bus_buses){
                list.add((bus_busImpl)ibus_bus) ;
            }
            bus_busFeignClient.removeBatch(list) ;
        }
    }


    public Page<Ibus_bus> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibus_bus bus_bus){
        Ibus_bus clientModel = bus_busFeignClient.getDraft(bus_bus.getId(),(bus_busImpl)bus_bus) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), bus_bus.getClass(), false);
        copier.copy(clientModel, bus_bus, null);
    }



}

