package cn.ibizlab.odoo.client.odoo_purchase.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ipurchase_order;
import cn.ibizlab.odoo.client.odoo_purchase.model.purchase_orderImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[purchase_order] 服务对象接口
 */
public interface purchase_orderFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_purchase/purchase_orders/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_purchase/purchase_orders/fetchdefault")
    public Page<purchase_orderImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_purchase/purchase_orders/{id}")
    public purchase_orderImpl update(@PathVariable("id") Integer id,@RequestBody purchase_orderImpl purchase_order);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_purchase/purchase_orders/removebatch")
    public purchase_orderImpl removeBatch(@RequestBody List<purchase_orderImpl> purchase_orders);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_purchase/purchase_orders/createbatch")
    public purchase_orderImpl createBatch(@RequestBody List<purchase_orderImpl> purchase_orders);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_purchase/purchase_orders/updatebatch")
    public purchase_orderImpl updateBatch(@RequestBody List<purchase_orderImpl> purchase_orders);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_purchase/purchase_orders/{id}")
    public purchase_orderImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_purchase/purchase_orders")
    public purchase_orderImpl create(@RequestBody purchase_orderImpl purchase_order);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_purchase/purchase_orders/select")
    public Page<purchase_orderImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_purchase/purchase_orders/{id}/getdraft")
    public purchase_orderImpl getDraft(@PathVariable("id") Integer id,@RequestBody purchase_orderImpl purchase_order);



}
