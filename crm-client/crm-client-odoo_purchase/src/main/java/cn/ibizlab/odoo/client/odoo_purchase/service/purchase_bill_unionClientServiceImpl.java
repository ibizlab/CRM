package cn.ibizlab.odoo.client.odoo_purchase.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ipurchase_bill_union;
import cn.ibizlab.odoo.client.odoo_purchase.config.odoo_purchaseClientProperties;
import cn.ibizlab.odoo.core.client.service.Ipurchase_bill_unionClientService;
import cn.ibizlab.odoo.client.odoo_purchase.model.purchase_bill_unionImpl;
import cn.ibizlab.odoo.client.odoo_purchase.feign.purchase_bill_unionFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[purchase_bill_union] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class purchase_bill_unionClientServiceImpl implements Ipurchase_bill_unionClientService {

    purchase_bill_unionFeignClient purchase_bill_unionFeignClient;

    @Autowired
    public purchase_bill_unionClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_purchaseClientProperties odoo_purchaseClientProperties) {
        if (odoo_purchaseClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.purchase_bill_unionFeignClient = nameBuilder.target(purchase_bill_unionFeignClient.class,"http://"+odoo_purchaseClientProperties.getServiceId()+"/") ;
		}else if (odoo_purchaseClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.purchase_bill_unionFeignClient = nameBuilder.target(purchase_bill_unionFeignClient.class,odoo_purchaseClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ipurchase_bill_union createModel() {
		return new purchase_bill_unionImpl();
	}


    public void remove(Ipurchase_bill_union purchase_bill_union){
        purchase_bill_unionFeignClient.remove(purchase_bill_union.getId()) ;
    }


    public void create(Ipurchase_bill_union purchase_bill_union){
        Ipurchase_bill_union clientModel = purchase_bill_unionFeignClient.create((purchase_bill_unionImpl)purchase_bill_union) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), purchase_bill_union.getClass(), false);
        copier.copy(clientModel, purchase_bill_union, null);
    }


    public Page<Ipurchase_bill_union> fetchDefault(SearchContext context){
        Page<purchase_bill_unionImpl> page = this.purchase_bill_unionFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Ipurchase_bill_union purchase_bill_union){
        Ipurchase_bill_union clientModel = purchase_bill_unionFeignClient.get(purchase_bill_union.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), purchase_bill_union.getClass(), false);
        copier.copy(clientModel, purchase_bill_union, null);
    }


    public void createBatch(List<Ipurchase_bill_union> purchase_bill_unions){
        if(purchase_bill_unions!=null){
            List<purchase_bill_unionImpl> list = new ArrayList<purchase_bill_unionImpl>();
            for(Ipurchase_bill_union ipurchase_bill_union :purchase_bill_unions){
                list.add((purchase_bill_unionImpl)ipurchase_bill_union) ;
            }
            purchase_bill_unionFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Ipurchase_bill_union> purchase_bill_unions){
        if(purchase_bill_unions!=null){
            List<purchase_bill_unionImpl> list = new ArrayList<purchase_bill_unionImpl>();
            for(Ipurchase_bill_union ipurchase_bill_union :purchase_bill_unions){
                list.add((purchase_bill_unionImpl)ipurchase_bill_union) ;
            }
            purchase_bill_unionFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Ipurchase_bill_union> purchase_bill_unions){
        if(purchase_bill_unions!=null){
            List<purchase_bill_unionImpl> list = new ArrayList<purchase_bill_unionImpl>();
            for(Ipurchase_bill_union ipurchase_bill_union :purchase_bill_unions){
                list.add((purchase_bill_unionImpl)ipurchase_bill_union) ;
            }
            purchase_bill_unionFeignClient.updateBatch(list) ;
        }
    }


    public void update(Ipurchase_bill_union purchase_bill_union){
        Ipurchase_bill_union clientModel = purchase_bill_unionFeignClient.update(purchase_bill_union.getId(),(purchase_bill_unionImpl)purchase_bill_union) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), purchase_bill_union.getClass(), false);
        copier.copy(clientModel, purchase_bill_union, null);
    }


    public Page<Ipurchase_bill_union> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ipurchase_bill_union purchase_bill_union){
        Ipurchase_bill_union clientModel = purchase_bill_unionFeignClient.getDraft(purchase_bill_union.getId(),(purchase_bill_unionImpl)purchase_bill_union) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), purchase_bill_union.getClass(), false);
        copier.copy(clientModel, purchase_bill_union, null);
    }



}

