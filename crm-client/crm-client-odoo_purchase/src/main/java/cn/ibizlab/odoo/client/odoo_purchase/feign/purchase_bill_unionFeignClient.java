package cn.ibizlab.odoo.client.odoo_purchase.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ipurchase_bill_union;
import cn.ibizlab.odoo.client.odoo_purchase.model.purchase_bill_unionImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[purchase_bill_union] 服务对象接口
 */
public interface purchase_bill_unionFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_purchase/purchase_bill_unions/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_purchase/purchase_bill_unions")
    public purchase_bill_unionImpl create(@RequestBody purchase_bill_unionImpl purchase_bill_union);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_purchase/purchase_bill_unions/fetchdefault")
    public Page<purchase_bill_unionImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_purchase/purchase_bill_unions/{id}")
    public purchase_bill_unionImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_purchase/purchase_bill_unions/createbatch")
    public purchase_bill_unionImpl createBatch(@RequestBody List<purchase_bill_unionImpl> purchase_bill_unions);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_purchase/purchase_bill_unions/removebatch")
    public purchase_bill_unionImpl removeBatch(@RequestBody List<purchase_bill_unionImpl> purchase_bill_unions);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_purchase/purchase_bill_unions/updatebatch")
    public purchase_bill_unionImpl updateBatch(@RequestBody List<purchase_bill_unionImpl> purchase_bill_unions);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_purchase/purchase_bill_unions/{id}")
    public purchase_bill_unionImpl update(@PathVariable("id") Integer id,@RequestBody purchase_bill_unionImpl purchase_bill_union);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_purchase/purchase_bill_unions/select")
    public Page<purchase_bill_unionImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_purchase/purchase_bill_unions/{id}/getdraft")
    public purchase_bill_unionImpl getDraft(@PathVariable("id") Integer id,@RequestBody purchase_bill_unionImpl purchase_bill_union);



}
