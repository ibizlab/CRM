package cn.ibizlab.odoo.client.odoo_sms.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isms_api;
import cn.ibizlab.odoo.client.odoo_sms.config.odoo_smsClientProperties;
import cn.ibizlab.odoo.core.client.service.Isms_apiClientService;
import cn.ibizlab.odoo.client.odoo_sms.model.sms_apiImpl;
import cn.ibizlab.odoo.client.odoo_sms.feign.sms_apiFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[sms_api] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class sms_apiClientServiceImpl implements Isms_apiClientService {

    sms_apiFeignClient sms_apiFeignClient;

    @Autowired
    public sms_apiClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_smsClientProperties odoo_smsClientProperties) {
        if (odoo_smsClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.sms_apiFeignClient = nameBuilder.target(sms_apiFeignClient.class,"http://"+odoo_smsClientProperties.getServiceId()+"/") ;
		}else if (odoo_smsClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.sms_apiFeignClient = nameBuilder.target(sms_apiFeignClient.class,odoo_smsClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Isms_api createModel() {
		return new sms_apiImpl();
	}


    public void create(Isms_api sms_api){
        Isms_api clientModel = sms_apiFeignClient.create((sms_apiImpl)sms_api) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sms_api.getClass(), false);
        copier.copy(clientModel, sms_api, null);
    }


    public void updateBatch(List<Isms_api> sms_apis){
        if(sms_apis!=null){
            List<sms_apiImpl> list = new ArrayList<sms_apiImpl>();
            for(Isms_api isms_api :sms_apis){
                list.add((sms_apiImpl)isms_api) ;
            }
            sms_apiFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Isms_api> sms_apis){
        if(sms_apis!=null){
            List<sms_apiImpl> list = new ArrayList<sms_apiImpl>();
            for(Isms_api isms_api :sms_apis){
                list.add((sms_apiImpl)isms_api) ;
            }
            sms_apiFeignClient.removeBatch(list) ;
        }
    }


    public Page<Isms_api> fetchDefault(SearchContext context){
        Page<sms_apiImpl> page = this.sms_apiFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Isms_api sms_api){
        sms_apiFeignClient.remove(sms_api.getId()) ;
    }


    public void get(Isms_api sms_api){
        Isms_api clientModel = sms_apiFeignClient.get(sms_api.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sms_api.getClass(), false);
        copier.copy(clientModel, sms_api, null);
    }


    public void update(Isms_api sms_api){
        Isms_api clientModel = sms_apiFeignClient.update(sms_api.getId(),(sms_apiImpl)sms_api) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sms_api.getClass(), false);
        copier.copy(clientModel, sms_api, null);
    }


    public void createBatch(List<Isms_api> sms_apis){
        if(sms_apis!=null){
            List<sms_apiImpl> list = new ArrayList<sms_apiImpl>();
            for(Isms_api isms_api :sms_apis){
                list.add((sms_apiImpl)isms_api) ;
            }
            sms_apiFeignClient.createBatch(list) ;
        }
    }


    public Page<Isms_api> select(SearchContext context){
        return null ;
    }


    public void getDraft(Isms_api sms_api){
        Isms_api clientModel = sms_apiFeignClient.getDraft(sms_api.getId(),(sms_apiImpl)sms_api) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), sms_api.getClass(), false);
        copier.copy(clientModel, sms_api, null);
    }



}

