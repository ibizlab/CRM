package cn.ibizlab.odoo.client.odoo_note.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Inote_tag;
import cn.ibizlab.odoo.client.odoo_note.config.odoo_noteClientProperties;
import cn.ibizlab.odoo.core.client.service.Inote_tagClientService;
import cn.ibizlab.odoo.client.odoo_note.model.note_tagImpl;
import cn.ibizlab.odoo.client.odoo_note.feign.note_tagFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[note_tag] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class note_tagClientServiceImpl implements Inote_tagClientService {

    note_tagFeignClient note_tagFeignClient;

    @Autowired
    public note_tagClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_noteClientProperties odoo_noteClientProperties) {
        if (odoo_noteClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.note_tagFeignClient = nameBuilder.target(note_tagFeignClient.class,"http://"+odoo_noteClientProperties.getServiceId()+"/") ;
		}else if (odoo_noteClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.note_tagFeignClient = nameBuilder.target(note_tagFeignClient.class,odoo_noteClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Inote_tag createModel() {
		return new note_tagImpl();
	}


    public void removeBatch(List<Inote_tag> note_tags){
        if(note_tags!=null){
            List<note_tagImpl> list = new ArrayList<note_tagImpl>();
            for(Inote_tag inote_tag :note_tags){
                list.add((note_tagImpl)inote_tag) ;
            }
            note_tagFeignClient.removeBatch(list) ;
        }
    }


    public void get(Inote_tag note_tag){
        Inote_tag clientModel = note_tagFeignClient.get(note_tag.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), note_tag.getClass(), false);
        copier.copy(clientModel, note_tag, null);
    }


    public void create(Inote_tag note_tag){
        Inote_tag clientModel = note_tagFeignClient.create((note_tagImpl)note_tag) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), note_tag.getClass(), false);
        copier.copy(clientModel, note_tag, null);
    }


    public Page<Inote_tag> fetchDefault(SearchContext context){
        Page<note_tagImpl> page = this.note_tagFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Inote_tag note_tag){
        note_tagFeignClient.remove(note_tag.getId()) ;
    }


    public void createBatch(List<Inote_tag> note_tags){
        if(note_tags!=null){
            List<note_tagImpl> list = new ArrayList<note_tagImpl>();
            for(Inote_tag inote_tag :note_tags){
                list.add((note_tagImpl)inote_tag) ;
            }
            note_tagFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Inote_tag> note_tags){
        if(note_tags!=null){
            List<note_tagImpl> list = new ArrayList<note_tagImpl>();
            for(Inote_tag inote_tag :note_tags){
                list.add((note_tagImpl)inote_tag) ;
            }
            note_tagFeignClient.updateBatch(list) ;
        }
    }


    public void update(Inote_tag note_tag){
        Inote_tag clientModel = note_tagFeignClient.update(note_tag.getId(),(note_tagImpl)note_tag) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), note_tag.getClass(), false);
        copier.copy(clientModel, note_tag, null);
    }


    public Page<Inote_tag> select(SearchContext context){
        return null ;
    }


    public void getDraft(Inote_tag note_tag){
        Inote_tag clientModel = note_tagFeignClient.getDraft(note_tag.getId(),(note_tagImpl)note_tag) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), note_tag.getClass(), false);
        copier.copy(clientModel, note_tag, null);
    }



}

