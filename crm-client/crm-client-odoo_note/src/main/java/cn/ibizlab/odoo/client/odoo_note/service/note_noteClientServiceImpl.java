package cn.ibizlab.odoo.client.odoo_note.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Inote_note;
import cn.ibizlab.odoo.client.odoo_note.config.odoo_noteClientProperties;
import cn.ibizlab.odoo.core.client.service.Inote_noteClientService;
import cn.ibizlab.odoo.client.odoo_note.model.note_noteImpl;
import cn.ibizlab.odoo.client.odoo_note.feign.note_noteFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[note_note] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class note_noteClientServiceImpl implements Inote_noteClientService {

    note_noteFeignClient note_noteFeignClient;

    @Autowired
    public note_noteClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_noteClientProperties odoo_noteClientProperties) {
        if (odoo_noteClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.note_noteFeignClient = nameBuilder.target(note_noteFeignClient.class,"http://"+odoo_noteClientProperties.getServiceId()+"/") ;
		}else if (odoo_noteClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.note_noteFeignClient = nameBuilder.target(note_noteFeignClient.class,odoo_noteClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Inote_note createModel() {
		return new note_noteImpl();
	}


    public void updateBatch(List<Inote_note> note_notes){
        if(note_notes!=null){
            List<note_noteImpl> list = new ArrayList<note_noteImpl>();
            for(Inote_note inote_note :note_notes){
                list.add((note_noteImpl)inote_note) ;
            }
            note_noteFeignClient.updateBatch(list) ;
        }
    }


    public void update(Inote_note note_note){
        Inote_note clientModel = note_noteFeignClient.update(note_note.getId(),(note_noteImpl)note_note) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), note_note.getClass(), false);
        copier.copy(clientModel, note_note, null);
    }


    public Page<Inote_note> fetchDefault(SearchContext context){
        Page<note_noteImpl> page = this.note_noteFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Inote_note note_note){
        Inote_note clientModel = note_noteFeignClient.get(note_note.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), note_note.getClass(), false);
        copier.copy(clientModel, note_note, null);
    }


    public void createBatch(List<Inote_note> note_notes){
        if(note_notes!=null){
            List<note_noteImpl> list = new ArrayList<note_noteImpl>();
            for(Inote_note inote_note :note_notes){
                list.add((note_noteImpl)inote_note) ;
            }
            note_noteFeignClient.createBatch(list) ;
        }
    }


    public void remove(Inote_note note_note){
        note_noteFeignClient.remove(note_note.getId()) ;
    }


    public void removeBatch(List<Inote_note> note_notes){
        if(note_notes!=null){
            List<note_noteImpl> list = new ArrayList<note_noteImpl>();
            for(Inote_note inote_note :note_notes){
                list.add((note_noteImpl)inote_note) ;
            }
            note_noteFeignClient.removeBatch(list) ;
        }
    }


    public void create(Inote_note note_note){
        Inote_note clientModel = note_noteFeignClient.create((note_noteImpl)note_note) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), note_note.getClass(), false);
        copier.copy(clientModel, note_note, null);
    }


    public Page<Inote_note> select(SearchContext context){
        return null ;
    }


    public void getDraft(Inote_note note_note){
        Inote_note clientModel = note_noteFeignClient.getDraft(note_note.getId(),(note_noteImpl)note_note) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), note_note.getClass(), false);
        copier.copy(clientModel, note_note, null);
    }



}

