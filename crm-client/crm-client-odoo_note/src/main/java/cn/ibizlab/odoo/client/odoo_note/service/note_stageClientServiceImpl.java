package cn.ibizlab.odoo.client.odoo_note.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Inote_stage;
import cn.ibizlab.odoo.client.odoo_note.config.odoo_noteClientProperties;
import cn.ibizlab.odoo.core.client.service.Inote_stageClientService;
import cn.ibizlab.odoo.client.odoo_note.model.note_stageImpl;
import cn.ibizlab.odoo.client.odoo_note.feign.note_stageFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[note_stage] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class note_stageClientServiceImpl implements Inote_stageClientService {

    note_stageFeignClient note_stageFeignClient;

    @Autowired
    public note_stageClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_noteClientProperties odoo_noteClientProperties) {
        if (odoo_noteClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.note_stageFeignClient = nameBuilder.target(note_stageFeignClient.class,"http://"+odoo_noteClientProperties.getServiceId()+"/") ;
		}else if (odoo_noteClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.note_stageFeignClient = nameBuilder.target(note_stageFeignClient.class,odoo_noteClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Inote_stage createModel() {
		return new note_stageImpl();
	}


    public void removeBatch(List<Inote_stage> note_stages){
        if(note_stages!=null){
            List<note_stageImpl> list = new ArrayList<note_stageImpl>();
            for(Inote_stage inote_stage :note_stages){
                list.add((note_stageImpl)inote_stage) ;
            }
            note_stageFeignClient.removeBatch(list) ;
        }
    }


    public void get(Inote_stage note_stage){
        Inote_stage clientModel = note_stageFeignClient.get(note_stage.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), note_stage.getClass(), false);
        copier.copy(clientModel, note_stage, null);
    }


    public void update(Inote_stage note_stage){
        Inote_stage clientModel = note_stageFeignClient.update(note_stage.getId(),(note_stageImpl)note_stage) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), note_stage.getClass(), false);
        copier.copy(clientModel, note_stage, null);
    }


    public void create(Inote_stage note_stage){
        Inote_stage clientModel = note_stageFeignClient.create((note_stageImpl)note_stage) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), note_stage.getClass(), false);
        copier.copy(clientModel, note_stage, null);
    }


    public void remove(Inote_stage note_stage){
        note_stageFeignClient.remove(note_stage.getId()) ;
    }


    public Page<Inote_stage> fetchDefault(SearchContext context){
        Page<note_stageImpl> page = this.note_stageFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Inote_stage> note_stages){
        if(note_stages!=null){
            List<note_stageImpl> list = new ArrayList<note_stageImpl>();
            for(Inote_stage inote_stage :note_stages){
                list.add((note_stageImpl)inote_stage) ;
            }
            note_stageFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Inote_stage> note_stages){
        if(note_stages!=null){
            List<note_stageImpl> list = new ArrayList<note_stageImpl>();
            for(Inote_stage inote_stage :note_stages){
                list.add((note_stageImpl)inote_stage) ;
            }
            note_stageFeignClient.createBatch(list) ;
        }
    }


    public Page<Inote_stage> select(SearchContext context){
        return null ;
    }


    public void getDraft(Inote_stage note_stage){
        Inote_stage clientModel = note_stageFeignClient.getDraft(note_stage.getId(),(note_stageImpl)note_stage) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), note_stage.getClass(), false);
        copier.copy(clientModel, note_stage, null);
    }



}

