package cn.ibizlab.odoo.client.odoo_web_editor.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iweb_editor_converter_test;
import cn.ibizlab.odoo.client.odoo_web_editor.config.odoo_web_editorClientProperties;
import cn.ibizlab.odoo.core.client.service.Iweb_editor_converter_testClientService;
import cn.ibizlab.odoo.client.odoo_web_editor.model.web_editor_converter_testImpl;
import cn.ibizlab.odoo.client.odoo_web_editor.feign.web_editor_converter_testFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[web_editor_converter_test] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class web_editor_converter_testClientServiceImpl implements Iweb_editor_converter_testClientService {

    web_editor_converter_testFeignClient web_editor_converter_testFeignClient;

    @Autowired
    public web_editor_converter_testClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_web_editorClientProperties odoo_web_editorClientProperties) {
        if (odoo_web_editorClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.web_editor_converter_testFeignClient = nameBuilder.target(web_editor_converter_testFeignClient.class,"http://"+odoo_web_editorClientProperties.getServiceId()+"/") ;
		}else if (odoo_web_editorClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.web_editor_converter_testFeignClient = nameBuilder.target(web_editor_converter_testFeignClient.class,odoo_web_editorClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iweb_editor_converter_test createModel() {
		return new web_editor_converter_testImpl();
	}


    public void removeBatch(List<Iweb_editor_converter_test> web_editor_converter_tests){
        if(web_editor_converter_tests!=null){
            List<web_editor_converter_testImpl> list = new ArrayList<web_editor_converter_testImpl>();
            for(Iweb_editor_converter_test iweb_editor_converter_test :web_editor_converter_tests){
                list.add((web_editor_converter_testImpl)iweb_editor_converter_test) ;
            }
            web_editor_converter_testFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Iweb_editor_converter_test web_editor_converter_test){
        web_editor_converter_testFeignClient.remove(web_editor_converter_test.getId()) ;
    }


    public void update(Iweb_editor_converter_test web_editor_converter_test){
        Iweb_editor_converter_test clientModel = web_editor_converter_testFeignClient.update(web_editor_converter_test.getId(),(web_editor_converter_testImpl)web_editor_converter_test) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), web_editor_converter_test.getClass(), false);
        copier.copy(clientModel, web_editor_converter_test, null);
    }


    public void createBatch(List<Iweb_editor_converter_test> web_editor_converter_tests){
        if(web_editor_converter_tests!=null){
            List<web_editor_converter_testImpl> list = new ArrayList<web_editor_converter_testImpl>();
            for(Iweb_editor_converter_test iweb_editor_converter_test :web_editor_converter_tests){
                list.add((web_editor_converter_testImpl)iweb_editor_converter_test) ;
            }
            web_editor_converter_testFeignClient.createBatch(list) ;
        }
    }


    public void create(Iweb_editor_converter_test web_editor_converter_test){
        Iweb_editor_converter_test clientModel = web_editor_converter_testFeignClient.create((web_editor_converter_testImpl)web_editor_converter_test) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), web_editor_converter_test.getClass(), false);
        copier.copy(clientModel, web_editor_converter_test, null);
    }


    public Page<Iweb_editor_converter_test> fetchDefault(SearchContext context){
        Page<web_editor_converter_testImpl> page = this.web_editor_converter_testFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Iweb_editor_converter_test web_editor_converter_test){
        Iweb_editor_converter_test clientModel = web_editor_converter_testFeignClient.get(web_editor_converter_test.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), web_editor_converter_test.getClass(), false);
        copier.copy(clientModel, web_editor_converter_test, null);
    }


    public void updateBatch(List<Iweb_editor_converter_test> web_editor_converter_tests){
        if(web_editor_converter_tests!=null){
            List<web_editor_converter_testImpl> list = new ArrayList<web_editor_converter_testImpl>();
            for(Iweb_editor_converter_test iweb_editor_converter_test :web_editor_converter_tests){
                list.add((web_editor_converter_testImpl)iweb_editor_converter_test) ;
            }
            web_editor_converter_testFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iweb_editor_converter_test> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iweb_editor_converter_test web_editor_converter_test){
        Iweb_editor_converter_test clientModel = web_editor_converter_testFeignClient.getDraft(web_editor_converter_test.getId(),(web_editor_converter_testImpl)web_editor_converter_test) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), web_editor_converter_test.getClass(), false);
        copier.copy(clientModel, web_editor_converter_test, null);
    }



}

