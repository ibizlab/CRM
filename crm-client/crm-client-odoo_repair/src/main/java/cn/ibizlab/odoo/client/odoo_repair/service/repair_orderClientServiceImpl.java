package cn.ibizlab.odoo.client.odoo_repair.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Irepair_order;
import cn.ibizlab.odoo.client.odoo_repair.config.odoo_repairClientProperties;
import cn.ibizlab.odoo.core.client.service.Irepair_orderClientService;
import cn.ibizlab.odoo.client.odoo_repair.model.repair_orderImpl;
import cn.ibizlab.odoo.client.odoo_repair.feign.repair_orderFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[repair_order] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class repair_orderClientServiceImpl implements Irepair_orderClientService {

    repair_orderFeignClient repair_orderFeignClient;

    @Autowired
    public repair_orderClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_repairClientProperties odoo_repairClientProperties) {
        if (odoo_repairClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.repair_orderFeignClient = nameBuilder.target(repair_orderFeignClient.class,"http://"+odoo_repairClientProperties.getServiceId()+"/") ;
		}else if (odoo_repairClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.repair_orderFeignClient = nameBuilder.target(repair_orderFeignClient.class,odoo_repairClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Irepair_order createModel() {
		return new repair_orderImpl();
	}


    public void createBatch(List<Irepair_order> repair_orders){
        if(repair_orders!=null){
            List<repair_orderImpl> list = new ArrayList<repair_orderImpl>();
            for(Irepair_order irepair_order :repair_orders){
                list.add((repair_orderImpl)irepair_order) ;
            }
            repair_orderFeignClient.createBatch(list) ;
        }
    }


    public void update(Irepair_order repair_order){
        Irepair_order clientModel = repair_orderFeignClient.update(repair_order.getId(),(repair_orderImpl)repair_order) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), repair_order.getClass(), false);
        copier.copy(clientModel, repair_order, null);
    }


    public void removeBatch(List<Irepair_order> repair_orders){
        if(repair_orders!=null){
            List<repair_orderImpl> list = new ArrayList<repair_orderImpl>();
            for(Irepair_order irepair_order :repair_orders){
                list.add((repair_orderImpl)irepair_order) ;
            }
            repair_orderFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Irepair_order> repair_orders){
        if(repair_orders!=null){
            List<repair_orderImpl> list = new ArrayList<repair_orderImpl>();
            for(Irepair_order irepair_order :repair_orders){
                list.add((repair_orderImpl)irepair_order) ;
            }
            repair_orderFeignClient.updateBatch(list) ;
        }
    }


    public void create(Irepair_order repair_order){
        Irepair_order clientModel = repair_orderFeignClient.create((repair_orderImpl)repair_order) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), repair_order.getClass(), false);
        copier.copy(clientModel, repair_order, null);
    }


    public Page<Irepair_order> fetchDefault(SearchContext context){
        Page<repair_orderImpl> page = this.repair_orderFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Irepair_order repair_order){
        Irepair_order clientModel = repair_orderFeignClient.get(repair_order.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), repair_order.getClass(), false);
        copier.copy(clientModel, repair_order, null);
    }


    public void remove(Irepair_order repair_order){
        repair_orderFeignClient.remove(repair_order.getId()) ;
    }


    public Page<Irepair_order> select(SearchContext context){
        return null ;
    }


    public void getDraft(Irepair_order repair_order){
        Irepair_order clientModel = repair_orderFeignClient.getDraft(repair_order.getId(),(repair_orderImpl)repair_order) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), repair_order.getClass(), false);
        copier.copy(clientModel, repair_order, null);
    }



}

