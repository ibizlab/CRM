package cn.ibizlab.odoo.client.odoo_repair.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Irepair_fee;
import cn.ibizlab.odoo.client.odoo_repair.model.repair_feeImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[repair_fee] 服务对象接口
 */
public interface repair_feeFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_repair/repair_fees/{id}")
    public repair_feeImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_repair/repair_fees")
    public repair_feeImpl create(@RequestBody repair_feeImpl repair_fee);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_repair/repair_fees/updatebatch")
    public repair_feeImpl updateBatch(@RequestBody List<repair_feeImpl> repair_fees);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_repair/repair_fees/createbatch")
    public repair_feeImpl createBatch(@RequestBody List<repair_feeImpl> repair_fees);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_repair/repair_fees/{id}")
    public repair_feeImpl update(@PathVariable("id") Integer id,@RequestBody repair_feeImpl repair_fee);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_repair/repair_fees/removebatch")
    public repair_feeImpl removeBatch(@RequestBody List<repair_feeImpl> repair_fees);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_repair/repair_fees/fetchdefault")
    public Page<repair_feeImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_repair/repair_fees/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_repair/repair_fees/select")
    public Page<repair_feeImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_repair/repair_fees/{id}/getdraft")
    public repair_feeImpl getDraft(@PathVariable("id") Integer id,@RequestBody repair_feeImpl repair_fee);



}
