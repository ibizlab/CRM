package cn.ibizlab.odoo.client.odoo_repair.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Irepair_order_make_invoice;
import cn.ibizlab.odoo.client.odoo_repair.model.repair_order_make_invoiceImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[repair_order_make_invoice] 服务对象接口
 */
public interface repair_order_make_invoiceFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_repair/repair_order_make_invoices/{id}")
    public repair_order_make_invoiceImpl update(@PathVariable("id") Integer id,@RequestBody repair_order_make_invoiceImpl repair_order_make_invoice);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_repair/repair_order_make_invoices")
    public repair_order_make_invoiceImpl create(@RequestBody repair_order_make_invoiceImpl repair_order_make_invoice);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_repair/repair_order_make_invoices/{id}")
    public repair_order_make_invoiceImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_repair/repair_order_make_invoices/updatebatch")
    public repair_order_make_invoiceImpl updateBatch(@RequestBody List<repair_order_make_invoiceImpl> repair_order_make_invoices);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_repair/repair_order_make_invoices/createbatch")
    public repair_order_make_invoiceImpl createBatch(@RequestBody List<repair_order_make_invoiceImpl> repair_order_make_invoices);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_repair/repair_order_make_invoices/removebatch")
    public repair_order_make_invoiceImpl removeBatch(@RequestBody List<repair_order_make_invoiceImpl> repair_order_make_invoices);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_repair/repair_order_make_invoices/fetchdefault")
    public Page<repair_order_make_invoiceImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_repair/repair_order_make_invoices/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_repair/repair_order_make_invoices/select")
    public Page<repair_order_make_invoiceImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_repair/repair_order_make_invoices/{id}/getdraft")
    public repair_order_make_invoiceImpl getDraft(@PathVariable("id") Integer id,@RequestBody repair_order_make_invoiceImpl repair_order_make_invoice);



}
