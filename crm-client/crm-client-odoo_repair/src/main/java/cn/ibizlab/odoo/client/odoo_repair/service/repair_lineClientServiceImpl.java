package cn.ibizlab.odoo.client.odoo_repair.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Irepair_line;
import cn.ibizlab.odoo.client.odoo_repair.config.odoo_repairClientProperties;
import cn.ibizlab.odoo.core.client.service.Irepair_lineClientService;
import cn.ibizlab.odoo.client.odoo_repair.model.repair_lineImpl;
import cn.ibizlab.odoo.client.odoo_repair.feign.repair_lineFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[repair_line] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class repair_lineClientServiceImpl implements Irepair_lineClientService {

    repair_lineFeignClient repair_lineFeignClient;

    @Autowired
    public repair_lineClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_repairClientProperties odoo_repairClientProperties) {
        if (odoo_repairClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.repair_lineFeignClient = nameBuilder.target(repair_lineFeignClient.class,"http://"+odoo_repairClientProperties.getServiceId()+"/") ;
		}else if (odoo_repairClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.repair_lineFeignClient = nameBuilder.target(repair_lineFeignClient.class,odoo_repairClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Irepair_line createModel() {
		return new repair_lineImpl();
	}


    public void updateBatch(List<Irepair_line> repair_lines){
        if(repair_lines!=null){
            List<repair_lineImpl> list = new ArrayList<repair_lineImpl>();
            for(Irepair_line irepair_line :repair_lines){
                list.add((repair_lineImpl)irepair_line) ;
            }
            repair_lineFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Irepair_line> repair_lines){
        if(repair_lines!=null){
            List<repair_lineImpl> list = new ArrayList<repair_lineImpl>();
            for(Irepair_line irepair_line :repair_lines){
                list.add((repair_lineImpl)irepair_line) ;
            }
            repair_lineFeignClient.createBatch(list) ;
        }
    }


    public Page<Irepair_line> fetchDefault(SearchContext context){
        Page<repair_lineImpl> page = this.repair_lineFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Irepair_line repair_line){
        Irepair_line clientModel = repair_lineFeignClient.get(repair_line.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), repair_line.getClass(), false);
        copier.copy(clientModel, repair_line, null);
    }


    public void update(Irepair_line repair_line){
        Irepair_line clientModel = repair_lineFeignClient.update(repair_line.getId(),(repair_lineImpl)repair_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), repair_line.getClass(), false);
        copier.copy(clientModel, repair_line, null);
    }


    public void removeBatch(List<Irepair_line> repair_lines){
        if(repair_lines!=null){
            List<repair_lineImpl> list = new ArrayList<repair_lineImpl>();
            for(Irepair_line irepair_line :repair_lines){
                list.add((repair_lineImpl)irepair_line) ;
            }
            repair_lineFeignClient.removeBatch(list) ;
        }
    }


    public void create(Irepair_line repair_line){
        Irepair_line clientModel = repair_lineFeignClient.create((repair_lineImpl)repair_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), repair_line.getClass(), false);
        copier.copy(clientModel, repair_line, null);
    }


    public void remove(Irepair_line repair_line){
        repair_lineFeignClient.remove(repair_line.getId()) ;
    }


    public Page<Irepair_line> select(SearchContext context){
        return null ;
    }


    public void getDraft(Irepair_line repair_line){
        Irepair_line clientModel = repair_lineFeignClient.getDraft(repair_line.getId(),(repair_lineImpl)repair_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), repair_line.getClass(), false);
        copier.copy(clientModel, repair_line, null);
    }



}

