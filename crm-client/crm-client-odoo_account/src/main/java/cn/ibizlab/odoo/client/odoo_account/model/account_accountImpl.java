package cn.ibizlab.odoo.client.odoo_account.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iaccount_account;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[account_account] 对象
 */
public class account_accountImpl implements Iaccount_account,Serializable{

    /**
     * 代码
     */
    public String code;

    @JsonIgnore
    public boolean codeDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 科目币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 科目币种
     */
    public String currency_id_text;

    @JsonIgnore
    public boolean currency_id_textDirtyFlag;
    
    /**
     * 废弃
     */
    public String deprecated;

    @JsonIgnore
    public boolean deprecatedDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 组
     */
    public Integer group_id;

    @JsonIgnore
    public boolean group_idDirtyFlag;
    
    /**
     * 组
     */
    public String group_id_text;

    @JsonIgnore
    public boolean group_id_textDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 内部群组
     */
    public String internal_group;

    @JsonIgnore
    public boolean internal_groupDirtyFlag;
    
    /**
     * 内部类型
     */
    public String internal_type;

    @JsonIgnore
    public boolean internal_typeDirtyFlag;
    
    /**
     * 最近的发票和付款匹配时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp last_time_entries_checked;

    @JsonIgnore
    public boolean last_time_entries_checkedDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 内部备注
     */
    public String note;

    @JsonIgnore
    public boolean noteDirtyFlag;
    
    /**
     * 期初贷方
     */
    public Double opening_credit;

    @JsonIgnore
    public boolean opening_creditDirtyFlag;
    
    /**
     * 期初借方
     */
    public Double opening_debit;

    @JsonIgnore
    public boolean opening_debitDirtyFlag;
    
    /**
     * 允许核销
     */
    public String reconcile;

    @JsonIgnore
    public boolean reconcileDirtyFlag;
    
    /**
     * 标签
     */
    public String tag_ids;

    @JsonIgnore
    public boolean tag_idsDirtyFlag;
    
    /**
     * 默认税
     */
    public String tax_ids;

    @JsonIgnore
    public boolean tax_idsDirtyFlag;
    
    /**
     * 类型
     */
    public Integer user_type_id;

    @JsonIgnore
    public boolean user_type_idDirtyFlag;
    
    /**
     * 类型
     */
    public String user_type_id_text;

    @JsonIgnore
    public boolean user_type_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [代码]
     */
    @JsonProperty("code")
    public String getCode(){
        return this.code ;
    }

    /**
     * 设置 [代码]
     */
    @JsonProperty("code")
    public void setCode(String  code){
        this.code = code ;
        this.codeDirtyFlag = true ;
    }

     /**
     * 获取 [代码]脏标记
     */
    @JsonIgnore
    public boolean getCodeDirtyFlag(){
        return this.codeDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [科目币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [科目币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [科目币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [科目币种]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return this.currency_id_text ;
    }

    /**
     * 设置 [科目币种]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [科目币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return this.currency_id_textDirtyFlag ;
    }   

    /**
     * 获取 [废弃]
     */
    @JsonProperty("deprecated")
    public String getDeprecated(){
        return this.deprecated ;
    }

    /**
     * 设置 [废弃]
     */
    @JsonProperty("deprecated")
    public void setDeprecated(String  deprecated){
        this.deprecated = deprecated ;
        this.deprecatedDirtyFlag = true ;
    }

     /**
     * 获取 [废弃]脏标记
     */
    @JsonIgnore
    public boolean getDeprecatedDirtyFlag(){
        return this.deprecatedDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [组]
     */
    @JsonProperty("group_id")
    public Integer getGroup_id(){
        return this.group_id ;
    }

    /**
     * 设置 [组]
     */
    @JsonProperty("group_id")
    public void setGroup_id(Integer  group_id){
        this.group_id = group_id ;
        this.group_idDirtyFlag = true ;
    }

     /**
     * 获取 [组]脏标记
     */
    @JsonIgnore
    public boolean getGroup_idDirtyFlag(){
        return this.group_idDirtyFlag ;
    }   

    /**
     * 获取 [组]
     */
    @JsonProperty("group_id_text")
    public String getGroup_id_text(){
        return this.group_id_text ;
    }

    /**
     * 设置 [组]
     */
    @JsonProperty("group_id_text")
    public void setGroup_id_text(String  group_id_text){
        this.group_id_text = group_id_text ;
        this.group_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [组]脏标记
     */
    @JsonIgnore
    public boolean getGroup_id_textDirtyFlag(){
        return this.group_id_textDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [内部群组]
     */
    @JsonProperty("internal_group")
    public String getInternal_group(){
        return this.internal_group ;
    }

    /**
     * 设置 [内部群组]
     */
    @JsonProperty("internal_group")
    public void setInternal_group(String  internal_group){
        this.internal_group = internal_group ;
        this.internal_groupDirtyFlag = true ;
    }

     /**
     * 获取 [内部群组]脏标记
     */
    @JsonIgnore
    public boolean getInternal_groupDirtyFlag(){
        return this.internal_groupDirtyFlag ;
    }   

    /**
     * 获取 [内部类型]
     */
    @JsonProperty("internal_type")
    public String getInternal_type(){
        return this.internal_type ;
    }

    /**
     * 设置 [内部类型]
     */
    @JsonProperty("internal_type")
    public void setInternal_type(String  internal_type){
        this.internal_type = internal_type ;
        this.internal_typeDirtyFlag = true ;
    }

     /**
     * 获取 [内部类型]脏标记
     */
    @JsonIgnore
    public boolean getInternal_typeDirtyFlag(){
        return this.internal_typeDirtyFlag ;
    }   

    /**
     * 获取 [最近的发票和付款匹配时间]
     */
    @JsonProperty("last_time_entries_checked")
    public Timestamp getLast_time_entries_checked(){
        return this.last_time_entries_checked ;
    }

    /**
     * 设置 [最近的发票和付款匹配时间]
     */
    @JsonProperty("last_time_entries_checked")
    public void setLast_time_entries_checked(Timestamp  last_time_entries_checked){
        this.last_time_entries_checked = last_time_entries_checked ;
        this.last_time_entries_checkedDirtyFlag = true ;
    }

     /**
     * 获取 [最近的发票和付款匹配时间]脏标记
     */
    @JsonIgnore
    public boolean getLast_time_entries_checkedDirtyFlag(){
        return this.last_time_entries_checkedDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [内部备注]
     */
    @JsonProperty("note")
    public String getNote(){
        return this.note ;
    }

    /**
     * 设置 [内部备注]
     */
    @JsonProperty("note")
    public void setNote(String  note){
        this.note = note ;
        this.noteDirtyFlag = true ;
    }

     /**
     * 获取 [内部备注]脏标记
     */
    @JsonIgnore
    public boolean getNoteDirtyFlag(){
        return this.noteDirtyFlag ;
    }   

    /**
     * 获取 [期初贷方]
     */
    @JsonProperty("opening_credit")
    public Double getOpening_credit(){
        return this.opening_credit ;
    }

    /**
     * 设置 [期初贷方]
     */
    @JsonProperty("opening_credit")
    public void setOpening_credit(Double  opening_credit){
        this.opening_credit = opening_credit ;
        this.opening_creditDirtyFlag = true ;
    }

     /**
     * 获取 [期初贷方]脏标记
     */
    @JsonIgnore
    public boolean getOpening_creditDirtyFlag(){
        return this.opening_creditDirtyFlag ;
    }   

    /**
     * 获取 [期初借方]
     */
    @JsonProperty("opening_debit")
    public Double getOpening_debit(){
        return this.opening_debit ;
    }

    /**
     * 设置 [期初借方]
     */
    @JsonProperty("opening_debit")
    public void setOpening_debit(Double  opening_debit){
        this.opening_debit = opening_debit ;
        this.opening_debitDirtyFlag = true ;
    }

     /**
     * 获取 [期初借方]脏标记
     */
    @JsonIgnore
    public boolean getOpening_debitDirtyFlag(){
        return this.opening_debitDirtyFlag ;
    }   

    /**
     * 获取 [允许核销]
     */
    @JsonProperty("reconcile")
    public String getReconcile(){
        return this.reconcile ;
    }

    /**
     * 设置 [允许核销]
     */
    @JsonProperty("reconcile")
    public void setReconcile(String  reconcile){
        this.reconcile = reconcile ;
        this.reconcileDirtyFlag = true ;
    }

     /**
     * 获取 [允许核销]脏标记
     */
    @JsonIgnore
    public boolean getReconcileDirtyFlag(){
        return this.reconcileDirtyFlag ;
    }   

    /**
     * 获取 [标签]
     */
    @JsonProperty("tag_ids")
    public String getTag_ids(){
        return this.tag_ids ;
    }

    /**
     * 设置 [标签]
     */
    @JsonProperty("tag_ids")
    public void setTag_ids(String  tag_ids){
        this.tag_ids = tag_ids ;
        this.tag_idsDirtyFlag = true ;
    }

     /**
     * 获取 [标签]脏标记
     */
    @JsonIgnore
    public boolean getTag_idsDirtyFlag(){
        return this.tag_idsDirtyFlag ;
    }   

    /**
     * 获取 [默认税]
     */
    @JsonProperty("tax_ids")
    public String getTax_ids(){
        return this.tax_ids ;
    }

    /**
     * 设置 [默认税]
     */
    @JsonProperty("tax_ids")
    public void setTax_ids(String  tax_ids){
        this.tax_ids = tax_ids ;
        this.tax_idsDirtyFlag = true ;
    }

     /**
     * 获取 [默认税]脏标记
     */
    @JsonIgnore
    public boolean getTax_idsDirtyFlag(){
        return this.tax_idsDirtyFlag ;
    }   

    /**
     * 获取 [类型]
     */
    @JsonProperty("user_type_id")
    public Integer getUser_type_id(){
        return this.user_type_id ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("user_type_id")
    public void setUser_type_id(Integer  user_type_id){
        this.user_type_id = user_type_id ;
        this.user_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getUser_type_idDirtyFlag(){
        return this.user_type_idDirtyFlag ;
    }   

    /**
     * 获取 [类型]
     */
    @JsonProperty("user_type_id_text")
    public String getUser_type_id_text(){
        return this.user_type_id_text ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("user_type_id_text")
    public void setUser_type_id_text(String  user_type_id_text){
        this.user_type_id_text = user_type_id_text ;
        this.user_type_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getUser_type_id_textDirtyFlag(){
        return this.user_type_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
