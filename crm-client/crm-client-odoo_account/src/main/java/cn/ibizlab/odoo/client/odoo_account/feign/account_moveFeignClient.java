package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_move;
import cn.ibizlab.odoo.client.odoo_account.model.account_moveImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_move] 服务对象接口
 */
public interface account_moveFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_moves/fetchdefault")
    public Page<account_moveImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_moves/updatebatch")
    public account_moveImpl updateBatch(@RequestBody List<account_moveImpl> account_moves);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_moves/{id}")
    public account_moveImpl update(@PathVariable("id") Integer id,@RequestBody account_moveImpl account_move);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_moves/{id}")
    public account_moveImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_moves")
    public account_moveImpl create(@RequestBody account_moveImpl account_move);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_moves/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_moves/removebatch")
    public account_moveImpl removeBatch(@RequestBody List<account_moveImpl> account_moves);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_moves/createbatch")
    public account_moveImpl createBatch(@RequestBody List<account_moveImpl> account_moves);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_moves/select")
    public Page<account_moveImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_moves/{id}/getdraft")
    public account_moveImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_moveImpl account_move);



}
