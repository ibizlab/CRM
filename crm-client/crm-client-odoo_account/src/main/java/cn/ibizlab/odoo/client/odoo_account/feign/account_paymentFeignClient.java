package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_payment;
import cn.ibizlab.odoo.client.odoo_account.model.account_paymentImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_payment] 服务对象接口
 */
public interface account_paymentFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_payments/updatebatch")
    public account_paymentImpl updateBatch(@RequestBody List<account_paymentImpl> account_payments);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_payments")
    public account_paymentImpl create(@RequestBody account_paymentImpl account_payment);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_payments/{id}")
    public account_paymentImpl update(@PathVariable("id") Integer id,@RequestBody account_paymentImpl account_payment);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_payments/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_payments/createbatch")
    public account_paymentImpl createBatch(@RequestBody List<account_paymentImpl> account_payments);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_payments/removebatch")
    public account_paymentImpl removeBatch(@RequestBody List<account_paymentImpl> account_payments);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_payments/fetchdefault")
    public Page<account_paymentImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_payments/{id}")
    public account_paymentImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_payments/select")
    public Page<account_paymentImpl> select();


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_payments/{id}/checkkey")
    public account_paymentImpl checkKey(@PathVariable("id") Integer id,@RequestBody account_paymentImpl account_payment);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_payments/{id}/getdraft")
    public account_paymentImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_paymentImpl account_payment);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_payments/{id}/save")
    public account_paymentImpl save(@PathVariable("id") Integer id,@RequestBody account_paymentImpl account_payment);



}
