package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_payment_term_line;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_payment_term_lineClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_payment_term_lineImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_payment_term_lineFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_payment_term_line] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_payment_term_lineClientServiceImpl implements Iaccount_payment_term_lineClientService {

    account_payment_term_lineFeignClient account_payment_term_lineFeignClient;

    @Autowired
    public account_payment_term_lineClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_payment_term_lineFeignClient = nameBuilder.target(account_payment_term_lineFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_payment_term_lineFeignClient = nameBuilder.target(account_payment_term_lineFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_payment_term_line createModel() {
		return new account_payment_term_lineImpl();
	}


    public void createBatch(List<Iaccount_payment_term_line> account_payment_term_lines){
        if(account_payment_term_lines!=null){
            List<account_payment_term_lineImpl> list = new ArrayList<account_payment_term_lineImpl>();
            for(Iaccount_payment_term_line iaccount_payment_term_line :account_payment_term_lines){
                list.add((account_payment_term_lineImpl)iaccount_payment_term_line) ;
            }
            account_payment_term_lineFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Iaccount_payment_term_line> account_payment_term_lines){
        if(account_payment_term_lines!=null){
            List<account_payment_term_lineImpl> list = new ArrayList<account_payment_term_lineImpl>();
            for(Iaccount_payment_term_line iaccount_payment_term_line :account_payment_term_lines){
                list.add((account_payment_term_lineImpl)iaccount_payment_term_line) ;
            }
            account_payment_term_lineFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Iaccount_payment_term_line> account_payment_term_lines){
        if(account_payment_term_lines!=null){
            List<account_payment_term_lineImpl> list = new ArrayList<account_payment_term_lineImpl>();
            for(Iaccount_payment_term_line iaccount_payment_term_line :account_payment_term_lines){
                list.add((account_payment_term_lineImpl)iaccount_payment_term_line) ;
            }
            account_payment_term_lineFeignClient.updateBatch(list) ;
        }
    }


    public void get(Iaccount_payment_term_line account_payment_term_line){
        Iaccount_payment_term_line clientModel = account_payment_term_lineFeignClient.get(account_payment_term_line.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_payment_term_line.getClass(), false);
        copier.copy(clientModel, account_payment_term_line, null);
    }


    public Page<Iaccount_payment_term_line> fetchDefault(SearchContext context){
        Page<account_payment_term_lineImpl> page = this.account_payment_term_lineFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Iaccount_payment_term_line account_payment_term_line){
        Iaccount_payment_term_line clientModel = account_payment_term_lineFeignClient.create((account_payment_term_lineImpl)account_payment_term_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_payment_term_line.getClass(), false);
        copier.copy(clientModel, account_payment_term_line, null);
    }


    public void update(Iaccount_payment_term_line account_payment_term_line){
        Iaccount_payment_term_line clientModel = account_payment_term_lineFeignClient.update(account_payment_term_line.getId(),(account_payment_term_lineImpl)account_payment_term_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_payment_term_line.getClass(), false);
        copier.copy(clientModel, account_payment_term_line, null);
    }


    public void remove(Iaccount_payment_term_line account_payment_term_line){
        account_payment_term_lineFeignClient.remove(account_payment_term_line.getId()) ;
    }


    public Page<Iaccount_payment_term_line> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_payment_term_line account_payment_term_line){
        Iaccount_payment_term_line clientModel = account_payment_term_lineFeignClient.getDraft(account_payment_term_line.getId(),(account_payment_term_lineImpl)account_payment_term_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_payment_term_line.getClass(), false);
        copier.copy(clientModel, account_payment_term_line, null);
    }



}

