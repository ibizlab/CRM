package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_print_journal;
import cn.ibizlab.odoo.client.odoo_account.model.account_print_journalImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_print_journal] 服务对象接口
 */
public interface account_print_journalFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_print_journals")
    public account_print_journalImpl create(@RequestBody account_print_journalImpl account_print_journal);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_print_journals/createbatch")
    public account_print_journalImpl createBatch(@RequestBody List<account_print_journalImpl> account_print_journals);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_print_journals/{id}")
    public account_print_journalImpl update(@PathVariable("id") Integer id,@RequestBody account_print_journalImpl account_print_journal);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_print_journals/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_print_journals/fetchdefault")
    public Page<account_print_journalImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_print_journals/removebatch")
    public account_print_journalImpl removeBatch(@RequestBody List<account_print_journalImpl> account_print_journals);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_print_journals/{id}")
    public account_print_journalImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_print_journals/updatebatch")
    public account_print_journalImpl updateBatch(@RequestBody List<account_print_journalImpl> account_print_journals);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_print_journals/select")
    public Page<account_print_journalImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_print_journals/{id}/getdraft")
    public account_print_journalImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_print_journalImpl account_print_journal);



}
