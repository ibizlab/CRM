package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_account_type;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_account_typeClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_account_typeImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_account_typeFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_account_type] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_account_typeClientServiceImpl implements Iaccount_account_typeClientService {

    account_account_typeFeignClient account_account_typeFeignClient;

    @Autowired
    public account_account_typeClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_account_typeFeignClient = nameBuilder.target(account_account_typeFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_account_typeFeignClient = nameBuilder.target(account_account_typeFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_account_type createModel() {
		return new account_account_typeImpl();
	}


    public void createBatch(List<Iaccount_account_type> account_account_types){
        if(account_account_types!=null){
            List<account_account_typeImpl> list = new ArrayList<account_account_typeImpl>();
            for(Iaccount_account_type iaccount_account_type :account_account_types){
                list.add((account_account_typeImpl)iaccount_account_type) ;
            }
            account_account_typeFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Iaccount_account_type> account_account_types){
        if(account_account_types!=null){
            List<account_account_typeImpl> list = new ArrayList<account_account_typeImpl>();
            for(Iaccount_account_type iaccount_account_type :account_account_types){
                list.add((account_account_typeImpl)iaccount_account_type) ;
            }
            account_account_typeFeignClient.removeBatch(list) ;
        }
    }


    public void create(Iaccount_account_type account_account_type){
        Iaccount_account_type clientModel = account_account_typeFeignClient.create((account_account_typeImpl)account_account_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_account_type.getClass(), false);
        copier.copy(clientModel, account_account_type, null);
    }


    public Page<Iaccount_account_type> fetchDefault(SearchContext context){
        Page<account_account_typeImpl> page = this.account_account_typeFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Iaccount_account_type> account_account_types){
        if(account_account_types!=null){
            List<account_account_typeImpl> list = new ArrayList<account_account_typeImpl>();
            for(Iaccount_account_type iaccount_account_type :account_account_types){
                list.add((account_account_typeImpl)iaccount_account_type) ;
            }
            account_account_typeFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Iaccount_account_type account_account_type){
        account_account_typeFeignClient.remove(account_account_type.getId()) ;
    }


    public void update(Iaccount_account_type account_account_type){
        Iaccount_account_type clientModel = account_account_typeFeignClient.update(account_account_type.getId(),(account_account_typeImpl)account_account_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_account_type.getClass(), false);
        copier.copy(clientModel, account_account_type, null);
    }


    public void get(Iaccount_account_type account_account_type){
        Iaccount_account_type clientModel = account_account_typeFeignClient.get(account_account_type.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_account_type.getClass(), false);
        copier.copy(clientModel, account_account_type, null);
    }


    public Page<Iaccount_account_type> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_account_type account_account_type){
        Iaccount_account_type clientModel = account_account_typeFeignClient.getDraft(account_account_type.getId(),(account_account_typeImpl)account_account_type) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_account_type.getClass(), false);
        copier.copy(clientModel, account_account_type, null);
    }



}

