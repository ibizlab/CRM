package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_chart_template;
import cn.ibizlab.odoo.client.odoo_account.model.account_chart_templateImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_chart_template] 服务对象接口
 */
public interface account_chart_templateFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_chart_templates/updatebatch")
    public account_chart_templateImpl updateBatch(@RequestBody List<account_chart_templateImpl> account_chart_templates);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_chart_templates/fetchdefault")
    public Page<account_chart_templateImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_chart_templates")
    public account_chart_templateImpl create(@RequestBody account_chart_templateImpl account_chart_template);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_chart_templates/{id}")
    public account_chart_templateImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_chart_templates/removebatch")
    public account_chart_templateImpl removeBatch(@RequestBody List<account_chart_templateImpl> account_chart_templates);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_chart_templates/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_chart_templates/{id}")
    public account_chart_templateImpl update(@PathVariable("id") Integer id,@RequestBody account_chart_templateImpl account_chart_template);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_chart_templates/createbatch")
    public account_chart_templateImpl createBatch(@RequestBody List<account_chart_templateImpl> account_chart_templates);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_chart_templates/select")
    public Page<account_chart_templateImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_chart_templates/{id}/getdraft")
    public account_chart_templateImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_chart_templateImpl account_chart_template);



}
