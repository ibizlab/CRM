package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_bank_statement_closebalance;
import cn.ibizlab.odoo.client.odoo_account.model.account_bank_statement_closebalanceImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_bank_statement_closebalance] 服务对象接口
 */
public interface account_bank_statement_closebalanceFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_bank_statement_closebalances/fetchdefault")
    public Page<account_bank_statement_closebalanceImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_bank_statement_closebalances/{id}")
    public account_bank_statement_closebalanceImpl update(@PathVariable("id") Integer id,@RequestBody account_bank_statement_closebalanceImpl account_bank_statement_closebalance);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_bank_statement_closebalances/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_bank_statement_closebalances/{id}")
    public account_bank_statement_closebalanceImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_bank_statement_closebalances")
    public account_bank_statement_closebalanceImpl create(@RequestBody account_bank_statement_closebalanceImpl account_bank_statement_closebalance);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_bank_statement_closebalances/createbatch")
    public account_bank_statement_closebalanceImpl createBatch(@RequestBody List<account_bank_statement_closebalanceImpl> account_bank_statement_closebalances);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_bank_statement_closebalances/removebatch")
    public account_bank_statement_closebalanceImpl removeBatch(@RequestBody List<account_bank_statement_closebalanceImpl> account_bank_statement_closebalances);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_bank_statement_closebalances/updatebatch")
    public account_bank_statement_closebalanceImpl updateBatch(@RequestBody List<account_bank_statement_closebalanceImpl> account_bank_statement_closebalances);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_bank_statement_closebalances/select")
    public Page<account_bank_statement_closebalanceImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_bank_statement_closebalances/{id}/getdraft")
    public account_bank_statement_closebalanceImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_bank_statement_closebalanceImpl account_bank_statement_closebalance);



}
