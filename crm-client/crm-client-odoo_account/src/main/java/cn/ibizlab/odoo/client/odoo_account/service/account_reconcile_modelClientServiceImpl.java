package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_reconcile_model;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_reconcile_modelClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_reconcile_modelImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_reconcile_modelFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_reconcile_model] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_reconcile_modelClientServiceImpl implements Iaccount_reconcile_modelClientService {

    account_reconcile_modelFeignClient account_reconcile_modelFeignClient;

    @Autowired
    public account_reconcile_modelClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_reconcile_modelFeignClient = nameBuilder.target(account_reconcile_modelFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_reconcile_modelFeignClient = nameBuilder.target(account_reconcile_modelFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_reconcile_model createModel() {
		return new account_reconcile_modelImpl();
	}


    public void remove(Iaccount_reconcile_model account_reconcile_model){
        account_reconcile_modelFeignClient.remove(account_reconcile_model.getId()) ;
    }


    public void create(Iaccount_reconcile_model account_reconcile_model){
        Iaccount_reconcile_model clientModel = account_reconcile_modelFeignClient.create((account_reconcile_modelImpl)account_reconcile_model) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_reconcile_model.getClass(), false);
        copier.copy(clientModel, account_reconcile_model, null);
    }


    public void update(Iaccount_reconcile_model account_reconcile_model){
        Iaccount_reconcile_model clientModel = account_reconcile_modelFeignClient.update(account_reconcile_model.getId(),(account_reconcile_modelImpl)account_reconcile_model) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_reconcile_model.getClass(), false);
        copier.copy(clientModel, account_reconcile_model, null);
    }


    public void updateBatch(List<Iaccount_reconcile_model> account_reconcile_models){
        if(account_reconcile_models!=null){
            List<account_reconcile_modelImpl> list = new ArrayList<account_reconcile_modelImpl>();
            for(Iaccount_reconcile_model iaccount_reconcile_model :account_reconcile_models){
                list.add((account_reconcile_modelImpl)iaccount_reconcile_model) ;
            }
            account_reconcile_modelFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Iaccount_reconcile_model> account_reconcile_models){
        if(account_reconcile_models!=null){
            List<account_reconcile_modelImpl> list = new ArrayList<account_reconcile_modelImpl>();
            for(Iaccount_reconcile_model iaccount_reconcile_model :account_reconcile_models){
                list.add((account_reconcile_modelImpl)iaccount_reconcile_model) ;
            }
            account_reconcile_modelFeignClient.createBatch(list) ;
        }
    }


    public void get(Iaccount_reconcile_model account_reconcile_model){
        Iaccount_reconcile_model clientModel = account_reconcile_modelFeignClient.get(account_reconcile_model.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_reconcile_model.getClass(), false);
        copier.copy(clientModel, account_reconcile_model, null);
    }


    public void removeBatch(List<Iaccount_reconcile_model> account_reconcile_models){
        if(account_reconcile_models!=null){
            List<account_reconcile_modelImpl> list = new ArrayList<account_reconcile_modelImpl>();
            for(Iaccount_reconcile_model iaccount_reconcile_model :account_reconcile_models){
                list.add((account_reconcile_modelImpl)iaccount_reconcile_model) ;
            }
            account_reconcile_modelFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iaccount_reconcile_model> fetchDefault(SearchContext context){
        Page<account_reconcile_modelImpl> page = this.account_reconcile_modelFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Iaccount_reconcile_model> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_reconcile_model account_reconcile_model){
        Iaccount_reconcile_model clientModel = account_reconcile_modelFeignClient.getDraft(account_reconcile_model.getId(),(account_reconcile_modelImpl)account_reconcile_model) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_reconcile_model.getClass(), false);
        copier.copy(clientModel, account_reconcile_model, null);
    }



}

