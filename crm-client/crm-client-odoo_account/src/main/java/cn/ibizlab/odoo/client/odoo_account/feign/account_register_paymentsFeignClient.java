package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_register_payments;
import cn.ibizlab.odoo.client.odoo_account.model.account_register_paymentsImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_register_payments] 服务对象接口
 */
public interface account_register_paymentsFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_register_payments")
    public account_register_paymentsImpl create(@RequestBody account_register_paymentsImpl account_register_payments);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_register_payments/createbatch")
    public account_register_paymentsImpl createBatch(@RequestBody List<account_register_paymentsImpl> account_register_payments);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_register_payments/removebatch")
    public account_register_paymentsImpl removeBatch(@RequestBody List<account_register_paymentsImpl> account_register_payments);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_register_payments/fetchdefault")
    public Page<account_register_paymentsImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_register_payments/updatebatch")
    public account_register_paymentsImpl updateBatch(@RequestBody List<account_register_paymentsImpl> account_register_payments);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_register_payments/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_register_payments/{id}")
    public account_register_paymentsImpl update(@PathVariable("id") Integer id,@RequestBody account_register_paymentsImpl account_register_payments);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_register_payments/{id}")
    public account_register_paymentsImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_register_payments/select")
    public Page<account_register_paymentsImpl> select();


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_register_payments/{id}/checkkey")
    public account_register_paymentsImpl checkKey(@PathVariable("id") Integer id,@RequestBody account_register_paymentsImpl account_register_payments);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_register_payments/{id}/save")
    public account_register_paymentsImpl save(@PathVariable("id") Integer id,@RequestBody account_register_paymentsImpl account_register_payments);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_register_payments/{id}/getdraft")
    public account_register_paymentsImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_register_paymentsImpl account_register_payments);



}
