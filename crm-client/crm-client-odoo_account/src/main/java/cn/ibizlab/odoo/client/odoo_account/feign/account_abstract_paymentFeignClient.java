package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_abstract_payment;
import cn.ibizlab.odoo.client.odoo_account.model.account_abstract_paymentImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_abstract_payment] 服务对象接口
 */
public interface account_abstract_paymentFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_abstract_payments/fetchdefault")
    public Page<account_abstract_paymentImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_abstract_payments")
    public account_abstract_paymentImpl create(@RequestBody account_abstract_paymentImpl account_abstract_payment);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_abstract_payments/removebatch")
    public account_abstract_paymentImpl removeBatch(@RequestBody List<account_abstract_paymentImpl> account_abstract_payments);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_abstract_payments/{id}")
    public account_abstract_paymentImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_abstract_payments/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_abstract_payments/updatebatch")
    public account_abstract_paymentImpl updateBatch(@RequestBody List<account_abstract_paymentImpl> account_abstract_payments);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_abstract_payments/{id}")
    public account_abstract_paymentImpl update(@PathVariable("id") Integer id,@RequestBody account_abstract_paymentImpl account_abstract_payment);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_abstract_payments/createbatch")
    public account_abstract_paymentImpl createBatch(@RequestBody List<account_abstract_paymentImpl> account_abstract_payments);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_abstract_payments/select")
    public Page<account_abstract_paymentImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_abstract_payments/{id}/getdraft")
    public account_abstract_paymentImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_abstract_paymentImpl account_abstract_payment);



}
