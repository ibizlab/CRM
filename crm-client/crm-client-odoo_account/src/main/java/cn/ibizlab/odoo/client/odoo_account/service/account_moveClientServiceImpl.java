package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_move;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_moveClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_moveImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_moveFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_move] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_moveClientServiceImpl implements Iaccount_moveClientService {

    account_moveFeignClient account_moveFeignClient;

    @Autowired
    public account_moveClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_moveFeignClient = nameBuilder.target(account_moveFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_moveFeignClient = nameBuilder.target(account_moveFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_move createModel() {
		return new account_moveImpl();
	}


    public Page<Iaccount_move> fetchDefault(SearchContext context){
        Page<account_moveImpl> page = this.account_moveFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Iaccount_move> account_moves){
        if(account_moves!=null){
            List<account_moveImpl> list = new ArrayList<account_moveImpl>();
            for(Iaccount_move iaccount_move :account_moves){
                list.add((account_moveImpl)iaccount_move) ;
            }
            account_moveFeignClient.updateBatch(list) ;
        }
    }


    public void update(Iaccount_move account_move){
        Iaccount_move clientModel = account_moveFeignClient.update(account_move.getId(),(account_moveImpl)account_move) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_move.getClass(), false);
        copier.copy(clientModel, account_move, null);
    }


    public void get(Iaccount_move account_move){
        Iaccount_move clientModel = account_moveFeignClient.get(account_move.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_move.getClass(), false);
        copier.copy(clientModel, account_move, null);
    }


    public void create(Iaccount_move account_move){
        Iaccount_move clientModel = account_moveFeignClient.create((account_moveImpl)account_move) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_move.getClass(), false);
        copier.copy(clientModel, account_move, null);
    }


    public void remove(Iaccount_move account_move){
        account_moveFeignClient.remove(account_move.getId()) ;
    }


    public void removeBatch(List<Iaccount_move> account_moves){
        if(account_moves!=null){
            List<account_moveImpl> list = new ArrayList<account_moveImpl>();
            for(Iaccount_move iaccount_move :account_moves){
                list.add((account_moveImpl)iaccount_move) ;
            }
            account_moveFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Iaccount_move> account_moves){
        if(account_moves!=null){
            List<account_moveImpl> list = new ArrayList<account_moveImpl>();
            for(Iaccount_move iaccount_move :account_moves){
                list.add((account_moveImpl)iaccount_move) ;
            }
            account_moveFeignClient.createBatch(list) ;
        }
    }


    public Page<Iaccount_move> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_move account_move){
        Iaccount_move clientModel = account_moveFeignClient.getDraft(account_move.getId(),(account_moveImpl)account_move) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_move.getClass(), false);
        copier.copy(clientModel, account_move, null);
    }



}

