package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_fiscal_position_tax;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_fiscal_position_taxClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_fiscal_position_taxImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_fiscal_position_taxFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_fiscal_position_tax] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_fiscal_position_taxClientServiceImpl implements Iaccount_fiscal_position_taxClientService {

    account_fiscal_position_taxFeignClient account_fiscal_position_taxFeignClient;

    @Autowired
    public account_fiscal_position_taxClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_fiscal_position_taxFeignClient = nameBuilder.target(account_fiscal_position_taxFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_fiscal_position_taxFeignClient = nameBuilder.target(account_fiscal_position_taxFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_fiscal_position_tax createModel() {
		return new account_fiscal_position_taxImpl();
	}


    public void removeBatch(List<Iaccount_fiscal_position_tax> account_fiscal_position_taxes){
        if(account_fiscal_position_taxes!=null){
            List<account_fiscal_position_taxImpl> list = new ArrayList<account_fiscal_position_taxImpl>();
            for(Iaccount_fiscal_position_tax iaccount_fiscal_position_tax :account_fiscal_position_taxes){
                list.add((account_fiscal_position_taxImpl)iaccount_fiscal_position_tax) ;
            }
            account_fiscal_position_taxFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Iaccount_fiscal_position_tax> account_fiscal_position_taxes){
        if(account_fiscal_position_taxes!=null){
            List<account_fiscal_position_taxImpl> list = new ArrayList<account_fiscal_position_taxImpl>();
            for(Iaccount_fiscal_position_tax iaccount_fiscal_position_tax :account_fiscal_position_taxes){
                list.add((account_fiscal_position_taxImpl)iaccount_fiscal_position_tax) ;
            }
            account_fiscal_position_taxFeignClient.updateBatch(list) ;
        }
    }


    public void get(Iaccount_fiscal_position_tax account_fiscal_position_tax){
        Iaccount_fiscal_position_tax clientModel = account_fiscal_position_taxFeignClient.get(account_fiscal_position_tax.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_fiscal_position_tax.getClass(), false);
        copier.copy(clientModel, account_fiscal_position_tax, null);
    }


    public void update(Iaccount_fiscal_position_tax account_fiscal_position_tax){
        Iaccount_fiscal_position_tax clientModel = account_fiscal_position_taxFeignClient.update(account_fiscal_position_tax.getId(),(account_fiscal_position_taxImpl)account_fiscal_position_tax) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_fiscal_position_tax.getClass(), false);
        copier.copy(clientModel, account_fiscal_position_tax, null);
    }


    public void createBatch(List<Iaccount_fiscal_position_tax> account_fiscal_position_taxes){
        if(account_fiscal_position_taxes!=null){
            List<account_fiscal_position_taxImpl> list = new ArrayList<account_fiscal_position_taxImpl>();
            for(Iaccount_fiscal_position_tax iaccount_fiscal_position_tax :account_fiscal_position_taxes){
                list.add((account_fiscal_position_taxImpl)iaccount_fiscal_position_tax) ;
            }
            account_fiscal_position_taxFeignClient.createBatch(list) ;
        }
    }


    public void create(Iaccount_fiscal_position_tax account_fiscal_position_tax){
        Iaccount_fiscal_position_tax clientModel = account_fiscal_position_taxFeignClient.create((account_fiscal_position_taxImpl)account_fiscal_position_tax) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_fiscal_position_tax.getClass(), false);
        copier.copy(clientModel, account_fiscal_position_tax, null);
    }


    public void remove(Iaccount_fiscal_position_tax account_fiscal_position_tax){
        account_fiscal_position_taxFeignClient.remove(account_fiscal_position_tax.getId()) ;
    }


    public Page<Iaccount_fiscal_position_tax> fetchDefault(SearchContext context){
        Page<account_fiscal_position_taxImpl> page = this.account_fiscal_position_taxFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Iaccount_fiscal_position_tax> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_fiscal_position_tax account_fiscal_position_tax){
        Iaccount_fiscal_position_tax clientModel = account_fiscal_position_taxFeignClient.getDraft(account_fiscal_position_tax.getId(),(account_fiscal_position_taxImpl)account_fiscal_position_tax) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_fiscal_position_tax.getClass(), false);
        copier.copy(clientModel, account_fiscal_position_tax, null);
    }



}

