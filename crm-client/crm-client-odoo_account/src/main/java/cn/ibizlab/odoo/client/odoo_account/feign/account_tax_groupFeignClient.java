package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_tax_group;
import cn.ibizlab.odoo.client.odoo_account.model.account_tax_groupImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_tax_group] 服务对象接口
 */
public interface account_tax_groupFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_tax_groups/createbatch")
    public account_tax_groupImpl createBatch(@RequestBody List<account_tax_groupImpl> account_tax_groups);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_tax_groups")
    public account_tax_groupImpl create(@RequestBody account_tax_groupImpl account_tax_group);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_tax_groups/fetchdefault")
    public Page<account_tax_groupImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_tax_groups/{id}")
    public account_tax_groupImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_tax_groups/updatebatch")
    public account_tax_groupImpl updateBatch(@RequestBody List<account_tax_groupImpl> account_tax_groups);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_tax_groups/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_tax_groups/{id}")
    public account_tax_groupImpl update(@PathVariable("id") Integer id,@RequestBody account_tax_groupImpl account_tax_group);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_tax_groups/removebatch")
    public account_tax_groupImpl removeBatch(@RequestBody List<account_tax_groupImpl> account_tax_groups);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_tax_groups/select")
    public Page<account_tax_groupImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_tax_groups/{id}/getdraft")
    public account_tax_groupImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_tax_groupImpl account_tax_group);



}
