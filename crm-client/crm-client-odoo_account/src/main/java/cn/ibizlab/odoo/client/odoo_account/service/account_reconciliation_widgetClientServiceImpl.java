package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_reconciliation_widget;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_reconciliation_widgetClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_reconciliation_widgetImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_reconciliation_widgetFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_reconciliation_widget] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_reconciliation_widgetClientServiceImpl implements Iaccount_reconciliation_widgetClientService {

    account_reconciliation_widgetFeignClient account_reconciliation_widgetFeignClient;

    @Autowired
    public account_reconciliation_widgetClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_reconciliation_widgetFeignClient = nameBuilder.target(account_reconciliation_widgetFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_reconciliation_widgetFeignClient = nameBuilder.target(account_reconciliation_widgetFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_reconciliation_widget createModel() {
		return new account_reconciliation_widgetImpl();
	}


    public void removeBatch(List<Iaccount_reconciliation_widget> account_reconciliation_widgets){
        if(account_reconciliation_widgets!=null){
            List<account_reconciliation_widgetImpl> list = new ArrayList<account_reconciliation_widgetImpl>();
            for(Iaccount_reconciliation_widget iaccount_reconciliation_widget :account_reconciliation_widgets){
                list.add((account_reconciliation_widgetImpl)iaccount_reconciliation_widget) ;
            }
            account_reconciliation_widgetFeignClient.removeBatch(list) ;
        }
    }


    public void update(Iaccount_reconciliation_widget account_reconciliation_widget){
        Iaccount_reconciliation_widget clientModel = account_reconciliation_widgetFeignClient.update(account_reconciliation_widget.getId(),(account_reconciliation_widgetImpl)account_reconciliation_widget) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_reconciliation_widget.getClass(), false);
        copier.copy(clientModel, account_reconciliation_widget, null);
    }


    public void createBatch(List<Iaccount_reconciliation_widget> account_reconciliation_widgets){
        if(account_reconciliation_widgets!=null){
            List<account_reconciliation_widgetImpl> list = new ArrayList<account_reconciliation_widgetImpl>();
            for(Iaccount_reconciliation_widget iaccount_reconciliation_widget :account_reconciliation_widgets){
                list.add((account_reconciliation_widgetImpl)iaccount_reconciliation_widget) ;
            }
            account_reconciliation_widgetFeignClient.createBatch(list) ;
        }
    }


    public void get(Iaccount_reconciliation_widget account_reconciliation_widget){
        Iaccount_reconciliation_widget clientModel = account_reconciliation_widgetFeignClient.get(account_reconciliation_widget.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_reconciliation_widget.getClass(), false);
        copier.copy(clientModel, account_reconciliation_widget, null);
    }


    public Page<Iaccount_reconciliation_widget> fetchDefault(SearchContext context){
        Page<account_reconciliation_widgetImpl> page = this.account_reconciliation_widgetFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Iaccount_reconciliation_widget account_reconciliation_widget){
        Iaccount_reconciliation_widget clientModel = account_reconciliation_widgetFeignClient.create((account_reconciliation_widgetImpl)account_reconciliation_widget) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_reconciliation_widget.getClass(), false);
        copier.copy(clientModel, account_reconciliation_widget, null);
    }


    public void remove(Iaccount_reconciliation_widget account_reconciliation_widget){
        account_reconciliation_widgetFeignClient.remove(account_reconciliation_widget.getId()) ;
    }


    public void updateBatch(List<Iaccount_reconciliation_widget> account_reconciliation_widgets){
        if(account_reconciliation_widgets!=null){
            List<account_reconciliation_widgetImpl> list = new ArrayList<account_reconciliation_widgetImpl>();
            for(Iaccount_reconciliation_widget iaccount_reconciliation_widget :account_reconciliation_widgets){
                list.add((account_reconciliation_widgetImpl)iaccount_reconciliation_widget) ;
            }
            account_reconciliation_widgetFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iaccount_reconciliation_widget> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_reconciliation_widget account_reconciliation_widget){
        Iaccount_reconciliation_widget clientModel = account_reconciliation_widgetFeignClient.getDraft(account_reconciliation_widget.getId(),(account_reconciliation_widgetImpl)account_reconciliation_widget) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_reconciliation_widget.getClass(), false);
        copier.copy(clientModel, account_reconciliation_widget, null);
    }



}

