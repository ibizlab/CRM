package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_cash_rounding;
import cn.ibizlab.odoo.client.odoo_account.model.account_cash_roundingImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_cash_rounding] 服务对象接口
 */
public interface account_cash_roundingFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_cash_roundings/{id}")
    public account_cash_roundingImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_cash_roundings/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_cash_roundings/createbatch")
    public account_cash_roundingImpl createBatch(@RequestBody List<account_cash_roundingImpl> account_cash_roundings);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_cash_roundings/removebatch")
    public account_cash_roundingImpl removeBatch(@RequestBody List<account_cash_roundingImpl> account_cash_roundings);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_cash_roundings")
    public account_cash_roundingImpl create(@RequestBody account_cash_roundingImpl account_cash_rounding);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_cash_roundings/fetchdefault")
    public Page<account_cash_roundingImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_cash_roundings/{id}")
    public account_cash_roundingImpl update(@PathVariable("id") Integer id,@RequestBody account_cash_roundingImpl account_cash_rounding);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_cash_roundings/updatebatch")
    public account_cash_roundingImpl updateBatch(@RequestBody List<account_cash_roundingImpl> account_cash_roundings);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_cash_roundings/select")
    public Page<account_cash_roundingImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_cash_roundings/{id}/getdraft")
    public account_cash_roundingImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_cash_roundingImpl account_cash_rounding);



}
