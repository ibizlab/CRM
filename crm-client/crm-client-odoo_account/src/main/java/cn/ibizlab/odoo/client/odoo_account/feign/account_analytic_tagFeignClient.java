package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_analytic_tag;
import cn.ibizlab.odoo.client.odoo_account.model.account_analytic_tagImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_analytic_tag] 服务对象接口
 */
public interface account_analytic_tagFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_analytic_tags/createbatch")
    public account_analytic_tagImpl createBatch(@RequestBody List<account_analytic_tagImpl> account_analytic_tags);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_analytic_tags/fetchdefault")
    public Page<account_analytic_tagImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_analytic_tags/updatebatch")
    public account_analytic_tagImpl updateBatch(@RequestBody List<account_analytic_tagImpl> account_analytic_tags);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_analytic_tags/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_analytic_tags/{id}")
    public account_analytic_tagImpl update(@PathVariable("id") Integer id,@RequestBody account_analytic_tagImpl account_analytic_tag);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_analytic_tags/{id}")
    public account_analytic_tagImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_analytic_tags")
    public account_analytic_tagImpl create(@RequestBody account_analytic_tagImpl account_analytic_tag);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_analytic_tags/removebatch")
    public account_analytic_tagImpl removeBatch(@RequestBody List<account_analytic_tagImpl> account_analytic_tags);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_analytic_tags/select")
    public Page<account_analytic_tagImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_analytic_tags/{id}/getdraft")
    public account_analytic_tagImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_analytic_tagImpl account_analytic_tag);



}
