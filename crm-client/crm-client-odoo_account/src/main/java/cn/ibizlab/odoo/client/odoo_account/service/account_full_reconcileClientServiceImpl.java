package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_full_reconcile;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_full_reconcileClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_full_reconcileImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_full_reconcileFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_full_reconcile] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_full_reconcileClientServiceImpl implements Iaccount_full_reconcileClientService {

    account_full_reconcileFeignClient account_full_reconcileFeignClient;

    @Autowired
    public account_full_reconcileClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_full_reconcileFeignClient = nameBuilder.target(account_full_reconcileFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_full_reconcileFeignClient = nameBuilder.target(account_full_reconcileFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_full_reconcile createModel() {
		return new account_full_reconcileImpl();
	}


    public void create(Iaccount_full_reconcile account_full_reconcile){
        Iaccount_full_reconcile clientModel = account_full_reconcileFeignClient.create((account_full_reconcileImpl)account_full_reconcile) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_full_reconcile.getClass(), false);
        copier.copy(clientModel, account_full_reconcile, null);
    }


    public void get(Iaccount_full_reconcile account_full_reconcile){
        Iaccount_full_reconcile clientModel = account_full_reconcileFeignClient.get(account_full_reconcile.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_full_reconcile.getClass(), false);
        copier.copy(clientModel, account_full_reconcile, null);
    }


    public void removeBatch(List<Iaccount_full_reconcile> account_full_reconciles){
        if(account_full_reconciles!=null){
            List<account_full_reconcileImpl> list = new ArrayList<account_full_reconcileImpl>();
            for(Iaccount_full_reconcile iaccount_full_reconcile :account_full_reconciles){
                list.add((account_full_reconcileImpl)iaccount_full_reconcile) ;
            }
            account_full_reconcileFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Iaccount_full_reconcile account_full_reconcile){
        account_full_reconcileFeignClient.remove(account_full_reconcile.getId()) ;
    }


    public Page<Iaccount_full_reconcile> fetchDefault(SearchContext context){
        Page<account_full_reconcileImpl> page = this.account_full_reconcileFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Iaccount_full_reconcile> account_full_reconciles){
        if(account_full_reconciles!=null){
            List<account_full_reconcileImpl> list = new ArrayList<account_full_reconcileImpl>();
            for(Iaccount_full_reconcile iaccount_full_reconcile :account_full_reconciles){
                list.add((account_full_reconcileImpl)iaccount_full_reconcile) ;
            }
            account_full_reconcileFeignClient.updateBatch(list) ;
        }
    }


    public void update(Iaccount_full_reconcile account_full_reconcile){
        Iaccount_full_reconcile clientModel = account_full_reconcileFeignClient.update(account_full_reconcile.getId(),(account_full_reconcileImpl)account_full_reconcile) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_full_reconcile.getClass(), false);
        copier.copy(clientModel, account_full_reconcile, null);
    }


    public void createBatch(List<Iaccount_full_reconcile> account_full_reconciles){
        if(account_full_reconciles!=null){
            List<account_full_reconcileImpl> list = new ArrayList<account_full_reconcileImpl>();
            for(Iaccount_full_reconcile iaccount_full_reconcile :account_full_reconciles){
                list.add((account_full_reconcileImpl)iaccount_full_reconcile) ;
            }
            account_full_reconcileFeignClient.createBatch(list) ;
        }
    }


    public Page<Iaccount_full_reconcile> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_full_reconcile account_full_reconcile){
        Iaccount_full_reconcile clientModel = account_full_reconcileFeignClient.getDraft(account_full_reconcile.getId(),(account_full_reconcileImpl)account_full_reconcile) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_full_reconcile.getClass(), false);
        copier.copy(clientModel, account_full_reconcile, null);
    }



}

