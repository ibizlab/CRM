package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_payment_term_line;
import cn.ibizlab.odoo.client.odoo_account.model.account_payment_term_lineImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_payment_term_line] 服务对象接口
 */
public interface account_payment_term_lineFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_payment_term_lines/createbatch")
    public account_payment_term_lineImpl createBatch(@RequestBody List<account_payment_term_lineImpl> account_payment_term_lines);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_payment_term_lines/removebatch")
    public account_payment_term_lineImpl removeBatch(@RequestBody List<account_payment_term_lineImpl> account_payment_term_lines);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_payment_term_lines/updatebatch")
    public account_payment_term_lineImpl updateBatch(@RequestBody List<account_payment_term_lineImpl> account_payment_term_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_payment_term_lines/{id}")
    public account_payment_term_lineImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_payment_term_lines/fetchdefault")
    public Page<account_payment_term_lineImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_payment_term_lines")
    public account_payment_term_lineImpl create(@RequestBody account_payment_term_lineImpl account_payment_term_line);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_payment_term_lines/{id}")
    public account_payment_term_lineImpl update(@PathVariable("id") Integer id,@RequestBody account_payment_term_lineImpl account_payment_term_line);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_payment_term_lines/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_payment_term_lines/select")
    public Page<account_payment_term_lineImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_payment_term_lines/{id}/getdraft")
    public account_payment_term_lineImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_payment_term_lineImpl account_payment_term_line);



}
