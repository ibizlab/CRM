package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_chart_template;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_chart_templateClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_chart_templateImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_chart_templateFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_chart_template] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_chart_templateClientServiceImpl implements Iaccount_chart_templateClientService {

    account_chart_templateFeignClient account_chart_templateFeignClient;

    @Autowired
    public account_chart_templateClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_chart_templateFeignClient = nameBuilder.target(account_chart_templateFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_chart_templateFeignClient = nameBuilder.target(account_chart_templateFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_chart_template createModel() {
		return new account_chart_templateImpl();
	}


    public void updateBatch(List<Iaccount_chart_template> account_chart_templates){
        if(account_chart_templates!=null){
            List<account_chart_templateImpl> list = new ArrayList<account_chart_templateImpl>();
            for(Iaccount_chart_template iaccount_chart_template :account_chart_templates){
                list.add((account_chart_templateImpl)iaccount_chart_template) ;
            }
            account_chart_templateFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iaccount_chart_template> fetchDefault(SearchContext context){
        Page<account_chart_templateImpl> page = this.account_chart_templateFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Iaccount_chart_template account_chart_template){
        Iaccount_chart_template clientModel = account_chart_templateFeignClient.create((account_chart_templateImpl)account_chart_template) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_chart_template.getClass(), false);
        copier.copy(clientModel, account_chart_template, null);
    }


    public void get(Iaccount_chart_template account_chart_template){
        Iaccount_chart_template clientModel = account_chart_templateFeignClient.get(account_chart_template.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_chart_template.getClass(), false);
        copier.copy(clientModel, account_chart_template, null);
    }


    public void removeBatch(List<Iaccount_chart_template> account_chart_templates){
        if(account_chart_templates!=null){
            List<account_chart_templateImpl> list = new ArrayList<account_chart_templateImpl>();
            for(Iaccount_chart_template iaccount_chart_template :account_chart_templates){
                list.add((account_chart_templateImpl)iaccount_chart_template) ;
            }
            account_chart_templateFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Iaccount_chart_template account_chart_template){
        account_chart_templateFeignClient.remove(account_chart_template.getId()) ;
    }


    public void update(Iaccount_chart_template account_chart_template){
        Iaccount_chart_template clientModel = account_chart_templateFeignClient.update(account_chart_template.getId(),(account_chart_templateImpl)account_chart_template) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_chart_template.getClass(), false);
        copier.copy(clientModel, account_chart_template, null);
    }


    public void createBatch(List<Iaccount_chart_template> account_chart_templates){
        if(account_chart_templates!=null){
            List<account_chart_templateImpl> list = new ArrayList<account_chart_templateImpl>();
            for(Iaccount_chart_template iaccount_chart_template :account_chart_templates){
                list.add((account_chart_templateImpl)iaccount_chart_template) ;
            }
            account_chart_templateFeignClient.createBatch(list) ;
        }
    }


    public Page<Iaccount_chart_template> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_chart_template account_chart_template){
        Iaccount_chart_template clientModel = account_chart_templateFeignClient.getDraft(account_chart_template.getId(),(account_chart_templateImpl)account_chart_template) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_chart_template.getClass(), false);
        copier.copy(clientModel, account_chart_template, null);
    }



}

