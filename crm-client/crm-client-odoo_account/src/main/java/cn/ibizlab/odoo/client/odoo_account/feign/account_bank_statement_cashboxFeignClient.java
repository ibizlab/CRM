package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_bank_statement_cashbox;
import cn.ibizlab.odoo.client.odoo_account.model.account_bank_statement_cashboxImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_bank_statement_cashbox] 服务对象接口
 */
public interface account_bank_statement_cashboxFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_bank_statement_cashboxes/{id}")
    public account_bank_statement_cashboxImpl update(@PathVariable("id") Integer id,@RequestBody account_bank_statement_cashboxImpl account_bank_statement_cashbox);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_bank_statement_cashboxes/{id}")
    public account_bank_statement_cashboxImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_bank_statement_cashboxes/removebatch")
    public account_bank_statement_cashboxImpl removeBatch(@RequestBody List<account_bank_statement_cashboxImpl> account_bank_statement_cashboxes);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_bank_statement_cashboxes/fetchdefault")
    public Page<account_bank_statement_cashboxImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_bank_statement_cashboxes/createbatch")
    public account_bank_statement_cashboxImpl createBatch(@RequestBody List<account_bank_statement_cashboxImpl> account_bank_statement_cashboxes);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_bank_statement_cashboxes/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_bank_statement_cashboxes/updatebatch")
    public account_bank_statement_cashboxImpl updateBatch(@RequestBody List<account_bank_statement_cashboxImpl> account_bank_statement_cashboxes);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_bank_statement_cashboxes")
    public account_bank_statement_cashboxImpl create(@RequestBody account_bank_statement_cashboxImpl account_bank_statement_cashbox);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_bank_statement_cashboxes/select")
    public Page<account_bank_statement_cashboxImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_bank_statement_cashboxes/{id}/getdraft")
    public account_bank_statement_cashboxImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_bank_statement_cashboxImpl account_bank_statement_cashbox);



}
