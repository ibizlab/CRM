package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_incoterms;
import cn.ibizlab.odoo.client.odoo_account.model.account_incotermsImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_incoterms] 服务对象接口
 */
public interface account_incotermsFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_incoterms/removebatch")
    public account_incotermsImpl removeBatch(@RequestBody List<account_incotermsImpl> account_incoterms);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_incoterms/fetchdefault")
    public Page<account_incotermsImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_incoterms/{id}")
    public account_incotermsImpl update(@PathVariable("id") Integer id,@RequestBody account_incotermsImpl account_incoterms);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_incoterms")
    public account_incotermsImpl create(@RequestBody account_incotermsImpl account_incoterms);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_incoterms/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_incoterms/{id}")
    public account_incotermsImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_incoterms/createbatch")
    public account_incotermsImpl createBatch(@RequestBody List<account_incotermsImpl> account_incoterms);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_incoterms/updatebatch")
    public account_incotermsImpl updateBatch(@RequestBody List<account_incotermsImpl> account_incoterms);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_incoterms/select")
    public Page<account_incotermsImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_incoterms/{id}/getdraft")
    public account_incotermsImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_incotermsImpl account_incoterms);



}
