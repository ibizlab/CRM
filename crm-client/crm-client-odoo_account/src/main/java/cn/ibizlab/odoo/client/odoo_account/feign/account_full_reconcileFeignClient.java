package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_full_reconcile;
import cn.ibizlab.odoo.client.odoo_account.model.account_full_reconcileImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_full_reconcile] 服务对象接口
 */
public interface account_full_reconcileFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_full_reconciles")
    public account_full_reconcileImpl create(@RequestBody account_full_reconcileImpl account_full_reconcile);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_full_reconciles/{id}")
    public account_full_reconcileImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_full_reconciles/removebatch")
    public account_full_reconcileImpl removeBatch(@RequestBody List<account_full_reconcileImpl> account_full_reconciles);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_full_reconciles/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_full_reconciles/fetchdefault")
    public Page<account_full_reconcileImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_full_reconciles/updatebatch")
    public account_full_reconcileImpl updateBatch(@RequestBody List<account_full_reconcileImpl> account_full_reconciles);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_full_reconciles/{id}")
    public account_full_reconcileImpl update(@PathVariable("id") Integer id,@RequestBody account_full_reconcileImpl account_full_reconcile);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_full_reconciles/createbatch")
    public account_full_reconcileImpl createBatch(@RequestBody List<account_full_reconcileImpl> account_full_reconciles);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_full_reconciles/select")
    public Page<account_full_reconcileImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_full_reconciles/{id}/getdraft")
    public account_full_reconcileImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_full_reconcileImpl account_full_reconcile);



}
