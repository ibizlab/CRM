package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_unreconcile;
import cn.ibizlab.odoo.client.odoo_account.model.account_unreconcileImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_unreconcile] 服务对象接口
 */
public interface account_unreconcileFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_unreconciles/fetchdefault")
    public Page<account_unreconcileImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_unreconciles/{id}")
    public account_unreconcileImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_unreconciles/createbatch")
    public account_unreconcileImpl createBatch(@RequestBody List<account_unreconcileImpl> account_unreconciles);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_unreconciles/removebatch")
    public account_unreconcileImpl removeBatch(@RequestBody List<account_unreconcileImpl> account_unreconciles);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_unreconciles")
    public account_unreconcileImpl create(@RequestBody account_unreconcileImpl account_unreconcile);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_unreconciles/updatebatch")
    public account_unreconcileImpl updateBatch(@RequestBody List<account_unreconcileImpl> account_unreconciles);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_unreconciles/{id}")
    public account_unreconcileImpl update(@PathVariable("id") Integer id,@RequestBody account_unreconcileImpl account_unreconcile);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_unreconciles/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_unreconciles/select")
    public Page<account_unreconcileImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_unreconciles/{id}/getdraft")
    public account_unreconcileImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_unreconcileImpl account_unreconcile);



}
