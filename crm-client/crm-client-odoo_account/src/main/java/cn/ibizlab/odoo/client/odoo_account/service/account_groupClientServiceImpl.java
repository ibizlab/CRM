package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_group;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_groupClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_groupImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_groupFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_group] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_groupClientServiceImpl implements Iaccount_groupClientService {

    account_groupFeignClient account_groupFeignClient;

    @Autowired
    public account_groupClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_groupFeignClient = nameBuilder.target(account_groupFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_groupFeignClient = nameBuilder.target(account_groupFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_group createModel() {
		return new account_groupImpl();
	}


    public void removeBatch(List<Iaccount_group> account_groups){
        if(account_groups!=null){
            List<account_groupImpl> list = new ArrayList<account_groupImpl>();
            for(Iaccount_group iaccount_group :account_groups){
                list.add((account_groupImpl)iaccount_group) ;
            }
            account_groupFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iaccount_group> fetchDefault(SearchContext context){
        Page<account_groupImpl> page = this.account_groupFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Iaccount_group account_group){
        Iaccount_group clientModel = account_groupFeignClient.update(account_group.getId(),(account_groupImpl)account_group) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_group.getClass(), false);
        copier.copy(clientModel, account_group, null);
    }


    public void create(Iaccount_group account_group){
        Iaccount_group clientModel = account_groupFeignClient.create((account_groupImpl)account_group) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_group.getClass(), false);
        copier.copy(clientModel, account_group, null);
    }


    public void createBatch(List<Iaccount_group> account_groups){
        if(account_groups!=null){
            List<account_groupImpl> list = new ArrayList<account_groupImpl>();
            for(Iaccount_group iaccount_group :account_groups){
                list.add((account_groupImpl)iaccount_group) ;
            }
            account_groupFeignClient.createBatch(list) ;
        }
    }


    public void get(Iaccount_group account_group){
        Iaccount_group clientModel = account_groupFeignClient.get(account_group.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_group.getClass(), false);
        copier.copy(clientModel, account_group, null);
    }


    public void remove(Iaccount_group account_group){
        account_groupFeignClient.remove(account_group.getId()) ;
    }


    public void updateBatch(List<Iaccount_group> account_groups){
        if(account_groups!=null){
            List<account_groupImpl> list = new ArrayList<account_groupImpl>();
            for(Iaccount_group iaccount_group :account_groups){
                list.add((account_groupImpl)iaccount_group) ;
            }
            account_groupFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iaccount_group> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_group account_group){
        Iaccount_group clientModel = account_groupFeignClient.getDraft(account_group.getId(),(account_groupImpl)account_group) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_group.getClass(), false);
        copier.copy(clientModel, account_group, null);
    }



}

