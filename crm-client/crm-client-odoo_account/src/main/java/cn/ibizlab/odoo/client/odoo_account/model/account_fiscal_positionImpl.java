package cn.ibizlab.odoo.client.odoo_account.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iaccount_fiscal_position;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[account_fiscal_position] 对象
 */
public class account_fiscal_positionImpl implements Iaccount_fiscal_position,Serializable{

    /**
     * 科目映射
     */
    public String account_ids;

    @JsonIgnore
    public boolean account_idsDirtyFlag;
    
    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 自动检测
     */
    public String auto_apply;

    @JsonIgnore
    public boolean auto_applyDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 国家群组
     */
    public Integer country_group_id;

    @JsonIgnore
    public boolean country_group_idDirtyFlag;
    
    /**
     * 国家群组
     */
    public String country_group_id_text;

    @JsonIgnore
    public boolean country_group_id_textDirtyFlag;
    
    /**
     * 国家
     */
    public Integer country_id;

    @JsonIgnore
    public boolean country_idDirtyFlag;
    
    /**
     * 国家
     */
    public String country_id_text;

    @JsonIgnore
    public boolean country_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 税科目调整
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 备注
     */
    public String note;

    @JsonIgnore
    public boolean noteDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 状态数
     */
    public Integer states_count;

    @JsonIgnore
    public boolean states_countDirtyFlag;
    
    /**
     * 联邦政府
     */
    public String state_ids;

    @JsonIgnore
    public boolean state_idsDirtyFlag;
    
    /**
     * 税映射
     */
    public String tax_ids;

    @JsonIgnore
    public boolean tax_idsDirtyFlag;
    
    /**
     * VAT必须
     */
    public String vat_required;

    @JsonIgnore
    public boolean vat_requiredDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 邮编范围从
     */
    public Integer zip_from;

    @JsonIgnore
    public boolean zip_fromDirtyFlag;
    
    /**
     * 邮编范围到
     */
    public Integer zip_to;

    @JsonIgnore
    public boolean zip_toDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [科目映射]
     */
    @JsonProperty("account_ids")
    public String getAccount_ids(){
        return this.account_ids ;
    }

    /**
     * 设置 [科目映射]
     */
    @JsonProperty("account_ids")
    public void setAccount_ids(String  account_ids){
        this.account_ids = account_ids ;
        this.account_idsDirtyFlag = true ;
    }

     /**
     * 获取 [科目映射]脏标记
     */
    @JsonIgnore
    public boolean getAccount_idsDirtyFlag(){
        return this.account_idsDirtyFlag ;
    }   

    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [自动检测]
     */
    @JsonProperty("auto_apply")
    public String getAuto_apply(){
        return this.auto_apply ;
    }

    /**
     * 设置 [自动检测]
     */
    @JsonProperty("auto_apply")
    public void setAuto_apply(String  auto_apply){
        this.auto_apply = auto_apply ;
        this.auto_applyDirtyFlag = true ;
    }

     /**
     * 获取 [自动检测]脏标记
     */
    @JsonIgnore
    public boolean getAuto_applyDirtyFlag(){
        return this.auto_applyDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [国家群组]
     */
    @JsonProperty("country_group_id")
    public Integer getCountry_group_id(){
        return this.country_group_id ;
    }

    /**
     * 设置 [国家群组]
     */
    @JsonProperty("country_group_id")
    public void setCountry_group_id(Integer  country_group_id){
        this.country_group_id = country_group_id ;
        this.country_group_idDirtyFlag = true ;
    }

     /**
     * 获取 [国家群组]脏标记
     */
    @JsonIgnore
    public boolean getCountry_group_idDirtyFlag(){
        return this.country_group_idDirtyFlag ;
    }   

    /**
     * 获取 [国家群组]
     */
    @JsonProperty("country_group_id_text")
    public String getCountry_group_id_text(){
        return this.country_group_id_text ;
    }

    /**
     * 设置 [国家群组]
     */
    @JsonProperty("country_group_id_text")
    public void setCountry_group_id_text(String  country_group_id_text){
        this.country_group_id_text = country_group_id_text ;
        this.country_group_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [国家群组]脏标记
     */
    @JsonIgnore
    public boolean getCountry_group_id_textDirtyFlag(){
        return this.country_group_id_textDirtyFlag ;
    }   

    /**
     * 获取 [国家]
     */
    @JsonProperty("country_id")
    public Integer getCountry_id(){
        return this.country_id ;
    }

    /**
     * 设置 [国家]
     */
    @JsonProperty("country_id")
    public void setCountry_id(Integer  country_id){
        this.country_id = country_id ;
        this.country_idDirtyFlag = true ;
    }

     /**
     * 获取 [国家]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idDirtyFlag(){
        return this.country_idDirtyFlag ;
    }   

    /**
     * 获取 [国家]
     */
    @JsonProperty("country_id_text")
    public String getCountry_id_text(){
        return this.country_id_text ;
    }

    /**
     * 设置 [国家]
     */
    @JsonProperty("country_id_text")
    public void setCountry_id_text(String  country_id_text){
        this.country_id_text = country_id_text ;
        this.country_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [国家]脏标记
     */
    @JsonIgnore
    public boolean getCountry_id_textDirtyFlag(){
        return this.country_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [税科目调整]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [税科目调整]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [税科目调整]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [备注]
     */
    @JsonProperty("note")
    public String getNote(){
        return this.note ;
    }

    /**
     * 设置 [备注]
     */
    @JsonProperty("note")
    public void setNote(String  note){
        this.note = note ;
        this.noteDirtyFlag = true ;
    }

     /**
     * 获取 [备注]脏标记
     */
    @JsonIgnore
    public boolean getNoteDirtyFlag(){
        return this.noteDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [状态数]
     */
    @JsonProperty("states_count")
    public Integer getStates_count(){
        return this.states_count ;
    }

    /**
     * 设置 [状态数]
     */
    @JsonProperty("states_count")
    public void setStates_count(Integer  states_count){
        this.states_count = states_count ;
        this.states_countDirtyFlag = true ;
    }

     /**
     * 获取 [状态数]脏标记
     */
    @JsonIgnore
    public boolean getStates_countDirtyFlag(){
        return this.states_countDirtyFlag ;
    }   

    /**
     * 获取 [联邦政府]
     */
    @JsonProperty("state_ids")
    public String getState_ids(){
        return this.state_ids ;
    }

    /**
     * 设置 [联邦政府]
     */
    @JsonProperty("state_ids")
    public void setState_ids(String  state_ids){
        this.state_ids = state_ids ;
        this.state_idsDirtyFlag = true ;
    }

     /**
     * 获取 [联邦政府]脏标记
     */
    @JsonIgnore
    public boolean getState_idsDirtyFlag(){
        return this.state_idsDirtyFlag ;
    }   

    /**
     * 获取 [税映射]
     */
    @JsonProperty("tax_ids")
    public String getTax_ids(){
        return this.tax_ids ;
    }

    /**
     * 设置 [税映射]
     */
    @JsonProperty("tax_ids")
    public void setTax_ids(String  tax_ids){
        this.tax_ids = tax_ids ;
        this.tax_idsDirtyFlag = true ;
    }

     /**
     * 获取 [税映射]脏标记
     */
    @JsonIgnore
    public boolean getTax_idsDirtyFlag(){
        return this.tax_idsDirtyFlag ;
    }   

    /**
     * 获取 [VAT必须]
     */
    @JsonProperty("vat_required")
    public String getVat_required(){
        return this.vat_required ;
    }

    /**
     * 设置 [VAT必须]
     */
    @JsonProperty("vat_required")
    public void setVat_required(String  vat_required){
        this.vat_required = vat_required ;
        this.vat_requiredDirtyFlag = true ;
    }

     /**
     * 获取 [VAT必须]脏标记
     */
    @JsonIgnore
    public boolean getVat_requiredDirtyFlag(){
        return this.vat_requiredDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [邮编范围从]
     */
    @JsonProperty("zip_from")
    public Integer getZip_from(){
        return this.zip_from ;
    }

    /**
     * 设置 [邮编范围从]
     */
    @JsonProperty("zip_from")
    public void setZip_from(Integer  zip_from){
        this.zip_from = zip_from ;
        this.zip_fromDirtyFlag = true ;
    }

     /**
     * 获取 [邮编范围从]脏标记
     */
    @JsonIgnore
    public boolean getZip_fromDirtyFlag(){
        return this.zip_fromDirtyFlag ;
    }   

    /**
     * 获取 [邮编范围到]
     */
    @JsonProperty("zip_to")
    public Integer getZip_to(){
        return this.zip_to ;
    }

    /**
     * 设置 [邮编范围到]
     */
    @JsonProperty("zip_to")
    public void setZip_to(Integer  zip_to){
        this.zip_to = zip_to ;
        this.zip_toDirtyFlag = true ;
    }

     /**
     * 获取 [邮编范围到]脏标记
     */
    @JsonIgnore
    public boolean getZip_toDirtyFlag(){
        return this.zip_toDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
