package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_incoterms;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_incotermsClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_incotermsImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_incotermsFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_incoterms] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_incotermsClientServiceImpl implements Iaccount_incotermsClientService {

    account_incotermsFeignClient account_incotermsFeignClient;

    @Autowired
    public account_incotermsClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_incotermsFeignClient = nameBuilder.target(account_incotermsFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_incotermsFeignClient = nameBuilder.target(account_incotermsFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_incoterms createModel() {
		return new account_incotermsImpl();
	}


    public void removeBatch(List<Iaccount_incoterms> account_incoterms){
        if(account_incoterms!=null){
            List<account_incotermsImpl> list = new ArrayList<account_incotermsImpl>();
            for(Iaccount_incoterms iaccount_incoterms :account_incoterms){
                list.add((account_incotermsImpl)iaccount_incoterms) ;
            }
            account_incotermsFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iaccount_incoterms> fetchDefault(SearchContext context){
        Page<account_incotermsImpl> page = this.account_incotermsFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Iaccount_incoterms account_incoterms){
        Iaccount_incoterms clientModel = account_incotermsFeignClient.update(account_incoterms.getId(),(account_incotermsImpl)account_incoterms) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_incoterms.getClass(), false);
        copier.copy(clientModel, account_incoterms, null);
    }


    public void create(Iaccount_incoterms account_incoterms){
        Iaccount_incoterms clientModel = account_incotermsFeignClient.create((account_incotermsImpl)account_incoterms) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_incoterms.getClass(), false);
        copier.copy(clientModel, account_incoterms, null);
    }


    public void remove(Iaccount_incoterms account_incoterms){
        account_incotermsFeignClient.remove(account_incoterms.getId()) ;
    }


    public void get(Iaccount_incoterms account_incoterms){
        Iaccount_incoterms clientModel = account_incotermsFeignClient.get(account_incoterms.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_incoterms.getClass(), false);
        copier.copy(clientModel, account_incoterms, null);
    }


    public void createBatch(List<Iaccount_incoterms> account_incoterms){
        if(account_incoterms!=null){
            List<account_incotermsImpl> list = new ArrayList<account_incotermsImpl>();
            for(Iaccount_incoterms iaccount_incoterms :account_incoterms){
                list.add((account_incotermsImpl)iaccount_incoterms) ;
            }
            account_incotermsFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Iaccount_incoterms> account_incoterms){
        if(account_incoterms!=null){
            List<account_incotermsImpl> list = new ArrayList<account_incotermsImpl>();
            for(Iaccount_incoterms iaccount_incoterms :account_incoterms){
                list.add((account_incotermsImpl)iaccount_incoterms) ;
            }
            account_incotermsFeignClient.updateBatch(list) ;
        }
    }


    public Page<Iaccount_incoterms> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_incoterms account_incoterms){
        Iaccount_incoterms clientModel = account_incotermsFeignClient.getDraft(account_incoterms.getId(),(account_incotermsImpl)account_incoterms) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_incoterms.getClass(), false);
        copier.copy(clientModel, account_incoterms, null);
    }



}

