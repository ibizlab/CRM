package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_reconciliation_widget;
import cn.ibizlab.odoo.client.odoo_account.model.account_reconciliation_widgetImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_reconciliation_widget] 服务对象接口
 */
public interface account_reconciliation_widgetFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_reconciliation_widgets/removebatch")
    public account_reconciliation_widgetImpl removeBatch(@RequestBody List<account_reconciliation_widgetImpl> account_reconciliation_widgets);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_reconciliation_widgets/{id}")
    public account_reconciliation_widgetImpl update(@PathVariable("id") Integer id,@RequestBody account_reconciliation_widgetImpl account_reconciliation_widget);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_reconciliation_widgets/createbatch")
    public account_reconciliation_widgetImpl createBatch(@RequestBody List<account_reconciliation_widgetImpl> account_reconciliation_widgets);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_reconciliation_widgets/{id}")
    public account_reconciliation_widgetImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_reconciliation_widgets/fetchdefault")
    public Page<account_reconciliation_widgetImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_reconciliation_widgets")
    public account_reconciliation_widgetImpl create(@RequestBody account_reconciliation_widgetImpl account_reconciliation_widget);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_reconciliation_widgets/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_reconciliation_widgets/updatebatch")
    public account_reconciliation_widgetImpl updateBatch(@RequestBody List<account_reconciliation_widgetImpl> account_reconciliation_widgets);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_reconciliation_widgets/select")
    public Page<account_reconciliation_widgetImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_reconciliation_widgets/{id}/getdraft")
    public account_reconciliation_widgetImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_reconciliation_widgetImpl account_reconciliation_widget);



}
