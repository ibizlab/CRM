package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_confirm;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_invoice_confirmClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_invoice_confirmImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_invoice_confirmFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_invoice_confirm] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_invoice_confirmClientServiceImpl implements Iaccount_invoice_confirmClientService {

    account_invoice_confirmFeignClient account_invoice_confirmFeignClient;

    @Autowired
    public account_invoice_confirmClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_invoice_confirmFeignClient = nameBuilder.target(account_invoice_confirmFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_invoice_confirmFeignClient = nameBuilder.target(account_invoice_confirmFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_invoice_confirm createModel() {
		return new account_invoice_confirmImpl();
	}


    public Page<Iaccount_invoice_confirm> fetchDefault(SearchContext context){
        Page<account_invoice_confirmImpl> page = this.account_invoice_confirmFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Iaccount_invoice_confirm account_invoice_confirm){
        Iaccount_invoice_confirm clientModel = account_invoice_confirmFeignClient.create((account_invoice_confirmImpl)account_invoice_confirm) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_confirm.getClass(), false);
        copier.copy(clientModel, account_invoice_confirm, null);
    }


    public void updateBatch(List<Iaccount_invoice_confirm> account_invoice_confirms){
        if(account_invoice_confirms!=null){
            List<account_invoice_confirmImpl> list = new ArrayList<account_invoice_confirmImpl>();
            for(Iaccount_invoice_confirm iaccount_invoice_confirm :account_invoice_confirms){
                list.add((account_invoice_confirmImpl)iaccount_invoice_confirm) ;
            }
            account_invoice_confirmFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Iaccount_invoice_confirm> account_invoice_confirms){
        if(account_invoice_confirms!=null){
            List<account_invoice_confirmImpl> list = new ArrayList<account_invoice_confirmImpl>();
            for(Iaccount_invoice_confirm iaccount_invoice_confirm :account_invoice_confirms){
                list.add((account_invoice_confirmImpl)iaccount_invoice_confirm) ;
            }
            account_invoice_confirmFeignClient.removeBatch(list) ;
        }
    }


    public void update(Iaccount_invoice_confirm account_invoice_confirm){
        Iaccount_invoice_confirm clientModel = account_invoice_confirmFeignClient.update(account_invoice_confirm.getId(),(account_invoice_confirmImpl)account_invoice_confirm) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_confirm.getClass(), false);
        copier.copy(clientModel, account_invoice_confirm, null);
    }


    public void createBatch(List<Iaccount_invoice_confirm> account_invoice_confirms){
        if(account_invoice_confirms!=null){
            List<account_invoice_confirmImpl> list = new ArrayList<account_invoice_confirmImpl>();
            for(Iaccount_invoice_confirm iaccount_invoice_confirm :account_invoice_confirms){
                list.add((account_invoice_confirmImpl)iaccount_invoice_confirm) ;
            }
            account_invoice_confirmFeignClient.createBatch(list) ;
        }
    }


    public void get(Iaccount_invoice_confirm account_invoice_confirm){
        Iaccount_invoice_confirm clientModel = account_invoice_confirmFeignClient.get(account_invoice_confirm.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_confirm.getClass(), false);
        copier.copy(clientModel, account_invoice_confirm, null);
    }


    public void remove(Iaccount_invoice_confirm account_invoice_confirm){
        account_invoice_confirmFeignClient.remove(account_invoice_confirm.getId()) ;
    }


    public Page<Iaccount_invoice_confirm> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_invoice_confirm account_invoice_confirm){
        Iaccount_invoice_confirm clientModel = account_invoice_confirmFeignClient.getDraft(account_invoice_confirm.getId(),(account_invoice_confirmImpl)account_invoice_confirm) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_confirm.getClass(), false);
        copier.copy(clientModel, account_invoice_confirm, null);
    }



}

