package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_tax;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_invoice_taxClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_invoice_taxImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_invoice_taxFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_invoice_tax] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_invoice_taxClientServiceImpl implements Iaccount_invoice_taxClientService {

    account_invoice_taxFeignClient account_invoice_taxFeignClient;

    @Autowired
    public account_invoice_taxClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_invoice_taxFeignClient = nameBuilder.target(account_invoice_taxFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_invoice_taxFeignClient = nameBuilder.target(account_invoice_taxFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_invoice_tax createModel() {
		return new account_invoice_taxImpl();
	}


    public void updateBatch(List<Iaccount_invoice_tax> account_invoice_taxes){
        if(account_invoice_taxes!=null){
            List<account_invoice_taxImpl> list = new ArrayList<account_invoice_taxImpl>();
            for(Iaccount_invoice_tax iaccount_invoice_tax :account_invoice_taxes){
                list.add((account_invoice_taxImpl)iaccount_invoice_tax) ;
            }
            account_invoice_taxFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Iaccount_invoice_tax> account_invoice_taxes){
        if(account_invoice_taxes!=null){
            List<account_invoice_taxImpl> list = new ArrayList<account_invoice_taxImpl>();
            for(Iaccount_invoice_tax iaccount_invoice_tax :account_invoice_taxes){
                list.add((account_invoice_taxImpl)iaccount_invoice_tax) ;
            }
            account_invoice_taxFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Iaccount_invoice_tax> account_invoice_taxes){
        if(account_invoice_taxes!=null){
            List<account_invoice_taxImpl> list = new ArrayList<account_invoice_taxImpl>();
            for(Iaccount_invoice_tax iaccount_invoice_tax :account_invoice_taxes){
                list.add((account_invoice_taxImpl)iaccount_invoice_tax) ;
            }
            account_invoice_taxFeignClient.createBatch(list) ;
        }
    }


    public void update(Iaccount_invoice_tax account_invoice_tax){
        Iaccount_invoice_tax clientModel = account_invoice_taxFeignClient.update(account_invoice_tax.getId(),(account_invoice_taxImpl)account_invoice_tax) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_tax.getClass(), false);
        copier.copy(clientModel, account_invoice_tax, null);
    }


    public void get(Iaccount_invoice_tax account_invoice_tax){
        Iaccount_invoice_tax clientModel = account_invoice_taxFeignClient.get(account_invoice_tax.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_tax.getClass(), false);
        copier.copy(clientModel, account_invoice_tax, null);
    }


    public void remove(Iaccount_invoice_tax account_invoice_tax){
        account_invoice_taxFeignClient.remove(account_invoice_tax.getId()) ;
    }


    public void create(Iaccount_invoice_tax account_invoice_tax){
        Iaccount_invoice_tax clientModel = account_invoice_taxFeignClient.create((account_invoice_taxImpl)account_invoice_tax) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_tax.getClass(), false);
        copier.copy(clientModel, account_invoice_tax, null);
    }


    public Page<Iaccount_invoice_tax> fetchDefault(SearchContext context){
        Page<account_invoice_taxImpl> page = this.account_invoice_taxFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Iaccount_invoice_tax> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_invoice_tax account_invoice_tax){
        Iaccount_invoice_tax clientModel = account_invoice_taxFeignClient.getDraft(account_invoice_tax.getId(),(account_invoice_taxImpl)account_invoice_tax) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_tax.getClass(), false);
        copier.copy(clientModel, account_invoice_tax, null);
    }



}

