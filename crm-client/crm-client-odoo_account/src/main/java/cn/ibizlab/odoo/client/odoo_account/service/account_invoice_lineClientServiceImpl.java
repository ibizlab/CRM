package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_line;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_invoice_lineClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_invoice_lineImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_invoice_lineFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_invoice_line] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_invoice_lineClientServiceImpl implements Iaccount_invoice_lineClientService {

    account_invoice_lineFeignClient account_invoice_lineFeignClient;

    @Autowired
    public account_invoice_lineClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_invoice_lineFeignClient = nameBuilder.target(account_invoice_lineFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_invoice_lineFeignClient = nameBuilder.target(account_invoice_lineFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_invoice_line createModel() {
		return new account_invoice_lineImpl();
	}


    public void get(Iaccount_invoice_line account_invoice_line){
        Iaccount_invoice_line clientModel = account_invoice_lineFeignClient.get(account_invoice_line.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_line.getClass(), false);
        copier.copy(clientModel, account_invoice_line, null);
    }


    public void create(Iaccount_invoice_line account_invoice_line){
        Iaccount_invoice_line clientModel = account_invoice_lineFeignClient.create((account_invoice_lineImpl)account_invoice_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_line.getClass(), false);
        copier.copy(clientModel, account_invoice_line, null);
    }


    public Page<Iaccount_invoice_line> fetchDefault(SearchContext context){
        Page<account_invoice_lineImpl> page = this.account_invoice_lineFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Iaccount_invoice_line> account_invoice_lines){
        if(account_invoice_lines!=null){
            List<account_invoice_lineImpl> list = new ArrayList<account_invoice_lineImpl>();
            for(Iaccount_invoice_line iaccount_invoice_line :account_invoice_lines){
                list.add((account_invoice_lineImpl)iaccount_invoice_line) ;
            }
            account_invoice_lineFeignClient.createBatch(list) ;
        }
    }


    public void update(Iaccount_invoice_line account_invoice_line){
        Iaccount_invoice_line clientModel = account_invoice_lineFeignClient.update(account_invoice_line.getId(),(account_invoice_lineImpl)account_invoice_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_line.getClass(), false);
        copier.copy(clientModel, account_invoice_line, null);
    }


    public void remove(Iaccount_invoice_line account_invoice_line){
        account_invoice_lineFeignClient.remove(account_invoice_line.getId()) ;
    }


    public void updateBatch(List<Iaccount_invoice_line> account_invoice_lines){
        if(account_invoice_lines!=null){
            List<account_invoice_lineImpl> list = new ArrayList<account_invoice_lineImpl>();
            for(Iaccount_invoice_line iaccount_invoice_line :account_invoice_lines){
                list.add((account_invoice_lineImpl)iaccount_invoice_line) ;
            }
            account_invoice_lineFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Iaccount_invoice_line> account_invoice_lines){
        if(account_invoice_lines!=null){
            List<account_invoice_lineImpl> list = new ArrayList<account_invoice_lineImpl>();
            for(Iaccount_invoice_line iaccount_invoice_line :account_invoice_lines){
                list.add((account_invoice_lineImpl)iaccount_invoice_line) ;
            }
            account_invoice_lineFeignClient.removeBatch(list) ;
        }
    }


    public Page<Iaccount_invoice_line> select(SearchContext context){
        return null ;
    }


    public void checkKey(Iaccount_invoice_line account_invoice_line){
        Iaccount_invoice_line clientModel = account_invoice_lineFeignClient.checkKey(account_invoice_line.getId(),(account_invoice_lineImpl)account_invoice_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_line.getClass(), false);
        copier.copy(clientModel, account_invoice_line, null);
    }


    public void getDraft(Iaccount_invoice_line account_invoice_line){
        Iaccount_invoice_line clientModel = account_invoice_lineFeignClient.getDraft(account_invoice_line.getId(),(account_invoice_lineImpl)account_invoice_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_line.getClass(), false);
        copier.copy(clientModel, account_invoice_line, null);
    }


    public void save(Iaccount_invoice_line account_invoice_line){
        Iaccount_invoice_line clientModel = account_invoice_lineFeignClient.save(account_invoice_line.getId(),(account_invoice_lineImpl)account_invoice_line) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_invoice_line.getClass(), false);
        copier.copy(clientModel, account_invoice_line, null);
    }



}

