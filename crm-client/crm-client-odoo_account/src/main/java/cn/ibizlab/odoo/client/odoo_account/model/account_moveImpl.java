package cn.ibizlab.odoo.client.odoo_account.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iaccount_move;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[account_move] 对象
 */
public class account_moveImpl implements Iaccount_move,Serializable{

    /**
     * 金额
     */
    public Double amount;

    @JsonIgnore
    public boolean amountDirtyFlag;
    
    /**
     * 自动撤销
     */
    public String auto_reverse;

    @JsonIgnore
    public boolean auto_reverseDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 币种
     */
    public String currency_id_text;

    @JsonIgnore
    public boolean currency_id_textDirtyFlag;
    
    /**
     * 日期
     */
    public Timestamp date;

    @JsonIgnore
    public boolean dateDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 科目
     */
    public Integer dummy_account_id;

    @JsonIgnore
    public boolean dummy_account_idDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 日记账
     */
    public Integer journal_id;

    @JsonIgnore
    public boolean journal_idDirtyFlag;
    
    /**
     * 日记账
     */
    public String journal_id_text;

    @JsonIgnore
    public boolean journal_id_textDirtyFlag;
    
    /**
     * 日记账项目
     */
    public String line_ids;

    @JsonIgnore
    public boolean line_idsDirtyFlag;
    
    /**
     * 匹配百分比
     */
    public Double matched_percentage;

    @JsonIgnore
    public boolean matched_percentageDirtyFlag;
    
    /**
     * 号码
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 内部备注
     */
    public String narration;

    @JsonIgnore
    public boolean narrationDirtyFlag;
    
    /**
     * 业务伙伴
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 业务伙伴
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 参考
     */
    public String ref;

    @JsonIgnore
    public boolean refDirtyFlag;
    
    /**
     * 撤销日期
     */
    public Timestamp reverse_date;

    @JsonIgnore
    public boolean reverse_dateDirtyFlag;
    
    /**
     * 撤销分录
     */
    public Integer reverse_entry_id;

    @JsonIgnore
    public boolean reverse_entry_idDirtyFlag;
    
    /**
     * 撤销分录
     */
    public String reverse_entry_id_text;

    @JsonIgnore
    public boolean reverse_entry_id_textDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 库存移动
     */
    public Integer stock_move_id;

    @JsonIgnore
    public boolean stock_move_idDirtyFlag;
    
    /**
     * 库存移动
     */
    public String stock_move_id_text;

    @JsonIgnore
    public boolean stock_move_id_textDirtyFlag;
    
    /**
     * 税率现金收付制分录
     */
    public Integer tax_cash_basis_rec_id;

    @JsonIgnore
    public boolean tax_cash_basis_rec_idDirtyFlag;
    
    /**
     * 税类别域
     */
    public String tax_type_domain;

    @JsonIgnore
    public boolean tax_type_domainDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [金额]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return this.amount ;
    }

    /**
     * 设置 [金额]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

     /**
     * 获取 [金额]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return this.amountDirtyFlag ;
    }   

    /**
     * 获取 [自动撤销]
     */
    @JsonProperty("auto_reverse")
    public String getAuto_reverse(){
        return this.auto_reverse ;
    }

    /**
     * 设置 [自动撤销]
     */
    @JsonProperty("auto_reverse")
    public void setAuto_reverse(String  auto_reverse){
        this.auto_reverse = auto_reverse ;
        this.auto_reverseDirtyFlag = true ;
    }

     /**
     * 获取 [自动撤销]脏标记
     */
    @JsonIgnore
    public boolean getAuto_reverseDirtyFlag(){
        return this.auto_reverseDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return this.currency_id_text ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return this.currency_id_textDirtyFlag ;
    }   

    /**
     * 获取 [日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

     /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [科目]
     */
    @JsonProperty("dummy_account_id")
    public Integer getDummy_account_id(){
        return this.dummy_account_id ;
    }

    /**
     * 设置 [科目]
     */
    @JsonProperty("dummy_account_id")
    public void setDummy_account_id(Integer  dummy_account_id){
        this.dummy_account_id = dummy_account_id ;
        this.dummy_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [科目]脏标记
     */
    @JsonIgnore
    public boolean getDummy_account_idDirtyFlag(){
        return this.dummy_account_idDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [日记账]
     */
    @JsonProperty("journal_id")
    public Integer getJournal_id(){
        return this.journal_id ;
    }

    /**
     * 设置 [日记账]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(Integer  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

     /**
     * 获取 [日记账]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return this.journal_idDirtyFlag ;
    }   

    /**
     * 获取 [日记账]
     */
    @JsonProperty("journal_id_text")
    public String getJournal_id_text(){
        return this.journal_id_text ;
    }

    /**
     * 设置 [日记账]
     */
    @JsonProperty("journal_id_text")
    public void setJournal_id_text(String  journal_id_text){
        this.journal_id_text = journal_id_text ;
        this.journal_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [日记账]脏标记
     */
    @JsonIgnore
    public boolean getJournal_id_textDirtyFlag(){
        return this.journal_id_textDirtyFlag ;
    }   

    /**
     * 获取 [日记账项目]
     */
    @JsonProperty("line_ids")
    public String getLine_ids(){
        return this.line_ids ;
    }

    /**
     * 设置 [日记账项目]
     */
    @JsonProperty("line_ids")
    public void setLine_ids(String  line_ids){
        this.line_ids = line_ids ;
        this.line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [日记账项目]脏标记
     */
    @JsonIgnore
    public boolean getLine_idsDirtyFlag(){
        return this.line_idsDirtyFlag ;
    }   

    /**
     * 获取 [匹配百分比]
     */
    @JsonProperty("matched_percentage")
    public Double getMatched_percentage(){
        return this.matched_percentage ;
    }

    /**
     * 设置 [匹配百分比]
     */
    @JsonProperty("matched_percentage")
    public void setMatched_percentage(Double  matched_percentage){
        this.matched_percentage = matched_percentage ;
        this.matched_percentageDirtyFlag = true ;
    }

     /**
     * 获取 [匹配百分比]脏标记
     */
    @JsonIgnore
    public boolean getMatched_percentageDirtyFlag(){
        return this.matched_percentageDirtyFlag ;
    }   

    /**
     * 获取 [号码]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [号码]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [号码]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [内部备注]
     */
    @JsonProperty("narration")
    public String getNarration(){
        return this.narration ;
    }

    /**
     * 设置 [内部备注]
     */
    @JsonProperty("narration")
    public void setNarration(String  narration){
        this.narration = narration ;
        this.narrationDirtyFlag = true ;
    }

     /**
     * 获取 [内部备注]脏标记
     */
    @JsonIgnore
    public boolean getNarrationDirtyFlag(){
        return this.narrationDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [参考]
     */
    @JsonProperty("ref")
    public String getRef(){
        return this.ref ;
    }

    /**
     * 设置 [参考]
     */
    @JsonProperty("ref")
    public void setRef(String  ref){
        this.ref = ref ;
        this.refDirtyFlag = true ;
    }

     /**
     * 获取 [参考]脏标记
     */
    @JsonIgnore
    public boolean getRefDirtyFlag(){
        return this.refDirtyFlag ;
    }   

    /**
     * 获取 [撤销日期]
     */
    @JsonProperty("reverse_date")
    public Timestamp getReverse_date(){
        return this.reverse_date ;
    }

    /**
     * 设置 [撤销日期]
     */
    @JsonProperty("reverse_date")
    public void setReverse_date(Timestamp  reverse_date){
        this.reverse_date = reverse_date ;
        this.reverse_dateDirtyFlag = true ;
    }

     /**
     * 获取 [撤销日期]脏标记
     */
    @JsonIgnore
    public boolean getReverse_dateDirtyFlag(){
        return this.reverse_dateDirtyFlag ;
    }   

    /**
     * 获取 [撤销分录]
     */
    @JsonProperty("reverse_entry_id")
    public Integer getReverse_entry_id(){
        return this.reverse_entry_id ;
    }

    /**
     * 设置 [撤销分录]
     */
    @JsonProperty("reverse_entry_id")
    public void setReverse_entry_id(Integer  reverse_entry_id){
        this.reverse_entry_id = reverse_entry_id ;
        this.reverse_entry_idDirtyFlag = true ;
    }

     /**
     * 获取 [撤销分录]脏标记
     */
    @JsonIgnore
    public boolean getReverse_entry_idDirtyFlag(){
        return this.reverse_entry_idDirtyFlag ;
    }   

    /**
     * 获取 [撤销分录]
     */
    @JsonProperty("reverse_entry_id_text")
    public String getReverse_entry_id_text(){
        return this.reverse_entry_id_text ;
    }

    /**
     * 设置 [撤销分录]
     */
    @JsonProperty("reverse_entry_id_text")
    public void setReverse_entry_id_text(String  reverse_entry_id_text){
        this.reverse_entry_id_text = reverse_entry_id_text ;
        this.reverse_entry_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [撤销分录]脏标记
     */
    @JsonIgnore
    public boolean getReverse_entry_id_textDirtyFlag(){
        return this.reverse_entry_id_textDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [库存移动]
     */
    @JsonProperty("stock_move_id")
    public Integer getStock_move_id(){
        return this.stock_move_id ;
    }

    /**
     * 设置 [库存移动]
     */
    @JsonProperty("stock_move_id")
    public void setStock_move_id(Integer  stock_move_id){
        this.stock_move_id = stock_move_id ;
        this.stock_move_idDirtyFlag = true ;
    }

     /**
     * 获取 [库存移动]脏标记
     */
    @JsonIgnore
    public boolean getStock_move_idDirtyFlag(){
        return this.stock_move_idDirtyFlag ;
    }   

    /**
     * 获取 [库存移动]
     */
    @JsonProperty("stock_move_id_text")
    public String getStock_move_id_text(){
        return this.stock_move_id_text ;
    }

    /**
     * 设置 [库存移动]
     */
    @JsonProperty("stock_move_id_text")
    public void setStock_move_id_text(String  stock_move_id_text){
        this.stock_move_id_text = stock_move_id_text ;
        this.stock_move_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [库存移动]脏标记
     */
    @JsonIgnore
    public boolean getStock_move_id_textDirtyFlag(){
        return this.stock_move_id_textDirtyFlag ;
    }   

    /**
     * 获取 [税率现金收付制分录]
     */
    @JsonProperty("tax_cash_basis_rec_id")
    public Integer getTax_cash_basis_rec_id(){
        return this.tax_cash_basis_rec_id ;
    }

    /**
     * 设置 [税率现金收付制分录]
     */
    @JsonProperty("tax_cash_basis_rec_id")
    public void setTax_cash_basis_rec_id(Integer  tax_cash_basis_rec_id){
        this.tax_cash_basis_rec_id = tax_cash_basis_rec_id ;
        this.tax_cash_basis_rec_idDirtyFlag = true ;
    }

     /**
     * 获取 [税率现金收付制分录]脏标记
     */
    @JsonIgnore
    public boolean getTax_cash_basis_rec_idDirtyFlag(){
        return this.tax_cash_basis_rec_idDirtyFlag ;
    }   

    /**
     * 获取 [税类别域]
     */
    @JsonProperty("tax_type_domain")
    public String getTax_type_domain(){
        return this.tax_type_domain ;
    }

    /**
     * 设置 [税类别域]
     */
    @JsonProperty("tax_type_domain")
    public void setTax_type_domain(String  tax_type_domain){
        this.tax_type_domain = tax_type_domain ;
        this.tax_type_domainDirtyFlag = true ;
    }

     /**
     * 获取 [税类别域]脏标记
     */
    @JsonIgnore
    public boolean getTax_type_domainDirtyFlag(){
        return this.tax_type_domainDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
