package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_fiscal_position_account;
import cn.ibizlab.odoo.client.odoo_account.model.account_fiscal_position_accountImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_fiscal_position_account] 服务对象接口
 */
public interface account_fiscal_position_accountFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_fiscal_position_accounts/updatebatch")
    public account_fiscal_position_accountImpl updateBatch(@RequestBody List<account_fiscal_position_accountImpl> account_fiscal_position_accounts);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_fiscal_position_accounts/fetchdefault")
    public Page<account_fiscal_position_accountImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_fiscal_position_accounts")
    public account_fiscal_position_accountImpl create(@RequestBody account_fiscal_position_accountImpl account_fiscal_position_account);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_fiscal_position_accounts/createbatch")
    public account_fiscal_position_accountImpl createBatch(@RequestBody List<account_fiscal_position_accountImpl> account_fiscal_position_accounts);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_fiscal_position_accounts/{id}")
    public account_fiscal_position_accountImpl update(@PathVariable("id") Integer id,@RequestBody account_fiscal_position_accountImpl account_fiscal_position_account);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_fiscal_position_accounts/{id}")
    public account_fiscal_position_accountImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_fiscal_position_accounts/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_fiscal_position_accounts/removebatch")
    public account_fiscal_position_accountImpl removeBatch(@RequestBody List<account_fiscal_position_accountImpl> account_fiscal_position_accounts);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_fiscal_position_accounts/select")
    public Page<account_fiscal_position_accountImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_fiscal_position_accounts/{id}/getdraft")
    public account_fiscal_position_accountImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_fiscal_position_accountImpl account_fiscal_position_account);



}
