package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_analytic_distribution;
import cn.ibizlab.odoo.client.odoo_account.model.account_analytic_distributionImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_analytic_distribution] 服务对象接口
 */
public interface account_analytic_distributionFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_analytic_distributions/updatebatch")
    public account_analytic_distributionImpl updateBatch(@RequestBody List<account_analytic_distributionImpl> account_analytic_distributions);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_analytic_distributions/createbatch")
    public account_analytic_distributionImpl createBatch(@RequestBody List<account_analytic_distributionImpl> account_analytic_distributions);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_analytic_distributions/fetchdefault")
    public Page<account_analytic_distributionImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_analytic_distributions")
    public account_analytic_distributionImpl create(@RequestBody account_analytic_distributionImpl account_analytic_distribution);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_analytic_distributions/{id}")
    public account_analytic_distributionImpl update(@PathVariable("id") Integer id,@RequestBody account_analytic_distributionImpl account_analytic_distribution);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_analytic_distributions/removebatch")
    public account_analytic_distributionImpl removeBatch(@RequestBody List<account_analytic_distributionImpl> account_analytic_distributions);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_analytic_distributions/{id}")
    public account_analytic_distributionImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_analytic_distributions/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_analytic_distributions/select")
    public Page<account_analytic_distributionImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_analytic_distributions/{id}/getdraft")
    public account_analytic_distributionImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_analytic_distributionImpl account_analytic_distribution);



}
