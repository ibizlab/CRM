package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_analytic_account;
import cn.ibizlab.odoo.client.odoo_account.model.account_analytic_accountImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_analytic_account] 服务对象接口
 */
public interface account_analytic_accountFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_analytic_accounts/updatebatch")
    public account_analytic_accountImpl updateBatch(@RequestBody List<account_analytic_accountImpl> account_analytic_accounts);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_analytic_accounts")
    public account_analytic_accountImpl create(@RequestBody account_analytic_accountImpl account_analytic_account);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_analytic_accounts/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_analytic_accounts/removebatch")
    public account_analytic_accountImpl removeBatch(@RequestBody List<account_analytic_accountImpl> account_analytic_accounts);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_analytic_accounts/createbatch")
    public account_analytic_accountImpl createBatch(@RequestBody List<account_analytic_accountImpl> account_analytic_accounts);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_analytic_accounts/fetchdefault")
    public Page<account_analytic_accountImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_analytic_accounts/{id}")
    public account_analytic_accountImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_analytic_accounts/{id}")
    public account_analytic_accountImpl update(@PathVariable("id") Integer id,@RequestBody account_analytic_accountImpl account_analytic_account);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_analytic_accounts/select")
    public Page<account_analytic_accountImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_analytic_accounts/{id}/getdraft")
    public account_analytic_accountImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_analytic_accountImpl account_analytic_account);



}
