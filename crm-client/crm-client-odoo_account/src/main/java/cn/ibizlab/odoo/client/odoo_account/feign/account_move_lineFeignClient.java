package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_move_line;
import cn.ibizlab.odoo.client.odoo_account.model.account_move_lineImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_move_line] 服务对象接口
 */
public interface account_move_lineFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_move_lines/{id}")
    public account_move_lineImpl update(@PathVariable("id") Integer id,@RequestBody account_move_lineImpl account_move_line);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_move_lines/createbatch")
    public account_move_lineImpl createBatch(@RequestBody List<account_move_lineImpl> account_move_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_move_lines/fetchdefault")
    public Page<account_move_lineImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_move_lines")
    public account_move_lineImpl create(@RequestBody account_move_lineImpl account_move_line);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_move_lines/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_move_lines/{id}")
    public account_move_lineImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_move_lines/updatebatch")
    public account_move_lineImpl updateBatch(@RequestBody List<account_move_lineImpl> account_move_lines);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_move_lines/removebatch")
    public account_move_lineImpl removeBatch(@RequestBody List<account_move_lineImpl> account_move_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_move_lines/select")
    public Page<account_move_lineImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_move_lines/{id}/getdraft")
    public account_move_lineImpl getDraft(@PathVariable("id") Integer id,@RequestBody account_move_lineImpl account_move_line);



}
