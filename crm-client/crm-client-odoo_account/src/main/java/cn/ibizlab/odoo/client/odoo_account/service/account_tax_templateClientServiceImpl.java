package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_tax_template;
import cn.ibizlab.odoo.client.odoo_account.config.odoo_accountClientProperties;
import cn.ibizlab.odoo.core.client.service.Iaccount_tax_templateClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_tax_templateImpl;
import cn.ibizlab.odoo.client.odoo_account.feign.account_tax_templateFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[account_tax_template] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class account_tax_templateClientServiceImpl implements Iaccount_tax_templateClientService {

    account_tax_templateFeignClient account_tax_templateFeignClient;

    @Autowired
    public account_tax_templateClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_accountClientProperties odoo_accountClientProperties) {
        if (odoo_accountClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_tax_templateFeignClient = nameBuilder.target(account_tax_templateFeignClient.class,"http://"+odoo_accountClientProperties.getServiceId()+"/") ;
		}else if (odoo_accountClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.account_tax_templateFeignClient = nameBuilder.target(account_tax_templateFeignClient.class,odoo_accountClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Iaccount_tax_template createModel() {
		return new account_tax_templateImpl();
	}


    public void update(Iaccount_tax_template account_tax_template){
        Iaccount_tax_template clientModel = account_tax_templateFeignClient.update(account_tax_template.getId(),(account_tax_templateImpl)account_tax_template) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_tax_template.getClass(), false);
        copier.copy(clientModel, account_tax_template, null);
    }


    public void create(Iaccount_tax_template account_tax_template){
        Iaccount_tax_template clientModel = account_tax_templateFeignClient.create((account_tax_templateImpl)account_tax_template) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_tax_template.getClass(), false);
        copier.copy(clientModel, account_tax_template, null);
    }


    public void get(Iaccount_tax_template account_tax_template){
        Iaccount_tax_template clientModel = account_tax_templateFeignClient.get(account_tax_template.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_tax_template.getClass(), false);
        copier.copy(clientModel, account_tax_template, null);
    }


    public void removeBatch(List<Iaccount_tax_template> account_tax_templates){
        if(account_tax_templates!=null){
            List<account_tax_templateImpl> list = new ArrayList<account_tax_templateImpl>();
            for(Iaccount_tax_template iaccount_tax_template :account_tax_templates){
                list.add((account_tax_templateImpl)iaccount_tax_template) ;
            }
            account_tax_templateFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Iaccount_tax_template> account_tax_templates){
        if(account_tax_templates!=null){
            List<account_tax_templateImpl> list = new ArrayList<account_tax_templateImpl>();
            for(Iaccount_tax_template iaccount_tax_template :account_tax_templates){
                list.add((account_tax_templateImpl)iaccount_tax_template) ;
            }
            account_tax_templateFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Iaccount_tax_template account_tax_template){
        account_tax_templateFeignClient.remove(account_tax_template.getId()) ;
    }


    public void createBatch(List<Iaccount_tax_template> account_tax_templates){
        if(account_tax_templates!=null){
            List<account_tax_templateImpl> list = new ArrayList<account_tax_templateImpl>();
            for(Iaccount_tax_template iaccount_tax_template :account_tax_templates){
                list.add((account_tax_templateImpl)iaccount_tax_template) ;
            }
            account_tax_templateFeignClient.createBatch(list) ;
        }
    }


    public Page<Iaccount_tax_template> fetchDefault(SearchContext context){
        Page<account_tax_templateImpl> page = this.account_tax_templateFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Iaccount_tax_template> select(SearchContext context){
        return null ;
    }


    public void getDraft(Iaccount_tax_template account_tax_template){
        Iaccount_tax_template clientModel = account_tax_templateFeignClient.getDraft(account_tax_template.getId(),(account_tax_templateImpl)account_tax_template) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), account_tax_template.getClass(), false);
        copier.copy(clientModel, account_tax_template, null);
    }



}

