package cn.ibizlab.odoo.client.odoo_base_import.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_m2o_related;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_m2o_relatedImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[base_import_tests_models_m2o_related] 服务对象接口
 */
public interface base_import_tests_models_m2o_relatedFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_m2o_relateds/fetchdefault")
    public Page<base_import_tests_models_m2o_relatedImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_m2o_relateds/removebatch")
    public base_import_tests_models_m2o_relatedImpl removeBatch(@RequestBody List<base_import_tests_models_m2o_relatedImpl> base_import_tests_models_m2o_relateds);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_m2o_relateds/updatebatch")
    public base_import_tests_models_m2o_relatedImpl updateBatch(@RequestBody List<base_import_tests_models_m2o_relatedImpl> base_import_tests_models_m2o_relateds);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_m2o_relateds/createbatch")
    public base_import_tests_models_m2o_relatedImpl createBatch(@RequestBody List<base_import_tests_models_m2o_relatedImpl> base_import_tests_models_m2o_relateds);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_m2o_relateds/{id}")
    public base_import_tests_models_m2o_relatedImpl update(@PathVariable("id") Integer id,@RequestBody base_import_tests_models_m2o_relatedImpl base_import_tests_models_m2o_related);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_m2o_relateds/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_m2o_relateds")
    public base_import_tests_models_m2o_relatedImpl create(@RequestBody base_import_tests_models_m2o_relatedImpl base_import_tests_models_m2o_related);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_m2o_relateds/{id}")
    public base_import_tests_models_m2o_relatedImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_m2o_relateds/select")
    public Page<base_import_tests_models_m2o_relatedImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_m2o_relateds/{id}/getdraft")
    public base_import_tests_models_m2o_relatedImpl getDraft(@PathVariable("id") Integer id,@RequestBody base_import_tests_models_m2o_relatedImpl base_import_tests_models_m2o_related);



}
