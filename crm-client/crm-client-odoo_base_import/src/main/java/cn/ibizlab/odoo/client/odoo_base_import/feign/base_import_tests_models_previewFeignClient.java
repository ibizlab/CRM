package cn.ibizlab.odoo.client.odoo_base_import.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_preview;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_previewImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[base_import_tests_models_preview] 服务对象接口
 */
public interface base_import_tests_models_previewFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_previews/createbatch")
    public base_import_tests_models_previewImpl createBatch(@RequestBody List<base_import_tests_models_previewImpl> base_import_tests_models_previews);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_previews/fetchdefault")
    public Page<base_import_tests_models_previewImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_previews/updatebatch")
    public base_import_tests_models_previewImpl updateBatch(@RequestBody List<base_import_tests_models_previewImpl> base_import_tests_models_previews);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_previews")
    public base_import_tests_models_previewImpl create(@RequestBody base_import_tests_models_previewImpl base_import_tests_models_preview);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_previews/{id}")
    public base_import_tests_models_previewImpl update(@PathVariable("id") Integer id,@RequestBody base_import_tests_models_previewImpl base_import_tests_models_preview);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_previews/{id}")
    public base_import_tests_models_previewImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_previews/removebatch")
    public base_import_tests_models_previewImpl removeBatch(@RequestBody List<base_import_tests_models_previewImpl> base_import_tests_models_previews);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_previews/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_previews/select")
    public Page<base_import_tests_models_previewImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_previews/{id}/getdraft")
    public base_import_tests_models_previewImpl getDraft(@PathVariable("id") Integer id,@RequestBody base_import_tests_models_previewImpl base_import_tests_models_preview);



}
