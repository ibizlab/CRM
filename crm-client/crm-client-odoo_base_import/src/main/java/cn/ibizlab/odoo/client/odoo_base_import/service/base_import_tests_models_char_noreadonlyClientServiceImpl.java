package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_char_noreadonly;
import cn.ibizlab.odoo.client.odoo_base_import.config.odoo_base_importClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibase_import_tests_models_char_noreadonlyClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_char_noreadonlyImpl;
import cn.ibizlab.odoo.client.odoo_base_import.feign.base_import_tests_models_char_noreadonlyFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[base_import_tests_models_char_noreadonly] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class base_import_tests_models_char_noreadonlyClientServiceImpl implements Ibase_import_tests_models_char_noreadonlyClientService {

    base_import_tests_models_char_noreadonlyFeignClient base_import_tests_models_char_noreadonlyFeignClient;

    @Autowired
    public base_import_tests_models_char_noreadonlyClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_base_importClientProperties odoo_base_importClientProperties) {
        if (odoo_base_importClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_tests_models_char_noreadonlyFeignClient = nameBuilder.target(base_import_tests_models_char_noreadonlyFeignClient.class,"http://"+odoo_base_importClientProperties.getServiceId()+"/") ;
		}else if (odoo_base_importClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_tests_models_char_noreadonlyFeignClient = nameBuilder.target(base_import_tests_models_char_noreadonlyFeignClient.class,odoo_base_importClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibase_import_tests_models_char_noreadonly createModel() {
		return new base_import_tests_models_char_noreadonlyImpl();
	}


    public void update(Ibase_import_tests_models_char_noreadonly base_import_tests_models_char_noreadonly){
        Ibase_import_tests_models_char_noreadonly clientModel = base_import_tests_models_char_noreadonlyFeignClient.update(base_import_tests_models_char_noreadonly.getId(),(base_import_tests_models_char_noreadonlyImpl)base_import_tests_models_char_noreadonly) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_char_noreadonly.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_char_noreadonly, null);
    }


    public void removeBatch(List<Ibase_import_tests_models_char_noreadonly> base_import_tests_models_char_noreadonlies){
        if(base_import_tests_models_char_noreadonlies!=null){
            List<base_import_tests_models_char_noreadonlyImpl> list = new ArrayList<base_import_tests_models_char_noreadonlyImpl>();
            for(Ibase_import_tests_models_char_noreadonly ibase_import_tests_models_char_noreadonly :base_import_tests_models_char_noreadonlies){
                list.add((base_import_tests_models_char_noreadonlyImpl)ibase_import_tests_models_char_noreadonly) ;
            }
            base_import_tests_models_char_noreadonlyFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Ibase_import_tests_models_char_noreadonly> base_import_tests_models_char_noreadonlies){
        if(base_import_tests_models_char_noreadonlies!=null){
            List<base_import_tests_models_char_noreadonlyImpl> list = new ArrayList<base_import_tests_models_char_noreadonlyImpl>();
            for(Ibase_import_tests_models_char_noreadonly ibase_import_tests_models_char_noreadonly :base_import_tests_models_char_noreadonlies){
                list.add((base_import_tests_models_char_noreadonlyImpl)ibase_import_tests_models_char_noreadonly) ;
            }
            base_import_tests_models_char_noreadonlyFeignClient.createBatch(list) ;
        }
    }


    public Page<Ibase_import_tests_models_char_noreadonly> fetchDefault(SearchContext context){
        Page<base_import_tests_models_char_noreadonlyImpl> page = this.base_import_tests_models_char_noreadonlyFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void updateBatch(List<Ibase_import_tests_models_char_noreadonly> base_import_tests_models_char_noreadonlies){
        if(base_import_tests_models_char_noreadonlies!=null){
            List<base_import_tests_models_char_noreadonlyImpl> list = new ArrayList<base_import_tests_models_char_noreadonlyImpl>();
            for(Ibase_import_tests_models_char_noreadonly ibase_import_tests_models_char_noreadonly :base_import_tests_models_char_noreadonlies){
                list.add((base_import_tests_models_char_noreadonlyImpl)ibase_import_tests_models_char_noreadonly) ;
            }
            base_import_tests_models_char_noreadonlyFeignClient.updateBatch(list) ;
        }
    }


    public void get(Ibase_import_tests_models_char_noreadonly base_import_tests_models_char_noreadonly){
        Ibase_import_tests_models_char_noreadonly clientModel = base_import_tests_models_char_noreadonlyFeignClient.get(base_import_tests_models_char_noreadonly.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_char_noreadonly.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_char_noreadonly, null);
    }


    public void create(Ibase_import_tests_models_char_noreadonly base_import_tests_models_char_noreadonly){
        Ibase_import_tests_models_char_noreadonly clientModel = base_import_tests_models_char_noreadonlyFeignClient.create((base_import_tests_models_char_noreadonlyImpl)base_import_tests_models_char_noreadonly) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_char_noreadonly.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_char_noreadonly, null);
    }


    public void remove(Ibase_import_tests_models_char_noreadonly base_import_tests_models_char_noreadonly){
        base_import_tests_models_char_noreadonlyFeignClient.remove(base_import_tests_models_char_noreadonly.getId()) ;
    }


    public Page<Ibase_import_tests_models_char_noreadonly> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibase_import_tests_models_char_noreadonly base_import_tests_models_char_noreadonly){
        Ibase_import_tests_models_char_noreadonly clientModel = base_import_tests_models_char_noreadonlyFeignClient.getDraft(base_import_tests_models_char_noreadonly.getId(),(base_import_tests_models_char_noreadonlyImpl)base_import_tests_models_char_noreadonly) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_char_noreadonly.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_char_noreadonly, null);
    }



}

