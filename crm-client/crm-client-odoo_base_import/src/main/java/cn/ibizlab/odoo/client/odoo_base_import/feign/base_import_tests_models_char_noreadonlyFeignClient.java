package cn.ibizlab.odoo.client.odoo_base_import.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_char_noreadonly;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_char_noreadonlyImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[base_import_tests_models_char_noreadonly] 服务对象接口
 */
public interface base_import_tests_models_char_noreadonlyFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_char_noreadonlies/{id}")
    public base_import_tests_models_char_noreadonlyImpl update(@PathVariable("id") Integer id,@RequestBody base_import_tests_models_char_noreadonlyImpl base_import_tests_models_char_noreadonly);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_char_noreadonlies/removebatch")
    public base_import_tests_models_char_noreadonlyImpl removeBatch(@RequestBody List<base_import_tests_models_char_noreadonlyImpl> base_import_tests_models_char_noreadonlies);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_char_noreadonlies/createbatch")
    public base_import_tests_models_char_noreadonlyImpl createBatch(@RequestBody List<base_import_tests_models_char_noreadonlyImpl> base_import_tests_models_char_noreadonlies);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_char_noreadonlies/fetchdefault")
    public Page<base_import_tests_models_char_noreadonlyImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_char_noreadonlies/updatebatch")
    public base_import_tests_models_char_noreadonlyImpl updateBatch(@RequestBody List<base_import_tests_models_char_noreadonlyImpl> base_import_tests_models_char_noreadonlies);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_char_noreadonlies/{id}")
    public base_import_tests_models_char_noreadonlyImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_char_noreadonlies")
    public base_import_tests_models_char_noreadonlyImpl create(@RequestBody base_import_tests_models_char_noreadonlyImpl base_import_tests_models_char_noreadonly);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_char_noreadonlies/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_char_noreadonlies/select")
    public Page<base_import_tests_models_char_noreadonlyImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_char_noreadonlies/{id}/getdraft")
    public base_import_tests_models_char_noreadonlyImpl getDraft(@PathVariable("id") Integer id,@RequestBody base_import_tests_models_char_noreadonlyImpl base_import_tests_models_char_noreadonly);



}
