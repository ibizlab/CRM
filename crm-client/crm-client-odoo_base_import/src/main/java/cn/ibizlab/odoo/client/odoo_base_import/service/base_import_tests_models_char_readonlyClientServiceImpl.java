package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_char_readonly;
import cn.ibizlab.odoo.client.odoo_base_import.config.odoo_base_importClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibase_import_tests_models_char_readonlyClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_char_readonlyImpl;
import cn.ibizlab.odoo.client.odoo_base_import.feign.base_import_tests_models_char_readonlyFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[base_import_tests_models_char_readonly] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class base_import_tests_models_char_readonlyClientServiceImpl implements Ibase_import_tests_models_char_readonlyClientService {

    base_import_tests_models_char_readonlyFeignClient base_import_tests_models_char_readonlyFeignClient;

    @Autowired
    public base_import_tests_models_char_readonlyClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_base_importClientProperties odoo_base_importClientProperties) {
        if (odoo_base_importClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_tests_models_char_readonlyFeignClient = nameBuilder.target(base_import_tests_models_char_readonlyFeignClient.class,"http://"+odoo_base_importClientProperties.getServiceId()+"/") ;
		}else if (odoo_base_importClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_tests_models_char_readonlyFeignClient = nameBuilder.target(base_import_tests_models_char_readonlyFeignClient.class,odoo_base_importClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibase_import_tests_models_char_readonly createModel() {
		return new base_import_tests_models_char_readonlyImpl();
	}


    public void update(Ibase_import_tests_models_char_readonly base_import_tests_models_char_readonly){
        Ibase_import_tests_models_char_readonly clientModel = base_import_tests_models_char_readonlyFeignClient.update(base_import_tests_models_char_readonly.getId(),(base_import_tests_models_char_readonlyImpl)base_import_tests_models_char_readonly) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_char_readonly.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_char_readonly, null);
    }


    public void createBatch(List<Ibase_import_tests_models_char_readonly> base_import_tests_models_char_readonlies){
        if(base_import_tests_models_char_readonlies!=null){
            List<base_import_tests_models_char_readonlyImpl> list = new ArrayList<base_import_tests_models_char_readonlyImpl>();
            for(Ibase_import_tests_models_char_readonly ibase_import_tests_models_char_readonly :base_import_tests_models_char_readonlies){
                list.add((base_import_tests_models_char_readonlyImpl)ibase_import_tests_models_char_readonly) ;
            }
            base_import_tests_models_char_readonlyFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Ibase_import_tests_models_char_readonly> base_import_tests_models_char_readonlies){
        if(base_import_tests_models_char_readonlies!=null){
            List<base_import_tests_models_char_readonlyImpl> list = new ArrayList<base_import_tests_models_char_readonlyImpl>();
            for(Ibase_import_tests_models_char_readonly ibase_import_tests_models_char_readonly :base_import_tests_models_char_readonlies){
                list.add((base_import_tests_models_char_readonlyImpl)ibase_import_tests_models_char_readonly) ;
            }
            base_import_tests_models_char_readonlyFeignClient.removeBatch(list) ;
        }
    }


    public void create(Ibase_import_tests_models_char_readonly base_import_tests_models_char_readonly){
        Ibase_import_tests_models_char_readonly clientModel = base_import_tests_models_char_readonlyFeignClient.create((base_import_tests_models_char_readonlyImpl)base_import_tests_models_char_readonly) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_char_readonly.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_char_readonly, null);
    }


    public void updateBatch(List<Ibase_import_tests_models_char_readonly> base_import_tests_models_char_readonlies){
        if(base_import_tests_models_char_readonlies!=null){
            List<base_import_tests_models_char_readonlyImpl> list = new ArrayList<base_import_tests_models_char_readonlyImpl>();
            for(Ibase_import_tests_models_char_readonly ibase_import_tests_models_char_readonly :base_import_tests_models_char_readonlies){
                list.add((base_import_tests_models_char_readonlyImpl)ibase_import_tests_models_char_readonly) ;
            }
            base_import_tests_models_char_readonlyFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Ibase_import_tests_models_char_readonly base_import_tests_models_char_readonly){
        base_import_tests_models_char_readonlyFeignClient.remove(base_import_tests_models_char_readonly.getId()) ;
    }


    public Page<Ibase_import_tests_models_char_readonly> fetchDefault(SearchContext context){
        Page<base_import_tests_models_char_readonlyImpl> page = this.base_import_tests_models_char_readonlyFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Ibase_import_tests_models_char_readonly base_import_tests_models_char_readonly){
        Ibase_import_tests_models_char_readonly clientModel = base_import_tests_models_char_readonlyFeignClient.get(base_import_tests_models_char_readonly.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_char_readonly.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_char_readonly, null);
    }


    public Page<Ibase_import_tests_models_char_readonly> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibase_import_tests_models_char_readonly base_import_tests_models_char_readonly){
        Ibase_import_tests_models_char_readonly clientModel = base_import_tests_models_char_readonlyFeignClient.getDraft(base_import_tests_models_char_readonly.getId(),(base_import_tests_models_char_readonlyImpl)base_import_tests_models_char_readonly) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_char_readonly.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_char_readonly, null);
    }



}

