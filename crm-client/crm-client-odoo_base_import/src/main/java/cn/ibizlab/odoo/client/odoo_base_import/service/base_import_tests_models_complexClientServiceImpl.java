package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_complex;
import cn.ibizlab.odoo.client.odoo_base_import.config.odoo_base_importClientProperties;
import cn.ibizlab.odoo.core.client.service.Ibase_import_tests_models_complexClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_complexImpl;
import cn.ibizlab.odoo.client.odoo_base_import.feign.base_import_tests_models_complexFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[base_import_tests_models_complex] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class base_import_tests_models_complexClientServiceImpl implements Ibase_import_tests_models_complexClientService {

    base_import_tests_models_complexFeignClient base_import_tests_models_complexFeignClient;

    @Autowired
    public base_import_tests_models_complexClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_base_importClientProperties odoo_base_importClientProperties) {
        if (odoo_base_importClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_tests_models_complexFeignClient = nameBuilder.target(base_import_tests_models_complexFeignClient.class,"http://"+odoo_base_importClientProperties.getServiceId()+"/") ;
		}else if (odoo_base_importClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.base_import_tests_models_complexFeignClient = nameBuilder.target(base_import_tests_models_complexFeignClient.class,odoo_base_importClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ibase_import_tests_models_complex createModel() {
		return new base_import_tests_models_complexImpl();
	}


    public void update(Ibase_import_tests_models_complex base_import_tests_models_complex){
        Ibase_import_tests_models_complex clientModel = base_import_tests_models_complexFeignClient.update(base_import_tests_models_complex.getId(),(base_import_tests_models_complexImpl)base_import_tests_models_complex) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_complex.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_complex, null);
    }


    public Page<Ibase_import_tests_models_complex> fetchDefault(SearchContext context){
        Page<base_import_tests_models_complexImpl> page = this.base_import_tests_models_complexFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void createBatch(List<Ibase_import_tests_models_complex> base_import_tests_models_complices){
        if(base_import_tests_models_complices!=null){
            List<base_import_tests_models_complexImpl> list = new ArrayList<base_import_tests_models_complexImpl>();
            for(Ibase_import_tests_models_complex ibase_import_tests_models_complex :base_import_tests_models_complices){
                list.add((base_import_tests_models_complexImpl)ibase_import_tests_models_complex) ;
            }
            base_import_tests_models_complexFeignClient.createBatch(list) ;
        }
    }


    public void remove(Ibase_import_tests_models_complex base_import_tests_models_complex){
        base_import_tests_models_complexFeignClient.remove(base_import_tests_models_complex.getId()) ;
    }


    public void get(Ibase_import_tests_models_complex base_import_tests_models_complex){
        Ibase_import_tests_models_complex clientModel = base_import_tests_models_complexFeignClient.get(base_import_tests_models_complex.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_complex.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_complex, null);
    }


    public void create(Ibase_import_tests_models_complex base_import_tests_models_complex){
        Ibase_import_tests_models_complex clientModel = base_import_tests_models_complexFeignClient.create((base_import_tests_models_complexImpl)base_import_tests_models_complex) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_complex.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_complex, null);
    }


    public void updateBatch(List<Ibase_import_tests_models_complex> base_import_tests_models_complices){
        if(base_import_tests_models_complices!=null){
            List<base_import_tests_models_complexImpl> list = new ArrayList<base_import_tests_models_complexImpl>();
            for(Ibase_import_tests_models_complex ibase_import_tests_models_complex :base_import_tests_models_complices){
                list.add((base_import_tests_models_complexImpl)ibase_import_tests_models_complex) ;
            }
            base_import_tests_models_complexFeignClient.updateBatch(list) ;
        }
    }


    public void removeBatch(List<Ibase_import_tests_models_complex> base_import_tests_models_complices){
        if(base_import_tests_models_complices!=null){
            List<base_import_tests_models_complexImpl> list = new ArrayList<base_import_tests_models_complexImpl>();
            for(Ibase_import_tests_models_complex ibase_import_tests_models_complex :base_import_tests_models_complices){
                list.add((base_import_tests_models_complexImpl)ibase_import_tests_models_complex) ;
            }
            base_import_tests_models_complexFeignClient.removeBatch(list) ;
        }
    }


    public Page<Ibase_import_tests_models_complex> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ibase_import_tests_models_complex base_import_tests_models_complex){
        Ibase_import_tests_models_complex clientModel = base_import_tests_models_complexFeignClient.getDraft(base_import_tests_models_complex.getId(),(base_import_tests_models_complexImpl)base_import_tests_models_complex) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), base_import_tests_models_complex.getClass(), false);
        copier.copy(clientModel, base_import_tests_models_complex, null);
    }



}

