package cn.ibizlab.odoo.client.odoo_base_import.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibase_import_import;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_importImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[base_import_import] 服务对象接口
 */
public interface base_import_importFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_imports/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_imports/fetchdefault")
    public Page<base_import_importImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_imports/{id}")
    public base_import_importImpl update(@PathVariable("id") Integer id,@RequestBody base_import_importImpl base_import_import);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_imports")
    public base_import_importImpl create(@RequestBody base_import_importImpl base_import_import);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_imports/{id}")
    public base_import_importImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_imports/updatebatch")
    public base_import_importImpl updateBatch(@RequestBody List<base_import_importImpl> base_import_imports);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_imports/removebatch")
    public base_import_importImpl removeBatch(@RequestBody List<base_import_importImpl> base_import_imports);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_imports/createbatch")
    public base_import_importImpl createBatch(@RequestBody List<base_import_importImpl> base_import_imports);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_imports/select")
    public Page<base_import_importImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_imports/{id}/getdraft")
    public base_import_importImpl getDraft(@PathVariable("id") Integer id,@RequestBody base_import_importImpl base_import_import);



}
