package cn.ibizlab.odoo.client.odoo_base_import.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_char_required;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_char_requiredImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[base_import_tests_models_char_required] 服务对象接口
 */
public interface base_import_tests_models_char_requiredFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_char_requireds/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_char_requireds/fetchdefault")
    public Page<base_import_tests_models_char_requiredImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_char_requireds/updatebatch")
    public base_import_tests_models_char_requiredImpl updateBatch(@RequestBody List<base_import_tests_models_char_requiredImpl> base_import_tests_models_char_requireds);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_char_requireds")
    public base_import_tests_models_char_requiredImpl create(@RequestBody base_import_tests_models_char_requiredImpl base_import_tests_models_char_required);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_char_requireds/createbatch")
    public base_import_tests_models_char_requiredImpl createBatch(@RequestBody List<base_import_tests_models_char_requiredImpl> base_import_tests_models_char_requireds);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_char_requireds/{id}")
    public base_import_tests_models_char_requiredImpl update(@PathVariable("id") Integer id,@RequestBody base_import_tests_models_char_requiredImpl base_import_tests_models_char_required);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_char_requireds/{id}")
    public base_import_tests_models_char_requiredImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_char_requireds/removebatch")
    public base_import_tests_models_char_requiredImpl removeBatch(@RequestBody List<base_import_tests_models_char_requiredImpl> base_import_tests_models_char_requireds);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_char_requireds/select")
    public Page<base_import_tests_models_char_requiredImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_char_requireds/{id}/getdraft")
    public base_import_tests_models_char_requiredImpl getDraft(@PathVariable("id") Integer id,@RequestBody base_import_tests_models_char_requiredImpl base_import_tests_models_char_required);



}
