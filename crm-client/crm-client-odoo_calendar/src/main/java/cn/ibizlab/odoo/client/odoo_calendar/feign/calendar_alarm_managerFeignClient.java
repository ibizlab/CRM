package cn.ibizlab.odoo.client.odoo_calendar.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Icalendar_alarm_manager;
import cn.ibizlab.odoo.client.odoo_calendar.model.calendar_alarm_managerImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[calendar_alarm_manager] 服务对象接口
 */
public interface calendar_alarm_managerFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_calendar/calendar_alarm_managers/updatebatch")
    public calendar_alarm_managerImpl updateBatch(@RequestBody List<calendar_alarm_managerImpl> calendar_alarm_managers);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_calendar/calendar_alarm_managers/{id}")
    public calendar_alarm_managerImpl update(@PathVariable("id") Integer id,@RequestBody calendar_alarm_managerImpl calendar_alarm_manager);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_calendar/calendar_alarm_managers/removebatch")
    public calendar_alarm_managerImpl removeBatch(@RequestBody List<calendar_alarm_managerImpl> calendar_alarm_managers);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_alarm_managers/{id}")
    public calendar_alarm_managerImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_calendar/calendar_alarm_managers/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_calendar/calendar_alarm_managers")
    public calendar_alarm_managerImpl create(@RequestBody calendar_alarm_managerImpl calendar_alarm_manager);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_alarm_managers/fetchdefault")
    public Page<calendar_alarm_managerImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_calendar/calendar_alarm_managers/createbatch")
    public calendar_alarm_managerImpl createBatch(@RequestBody List<calendar_alarm_managerImpl> calendar_alarm_managers);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_alarm_managers/select")
    public Page<calendar_alarm_managerImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_alarm_managers/{id}/getdraft")
    public calendar_alarm_managerImpl getDraft(@PathVariable("id") Integer id,@RequestBody calendar_alarm_managerImpl calendar_alarm_manager);



}
