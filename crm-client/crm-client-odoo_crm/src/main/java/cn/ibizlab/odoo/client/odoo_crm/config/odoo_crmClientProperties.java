package cn.ibizlab.odoo.client.odoo_crm.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.service.odoo.crm")
@Data
public class odoo_crmClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

	private String serviceUrl ;
    	
    private String serviceId ;

}
