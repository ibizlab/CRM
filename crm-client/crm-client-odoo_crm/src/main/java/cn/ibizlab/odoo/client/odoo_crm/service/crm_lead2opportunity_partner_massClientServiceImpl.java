package cn.ibizlab.odoo.client.odoo_crm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icrm_lead2opportunity_partner_mass;
import cn.ibizlab.odoo.client.odoo_crm.config.odoo_crmClientProperties;
import cn.ibizlab.odoo.core.client.service.Icrm_lead2opportunity_partner_massClientService;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_lead2opportunity_partner_massImpl;
import cn.ibizlab.odoo.client.odoo_crm.feign.crm_lead2opportunity_partner_massFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[crm_lead2opportunity_partner_mass] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class crm_lead2opportunity_partner_massClientServiceImpl implements Icrm_lead2opportunity_partner_massClientService {

    crm_lead2opportunity_partner_massFeignClient crm_lead2opportunity_partner_massFeignClient;

    @Autowired
    public crm_lead2opportunity_partner_massClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_crmClientProperties odoo_crmClientProperties) {
        if (odoo_crmClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.crm_lead2opportunity_partner_massFeignClient = nameBuilder.target(crm_lead2opportunity_partner_massFeignClient.class,"http://"+odoo_crmClientProperties.getServiceId()+"/") ;
		}else if (odoo_crmClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.crm_lead2opportunity_partner_massFeignClient = nameBuilder.target(crm_lead2opportunity_partner_massFeignClient.class,odoo_crmClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Icrm_lead2opportunity_partner_mass createModel() {
		return new crm_lead2opportunity_partner_massImpl();
	}


    public void updateBatch(List<Icrm_lead2opportunity_partner_mass> crm_lead2opportunity_partner_masses){
        if(crm_lead2opportunity_partner_masses!=null){
            List<crm_lead2opportunity_partner_massImpl> list = new ArrayList<crm_lead2opportunity_partner_massImpl>();
            for(Icrm_lead2opportunity_partner_mass icrm_lead2opportunity_partner_mass :crm_lead2opportunity_partner_masses){
                list.add((crm_lead2opportunity_partner_massImpl)icrm_lead2opportunity_partner_mass) ;
            }
            crm_lead2opportunity_partner_massFeignClient.updateBatch(list) ;
        }
    }


    public Page<Icrm_lead2opportunity_partner_mass> fetchDefault(SearchContext context){
        Page<crm_lead2opportunity_partner_massImpl> page = this.crm_lead2opportunity_partner_massFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Icrm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass){
        Icrm_lead2opportunity_partner_mass clientModel = crm_lead2opportunity_partner_massFeignClient.update(crm_lead2opportunity_partner_mass.getId(),(crm_lead2opportunity_partner_massImpl)crm_lead2opportunity_partner_mass) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_lead2opportunity_partner_mass.getClass(), false);
        copier.copy(clientModel, crm_lead2opportunity_partner_mass, null);
    }


    public void remove(Icrm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass){
        crm_lead2opportunity_partner_massFeignClient.remove(crm_lead2opportunity_partner_mass.getId()) ;
    }


    public void get(Icrm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass){
        Icrm_lead2opportunity_partner_mass clientModel = crm_lead2opportunity_partner_massFeignClient.get(crm_lead2opportunity_partner_mass.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_lead2opportunity_partner_mass.getClass(), false);
        copier.copy(clientModel, crm_lead2opportunity_partner_mass, null);
    }


    public void create(Icrm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass){
        Icrm_lead2opportunity_partner_mass clientModel = crm_lead2opportunity_partner_massFeignClient.create((crm_lead2opportunity_partner_massImpl)crm_lead2opportunity_partner_mass) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_lead2opportunity_partner_mass.getClass(), false);
        copier.copy(clientModel, crm_lead2opportunity_partner_mass, null);
    }


    public void removeBatch(List<Icrm_lead2opportunity_partner_mass> crm_lead2opportunity_partner_masses){
        if(crm_lead2opportunity_partner_masses!=null){
            List<crm_lead2opportunity_partner_massImpl> list = new ArrayList<crm_lead2opportunity_partner_massImpl>();
            for(Icrm_lead2opportunity_partner_mass icrm_lead2opportunity_partner_mass :crm_lead2opportunity_partner_masses){
                list.add((crm_lead2opportunity_partner_massImpl)icrm_lead2opportunity_partner_mass) ;
            }
            crm_lead2opportunity_partner_massFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Icrm_lead2opportunity_partner_mass> crm_lead2opportunity_partner_masses){
        if(crm_lead2opportunity_partner_masses!=null){
            List<crm_lead2opportunity_partner_massImpl> list = new ArrayList<crm_lead2opportunity_partner_massImpl>();
            for(Icrm_lead2opportunity_partner_mass icrm_lead2opportunity_partner_mass :crm_lead2opportunity_partner_masses){
                list.add((crm_lead2opportunity_partner_massImpl)icrm_lead2opportunity_partner_mass) ;
            }
            crm_lead2opportunity_partner_massFeignClient.createBatch(list) ;
        }
    }


    public Page<Icrm_lead2opportunity_partner_mass> select(SearchContext context){
        return null ;
    }


    public void getDraft(Icrm_lead2opportunity_partner_mass crm_lead2opportunity_partner_mass){
        Icrm_lead2opportunity_partner_mass clientModel = crm_lead2opportunity_partner_massFeignClient.getDraft(crm_lead2opportunity_partner_mass.getId(),(crm_lead2opportunity_partner_massImpl)crm_lead2opportunity_partner_mass) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_lead2opportunity_partner_mass.getClass(), false);
        copier.copy(clientModel, crm_lead2opportunity_partner_mass, null);
    }



}

