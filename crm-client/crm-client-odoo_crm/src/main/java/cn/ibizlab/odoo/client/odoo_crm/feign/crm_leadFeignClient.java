package cn.ibizlab.odoo.client.odoo_crm.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Icrm_lead;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_leadImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[crm_lead] 服务对象接口
 */
public interface crm_leadFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_leads/{id}")
    public crm_leadImpl update(@PathVariable("id") Integer id,@RequestBody crm_leadImpl crm_lead);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_leads/updatebatch")
    public crm_leadImpl updateBatch(@RequestBody List<crm_leadImpl> crm_leads);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_leads/fetchdefault")
    public Page<crm_leadImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_leads")
    public crm_leadImpl create(@RequestBody crm_leadImpl crm_lead);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_leads/createbatch")
    public crm_leadImpl createBatch(@RequestBody List<crm_leadImpl> crm_leads);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_leads/{id}")
    public crm_leadImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_leads/removebatch")
    public crm_leadImpl removeBatch(@RequestBody List<crm_leadImpl> crm_leads);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_leads/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_leads/select")
    public Page<crm_leadImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_leads/{id}/getdraft")
    public crm_leadImpl getDraft(@PathVariable("id") Integer id,@RequestBody crm_leadImpl crm_lead);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_leads/{id}/save")
    public crm_leadImpl save(@PathVariable("id") Integer id,@RequestBody crm_leadImpl crm_lead);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_leads/{id}/checkkey")
    public crm_leadImpl checkKey(@PathVariable("id") Integer id,@RequestBody crm_leadImpl crm_lead);



}
