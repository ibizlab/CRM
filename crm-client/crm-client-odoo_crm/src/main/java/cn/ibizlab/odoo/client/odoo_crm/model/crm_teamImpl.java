package cn.ibizlab.odoo.client.odoo_crm.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Icrm_team;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[crm_team] 对象
 */
public class crm_teamImpl implements Icrm_team,Serializable{

    /**
     * 遗弃购物车数量
     */
    public Integer abandoned_carts_amount;

    @JsonIgnore
    public boolean abandoned_carts_amountDirtyFlag;
    
    /**
     * 遗弃购物车数量
     */
    public Integer abandoned_carts_count;

    @JsonIgnore
    public boolean abandoned_carts_countDirtyFlag;
    
    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 安全联系人别名
     */
    public String alias_contact;

    @JsonIgnore
    public boolean alias_contactDirtyFlag;
    
    /**
     * 默认值
     */
    public String alias_defaults;

    @JsonIgnore
    public boolean alias_defaultsDirtyFlag;
    
    /**
     * 网域别名
     */
    public String alias_domain;

    @JsonIgnore
    public boolean alias_domainDirtyFlag;
    
    /**
     * 记录线索ID
     */
    public Integer alias_force_thread_id;

    @JsonIgnore
    public boolean alias_force_thread_idDirtyFlag;
    
    /**
     * 别名
     */
    public Integer alias_id;

    @JsonIgnore
    public boolean alias_idDirtyFlag;
    
    /**
     * 模型别名
     */
    public Integer alias_model_id;

    @JsonIgnore
    public boolean alias_model_idDirtyFlag;
    
    /**
     * 别名
     */
    public String alias_name;

    @JsonIgnore
    public boolean alias_nameDirtyFlag;
    
    /**
     * 上级模型
     */
    public Integer alias_parent_model_id;

    @JsonIgnore
    public boolean alias_parent_model_idDirtyFlag;
    
    /**
     * 上级记录ID
     */
    public Integer alias_parent_thread_id;

    @JsonIgnore
    public boolean alias_parent_thread_idDirtyFlag;
    
    /**
     * 所有者
     */
    public Integer alias_user_id;

    @JsonIgnore
    public boolean alias_user_idDirtyFlag;
    
    /**
     * 颜色索引
     */
    public Integer color;

    @JsonIgnore
    public boolean colorDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 仪表板按钮
     */
    public String dashboard_button_name;

    @JsonIgnore
    public boolean dashboard_button_nameDirtyFlag;
    
    /**
     * 数据仪表图
     */
    public String dashboard_graph_data;

    @JsonIgnore
    public boolean dashboard_graph_dataDirtyFlag;
    
    /**
     * 分组
     */
    public String dashboard_graph_group;

    @JsonIgnore
    public boolean dashboard_graph_groupDirtyFlag;
    
    /**
     * 分组方式
     */
    public String dashboard_graph_group_pipeline;

    @JsonIgnore
    public boolean dashboard_graph_group_pipelineDirtyFlag;
    
    /**
     * POS分组
     */
    public String dashboard_graph_group_pos;

    @JsonIgnore
    public boolean dashboard_graph_group_posDirtyFlag;
    
    /**
     * 内容
     */
    public String dashboard_graph_model;

    @JsonIgnore
    public boolean dashboard_graph_modelDirtyFlag;
    
    /**
     * 比例
     */
    public String dashboard_graph_period;

    @JsonIgnore
    public boolean dashboard_graph_periodDirtyFlag;
    
    /**
     * 预计关闭
     */
    public String dashboard_graph_period_pipeline;

    @JsonIgnore
    public boolean dashboard_graph_period_pipelineDirtyFlag;
    
    /**
     * 类型
     */
    public String dashboard_graph_type;

    @JsonIgnore
    public boolean dashboard_graph_typeDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 最喜欢的成员
     */
    public String favorite_user_ids;

    @JsonIgnore
    public boolean favorite_user_idsDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 本月已开发票
     */
    public Integer invoiced;

    @JsonIgnore
    public boolean invoicedDirtyFlag;
    
    /**
     * 发票目标
     */
    public Integer invoiced_target;

    @JsonIgnore
    public boolean invoiced_targetDirtyFlag;
    
    /**
     * 显示仪表
     */
    public String is_favorite;

    @JsonIgnore
    public boolean is_favoriteDirtyFlag;
    
    /**
     * 渠道人员
     */
    public String member_ids;

    @JsonIgnore
    public boolean member_idsDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 错误数
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 消息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 需要激活
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 行动数量
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 销售团队
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 商机收入
     */
    public Integer opportunities_amount;

    @JsonIgnore
    public boolean opportunities_amountDirtyFlag;
    
    /**
     * 开启的商机数
     */
    public Integer opportunities_count;

    @JsonIgnore
    public boolean opportunities_countDirtyFlag;
    
    /**
     * POS
     */
    public String pos_config_ids;

    @JsonIgnore
    public boolean pos_config_idsDirtyFlag;
    
    /**
     * 会议销售金额
     */
    public Double pos_order_amount_total;

    @JsonIgnore
    public boolean pos_order_amount_totalDirtyFlag;
    
    /**
     * 开放POS会议
     */
    public Integer pos_sessions_open_count;

    @JsonIgnore
    public boolean pos_sessions_open_countDirtyFlag;
    
    /**
     * 发票金额
     */
    public Integer quotations_amount;

    @JsonIgnore
    public boolean quotations_amountDirtyFlag;
    
    /**
     * 发票报价单
     */
    public Integer quotations_count;

    @JsonIgnore
    public boolean quotations_countDirtyFlag;
    
    /**
     * 回复 至
     */
    public String reply_to;

    @JsonIgnore
    public boolean reply_toDirtyFlag;
    
    /**
     * 发票销售单
     */
    public Integer sales_to_invoice_count;

    @JsonIgnore
    public boolean sales_to_invoice_countDirtyFlag;
    
    /**
     * 团队类型
     */
    public String team_type;

    @JsonIgnore
    public boolean team_typeDirtyFlag;
    
    /**
     * 未分派线索
     */
    public Integer unassigned_leads_count;

    @JsonIgnore
    public boolean unassigned_leads_countDirtyFlag;
    
    /**
     * 团队负责人
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 团队负责人
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 设定开票目标
     */
    public String use_invoices;

    @JsonIgnore
    public boolean use_invoicesDirtyFlag;
    
    /**
     * 线索
     */
    public String use_leads;

    @JsonIgnore
    public boolean use_leadsDirtyFlag;
    
    /**
     * 渠道
     */
    public String use_opportunities;

    @JsonIgnore
    public boolean use_opportunitiesDirtyFlag;
    
    /**
     * 报价单
     */
    public String use_quotations;

    @JsonIgnore
    public boolean use_quotationsDirtyFlag;
    
    /**
     * 网站
     */
    public String website_ids;

    @JsonIgnore
    public boolean website_idsDirtyFlag;
    
    /**
     * 网站信息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [遗弃购物车数量]
     */
    @JsonProperty("abandoned_carts_amount")
    public Integer getAbandoned_carts_amount(){
        return this.abandoned_carts_amount ;
    }

    /**
     * 设置 [遗弃购物车数量]
     */
    @JsonProperty("abandoned_carts_amount")
    public void setAbandoned_carts_amount(Integer  abandoned_carts_amount){
        this.abandoned_carts_amount = abandoned_carts_amount ;
        this.abandoned_carts_amountDirtyFlag = true ;
    }

     /**
     * 获取 [遗弃购物车数量]脏标记
     */
    @JsonIgnore
    public boolean getAbandoned_carts_amountDirtyFlag(){
        return this.abandoned_carts_amountDirtyFlag ;
    }   

    /**
     * 获取 [遗弃购物车数量]
     */
    @JsonProperty("abandoned_carts_count")
    public Integer getAbandoned_carts_count(){
        return this.abandoned_carts_count ;
    }

    /**
     * 设置 [遗弃购物车数量]
     */
    @JsonProperty("abandoned_carts_count")
    public void setAbandoned_carts_count(Integer  abandoned_carts_count){
        this.abandoned_carts_count = abandoned_carts_count ;
        this.abandoned_carts_countDirtyFlag = true ;
    }

     /**
     * 获取 [遗弃购物车数量]脏标记
     */
    @JsonIgnore
    public boolean getAbandoned_carts_countDirtyFlag(){
        return this.abandoned_carts_countDirtyFlag ;
    }   

    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [安全联系人别名]
     */
    @JsonProperty("alias_contact")
    public String getAlias_contact(){
        return this.alias_contact ;
    }

    /**
     * 设置 [安全联系人别名]
     */
    @JsonProperty("alias_contact")
    public void setAlias_contact(String  alias_contact){
        this.alias_contact = alias_contact ;
        this.alias_contactDirtyFlag = true ;
    }

     /**
     * 获取 [安全联系人别名]脏标记
     */
    @JsonIgnore
    public boolean getAlias_contactDirtyFlag(){
        return this.alias_contactDirtyFlag ;
    }   

    /**
     * 获取 [默认值]
     */
    @JsonProperty("alias_defaults")
    public String getAlias_defaults(){
        return this.alias_defaults ;
    }

    /**
     * 设置 [默认值]
     */
    @JsonProperty("alias_defaults")
    public void setAlias_defaults(String  alias_defaults){
        this.alias_defaults = alias_defaults ;
        this.alias_defaultsDirtyFlag = true ;
    }

     /**
     * 获取 [默认值]脏标记
     */
    @JsonIgnore
    public boolean getAlias_defaultsDirtyFlag(){
        return this.alias_defaultsDirtyFlag ;
    }   

    /**
     * 获取 [网域别名]
     */
    @JsonProperty("alias_domain")
    public String getAlias_domain(){
        return this.alias_domain ;
    }

    /**
     * 设置 [网域别名]
     */
    @JsonProperty("alias_domain")
    public void setAlias_domain(String  alias_domain){
        this.alias_domain = alias_domain ;
        this.alias_domainDirtyFlag = true ;
    }

     /**
     * 获取 [网域别名]脏标记
     */
    @JsonIgnore
    public boolean getAlias_domainDirtyFlag(){
        return this.alias_domainDirtyFlag ;
    }   

    /**
     * 获取 [记录线索ID]
     */
    @JsonProperty("alias_force_thread_id")
    public Integer getAlias_force_thread_id(){
        return this.alias_force_thread_id ;
    }

    /**
     * 设置 [记录线索ID]
     */
    @JsonProperty("alias_force_thread_id")
    public void setAlias_force_thread_id(Integer  alias_force_thread_id){
        this.alias_force_thread_id = alias_force_thread_id ;
        this.alias_force_thread_idDirtyFlag = true ;
    }

     /**
     * 获取 [记录线索ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_force_thread_idDirtyFlag(){
        return this.alias_force_thread_idDirtyFlag ;
    }   

    /**
     * 获取 [别名]
     */
    @JsonProperty("alias_id")
    public Integer getAlias_id(){
        return this.alias_id ;
    }

    /**
     * 设置 [别名]
     */
    @JsonProperty("alias_id")
    public void setAlias_id(Integer  alias_id){
        this.alias_id = alias_id ;
        this.alias_idDirtyFlag = true ;
    }

     /**
     * 获取 [别名]脏标记
     */
    @JsonIgnore
    public boolean getAlias_idDirtyFlag(){
        return this.alias_idDirtyFlag ;
    }   

    /**
     * 获取 [模型别名]
     */
    @JsonProperty("alias_model_id")
    public Integer getAlias_model_id(){
        return this.alias_model_id ;
    }

    /**
     * 设置 [模型别名]
     */
    @JsonProperty("alias_model_id")
    public void setAlias_model_id(Integer  alias_model_id){
        this.alias_model_id = alias_model_id ;
        this.alias_model_idDirtyFlag = true ;
    }

     /**
     * 获取 [模型别名]脏标记
     */
    @JsonIgnore
    public boolean getAlias_model_idDirtyFlag(){
        return this.alias_model_idDirtyFlag ;
    }   

    /**
     * 获取 [别名]
     */
    @JsonProperty("alias_name")
    public String getAlias_name(){
        return this.alias_name ;
    }

    /**
     * 设置 [别名]
     */
    @JsonProperty("alias_name")
    public void setAlias_name(String  alias_name){
        this.alias_name = alias_name ;
        this.alias_nameDirtyFlag = true ;
    }

     /**
     * 获取 [别名]脏标记
     */
    @JsonIgnore
    public boolean getAlias_nameDirtyFlag(){
        return this.alias_nameDirtyFlag ;
    }   

    /**
     * 获取 [上级模型]
     */
    @JsonProperty("alias_parent_model_id")
    public Integer getAlias_parent_model_id(){
        return this.alias_parent_model_id ;
    }

    /**
     * 设置 [上级模型]
     */
    @JsonProperty("alias_parent_model_id")
    public void setAlias_parent_model_id(Integer  alias_parent_model_id){
        this.alias_parent_model_id = alias_parent_model_id ;
        this.alias_parent_model_idDirtyFlag = true ;
    }

     /**
     * 获取 [上级模型]脏标记
     */
    @JsonIgnore
    public boolean getAlias_parent_model_idDirtyFlag(){
        return this.alias_parent_model_idDirtyFlag ;
    }   

    /**
     * 获取 [上级记录ID]
     */
    @JsonProperty("alias_parent_thread_id")
    public Integer getAlias_parent_thread_id(){
        return this.alias_parent_thread_id ;
    }

    /**
     * 设置 [上级记录ID]
     */
    @JsonProperty("alias_parent_thread_id")
    public void setAlias_parent_thread_id(Integer  alias_parent_thread_id){
        this.alias_parent_thread_id = alias_parent_thread_id ;
        this.alias_parent_thread_idDirtyFlag = true ;
    }

     /**
     * 获取 [上级记录ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_parent_thread_idDirtyFlag(){
        return this.alias_parent_thread_idDirtyFlag ;
    }   

    /**
     * 获取 [所有者]
     */
    @JsonProperty("alias_user_id")
    public Integer getAlias_user_id(){
        return this.alias_user_id ;
    }

    /**
     * 设置 [所有者]
     */
    @JsonProperty("alias_user_id")
    public void setAlias_user_id(Integer  alias_user_id){
        this.alias_user_id = alias_user_id ;
        this.alias_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [所有者]脏标记
     */
    @JsonIgnore
    public boolean getAlias_user_idDirtyFlag(){
        return this.alias_user_idDirtyFlag ;
    }   

    /**
     * 获取 [颜色索引]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return this.color ;
    }

    /**
     * 设置 [颜色索引]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

     /**
     * 获取 [颜色索引]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return this.colorDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [仪表板按钮]
     */
    @JsonProperty("dashboard_button_name")
    public String getDashboard_button_name(){
        return this.dashboard_button_name ;
    }

    /**
     * 设置 [仪表板按钮]
     */
    @JsonProperty("dashboard_button_name")
    public void setDashboard_button_name(String  dashboard_button_name){
        this.dashboard_button_name = dashboard_button_name ;
        this.dashboard_button_nameDirtyFlag = true ;
    }

     /**
     * 获取 [仪表板按钮]脏标记
     */
    @JsonIgnore
    public boolean getDashboard_button_nameDirtyFlag(){
        return this.dashboard_button_nameDirtyFlag ;
    }   

    /**
     * 获取 [数据仪表图]
     */
    @JsonProperty("dashboard_graph_data")
    public String getDashboard_graph_data(){
        return this.dashboard_graph_data ;
    }

    /**
     * 设置 [数据仪表图]
     */
    @JsonProperty("dashboard_graph_data")
    public void setDashboard_graph_data(String  dashboard_graph_data){
        this.dashboard_graph_data = dashboard_graph_data ;
        this.dashboard_graph_dataDirtyFlag = true ;
    }

     /**
     * 获取 [数据仪表图]脏标记
     */
    @JsonIgnore
    public boolean getDashboard_graph_dataDirtyFlag(){
        return this.dashboard_graph_dataDirtyFlag ;
    }   

    /**
     * 获取 [分组]
     */
    @JsonProperty("dashboard_graph_group")
    public String getDashboard_graph_group(){
        return this.dashboard_graph_group ;
    }

    /**
     * 设置 [分组]
     */
    @JsonProperty("dashboard_graph_group")
    public void setDashboard_graph_group(String  dashboard_graph_group){
        this.dashboard_graph_group = dashboard_graph_group ;
        this.dashboard_graph_groupDirtyFlag = true ;
    }

     /**
     * 获取 [分组]脏标记
     */
    @JsonIgnore
    public boolean getDashboard_graph_groupDirtyFlag(){
        return this.dashboard_graph_groupDirtyFlag ;
    }   

    /**
     * 获取 [分组方式]
     */
    @JsonProperty("dashboard_graph_group_pipeline")
    public String getDashboard_graph_group_pipeline(){
        return this.dashboard_graph_group_pipeline ;
    }

    /**
     * 设置 [分组方式]
     */
    @JsonProperty("dashboard_graph_group_pipeline")
    public void setDashboard_graph_group_pipeline(String  dashboard_graph_group_pipeline){
        this.dashboard_graph_group_pipeline = dashboard_graph_group_pipeline ;
        this.dashboard_graph_group_pipelineDirtyFlag = true ;
    }

     /**
     * 获取 [分组方式]脏标记
     */
    @JsonIgnore
    public boolean getDashboard_graph_group_pipelineDirtyFlag(){
        return this.dashboard_graph_group_pipelineDirtyFlag ;
    }   

    /**
     * 获取 [POS分组]
     */
    @JsonProperty("dashboard_graph_group_pos")
    public String getDashboard_graph_group_pos(){
        return this.dashboard_graph_group_pos ;
    }

    /**
     * 设置 [POS分组]
     */
    @JsonProperty("dashboard_graph_group_pos")
    public void setDashboard_graph_group_pos(String  dashboard_graph_group_pos){
        this.dashboard_graph_group_pos = dashboard_graph_group_pos ;
        this.dashboard_graph_group_posDirtyFlag = true ;
    }

     /**
     * 获取 [POS分组]脏标记
     */
    @JsonIgnore
    public boolean getDashboard_graph_group_posDirtyFlag(){
        return this.dashboard_graph_group_posDirtyFlag ;
    }   

    /**
     * 获取 [内容]
     */
    @JsonProperty("dashboard_graph_model")
    public String getDashboard_graph_model(){
        return this.dashboard_graph_model ;
    }

    /**
     * 设置 [内容]
     */
    @JsonProperty("dashboard_graph_model")
    public void setDashboard_graph_model(String  dashboard_graph_model){
        this.dashboard_graph_model = dashboard_graph_model ;
        this.dashboard_graph_modelDirtyFlag = true ;
    }

     /**
     * 获取 [内容]脏标记
     */
    @JsonIgnore
    public boolean getDashboard_graph_modelDirtyFlag(){
        return this.dashboard_graph_modelDirtyFlag ;
    }   

    /**
     * 获取 [比例]
     */
    @JsonProperty("dashboard_graph_period")
    public String getDashboard_graph_period(){
        return this.dashboard_graph_period ;
    }

    /**
     * 设置 [比例]
     */
    @JsonProperty("dashboard_graph_period")
    public void setDashboard_graph_period(String  dashboard_graph_period){
        this.dashboard_graph_period = dashboard_graph_period ;
        this.dashboard_graph_periodDirtyFlag = true ;
    }

     /**
     * 获取 [比例]脏标记
     */
    @JsonIgnore
    public boolean getDashboard_graph_periodDirtyFlag(){
        return this.dashboard_graph_periodDirtyFlag ;
    }   

    /**
     * 获取 [预计关闭]
     */
    @JsonProperty("dashboard_graph_period_pipeline")
    public String getDashboard_graph_period_pipeline(){
        return this.dashboard_graph_period_pipeline ;
    }

    /**
     * 设置 [预计关闭]
     */
    @JsonProperty("dashboard_graph_period_pipeline")
    public void setDashboard_graph_period_pipeline(String  dashboard_graph_period_pipeline){
        this.dashboard_graph_period_pipeline = dashboard_graph_period_pipeline ;
        this.dashboard_graph_period_pipelineDirtyFlag = true ;
    }

     /**
     * 获取 [预计关闭]脏标记
     */
    @JsonIgnore
    public boolean getDashboard_graph_period_pipelineDirtyFlag(){
        return this.dashboard_graph_period_pipelineDirtyFlag ;
    }   

    /**
     * 获取 [类型]
     */
    @JsonProperty("dashboard_graph_type")
    public String getDashboard_graph_type(){
        return this.dashboard_graph_type ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("dashboard_graph_type")
    public void setDashboard_graph_type(String  dashboard_graph_type){
        this.dashboard_graph_type = dashboard_graph_type ;
        this.dashboard_graph_typeDirtyFlag = true ;
    }

     /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getDashboard_graph_typeDirtyFlag(){
        return this.dashboard_graph_typeDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [最喜欢的成员]
     */
    @JsonProperty("favorite_user_ids")
    public String getFavorite_user_ids(){
        return this.favorite_user_ids ;
    }

    /**
     * 设置 [最喜欢的成员]
     */
    @JsonProperty("favorite_user_ids")
    public void setFavorite_user_ids(String  favorite_user_ids){
        this.favorite_user_ids = favorite_user_ids ;
        this.favorite_user_idsDirtyFlag = true ;
    }

     /**
     * 获取 [最喜欢的成员]脏标记
     */
    @JsonIgnore
    public boolean getFavorite_user_idsDirtyFlag(){
        return this.favorite_user_idsDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [本月已开发票]
     */
    @JsonProperty("invoiced")
    public Integer getInvoiced(){
        return this.invoiced ;
    }

    /**
     * 设置 [本月已开发票]
     */
    @JsonProperty("invoiced")
    public void setInvoiced(Integer  invoiced){
        this.invoiced = invoiced ;
        this.invoicedDirtyFlag = true ;
    }

     /**
     * 获取 [本月已开发票]脏标记
     */
    @JsonIgnore
    public boolean getInvoicedDirtyFlag(){
        return this.invoicedDirtyFlag ;
    }   

    /**
     * 获取 [发票目标]
     */
    @JsonProperty("invoiced_target")
    public Integer getInvoiced_target(){
        return this.invoiced_target ;
    }

    /**
     * 设置 [发票目标]
     */
    @JsonProperty("invoiced_target")
    public void setInvoiced_target(Integer  invoiced_target){
        this.invoiced_target = invoiced_target ;
        this.invoiced_targetDirtyFlag = true ;
    }

     /**
     * 获取 [发票目标]脏标记
     */
    @JsonIgnore
    public boolean getInvoiced_targetDirtyFlag(){
        return this.invoiced_targetDirtyFlag ;
    }   

    /**
     * 获取 [显示仪表]
     */
    @JsonProperty("is_favorite")
    public String getIs_favorite(){
        return this.is_favorite ;
    }

    /**
     * 设置 [显示仪表]
     */
    @JsonProperty("is_favorite")
    public void setIs_favorite(String  is_favorite){
        this.is_favorite = is_favorite ;
        this.is_favoriteDirtyFlag = true ;
    }

     /**
     * 获取 [显示仪表]脏标记
     */
    @JsonIgnore
    public boolean getIs_favoriteDirtyFlag(){
        return this.is_favoriteDirtyFlag ;
    }   

    /**
     * 获取 [渠道人员]
     */
    @JsonProperty("member_ids")
    public String getMember_ids(){
        return this.member_ids ;
    }

    /**
     * 设置 [渠道人员]
     */
    @JsonProperty("member_ids")
    public void setMember_ids(String  member_ids){
        this.member_ids = member_ids ;
        this.member_idsDirtyFlag = true ;
    }

     /**
     * 获取 [渠道人员]脏标记
     */
    @JsonIgnore
    public boolean getMember_idsDirtyFlag(){
        return this.member_idsDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [错误数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [需要激活]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要激活]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [需要激活]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [行动数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [销售团队]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [销售团队]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [销售团队]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [商机收入]
     */
    @JsonProperty("opportunities_amount")
    public Integer getOpportunities_amount(){
        return this.opportunities_amount ;
    }

    /**
     * 设置 [商机收入]
     */
    @JsonProperty("opportunities_amount")
    public void setOpportunities_amount(Integer  opportunities_amount){
        this.opportunities_amount = opportunities_amount ;
        this.opportunities_amountDirtyFlag = true ;
    }

     /**
     * 获取 [商机收入]脏标记
     */
    @JsonIgnore
    public boolean getOpportunities_amountDirtyFlag(){
        return this.opportunities_amountDirtyFlag ;
    }   

    /**
     * 获取 [开启的商机数]
     */
    @JsonProperty("opportunities_count")
    public Integer getOpportunities_count(){
        return this.opportunities_count ;
    }

    /**
     * 设置 [开启的商机数]
     */
    @JsonProperty("opportunities_count")
    public void setOpportunities_count(Integer  opportunities_count){
        this.opportunities_count = opportunities_count ;
        this.opportunities_countDirtyFlag = true ;
    }

     /**
     * 获取 [开启的商机数]脏标记
     */
    @JsonIgnore
    public boolean getOpportunities_countDirtyFlag(){
        return this.opportunities_countDirtyFlag ;
    }   

    /**
     * 获取 [POS]
     */
    @JsonProperty("pos_config_ids")
    public String getPos_config_ids(){
        return this.pos_config_ids ;
    }

    /**
     * 设置 [POS]
     */
    @JsonProperty("pos_config_ids")
    public void setPos_config_ids(String  pos_config_ids){
        this.pos_config_ids = pos_config_ids ;
        this.pos_config_idsDirtyFlag = true ;
    }

     /**
     * 获取 [POS]脏标记
     */
    @JsonIgnore
    public boolean getPos_config_idsDirtyFlag(){
        return this.pos_config_idsDirtyFlag ;
    }   

    /**
     * 获取 [会议销售金额]
     */
    @JsonProperty("pos_order_amount_total")
    public Double getPos_order_amount_total(){
        return this.pos_order_amount_total ;
    }

    /**
     * 设置 [会议销售金额]
     */
    @JsonProperty("pos_order_amount_total")
    public void setPos_order_amount_total(Double  pos_order_amount_total){
        this.pos_order_amount_total = pos_order_amount_total ;
        this.pos_order_amount_totalDirtyFlag = true ;
    }

     /**
     * 获取 [会议销售金额]脏标记
     */
    @JsonIgnore
    public boolean getPos_order_amount_totalDirtyFlag(){
        return this.pos_order_amount_totalDirtyFlag ;
    }   

    /**
     * 获取 [开放POS会议]
     */
    @JsonProperty("pos_sessions_open_count")
    public Integer getPos_sessions_open_count(){
        return this.pos_sessions_open_count ;
    }

    /**
     * 设置 [开放POS会议]
     */
    @JsonProperty("pos_sessions_open_count")
    public void setPos_sessions_open_count(Integer  pos_sessions_open_count){
        this.pos_sessions_open_count = pos_sessions_open_count ;
        this.pos_sessions_open_countDirtyFlag = true ;
    }

     /**
     * 获取 [开放POS会议]脏标记
     */
    @JsonIgnore
    public boolean getPos_sessions_open_countDirtyFlag(){
        return this.pos_sessions_open_countDirtyFlag ;
    }   

    /**
     * 获取 [发票金额]
     */
    @JsonProperty("quotations_amount")
    public Integer getQuotations_amount(){
        return this.quotations_amount ;
    }

    /**
     * 设置 [发票金额]
     */
    @JsonProperty("quotations_amount")
    public void setQuotations_amount(Integer  quotations_amount){
        this.quotations_amount = quotations_amount ;
        this.quotations_amountDirtyFlag = true ;
    }

     /**
     * 获取 [发票金额]脏标记
     */
    @JsonIgnore
    public boolean getQuotations_amountDirtyFlag(){
        return this.quotations_amountDirtyFlag ;
    }   

    /**
     * 获取 [发票报价单]
     */
    @JsonProperty("quotations_count")
    public Integer getQuotations_count(){
        return this.quotations_count ;
    }

    /**
     * 设置 [发票报价单]
     */
    @JsonProperty("quotations_count")
    public void setQuotations_count(Integer  quotations_count){
        this.quotations_count = quotations_count ;
        this.quotations_countDirtyFlag = true ;
    }

     /**
     * 获取 [发票报价单]脏标记
     */
    @JsonIgnore
    public boolean getQuotations_countDirtyFlag(){
        return this.quotations_countDirtyFlag ;
    }   

    /**
     * 获取 [回复 至]
     */
    @JsonProperty("reply_to")
    public String getReply_to(){
        return this.reply_to ;
    }

    /**
     * 设置 [回复 至]
     */
    @JsonProperty("reply_to")
    public void setReply_to(String  reply_to){
        this.reply_to = reply_to ;
        this.reply_toDirtyFlag = true ;
    }

     /**
     * 获取 [回复 至]脏标记
     */
    @JsonIgnore
    public boolean getReply_toDirtyFlag(){
        return this.reply_toDirtyFlag ;
    }   

    /**
     * 获取 [发票销售单]
     */
    @JsonProperty("sales_to_invoice_count")
    public Integer getSales_to_invoice_count(){
        return this.sales_to_invoice_count ;
    }

    /**
     * 设置 [发票销售单]
     */
    @JsonProperty("sales_to_invoice_count")
    public void setSales_to_invoice_count(Integer  sales_to_invoice_count){
        this.sales_to_invoice_count = sales_to_invoice_count ;
        this.sales_to_invoice_countDirtyFlag = true ;
    }

     /**
     * 获取 [发票销售单]脏标记
     */
    @JsonIgnore
    public boolean getSales_to_invoice_countDirtyFlag(){
        return this.sales_to_invoice_countDirtyFlag ;
    }   

    /**
     * 获取 [团队类型]
     */
    @JsonProperty("team_type")
    public String getTeam_type(){
        return this.team_type ;
    }

    /**
     * 设置 [团队类型]
     */
    @JsonProperty("team_type")
    public void setTeam_type(String  team_type){
        this.team_type = team_type ;
        this.team_typeDirtyFlag = true ;
    }

     /**
     * 获取 [团队类型]脏标记
     */
    @JsonIgnore
    public boolean getTeam_typeDirtyFlag(){
        return this.team_typeDirtyFlag ;
    }   

    /**
     * 获取 [未分派线索]
     */
    @JsonProperty("unassigned_leads_count")
    public Integer getUnassigned_leads_count(){
        return this.unassigned_leads_count ;
    }

    /**
     * 设置 [未分派线索]
     */
    @JsonProperty("unassigned_leads_count")
    public void setUnassigned_leads_count(Integer  unassigned_leads_count){
        this.unassigned_leads_count = unassigned_leads_count ;
        this.unassigned_leads_countDirtyFlag = true ;
    }

     /**
     * 获取 [未分派线索]脏标记
     */
    @JsonIgnore
    public boolean getUnassigned_leads_countDirtyFlag(){
        return this.unassigned_leads_countDirtyFlag ;
    }   

    /**
     * 获取 [团队负责人]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [团队负责人]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [团队负责人]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [团队负责人]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [团队负责人]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [团队负责人]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [设定开票目标]
     */
    @JsonProperty("use_invoices")
    public String getUse_invoices(){
        return this.use_invoices ;
    }

    /**
     * 设置 [设定开票目标]
     */
    @JsonProperty("use_invoices")
    public void setUse_invoices(String  use_invoices){
        this.use_invoices = use_invoices ;
        this.use_invoicesDirtyFlag = true ;
    }

     /**
     * 获取 [设定开票目标]脏标记
     */
    @JsonIgnore
    public boolean getUse_invoicesDirtyFlag(){
        return this.use_invoicesDirtyFlag ;
    }   

    /**
     * 获取 [线索]
     */
    @JsonProperty("use_leads")
    public String getUse_leads(){
        return this.use_leads ;
    }

    /**
     * 设置 [线索]
     */
    @JsonProperty("use_leads")
    public void setUse_leads(String  use_leads){
        this.use_leads = use_leads ;
        this.use_leadsDirtyFlag = true ;
    }

     /**
     * 获取 [线索]脏标记
     */
    @JsonIgnore
    public boolean getUse_leadsDirtyFlag(){
        return this.use_leadsDirtyFlag ;
    }   

    /**
     * 获取 [渠道]
     */
    @JsonProperty("use_opportunities")
    public String getUse_opportunities(){
        return this.use_opportunities ;
    }

    /**
     * 设置 [渠道]
     */
    @JsonProperty("use_opportunities")
    public void setUse_opportunities(String  use_opportunities){
        this.use_opportunities = use_opportunities ;
        this.use_opportunitiesDirtyFlag = true ;
    }

     /**
     * 获取 [渠道]脏标记
     */
    @JsonIgnore
    public boolean getUse_opportunitiesDirtyFlag(){
        return this.use_opportunitiesDirtyFlag ;
    }   

    /**
     * 获取 [报价单]
     */
    @JsonProperty("use_quotations")
    public String getUse_quotations(){
        return this.use_quotations ;
    }

    /**
     * 设置 [报价单]
     */
    @JsonProperty("use_quotations")
    public void setUse_quotations(String  use_quotations){
        this.use_quotations = use_quotations ;
        this.use_quotationsDirtyFlag = true ;
    }

     /**
     * 获取 [报价单]脏标记
     */
    @JsonIgnore
    public boolean getUse_quotationsDirtyFlag(){
        return this.use_quotationsDirtyFlag ;
    }   

    /**
     * 获取 [网站]
     */
    @JsonProperty("website_ids")
    public String getWebsite_ids(){
        return this.website_ids ;
    }

    /**
     * 设置 [网站]
     */
    @JsonProperty("website_ids")
    public void setWebsite_ids(String  website_ids){
        this.website_ids = website_ids ;
        this.website_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idsDirtyFlag(){
        return this.website_idsDirtyFlag ;
    }   

    /**
     * 获取 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站信息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
