package cn.ibizlab.odoo.client.odoo_crm.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Icrm_activity_report;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_activity_reportImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[crm_activity_report] 服务对象接口
 */
public interface crm_activity_reportFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_activity_reports/removebatch")
    public crm_activity_reportImpl removeBatch(@RequestBody List<crm_activity_reportImpl> crm_activity_reports);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_activity_reports/createbatch")
    public crm_activity_reportImpl createBatch(@RequestBody List<crm_activity_reportImpl> crm_activity_reports);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_activity_reports/updatebatch")
    public crm_activity_reportImpl updateBatch(@RequestBody List<crm_activity_reportImpl> crm_activity_reports);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_activity_reports/fetchdefault")
    public Page<crm_activity_reportImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_activity_reports")
    public crm_activity_reportImpl create(@RequestBody crm_activity_reportImpl crm_activity_report);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_activity_reports/{id}")
    public crm_activity_reportImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_activity_reports/{id}")
    public crm_activity_reportImpl update(@PathVariable("id") Integer id,@RequestBody crm_activity_reportImpl crm_activity_report);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_activity_reports/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_activity_reports/select")
    public Page<crm_activity_reportImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_activity_reports/{id}/getdraft")
    public crm_activity_reportImpl getDraft(@PathVariable("id") Integer id,@RequestBody crm_activity_reportImpl crm_activity_report);



}
