package cn.ibizlab.odoo.client.odoo_crm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icrm_activity_report;
import cn.ibizlab.odoo.client.odoo_crm.config.odoo_crmClientProperties;
import cn.ibizlab.odoo.core.client.service.Icrm_activity_reportClientService;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_activity_reportImpl;
import cn.ibizlab.odoo.client.odoo_crm.feign.crm_activity_reportFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[crm_activity_report] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class crm_activity_reportClientServiceImpl implements Icrm_activity_reportClientService {

    crm_activity_reportFeignClient crm_activity_reportFeignClient;

    @Autowired
    public crm_activity_reportClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_crmClientProperties odoo_crmClientProperties) {
        if (odoo_crmClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.crm_activity_reportFeignClient = nameBuilder.target(crm_activity_reportFeignClient.class,"http://"+odoo_crmClientProperties.getServiceId()+"/") ;
		}else if (odoo_crmClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.crm_activity_reportFeignClient = nameBuilder.target(crm_activity_reportFeignClient.class,odoo_crmClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Icrm_activity_report createModel() {
		return new crm_activity_reportImpl();
	}


    public void removeBatch(List<Icrm_activity_report> crm_activity_reports){
        if(crm_activity_reports!=null){
            List<crm_activity_reportImpl> list = new ArrayList<crm_activity_reportImpl>();
            for(Icrm_activity_report icrm_activity_report :crm_activity_reports){
                list.add((crm_activity_reportImpl)icrm_activity_report) ;
            }
            crm_activity_reportFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Icrm_activity_report> crm_activity_reports){
        if(crm_activity_reports!=null){
            List<crm_activity_reportImpl> list = new ArrayList<crm_activity_reportImpl>();
            for(Icrm_activity_report icrm_activity_report :crm_activity_reports){
                list.add((crm_activity_reportImpl)icrm_activity_report) ;
            }
            crm_activity_reportFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Icrm_activity_report> crm_activity_reports){
        if(crm_activity_reports!=null){
            List<crm_activity_reportImpl> list = new ArrayList<crm_activity_reportImpl>();
            for(Icrm_activity_report icrm_activity_report :crm_activity_reports){
                list.add((crm_activity_reportImpl)icrm_activity_report) ;
            }
            crm_activity_reportFeignClient.updateBatch(list) ;
        }
    }


    public Page<Icrm_activity_report> fetchDefault(SearchContext context){
        Page<crm_activity_reportImpl> page = this.crm_activity_reportFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Icrm_activity_report crm_activity_report){
        Icrm_activity_report clientModel = crm_activity_reportFeignClient.create((crm_activity_reportImpl)crm_activity_report) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_activity_report.getClass(), false);
        copier.copy(clientModel, crm_activity_report, null);
    }


    public void get(Icrm_activity_report crm_activity_report){
        Icrm_activity_report clientModel = crm_activity_reportFeignClient.get(crm_activity_report.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_activity_report.getClass(), false);
        copier.copy(clientModel, crm_activity_report, null);
    }


    public void update(Icrm_activity_report crm_activity_report){
        Icrm_activity_report clientModel = crm_activity_reportFeignClient.update(crm_activity_report.getId(),(crm_activity_reportImpl)crm_activity_report) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_activity_report.getClass(), false);
        copier.copy(clientModel, crm_activity_report, null);
    }


    public void remove(Icrm_activity_report crm_activity_report){
        crm_activity_reportFeignClient.remove(crm_activity_report.getId()) ;
    }


    public Page<Icrm_activity_report> select(SearchContext context){
        return null ;
    }


    public void getDraft(Icrm_activity_report crm_activity_report){
        Icrm_activity_report clientModel = crm_activity_reportFeignClient.getDraft(crm_activity_report.getId(),(crm_activity_reportImpl)crm_activity_report) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_activity_report.getClass(), false);
        copier.copy(clientModel, crm_activity_report, null);
    }



}

