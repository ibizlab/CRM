package cn.ibizlab.odoo.client.odoo_crm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icrm_lead_tag;
import cn.ibizlab.odoo.client.odoo_crm.config.odoo_crmClientProperties;
import cn.ibizlab.odoo.core.client.service.Icrm_lead_tagClientService;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_lead_tagImpl;
import cn.ibizlab.odoo.client.odoo_crm.feign.crm_lead_tagFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[crm_lead_tag] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class crm_lead_tagClientServiceImpl implements Icrm_lead_tagClientService {

    crm_lead_tagFeignClient crm_lead_tagFeignClient;

    @Autowired
    public crm_lead_tagClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_crmClientProperties odoo_crmClientProperties) {
        if (odoo_crmClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.crm_lead_tagFeignClient = nameBuilder.target(crm_lead_tagFeignClient.class,"http://"+odoo_crmClientProperties.getServiceId()+"/") ;
		}else if (odoo_crmClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.crm_lead_tagFeignClient = nameBuilder.target(crm_lead_tagFeignClient.class,odoo_crmClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Icrm_lead_tag createModel() {
		return new crm_lead_tagImpl();
	}


    public void remove(Icrm_lead_tag crm_lead_tag){
        crm_lead_tagFeignClient.remove(crm_lead_tag.getId()) ;
    }


    public void get(Icrm_lead_tag crm_lead_tag){
        Icrm_lead_tag clientModel = crm_lead_tagFeignClient.get(crm_lead_tag.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_lead_tag.getClass(), false);
        copier.copy(clientModel, crm_lead_tag, null);
    }


    public void update(Icrm_lead_tag crm_lead_tag){
        Icrm_lead_tag clientModel = crm_lead_tagFeignClient.update(crm_lead_tag.getId(),(crm_lead_tagImpl)crm_lead_tag) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_lead_tag.getClass(), false);
        copier.copy(clientModel, crm_lead_tag, null);
    }


    public void updateBatch(List<Icrm_lead_tag> crm_lead_tags){
        if(crm_lead_tags!=null){
            List<crm_lead_tagImpl> list = new ArrayList<crm_lead_tagImpl>();
            for(Icrm_lead_tag icrm_lead_tag :crm_lead_tags){
                list.add((crm_lead_tagImpl)icrm_lead_tag) ;
            }
            crm_lead_tagFeignClient.updateBatch(list) ;
        }
    }


    public void create(Icrm_lead_tag crm_lead_tag){
        Icrm_lead_tag clientModel = crm_lead_tagFeignClient.create((crm_lead_tagImpl)crm_lead_tag) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_lead_tag.getClass(), false);
        copier.copy(clientModel, crm_lead_tag, null);
    }


    public Page<Icrm_lead_tag> fetchDefault(SearchContext context){
        Page<crm_lead_tagImpl> page = this.crm_lead_tagFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void removeBatch(List<Icrm_lead_tag> crm_lead_tags){
        if(crm_lead_tags!=null){
            List<crm_lead_tagImpl> list = new ArrayList<crm_lead_tagImpl>();
            for(Icrm_lead_tag icrm_lead_tag :crm_lead_tags){
                list.add((crm_lead_tagImpl)icrm_lead_tag) ;
            }
            crm_lead_tagFeignClient.removeBatch(list) ;
        }
    }


    public void createBatch(List<Icrm_lead_tag> crm_lead_tags){
        if(crm_lead_tags!=null){
            List<crm_lead_tagImpl> list = new ArrayList<crm_lead_tagImpl>();
            for(Icrm_lead_tag icrm_lead_tag :crm_lead_tags){
                list.add((crm_lead_tagImpl)icrm_lead_tag) ;
            }
            crm_lead_tagFeignClient.createBatch(list) ;
        }
    }


    public Page<Icrm_lead_tag> select(SearchContext context){
        return null ;
    }


    public void getDraft(Icrm_lead_tag crm_lead_tag){
        Icrm_lead_tag clientModel = crm_lead_tagFeignClient.getDraft(crm_lead_tag.getId(),(crm_lead_tagImpl)crm_lead_tag) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), crm_lead_tag.getClass(), false);
        copier.copy(clientModel, crm_lead_tag, null);
    }



}

