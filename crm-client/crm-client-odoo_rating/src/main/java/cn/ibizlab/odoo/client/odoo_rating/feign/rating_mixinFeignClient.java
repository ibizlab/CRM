package cn.ibizlab.odoo.client.odoo_rating.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Irating_mixin;
import cn.ibizlab.odoo.client.odoo_rating.model.rating_mixinImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[rating_mixin] 服务对象接口
 */
public interface rating_mixinFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_rating/rating_mixins/updatebatch")
    public rating_mixinImpl updateBatch(@RequestBody List<rating_mixinImpl> rating_mixins);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_rating/rating_mixins")
    public rating_mixinImpl create(@RequestBody rating_mixinImpl rating_mixin);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_rating/rating_mixins/{id}")
    public rating_mixinImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_rating/rating_mixins/removebatch")
    public rating_mixinImpl removeBatch(@RequestBody List<rating_mixinImpl> rating_mixins);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_rating/rating_mixins/{id}")
    public rating_mixinImpl update(@PathVariable("id") Integer id,@RequestBody rating_mixinImpl rating_mixin);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_rating/rating_mixins/createbatch")
    public rating_mixinImpl createBatch(@RequestBody List<rating_mixinImpl> rating_mixins);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_rating/rating_mixins/fetchdefault")
    public Page<rating_mixinImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_rating/rating_mixins/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_rating/rating_mixins/select")
    public Page<rating_mixinImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_rating/rating_mixins/{id}/getdraft")
    public rating_mixinImpl getDraft(@PathVariable("id") Integer id,@RequestBody rating_mixinImpl rating_mixin);



}
