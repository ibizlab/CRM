package cn.ibizlab.odoo.client.odoo_rating.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Irating_mixin;
import cn.ibizlab.odoo.client.odoo_rating.config.odoo_ratingClientProperties;
import cn.ibizlab.odoo.core.client.service.Irating_mixinClientService;
import cn.ibizlab.odoo.client.odoo_rating.model.rating_mixinImpl;
import cn.ibizlab.odoo.client.odoo_rating.feign.rating_mixinFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[rating_mixin] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class rating_mixinClientServiceImpl implements Irating_mixinClientService {

    rating_mixinFeignClient rating_mixinFeignClient;

    @Autowired
    public rating_mixinClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_ratingClientProperties odoo_ratingClientProperties) {
        if (odoo_ratingClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.rating_mixinFeignClient = nameBuilder.target(rating_mixinFeignClient.class,"http://"+odoo_ratingClientProperties.getServiceId()+"/") ;
		}else if (odoo_ratingClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.rating_mixinFeignClient = nameBuilder.target(rating_mixinFeignClient.class,odoo_ratingClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Irating_mixin createModel() {
		return new rating_mixinImpl();
	}


    public void updateBatch(List<Irating_mixin> rating_mixins){
        if(rating_mixins!=null){
            List<rating_mixinImpl> list = new ArrayList<rating_mixinImpl>();
            for(Irating_mixin irating_mixin :rating_mixins){
                list.add((rating_mixinImpl)irating_mixin) ;
            }
            rating_mixinFeignClient.updateBatch(list) ;
        }
    }


    public void create(Irating_mixin rating_mixin){
        Irating_mixin clientModel = rating_mixinFeignClient.create((rating_mixinImpl)rating_mixin) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), rating_mixin.getClass(), false);
        copier.copy(clientModel, rating_mixin, null);
    }


    public void get(Irating_mixin rating_mixin){
        Irating_mixin clientModel = rating_mixinFeignClient.get(rating_mixin.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), rating_mixin.getClass(), false);
        copier.copy(clientModel, rating_mixin, null);
    }


    public void removeBatch(List<Irating_mixin> rating_mixins){
        if(rating_mixins!=null){
            List<rating_mixinImpl> list = new ArrayList<rating_mixinImpl>();
            for(Irating_mixin irating_mixin :rating_mixins){
                list.add((rating_mixinImpl)irating_mixin) ;
            }
            rating_mixinFeignClient.removeBatch(list) ;
        }
    }


    public void update(Irating_mixin rating_mixin){
        Irating_mixin clientModel = rating_mixinFeignClient.update(rating_mixin.getId(),(rating_mixinImpl)rating_mixin) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), rating_mixin.getClass(), false);
        copier.copy(clientModel, rating_mixin, null);
    }


    public void createBatch(List<Irating_mixin> rating_mixins){
        if(rating_mixins!=null){
            List<rating_mixinImpl> list = new ArrayList<rating_mixinImpl>();
            for(Irating_mixin irating_mixin :rating_mixins){
                list.add((rating_mixinImpl)irating_mixin) ;
            }
            rating_mixinFeignClient.createBatch(list) ;
        }
    }


    public Page<Irating_mixin> fetchDefault(SearchContext context){
        Page<rating_mixinImpl> page = this.rating_mixinFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Irating_mixin rating_mixin){
        rating_mixinFeignClient.remove(rating_mixin.getId()) ;
    }


    public Page<Irating_mixin> select(SearchContext context){
        return null ;
    }


    public void getDraft(Irating_mixin rating_mixin){
        Irating_mixin clientModel = rating_mixinFeignClient.getDraft(rating_mixin.getId(),(rating_mixinImpl)rating_mixin) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), rating_mixin.getClass(), false);
        copier.copy(clientModel, rating_mixin, null);
    }



}

