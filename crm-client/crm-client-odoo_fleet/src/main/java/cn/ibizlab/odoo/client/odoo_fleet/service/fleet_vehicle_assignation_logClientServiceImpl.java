package cn.ibizlab.odoo.client.odoo_fleet.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_assignation_log;
import cn.ibizlab.odoo.client.odoo_fleet.config.odoo_fleetClientProperties;
import cn.ibizlab.odoo.core.client.service.Ifleet_vehicle_assignation_logClientService;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_assignation_logImpl;
import cn.ibizlab.odoo.client.odoo_fleet.feign.fleet_vehicle_assignation_logFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[fleet_vehicle_assignation_log] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class fleet_vehicle_assignation_logClientServiceImpl implements Ifleet_vehicle_assignation_logClientService {

    fleet_vehicle_assignation_logFeignClient fleet_vehicle_assignation_logFeignClient;

    @Autowired
    public fleet_vehicle_assignation_logClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_fleetClientProperties odoo_fleetClientProperties) {
        if (odoo_fleetClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.fleet_vehicle_assignation_logFeignClient = nameBuilder.target(fleet_vehicle_assignation_logFeignClient.class,"http://"+odoo_fleetClientProperties.getServiceId()+"/") ;
		}else if (odoo_fleetClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.fleet_vehicle_assignation_logFeignClient = nameBuilder.target(fleet_vehicle_assignation_logFeignClient.class,odoo_fleetClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ifleet_vehicle_assignation_log createModel() {
		return new fleet_vehicle_assignation_logImpl();
	}


    public void removeBatch(List<Ifleet_vehicle_assignation_log> fleet_vehicle_assignation_logs){
        if(fleet_vehicle_assignation_logs!=null){
            List<fleet_vehicle_assignation_logImpl> list = new ArrayList<fleet_vehicle_assignation_logImpl>();
            for(Ifleet_vehicle_assignation_log ifleet_vehicle_assignation_log :fleet_vehicle_assignation_logs){
                list.add((fleet_vehicle_assignation_logImpl)ifleet_vehicle_assignation_log) ;
            }
            fleet_vehicle_assignation_logFeignClient.removeBatch(list) ;
        }
    }


    public void updateBatch(List<Ifleet_vehicle_assignation_log> fleet_vehicle_assignation_logs){
        if(fleet_vehicle_assignation_logs!=null){
            List<fleet_vehicle_assignation_logImpl> list = new ArrayList<fleet_vehicle_assignation_logImpl>();
            for(Ifleet_vehicle_assignation_log ifleet_vehicle_assignation_log :fleet_vehicle_assignation_logs){
                list.add((fleet_vehicle_assignation_logImpl)ifleet_vehicle_assignation_log) ;
            }
            fleet_vehicle_assignation_logFeignClient.updateBatch(list) ;
        }
    }


    public void update(Ifleet_vehicle_assignation_log fleet_vehicle_assignation_log){
        Ifleet_vehicle_assignation_log clientModel = fleet_vehicle_assignation_logFeignClient.update(fleet_vehicle_assignation_log.getId(),(fleet_vehicle_assignation_logImpl)fleet_vehicle_assignation_log) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_assignation_log.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_assignation_log, null);
    }


    public void createBatch(List<Ifleet_vehicle_assignation_log> fleet_vehicle_assignation_logs){
        if(fleet_vehicle_assignation_logs!=null){
            List<fleet_vehicle_assignation_logImpl> list = new ArrayList<fleet_vehicle_assignation_logImpl>();
            for(Ifleet_vehicle_assignation_log ifleet_vehicle_assignation_log :fleet_vehicle_assignation_logs){
                list.add((fleet_vehicle_assignation_logImpl)ifleet_vehicle_assignation_log) ;
            }
            fleet_vehicle_assignation_logFeignClient.createBatch(list) ;
        }
    }


    public void get(Ifleet_vehicle_assignation_log fleet_vehicle_assignation_log){
        Ifleet_vehicle_assignation_log clientModel = fleet_vehicle_assignation_logFeignClient.get(fleet_vehicle_assignation_log.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_assignation_log.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_assignation_log, null);
    }


    public void create(Ifleet_vehicle_assignation_log fleet_vehicle_assignation_log){
        Ifleet_vehicle_assignation_log clientModel = fleet_vehicle_assignation_logFeignClient.create((fleet_vehicle_assignation_logImpl)fleet_vehicle_assignation_log) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_assignation_log.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_assignation_log, null);
    }


    public Page<Ifleet_vehicle_assignation_log> fetchDefault(SearchContext context){
        Page<fleet_vehicle_assignation_logImpl> page = this.fleet_vehicle_assignation_logFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void remove(Ifleet_vehicle_assignation_log fleet_vehicle_assignation_log){
        fleet_vehicle_assignation_logFeignClient.remove(fleet_vehicle_assignation_log.getId()) ;
    }


    public Page<Ifleet_vehicle_assignation_log> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ifleet_vehicle_assignation_log fleet_vehicle_assignation_log){
        Ifleet_vehicle_assignation_log clientModel = fleet_vehicle_assignation_logFeignClient.getDraft(fleet_vehicle_assignation_log.getId(),(fleet_vehicle_assignation_logImpl)fleet_vehicle_assignation_log) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_assignation_log.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_assignation_log, null);
    }



}

