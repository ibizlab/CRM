package cn.ibizlab.odoo.client.odoo_fleet.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_log_contract;
import cn.ibizlab.odoo.client.odoo_fleet.config.odoo_fleetClientProperties;
import cn.ibizlab.odoo.core.client.service.Ifleet_vehicle_log_contractClientService;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_log_contractImpl;
import cn.ibizlab.odoo.client.odoo_fleet.feign.fleet_vehicle_log_contractFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[fleet_vehicle_log_contract] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class fleet_vehicle_log_contractClientServiceImpl implements Ifleet_vehicle_log_contractClientService {

    fleet_vehicle_log_contractFeignClient fleet_vehicle_log_contractFeignClient;

    @Autowired
    public fleet_vehicle_log_contractClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_fleetClientProperties odoo_fleetClientProperties) {
        if (odoo_fleetClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.fleet_vehicle_log_contractFeignClient = nameBuilder.target(fleet_vehicle_log_contractFeignClient.class,"http://"+odoo_fleetClientProperties.getServiceId()+"/") ;
		}else if (odoo_fleetClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.fleet_vehicle_log_contractFeignClient = nameBuilder.target(fleet_vehicle_log_contractFeignClient.class,odoo_fleetClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ifleet_vehicle_log_contract createModel() {
		return new fleet_vehicle_log_contractImpl();
	}


    public void remove(Ifleet_vehicle_log_contract fleet_vehicle_log_contract){
        fleet_vehicle_log_contractFeignClient.remove(fleet_vehicle_log_contract.getId()) ;
    }


    public Page<Ifleet_vehicle_log_contract> fetchDefault(SearchContext context){
        Page<fleet_vehicle_log_contractImpl> page = this.fleet_vehicle_log_contractFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void update(Ifleet_vehicle_log_contract fleet_vehicle_log_contract){
        Ifleet_vehicle_log_contract clientModel = fleet_vehicle_log_contractFeignClient.update(fleet_vehicle_log_contract.getId(),(fleet_vehicle_log_contractImpl)fleet_vehicle_log_contract) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_log_contract.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_log_contract, null);
    }


    public void get(Ifleet_vehicle_log_contract fleet_vehicle_log_contract){
        Ifleet_vehicle_log_contract clientModel = fleet_vehicle_log_contractFeignClient.get(fleet_vehicle_log_contract.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_log_contract.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_log_contract, null);
    }


    public void updateBatch(List<Ifleet_vehicle_log_contract> fleet_vehicle_log_contracts){
        if(fleet_vehicle_log_contracts!=null){
            List<fleet_vehicle_log_contractImpl> list = new ArrayList<fleet_vehicle_log_contractImpl>();
            for(Ifleet_vehicle_log_contract ifleet_vehicle_log_contract :fleet_vehicle_log_contracts){
                list.add((fleet_vehicle_log_contractImpl)ifleet_vehicle_log_contract) ;
            }
            fleet_vehicle_log_contractFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Ifleet_vehicle_log_contract> fleet_vehicle_log_contracts){
        if(fleet_vehicle_log_contracts!=null){
            List<fleet_vehicle_log_contractImpl> list = new ArrayList<fleet_vehicle_log_contractImpl>();
            for(Ifleet_vehicle_log_contract ifleet_vehicle_log_contract :fleet_vehicle_log_contracts){
                list.add((fleet_vehicle_log_contractImpl)ifleet_vehicle_log_contract) ;
            }
            fleet_vehicle_log_contractFeignClient.createBatch(list) ;
        }
    }


    public void create(Ifleet_vehicle_log_contract fleet_vehicle_log_contract){
        Ifleet_vehicle_log_contract clientModel = fleet_vehicle_log_contractFeignClient.create((fleet_vehicle_log_contractImpl)fleet_vehicle_log_contract) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_log_contract.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_log_contract, null);
    }


    public void removeBatch(List<Ifleet_vehicle_log_contract> fleet_vehicle_log_contracts){
        if(fleet_vehicle_log_contracts!=null){
            List<fleet_vehicle_log_contractImpl> list = new ArrayList<fleet_vehicle_log_contractImpl>();
            for(Ifleet_vehicle_log_contract ifleet_vehicle_log_contract :fleet_vehicle_log_contracts){
                list.add((fleet_vehicle_log_contractImpl)ifleet_vehicle_log_contract) ;
            }
            fleet_vehicle_log_contractFeignClient.removeBatch(list) ;
        }
    }


    public Page<Ifleet_vehicle_log_contract> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ifleet_vehicle_log_contract fleet_vehicle_log_contract){
        Ifleet_vehicle_log_contract clientModel = fleet_vehicle_log_contractFeignClient.getDraft(fleet_vehicle_log_contract.getId(),(fleet_vehicle_log_contractImpl)fleet_vehicle_log_contract) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_log_contract.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_log_contract, null);
    }



}

