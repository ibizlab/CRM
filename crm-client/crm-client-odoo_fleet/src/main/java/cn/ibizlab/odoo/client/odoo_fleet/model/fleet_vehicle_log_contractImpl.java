package cn.ibizlab.odoo.client.odoo_fleet.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_log_contract;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 接口实体[fleet_vehicle_log_contract] 对象
 */
public class fleet_vehicle_log_contractImpl implements Ifleet_vehicle_log_contract,Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 下一活动截止日期
     */
    public Timestamp activity_date_deadline;

    @JsonIgnore
    public boolean activity_date_deadlineDirtyFlag;
    
    /**
     * 活动
     */
    public String activity_ids;

    @JsonIgnore
    public boolean activity_idsDirtyFlag;
    
    /**
     * 活动状态
     */
    public String activity_state;

    @JsonIgnore
    public boolean activity_stateDirtyFlag;
    
    /**
     * 下一活动摘要
     */
    public String activity_summary;

    @JsonIgnore
    public boolean activity_summaryDirtyFlag;
    
    /**
     * 下一活动类型
     */
    public Integer activity_type_id;

    @JsonIgnore
    public boolean activity_type_idDirtyFlag;
    
    /**
     * 责任用户
     */
    public Integer activity_user_id;

    @JsonIgnore
    public boolean activity_user_idDirtyFlag;
    
    /**
     * 总价
     */
    public Double amount;

    @JsonIgnore
    public boolean amountDirtyFlag;
    
    /**
     * 自动生成
     */
    public String auto_generated;

    @JsonIgnore
    public boolean auto_generatedDirtyFlag;
    
    /**
     * 合同
     */
    public Integer contract_id;

    @JsonIgnore
    public boolean contract_idDirtyFlag;
    
    /**
     * 总额
     */
    public Double cost_amount;

    @JsonIgnore
    public boolean cost_amountDirtyFlag;
    
    /**
     * 经常成本频率
     */
    public String cost_frequency;

    @JsonIgnore
    public boolean cost_frequencyDirtyFlag;
    
    /**
     * 经常成本数量
     */
    public Double cost_generated;

    @JsonIgnore
    public boolean cost_generatedDirtyFlag;
    
    /**
     * 成本
     */
    public Integer cost_id;

    @JsonIgnore
    public boolean cost_idDirtyFlag;
    
    /**
     * 包括服务
     */
    public String cost_ids;

    @JsonIgnore
    public boolean cost_idsDirtyFlag;
    
    /**
     * 成本
     */
    public String cost_id_text;

    @JsonIgnore
    public boolean cost_id_textDirtyFlag;
    
    /**
     * 类型
     */
    public Integer cost_subtype_id;

    @JsonIgnore
    public boolean cost_subtype_idDirtyFlag;
    
    /**
     * 费用所属类别
     */
    public String cost_type;

    @JsonIgnore
    public boolean cost_typeDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 日期
     */
    public Timestamp date;

    @JsonIgnore
    public boolean dateDirtyFlag;
    
    /**
     * 警告日期
     */
    public Integer days_left;

    @JsonIgnore
    public boolean days_leftDirtyFlag;
    
    /**
     * 成本说明
     */
    public String description;

    @JsonIgnore
    public boolean descriptionDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 合同到期日期
     */
    public Timestamp expiration_date;

    @JsonIgnore
    public boolean expiration_dateDirtyFlag;
    
    /**
     * 已发生费用
     */
    public String generated_cost_ids;

    @JsonIgnore
    public boolean generated_cost_idsDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 供应商
     */
    public Integer insurer_id;

    @JsonIgnore
    public boolean insurer_idDirtyFlag;
    
    /**
     * 供应商
     */
    public String insurer_id_text;

    @JsonIgnore
    public boolean insurer_id_textDirtyFlag;
    
    /**
     * 合同参考
     */
    public String ins_ref;

    @JsonIgnore
    public boolean ins_refDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 错误数
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 消息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 需要激活
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 行动数量
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 条款和条件
     */
    public String notes;

    @JsonIgnore
    public boolean notesDirtyFlag;
    
    /**
     * 创建时的里程表
     */
    public Double odometer;

    @JsonIgnore
    public boolean odometerDirtyFlag;
    
    /**
     * 里程表
     */
    public Integer odometer_id;

    @JsonIgnore
    public boolean odometer_idDirtyFlag;
    
    /**
     * 单位
     */
    public String odometer_unit;

    @JsonIgnore
    public boolean odometer_unitDirtyFlag;
    
    /**
     * 上级
     */
    public Integer parent_id;

    @JsonIgnore
    public boolean parent_idDirtyFlag;
    
    /**
     * 驾驶员
     */
    public Integer purchaser_id;

    @JsonIgnore
    public boolean purchaser_idDirtyFlag;
    
    /**
     * 驾驶员
     */
    public String purchaser_id_text;

    @JsonIgnore
    public boolean purchaser_id_textDirtyFlag;
    
    /**
     * 合同开始日期
     */
    public Timestamp start_date;

    @JsonIgnore
    public boolean start_dateDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 指标性成本总计
     */
    public Double sum_cost;

    @JsonIgnore
    public boolean sum_costDirtyFlag;
    
    /**
     * 负责
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 负责
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 车辆
     */
    public Integer vehicle_id;

    @JsonIgnore
    public boolean vehicle_idDirtyFlag;
    
    /**
     * 网站消息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return this.activity_date_deadline ;
    }

    /**
     * 设置 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动截止日期]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return this.activity_date_deadlineDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return this.activity_ids ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return this.activity_idsDirtyFlag ;
    }   

    /**
     * 获取 [活动状态]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return this.activity_state ;
    }

    /**
     * 设置 [活动状态]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

     /**
     * 获取 [活动状态]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return this.activity_stateDirtyFlag ;
    }   

    /**
     * 获取 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return this.activity_summary ;
    }

    /**
     * 设置 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动摘要]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return this.activity_summaryDirtyFlag ;
    }   

    /**
     * 获取 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return this.activity_type_id ;
    }

    /**
     * 设置 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动类型]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return this.activity_type_idDirtyFlag ;
    }   

    /**
     * 获取 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return this.activity_user_id ;
    }

    /**
     * 设置 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [责任用户]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return this.activity_user_idDirtyFlag ;
    }   

    /**
     * 获取 [总价]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return this.amount ;
    }

    /**
     * 设置 [总价]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

     /**
     * 获取 [总价]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return this.amountDirtyFlag ;
    }   

    /**
     * 获取 [自动生成]
     */
    @JsonProperty("auto_generated")
    public String getAuto_generated(){
        return this.auto_generated ;
    }

    /**
     * 设置 [自动生成]
     */
    @JsonProperty("auto_generated")
    public void setAuto_generated(String  auto_generated){
        this.auto_generated = auto_generated ;
        this.auto_generatedDirtyFlag = true ;
    }

     /**
     * 获取 [自动生成]脏标记
     */
    @JsonIgnore
    public boolean getAuto_generatedDirtyFlag(){
        return this.auto_generatedDirtyFlag ;
    }   

    /**
     * 获取 [合同]
     */
    @JsonProperty("contract_id")
    public Integer getContract_id(){
        return this.contract_id ;
    }

    /**
     * 设置 [合同]
     */
    @JsonProperty("contract_id")
    public void setContract_id(Integer  contract_id){
        this.contract_id = contract_id ;
        this.contract_idDirtyFlag = true ;
    }

     /**
     * 获取 [合同]脏标记
     */
    @JsonIgnore
    public boolean getContract_idDirtyFlag(){
        return this.contract_idDirtyFlag ;
    }   

    /**
     * 获取 [总额]
     */
    @JsonProperty("cost_amount")
    public Double getCost_amount(){
        return this.cost_amount ;
    }

    /**
     * 设置 [总额]
     */
    @JsonProperty("cost_amount")
    public void setCost_amount(Double  cost_amount){
        this.cost_amount = cost_amount ;
        this.cost_amountDirtyFlag = true ;
    }

     /**
     * 获取 [总额]脏标记
     */
    @JsonIgnore
    public boolean getCost_amountDirtyFlag(){
        return this.cost_amountDirtyFlag ;
    }   

    /**
     * 获取 [经常成本频率]
     */
    @JsonProperty("cost_frequency")
    public String getCost_frequency(){
        return this.cost_frequency ;
    }

    /**
     * 设置 [经常成本频率]
     */
    @JsonProperty("cost_frequency")
    public void setCost_frequency(String  cost_frequency){
        this.cost_frequency = cost_frequency ;
        this.cost_frequencyDirtyFlag = true ;
    }

     /**
     * 获取 [经常成本频率]脏标记
     */
    @JsonIgnore
    public boolean getCost_frequencyDirtyFlag(){
        return this.cost_frequencyDirtyFlag ;
    }   

    /**
     * 获取 [经常成本数量]
     */
    @JsonProperty("cost_generated")
    public Double getCost_generated(){
        return this.cost_generated ;
    }

    /**
     * 设置 [经常成本数量]
     */
    @JsonProperty("cost_generated")
    public void setCost_generated(Double  cost_generated){
        this.cost_generated = cost_generated ;
        this.cost_generatedDirtyFlag = true ;
    }

     /**
     * 获取 [经常成本数量]脏标记
     */
    @JsonIgnore
    public boolean getCost_generatedDirtyFlag(){
        return this.cost_generatedDirtyFlag ;
    }   

    /**
     * 获取 [成本]
     */
    @JsonProperty("cost_id")
    public Integer getCost_id(){
        return this.cost_id ;
    }

    /**
     * 设置 [成本]
     */
    @JsonProperty("cost_id")
    public void setCost_id(Integer  cost_id){
        this.cost_id = cost_id ;
        this.cost_idDirtyFlag = true ;
    }

     /**
     * 获取 [成本]脏标记
     */
    @JsonIgnore
    public boolean getCost_idDirtyFlag(){
        return this.cost_idDirtyFlag ;
    }   

    /**
     * 获取 [包括服务]
     */
    @JsonProperty("cost_ids")
    public String getCost_ids(){
        return this.cost_ids ;
    }

    /**
     * 设置 [包括服务]
     */
    @JsonProperty("cost_ids")
    public void setCost_ids(String  cost_ids){
        this.cost_ids = cost_ids ;
        this.cost_idsDirtyFlag = true ;
    }

     /**
     * 获取 [包括服务]脏标记
     */
    @JsonIgnore
    public boolean getCost_idsDirtyFlag(){
        return this.cost_idsDirtyFlag ;
    }   

    /**
     * 获取 [成本]
     */
    @JsonProperty("cost_id_text")
    public String getCost_id_text(){
        return this.cost_id_text ;
    }

    /**
     * 设置 [成本]
     */
    @JsonProperty("cost_id_text")
    public void setCost_id_text(String  cost_id_text){
        this.cost_id_text = cost_id_text ;
        this.cost_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [成本]脏标记
     */
    @JsonIgnore
    public boolean getCost_id_textDirtyFlag(){
        return this.cost_id_textDirtyFlag ;
    }   

    /**
     * 获取 [类型]
     */
    @JsonProperty("cost_subtype_id")
    public Integer getCost_subtype_id(){
        return this.cost_subtype_id ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("cost_subtype_id")
    public void setCost_subtype_id(Integer  cost_subtype_id){
        this.cost_subtype_id = cost_subtype_id ;
        this.cost_subtype_idDirtyFlag = true ;
    }

     /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getCost_subtype_idDirtyFlag(){
        return this.cost_subtype_idDirtyFlag ;
    }   

    /**
     * 获取 [费用所属类别]
     */
    @JsonProperty("cost_type")
    public String getCost_type(){
        return this.cost_type ;
    }

    /**
     * 设置 [费用所属类别]
     */
    @JsonProperty("cost_type")
    public void setCost_type(String  cost_type){
        this.cost_type = cost_type ;
        this.cost_typeDirtyFlag = true ;
    }

     /**
     * 获取 [费用所属类别]脏标记
     */
    @JsonIgnore
    public boolean getCost_typeDirtyFlag(){
        return this.cost_typeDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

     /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }   

    /**
     * 获取 [警告日期]
     */
    @JsonProperty("days_left")
    public Integer getDays_left(){
        return this.days_left ;
    }

    /**
     * 设置 [警告日期]
     */
    @JsonProperty("days_left")
    public void setDays_left(Integer  days_left){
        this.days_left = days_left ;
        this.days_leftDirtyFlag = true ;
    }

     /**
     * 获取 [警告日期]脏标记
     */
    @JsonIgnore
    public boolean getDays_leftDirtyFlag(){
        return this.days_leftDirtyFlag ;
    }   

    /**
     * 获取 [成本说明]
     */
    @JsonProperty("description")
    public String getDescription(){
        return this.description ;
    }

    /**
     * 设置 [成本说明]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [成本说明]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return this.descriptionDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [合同到期日期]
     */
    @JsonProperty("expiration_date")
    public Timestamp getExpiration_date(){
        return this.expiration_date ;
    }

    /**
     * 设置 [合同到期日期]
     */
    @JsonProperty("expiration_date")
    public void setExpiration_date(Timestamp  expiration_date){
        this.expiration_date = expiration_date ;
        this.expiration_dateDirtyFlag = true ;
    }

     /**
     * 获取 [合同到期日期]脏标记
     */
    @JsonIgnore
    public boolean getExpiration_dateDirtyFlag(){
        return this.expiration_dateDirtyFlag ;
    }   

    /**
     * 获取 [已发生费用]
     */
    @JsonProperty("generated_cost_ids")
    public String getGenerated_cost_ids(){
        return this.generated_cost_ids ;
    }

    /**
     * 设置 [已发生费用]
     */
    @JsonProperty("generated_cost_ids")
    public void setGenerated_cost_ids(String  generated_cost_ids){
        this.generated_cost_ids = generated_cost_ids ;
        this.generated_cost_idsDirtyFlag = true ;
    }

     /**
     * 获取 [已发生费用]脏标记
     */
    @JsonIgnore
    public boolean getGenerated_cost_idsDirtyFlag(){
        return this.generated_cost_idsDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [供应商]
     */
    @JsonProperty("insurer_id")
    public Integer getInsurer_id(){
        return this.insurer_id ;
    }

    /**
     * 设置 [供应商]
     */
    @JsonProperty("insurer_id")
    public void setInsurer_id(Integer  insurer_id){
        this.insurer_id = insurer_id ;
        this.insurer_idDirtyFlag = true ;
    }

     /**
     * 获取 [供应商]脏标记
     */
    @JsonIgnore
    public boolean getInsurer_idDirtyFlag(){
        return this.insurer_idDirtyFlag ;
    }   

    /**
     * 获取 [供应商]
     */
    @JsonProperty("insurer_id_text")
    public String getInsurer_id_text(){
        return this.insurer_id_text ;
    }

    /**
     * 设置 [供应商]
     */
    @JsonProperty("insurer_id_text")
    public void setInsurer_id_text(String  insurer_id_text){
        this.insurer_id_text = insurer_id_text ;
        this.insurer_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [供应商]脏标记
     */
    @JsonIgnore
    public boolean getInsurer_id_textDirtyFlag(){
        return this.insurer_id_textDirtyFlag ;
    }   

    /**
     * 获取 [合同参考]
     */
    @JsonProperty("ins_ref")
    public String getIns_ref(){
        return this.ins_ref ;
    }

    /**
     * 设置 [合同参考]
     */
    @JsonProperty("ins_ref")
    public void setIns_ref(String  ins_ref){
        this.ins_ref = ins_ref ;
        this.ins_refDirtyFlag = true ;
    }

     /**
     * 获取 [合同参考]脏标记
     */
    @JsonIgnore
    public boolean getIns_refDirtyFlag(){
        return this.ins_refDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [错误数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [需要激活]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要激活]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [需要激活]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [行动数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [条款和条件]
     */
    @JsonProperty("notes")
    public String getNotes(){
        return this.notes ;
    }

    /**
     * 设置 [条款和条件]
     */
    @JsonProperty("notes")
    public void setNotes(String  notes){
        this.notes = notes ;
        this.notesDirtyFlag = true ;
    }

     /**
     * 获取 [条款和条件]脏标记
     */
    @JsonIgnore
    public boolean getNotesDirtyFlag(){
        return this.notesDirtyFlag ;
    }   

    /**
     * 获取 [创建时的里程表]
     */
    @JsonProperty("odometer")
    public Double getOdometer(){
        return this.odometer ;
    }

    /**
     * 设置 [创建时的里程表]
     */
    @JsonProperty("odometer")
    public void setOdometer(Double  odometer){
        this.odometer = odometer ;
        this.odometerDirtyFlag = true ;
    }

     /**
     * 获取 [创建时的里程表]脏标记
     */
    @JsonIgnore
    public boolean getOdometerDirtyFlag(){
        return this.odometerDirtyFlag ;
    }   

    /**
     * 获取 [里程表]
     */
    @JsonProperty("odometer_id")
    public Integer getOdometer_id(){
        return this.odometer_id ;
    }

    /**
     * 设置 [里程表]
     */
    @JsonProperty("odometer_id")
    public void setOdometer_id(Integer  odometer_id){
        this.odometer_id = odometer_id ;
        this.odometer_idDirtyFlag = true ;
    }

     /**
     * 获取 [里程表]脏标记
     */
    @JsonIgnore
    public boolean getOdometer_idDirtyFlag(){
        return this.odometer_idDirtyFlag ;
    }   

    /**
     * 获取 [单位]
     */
    @JsonProperty("odometer_unit")
    public String getOdometer_unit(){
        return this.odometer_unit ;
    }

    /**
     * 设置 [单位]
     */
    @JsonProperty("odometer_unit")
    public void setOdometer_unit(String  odometer_unit){
        this.odometer_unit = odometer_unit ;
        this.odometer_unitDirtyFlag = true ;
    }

     /**
     * 获取 [单位]脏标记
     */
    @JsonIgnore
    public boolean getOdometer_unitDirtyFlag(){
        return this.odometer_unitDirtyFlag ;
    }   

    /**
     * 获取 [上级]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return this.parent_id ;
    }

    /**
     * 设置 [上级]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

     /**
     * 获取 [上级]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return this.parent_idDirtyFlag ;
    }   

    /**
     * 获取 [驾驶员]
     */
    @JsonProperty("purchaser_id")
    public Integer getPurchaser_id(){
        return this.purchaser_id ;
    }

    /**
     * 设置 [驾驶员]
     */
    @JsonProperty("purchaser_id")
    public void setPurchaser_id(Integer  purchaser_id){
        this.purchaser_id = purchaser_id ;
        this.purchaser_idDirtyFlag = true ;
    }

     /**
     * 获取 [驾驶员]脏标记
     */
    @JsonIgnore
    public boolean getPurchaser_idDirtyFlag(){
        return this.purchaser_idDirtyFlag ;
    }   

    /**
     * 获取 [驾驶员]
     */
    @JsonProperty("purchaser_id_text")
    public String getPurchaser_id_text(){
        return this.purchaser_id_text ;
    }

    /**
     * 设置 [驾驶员]
     */
    @JsonProperty("purchaser_id_text")
    public void setPurchaser_id_text(String  purchaser_id_text){
        this.purchaser_id_text = purchaser_id_text ;
        this.purchaser_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [驾驶员]脏标记
     */
    @JsonIgnore
    public boolean getPurchaser_id_textDirtyFlag(){
        return this.purchaser_id_textDirtyFlag ;
    }   

    /**
     * 获取 [合同开始日期]
     */
    @JsonProperty("start_date")
    public Timestamp getStart_date(){
        return this.start_date ;
    }

    /**
     * 设置 [合同开始日期]
     */
    @JsonProperty("start_date")
    public void setStart_date(Timestamp  start_date){
        this.start_date = start_date ;
        this.start_dateDirtyFlag = true ;
    }

     /**
     * 获取 [合同开始日期]脏标记
     */
    @JsonIgnore
    public boolean getStart_dateDirtyFlag(){
        return this.start_dateDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [指标性成本总计]
     */
    @JsonProperty("sum_cost")
    public Double getSum_cost(){
        return this.sum_cost ;
    }

    /**
     * 设置 [指标性成本总计]
     */
    @JsonProperty("sum_cost")
    public void setSum_cost(Double  sum_cost){
        this.sum_cost = sum_cost ;
        this.sum_costDirtyFlag = true ;
    }

     /**
     * 获取 [指标性成本总计]脏标记
     */
    @JsonIgnore
    public boolean getSum_costDirtyFlag(){
        return this.sum_costDirtyFlag ;
    }   

    /**
     * 获取 [负责]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [负责]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [负责]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [负责]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [负责]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [负责]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [车辆]
     */
    @JsonProperty("vehicle_id")
    public Integer getVehicle_id(){
        return this.vehicle_id ;
    }

    /**
     * 设置 [车辆]
     */
    @JsonProperty("vehicle_id")
    public void setVehicle_id(Integer  vehicle_id){
        this.vehicle_id = vehicle_id ;
        this.vehicle_idDirtyFlag = true ;
    }

     /**
     * 获取 [车辆]脏标记
     */
    @JsonIgnore
    public boolean getVehicle_idDirtyFlag(){
        return this.vehicle_idDirtyFlag ;
    }   

    /**
     * 获取 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站消息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   

}
