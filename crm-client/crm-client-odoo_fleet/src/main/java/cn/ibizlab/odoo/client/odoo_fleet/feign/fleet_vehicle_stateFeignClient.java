package cn.ibizlab.odoo.client.odoo_fleet.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_state;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_stateImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[fleet_vehicle_state] 服务对象接口
 */
public interface fleet_vehicle_stateFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_states/createbatch")
    public fleet_vehicle_stateImpl createBatch(@RequestBody List<fleet_vehicle_stateImpl> fleet_vehicle_states);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_states")
    public fleet_vehicle_stateImpl create(@RequestBody fleet_vehicle_stateImpl fleet_vehicle_state);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_states/updatebatch")
    public fleet_vehicle_stateImpl updateBatch(@RequestBody List<fleet_vehicle_stateImpl> fleet_vehicle_states);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_states/removebatch")
    public fleet_vehicle_stateImpl removeBatch(@RequestBody List<fleet_vehicle_stateImpl> fleet_vehicle_states);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_states/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_states/fetchdefault")
    public Page<fleet_vehicle_stateImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_states/{id}")
    public fleet_vehicle_stateImpl update(@PathVariable("id") Integer id,@RequestBody fleet_vehicle_stateImpl fleet_vehicle_state);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_states/{id}")
    public fleet_vehicle_stateImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_states/select")
    public Page<fleet_vehicle_stateImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_states/{id}/getdraft")
    public fleet_vehicle_stateImpl getDraft(@PathVariable("id") Integer id,@RequestBody fleet_vehicle_stateImpl fleet_vehicle_state);



}
