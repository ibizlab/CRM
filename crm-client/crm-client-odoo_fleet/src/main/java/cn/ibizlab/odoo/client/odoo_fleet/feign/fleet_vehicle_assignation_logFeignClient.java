package cn.ibizlab.odoo.client.odoo_fleet.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_assignation_log;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_assignation_logImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[fleet_vehicle_assignation_log] 服务对象接口
 */
public interface fleet_vehicle_assignation_logFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_assignation_logs/removebatch")
    public fleet_vehicle_assignation_logImpl removeBatch(@RequestBody List<fleet_vehicle_assignation_logImpl> fleet_vehicle_assignation_logs);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_assignation_logs/updatebatch")
    public fleet_vehicle_assignation_logImpl updateBatch(@RequestBody List<fleet_vehicle_assignation_logImpl> fleet_vehicle_assignation_logs);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_assignation_logs/{id}")
    public fleet_vehicle_assignation_logImpl update(@PathVariable("id") Integer id,@RequestBody fleet_vehicle_assignation_logImpl fleet_vehicle_assignation_log);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_assignation_logs/createbatch")
    public fleet_vehicle_assignation_logImpl createBatch(@RequestBody List<fleet_vehicle_assignation_logImpl> fleet_vehicle_assignation_logs);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_assignation_logs/{id}")
    public fleet_vehicle_assignation_logImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_assignation_logs")
    public fleet_vehicle_assignation_logImpl create(@RequestBody fleet_vehicle_assignation_logImpl fleet_vehicle_assignation_log);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_assignation_logs/fetchdefault")
    public Page<fleet_vehicle_assignation_logImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_assignation_logs/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_assignation_logs/select")
    public Page<fleet_vehicle_assignation_logImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_assignation_logs/{id}/getdraft")
    public fleet_vehicle_assignation_logImpl getDraft(@PathVariable("id") Integer id,@RequestBody fleet_vehicle_assignation_logImpl fleet_vehicle_assignation_log);



}
