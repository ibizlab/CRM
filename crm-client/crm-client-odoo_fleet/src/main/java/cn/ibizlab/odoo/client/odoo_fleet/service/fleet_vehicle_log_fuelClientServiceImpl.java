package cn.ibizlab.odoo.client.odoo_fleet.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_log_fuel;
import cn.ibizlab.odoo.client.odoo_fleet.config.odoo_fleetClientProperties;
import cn.ibizlab.odoo.core.client.service.Ifleet_vehicle_log_fuelClientService;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_log_fuelImpl;
import cn.ibizlab.odoo.client.odoo_fleet.feign.fleet_vehicle_log_fuelFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[fleet_vehicle_log_fuel] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class fleet_vehicle_log_fuelClientServiceImpl implements Ifleet_vehicle_log_fuelClientService {

    fleet_vehicle_log_fuelFeignClient fleet_vehicle_log_fuelFeignClient;

    @Autowired
    public fleet_vehicle_log_fuelClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_fleetClientProperties odoo_fleetClientProperties) {
        if (odoo_fleetClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.fleet_vehicle_log_fuelFeignClient = nameBuilder.target(fleet_vehicle_log_fuelFeignClient.class,"http://"+odoo_fleetClientProperties.getServiceId()+"/") ;
		}else if (odoo_fleetClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.fleet_vehicle_log_fuelFeignClient = nameBuilder.target(fleet_vehicle_log_fuelFeignClient.class,odoo_fleetClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ifleet_vehicle_log_fuel createModel() {
		return new fleet_vehicle_log_fuelImpl();
	}


    public void removeBatch(List<Ifleet_vehicle_log_fuel> fleet_vehicle_log_fuels){
        if(fleet_vehicle_log_fuels!=null){
            List<fleet_vehicle_log_fuelImpl> list = new ArrayList<fleet_vehicle_log_fuelImpl>();
            for(Ifleet_vehicle_log_fuel ifleet_vehicle_log_fuel :fleet_vehicle_log_fuels){
                list.add((fleet_vehicle_log_fuelImpl)ifleet_vehicle_log_fuel) ;
            }
            fleet_vehicle_log_fuelFeignClient.removeBatch(list) ;
        }
    }


    public void update(Ifleet_vehicle_log_fuel fleet_vehicle_log_fuel){
        Ifleet_vehicle_log_fuel clientModel = fleet_vehicle_log_fuelFeignClient.update(fleet_vehicle_log_fuel.getId(),(fleet_vehicle_log_fuelImpl)fleet_vehicle_log_fuel) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_log_fuel.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_log_fuel, null);
    }


    public void create(Ifleet_vehicle_log_fuel fleet_vehicle_log_fuel){
        Ifleet_vehicle_log_fuel clientModel = fleet_vehicle_log_fuelFeignClient.create((fleet_vehicle_log_fuelImpl)fleet_vehicle_log_fuel) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_log_fuel.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_log_fuel, null);
    }


    public void updateBatch(List<Ifleet_vehicle_log_fuel> fleet_vehicle_log_fuels){
        if(fleet_vehicle_log_fuels!=null){
            List<fleet_vehicle_log_fuelImpl> list = new ArrayList<fleet_vehicle_log_fuelImpl>();
            for(Ifleet_vehicle_log_fuel ifleet_vehicle_log_fuel :fleet_vehicle_log_fuels){
                list.add((fleet_vehicle_log_fuelImpl)ifleet_vehicle_log_fuel) ;
            }
            fleet_vehicle_log_fuelFeignClient.updateBatch(list) ;
        }
    }


    public void remove(Ifleet_vehicle_log_fuel fleet_vehicle_log_fuel){
        fleet_vehicle_log_fuelFeignClient.remove(fleet_vehicle_log_fuel.getId()) ;
    }


    public void get(Ifleet_vehicle_log_fuel fleet_vehicle_log_fuel){
        Ifleet_vehicle_log_fuel clientModel = fleet_vehicle_log_fuelFeignClient.get(fleet_vehicle_log_fuel.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_log_fuel.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_log_fuel, null);
    }


    public void createBatch(List<Ifleet_vehicle_log_fuel> fleet_vehicle_log_fuels){
        if(fleet_vehicle_log_fuels!=null){
            List<fleet_vehicle_log_fuelImpl> list = new ArrayList<fleet_vehicle_log_fuelImpl>();
            for(Ifleet_vehicle_log_fuel ifleet_vehicle_log_fuel :fleet_vehicle_log_fuels){
                list.add((fleet_vehicle_log_fuelImpl)ifleet_vehicle_log_fuel) ;
            }
            fleet_vehicle_log_fuelFeignClient.createBatch(list) ;
        }
    }


    public Page<Ifleet_vehicle_log_fuel> fetchDefault(SearchContext context){
        Page<fleet_vehicle_log_fuelImpl> page = this.fleet_vehicle_log_fuelFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public Page<Ifleet_vehicle_log_fuel> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ifleet_vehicle_log_fuel fleet_vehicle_log_fuel){
        Ifleet_vehicle_log_fuel clientModel = fleet_vehicle_log_fuelFeignClient.getDraft(fleet_vehicle_log_fuel.getId(),(fleet_vehicle_log_fuelImpl)fleet_vehicle_log_fuel) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_log_fuel.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_log_fuel, null);
    }



}

