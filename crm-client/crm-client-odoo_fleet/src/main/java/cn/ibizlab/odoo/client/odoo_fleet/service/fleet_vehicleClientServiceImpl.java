package cn.ibizlab.odoo.client.odoo_fleet.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle;
import cn.ibizlab.odoo.client.odoo_fleet.config.odoo_fleetClientProperties;
import cn.ibizlab.odoo.core.client.service.Ifleet_vehicleClientService;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicleImpl;
import cn.ibizlab.odoo.client.odoo_fleet.feign.fleet_vehicleFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[fleet_vehicle] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class fleet_vehicleClientServiceImpl implements Ifleet_vehicleClientService {

    fleet_vehicleFeignClient fleet_vehicleFeignClient;

    @Autowired
    public fleet_vehicleClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_fleetClientProperties odoo_fleetClientProperties) {
        if (odoo_fleetClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.fleet_vehicleFeignClient = nameBuilder.target(fleet_vehicleFeignClient.class,"http://"+odoo_fleetClientProperties.getServiceId()+"/") ;
		}else if (odoo_fleetClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.fleet_vehicleFeignClient = nameBuilder.target(fleet_vehicleFeignClient.class,odoo_fleetClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ifleet_vehicle createModel() {
		return new fleet_vehicleImpl();
	}


    public void removeBatch(List<Ifleet_vehicle> fleet_vehicles){
        if(fleet_vehicles!=null){
            List<fleet_vehicleImpl> list = new ArrayList<fleet_vehicleImpl>();
            for(Ifleet_vehicle ifleet_vehicle :fleet_vehicles){
                list.add((fleet_vehicleImpl)ifleet_vehicle) ;
            }
            fleet_vehicleFeignClient.removeBatch(list) ;
        }
    }


    public void remove(Ifleet_vehicle fleet_vehicle){
        fleet_vehicleFeignClient.remove(fleet_vehicle.getId()) ;
    }


    public void createBatch(List<Ifleet_vehicle> fleet_vehicles){
        if(fleet_vehicles!=null){
            List<fleet_vehicleImpl> list = new ArrayList<fleet_vehicleImpl>();
            for(Ifleet_vehicle ifleet_vehicle :fleet_vehicles){
                list.add((fleet_vehicleImpl)ifleet_vehicle) ;
            }
            fleet_vehicleFeignClient.createBatch(list) ;
        }
    }


    public void updateBatch(List<Ifleet_vehicle> fleet_vehicles){
        if(fleet_vehicles!=null){
            List<fleet_vehicleImpl> list = new ArrayList<fleet_vehicleImpl>();
            for(Ifleet_vehicle ifleet_vehicle :fleet_vehicles){
                list.add((fleet_vehicleImpl)ifleet_vehicle) ;
            }
            fleet_vehicleFeignClient.updateBatch(list) ;
        }
    }


    public void update(Ifleet_vehicle fleet_vehicle){
        Ifleet_vehicle clientModel = fleet_vehicleFeignClient.update(fleet_vehicle.getId(),(fleet_vehicleImpl)fleet_vehicle) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle.getClass(), false);
        copier.copy(clientModel, fleet_vehicle, null);
    }


    public void get(Ifleet_vehicle fleet_vehicle){
        Ifleet_vehicle clientModel = fleet_vehicleFeignClient.get(fleet_vehicle.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle.getClass(), false);
        copier.copy(clientModel, fleet_vehicle, null);
    }


    public Page<Ifleet_vehicle> fetchDefault(SearchContext context){
        Page<fleet_vehicleImpl> page = this.fleet_vehicleFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void create(Ifleet_vehicle fleet_vehicle){
        Ifleet_vehicle clientModel = fleet_vehicleFeignClient.create((fleet_vehicleImpl)fleet_vehicle) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle.getClass(), false);
        copier.copy(clientModel, fleet_vehicle, null);
    }


    public Page<Ifleet_vehicle> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ifleet_vehicle fleet_vehicle){
        Ifleet_vehicle clientModel = fleet_vehicleFeignClient.getDraft(fleet_vehicle.getId(),(fleet_vehicleImpl)fleet_vehicle) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle.getClass(), false);
        copier.copy(clientModel, fleet_vehicle, null);
    }



}

