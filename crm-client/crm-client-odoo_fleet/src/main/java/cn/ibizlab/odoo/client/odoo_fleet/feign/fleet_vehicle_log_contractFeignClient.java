package cn.ibizlab.odoo.client.odoo_fleet.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_log_contract;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_log_contractImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[fleet_vehicle_log_contract] 服务对象接口
 */
public interface fleet_vehicle_log_contractFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_log_contracts/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_log_contracts/fetchdefault")
    public Page<fleet_vehicle_log_contractImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_log_contracts/{id}")
    public fleet_vehicle_log_contractImpl update(@PathVariable("id") Integer id,@RequestBody fleet_vehicle_log_contractImpl fleet_vehicle_log_contract);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_log_contracts/{id}")
    public fleet_vehicle_log_contractImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_log_contracts/updatebatch")
    public fleet_vehicle_log_contractImpl updateBatch(@RequestBody List<fleet_vehicle_log_contractImpl> fleet_vehicle_log_contracts);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_log_contracts/createbatch")
    public fleet_vehicle_log_contractImpl createBatch(@RequestBody List<fleet_vehicle_log_contractImpl> fleet_vehicle_log_contracts);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_log_contracts")
    public fleet_vehicle_log_contractImpl create(@RequestBody fleet_vehicle_log_contractImpl fleet_vehicle_log_contract);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_log_contracts/removebatch")
    public fleet_vehicle_log_contractImpl removeBatch(@RequestBody List<fleet_vehicle_log_contractImpl> fleet_vehicle_log_contracts);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_log_contracts/select")
    public Page<fleet_vehicle_log_contractImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_log_contracts/{id}/getdraft")
    public fleet_vehicle_log_contractImpl getDraft(@PathVariable("id") Integer id,@RequestBody fleet_vehicle_log_contractImpl fleet_vehicle_log_contract);



}
