package cn.ibizlab.odoo.client.odoo_fleet.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;

import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_model_brand;
import cn.ibizlab.odoo.client.odoo_fleet.config.odoo_fleetClientProperties;
import cn.ibizlab.odoo.core.client.service.Ifleet_vehicle_model_brandClientService;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_model_brandImpl;
import cn.ibizlab.odoo.client.odoo_fleet.feign.fleet_vehicle_model_brandFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.cloud.openfeign.ribbon.LoadBalancerFeignClient;
import cn.ibizlab.odoo.util.feign.FeignRequestInterceptor;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.util.feign.suport.SearchContextFeignEncode;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 实体[fleet_vehicle_model_brand] 服务对象接口
 */
@Slf4j
@IBIZLog
@Service
public class fleet_vehicle_model_brandClientServiceImpl implements Ifleet_vehicle_model_brandClientService {

    fleet_vehicle_model_brandFeignClient fleet_vehicle_model_brandFeignClient;

    @Autowired
    public fleet_vehicle_model_brandClientServiceImpl(Decoder decoder, Encoder encoder, Client client, Contract contract ,FeignRequestInterceptor feignRequestInterceptor,odoo_fleetClientProperties odoo_fleetClientProperties) {
        if (odoo_fleetClientProperties.getServiceId()!=null) {
    		Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.fleet_vehicle_model_brandFeignClient = nameBuilder.target(fleet_vehicle_model_brandFeignClient.class,"http://"+odoo_fleetClientProperties.getServiceId()+"/") ;
		}else if (odoo_fleetClientProperties.getServiceUrl()!=null) {
			if (client instanceof LoadBalancerFeignClient) {
				client = ((LoadBalancerFeignClient) client).getDelegate();
			}
			Feign.Builder nameBuilder = Feign.builder()
                    .client(client)
                    .encoder(new SearchContextFeignEncode(encoder))
                    .decoder(decoder)
                    .contract(contract)
                    .requestInterceptor(feignRequestInterceptor)
                    ;
        	this.fleet_vehicle_model_brandFeignClient = nameBuilder.target(fleet_vehicle_model_brandFeignClient.class,odoo_fleetClientProperties.getServiceUrl()) ;
		}
    }
    @Override
    public Ifleet_vehicle_model_brand createModel() {
		return new fleet_vehicle_model_brandImpl();
	}


    public void updateBatch(List<Ifleet_vehicle_model_brand> fleet_vehicle_model_brands){
        if(fleet_vehicle_model_brands!=null){
            List<fleet_vehicle_model_brandImpl> list = new ArrayList<fleet_vehicle_model_brandImpl>();
            for(Ifleet_vehicle_model_brand ifleet_vehicle_model_brand :fleet_vehicle_model_brands){
                list.add((fleet_vehicle_model_brandImpl)ifleet_vehicle_model_brand) ;
            }
            fleet_vehicle_model_brandFeignClient.updateBatch(list) ;
        }
    }


    public void createBatch(List<Ifleet_vehicle_model_brand> fleet_vehicle_model_brands){
        if(fleet_vehicle_model_brands!=null){
            List<fleet_vehicle_model_brandImpl> list = new ArrayList<fleet_vehicle_model_brandImpl>();
            for(Ifleet_vehicle_model_brand ifleet_vehicle_model_brand :fleet_vehicle_model_brands){
                list.add((fleet_vehicle_model_brandImpl)ifleet_vehicle_model_brand) ;
            }
            fleet_vehicle_model_brandFeignClient.createBatch(list) ;
        }
    }


    public void removeBatch(List<Ifleet_vehicle_model_brand> fleet_vehicle_model_brands){
        if(fleet_vehicle_model_brands!=null){
            List<fleet_vehicle_model_brandImpl> list = new ArrayList<fleet_vehicle_model_brandImpl>();
            for(Ifleet_vehicle_model_brand ifleet_vehicle_model_brand :fleet_vehicle_model_brands){
                list.add((fleet_vehicle_model_brandImpl)ifleet_vehicle_model_brand) ;
            }
            fleet_vehicle_model_brandFeignClient.removeBatch(list) ;
        }
    }


    public Page<Ifleet_vehicle_model_brand> fetchDefault(SearchContext context){
        Page<fleet_vehicle_model_brandImpl> page = this.fleet_vehicle_model_brandFeignClient.fetchDefault(context) ;
        return new PageImpl(page.getContent(),context.getPageable(),page.getTotalElements());
    }


    public void get(Ifleet_vehicle_model_brand fleet_vehicle_model_brand){
        Ifleet_vehicle_model_brand clientModel = fleet_vehicle_model_brandFeignClient.get(fleet_vehicle_model_brand.getId()) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_model_brand.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_model_brand, null);
    }


    public void remove(Ifleet_vehicle_model_brand fleet_vehicle_model_brand){
        fleet_vehicle_model_brandFeignClient.remove(fleet_vehicle_model_brand.getId()) ;
    }


    public void update(Ifleet_vehicle_model_brand fleet_vehicle_model_brand){
        Ifleet_vehicle_model_brand clientModel = fleet_vehicle_model_brandFeignClient.update(fleet_vehicle_model_brand.getId(),(fleet_vehicle_model_brandImpl)fleet_vehicle_model_brand) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_model_brand.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_model_brand, null);
    }


    public void create(Ifleet_vehicle_model_brand fleet_vehicle_model_brand){
        Ifleet_vehicle_model_brand clientModel = fleet_vehicle_model_brandFeignClient.create((fleet_vehicle_model_brandImpl)fleet_vehicle_model_brand) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_model_brand.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_model_brand, null);
    }


    public Page<Ifleet_vehicle_model_brand> select(SearchContext context){
        return null ;
    }


    public void getDraft(Ifleet_vehicle_model_brand fleet_vehicle_model_brand){
        Ifleet_vehicle_model_brand clientModel = fleet_vehicle_model_brandFeignClient.getDraft(fleet_vehicle_model_brand.getId(),(fleet_vehicle_model_brandImpl)fleet_vehicle_model_brand) ;
        BeanCopier copier=BeanCopier.create(clientModel.getClass(), fleet_vehicle_model_brand.getClass(), false);
        copier.copy(clientModel, fleet_vehicle_model_brand, null);
    }



}

