package cn.ibizlab.odoo.client.odoo_fleet.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_model;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_modelImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[fleet_vehicle_model] 服务对象接口
 */
public interface fleet_vehicle_modelFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_models/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_models")
    public fleet_vehicle_modelImpl create(@RequestBody fleet_vehicle_modelImpl fleet_vehicle_model);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_models/fetchdefault")
    public Page<fleet_vehicle_modelImpl> fetchDefault(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_models/{id}")
    public fleet_vehicle_modelImpl update(@PathVariable("id") Integer id,@RequestBody fleet_vehicle_modelImpl fleet_vehicle_model);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_models/{id}")
    public fleet_vehicle_modelImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_models/updatebatch")
    public fleet_vehicle_modelImpl updateBatch(@RequestBody List<fleet_vehicle_modelImpl> fleet_vehicle_models);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_models/createbatch")
    public fleet_vehicle_modelImpl createBatch(@RequestBody List<fleet_vehicle_modelImpl> fleet_vehicle_models);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_models/removebatch")
    public fleet_vehicle_modelImpl removeBatch(@RequestBody List<fleet_vehicle_modelImpl> fleet_vehicle_models);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_models/select")
    public Page<fleet_vehicle_modelImpl> select();


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_models/{id}/getdraft")
    public fleet_vehicle_modelImpl getDraft(@PathVariable("id") Integer id,@RequestBody fleet_vehicle_modelImpl fleet_vehicle_model);



}
