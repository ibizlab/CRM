package cn.ibizlab.odoo.activiti.resource;

import com.alibaba.fastjson.JSONObject;
import cn.ibizlab.odoo.activiti.service.IWFCoreService;
import org.activiti.api.task.model.Task;
import org.activiti.bpmn.model.SequenceFlow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class WFCoreResource {

    @Autowired
    IWFCoreService wfCoreService;

    /**
     * 工作流部署
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wfdeploy")
    public JSONObject wfDeploy(@RequestBody JSONObject et){
        return wfCoreService.wfDeploy(et);
    }
    /**
     * 工作流启动
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wfstart")
    public JSONObject wfStart(@RequestBody JSONObject et){
        return wfCoreService.wfStart(et);
    }
    /**
     * 工作流提交处理
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wfsubmit")
    public JSONObject wfSubmit(@RequestBody JSONObject et){
        return wfCoreService.wfSubmit(et);
    }
    /**
     * 工作流跳转
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wfgoto")
    public JSONObject wfGoto(@RequestBody JSONObject et){
        return wfCoreService.wfGoto(et);
    }

    /**
     * 结束流程
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wfclose")
    public JSONObject wfClose(@RequestBody JSONObject et){
        return wfCoreService.wfClose(et);
    }
    /**
     * 工作流挂起
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wfsuspend")
    public JSONObject wfSuspend(@RequestBody JSONObject et){
        return wfCoreService.wfSuspend(et);
    }
    /**
     * 工作流激活
     */
    @RequestMapping(method = RequestMethod.POST, value = "/wfresume")
    public JSONObject wfResume(@RequestBody JSONObject et){
        return wfCoreService.wfResume(et);
    }
}
