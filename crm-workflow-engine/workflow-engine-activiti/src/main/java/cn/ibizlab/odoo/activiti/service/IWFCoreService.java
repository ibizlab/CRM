package cn.ibizlab.odoo.activiti.service;

import com.alibaba.fastjson.JSONObject;
import org.activiti.api.task.model.Task;
import org.activiti.bpmn.model.SequenceFlow;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface IWFCoreService {

    /**
     * 工作流启动
     */
    JSONObject wfStart(JSONObject et);
    /**
     * 工作流提交处理
     */
    JSONObject wfSubmit(JSONObject et);
    /**
     * 工作流跳转
     */
    JSONObject wfGoto(JSONObject et);
    /**
     * 工作流关闭
     */
    JSONObject wfClose(JSONObject et);
    /**
     * 工作流部署
     */
    JSONObject wfDeploy(JSONObject et);
    /**
     * 流程挂起
     */
    JSONObject wfSuspend(JSONObject et);
    /**
     * 流程恢复
     */
    JSONObject wfResume(JSONObject et);




}
