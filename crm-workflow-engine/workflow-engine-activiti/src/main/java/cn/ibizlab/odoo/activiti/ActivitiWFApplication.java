package cn.ibizlab.odoo.activiti;

import cn.ibizlab.odoo.activiti.util.serialize.DomainSerializerProvider;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication(exclude = {

})
@Import({
        FeignClientsConfiguration.class
})
@ComponentScans({
//        @ComponentScan(basePackages = {"cn.ibizlab.odoo.util", "cn.ibizlab.odoo.activiti"})
})
@EnableDiscoveryClient
@Configuration
@EnableFeignClients
@EnableConfigurationProperties(cn.ibizlab.odoo.activiti.ActivitiWFApplication.WebClientProperties.class)
public class ActivitiWFApplication {

    public static void main(String[] args) {
        SpringApplicationBuilder builder=new SpringApplicationBuilder(ActivitiWFApplication.class);
        builder.run(args);
    }

    @Bean
    public MappingJackson2HttpMessageConverter mappingJacksonHttpMessageConverter(ObjectMapper objectMapper) {
        objectMapper.setSerializerProvider(new DomainSerializerProvider()) ;
        final MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter(objectMapper);
        return converter;
    }

    @ConfigurationProperties(prefix = "uniwf.web")
    @Data
    public class WebClientProperties {

        private String tokenUrl ;

        private String clientId ;

        private String clientSecret ;

        private String serviceUrl ;

        private String serviceId ;

    }
}
