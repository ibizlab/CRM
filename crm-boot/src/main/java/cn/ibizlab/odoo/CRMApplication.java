package cn.ibizlab.odoo;

import java.util.List;

import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import io.seata.spring.annotation.datasource.EnableAutoDataSourceProxy;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import cn.ibizlab.odoo.util.web.SearchContextHandlerMethodArgumentResolver;
import cn.ibizlab.odoo.util.serialize.DomainSerializerProvider;
import cn.ibizlab.odoo.util.helper.UniqueNameGenerator;

@Slf4j
@SpringBootApplication(exclude = {

})
@Import({
//    cn.ibizlab.odoo
})
@EnableDiscoveryClient
@Configuration
@ComponentScan(nameGenerator = UniqueNameGenerator.class)
@EnableTransactionManagement
//@EnableAutoDataSourceProxy
public class CRMApplication extends WebMvcConfigurerAdapter {

    public static void main(String[] args) {
        SpringApplicationBuilder builder=new SpringApplicationBuilder(CRMApplication.class);
        builder.run(args);
    }

    @Bean
    public MappingJackson2HttpMessageConverter mappingJacksonHttpMessageConverter(ObjectMapper objectMapper) {
    	objectMapper.setSerializerProvider(new DomainSerializerProvider()) ;
    	final MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter(objectMapper);
        return converter;
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        super.addArgumentResolvers(argumentResolvers);
        argumentResolvers.add(new SearchContextHandlerMethodArgumentResolver());
    }
}
