
export default {
  fields: {
    display_name: '显示名称',
    fixed_price: '固定价格',
    name: '名称',
    price: '价格',
    __last_update: '最后修改日',
    base: '基于',
    date_end: '结束日期',
    percent_price: '百分比价格',
    price_round: '价格舍入',
    write_date: '最后更新时间',
    id: 'ID',
    create_date: '创建时间',
    compute_price: '计算价格',
    price_min_margin: '最小价格毛利',
    date_start: '开始日期',
    price_max_margin: '最大价格毛利',
    applied_on: '应用于',
    min_quantity: '最小数量',
    price_surcharge: '价格附加费用',
    price_discount: '价格折扣',
    pricelist_id_text: '价格表',
    currency_id_text: '币种',
    categ_id_text: '产品种类',
    base_pricelist_id_text: '其他价格表',
    write_uid_text: '最后更新人',
    product_tmpl_id_text: '产品模板',
    create_uid_text: '创建人',
    company_id_text: '公司',
    product_id_text: '产品',
    product_id: '产品',
    pricelist_id: '价格表',
    write_uid: '最后更新人',
    company_id: '公司',
    base_pricelist_id: '其他价格表',
    currency_id: '币种',
    categ_id: '产品种类',
    create_uid: '创建人',
    product_tmpl_id: '产品模板',
  },
	views: {
		gridview: {
			caption: "价格表明细",
      title: '价格表明细',
		},
		pickupgridview: {
			caption: "价格表明细",
      title: '价格表明细',
		},
		redirectview: {
			caption: "价格表明细",
      title: '价格表明细',
		},
		pickupview: {
			caption: "价格表明细",
      title: '价格表明细',
		},
		editview: {
			caption: "价格表明细",
      title: '价格表明细',
		},
		mpickupview: {
			caption: "价格表明细",
      title: '价格表明细',
		},
	},
	main_form: {
		details: {
			group1: "价格表明细基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "最后更新时间", 
			srforikey: "", 
			srfkey: "ID", 
			srfmajortext: "名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			product_id_text: "产品", 
			price: "价格", 
			id: "ID", 
			product_id: "产品", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			product_id_text: "产品",
			price: "价格",
			write_uid_text: "最后更新人",
			write_date: "最后更新时间",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem6: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem16: {
			caption: "其它",
			tip: "其它",
		},
		tbitem23: {
			caption: "导入",
			tip: "导入",
		},
		tbitem17: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
		tbitem18: {
			caption: "Help",
			tip: "Help",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem16: {
			caption: "-",
			tip: "",
		},
		tbitem23: {
			caption: "第一个记录",
			tip: "第一个记录",
		},
		tbitem24: {
			caption: "上一个记录",
			tip: "上一个记录",
		},
		tbitem25: {
			caption: "下一个记录",
			tip: "下一个记录",
		},
		tbitem26: {
			caption: "最后一个记录",
			tip: "最后一个记录",
		},
		tbitem21: {
			caption: "-",
			tip: "",
		},
		tbitem22: {
			caption: "Help",
			tip: "Help",
		},
	},
};