export default {
  fields: {
    opportunities_amount: '商机收入',
    id: 'ID',
    create_date: '创建时间',
    use_opportunities: '渠道',
    display_name: '显示名称',
    quotations_count: '发票报价单',
    dashboard_graph_type: '类型',
    is_favorite: '显示仪表',
    opportunities_count: '开启的商机数',
    color: '颜色索引',
    use_quotations: '报价单',
    reply_to: '回复 至',
    favorite_user_ids: '最喜欢的成员',
    team_type: '团队类型',
    message_main_attachment_id: '附件',
    message_needaction_counter: '行动数量',
    dashboard_graph_model: '内容',
    message_has_error: '消息递送错误',
    dashboard_graph_period_pipeline: '预计关闭',
    abandoned_carts_count: '遗弃购物车数量',
    pos_config_ids: 'POS',
    message_has_error_counter: '错误数',
    use_leads: '线索',
    message_is_follower: '关注者',
    website_ids: '网站',
    quotations_amount: '发票金额',
    invoiced: '本月已开发票',
    abandoned_carts_amount: '遗弃购物车数量',
    active: '有效',
    message_unread: '未读消息',
    __last_update: '最后修改日',
    sales_to_invoice_count: '发票销售单',
    message_needaction: '需要激活',
    unassigned_leads_count: '未分派线索',
    message_ids: '消息',
    dashboard_graph_group_pos: 'POS分组',
    member_ids: '渠道人员',
    write_date: '最后更新时间',
    invoiced_target: '发票目标',
    dashboard_graph_group: '分组',
    message_unread_counter: '未读消息计数器',
    dashboard_graph_data: '数据仪表图',
    dashboard_graph_period: '比例',
    website_message_ids: '网站信息',
    message_attachment_count: '附件数量',
    message_partner_ids: '关注者(业务伙伴)',
    pos_sessions_open_count: '开放POS会议',
    use_invoices: '设定开票目标',
    dashboard_button_name: '仪表板按钮',
    message_follower_ids: '关注者',
    dashboard_graph_group_pipeline: '分组方式',
    pos_order_amount_total: '会议销售金额',
    message_channel_ids: '关注者(渠道)',
    name: '销售团队',
    alias_domain: '网域别名',
    company_id_text: '公司',
    alias_model_id: '模型别名',
    alias_parent_model_id: '上级模型',
    alias_contact: '安全联系人别名',
    alias_user_id: '所有者',
    currency_id: '币种',
    alias_defaults: '默认值',
    alias_parent_thread_id: '上级记录ID',
    alias_force_thread_id: '记录线索ID',
    create_uid_text: '创建人',
    user_id_text: '团队负责人',
    alias_name: '别名',
    write_uid_text: '最后更新',
    write_uid: '最后更新',
    alias_id: '别名',
    create_uid: '创建人',
    company_id: '公司',
    user_id: '团队负责人',
  },
	views: {
		dataviewexpview: {
			caption: '销售团队',
      title: '销售团队',
		},
		editview9_editmode: {
			caption: '销售团队编辑',
      title: '销售团队编辑',
		},
		tabexpview: {
			caption: '销售团队',
      title: '销售团队',
		},
		pickupgridview: {
			caption: '销售渠道',
      title: '销售渠道',
		},
		dashboardview10: {
			caption: '销售渠道',
      title: '销售渠道',
		},
		gridexpview: {
			caption: '销售渠道',
      title: '销售渠道',
		},
		dashboardview9: {
			caption: '销售渠道',
      title: '销售渠道',
		},
		editview9: {
			caption: '销售团队',
      title: '销售团队',
		},
		dashboardview11: {
			caption: '销售渠道',
      title: '销售渠道',
		},
		pickupview: {
			caption: '销售渠道',
      title: '销售渠道',
		},
		gridview: {
			caption: '销售团队',
      title: '销售团队',
		},
		editview: {
			caption: '销售团队编辑',
      title: '销售团队编辑',
		},
		dataviewexpview2: {
			caption: '销售团队',
      title: '销售团队',
		},
	},
	main2_form: {
		details: {
			group1: '销售团队', 
			formpage1: '基本信息', 
			srfupdatedate: '最后更新时间', 
			srforikey: '', 
			srfkey: 'ID', 
			srfmajortext: '销售团队', 
			srftempmode: '', 
			srfuf: '', 
			srfdeid: '', 
			srfsourcekey: '', 
			name: '销售团队', 
			user_id_text: '团队负责人', 
			id: 'ID', 
			user_id: '团队负责人', 
		},
		uiactions: {
		},
	},
	main_form: {
		details: {
			group1: '基本信息', 
			druipart1: '', 
			grouppanel1: '团队成员', 
			formpage1: '基本信息', 
			srfupdatedate: '最后更新时间', 
			srforikey: '', 
			srfkey: 'ID', 
			srfmajortext: '销售团队', 
			srftempmode: '', 
			srfuf: '', 
			srfdeid: '', 
			srfsourcekey: '', 
			name: '销售团队', 
			user_id_text: '团队负责人', 
			id: 'ID', 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			name: '销售团队',
			user_id_text: '团队负责人',
		},
		uiactions: {
		},
	},
	gridexpbar_grid_grid: {
		columns: {
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: '常规条件', 
		},
		uiactions: {
		},
	},
	editview9_editmodetoolbar_toolbar: {
		tbitem1: {
			caption: '保存并关闭',
			tip: '保存并关闭',
		},
		tbitem2: {
			caption: '关闭',
			tip: '关闭',
		},
	},
	dataviewexpviewdataviewexpbar_toolbar_toolbar: {
		deuiaction1: {
			caption: '新建',
			tip: '新建',
		},
	},
	dataviewexpview2dataviewexpbar_toolbar_toolbar: {
		deuiaction1: {
			caption: '新建',
			tip: '新建',
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: '新建',
			tip: '新建',
		},
		tbitem4: {
			caption: '编辑',
			tip: '编辑',
		},
		tbitem6: {
			caption: '拷贝',
			tip: '拷贝',
		},
		tbitem7: {
			caption: '-',
			tip: '',
		},
		tbitem8: {
			caption: '删除',
			tip: '删除',
		},
		tbitem9: {
			caption: '-',
			tip: '',
		},
		tbitem13: {
			caption: '导出',
			tip: '导出',
		},
		tbitem10: {
			caption: '-',
			tip: '',
		},
		tbitem16: {
			caption: '其它',
			tip: '其它',
		},
		tbitem23: {
			caption: '导入',
			tip: '导入',
		},
		tbitem17: {
			caption: '-',
			tip: '',
		},
		tbitem19: {
			caption: '过滤',
			tip: '过滤',
		},
		tbitem18: {
			caption: '帮助',
			tip: '帮助',
		},
	},
	editviewtoolbar_toolbar: {
		tbitem1: {
			caption: '保存并关闭',
			tip: '保存并关闭',
		},
		tbitem2: {
			caption: '关闭',
			tip: '关闭',
		},
	},
	editview9toolbar_toolbar: {
		deuiaction2: {
			caption: '编辑',
			tip: '编辑',
		},
	},
};