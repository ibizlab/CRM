
export default {
  fields: {
    name: '价格表名称',
    display_name: '显示名称',
    sequence: '序号',
    selectable: '可选',
    discount_policy: '折扣政策',
    country_group_ids: '国家组',
    code: '电子商务促销代码',
    website_id: '网站',
    active: '有效',
    write_date: '最后更新时间',
    create_date: '创建时间',
    item_ids: '价格表项目',
    id: 'ID',
    __last_update: '最后修改日',
    currency_id_text: '币种',
    write_uid_text: '最后更新人',
    create_uid_text: '创建人',
    company_id_text: '公司',
    create_uid: '创建人',
    write_uid: '最后更新人',
    currency_id: '币种',
    company_id: '公司',
  },
	views: {
		mpickupview: {
			caption: "价格表",
      title: '价格表',
		},
		editview: {
			caption: "合同信息",
      title: '合同信息',
		},
		redirectview: {
			caption: "价格表",
      title: '价格表',
		},
		pickupgridview: {
			caption: "价格表",
      title: '价格表',
		},
		pickupview: {
			caption: "价格表",
      title: '价格表',
		},
		gridview: {
			caption: "合同",
      title: '合同',
		},
	},
	main_form: {
		details: {
			group1: "基本信息", 
			druipart1: "", 
			grouppanel1: "明细信息", 
			formpage1: "基本信息", 
			srfupdatedate: "最后更新时间", 
			srforikey: "", 
			srfkey: "ID", 
			srfmajortext: "价格表名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			sequence: "合同号", 
			name: "合同名称", 
			company_id_text: "公司", 
			id: "ID", 
			company_id: "公司", 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			sequence: "合同号",
			name: "合同名称",
			company_id_text: "公司",
			active: "有效",
			write_uid_text: "最后更新人",
			write_date: "最后更新时间",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem16: {
			caption: "-",
			tip: "",
		},
		tbitem23: {
			caption: "第一个记录",
			tip: "第一个记录",
		},
		tbitem24: {
			caption: "上一个记录",
			tip: "上一个记录",
		},
		tbitem25: {
			caption: "下一个记录",
			tip: "下一个记录",
		},
		tbitem26: {
			caption: "最后一个记录",
			tip: "最后一个记录",
		},
		tbitem21: {
			caption: "-",
			tip: "",
		},
		tbitem22: {
			caption: "Help",
			tip: "Help",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "New",
			tip: "New",
		},
		tbitem4: {
			caption: "Edit",
			tip: "Edit {0}",
		},
		tbitem6: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem16: {
			caption: "其它",
			tip: "其它",
		},
		tbitem23: {
			caption: "导入",
			tip: "导入",
		},
		tbitem17: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "Filter",
			tip: "Filter",
		},
		tbitem18: {
			caption: "Help",
			tip: "Help",
		},
	},
};