export default {
  fields: {
    __last_update: '最后修改日',
    property_stock_valuation_account_id: '库存计价科目',
    child_id: '下级类别',
    product_count: '# 产品',
    route_ids: '路线',
    name: '名称',
    display_name: '显示名称',
    property_valuation: '库存计价',
    parent_path: '父级路径',
    id: 'ID',
    complete_name: '完整名称',
    property_account_creditor_price_difference_categ: '价格差异科目',
    property_stock_account_output_categ_id: '库存出货科目',
    create_date: '创建时间',
    total_route_ids: '路线合计',
    property_cost_method: '成本方法',
    property_account_income_categ_id: '收入科目',
    property_account_expense_categ_id: '费用科目',
    property_stock_journal: '库存日记账',
    write_date: '最后更新时间',
    property_stock_account_input_categ_id: '库存进货科目',
    write_uid_text: '最后更新人',
    parent_id_text: '上级类别',
    create_uid_text: '创建人',
    removal_strategy_id_text: '强制下架策略',
    parent_id: '上级类别',
    create_uid: '创建人',
    removal_strategy_id: '强制下架策略',
    write_uid: '最后更新人',
  },
	views: {
		pickupview: {
			caption: '产品种类',
      title: '产品种类',
		},
		gridview: {
			caption: '产品种类',
      title: '产品种类',
		},
		pickupgridview: {
			caption: '产品种类',
      title: '产品种类',
		},
		editview: {
			caption: '产品种类',
      title: '产品种类',
		},
	},
	main_form: {
		details: {
			group1: '产品种类基本信息', 
			formpage1: '基本信息', 
			group2: '操作信息', 
			formpage2: '其它', 
			srfupdatedate: '最后更新时间', 
			srforikey: '', 
			srfkey: 'ID', 
			srfmajortext: '名称', 
			srftempmode: '', 
			srfuf: '', 
			srfdeid: '', 
			srfsourcekey: '', 
			name: '名称', 
			parent_id_text: '上级类别', 
			create_uid_text: '创建人', 
			create_date: '创建时间', 
			write_uid_text: '最后更新人', 
			write_date: '最后更新时间', 
			id: 'ID', 
			write_uid: '最后更新人', 
			create_uid: '创建人', 
			parent_id: '上级类别', 
		},
		uiactions: {
		},
	},
	main_grid: {
		columns: {
			name: '名称',
			parent_id_text: '上级类别',
			write_uid_text: '最后更新人',
			write_date: '最后更新时间',
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: '常规条件', 
		},
		uiactions: {
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: '新建',
			tip: '新建',
		},
		tbitem4: {
			caption: '编辑',
			tip: '编辑',
		},
		tbitem6: {
			caption: '拷贝',
			tip: '拷贝',
		},
		tbitem7: {
			caption: '-',
			tip: '',
		},
		tbitem8: {
			caption: '删除',
			tip: '删除',
		},
		tbitem9: {
			caption: '-',
			tip: '',
		},
		tbitem13: {
			caption: '导出',
			tip: '导出',
		},
		tbitem10: {
			caption: '-',
			tip: '',
		},
		tbitem16: {
			caption: '其它',
			tip: '其它',
		},
		tbitem23: {
			caption: '导入',
			tip: '导入',
		},
		tbitem17: {
			caption: '-',
			tip: '',
		},
		tbitem19: {
			caption: '过滤',
			tip: '过滤',
		},
		tbitem18: {
			caption: '帮助',
			tip: '帮助',
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: '保存',
			tip: '保存',
		},
		tbitem4: {
			caption: '保存并新建',
			tip: '保存并新建',
		},
		tbitem5: {
			caption: '保存并关闭',
			tip: '保存并关闭',
		},
		tbitem6: {
			caption: '-',
			tip: '',
		},
		tbitem7: {
			caption: '删除并关闭',
			tip: '删除并关闭',
		},
		tbitem8: {
			caption: '-',
			tip: '',
		},
		tbitem12: {
			caption: '新建',
			tip: '新建',
		},
		tbitem13: {
			caption: '-',
			tip: '',
		},
		tbitem14: {
			caption: '拷贝',
			tip: '拷贝',
		},
		tbitem16: {
			caption: '-',
			tip: '',
		},
		tbitem23: {
			caption: '第一个记录',
			tip: '第一个记录',
		},
		tbitem24: {
			caption: '上一个记录',
			tip: '上一个记录',
		},
		tbitem25: {
			caption: '下一个记录',
			tip: '下一个记录',
		},
		tbitem26: {
			caption: '最后一个记录',
			tip: '最后一个记录',
		},
		tbitem21: {
			caption: '-',
			tip: '',
		},
		tbitem22: {
			caption: '帮助',
			tip: '帮助',
		},
	},
};