import product_price_list_zh_CN from '@locale/lanres/product-price-list/product-price-list_zh_CN';
import account_journal_zh_CN from '@locale/lanres/account-journal/account-journal_zh_CN';
import product_pricelist_zh_CN from '@locale/lanres/product-pricelist/product-pricelist_zh_CN';
import crm_lead_zh_CN from '@locale/lanres/crm-lead/crm-lead_zh_CN';
import mail_activity_zh_CN from '@locale/lanres/mail-activity/mail-activity_zh_CN';
import res_company_zh_CN from '@locale/lanres/res-company/res-company_zh_CN';
import sale_order_line_zh_CN from '@locale/lanres/sale-order-line/sale-order-line_zh_CN';
import account_payment_zh_CN from '@locale/lanres/account-payment/account-payment_zh_CN';
import product_product_zh_CN from '@locale/lanres/product-product/product-product_zh_CN';
import calendar_event_zh_CN from '@locale/lanres/calendar-event/calendar-event_zh_CN';
import crm_team_zh_CN from '@locale/lanres/crm-team/crm-team_zh_CN';
import product_category_zh_CN from '@locale/lanres/product-category/product-category_zh_CN';
import res_partner_p_zh_CN from '@locale/lanres/res-partner-p/res-partner-p_zh_CN';
import product_pricelist_item_zh_CN from '@locale/lanres/product-pricelist-item/product-pricelist-item_zh_CN';
import account_invoice_zh_CN from '@locale/lanres/account-invoice/account-invoice_zh_CN';
import mail_message_zh_CN from '@locale/lanres/mail-message/mail-message_zh_CN';
import res_users_zh_CN from '@locale/lanres/res-users/res-users_zh_CN';
import sale_order_zh_CN from '@locale/lanres/sale-order/sale-order_zh_CN';
import mail_activity_type_zh_CN from '@locale/lanres/mail-activity-type/mail-activity-type_zh_CN';
import res_partner_zh_CN from '@locale/lanres/res-partner/res-partner_zh_CN';
import account_register_payments_zh_CN from '@locale/lanres/account-register-payments/account-register-payments_zh_CN';
import dynachart_zh_CN from '@locale/lanres/dyna-chart/dyna-chart_zh_CN';
import dynadashboard_zh_CN from '@locale/lanres/dyna-dashboard/dyna-dashboard_zh_CN';
import account_invoice_line_zh_CN from '@locale/lanres/account-invoice-line/account-invoice-line_zh_CN';
import components_zh_CN from '@locale/lanres/components/components_zh_CN';
import codelist_zh_CN from '@locale/lanres/codelist/codelist_zh_CN';
import userCustom_zh_CN from '@locale/lanres/userCustom/userCustom_zh_CN';

export default {
    app: {
        commonWords:{
            error: '失败',
            success: '成功',
            ok: '确认',
            cancel: '取消',
        },
        gridpage: {
            choicecolumns: '选择列',
            refresh: '刷新',
            show: '显示',
            records: '条',
            totle: '共',
        },
        tabpage: {
            sureclosetip: {
                title: '关闭提醒',
                content: '表单数据已经修改，确定要关闭？',
            },
            closeall: '关闭所有',
            closeother: '关闭其他',
        },
        fileUpload: {
            caption: '上传',
        },
        searchButton: {
            search: '搜索',
            reset: '重置',
        },
        calendar:{
          today: '今天',
          month: '月',
          week: '周',
          day: '天',
          list: '列',
          dateSelectModalTitle: '选择要跳转的时间',
          gotoDate: '跳转',
        },
        // 非实体视图
        views: {
            salespersonportalview: {
                caption: '',
                title: '',
            },
            teamleaderportalview: {
                caption: '',
                title: '',
            },
            appindexview: {
                caption: 'iBizOdoo',
                title: 'iBizOdoo',
            },
            appportalview: {
                caption: '',
                title: '',
            },
            leaderportalview: {
                caption: '',
                title: '',
            },
        },
        utilview:{
            importview:"导入数据",
            warning:"警告",
            info:"请配置数据导入项" 
        },
        menus: {
            appindexview: {
                menuitem17: '工作台',
                menuitem1: '客户管理',
                menuitem2: '客户信息',
                menuitem19: '客户导览',
                menuitem21: '团队客户',
                menuitem16: '联系人',
                menuitem45: '商机信息',
                menuitem3: '团队商机',
                menuitem31: '活动日历',
                menuitem4: '产品管理',
                menuitem5: '产品种类',
                menuitem6: '产品信息',
                menuitem7: '销售管理',
                menuitem9: '销售团队',
                menuitem20: '销售团队导览',
                menuitem22: '销售人员',
                menuitem15: '合同信息',
                menuitem10: '报价单',
                menuitem11: '销售订单',
                menuitem8: '账单管理',
                menuitem12: '账单',
                menuitem13: '发票',
                menuitem14: '付款登记',
                menuitem23: '系统',
                menuitem24: '用户',
                hometopmenus: '首页头部菜单(隐藏)',
                menuitem44: '移动端',
                menuitem18: 'iBiz开放平台',
                menuitem25: '项目文件',
                menuitem30: '模型设计工具',
                menuitem32: 'iBiz论坛',
                menuitem26: '销售员',
                menuitem28: '销售员工作台',
                menuitem39: '我的客户',
                menuitem35: '我的联系人',
                menuitem36: '我的商机',
                menuitem27: '团队负责人',
                menuitem29: '团队负责人工作台',
                menuitem37: '我的团队',
                menuitem41: '团队客户',
                menuitem42: '团队商机',
                menuitem33: '公司领导',
                menuitem34: '公司领导工作台',
                menuitem38: '全部团队',
                menuitem40: '全部客户',
                menuitem43: '全部商机',
            },
        },
    },
    entities: {
        product_price_list: product_price_list_zh_CN,
        account_journal: account_journal_zh_CN,
        product_pricelist: product_pricelist_zh_CN,
        crm_lead: crm_lead_zh_CN,
        mail_activity: mail_activity_zh_CN,
        res_company: res_company_zh_CN,
        sale_order_line: sale_order_line_zh_CN,
        account_payment: account_payment_zh_CN,
        product_product: product_product_zh_CN,
        calendar_event: calendar_event_zh_CN,
        crm_team: crm_team_zh_CN,
        product_category: product_category_zh_CN,
        res_partner_p: res_partner_p_zh_CN,
        product_pricelist_item: product_pricelist_item_zh_CN,
        account_invoice: account_invoice_zh_CN,
        mail_message: mail_message_zh_CN,
        res_users: res_users_zh_CN,
        sale_order: sale_order_zh_CN,
        mail_activity_type: mail_activity_type_zh_CN,
        res_partner: res_partner_zh_CN,
        account_register_payments: account_register_payments_zh_CN,
        dynachart: dynachart_zh_CN,
        dynadashboard: dynadashboard_zh_CN,
        account_invoice_line: account_invoice_line_zh_CN,
    },
    components: components_zh_CN,
    codelist: codelist_zh_CN,
    userCustom: userCustom_zh_CN,
};