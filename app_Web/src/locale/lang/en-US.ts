import product_price_list_en_US from '@locale/lanres/product-price-list/product-price-list_en_US';
import account_journal_en_US from '@locale/lanres/account-journal/account-journal_en_US';
import product_pricelist_en_US from '@locale/lanres/product-pricelist/product-pricelist_en_US';
import crm_lead_en_US from '@locale/lanres/crm-lead/crm-lead_en_US';
import mail_activity_en_US from '@locale/lanres/mail-activity/mail-activity_en_US';
import res_company_en_US from '@locale/lanres/res-company/res-company_en_US';
import sale_order_line_en_US from '@locale/lanres/sale-order-line/sale-order-line_en_US';
import account_payment_en_US from '@locale/lanres/account-payment/account-payment_en_US';
import product_product_en_US from '@locale/lanres/product-product/product-product_en_US';
import calendar_event_en_US from '@locale/lanres/calendar-event/calendar-event_en_US';
import crm_team_en_US from '@locale/lanres/crm-team/crm-team_en_US';
import product_category_en_US from '@locale/lanres/product-category/product-category_en_US';
import res_partner_p_en_US from '@locale/lanres/res-partner-p/res-partner-p_en_US';
import product_pricelist_item_en_US from '@locale/lanres/product-pricelist-item/product-pricelist-item_en_US';
import account_invoice_en_US from '@locale/lanres/account-invoice/account-invoice_en_US';
import mail_message_en_US from '@locale/lanres/mail-message/mail-message_en_US';
import res_users_en_US from '@locale/lanres/res-users/res-users_en_US';
import sale_order_en_US from '@locale/lanres/sale-order/sale-order_en_US';
import mail_activity_type_en_US from '@locale/lanres/mail-activity-type/mail-activity-type_en_US';
import res_partner_en_US from '@locale/lanres/res-partner/res-partner_en_US';
import account_register_payments_en_US from '@locale/lanres/account-register-payments/account-register-payments_en_US';
import dynachart_en_US from '@locale/lanres/dyna-chart/dyna-chart_en_US';
import dynadashboard_en_US from '@locale/lanres/dyna-dashboard/dyna-dashboard_en_US';
import account_invoice_line_en_US from '@locale/lanres/account-invoice-line/account-invoice-line_en_US';
import components_en_US from '@locale/lanres/components/components_en_US';
import codelist_en_US from '@locale/lanres/codelist/codelist_en_US';
import userCustom_en_US from '@locale/lanres/userCustom/userCustom_en_US';

export default {
    app: {
        commonWords:{
            error: 'Error',
            success: 'Success',
            ok: 'OK',
            cancel: 'Cancel',
        },
        gridpage: {
            choicecolumns: 'Choice columns',
            refresh: 'refresh',
            show: 'Show',
            records: 'records',
            totle: 'totle',
        },
        tabpage: {
            sureclosetip: {
                title: 'Close warning',
                content: 'Form data Changed, are sure close?',
            },
            closeall: 'Close all',
            closeother: 'Close other',
        },
        fileUpload: {
            caption: 'Upload',
        },
        searchButton: {
            search: 'Search',
            reset: 'Reset',
        },
        calendar:{
          today: 'today',
          month: 'month',
          week: 'week',
          day: 'day',
          list: 'list',
          dateSelectModalTitle: 'select the time you wanted',
          gotoDate: 'goto',
        },
        // 非实体视图
        views: {
            salespersonportalview: {
                caption: '',
                title: '',
            },
            teamleaderportalview: {
                caption: '',
                title: '',
            },
            appindexview: {
                caption: 'iBizOdoo',
                title: 'iBizOdoo',
            },
            appportalview: {
                caption: '',
                title: '',
            },
            leaderportalview: {
                caption: '',
                title: '',
            },
        },
        utilview:{
            importview:"Import Data",
            warning:"warning",
            info:"Please configure the data import item"    
        },
        menus: {
            appindexview: {
                menuitem17: '工作台',
                menuitem1: '客户管理',
                menuitem2: '客户信息',
                menuitem19: '客户导览',
                menuitem21: '团队客户',
                menuitem16: '联系人',
                menuitem45: '商机信息',
                menuitem3: '团队商机',
                menuitem31: '活动日历',
                menuitem4: '产品管理',
                menuitem5: '产品种类',
                menuitem6: '产品信息',
                menuitem7: '销售管理',
                menuitem9: '销售团队',
                menuitem20: '销售团队导览',
                menuitem22: '销售人员',
                menuitem15: '合同信息',
                menuitem10: '报价单',
                menuitem11: '销售订单',
                menuitem8: '账单管理',
                menuitem12: '账单',
                menuitem13: '发票',
                menuitem14: '付款登记',
                menuitem23: '系统',
                menuitem24: '用户',
                hometopmenus: '首页头部菜单(隐藏)',
                menuitem44: '移动端',
                menuitem18: 'iBiz开放平台',
                menuitem25: '项目文件',
                menuitem30: '模型设计工具',
                menuitem32: 'iBiz论坛',
                menuitem26: '销售员',
                menuitem28: '销售员工作台',
                menuitem39: '我的客户',
                menuitem35: '我的联系人',
                menuitem36: '我的商机',
                menuitem27: '团队负责人',
                menuitem29: '团队负责人工作台',
                menuitem37: '我的团队',
                menuitem41: '团队客户',
                menuitem42: '团队商机',
                menuitem33: '公司领导',
                menuitem34: '公司领导工作台',
                menuitem38: '全部团队',
                menuitem40: '全部客户',
                menuitem43: '全部商机',
            },
        },
    },
    entities: {
        product_price_list: product_price_list_en_US,
        account_journal: account_journal_en_US,
        product_pricelist: product_pricelist_en_US,
        crm_lead: crm_lead_en_US,
        mail_activity: mail_activity_en_US,
        res_company: res_company_en_US,
        sale_order_line: sale_order_line_en_US,
        account_payment: account_payment_en_US,
        product_product: product_product_en_US,
        calendar_event: calendar_event_en_US,
        crm_team: crm_team_en_US,
        product_category: product_category_en_US,
        res_partner_p: res_partner_p_en_US,
        product_pricelist_item: product_pricelist_item_en_US,
        account_invoice: account_invoice_en_US,
        mail_message: mail_message_en_US,
        res_users: res_users_en_US,
        sale_order: sale_order_en_US,
        mail_activity_type: mail_activity_type_en_US,
        res_partner: res_partner_en_US,
        account_register_payments: account_register_payments_en_US,
        dynachart: dynachart_en_US,
        dynadashboard: dynadashboard_en_US,
        account_invoice_line: account_invoice_line_en_US,
    },
    components: components_en_US,
    codelist: codelist_en_US,
    userCustom: userCustom_en_US,
};