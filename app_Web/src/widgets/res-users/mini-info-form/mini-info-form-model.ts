/**
 * MiniInfo 部件模型
 *
 * @export
 * @class MiniInfoModel
 */
export default class MiniInfoModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof MiniInfoModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'name',
        prop: 'name',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'email',
        prop: 'email',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'phone',
        prop: 'phone',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'mobile',
        prop: 'mobile',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'res_users',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}