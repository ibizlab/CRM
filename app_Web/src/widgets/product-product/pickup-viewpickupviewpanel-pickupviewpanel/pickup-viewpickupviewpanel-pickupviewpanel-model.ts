/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'variant_seller_ids',
      },
      {
        name: 'product_template_attribute_value_ids',
      },
      {
        name: 'product_variant_ids',
      },
      {
        name: 'image_small',
      },
      {
        name: 'message_unread',
      },
      {
        name: 'volume',
      },
      {
        name: 'lst_price',
      },
      {
        name: 'valid_product_attribute_ids',
      },
      {
        name: 'stock_fifo_manual_move_ids',
      },
      {
        name: 'stock_quant_ids',
      },
      {
        name: 'supplier_taxes_id',
      },
      {
        name: 'pricelist_item_ids',
      },
      {
        name: '__last_update',
      },
      {
        name: 'accessory_product_ids',
      },
      {
        name: 'seller_ids',
      },
      {
        name: 'valid_product_attribute_value_wnva_ids',
      },
      {
        name: 'partner_ref',
      },
      {
        name: 'product_image_ids',
      },
      {
        name: 'mrp_product_qty',
      },
      {
        name: 'write_date',
      },
      {
        name: 'valid_product_template_attribute_line_ids',
      },
      {
        name: 'activity_type_id',
      },
      {
        name: 'public_categ_ids',
      },
      {
        name: 'event_ticket_ids',
      },
      {
        name: 'price',
      },
      {
        name: 'attribute_line_ids',
      },
      {
        name: 'virtual_available',
      },
      {
        name: 'nbr_reordering_rules',
      },
      {
        name: 'active',
      },
      {
        name: 'message_is_follower',
      },
      {
        name: 'message_unread_counter',
      },
      {
        name: 'product_product',
        prop: 'id',
      },
      {
        name: 'website_price_difference',
      },
      {
        name: 'message_follower_ids',
      },
      {
        name: 'cart_qty',
      },
      {
        name: 'website_public_price',
      },
      {
        name: 'rating_ids',
      },
      {
        name: 'bom_line_ids',
      },
      {
        name: 'website_price',
      },
      {
        name: 'outgoing_qty',
      },
      {
        name: 'sales_count',
      },
      {
        name: 'valid_product_attribute_wnva_ids',
      },
      {
        name: 'image_medium',
      },
      {
        name: 'valid_existing_variant_ids',
      },
      {
        name: 'stock_value_currency_id',
      },
      {
        name: 'stock_value',
      },
      {
        name: 'website_style_ids',
      },
      {
        name: 'message_channel_ids',
      },
      {
        name: 'weight',
      },
      {
        name: 'bom_ids',
      },
      {
        name: 'message_has_error_counter',
      },
      {
        name: 'activity_state',
      },
      {
        name: 'valid_product_template_attribute_line_wnva_ids',
      },
      {
        name: 'message_main_attachment_id',
      },
      {
        name: 'message_partner_ids',
      },
      {
        name: 'create_date',
      },
      {
        name: 'valid_product_attribute_value_ids',
      },
      {
        name: 'qty_available',
      },
      {
        name: 'message_attachment_count',
      },
      {
        name: 'image_variant',
      },
      {
        name: 'stock_move_ids',
      },
      {
        name: 'message_needaction',
      },
      {
        name: 'website_message_ids',
      },
      {
        name: 'stock_fifo_real_time_aml_ids',
      },
      {
        name: 'activity_date_deadline',
      },
      {
        name: 'code',
      },
      {
        name: 'reordering_min_qty',
      },
      {
        name: 'image',
      },
      {
        name: 'route_ids',
      },
      {
        name: 'taxes_id',
      },
      {
        name: 'bom_count',
      },
      {
        name: 'message_needaction_counter',
      },
      {
        name: 'packaging_ids',
      },
      {
        name: 'valid_archived_variant_ids',
      },
      {
        name: 'activity_user_id',
      },
      {
        name: 'item_ids',
      },
      {
        name: 'purchased_product_qty',
      },
      {
        name: 'reordering_max_qty',
      },
      {
        name: 'orderpoint_ids',
      },
      {
        name: 'optional_product_ids',
      },
      {
        name: 'is_product_variant',
      },
      {
        name: 'used_in_bom_count',
      },
      {
        name: 'qty_at_date',
      },
      {
        name: 'message_has_error',
      },
      {
        name: 'activity_ids',
      },
      {
        name: 'message_ids',
      },
      {
        name: 'barcode',
      },
      {
        name: 'display_name',
      },
      {
        name: 'standard_price',
      },
      {
        name: 'attribute_value_ids',
      },
      {
        name: 'price_extra',
      },
      {
        name: 'variant_bom_ids',
      },
      {
        name: 'alternative_product_ids',
      },
      {
        name: 'default_code',
      },
      {
        name: 'route_from_categ_ids',
      },
      {
        name: 'activity_summary',
      },
      {
        name: 'incoming_qty',
      },
      {
        name: 'currency_id',
      },
      {
        name: 'tracking',
      },
      {
        name: 'description_picking',
      },
      {
        name: 'property_stock_account_output',
      },
      {
        name: 'sale_ok',
      },
      {
        name: 'website_description',
      },
      {
        name: 'website_meta_og_img',
      },
      {
        name: 'company_id',
      },
      {
        name: 'to_weight',
      },
      {
        name: 'description',
      },
      {
        name: 'description_pickingin',
      },
      {
        name: 'list_price',
      },
      {
        name: 'hide_expense_policy',
      },
      {
        name: 'description_sale',
      },
      {
        name: 'cost_method',
      },
      {
        name: 'sequence',
      },
      {
        name: 'sale_line_warn_msg',
      },
      {
        name: 'warehouse_id',
      },
      {
        name: 'rental',
      },
      {
        name: 'property_account_creditor_price_difference',
      },
      {
        name: 'weight_uom_name',
      },
      {
        name: 'cost_currency_id',
      },
      {
        name: 'property_stock_account_input',
      },
      {
        name: 'name',
      },
      {
        name: 'produce_delay',
      },
      {
        name: 'is_seo_optimized',
      },
      {
        name: 'website_url',
      },
      {
        name: 'rating_last_feedback',
      },
      {
        name: 'website_size_y',
      },
      {
        name: 'event_ok',
      },
      {
        name: 'inventory_availability',
      },
      {
        name: 'purchase_ok',
      },
      {
        name: 'create_uid_text',
      },
      {
        name: 'rating_last_value',
      },
      {
        name: 'website_meta_title',
      },
      {
        name: 'rating_last_image',
      },
      {
        name: 'description_purchase',
      },
      {
        name: 'website_id',
      },
      {
        name: 'can_be_expensed',
      },
      {
        name: 'sale_line_warn',
      },
      {
        name: 'website_size_x',
      },
      {
        name: 'service_to_purchase',
      },
      {
        name: 'website_sequence',
      },
      {
        name: 'property_stock_inventory',
      },
      {
        name: 'location_id',
      },
      {
        name: 'property_valuation',
      },
      {
        name: 'is_published',
      },
      {
        name: 'expense_policy',
      },
      {
        name: 'weight_uom_id',
      },
      {
        name: 'color',
      },
      {
        name: 'property_stock_production',
      },
      {
        name: 'website_published',
      },
      {
        name: 'website_meta_keywords',
      },
      {
        name: 'description_pickingout',
      },
      {
        name: 'pricelist_id',
      },
      {
        name: 'rating_count',
      },
      {
        name: 'website_meta_description',
      },
      {
        name: 'valuation',
      },
      {
        name: 'invoice_policy',
      },
      {
        name: 'purchase_line_warn_msg',
      },
      {
        name: 'write_uid_text',
      },
      {
        name: 'property_account_income_id',
      },
      {
        name: 'property_cost_method',
      },
      {
        name: 'categ_id',
      },
      {
        name: 'isparts',
      },
      {
        name: 'uom_id',
      },
      {
        name: 'product_variant_id',
      },
      {
        name: 'type',
      },
      {
        name: 'purchase_method',
      },
      {
        name: 'responsible_id',
      },
      {
        name: 'service_type',
      },
      {
        name: 'uom_name',
      },
      {
        name: 'available_threshold',
      },
      {
        name: 'purchase_line_warn',
      },
      {
        name: 'product_variant_count',
      },
      {
        name: 'pos_categ_id',
      },
      {
        name: 'custom_message',
      },
      {
        name: 'property_account_expense_id',
      },
      {
        name: 'sale_delay',
      },
      {
        name: 'uom_po_id',
      },
      {
        name: 'available_in_pos',
      },
      {
        name: 'create_uid',
      },
      {
        name: 'write_uid',
      },
      {
        name: 'product_tmpl_id',
      },
    ]
  }


}