/**
 * BillDetailGrid 部件模型
 *
 * @export
 * @class BillDetailGridModel
 */
export default class BillDetailGridModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof BillDetailGridGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof BillDetailGridGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'company_id',
          prop: 'company_id',
          dataType: 'PICKUP',
        },
        {
          name: 'partner_id',
          prop: 'partner_id',
          dataType: 'PICKUP',
        },
        {
          name: 'currency_id',
          prop: 'currency_id',
          dataType: 'PICKUP',
        },
        {
          name: 'write_uid',
          prop: 'write_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'product_id',
          prop: 'product_id',
          dataType: 'PICKUP',
        },
        {
          name: 'account_id',
          prop: 'account_id',
          dataType: 'PICKUP',
        },
        {
          name: 'uom_id',
          prop: 'uom_id',
          dataType: 'PICKUP',
        },
        {
          name: 'uom_id_text',
          prop: 'uom_id_text',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'create_uid',
          prop: 'create_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'account_analytic_id',
          prop: 'account_analytic_id',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'name',
          dataType: 'LONGTEXT',
        },
        {
          name: 'product_id_text',
          prop: 'product_id_text',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'srfkey',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'srfdataaccaction',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'price_unit',
          prop: 'price_unit',
          dataType: 'FLOAT',
        },
        {
          name: 'quantity',
          prop: 'quantity',
          dataType: 'FLOAT',
        },
        {
          name: 'purchase_line_id',
          prop: 'purchase_line_id',
          dataType: 'PICKUP',
        },
        {
          name: 'invoice_id',
          prop: 'invoice_id',
          dataType: 'PICKUP',
        },
        {
          name: 'price_total',
          prop: 'price_total',
          dataType: 'DECIMAL',
        },
        {
          name: 'account_invoice_line',
          prop: 'id',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}