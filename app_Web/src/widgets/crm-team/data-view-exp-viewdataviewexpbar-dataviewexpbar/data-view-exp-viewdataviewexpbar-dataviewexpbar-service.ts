import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import Crm_teamService from '@/service/crm-team/crm-team-service';
import DataViewExpViewdataviewexpbarModel from './data-view-exp-viewdataviewexpbar-dataviewexpbar-model';


/**
 * DataViewExpViewdataviewexpbar 部件服务对象
 *
 * @export
 * @class DataViewExpViewdataviewexpbarService
 */
export default class DataViewExpViewdataviewexpbarService extends ControlService {

    /**
     * 销售渠道服务对象
     *
     * @type {Crm_teamService}
     * @memberof DataViewExpViewdataviewexpbarService
     */
    public appEntityService: Crm_teamService = new Crm_teamService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof DataViewExpViewdataviewexpbarService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of DataViewExpViewdataviewexpbarService.
     * 
     * @param {*} [opts={}]
     * @memberof DataViewExpViewdataviewexpbarService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new DataViewExpViewdataviewexpbarModel();
    }

}