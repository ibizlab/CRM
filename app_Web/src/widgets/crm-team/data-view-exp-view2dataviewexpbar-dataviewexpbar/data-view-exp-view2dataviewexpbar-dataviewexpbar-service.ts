import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import Crm_teamService from '@/service/crm-team/crm-team-service';
import DataViewExpView2dataviewexpbarModel from './data-view-exp-view2dataviewexpbar-dataviewexpbar-model';


/**
 * DataViewExpView2dataviewexpbar 部件服务对象
 *
 * @export
 * @class DataViewExpView2dataviewexpbarService
 */
export default class DataViewExpView2dataviewexpbarService extends ControlService {

    /**
     * 销售渠道服务对象
     *
     * @type {Crm_teamService}
     * @memberof DataViewExpView2dataviewexpbarService
     */
    public appEntityService: Crm_teamService = new Crm_teamService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof DataViewExpView2dataviewexpbarService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of DataViewExpView2dataviewexpbarService.
     * 
     * @param {*} [opts={}]
     * @memberof DataViewExpView2dataviewexpbarService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new DataViewExpView2dataviewexpbarModel();
    }

}