import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import Crm_teamService from '@/service/crm-team/crm-team-service';
import TeamCardModel from './team-card-dataview-model';


/**
 * TeamCard 部件服务对象
 *
 * @export
 * @class TeamCardService
 */
export default class TeamCardService extends ControlService {

    /**
     * 销售渠道服务对象
     *
     * @type {Crm_teamService}
     * @memberof TeamCardService
     */
    public appEntityService: Crm_teamService = new Crm_teamService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof TeamCardService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of TeamCardService.
     * 
     * @param {*} [opts={}]
     * @memberof TeamCardService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new TeamCardModel();
    }




    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof TeamCardService
     */
    @Errorlog
    public search(action: string,context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            const _appEntityService: any = this.appEntityService;
            let result: Promise<any>;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            }else{
                result =_appEntityService.FetchDefault(Context,Data, isloading);
            }
            result.then((response) => {
                this.handleResponse(action, response);
                resolve(response);
            }).catch(response => {
                reject(response);
            });      
        });
    }

    /**
     * 删除数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof TeamCardService
     */
    @Errorlog
    public delete(action: string,context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            const _appEntityService: any = this.appEntityService;
            let result: Promise<any>;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            }else{
                result =_appEntityService.remove(Context,Data, isloading);
            }
            result.then((response) => {
                this.handleResponse(action, response);
                resolve(response);
            }).catch(response => {
                reject(response);
            });      
        });
    }



}