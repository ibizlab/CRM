import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * DynaMain 部件服务对象
 *
 * @export
 * @class DynaMainService
 */
export default class DynaMainService extends ControlService {
}