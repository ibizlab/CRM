/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof MainModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'opportunities_amount',
      },
      {
        name: 'crm_team',
        prop: 'id',
      },
      {
        name: 'create_date',
      },
      {
        name: 'use_opportunities',
      },
      {
        name: 'display_name',
      },
      {
        name: 'quotations_count',
      },
      {
        name: 'dashboard_graph_type',
      },
      {
        name: 'is_favorite',
      },
      {
        name: 'opportunities_count',
      },
      {
        name: 'color',
      },
      {
        name: 'use_quotations',
      },
      {
        name: 'reply_to',
      },
      {
        name: 'favorite_user_ids',
      },
      {
        name: 'team_type',
      },
      {
        name: 'message_main_attachment_id',
      },
      {
        name: 'message_needaction_counter',
      },
      {
        name: 'dashboard_graph_model',
      },
      {
        name: 'message_has_error',
      },
      {
        name: 'dashboard_graph_period_pipeline',
      },
      {
        name: 'abandoned_carts_count',
      },
      {
        name: 'pos_config_ids',
      },
      {
        name: 'message_has_error_counter',
      },
      {
        name: 'use_leads',
      },
      {
        name: 'message_is_follower',
      },
      {
        name: 'website_ids',
      },
      {
        name: 'quotations_amount',
      },
      {
        name: 'invoiced',
      },
      {
        name: 'abandoned_carts_amount',
      },
      {
        name: 'active',
      },
      {
        name: 'message_unread',
      },
      {
        name: '__last_update',
      },
      {
        name: 'sales_to_invoice_count',
      },
      {
        name: 'message_needaction',
      },
      {
        name: 'unassigned_leads_count',
      },
      {
        name: 'message_ids',
      },
      {
        name: 'dashboard_graph_group_pos',
      },
      {
        name: 'member_ids',
      },
      {
        name: 'write_date',
      },
      {
        name: 'invoiced_target',
      },
      {
        name: 'dashboard_graph_group',
      },
      {
        name: 'message_unread_counter',
      },
      {
        name: 'dashboard_graph_data',
      },
      {
        name: 'dashboard_graph_period',
      },
      {
        name: 'website_message_ids',
      },
      {
        name: 'message_attachment_count',
      },
      {
        name: 'message_partner_ids',
      },
      {
        name: 'pos_sessions_open_count',
      },
      {
        name: 'use_invoices',
      },
      {
        name: 'dashboard_button_name',
      },
      {
        name: 'message_follower_ids',
      },
      {
        name: 'dashboard_graph_group_pipeline',
      },
      {
        name: 'pos_order_amount_total',
      },
      {
        name: 'message_channel_ids',
      },
      {
        name: 'name',
      },
      {
        name: 'alias_domain',
      },
      {
        name: 'company_id_text',
      },
      {
        name: 'alias_model_id',
      },
      {
        name: 'alias_parent_model_id',
      },
      {
        name: 'alias_contact',
      },
      {
        name: 'alias_user_id',
      },
      {
        name: 'currency_id',
      },
      {
        name: 'alias_defaults',
      },
      {
        name: 'alias_parent_thread_id',
      },
      {
        name: 'alias_force_thread_id',
      },
      {
        name: 'create_uid_text',
      },
      {
        name: 'user_id_text',
      },
      {
        name: 'alias_name',
      },
      {
        name: 'write_uid_text',
      },
      {
        name: 'write_uid',
      },
      {
        name: 'alias_id',
      },
      {
        name: 'create_uid',
      },
      {
        name: 'company_id',
      },
      {
        name: 'user_id',
      },
    ]
  }


}