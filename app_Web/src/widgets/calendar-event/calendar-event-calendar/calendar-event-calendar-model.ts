/**
 * CalendarEvent 部件模型
 *
 * @export
 * @class CalendarEventModel
 */
export default class CalendarEventModel {

	/**
	 * 日历项类型
	 *
	 * @returns {any[]}
	 * @memberof CalendarEventCalendarexpbar_calendarMode
	 */
	public itemType: string = "";

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof CalendarEventCalendarexpbar_calendarMode
	 */
	public getDataItems(): any[] {
     let dataItems: any = [
          // 前端新增修改标识，新增为"0",修改为"1"或未设值
          {
            name: 'srffrontuf',
            prop: 'srffrontuf',
            dataType: 'TEXT',
          },
          {
            name: 'color',
          },
          {
            name: 'textColor',
          },
          {
            name: 'itemType',
          },
          {
            name: 'query',
            prop: 'query',
          },
      ];
      switch(this.itemType){
          case "Event":
              dataItems = [...dataItems,
                  {
                    name: 'calendar_event',
                    prop: 'id'
                  },
                  {
                    name: 'title',
                    prop: 'name'
                  },
                  {
                    name:'start',
                    prop:'start_datetime'
                  },
                  {
                    name:'end',
                    prop:'stop_datetime'
                  },
              ];
              break;
      }
      return dataItems;
	}

}