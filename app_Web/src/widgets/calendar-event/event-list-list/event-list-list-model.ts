/**
 * EventList 部件模型
 *
 * @export
 * @class EventListModel
 */
export default class EventListModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof EventListListMode
	 */
	public getDataItems(): any[] {
		return [
			{
				name: 'start_date',
				prop: 'start_date',
				dataType: 'DATE',
			},
			{
				name: 'opportunity_id_text',
				prop: 'opportunity_id_text',
				dataType: 'PICKUPTEXT',
			},
			{
				name: 'display_name',
				prop: 'display_name',
				dataType: 'TEXT',
			},
			{
				name: 'stop_date',
				prop: 'stop_date',
				dataType: 'DATE',
			},
      {
        name:'size',
        prop:'size'
      },
      {
        name:'query',
        prop:'query'
      },
      {
        name:'page',
        prop:'page'
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
		]
	}

}