/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof MainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'name',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'parent_id_text',
        prop: 'parent_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'create_uid_text',
        prop: 'create_uid_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'create_date',
        prop: 'create_date',
        dataType: 'DATETIME',
      },
      {
        name: 'write_uid_text',
        prop: 'write_uid_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'write_date',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'write_uid',
        prop: 'write_uid',
        dataType: 'PICKUP',
      },
      {
        name: 'create_uid',
        prop: 'create_uid',
        dataType: 'PICKUP',
      },
      {
        name: 'parent_id',
        prop: 'parent_id',
        dataType: 'PICKUP',
      },
      {
        name: 'product_category',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}