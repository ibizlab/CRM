/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof MainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'client_order_ref',
        prop: 'client_order_ref',
        dataType: 'TEXT',
      },
      {
        name: 'partner_id_text',
        prop: 'partner_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'confirmation_date',
        prop: 'confirmation_date',
        dataType: 'DATETIME',
      },
      {
        name: 'state',
        prop: 'state',
        dataType: 'SSCODELIST',
      },
      {
        name: 'amount_total',
        prop: 'amount_total',
        dataType: 'DECIMAL',
      },
      {
        name: 'user_id_text',
        prop: 'user_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'team_id_text',
        prop: 'team_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'create_uid_text',
        prop: 'create_uid_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'create_date',
        prop: 'create_date',
        dataType: 'DATETIME',
      },
      {
        name: 'write_uid_text',
        prop: 'write_uid_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'write_date',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'partner_id',
        prop: 'partner_id',
        dataType: 'PICKUP',
      },
      {
        name: 'write_uid',
        prop: 'write_uid',
        dataType: 'PICKUP',
      },
      {
        name: 'create_uid',
        prop: 'create_uid',
        dataType: 'PICKUP',
      },
      {
        name: 'user_id',
        prop: 'user_id',
        dataType: 'PICKUP',
      },
      {
        name: 'team_id',
        prop: 'team_id',
        dataType: 'PICKUP',
      },
      {
        name: 'sale_order',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}