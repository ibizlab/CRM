/**
 * QuoteGrid 部件模型
 *
 * @export
 * @class QuoteGridModel
 */
export default class QuoteGridModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof QuoteGridGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof QuoteGridGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'company_id',
          prop: 'company_id',
          dataType: 'PICKUP',
        },
        {
          name: 'pricelist_id',
          prop: 'pricelist_id',
          dataType: 'PICKUP',
        },
        {
          name: 'create_uid',
          prop: 'create_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'state',
          prop: 'state',
          dataType: 'SSCODELIST',
        },
        {
          name: 'partner_shipping_id',
          prop: 'partner_shipping_id',
          dataType: 'PICKUP',
        },
        {
          name: 'medium_id',
          prop: 'medium_id',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'name',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'srfkey',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'user_id_text',
          prop: 'user_id_text',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'payment_term_id',
          prop: 'payment_term_id',
          dataType: 'PICKUP',
        },
        {
          name: 'incoterm',
          prop: 'incoterm',
          dataType: 'PICKUP',
        },
        {
          name: 'user_id',
          prop: 'user_id',
          dataType: 'PICKUP',
        },
        {
          name: 'client_order_ref',
          prop: 'client_order_ref',
          dataType: 'TEXT',
        },
        {
          name: 'partner_id',
          prop: 'partner_id',
          dataType: 'PICKUP',
        },
        {
          name: 'opportunity_id',
          prop: 'opportunity_id',
          dataType: 'PICKUP',
        },
        {
          name: 'write_uid',
          prop: 'write_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'warehouse_id',
          prop: 'warehouse_id',
          dataType: 'PICKUP',
        },
        {
          name: 'source_id',
          prop: 'source_id',
          dataType: 'PICKUP',
        },
        {
          name: 'fiscal_position_id',
          prop: 'fiscal_position_id',
          dataType: 'PICKUP',
        },
        {
          name: 'sale_order_template_id',
          prop: 'sale_order_template_id',
          dataType: 'PICKUP',
        },
        {
          name: 'campaign_id',
          prop: 'campaign_id',
          dataType: 'PICKUP',
        },
        {
          name: 'partner_invoice_id',
          prop: 'partner_invoice_id',
          dataType: 'PICKUP',
        },
        {
          name: 'date_order',
          prop: 'date_order',
          dataType: 'DATETIME',
        },
        {
          name: 'partner_id_text',
          prop: 'partner_id_text',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'amount_total',
          prop: 'amount_total',
          dataType: 'DECIMAL',
        },
        {
          name: 'analytic_account_id',
          prop: 'analytic_account_id',
          dataType: 'PICKUP',
        },
        {
          name: 'team_id',
          prop: 'team_id',
          dataType: 'PICKUP',
        },
        {
          name: 'sale_order',
          prop: 'id',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}