/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'partner_id',
          prop: 'partner_id',
          dataType: 'PICKUP',
        },
        {
          name: 'currency_id',
          prop: 'currency_id',
          dataType: 'PICKUP',
        },
        {
          name: 'payment_date',
          prop: 'payment_date',
          dataType: 'DATE',
        },
        {
          name: 'write_uid',
          prop: 'write_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'payment_transaction_id',
          prop: 'payment_transaction_id',
          dataType: 'PICKUP',
        },
        {
          name: 'communication',
          prop: 'communication',
          dataType: 'TEXT',
        },
        {
          name: 'payment_method_id',
          prop: 'payment_method_id',
          dataType: 'PICKUP',
        },
        {
          name: 'create_uid',
          prop: 'create_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'destination_journal_id',
          prop: 'destination_journal_id',
          dataType: 'PICKUP',
        },
        {
          name: 'writeoff_account_id',
          prop: 'writeoff_account_id',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'name',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'srfdataaccaction',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'amount',
          prop: 'amount',
          dataType: 'DECIMAL',
        },
        {
          name: 'partner_id_text',
          prop: 'partner_id_text',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'journal_id',
          prop: 'journal_id',
          dataType: 'PICKUP',
        },
        {
          name: 'payment_token_id',
          prop: 'payment_token_id',
          dataType: 'PICKUP',
        },
        {
          name: 'partner_bank_account_id',
          prop: 'partner_bank_account_id',
          dataType: 'PICKUP',
        },
        {
          name: 'account_payment',
          prop: 'id',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}