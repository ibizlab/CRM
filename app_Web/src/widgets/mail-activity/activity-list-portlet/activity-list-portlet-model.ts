/**
 * ActivityList 部件模型
 *
 * @export
 * @class ActivityListModel
 */
export default class ActivityListModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof ActivityListModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'display_name',
      },
      {
        name: 'mail_template_ids',
      },
      {
        name: 'automated',
      },
      {
        name: 'res_name',
      },
      {
        name: 'state',
      },
      {
        name: 'mail_activity',
        prop: 'id',
      },
      {
        name: 'create_date',
      },
      {
        name: 'write_date',
      },
      {
        name: 'date_deadline',
      },
      {
        name: '__last_update',
      },
      {
        name: 'summary',
      },
      {
        name: 'has_recommended_activities',
      },
      {
        name: 'res_id',
      },
      {
        name: 'note',
      },
      {
        name: 'feedback',
      },
      {
        name: 'res_model_id',
      },
      {
        name: 'res_model',
      },
      {
        name: 'user_id_text',
      },
      {
        name: 'activity_category',
      },
      {
        name: 'activity_type_id_text',
      },
      {
        name: 'previous_activity_type_id_text',
      },
      {
        name: 'note_id_text',
      },
      {
        name: 'create_user_id_text',
      },
      {
        name: 'icon',
      },
      {
        name: 'create_uid_text',
      },
      {
        name: 'activity_decoration',
      },
      {
        name: 'write_uid_text',
      },
      {
        name: 'force_next',
      },
      {
        name: 'recommended_activity_type_id_text',
      },
      {
        name: 'calendar_event_id_text',
      },
      {
        name: 'recommended_activity_type_id',
      },
      {
        name: 'activity_type_id',
      },
      {
        name: 'create_user_id',
      },
      {
        name: 'create_uid',
      },
      {
        name: 'user_id',
      },
      {
        name: 'note_id',
      },
      {
        name: 'previous_activity_type_id',
      },
      {
        name: 'calendar_event_id',
      },
      {
        name: 'write_uid',
      },
    ]
  }


}