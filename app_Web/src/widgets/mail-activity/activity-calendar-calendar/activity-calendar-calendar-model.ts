/**
 * ActivityCalendar 部件模型
 *
 * @export
 * @class ActivityCalendarModel
 */
export default class ActivityCalendarModel {

	/**
	 * 日历项类型
	 *
	 * @returns {any[]}
	 * @memberof ActivityCalendarCalendarMode
	 */
	public itemType: string = "";

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof ActivityCalendarCalendarMode
	 */
	public getDataItems(): any[] {
     let dataItems: any = [
          // 前端新增修改标识，新增为"0",修改为"1"或未设值
          {
            name: 'srffrontuf',
            prop: 'srffrontuf',
            dataType: 'TEXT',
          },
          {
            name: 'color',
          },
          {
            name: 'textColor',
          },
          {
            name: 'itemType',
          },
          {
            name: 'query',
            prop: 'query',
          },
      ];
      switch(this.itemType){
          case "Activity":
              dataItems = [...dataItems,
                  {
                    name: 'mail_activity',
                    prop: 'id'
                  },
                  {
                    name: 'title',
                    prop: 'display_name'
                  },
                  {
                    name:'start',
                    prop:'create_date'
                  },
                  {
                    name:'end',
                    prop:'date_deadline'
                  },
              ];
              break;
      }
      return dataItems;
	}

}