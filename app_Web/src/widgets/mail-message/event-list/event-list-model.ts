/**
 * Event 部件模型
 *
 * @export
 * @class EventModel
 */
export default class EventModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof EventDashboard_sysportlet2_listMode
	 */
	public getDataItems(): any[] {
		return [
			{
				name: 'body',
				prop: 'body',
				dataType: 'HTMLTEXT',
			},
      {
        name:'size',
        prop:'size'
      },
      {
        name:'query',
        prop:'query'
      },
      {
        name:'page',
        prop:'page'
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
		]
	}

}