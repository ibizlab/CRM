import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * Event 部件服务对象
 *
 * @export
 * @class EventService
 */
export default class EventService extends ControlService {
}