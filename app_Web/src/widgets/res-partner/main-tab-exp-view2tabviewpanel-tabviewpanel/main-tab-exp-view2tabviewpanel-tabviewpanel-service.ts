import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * MainTabExpView2tabviewpanel 部件服务对象
 *
 * @export
 * @class MainTabExpView2tabviewpanelService
 */
export default class MainTabExpView2tabviewpanelService extends ControlService {
}