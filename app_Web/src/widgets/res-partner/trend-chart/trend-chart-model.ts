/**
 * Trend 部件模型
 *
 * @export
 * @class TrendModel
 */
export default class TrendModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof TrendPortlet_Trend_chartMode
	 */
	public getDataItems(): any[] {
		return [
      {
        name:'query',
        prop:'query'
      },
		]
	}

}