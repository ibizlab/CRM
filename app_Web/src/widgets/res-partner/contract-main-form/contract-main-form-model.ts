/**
 * ContractMain 部件模型
 *
 * @export
 * @class ContractMainModel
 */
export default class ContractMainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof ContractMainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'name',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'is_company',
        prop: 'is_company',
        dataType: 'TRUEFALSE',
      },
      {
        name: 'company_id_text',
        prop: 'company_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'country_id_text',
        prop: 'country_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'state_id_text',
        prop: 'state_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'city',
        prop: 'city',
        dataType: 'TEXT',
      },
      {
        name: 'zip',
        prop: 'zip',
        dataType: 'TEXT',
      },
      {
        name: 'street',
        prop: 'street',
        dataType: 'TEXT',
      },
      {
        name: 'street2',
        prop: 'street2',
        dataType: 'TEXT',
      },
      {
        name: 'ibizfunction',
        prop: 'ibizfunction',
        dataType: 'TEXT',
      },
      {
        name: 'email',
        prop: 'email',
        dataType: 'TEXT',
      },
      {
        name: 'phone',
        prop: 'phone',
        dataType: 'TEXT',
      },
      {
        name: 'mobile',
        prop: 'mobile',
        dataType: 'TEXT',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'state_id',
        prop: 'state_id',
        dataType: 'PICKUP',
      },
      {
        name: 'company_id',
        prop: 'company_id',
        dataType: 'PICKUP',
      },
      {
        name: 'country_id',
        prop: 'country_id',
        dataType: 'PICKUP',
      },
      {
        name: 'res_partner',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}