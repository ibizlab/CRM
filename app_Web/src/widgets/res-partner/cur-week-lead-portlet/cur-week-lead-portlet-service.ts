import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * CurWeekLead 部件服务对象
 *
 * @export
 * @class CurWeekLeadService
 */
export default class CurWeekLeadService extends ControlService {
}