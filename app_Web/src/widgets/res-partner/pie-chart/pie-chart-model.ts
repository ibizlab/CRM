/**
 * Pie 部件模型
 *
 * @export
 * @class PieModel
 */
export default class PieModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof PiePortlet_Pie_chartMode
	 */
	public getDataItems(): any[] {
		return [
      {
        name:'query',
        prop:'query'
      },
		]
	}

}