import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * SalesPersonTreeExpViewtreeexpbar 部件服务对象
 *
 * @export
 * @class SalesPersonTreeExpViewtreeexpbarService
 */
export default class SalesPersonTreeExpViewtreeexpbarService extends ControlService {
}