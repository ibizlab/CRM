/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'company_id',
          prop: 'company_id',
          dataType: 'PICKUP',
        },
        {
          name: 'zip',
          prop: 'zip',
          dataType: 'TEXT',
        },
        {
          name: 'write_uid',
          prop: 'write_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'phone',
          prop: 'phone',
          dataType: 'TEXT',
        },
        {
          name: 'industry_id',
          prop: 'industry_id',
          dataType: 'PICKUP',
        },
        {
          name: 'country_id',
          prop: 'country_id',
          dataType: 'PICKUP',
        },
        {
          name: 'create_uid',
          prop: 'create_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'actor',
          prop: 'create_uid_text',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'srfmajortext',
          prop: 'name',
          dataType: 'TEXT',
        },
        {
          name: 'write_date',
          prop: 'write_date',
          dataType: 'DATETIME',
        },
        {
          name: 'srfkey',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'srfdataaccaction',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'state_id',
          prop: 'state_id',
          dataType: 'PICKUP',
        },
        {
          name: 'title',
          prop: 'title',
          dataType: 'PICKUP',
        },
        {
          name: 'time',
          prop: 'create_date',
          dataType: 'DATETIME',
        },
        {
          name: 'email',
          prop: 'email',
          dataType: 'TEXT',
        },
        {
          name: 'write_uid_text',
          prop: 'write_uid_text',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'name',
          prop: 'name',
          dataType: 'TEXT',
        },
        {
          name: 'user_id',
          prop: 'user_id',
          dataType: 'PICKUP',
        },
        {
          name: 'contact_address',
          prop: 'contact_address',
          dataType: 'TEXT',
        },
        {
          name: 'team_id',
          prop: 'team_id',
          dataType: 'PICKUP',
        },
        {
          name: 'commercial_partner_id',
          prop: 'commercial_partner_id',
          dataType: 'PICKUP',
        },
        {
          name: 'parent_id',
          prop: 'parent_id',
          dataType: 'PICKUP',
        },
        {
          name: 'mobile',
          prop: 'mobile',
          dataType: 'TEXT',
        },
        {
          name: 'res_partner',
          prop: 'id',
        },
        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}