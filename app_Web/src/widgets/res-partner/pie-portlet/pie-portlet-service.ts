import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * Pie 部件服务对象
 *
 * @export
 * @class PieService
 */
export default class PieService extends ControlService {
}