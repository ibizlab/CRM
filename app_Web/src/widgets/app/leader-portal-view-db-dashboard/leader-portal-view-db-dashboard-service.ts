import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * LeaderPortalView_db 部件服务对象
 *
 * @export
 * @class LeaderPortalView_dbService
 */
export default class LeaderPortalView_dbService extends ControlService {
}