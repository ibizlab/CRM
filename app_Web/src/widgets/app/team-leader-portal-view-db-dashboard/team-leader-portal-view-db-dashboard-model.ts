/**
 * TeamLeaderPortalView_db 部件模型
 *
 * @export
 * @class TeamLeaderPortalView_dbModel
 */
export default class TeamLeaderPortalView_dbModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TeamLeaderPortalView_dbModel
    */
  public getDataItems(): any[] {
    return [
    ]
  }


}