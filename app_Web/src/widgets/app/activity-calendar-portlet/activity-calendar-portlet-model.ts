/**
 * ActivityCalendar 部件模型
 *
 * @export
 * @class ActivityCalendarModel
 */
export default class ActivityCalendarModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof ActivityCalendarModel
    */
  public getDataItems(): any[] {
    return [
    ]
  }


}