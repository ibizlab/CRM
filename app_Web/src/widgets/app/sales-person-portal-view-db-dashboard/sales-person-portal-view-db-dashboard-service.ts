import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * SalesPersonPortalView_db 部件服务对象
 *
 * @export
 * @class SalesPersonPortalView_dbService
 */
export default class SalesPersonPortalView_dbService extends ControlService {
}