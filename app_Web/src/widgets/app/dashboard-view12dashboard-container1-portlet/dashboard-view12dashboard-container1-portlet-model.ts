/**
 * DashboardView12dashboard_container1 部件模型
 *
 * @export
 * @class DashboardView12dashboard_container1Model
 */
export default class DashboardView12dashboard_container1Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof DashboardView12dashboard_container1Model
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'image',
      },
      {
        name: 'type',
      },
      {
        name: 'color',
      },
      {
        name: 'payment_token_ids',
      },
      {
        name: 'invoice_ids',
      },
      {
        name: 'meeting_count',
      },
      {
        name: 'supplier_invoice_count',
      },
      {
        name: 'company_name',
      },
      {
        name: 'website_published',
      },
      {
        name: 'last_time_entries_checked',
      },
      {
        name: 'message_unread',
      },
      {
        name: 'trust',
      },
      {
        name: 'ibizfunction',
      },
      {
        name: 'total_invoiced',
      },
      {
        name: 'pos_order_count',
      },
      {
        name: 'contact_address',
      },
      {
        name: 'invoice_warn',
      },
      {
        name: 'bank_ids',
      },
      {
        name: 'signup_expiration',
      },
      {
        name: 'purchase_order_count',
      },
      {
        name: 'has_unreconciled_entries',
      },
      {
        name: 'category_id',
      },
      {
        name: 'website_description',
      },
      {
        name: 'message_main_attachment_id',
      },
      {
        name: 'meeting_ids',
      },
      {
        name: 'employee',
      },
      {
        name: 'display_name',
      },
      {
        name: 'child_ids',
      },
      {
        name: 'website_meta_description',
      },
      {
        name: 'is_blacklisted',
      },
      {
        name: 'property_product_pricelist',
      },
      {
        name: 'activity_date_deadline',
      },
      {
        name: 'activity_type_id',
      },
      {
        name: 'signup_token',
      },
      {
        name: 'ref_company_ids',
      },
      {
        name: 'is_company',
      },
      {
        name: 'phone',
      },
      {
        name: 'create_date',
      },
      {
        name: 'tz',
      },
      {
        name: 'event_count',
      },
      {
        name: 'message_has_error',
      },
      {
        name: 'calendar_last_notif_ack',
      },
      {
        name: 'message_channel_ids',
      },
      {
        name: 'signup_type',
      },
      {
        name: 'email_formatted',
      },
      {
        name: 'website_message_ids',
      },
      {
        name: 'partner_share',
      },
      {
        name: 'street2',
      },
      {
        name: 'debit',
      },
      {
        name: 'payment_token_count',
      },
      {
        name: 'ref',
      },
      {
        name: 'partner_gid',
      },
      {
        name: 'signup_valid',
      },
      {
        name: 'website_meta_og_img',
      },
      {
        name: 'image_small',
      },
      {
        name: 'bank_account_count',
      },
      {
        name: 'street',
      },
      {
        name: 'sale_warn',
      },
      {
        name: 'message_bounce',
      },
      {
        name: 'message_needaction_counter',
      },
      {
        name: 'message_follower_ids',
      },
      {
        name: 'opportunity_count',
      },
      {
        name: 'date',
      },
      {
        name: '__last_update',
      },
      {
        name: 'message_partner_ids',
      },
      {
        name: 'self',
      },
      {
        name: 'im_status',
      },
      {
        name: 'customer',
      },
      {
        name: 'write_date',
      },
      {
        name: 'message_has_error_counter',
      },
      {
        name: 'invoice_warn_msg',
      },
      {
        name: 'message_needaction',
      },
      {
        name: 'picking_warn',
      },
      {
        name: 'contract_ids',
      },
      {
        name: 'currency_id',
      },
      {
        name: 'website',
      },
      {
        name: 'mobile',
      },
      {
        name: 'message_attachment_count',
      },
      {
        name: 'city',
      },
      {
        name: 'property_payment_term_id',
      },
      {
        name: 'user_ids',
      },
      {
        name: 'website_meta_keywords',
      },
      {
        name: 'channel_ids',
      },
      {
        name: 'purchase_warn',
      },
      {
        name: 'journal_item_count',
      },
      {
        name: 'supplier',
      },
      {
        name: 'property_stock_supplier',
      },
      {
        name: 'property_account_payable_id',
      },
      {
        name: 'website_short_description',
      },
      {
        name: 'sale_warn_msg',
      },
      {
        name: 'credit',
      },
      {
        name: 'activity_state',
      },
      {
        name: 'activity_ids',
      },
      {
        name: 'message_is_follower',
      },
      {
        name: 'name',
      },
      {
        name: 'vat',
      },
      {
        name: 'property_supplier_payment_term_id',
      },
      {
        name: 'property_stock_customer',
      },
      {
        name: 'comment',
      },
      {
        name: 'task_ids',
      },
      {
        name: 'message_unread_counter',
      },
      {
        name: 'email',
      },
      {
        name: 'purchase_warn_msg',
      },
      {
        name: 'website_meta_title',
      },
      {
        name: 'zip',
      },
      {
        name: 'tz_offset',
      },
      {
        name: 'company_type',
      },
      {
        name: 'activity_summary',
      },
      {
        name: 'task_count',
      },
      {
        name: 'credit_limit',
      },
      {
        name: 'property_account_receivable_id',
      },
      {
        name: 'property_purchase_currency_id',
      },
      {
        name: 'picking_warn_msg',
      },
      {
        name: 'res_partner',
        prop: 'id',
      },
      {
        name: 'signup_url',
      },
      {
        name: 'lang',
      },
      {
        name: 'message_ids',
      },
      {
        name: 'property_account_position_id',
      },
      {
        name: 'website_id',
      },
      {
        name: 'active',
      },
      {
        name: 'barcode',
      },
      {
        name: 'is_published',
      },
      {
        name: 'activity_user_id',
      },
      {
        name: 'sale_order_count',
      },
      {
        name: 'image_medium',
      },
      {
        name: 'additional_info',
      },
      {
        name: 'opportunity_ids',
      },
      {
        name: 'contracts_count',
      },
      {
        name: 'debit_limit',
      },
      {
        name: 'website_url',
      },
      {
        name: 'sale_order_ids',
      },
      {
        name: 'last_website_so_id',
      },
      {
        name: 'is_seo_optimized',
      },
      {
        name: 'commercial_company_name',
      },
      {
        name: 'write_uid_text',
      },
      {
        name: 'title_text',
      },
      {
        name: 'company_id_text',
      },
      {
        name: 'country_id_text',
      },
      {
        name: 'state_id_text',
      },
      {
        name: 'commercial_partner_id_text',
      },
      {
        name: 'parent_name',
      },
      {
        name: 'user_id_text',
      },
      {
        name: 'create_uid_text',
      },
      {
        name: 'industry_id_text',
      },
      {
        name: 'team_id_text',
      },
      {
        name: 'team_id',
      },
      {
        name: 'state_id',
      },
      {
        name: 'user_id',
      },
      {
        name: 'create_uid',
      },
      {
        name: 'parent_id',
      },
      {
        name: 'title',
      },
      {
        name: 'write_uid',
      },
      {
        name: 'commercial_partner_id',
      },
      {
        name: 'industry_id',
      },
      {
        name: 'company_id',
      },
      {
        name: 'country_id',
      },
    ]
  }


}