/**
 * Default 部件模型
 *
 * @export
 * @class DefaultModel
 */
export default class DefaultModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof DefaultModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'n_activity_state_eq',
        prop: 'activity_state',
        dataType: 'SSCODELIST',
      },
      {
        name: 'n_campaign_id_text_like',
        prop: 'campaign_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_company_id_text_eq',
        prop: 'company_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_create_uid_text_like',
        prop: 'create_uid_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_create_uid_eq',
        prop: 'create_uid',
        dataType: 'PICKUP',
      },
      {
        name: 'n_company_id_eq',
        prop: 'company_id',
        dataType: 'PICKUP',
      },
      {
        name: 'n_campaign_id_eq',
        prop: 'campaign_id',
        dataType: 'PICKUP',
      },
      {
        name: 'n_medium_id_text_like',
        prop: 'medium_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_lost_reason_text_like',
        prop: 'lost_reason_text',
        dataType: 'PICKUPTEXT',
      },
    ]
  }

}