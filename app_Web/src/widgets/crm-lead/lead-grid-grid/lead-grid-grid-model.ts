/**
 * LeadGrid 部件模型
 *
 * @export
 * @class LeadGridModel
 */
export default class LeadGridModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof LeadGridGridexpbar_gridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof LeadGridGridexpbar_gridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'partner_id',
          prop: 'partner_id',
          dataType: 'PICKUP',
        },
        {
          name: 'company_id',
          prop: 'company_id',
          dataType: 'PICKUP',
        },
        {
          name: 'write_uid',
          prop: 'write_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'source_id',
          prop: 'source_id',
          dataType: 'PICKUP',
        },
        {
          name: 'country_id',
          prop: 'country_id',
          dataType: 'PICKUP',
        },
        {
          name: 'create_uid',
          prop: 'create_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'team_id_text',
          prop: 'team_id_text',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'lost_reason',
          prop: 'lost_reason',
          dataType: 'PICKUP',
        },
        {
          name: 'medium_id',
          prop: 'medium_id',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'name',
          dataType: 'TEXT',
        },
        {
          name: 'campaign_id',
          prop: 'campaign_id',
          dataType: 'PICKUP',
        },
        {
          name: 'srfkey',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'srfdataaccaction',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'state_id',
          prop: 'state_id',
          dataType: 'PICKUP',
        },
        {
          name: 'title',
          prop: 'title',
          dataType: 'PICKUP',
        },
        {
          name: 'user_id_text',
          prop: 'user_id_text',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'name',
          prop: 'name',
          dataType: 'TEXT',
        },
        {
          name: 'planned_revenue',
          prop: 'planned_revenue',
          dataType: 'DECIMAL',
        },
        {
          name: 'user_id',
          prop: 'user_id',
          dataType: 'PICKUP',
        },
        {
          name: 'partner_name',
          prop: 'partner_name',
          dataType: 'TEXT',
        },
        {
          name: 'team_id',
          prop: 'team_id',
          dataType: 'PICKUP',
        },
        {
          name: 'stage_id',
          prop: 'stage_id',
          dataType: 'PICKUP',
        },
        {
          name: 'crm_lead',
          prop: 'id',
        },
        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}