import { Http,Util,Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import Crm_leadService from '@/service/crm-lead/crm-lead-service';
import LeadCardModel from './lead-card-dataview-model';


/**
 * LeadCard 部件服务对象
 *
 * @export
 * @class LeadCardService
 */
export default class LeadCardService extends ControlService {

    /**
     * 线索/商机服务对象
     *
     * @type {Crm_leadService}
     * @memberof LeadCardService
     */
    public appEntityService: Crm_leadService = new Crm_leadService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof LeadCardService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of LeadCardService.
     * 
     * @param {*} [opts={}]
     * @memberof LeadCardService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new LeadCardModel();
    }




    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadCardService
     */
    @Errorlog
    public search(action: string,context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            const _appEntityService: any = this.appEntityService;
            let result: Promise<any>;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            }else{
                result =_appEntityService.FetchDefault(Context,Data, isloading);
            }
            result.then((response) => {
                this.handleResponse(action, response);
                resolve(response);
            }).catch(response => {
                reject(response);
            });      
        });
    }

    /**
     * 删除数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LeadCardService
     */
    @Errorlog
    public delete(action: string,context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            const _appEntityService: any = this.appEntityService;
            let result: Promise<any>;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            }else{
                result =_appEntityService.remove(Context,Data, isloading);
            }
            result.then((response) => {
                this.handleResponse(action, response);
                resolve(response);
            }).catch(response => {
                reject(response);
            });      
        });
    }



}