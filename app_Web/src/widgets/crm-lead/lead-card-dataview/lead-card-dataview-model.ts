/**
 * LeadCard 部件模型
 *
 * @export
 * @class LeadCardModel
 */
export default class LeadCardModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof LeadCardDataViewMode
	 */
	public getDataItems(): any[] {
		return [
			{
				name: 'srficonpath',
			},
			{
				name: 'srfkey',
				prop: 'id',
				dataType: 'ACID',
			},
			{
				name: 'srfmajortext',
				prop: 'name',
				dataType: 'TEXT',
			},

			{
				name: 'crm_lead',
				prop: 'id',
				dataType: 'FONTKEY',
			},

      {
        name: 'n_activity_state_eq',
        prop: 'n_activity_state_eq',
        dataType: 'SSCODELIST',
      },
      {
        name: 'n_campaign_id_text_like',
        prop: 'n_campaign_id_text_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_company_id_text_eq',
        prop: 'n_company_id_text_eq',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_create_uid_text_like',
        prop: 'n_create_uid_text_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_create_uid_eq',
        prop: 'n_create_uid_eq',
        dataType: 'PICKUP',
      },
      {
        name: 'n_company_id_eq',
        prop: 'n_company_id_eq',
        dataType: 'PICKUP',
      },
      {
        name: 'n_campaign_id_eq',
        prop: 'n_campaign_id_eq',
        dataType: 'PICKUP',
      },
      {
        name: 'n_medium_id_text_like',
        prop: 'n_medium_id_text_like',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_lost_reason_text_like',
        prop: 'n_lost_reason_text_like',
        dataType: 'PICKUPTEXT',
      },


      {
        name:'size',
        prop:'size'
      },
      {
        name:'query',
        prop:'query'
      },
      {
        name:'sort',
        prop:'sort'
      },
      {
        name:'page',
        prop:'page'
      },
      {
        name:'srfparentdata',
        prop:'srfparentdata'
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
		]
	}

}