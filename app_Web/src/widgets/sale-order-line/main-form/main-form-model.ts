/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof MainModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name',
        dataType: 'LONGTEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'product_id_text',
        prop: 'product_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'price_unit',
        prop: 'price_unit',
        dataType: 'FLOAT',
      },
      {
        name: 'product_uom_qty',
        prop: 'product_uom_qty',
        dataType: 'FLOAT',
      },
      {
        name: 'product_uom_text',
        prop: 'product_uom_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'name',
        prop: 'name',
        dataType: 'LONGTEXT',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'product_id',
        prop: 'product_id',
        dataType: 'PICKUP',
      },
      {
        name: 'product_uom',
        prop: 'product_uom',
        dataType: 'PICKUP',
      },
      {
        name: 'sale_order',
        prop: 'id',
        dataType: 'FONTKEY',
      },
      {
        name: 'sale_order_line',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}