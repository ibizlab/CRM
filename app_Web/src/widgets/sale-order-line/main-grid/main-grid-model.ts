/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'company_id',
          prop: 'company_id',
          dataType: 'PICKUP',
        },
        {
          name: 'product_packaging',
          prop: 'product_packaging',
          dataType: 'PICKUP',
        },
        {
          name: 'product_uom_text',
          prop: 'product_uom_text',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'currency_id',
          prop: 'currency_id',
          dataType: 'PICKUP',
        },
        {
          name: 'write_uid',
          prop: 'write_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'product_id',
          prop: 'product_id',
          dataType: 'PICKUP',
        },
        {
          name: 'salesman_id',
          prop: 'salesman_id',
          dataType: 'PICKUP',
        },
        {
          name: 'create_uid',
          prop: 'create_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'linked_line_id',
          prop: 'linked_line_id',
          dataType: 'PICKUP',
        },
        {
          name: 'event_id',
          prop: 'event_id',
          dataType: 'PICKUP',
        },
        {
          name: 'product_uom',
          prop: 'product_uom',
          dataType: 'PICKUP',
        },
        {
          name: 'order_id',
          prop: 'order_id',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'name',
          dataType: 'LONGTEXT',
        },
        {
          name: 'product_id_text',
          prop: 'product_id_text',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'srfkey',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'srfdataaccaction',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'product_uom_qty',
          prop: 'product_uom_qty',
          dataType: 'FLOAT',
        },
        {
          name: 'event_ticket_id',
          prop: 'event_ticket_id',
          dataType: 'PICKUP',
        },
        {
          name: 'route_id',
          prop: 'route_id',
          dataType: 'PICKUP',
        },
        {
          name: 'price_unit',
          prop: 'price_unit',
          dataType: 'FLOAT',
        },
        {
          name: 'order_partner_id',
          prop: 'order_partner_id',
          dataType: 'PICKUP',
        },
        {
          name: 'sale_order',
          prop: 'id',
          dataType: 'FONTKEY',
        },
        {
          name: 'sale_order_line',
          prop: 'id',
        },
      {
        name: 'sale_order',
        prop: 'id',
        dataType: 'FONTKEY',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}