/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'name',
      },
      {
        name: 'display_name',
      },
      {
        name: 'sequence',
      },
      {
        name: 'selectable',
      },
      {
        name: 'discount_policy',
      },
      {
        name: 'country_group_ids',
      },
      {
        name: 'code',
      },
      {
        name: 'website_id',
      },
      {
        name: 'active',
      },
      {
        name: 'write_date',
      },
      {
        name: 'create_date',
      },
      {
        name: 'item_ids',
      },
      {
        name: 'product_pricelist',
        prop: 'id',
      },
      {
        name: '__last_update',
      },
      {
        name: 'currency_id_text',
      },
      {
        name: 'write_uid_text',
      },
      {
        name: 'create_uid_text',
      },
      {
        name: 'company_id_text',
      },
      {
        name: 'create_uid',
      },
      {
        name: 'write_uid',
      },
      {
        name: 'currency_id',
      },
      {
        name: 'company_id',
      },
    ]
  }


}