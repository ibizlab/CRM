/**
 * MPickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class MPickupViewpickupviewpanelModel
 */
export default class MPickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof MPickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'display_name',
      },
      {
        name: 'fixed_price',
      },
      {
        name: 'name',
      },
      {
        name: 'price',
      },
      {
        name: '__last_update',
      },
      {
        name: 'base',
      },
      {
        name: 'date_end',
      },
      {
        name: 'percent_price',
      },
      {
        name: 'price_round',
      },
      {
        name: 'write_date',
      },
      {
        name: 'product_pricelist_item',
        prop: 'id',
      },
      {
        name: 'create_date',
      },
      {
        name: 'compute_price',
      },
      {
        name: 'price_min_margin',
      },
      {
        name: 'date_start',
      },
      {
        name: 'price_max_margin',
      },
      {
        name: 'applied_on',
      },
      {
        name: 'min_quantity',
      },
      {
        name: 'price_surcharge',
      },
      {
        name: 'price_discount',
      },
      {
        name: 'pricelist_id_text',
      },
      {
        name: 'currency_id_text',
      },
      {
        name: 'categ_id_text',
      },
      {
        name: 'base_pricelist_id_text',
      },
      {
        name: 'write_uid_text',
      },
      {
        name: 'product_tmpl_id_text',
      },
      {
        name: 'create_uid_text',
      },
      {
        name: 'company_id_text',
      },
      {
        name: 'product_id_text',
      },
      {
        name: 'product_id',
      },
      {
        name: 'pricelist_id',
      },
      {
        name: 'write_uid',
      },
      {
        name: 'company_id',
      },
      {
        name: 'base_pricelist_id',
      },
      {
        name: 'currency_id',
      },
      {
        name: 'categ_id',
      },
      {
        name: 'create_uid',
      },
      {
        name: 'product_tmpl_id',
      },
    ]
  }


}