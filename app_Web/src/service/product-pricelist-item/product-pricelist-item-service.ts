import { Http,Util } from '@/utils';
import Product_pricelist_itemServiceBase from './product-pricelist-item-service-base';


/**
 * 价格表明细服务对象
 *
 * @export
 * @class Product_pricelist_itemService
 * @extends {Product_pricelist_itemServiceBase}
 */
export default class Product_pricelist_itemService extends Product_pricelist_itemServiceBase {

    /**
     * Creates an instance of  Product_pricelist_itemService.
     * 
     * @param {*} [opts={}]
     * @memberof  Product_pricelist_itemService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}