import { Http,Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 客户服务对象基类
 *
 * @export
 * @class Res_partnerServiceBase
 * @extends {EntityServie}
 */
export default class Res_partnerServiceBase extends EntityService {

    /**
     * Creates an instance of  Res_partnerServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_partnerServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof Res_partnerServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='res_partner';
        this.APPDEKEY = 'id';
        this.APPDENAME = 'res_partners';
        this.APPDETEXT = 'name';
        this.APPNAME = 'web';
        this.SYSTEMNAME = 'crm';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partnerServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
            return Http.getInstance().post(`/res_partners/${context.res_partner}/select`,data,isloading);
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partnerServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
            return Http.getInstance().delete(`/res_partners/${context.res_partner}`,isloading);

    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partnerServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
            let res:any = await Http.getInstance().get(`/res_partners/${context.res_partner}`,isloading);
            this.tempStorage.setItem(context.srfsessionkey+'_account_payments',JSON.stringify(res.data.account_payments));
            this.tempStorage.setItem(context.srfsessionkey+'_sale_orders',JSON.stringify(res.data.sale_orders));
            this.tempStorage.setItem(context.srfsessionkey+'_crm_leads',JSON.stringify(res.data.crm_leads));
            this.tempStorage.setItem(context.srfsessionkey+'_account_invoices',JSON.stringify(res.data.account_invoices));
            this.tempStorage.setItem(context.srfsessionkey+'_res_partner_ps',JSON.stringify(res.data.res_partner_ps));
            return res;

    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partnerServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
            return Http.getInstance().post(`/res_partners/${context.res_partner}/checkkey`,data,isloading);
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partnerServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let masterData:any = {};
        let account_paymentsData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_account_payments'),'undefined')){
            account_paymentsData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_account_payments') as any);
            if(account_paymentsData && account_paymentsData.length && account_paymentsData.length > 0){
                account_paymentsData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.id = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.account_payments = account_paymentsData;
        let sale_ordersData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_sale_orders'),'undefined')){
            sale_ordersData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_sale_orders') as any);
            if(sale_ordersData && sale_ordersData.length && sale_ordersData.length > 0){
                sale_ordersData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.id = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.sale_orders = sale_ordersData;
        let crm_leadsData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_crm_leads'),'undefined')){
            crm_leadsData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_crm_leads') as any);
            if(crm_leadsData && crm_leadsData.length && crm_leadsData.length > 0){
                crm_leadsData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.id = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.crm_leads = crm_leadsData;
        let account_invoicesData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_account_invoices'),'undefined')){
            account_invoicesData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_account_invoices') as any);
            if(account_invoicesData && account_invoicesData.length && account_invoicesData.length > 0){
                account_invoicesData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.id = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.account_invoices = account_invoicesData;
        let res_partner_psData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_res_partner_ps'),'undefined')){
            res_partner_psData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_res_partner_ps') as any);
            if(res_partner_psData && res_partner_psData.length && res_partner_psData.length > 0){
                res_partner_psData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.id = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.res_partner_ps = res_partner_psData;
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/res_partners/${context.res_partner}/save`,data,isloading);
            this.tempStorage.setItem(context.srfsessionkey+'_account_payments',JSON.stringify(res.data.account_payments));
            this.tempStorage.setItem(context.srfsessionkey+'_sale_orders',JSON.stringify(res.data.sale_orders));
            this.tempStorage.setItem(context.srfsessionkey+'_crm_leads',JSON.stringify(res.data.crm_leads));
            this.tempStorage.setItem(context.srfsessionkey+'_account_invoices',JSON.stringify(res.data.account_invoices));
            this.tempStorage.setItem(context.srfsessionkey+'_res_partner_ps',JSON.stringify(res.data.res_partner_ps));
            return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partnerServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let masterData:any = {};
        let account_paymentsData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_account_payments'),'undefined')){
            account_paymentsData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_account_payments') as any);
            if(account_paymentsData && account_paymentsData.length && account_paymentsData.length > 0){
                account_paymentsData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.id = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.account_payments = account_paymentsData;
        let sale_ordersData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_sale_orders'),'undefined')){
            sale_ordersData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_sale_orders') as any);
            if(sale_ordersData && sale_ordersData.length && sale_ordersData.length > 0){
                sale_ordersData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.id = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.sale_orders = sale_ordersData;
        let crm_leadsData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_crm_leads'),'undefined')){
            crm_leadsData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_crm_leads') as any);
            if(crm_leadsData && crm_leadsData.length && crm_leadsData.length > 0){
                crm_leadsData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.id = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.crm_leads = crm_leadsData;
        let account_invoicesData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_account_invoices'),'undefined')){
            account_invoicesData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_account_invoices') as any);
            if(account_invoicesData && account_invoicesData.length && account_invoicesData.length > 0){
                account_invoicesData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.id = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.account_invoices = account_invoicesData;
        let res_partner_psData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_res_partner_ps'),'undefined')){
            res_partner_psData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_res_partner_ps') as any);
            if(res_partner_psData && res_partner_psData.length && res_partner_psData.length > 0){
                res_partner_psData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.id = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.res_partner_ps = res_partner_psData;
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/res_partners/${context.res_partner}`,data,isloading);
            this.tempStorage.setItem(context.srfsessionkey+'_account_payments',JSON.stringify(res.data.account_payments));
            this.tempStorage.setItem(context.srfsessionkey+'_sale_orders',JSON.stringify(res.data.sale_orders));
            this.tempStorage.setItem(context.srfsessionkey+'_crm_leads',JSON.stringify(res.data.crm_leads));
            this.tempStorage.setItem(context.srfsessionkey+'_account_invoices',JSON.stringify(res.data.account_invoices));
            this.tempStorage.setItem(context.srfsessionkey+'_res_partner_ps',JSON.stringify(res.data.res_partner_ps));
            return res;
    }

    /**
     * CreateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partnerServiceBase
     */
    public async CreateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
            return Http.getInstance().post(`/res_partners/${context.res_partner}/createbatch`,data,isloading);
    }

    /**
     * UpdateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partnerServiceBase
     */
    public async UpdateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
            return Http.getInstance().put(`/res_partners/${context.res_partner}/updatebatch`,data,isloading);
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partnerServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let res:any = await  Http.getInstance().get(`/res_partners/getdraft`,isloading);
        res.data.res_partner = data.res_partner;
            this.tempStorage.setItem(context.srfsessionkey+'_account_payments',JSON.stringify(res.data.account_payments));
            this.tempStorage.setItem(context.srfsessionkey+'_sale_orders',JSON.stringify(res.data.sale_orders));
            this.tempStorage.setItem(context.srfsessionkey+'_crm_leads',JSON.stringify(res.data.crm_leads));
            this.tempStorage.setItem(context.srfsessionkey+'_account_invoices',JSON.stringify(res.data.account_invoices));
            this.tempStorage.setItem(context.srfsessionkey+'_res_partner_ps',JSON.stringify(res.data.res_partner_ps));
        return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partnerServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let masterData:any = {};
        let account_paymentsData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_account_payments'),'undefined')){
            account_paymentsData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_account_payments') as any);
            if(account_paymentsData && account_paymentsData.length && account_paymentsData.length > 0){
                account_paymentsData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.id = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.account_payments = account_paymentsData;
        let sale_ordersData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_sale_orders'),'undefined')){
            sale_ordersData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_sale_orders') as any);
            if(sale_ordersData && sale_ordersData.length && sale_ordersData.length > 0){
                sale_ordersData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.id = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.sale_orders = sale_ordersData;
        let crm_leadsData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_crm_leads'),'undefined')){
            crm_leadsData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_crm_leads') as any);
            if(crm_leadsData && crm_leadsData.length && crm_leadsData.length > 0){
                crm_leadsData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.id = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.crm_leads = crm_leadsData;
        let account_invoicesData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_account_invoices'),'undefined')){
            account_invoicesData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_account_invoices') as any);
            if(account_invoicesData && account_invoicesData.length && account_invoicesData.length > 0){
                account_invoicesData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.id = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.account_invoices = account_invoicesData;
        let res_partner_psData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_res_partner_ps'),'undefined')){
            res_partner_psData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_res_partner_ps') as any);
            if(res_partner_psData && res_partner_psData.length && res_partner_psData.length > 0){
                res_partner_psData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.id = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.res_partner_ps = res_partner_psData;
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/res_partners`,data,isloading);
        this.tempStorage.setItem(tempContext.srfsessionkey+'_account_payments',JSON.stringify(res.data.account_payments));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_sale_orders',JSON.stringify(res.data.sale_orders));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_crm_leads',JSON.stringify(res.data.crm_leads));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_account_invoices',JSON.stringify(res.data.account_invoices));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_res_partner_ps',JSON.stringify(res.data.res_partner_ps));
        return res;
    }

    /**
     * RemoveBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partnerServiceBase
     */
    public async RemoveBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
            return Http.getInstance().delete(`/res_partners/${context.res_partner}/removebatch`,isloading);

    }

    /**
     * FetchContacts接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partnerServiceBase
     */
    public async FetchContacts(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let tempData:any = JSON.parse(JSON.stringify(data));
        return Http.getInstance().get(`/res_partners/fetchcontacts`,tempData,isloading);
    }

    /**
     * FetchCompany接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partnerServiceBase
     */
    public async FetchCompany(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let tempData:any = JSON.parse(JSON.stringify(data));
        return Http.getInstance().get(`/res_partners/fetchcompany`,tempData,isloading);
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partnerServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let tempData:any = JSON.parse(JSON.stringify(data));
        return Http.getInstance().get(`/res_partners/fetchdefault`,tempData,isloading);
    }
}