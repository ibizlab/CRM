import { Http,Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 销售渠道服务对象基类
 *
 * @export
 * @class Crm_teamServiceBase
 * @extends {EntityServie}
 */
export default class Crm_teamServiceBase extends EntityService {

    /**
     * Creates an instance of  Crm_teamServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  Crm_teamServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof Crm_teamServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='crm_team';
        this.APPDEKEY = 'id';
        this.APPDENAME = 'crm_teams';
        this.APPDETEXT = 'name';
        this.APPNAME = 'web';
        this.SYSTEMNAME = 'crm';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Crm_teamServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
            return Http.getInstance().post(`/crm_teams/${context.crm_team}/select`,data,isloading);
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Crm_teamServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
            let res:any = await Http.getInstance().get(`/crm_teams/${context.crm_team}`,isloading);
            this.tempStorage.setItem(context.srfsessionkey+'_res_users',JSON.stringify(res.data.res_users));
            return res;

    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Crm_teamServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
            return Http.getInstance().post(`/crm_teams/${context.crm_team}/checkkey`,data,isloading);
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Crm_teamServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let res:any = await  Http.getInstance().get(`/crm_teams/getdraft`,isloading);
        res.data.crm_team = data.crm_team;
            this.tempStorage.setItem(context.srfsessionkey+'_res_users',JSON.stringify(res.data.res_users));
        return res;
    }

    /**
     * RemoveBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Crm_teamServiceBase
     */
    public async RemoveBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
            return Http.getInstance().delete(`/crm_teams/${context.crm_team}/removebatch`,isloading);

    }

    /**
     * CreateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Crm_teamServiceBase
     */
    public async CreateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
            return Http.getInstance().post(`/crm_teams/${context.crm_team}/createbatch`,data,isloading);
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Crm_teamServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let masterData:any = {};
        let res_usersData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_res_users'),'undefined')){
            res_usersData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_res_users') as any);
            if(res_usersData && res_usersData.length && res_usersData.length > 0){
                res_usersData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.id = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.res_users = res_usersData;
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/crm_teams/${context.crm_team}`,data,isloading);
            this.tempStorage.setItem(context.srfsessionkey+'_res_users',JSON.stringify(res.data.res_users));
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Crm_teamServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
            return Http.getInstance().delete(`/crm_teams/${context.crm_team}`,isloading);

    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Crm_teamServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let masterData:any = {};
        let res_usersData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_res_users'),'undefined')){
            res_usersData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_res_users') as any);
            if(res_usersData && res_usersData.length && res_usersData.length > 0){
                res_usersData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.id = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.res_users = res_usersData;
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/crm_teams`,data,isloading);
        this.tempStorage.setItem(tempContext.srfsessionkey+'_res_users',JSON.stringify(res.data.res_users));
        return res;
    }

    /**
     * UpdateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Crm_teamServiceBase
     */
    public async UpdateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
            return Http.getInstance().put(`/crm_teams/${context.crm_team}/updatebatch`,data,isloading);
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Crm_teamServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let masterData:any = {};
        let res_usersData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_res_users'),'undefined')){
            res_usersData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_res_users') as any);
            if(res_usersData && res_usersData.length && res_usersData.length > 0){
                res_usersData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.id = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.res_users = res_usersData;
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/crm_teams/${context.crm_team}/save`,data,isloading);
            this.tempStorage.setItem(context.srfsessionkey+'_res_users',JSON.stringify(res.data.res_users));
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Crm_teamServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        let tempData:any = JSON.parse(JSON.stringify(data));
        return Http.getInstance().get(`/crm_teams/fetchdefault`,tempData,isloading);
    }
}