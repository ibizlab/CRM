import { Http,Util } from '@/utils';
import Mail_activityServiceBase from './mail-activity-service-base';


/**
 * 活动服务对象
 *
 * @export
 * @class Mail_activityService
 * @extends {Mail_activityServiceBase}
 */
export default class Mail_activityService extends Mail_activityServiceBase {

    /**
     * Creates an instance of  Mail_activityService.
     * 
     * @param {*} [opts={}]
     * @memberof  Mail_activityService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}