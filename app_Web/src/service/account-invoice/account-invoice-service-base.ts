import { Http,Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 发票服务对象基类
 *
 * @export
 * @class Account_invoiceServiceBase
 * @extends {EntityServie}
 */
export default class Account_invoiceServiceBase extends EntityService {

    /**
     * Creates an instance of  Account_invoiceServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_invoiceServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof Account_invoiceServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='account_invoice';
        this.APPDEKEY = 'id';
        this.APPDENAME = 'account_invoices';
        this.APPDETEXT = 'name';
        this.APPNAME = 'web';
        this.SYSTEMNAME = 'crm';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Account_invoiceServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.account_invoice){
            return Http.getInstance().post(`/res_partners/${context.res_partner}/account_invoices/${context.account_invoice}/select`,data,isloading);
        }
            return Http.getInstance().post(`/account_invoices/${context.account_invoice}/select`,data,isloading);
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Account_invoiceServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.account_invoice){
            return Http.getInstance().delete(`/res_partners/${context.res_partner}/account_invoices/${context.account_invoice}`,isloading);
        }
            return Http.getInstance().delete(`/account_invoices/${context.account_invoice}`,isloading);

    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Account_invoiceServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.account_invoice){
            return Http.getInstance().post(`/res_partners/${context.res_partner}/account_invoices/${context.account_invoice}/checkkey`,data,isloading);
        }
            return Http.getInstance().post(`/account_invoices/${context.account_invoice}/checkkey`,data,isloading);
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Account_invoiceServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && true){
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            return Http.getInstance().post(`/res_partners/${context.res_partner}/account_invoices`,data,isloading);
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/account_invoices`,data,isloading);
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Account_invoiceServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.account_invoice){
            return Http.getInstance().put(`/res_partners/${context.res_partner}/account_invoices/${context.account_invoice}`,data,isloading);
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/account_invoices/${context.account_invoice}`,data,isloading);
            return res;
    }

    /**
     * UpdateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Account_invoiceServiceBase
     */
    public async UpdateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.account_invoice){
            return Http.getInstance().put(`/res_partners/${context.res_partner}/account_invoices/${context.account_invoice}/updatebatch`,data,isloading);
        }
            return Http.getInstance().put(`/account_invoices/${context.account_invoice}/updatebatch`,data,isloading);
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Account_invoiceServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.account_invoice){
            return Http.getInstance().get(`/res_partners/${context.res_partner}/account_invoices/${context.account_invoice}`,isloading);
        }
            let res:any = await Http.getInstance().get(`/account_invoices/${context.account_invoice}`,isloading);
            return res;

    }

    /**
     * CreateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Account_invoiceServiceBase
     */
    public async CreateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.account_invoice){
            return Http.getInstance().post(`/res_partners/${context.res_partner}/account_invoices/${context.account_invoice}/createbatch`,data,isloading);
        }
            return Http.getInstance().post(`/account_invoices/${context.account_invoice}/createbatch`,data,isloading);
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Account_invoiceServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && true){
            return Http.getInstance().get(`/res_partners/${context.res_partner}/account_invoices/getdraft`,isloading);
        }
        let res:any = await  Http.getInstance().get(`/account_invoices/getdraft`,isloading);
        res.data.account_invoice = data.account_invoice;
        return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Account_invoiceServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.account_invoice){
            return Http.getInstance().post(`/res_partners/${context.res_partner}/account_invoices/${context.account_invoice}/save`,data,isloading);
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/account_invoices/${context.account_invoice}/save`,data,isloading);
            return res;
    }

    /**
     * RemoveBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Account_invoiceServiceBase
     */
    public async RemoveBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.account_invoice){
            return Http.getInstance().delete(`/res_partners/${context.res_partner}/account_invoices/${context.account_invoice}/removebatch`,isloading);
        }
            return Http.getInstance().delete(`/account_invoices/${context.account_invoice}/removebatch`,isloading);

    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Account_invoiceServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return Http.getInstance().get(`/res_partners/${context.res_partner}/account_invoices/fetchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return Http.getInstance().get(`/account_invoices/fetchdefault`,tempData,isloading);
    }
}