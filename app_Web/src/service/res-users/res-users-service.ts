import { Http,Util } from '@/utils';
import Res_usersServiceBase from './res-users-service-base';


/**
 * 用户服务对象
 *
 * @export
 * @class Res_usersService
 * @extends {Res_usersServiceBase}
 */
export default class Res_usersService extends Res_usersServiceBase {

    /**
     * Creates an instance of  Res_usersService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_usersService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}