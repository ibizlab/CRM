import { Http,Util } from '@/utils';
import Account_invoice_lineServiceBase from './account-invoice-line-service-base';


/**
 * 发票行服务对象
 *
 * @export
 * @class Account_invoice_lineService
 * @extends {Account_invoice_lineServiceBase}
 */
export default class Account_invoice_lineService extends Account_invoice_lineServiceBase {

    /**
     * Creates an instance of  Account_invoice_lineService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_invoice_lineService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}