import { Http,Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 销售订单服务对象基类
 *
 * @export
 * @class Sale_orderServiceBase
 * @extends {EntityServie}
 */
export default class Sale_orderServiceBase extends EntityService {

    /**
     * Creates an instance of  Sale_orderServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  Sale_orderServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof Sale_orderServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='sale_order';
        this.APPDEKEY = 'id';
        this.APPDENAME = 'sale_orders';
        this.APPDETEXT = 'name';
        this.APPNAME = 'web';
        this.SYSTEMNAME = 'crm';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Sale_orderServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.sale_order){
            return Http.getInstance().post(`/res_partners/${context.res_partner}/sale_orders/${context.sale_order}/select`,data,isloading);
        }
            return Http.getInstance().post(`/sale_orders/${context.sale_order}/select`,data,isloading);
    }

    /**
     * UpdateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Sale_orderServiceBase
     */
    public async UpdateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.sale_order){
            return Http.getInstance().put(`/res_partners/${context.res_partner}/sale_orders/${context.sale_order}/updatebatch`,data,isloading);
        }
            return Http.getInstance().put(`/sale_orders/${context.sale_order}/updatebatch`,data,isloading);
    }

    /**
     * RemoveBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Sale_orderServiceBase
     */
    public async RemoveBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.sale_order){
            return Http.getInstance().delete(`/res_partners/${context.res_partner}/sale_orders/${context.sale_order}/removebatch`,isloading);
        }
            return Http.getInstance().delete(`/sale_orders/${context.sale_order}/removebatch`,isloading);

    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Sale_orderServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.sale_order){
            return Http.getInstance().put(`/res_partners/${context.res_partner}/sale_orders/${context.sale_order}`,data,isloading);
        }
        let masterData:any = {};
        let sale_order_linesData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_sale_order_lines'),'undefined')){
            sale_order_linesData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_sale_order_lines') as any);
            if(sale_order_linesData && sale_order_linesData.length && sale_order_linesData.length > 0){
                sale_order_linesData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.id = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.sale_order_lines = sale_order_linesData;
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/sale_orders/${context.sale_order}`,data,isloading);
            this.tempStorage.setItem(context.srfsessionkey+'_sale_order_lines',JSON.stringify(res.data.sale_order_lines));
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Sale_orderServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.sale_order){
            return Http.getInstance().get(`/res_partners/${context.res_partner}/sale_orders/${context.sale_order}`,isloading);
        }
            let res:any = await Http.getInstance().get(`/sale_orders/${context.sale_order}`,isloading);
            this.tempStorage.setItem(context.srfsessionkey+'_sale_order_lines',JSON.stringify(res.data.sale_order_lines));
            return res;

    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Sale_orderServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.sale_order){
            return Http.getInstance().post(`/res_partners/${context.res_partner}/sale_orders/${context.sale_order}/save`,data,isloading);
        }
        let masterData:any = {};
        let sale_order_linesData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_sale_order_lines'),'undefined')){
            sale_order_linesData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_sale_order_lines') as any);
            if(sale_order_linesData && sale_order_linesData.length && sale_order_linesData.length > 0){
                sale_order_linesData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.id = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.sale_order_lines = sale_order_linesData;
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/sale_orders/${context.sale_order}/save`,data,isloading);
            this.tempStorage.setItem(context.srfsessionkey+'_sale_order_lines',JSON.stringify(res.data.sale_order_lines));
            return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Sale_orderServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.sale_order){
            return Http.getInstance().post(`/res_partners/${context.res_partner}/sale_orders/${context.sale_order}/checkkey`,data,isloading);
        }
            return Http.getInstance().post(`/sale_orders/${context.sale_order}/checkkey`,data,isloading);
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Sale_orderServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.sale_order){
            return Http.getInstance().delete(`/res_partners/${context.res_partner}/sale_orders/${context.sale_order}`,isloading);
        }
            return Http.getInstance().delete(`/sale_orders/${context.sale_order}`,isloading);

    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Sale_orderServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && true){
            return Http.getInstance().get(`/res_partners/${context.res_partner}/sale_orders/getdraft`,isloading);
        }
        let res:any = await  Http.getInstance().get(`/sale_orders/getdraft`,isloading);
        res.data.sale_order = data.sale_order;
            this.tempStorage.setItem(context.srfsessionkey+'_sale_order_lines',JSON.stringify(res.data.sale_order_lines));
        return res;
    }

    /**
     * CreateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Sale_orderServiceBase
     */
    public async CreateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.sale_order){
            return Http.getInstance().post(`/res_partners/${context.res_partner}/sale_orders/${context.sale_order}/createbatch`,data,isloading);
        }
            return Http.getInstance().post(`/sale_orders/${context.sale_order}/createbatch`,data,isloading);
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Sale_orderServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && true){
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            return Http.getInstance().post(`/res_partners/${context.res_partner}/sale_orders`,data,isloading);
        }
        let masterData:any = {};
        let sale_order_linesData:any = [];
        if(!Object.is(this.tempStorage.getItem(context.srfsessionkey+'_sale_order_lines'),'undefined')){
            sale_order_linesData = JSON.parse(this.tempStorage.getItem(context.srfsessionkey+'_sale_order_lines') as any);
            if(sale_order_linesData && sale_order_linesData.length && sale_order_linesData.length > 0){
                sale_order_linesData.forEach((item:any) => {
                    if(item.srffrontuf){
                        if(Object.is(item.srffrontuf,"0")){
                            item.id = null;
                        }
                        delete item.srffrontuf;
                    }
                });
            }
        }
        masterData.sale_order_lines = sale_order_linesData;
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/sale_orders`,data,isloading);
        this.tempStorage.setItem(tempContext.srfsessionkey+'_sale_order_lines',JSON.stringify(res.data.sale_order_lines));
        return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Sale_orderServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return Http.getInstance().get(`/res_partners/${context.res_partner}/sale_orders/fetchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return Http.getInstance().get(`/sale_orders/fetchdefault`,tempData,isloading);
    }
}