import { Http,Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 客户服务对象基类
 *
 * @export
 * @class Res_partner_pServiceBase
 * @extends {EntityServie}
 */
export default class Res_partner_pServiceBase extends EntityService {

    /**
     * Creates an instance of  Res_partner_pServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_partner_pServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof Res_partner_pServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='res_partner_p';
        this.APPDEKEY = 'id';
        this.APPDENAME = 'res_partner_ps';
        this.APPDETEXT = 'name';
        this.APPNAME = 'web';
        this.SYSTEMNAME = 'crm';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partner_pServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.res_partner_p){
            return Http.getInstance().post(`/res_partners/${context.res_partner}/res_partner_ps/${context.res_partner_p}/select`,data,isloading);
        }
            return Http.getInstance().post(`/res_partner_ps/${context.res_partner_p}/select`,data,isloading);
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partner_pServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.res_partner_p){
            return Http.getInstance().delete(`/res_partners/${context.res_partner}/res_partner_ps/${context.res_partner_p}`,isloading);
        }
            return Http.getInstance().delete(`/res_partner_ps/${context.res_partner_p}`,isloading);

    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partner_pServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.res_partner_p){
            return Http.getInstance().get(`/res_partners/${context.res_partner}/res_partner_ps/${context.res_partner_p}`,isloading);
        }
            let res:any = await Http.getInstance().get(`/res_partner_ps/${context.res_partner_p}`,isloading);
            return res;

    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partner_pServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.res_partner_p){
            return Http.getInstance().post(`/res_partners/${context.res_partner}/res_partner_ps/${context.res_partner_p}/checkkey`,data,isloading);
        }
            return Http.getInstance().post(`/res_partner_ps/${context.res_partner_p}/checkkey`,data,isloading);
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partner_pServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.res_partner_p){
            return Http.getInstance().post(`/res_partners/${context.res_partner}/res_partner_ps/${context.res_partner_p}/save`,data,isloading);
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/res_partner_ps/${context.res_partner_p}/save`,data,isloading);
            return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partner_pServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.res_partner_p){
            return Http.getInstance().put(`/res_partners/${context.res_partner}/res_partner_ps/${context.res_partner_p}`,data,isloading);
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/res_partner_ps/${context.res_partner_p}`,data,isloading);
            return res;
    }

    /**
     * CreateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partner_pServiceBase
     */
    public async CreateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.res_partner_p){
            return Http.getInstance().post(`/res_partners/${context.res_partner}/res_partner_ps/${context.res_partner_p}/createbatch`,data,isloading);
        }
            return Http.getInstance().post(`/res_partner_ps/${context.res_partner_p}/createbatch`,data,isloading);
    }

    /**
     * UpdateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partner_pServiceBase
     */
    public async UpdateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.res_partner_p){
            return Http.getInstance().put(`/res_partners/${context.res_partner}/res_partner_ps/${context.res_partner_p}/updatebatch`,data,isloading);
        }
            return Http.getInstance().put(`/res_partner_ps/${context.res_partner_p}/updatebatch`,data,isloading);
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partner_pServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && true){
            return Http.getInstance().get(`/res_partners/${context.res_partner}/res_partner_ps/getdraft`,isloading);
        }
        let res:any = await  Http.getInstance().get(`/res_partner_ps/getdraft`,isloading);
        res.data.res_partner_p = data.res_partner_p;
        return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partner_pServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && true){
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            return Http.getInstance().post(`/res_partners/${context.res_partner}/res_partner_ps`,data,isloading);
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/res_partner_ps`,data,isloading);
        return res;
    }

    /**
     * RemoveBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partner_pServiceBase
     */
    public async RemoveBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.res_partner_p){
            return Http.getInstance().delete(`/res_partners/${context.res_partner}/res_partner_ps/${context.res_partner_p}/removebatch`,isloading);
        }
            return Http.getInstance().delete(`/res_partner_ps/${context.res_partner_p}/removebatch`,isloading);

    }

    /**
     * FetchContacts接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partner_pServiceBase
     */
    public async FetchContacts(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return Http.getInstance().get(`/res_partners/${context.res_partner}/res_partner_ps/fetchcontacts`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return Http.getInstance().get(`/res_partner_ps/fetchcontacts`,tempData,isloading);
    }

    /**
     * FetchCompany接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partner_pServiceBase
     */
    public async FetchCompany(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return Http.getInstance().get(`/res_partners/${context.res_partner}/res_partner_ps/fetchcompany`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return Http.getInstance().get(`/res_partner_ps/fetchcompany`,tempData,isloading);
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partner_pServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return Http.getInstance().get(`/res_partners/${context.res_partner}/res_partner_ps/fetchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return Http.getInstance().get(`/res_partner_ps/fetchdefault`,tempData,isloading);
    }
}