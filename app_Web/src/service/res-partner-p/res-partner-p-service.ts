import { Http,Util } from '@/utils';
import Res_partner_pServiceBase from './res-partner-p-service-base';


/**
 * 客户服务对象
 *
 * @export
 * @class Res_partner_pService
 * @extends {Res_partner_pServiceBase}
 */
export default class Res_partner_pService extends Res_partner_pServiceBase {

    /**
     * Creates an instance of  Res_partner_pService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_partner_pService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}