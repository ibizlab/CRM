import { Http,Util } from '@/utils';
import Calendar_eventServiceBase from './calendar-event-service-base';


/**
 * 活动服务对象
 *
 * @export
 * @class Calendar_eventService
 * @extends {Calendar_eventServiceBase}
 */
export default class Calendar_eventService extends Calendar_eventServiceBase {

    /**
     * Creates an instance of  Calendar_eventService.
     * 
     * @param {*} [opts={}]
     * @memberof  Calendar_eventService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}