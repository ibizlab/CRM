import { Http,Util } from '@/utils';
import Account_paymentServiceBase from './account-payment-service-base';


/**
 * 付款服务对象
 *
 * @export
 * @class Account_paymentService
 * @extends {Account_paymentServiceBase}
 */
export default class Account_paymentService extends Account_paymentServiceBase {

    /**
     * Creates an instance of  Account_paymentService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_paymentService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}