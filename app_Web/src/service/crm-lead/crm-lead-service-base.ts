import { Http,Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 线索/商机服务对象基类
 *
 * @export
 * @class Crm_leadServiceBase
 * @extends {EntityServie}
 */
export default class Crm_leadServiceBase extends EntityService {

    /**
     * Creates an instance of  Crm_leadServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  Crm_leadServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof Crm_leadServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='crm_lead';
        this.APPDEKEY = 'id';
        this.APPDENAME = 'crm_leads';
        this.APPDETEXT = 'name';
        this.APPNAME = 'web';
        this.SYSTEMNAME = 'crm';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Crm_leadServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.crm_lead){
            return Http.getInstance().post(`/res_partners/${context.res_partner}/crm_leads/${context.crm_lead}/select`,data,isloading);
        }
            return Http.getInstance().post(`/crm_leads/${context.crm_lead}/select`,data,isloading);
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Crm_leadServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && true){
            return Http.getInstance().get(`/res_partners/${context.res_partner}/crm_leads/getdraft`,isloading);
        }
        let res:any = await  Http.getInstance().get(`/crm_leads/getdraft`,isloading);
        res.data.crm_lead = data.crm_lead;
        return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Crm_leadServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.crm_lead){
            return Http.getInstance().delete(`/res_partners/${context.res_partner}/crm_leads/${context.crm_lead}`,isloading);
        }
            return Http.getInstance().delete(`/crm_leads/${context.crm_lead}`,isloading);

    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Crm_leadServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.crm_lead){
            return Http.getInstance().post(`/res_partners/${context.res_partner}/crm_leads/${context.crm_lead}/save`,data,isloading);
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/crm_leads/${context.crm_lead}/save`,data,isloading);
            return res;
    }

    /**
     * UpdateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Crm_leadServiceBase
     */
    public async UpdateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.crm_lead){
            return Http.getInstance().put(`/res_partners/${context.res_partner}/crm_leads/${context.crm_lead}/updatebatch`,data,isloading);
        }
            return Http.getInstance().put(`/crm_leads/${context.crm_lead}/updatebatch`,data,isloading);
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Crm_leadServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.crm_lead){
            return Http.getInstance().get(`/res_partners/${context.res_partner}/crm_leads/${context.crm_lead}`,isloading);
        }
            let res:any = await Http.getInstance().get(`/crm_leads/${context.crm_lead}`,isloading);
            return res;

    }

    /**
     * CreateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Crm_leadServiceBase
     */
    public async CreateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.crm_lead){
            return Http.getInstance().post(`/res_partners/${context.res_partner}/crm_leads/${context.crm_lead}/createbatch`,data,isloading);
        }
            return Http.getInstance().post(`/crm_leads/${context.crm_lead}/createbatch`,data,isloading);
    }

    /**
     * RemoveBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Crm_leadServiceBase
     */
    public async RemoveBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.crm_lead){
            return Http.getInstance().delete(`/res_partners/${context.res_partner}/crm_leads/${context.crm_lead}/removebatch`,isloading);
        }
            return Http.getInstance().delete(`/crm_leads/${context.crm_lead}/removebatch`,isloading);

    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Crm_leadServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && true){
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            return Http.getInstance().post(`/res_partners/${context.res_partner}/crm_leads`,data,isloading);
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/crm_leads`,data,isloading);
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Crm_leadServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.crm_lead){
            return Http.getInstance().post(`/res_partners/${context.res_partner}/crm_leads/${context.crm_lead}/checkkey`,data,isloading);
        }
            return Http.getInstance().post(`/crm_leads/${context.crm_lead}/checkkey`,data,isloading);
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Crm_leadServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && context.crm_lead){
            return Http.getInstance().put(`/res_partners/${context.res_partner}/crm_leads/${context.crm_lead}`,data,isloading);
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/crm_leads/${context.crm_lead}`,data,isloading);
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Crm_leadServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return Http.getInstance().get(`/res_partners/${context.res_partner}/crm_leads/fetchdefault`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return Http.getInstance().get(`/crm_leads/fetchdefault`,tempData,isloading);
    }

    /**
     * FetchDefault2接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Crm_leadServiceBase
     */
    public async FetchDefault2(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_partner && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            return Http.getInstance().get(`/res_partners/${context.res_partner}/crm_leads/fetchdefault2`,tempData,isloading);
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        return Http.getInstance().get(`/crm_leads/fetchdefault2`,tempData,isloading);
    }
}