import PortalCounterServiceBase from './portal-counter-base';

/**
 * 门户界面计数器计数器服务对象
 *
 * @export
 * @class PortalCounterService
 */
export default class PortalCounterService extends PortalCounterServiceBase {

    /**
     * Creates an instance of  PortalCounterService.
     * 
     * @param {*} [opts={}]
     * @memberof  PortalCounterService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}