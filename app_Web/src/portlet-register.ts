import Res_usersTeamUserList from '@/widgets/res-users/team-user-list-portlet/team-user-list-portlet.vue';
import DynaPartnerExpView from '@/widgets/app/dyna-partner-exp-view-portlet/dyna-partner-exp-view-portlet.vue';
import Res_partnerPie from '@/widgets/res-partner/pie-portlet/pie-portlet.vue';
import Res_partnerTrend from '@/widgets/res-partner/trend-portlet/trend-portlet.vue';
import Mail_activityActivityList from '@/widgets/mail-activity/activity-list-portlet/activity-list-portlet.vue';
import ActivityCalendar from '@/widgets/app/activity-calendar-portlet/activity-calendar-portlet.vue';
import Crm_leadLeadChart from '@/widgets/crm-lead/lead-chart-portlet/lead-chart-portlet.vue';
import Crm_teamMainView9_Team from '@/widgets/crm-team/main-view9-team-portlet/main-view9-team-portlet.vue';
import Crm_teamTabExpView_Team from '@/widgets/crm-team/tab-exp-view-team-portlet/tab-exp-view-team-portlet.vue';
import Mail_messageEvent from '@/widgets/mail-message/event-portlet/event-portlet.vue';
import Res_usersBriefReport from '@/widgets/res-users/brief-report-portlet/brief-report-portlet.vue';
import Sale_orderOrderBar from '@/widgets/sale-order/order-bar-portlet/order-bar-portlet.vue';
import Res_partnerMainView9 from '@/widgets/res-partner/main-view9-portlet/main-view9-portlet.vue';
import Res_partnerTabExpView_Partner from '@/widgets/res-partner/tab-exp-view-partner-portlet/tab-exp-view-partner-portlet.vue';
import Res_partnerToDoOrders from '@/widgets/res-partner/to-do-orders-portlet/to-do-orders-portlet.vue';
import Res_partnerCurWeekLead from '@/widgets/res-partner/cur-week-lead-portlet/cur-week-lead-portlet.vue';

export const  PortletComponent = {
    install(v: any, opt: any) {
        v.component('app-res-users-team-user-list-portlet', Res_usersTeamUserList);
        v.component('app-dyna-partner-exp-view-portlet', DynaPartnerExpView);
        v.component('app-res-partner-pie-portlet', Res_partnerPie);
        v.component('app-res-partner-trend-portlet', Res_partnerTrend);
        v.component('app-res-partner-to-do-orders-portlet', Res_partnerToDoOrders);
        v.component('app-mail-activity-activity-list-portlet', Mail_activityActivityList);
        v.component('app-activity-calendar-portlet', ActivityCalendar);
        v.component('app-res-users-brief-report-portlet', Res_usersBriefReport);
        v.component('app-crm-lead-lead-chart-portlet', Crm_leadLeadChart);
        v.component('app-sale-order-order-bar-portlet', Sale_orderOrderBar);
        v.component('app-mail-message-event-portlet', Mail_messageEvent);
        v.component('crm-lead-lead-chart-portlet', Crm_leadLeadChart);
        v.component('crm-team-main-view9-team-portlet', Crm_teamMainView9_Team);
        v.component('crm-team-tab-exp-view-team-portlet', Crm_teamTabExpView_Team);
        v.component('mail-message-event-portlet', Mail_messageEvent);
        v.component('res-users-brief-report-portlet', Res_usersBriefReport);
        v.component('sale-order-order-bar-portlet', Sale_orderOrderBar);
        v.component('res-partner-main-view9-portlet', Res_partnerMainView9);
        v.component('res-partner-tab-exp-view-partner-portlet', Res_partnerTabExpView_Partner);
        v.component('res-partner-to-do-orders-portlet', Res_partnerToDoOrders);
        v.component('res-partner-cur-week-lead-portlet', Res_partnerCurWeekLead);
    }
};