import Crm_teamUIServiceBase from './crm-team-ui-service-base';

/**
 * 销售渠道UI服务对象
 *
 * @export
 * @class Crm_teamUIService
 */
export default class Crm_teamUIService extends Crm_teamUIServiceBase {

    /**
     * Creates an instance of  Crm_teamUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Crm_teamUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}