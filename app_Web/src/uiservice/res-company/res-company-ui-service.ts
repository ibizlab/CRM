import Res_companyUIServiceBase from './res-company-ui-service-base';

/**
 * 公司UI服务对象
 *
 * @export
 * @class Res_companyUIService
 */
export default class Res_companyUIService extends Res_companyUIServiceBase {

    /**
     * Creates an instance of  Res_companyUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_companyUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}