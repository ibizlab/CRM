/**
 * UI服务注册中心
 *
 * @export
 * @class UIServiceRegister
 */
export class UIServiceRegister {

    /**
     * 所有UI实体服务Map
     *
     * @protected
     * @type {*}
     * @memberof UIServiceRegister
     */
    protected allUIService: Map<string, () => Promise<any>> = new Map();

    /**
     * 已加载UI实体服务Map缓存
     *
     * @protected
     * @type {Map<string, any>}
     * @memberof UIServiceRegister
     */
    protected serviceCache: Map<string, any> = new Map();

    /**
     * Creates an instance of UIServiceRegister.
     * @memberof UIServiceRegister
     */
    constructor() {
        this.init();
    }

    /**
     * 初始化
     *
     * @protected
     * @memberof UIServiceRegister
     */
    protected init(): void {
                this.allUIService.set('product_price_list', () => import('@/uiservice/product-price-list/product-price-list-ui-service'));
        this.allUIService.set('account_journal', () => import('@/uiservice/account-journal/account-journal-ui-service'));
        this.allUIService.set('product_pricelist', () => import('@/uiservice/product-pricelist/product-pricelist-ui-service'));
        this.allUIService.set('crm_lead', () => import('@/uiservice/crm-lead/crm-lead-ui-service'));
        this.allUIService.set('mail_activity', () => import('@/uiservice/mail-activity/mail-activity-ui-service'));
        this.allUIService.set('res_company', () => import('@/uiservice/res-company/res-company-ui-service'));
        this.allUIService.set('sale_order_line', () => import('@/uiservice/sale-order-line/sale-order-line-ui-service'));
        this.allUIService.set('account_payment', () => import('@/uiservice/account-payment/account-payment-ui-service'));
        this.allUIService.set('product_product', () => import('@/uiservice/product-product/product-product-ui-service'));
        this.allUIService.set('calendar_event', () => import('@/uiservice/calendar-event/calendar-event-ui-service'));
        this.allUIService.set('crm_team', () => import('@/uiservice/crm-team/crm-team-ui-service'));
        this.allUIService.set('product_category', () => import('@/uiservice/product-category/product-category-ui-service'));
        this.allUIService.set('res_partner_p', () => import('@/uiservice/res-partner-p/res-partner-p-ui-service'));
        this.allUIService.set('product_pricelist_item', () => import('@/uiservice/product-pricelist-item/product-pricelist-item-ui-service'));
        this.allUIService.set('account_invoice', () => import('@/uiservice/account-invoice/account-invoice-ui-service'));
        this.allUIService.set('mail_message', () => import('@/uiservice/mail-message/mail-message-ui-service'));
        this.allUIService.set('res_users', () => import('@/uiservice/res-users/res-users-ui-service'));
        this.allUIService.set('sale_order', () => import('@/uiservice/sale-order/sale-order-ui-service'));
        this.allUIService.set('mail_activity_type', () => import('@/uiservice/mail-activity-type/mail-activity-type-ui-service'));
        this.allUIService.set('res_partner', () => import('@/uiservice/res-partner/res-partner-ui-service'));
        this.allUIService.set('account_register_payments', () => import('@/uiservice/account-register-payments/account-register-payments-ui-service'));
        this.allUIService.set('dynachart', () => import('@/uiservice/dyna-chart/dyna-chart-ui-service'));
        this.allUIService.set('dynadashboard', () => import('@/uiservice/dyna-dashboard/dyna-dashboard-ui-service'));
        this.allUIService.set('account_invoice_line', () => import('@/uiservice/account-invoice-line/account-invoice-line-ui-service'));
    }

    /**
     * 加载服务实体
     *
     * @protected
     * @param {string} serviceName
     * @returns {Promise<any>}
     * @memberof UIServiceRegister
     */
    protected async loadService(serviceName: string): Promise<any> {
        const service = this.allUIService.get(serviceName);
        if (service) {
            return service();
        }
    }

    /**
     * 获取应用实体服务
     *
     * @param {string} name
     * @returns {Promise<any>}
     * @memberof UIServiceRegister
     */
    public async getService(name: string): Promise<any> {
        if (this.serviceCache.has(name)) {
            return this.serviceCache.get(name);
        }
        const entityService: any = await this.loadService(name);
        if (entityService && entityService.default) {
            const instance: any = new entityService.default();
            this.serviceCache.set(name, instance);
            return instance;
        }
    }

}
export const uiServiceRegister: UIServiceRegister = new UIServiceRegister();