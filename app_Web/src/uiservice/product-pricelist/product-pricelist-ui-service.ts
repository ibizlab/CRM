import Product_pricelistUIServiceBase from './product-pricelist-ui-service-base';

/**
 * 价格表UI服务对象
 *
 * @export
 * @class Product_pricelistUIService
 */
export default class Product_pricelistUIService extends Product_pricelistUIServiceBase {

    /**
     * Creates an instance of  Product_pricelistUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Product_pricelistUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}