import Account_invoice_lineUIServiceBase from './account-invoice-line-ui-service-base';

/**
 * 发票行UI服务对象
 *
 * @export
 * @class Account_invoice_lineUIService
 */
export default class Account_invoice_lineUIService extends Account_invoice_lineUIServiceBase {

    /**
     * Creates an instance of  Account_invoice_lineUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_invoice_lineUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}