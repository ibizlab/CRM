import Sale_order_lineUIServiceBase from './sale-order-line-ui-service-base';

/**
 * 销售订单行UI服务对象
 *
 * @export
 * @class Sale_order_lineUIService
 */
export default class Sale_order_lineUIService extends Sale_order_lineUIServiceBase {

    /**
     * Creates an instance of  Sale_order_lineUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Sale_order_lineUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}