import Product_pricelist_itemUIServiceBase from './product-pricelist-item-ui-service-base';

/**
 * 价格表明细UI服务对象
 *
 * @export
 * @class Product_pricelist_itemUIService
 */
export default class Product_pricelist_itemUIService extends Product_pricelist_itemUIServiceBase {

    /**
     * Creates an instance of  Product_pricelist_itemUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Product_pricelist_itemUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}