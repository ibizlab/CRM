import { MockAdapter } from '../mock-adapter';
const mock = MockAdapter.getInstance();

import Mock from 'mockjs'

// 获取全部数组
mock.onGet('./assets/json/data-dictionary.json').reply((config: any) => {
    let status = MockAdapter.mockStatus(config);
    return [status, [
        {
        srfkey: 'SALE_ORDER__STATE',
        emptytext: '未定义',
        "codelisttype":"static",
        items: [
            {
                id: 'draft',
                label: '报价单',
                text: '报价单',
                "data":"",
                "codename":"Draft",
                value: 'draft',
                
                disabled: false,
            },
            {
                id: 'sent',
                label: '报价已发送',
                text: '报价已发送',
                "data":"",
                "codename":"Sent",
                value: 'sent',
                
                disabled: false,
            },
            {
                id: 'sale',
                label: '销售订单',
                text: '销售订单',
                "data":"",
                "codename":"Sale",
                value: 'sale',
                
                disabled: false,
            },
            {
                id: 'done',
                label: '已锁定',
                text: '已锁定',
                "data":"",
                "codename":"Done",
                value: 'done',
                
                disabled: false,
            },
            {
                id: 'cancel',
                label: '已取消',
                text: '已取消',
                "data":"",
                "codename":"Cancel",
                value: 'cancel',
                
                disabled: false,
            },
        ]
    },
    {
        "srfkey": "ACCOUNT_INVOICE__STATE",
        "emptytext": "未定义",
        "codelisttype":"dynamic",
        "appdataentity":"",
        "appdedataset":"",
        "items": []
    },
    {
        srfkey: 'TrueFalse2',
        emptytext: '未定义',
        "codelisttype":"static",
        items: [
            {
                id: 'true',
                label: '是',
                text: '是',
                "data":"",
                "codename":"True",
                value: 'true',
                
                disabled: false,
            },
            {
                id: 'false',
                label: '否',
                text: '否',
                "data":"",
                "codename":"False",
                value: 'false',
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: 'CRM_LEAD__ACTIVITY_STATE',
        emptytext: '未定义',
        "codelisttype":"static",
        items: [
            {
                id: 'overdue',
                label: '逾期',
                text: '逾期',
                "data":"",
                "codename":"Overdue",
                value: 'overdue',
                
                disabled: false,
            },
            {
                id: 'today',
                label: '今日',
                text: '今日',
                "data":"",
                "codename":"Today",
                value: 'today',
                
                disabled: false,
            },
            {
                id: 'planned',
                label: '已计划',
                text: '已计划',
                "data":"",
                "codename":"Planned",
                value: 'planned',
                
                disabled: false,
            },
        ]
    }
    ]];
});

