package cn.ibizlab.odoo.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_recruitment_stage;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_recruitment_stageSearchContext;


/**
 * 实体[Hr_recruitment_stage] 服务对象接口
 */
public interface IHr_recruitment_stageService{

    Hr_recruitment_stage get(Integer key) ;
    boolean create(Hr_recruitment_stage et) ;
    void createBatch(List<Hr_recruitment_stage> list) ;
    boolean update(Hr_recruitment_stage et) ;
    void updateBatch(List<Hr_recruitment_stage> list) ;
    Hr_recruitment_stage getDraft(Hr_recruitment_stage et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Hr_recruitment_stage> searchDefault(Hr_recruitment_stageSearchContext context) ;

}



