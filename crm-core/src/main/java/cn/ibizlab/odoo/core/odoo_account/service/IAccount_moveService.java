package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_move;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_moveSearchContext;


/**
 * 实体[Account_move] 服务对象接口
 */
public interface IAccount_moveService{

    boolean update(Account_move et) ;
    void updateBatch(List<Account_move> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Account_move get(Integer key) ;
    boolean create(Account_move et) ;
    void createBatch(List<Account_move> list) ;
    Account_move getDraft(Account_move et) ;
    Page<Account_move> searchDefault(Account_moveSearchContext context) ;

}



