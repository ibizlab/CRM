package cn.ibizlab.odoo.core.odoo_fetchmail.valuerule.anno.fetchmail_server;

import cn.ibizlab.odoo.core.odoo_fetchmail.valuerule.validator.fetchmail_server.Fetchmail_serverMessage_idsDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Fetchmail_server
 * 属性：Message_ids
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Fetchmail_serverMessage_idsDefaultValidator.class})
public @interface Fetchmail_serverMessage_idsDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[1048576]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
