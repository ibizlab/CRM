package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_tax;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_taxSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_invoice_tax] 服务对象接口
 */
@Component
public class account_invoice_taxFallback implements account_invoice_taxFeignClient{




    public Account_invoice_tax update(Integer id, Account_invoice_tax account_invoice_tax){
            return null;
     }
    public Boolean updateBatch(List<Account_invoice_tax> account_invoice_taxes){
            return false;
     }


    public Account_invoice_tax get(Integer id){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Account_invoice_tax create(Account_invoice_tax account_invoice_tax){
            return null;
     }
    public Boolean createBatch(List<Account_invoice_tax> account_invoice_taxes){
            return false;
     }

    public Page<Account_invoice_tax> searchDefault(Account_invoice_taxSearchContext context){
            return null;
     }


    public Page<Account_invoice_tax> select(){
            return null;
     }

    public Account_invoice_tax getDraft(){
            return null;
    }



}
