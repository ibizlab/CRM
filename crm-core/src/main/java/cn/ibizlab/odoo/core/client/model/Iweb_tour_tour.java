package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [web_tour_tour] 对象
 */
public interface Iweb_tour_tour {

    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [向导名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [向导名称]
     */
    public String getName();

    /**
     * 获取 [向导名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [消耗于]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [消耗于]
     */
    public Integer getUser_id();

    /**
     * 获取 [消耗于]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [消耗于]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [消耗于]
     */
    public String getUser_id_text();

    /**
     * 获取 [消耗于]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [修改日期]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [修改日期]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [修改日期]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
