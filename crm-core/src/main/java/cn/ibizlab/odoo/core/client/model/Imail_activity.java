package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [mail_activity] 对象
 */
public interface Imail_activity {

    /**
     * 获取 [类别]
     */
    public void setActivity_category(String activity_category);
    
    /**
     * 设置 [类别]
     */
    public String getActivity_category();

    /**
     * 获取 [类别]脏标记
     */
    public boolean getActivity_categoryDirtyFlag();
    /**
     * 获取 [排版类型]
     */
    public void setActivity_decoration(String activity_decoration);
    
    /**
     * 设置 [排版类型]
     */
    public String getActivity_decoration();

    /**
     * 获取 [排版类型]脏标记
     */
    public boolean getActivity_decorationDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setActivity_type_id(Integer activity_type_id);
    
    /**
     * 设置 [活动]
     */
    public Integer getActivity_type_id();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getActivity_type_idDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setActivity_type_id_text(String activity_type_id_text);
    
    /**
     * 设置 [活动]
     */
    public String getActivity_type_id_text();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getActivity_type_id_textDirtyFlag();
    /**
     * 获取 [自动活动]
     */
    public void setAutomated(String automated);
    
    /**
     * 设置 [自动活动]
     */
    public String getAutomated();

    /**
     * 获取 [自动活动]脏标记
     */
    public boolean getAutomatedDirtyFlag();
    /**
     * 获取 [日历会议]
     */
    public void setCalendar_event_id(Integer calendar_event_id);
    
    /**
     * 设置 [日历会议]
     */
    public Integer getCalendar_event_id();

    /**
     * 获取 [日历会议]脏标记
     */
    public boolean getCalendar_event_idDirtyFlag();
    /**
     * 获取 [日历会议]
     */
    public void setCalendar_event_id_text(String calendar_event_id_text);
    
    /**
     * 设置 [日历会议]
     */
    public String getCalendar_event_id_text();

    /**
     * 获取 [日历会议]脏标记
     */
    public boolean getCalendar_event_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [建立者]
     */
    public void setCreate_user_id(Integer create_user_id);
    
    /**
     * 设置 [建立者]
     */
    public Integer getCreate_user_id();

    /**
     * 获取 [建立者]脏标记
     */
    public boolean getCreate_user_idDirtyFlag();
    /**
     * 获取 [建立者]
     */
    public void setCreate_user_id_text(String create_user_id_text);
    
    /**
     * 设置 [建立者]
     */
    public String getCreate_user_id_text();

    /**
     * 获取 [建立者]脏标记
     */
    public boolean getCreate_user_id_textDirtyFlag();
    /**
     * 获取 [到期时间]
     */
    public void setDate_deadline(Timestamp date_deadline);
    
    /**
     * 设置 [到期时间]
     */
    public Timestamp getDate_deadline();

    /**
     * 获取 [到期时间]脏标记
     */
    public boolean getDate_deadlineDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [反馈]
     */
    public void setFeedback(String feedback);
    
    /**
     * 设置 [反馈]
     */
    public String getFeedback();

    /**
     * 获取 [反馈]脏标记
     */
    public boolean getFeedbackDirtyFlag();
    /**
     * 获取 [自动安排下一个活动]
     */
    public void setForce_next(String force_next);
    
    /**
     * 设置 [自动安排下一个活动]
     */
    public String getForce_next();

    /**
     * 获取 [自动安排下一个活动]脏标记
     */
    public boolean getForce_nextDirtyFlag();
    /**
     * 获取 [下一活动可用]
     */
    public void setHas_recommended_activities(String has_recommended_activities);
    
    /**
     * 设置 [下一活动可用]
     */
    public String getHas_recommended_activities();

    /**
     * 获取 [下一活动可用]脏标记
     */
    public boolean getHas_recommended_activitiesDirtyFlag();
    /**
     * 获取 [图标]
     */
    public void setIcon(String icon);
    
    /**
     * 设置 [图标]
     */
    public String getIcon();

    /**
     * 获取 [图标]脏标记
     */
    public boolean getIconDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [邮件模板]
     */
    public void setMail_template_ids(String mail_template_ids);
    
    /**
     * 设置 [邮件模板]
     */
    public String getMail_template_ids();

    /**
     * 获取 [邮件模板]脏标记
     */
    public boolean getMail_template_idsDirtyFlag();
    /**
     * 获取 [备注]
     */
    public void setNote(String note);
    
    /**
     * 设置 [备注]
     */
    public String getNote();

    /**
     * 获取 [备注]脏标记
     */
    public boolean getNoteDirtyFlag();
    /**
     * 获取 [相关便签]
     */
    public void setNote_id(Integer note_id);
    
    /**
     * 设置 [相关便签]
     */
    public Integer getNote_id();

    /**
     * 获取 [相关便签]脏标记
     */
    public boolean getNote_idDirtyFlag();
    /**
     * 获取 [相关便签]
     */
    public void setNote_id_text(String note_id_text);
    
    /**
     * 设置 [相关便签]
     */
    public String getNote_id_text();

    /**
     * 获取 [相关便签]脏标记
     */
    public boolean getNote_id_textDirtyFlag();
    /**
     * 获取 [前一活动类型]
     */
    public void setPrevious_activity_type_id(Integer previous_activity_type_id);
    
    /**
     * 设置 [前一活动类型]
     */
    public Integer getPrevious_activity_type_id();

    /**
     * 获取 [前一活动类型]脏标记
     */
    public boolean getPrevious_activity_type_idDirtyFlag();
    /**
     * 获取 [前一活动类型]
     */
    public void setPrevious_activity_type_id_text(String previous_activity_type_id_text);
    
    /**
     * 设置 [前一活动类型]
     */
    public String getPrevious_activity_type_id_text();

    /**
     * 获取 [前一活动类型]脏标记
     */
    public boolean getPrevious_activity_type_id_textDirtyFlag();
    /**
     * 获取 [推荐的活动类型]
     */
    public void setRecommended_activity_type_id(Integer recommended_activity_type_id);
    
    /**
     * 设置 [推荐的活动类型]
     */
    public Integer getRecommended_activity_type_id();

    /**
     * 获取 [推荐的活动类型]脏标记
     */
    public boolean getRecommended_activity_type_idDirtyFlag();
    /**
     * 获取 [推荐的活动类型]
     */
    public void setRecommended_activity_type_id_text(String recommended_activity_type_id_text);
    
    /**
     * 设置 [推荐的活动类型]
     */
    public String getRecommended_activity_type_id_text();

    /**
     * 获取 [推荐的活动类型]脏标记
     */
    public boolean getRecommended_activity_type_id_textDirtyFlag();
    /**
     * 获取 [相关文档编号]
     */
    public void setRes_id(Integer res_id);
    
    /**
     * 设置 [相关文档编号]
     */
    public Integer getRes_id();

    /**
     * 获取 [相关文档编号]脏标记
     */
    public boolean getRes_idDirtyFlag();
    /**
     * 获取 [相关的文档模型]
     */
    public void setRes_model(String res_model);
    
    /**
     * 设置 [相关的文档模型]
     */
    public String getRes_model();

    /**
     * 获取 [相关的文档模型]脏标记
     */
    public boolean getRes_modelDirtyFlag();
    /**
     * 获取 [文档模型]
     */
    public void setRes_model_id(Integer res_model_id);
    
    /**
     * 设置 [文档模型]
     */
    public Integer getRes_model_id();

    /**
     * 获取 [文档模型]脏标记
     */
    public boolean getRes_model_idDirtyFlag();
    /**
     * 获取 [文档名称]
     */
    public void setRes_name(String res_name);
    
    /**
     * 设置 [文档名称]
     */
    public String getRes_name();

    /**
     * 获取 [文档名称]脏标记
     */
    public boolean getRes_nameDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [摘要]
     */
    public void setSummary(String summary);
    
    /**
     * 设置 [摘要]
     */
    public String getSummary();

    /**
     * 获取 [摘要]脏标记
     */
    public boolean getSummaryDirtyFlag();
    /**
     * 获取 [分派给]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [分派给]
     */
    public Integer getUser_id();

    /**
     * 获取 [分派给]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [分派给]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [分派给]
     */
    public String getUser_id_text();

    /**
     * 获取 [分派给]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
