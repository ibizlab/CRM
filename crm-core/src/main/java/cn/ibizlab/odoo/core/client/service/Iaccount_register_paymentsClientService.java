package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_register_payments;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_register_payments] 服务对象接口
 */
public interface Iaccount_register_paymentsClientService{

    public Iaccount_register_payments createModel() ;

    public void create(Iaccount_register_payments account_register_payments);

    public void createBatch(List<Iaccount_register_payments> account_register_payments);

    public void removeBatch(List<Iaccount_register_payments> account_register_payments);

    public Page<Iaccount_register_payments> fetchDefault(SearchContext context);

    public void updateBatch(List<Iaccount_register_payments> account_register_payments);

    public void remove(Iaccount_register_payments account_register_payments);

    public void update(Iaccount_register_payments account_register_payments);

    public void get(Iaccount_register_payments account_register_payments);

    public Page<Iaccount_register_payments> select(SearchContext context);

    public void checkKey(Iaccount_register_payments account_register_payments);

    public void save(Iaccount_register_payments account_register_payments);

    public void getDraft(Iaccount_register_payments account_register_payments);

}
