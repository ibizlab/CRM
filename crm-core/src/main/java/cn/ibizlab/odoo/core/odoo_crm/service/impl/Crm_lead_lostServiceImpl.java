package cn.ibizlab.odoo.core.odoo_crm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead_lost;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lead_lostSearchContext;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_lead_lostService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_crm.client.crm_lead_lostFeignClient;

/**
 * 实体[获取丢失原因] 服务对象接口实现
 */
@Slf4j
@Service
public class Crm_lead_lostServiceImpl implements ICrm_lead_lostService {

    @Autowired
    crm_lead_lostFeignClient crm_lead_lostFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=crm_lead_lostFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        crm_lead_lostFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Crm_lead_lost et) {
        Crm_lead_lost rt = crm_lead_lostFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Crm_lead_lost> list){
        crm_lead_lostFeignClient.updateBatch(list) ;
    }

    @Override
    public Crm_lead_lost getDraft(Crm_lead_lost et) {
        et=crm_lead_lostFeignClient.getDraft();
        return et;
    }

    @Override
    public Crm_lead_lost get(Integer id) {
		Crm_lead_lost et=crm_lead_lostFeignClient.get(id);
        if(et==null){
            et=new Crm_lead_lost();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Crm_lead_lost et) {
        Crm_lead_lost rt = crm_lead_lostFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Crm_lead_lost> list){
        crm_lead_lostFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Crm_lead_lost> searchDefault(Crm_lead_lostSearchContext context) {
        Page<Crm_lead_lost> crm_lead_losts=crm_lead_lostFeignClient.searchDefault(context);
        return crm_lead_losts;
    }


}


