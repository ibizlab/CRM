package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ires_country_group;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_country_group] 服务对象接口
 */
public interface Ires_country_groupClientService{

    public Ires_country_group createModel() ;

    public Page<Ires_country_group> fetchDefault(SearchContext context);

    public void get(Ires_country_group res_country_group);

    public void update(Ires_country_group res_country_group);

    public void removeBatch(List<Ires_country_group> res_country_groups);

    public void createBatch(List<Ires_country_group> res_country_groups);

    public void remove(Ires_country_group res_country_group);

    public void updateBatch(List<Ires_country_group> res_country_groups);

    public void create(Ires_country_group res_country_group);

    public Page<Ires_country_group> select(SearchContext context);

    public void getDraft(Ires_country_group res_country_group);

}
