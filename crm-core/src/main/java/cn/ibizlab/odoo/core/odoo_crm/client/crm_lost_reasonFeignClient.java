package cn.ibizlab.odoo.core.odoo_crm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lost_reason;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lost_reasonSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[crm_lost_reason] 服务对象接口
 */
@FeignClient(value = "odoo-crm", contextId = "crm-lost-reason", fallback = crm_lost_reasonFallback.class)
public interface crm_lost_reasonFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/crm_lost_reasons/{id}")
    Crm_lost_reason update(@PathVariable("id") Integer id,@RequestBody Crm_lost_reason crm_lost_reason);

    @RequestMapping(method = RequestMethod.PUT, value = "/crm_lost_reasons/batch")
    Boolean updateBatch(@RequestBody List<Crm_lost_reason> crm_lost_reasons);




    @RequestMapping(method = RequestMethod.DELETE, value = "/crm_lost_reasons/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/crm_lost_reasons/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/crm_lost_reasons")
    Crm_lost_reason create(@RequestBody Crm_lost_reason crm_lost_reason);

    @RequestMapping(method = RequestMethod.POST, value = "/crm_lost_reasons/batch")
    Boolean createBatch(@RequestBody List<Crm_lost_reason> crm_lost_reasons);



    @RequestMapping(method = RequestMethod.POST, value = "/crm_lost_reasons/searchdefault")
    Page<Crm_lost_reason> searchDefault(@RequestBody Crm_lost_reasonSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/crm_lost_reasons/{id}")
    Crm_lost_reason get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.GET, value = "/crm_lost_reasons/select")
    Page<Crm_lost_reason> select();


    @RequestMapping(method = RequestMethod.GET, value = "/crm_lost_reasons/getdraft")
    Crm_lost_reason getDraft();


}
