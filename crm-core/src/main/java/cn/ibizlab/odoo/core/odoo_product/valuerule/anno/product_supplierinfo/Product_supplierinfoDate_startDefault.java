package cn.ibizlab.odoo.core.odoo_product.valuerule.anno.product_supplierinfo;

import cn.ibizlab.odoo.core.odoo_product.valuerule.validator.product_supplierinfo.Product_supplierinfoDate_startDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Product_supplierinfo
 * 属性：Date_start
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Product_supplierinfoDate_startDefaultValidator.class})
public @interface Product_supplierinfoDate_startDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
