package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [survey_label] 对象
 */
public interface Isurvey_label {

    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [疑问]
     */
    public void setQuestion_id(Integer question_id);
    
    /**
     * 设置 [疑问]
     */
    public Integer getQuestion_id();

    /**
     * 获取 [疑问]脏标记
     */
    public boolean getQuestion_idDirtyFlag();
    /**
     * 获取 [第二个问题]
     */
    public void setQuestion_id_2(Integer question_id_2);
    
    /**
     * 设置 [第二个问题]
     */
    public Integer getQuestion_id_2();

    /**
     * 获取 [第二个问题]脏标记
     */
    public boolean getQuestion_id_2DirtyFlag();
    /**
     * 获取 [这个选项的分数]
     */
    public void setQuizz_mark(Double quizz_mark);
    
    /**
     * 设置 [这个选项的分数]
     */
    public Double getQuizz_mark();

    /**
     * 获取 [这个选项的分数]脏标记
     */
    public boolean getQuizz_markDirtyFlag();
    /**
     * 获取 [标签序列顺序]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [标签序列顺序]
     */
    public Integer getSequence();

    /**
     * 获取 [标签序列顺序]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [建议值]
     */
    public void setValue(String value);
    
    /**
     * 设置 [建议值]
     */
    public String getValue();

    /**
     * 获取 [建议值]脏标记
     */
    public boolean getValueDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
