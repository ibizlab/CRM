package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_template;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_templateSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_template] 服务对象接口
 */
@Component
public class mail_templateFallback implements mail_templateFeignClient{


    public Mail_template create(Mail_template mail_template){
            return null;
     }
    public Boolean createBatch(List<Mail_template> mail_templates){
            return false;
     }


    public Page<Mail_template> searchDefault(Mail_templateSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Mail_template update(Integer id, Mail_template mail_template){
            return null;
     }
    public Boolean updateBatch(List<Mail_template> mail_templates){
            return false;
     }


    public Mail_template get(Integer id){
            return null;
     }



    public Page<Mail_template> select(){
            return null;
     }

    public Mail_template getDraft(){
            return null;
    }



}
