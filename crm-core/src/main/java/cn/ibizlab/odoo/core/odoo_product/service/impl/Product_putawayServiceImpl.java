package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_putaway;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_putawaySearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_putawayService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_product.client.product_putawayFeignClient;

/**
 * 实体[上架策略] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_putawayServiceImpl implements IProduct_putawayService {

    @Autowired
    product_putawayFeignClient product_putawayFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=product_putawayFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        product_putawayFeignClient.removeBatch(idList);
    }

    @Override
    public Product_putaway getDraft(Product_putaway et) {
        et=product_putawayFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Product_putaway et) {
        Product_putaway rt = product_putawayFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_putaway> list){
        product_putawayFeignClient.createBatch(list) ;
    }

    @Override
    public Product_putaway get(Integer id) {
		Product_putaway et=product_putawayFeignClient.get(id);
        if(et==null){
            et=new Product_putaway();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Product_putaway et) {
        Product_putaway rt = product_putawayFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Product_putaway> list){
        product_putawayFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_putaway> searchDefault(Product_putawaySearchContext context) {
        Page<Product_putaway> product_putaways=product_putawayFeignClient.searchDefault(context);
        return product_putaways;
    }


}


