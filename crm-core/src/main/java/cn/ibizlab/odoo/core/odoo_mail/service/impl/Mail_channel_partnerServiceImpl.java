package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_channel_partner;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_channel_partnerSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_channel_partnerService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_channel_partnerFeignClient;

/**
 * 实体[渠道指南] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_channel_partnerServiceImpl implements IMail_channel_partnerService {

    @Autowired
    mail_channel_partnerFeignClient mail_channel_partnerFeignClient;


    @Override
    public Mail_channel_partner getDraft(Mail_channel_partner et) {
        et=mail_channel_partnerFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Mail_channel_partner et) {
        Mail_channel_partner rt = mail_channel_partnerFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_channel_partner> list){
        mail_channel_partnerFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mail_channel_partnerFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_channel_partnerFeignClient.removeBatch(idList);
    }

    @Override
    public Mail_channel_partner get(Integer id) {
		Mail_channel_partner et=mail_channel_partnerFeignClient.get(id);
        if(et==null){
            et=new Mail_channel_partner();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Mail_channel_partner et) {
        Mail_channel_partner rt = mail_channel_partnerFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_channel_partner> list){
        mail_channel_partnerFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_channel_partner> searchDefault(Mail_channel_partnerSearchContext context) {
        Page<Mail_channel_partner> mail_channel_partners=mail_channel_partnerFeignClient.searchDefault(context);
        return mail_channel_partners;
    }


}


