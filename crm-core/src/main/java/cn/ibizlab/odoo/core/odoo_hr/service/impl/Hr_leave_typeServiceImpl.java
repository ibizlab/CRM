package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave_type;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leave_typeSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_leave_typeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_hr.client.hr_leave_typeFeignClient;

/**
 * 实体[休假类型] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_leave_typeServiceImpl implements IHr_leave_typeService {

    @Autowired
    hr_leave_typeFeignClient hr_leave_typeFeignClient;


    @Override
    public boolean create(Hr_leave_type et) {
        Hr_leave_type rt = hr_leave_typeFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_leave_type> list){
        hr_leave_typeFeignClient.createBatch(list) ;
    }

    @Override
    public Hr_leave_type getDraft(Hr_leave_type et) {
        et=hr_leave_typeFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Hr_leave_type et) {
        Hr_leave_type rt = hr_leave_typeFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Hr_leave_type> list){
        hr_leave_typeFeignClient.updateBatch(list) ;
    }

    @Override
    public Hr_leave_type get(Integer id) {
		Hr_leave_type et=hr_leave_typeFeignClient.get(id);
        if(et==null){
            et=new Hr_leave_type();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=hr_leave_typeFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        hr_leave_typeFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_leave_type> searchDefault(Hr_leave_typeSearchContext context) {
        Page<Hr_leave_type> hr_leave_types=hr_leave_typeFeignClient.searchDefault(context);
        return hr_leave_types;
    }


}


