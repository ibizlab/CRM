package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [account_tax_template] 对象
 */
public interface Iaccount_tax_template {

    /**
     * 获取 [税率科目]
     */
    public void setAccount_id(Integer account_id);
    
    /**
     * 设置 [税率科目]
     */
    public Integer getAccount_id();

    /**
     * 获取 [税率科目]脏标记
     */
    public boolean getAccount_idDirtyFlag();
    /**
     * 获取 [税率科目]
     */
    public void setAccount_id_text(String account_id_text);
    
    /**
     * 设置 [税率科目]
     */
    public String getAccount_id_text();

    /**
     * 获取 [税率科目]脏标记
     */
    public boolean getAccount_id_textDirtyFlag();
    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [金额]
     */
    public void setAmount(Double amount);
    
    /**
     * 设置 [金额]
     */
    public Double getAmount();

    /**
     * 获取 [金额]脏标记
     */
    public boolean getAmountDirtyFlag();
    /**
     * 获取 [税率计算]
     */
    public void setAmount_type(String amount_type);
    
    /**
     * 设置 [税率计算]
     */
    public String getAmount_type();

    /**
     * 获取 [税率计算]脏标记
     */
    public boolean getAmount_typeDirtyFlag();
    /**
     * 获取 [分析成本]
     */
    public void setAnalytic(String analytic);
    
    /**
     * 设置 [分析成本]
     */
    public String getAnalytic();

    /**
     * 获取 [分析成本]脏标记
     */
    public boolean getAnalyticDirtyFlag();
    /**
     * 获取 [税应收科目]
     */
    public void setCash_basis_account_id(Integer cash_basis_account_id);
    
    /**
     * 设置 [税应收科目]
     */
    public Integer getCash_basis_account_id();

    /**
     * 获取 [税应收科目]脏标记
     */
    public boolean getCash_basis_account_idDirtyFlag();
    /**
     * 获取 [税应收科目]
     */
    public void setCash_basis_account_id_text(String cash_basis_account_id_text);
    
    /**
     * 设置 [税应收科目]
     */
    public String getCash_basis_account_id_text();

    /**
     * 获取 [税应收科目]脏标记
     */
    public boolean getCash_basis_account_id_textDirtyFlag();
    /**
     * 获取 [基本税应收科目]
     */
    public void setCash_basis_base_account_id(Integer cash_basis_base_account_id);
    
    /**
     * 设置 [基本税应收科目]
     */
    public Integer getCash_basis_base_account_id();

    /**
     * 获取 [基本税应收科目]脏标记
     */
    public boolean getCash_basis_base_account_idDirtyFlag();
    /**
     * 获取 [基本税应收科目]
     */
    public void setCash_basis_base_account_id_text(String cash_basis_base_account_id_text);
    
    /**
     * 设置 [基本税应收科目]
     */
    public String getCash_basis_base_account_id_text();

    /**
     * 获取 [基本税应收科目]脏标记
     */
    public boolean getCash_basis_base_account_id_textDirtyFlag();
    /**
     * 获取 [表模板]
     */
    public void setChart_template_id(Integer chart_template_id);
    
    /**
     * 设置 [表模板]
     */
    public Integer getChart_template_id();

    /**
     * 获取 [表模板]脏标记
     */
    public boolean getChart_template_idDirtyFlag();
    /**
     * 获取 [表模板]
     */
    public void setChart_template_id_text(String chart_template_id_text);
    
    /**
     * 设置 [表模板]
     */
    public String getChart_template_id_text();

    /**
     * 获取 [表模板]脏标记
     */
    public boolean getChart_template_id_textDirtyFlag();
    /**
     * 获取 [子级税]
     */
    public void setChildren_tax_ids(String children_tax_ids);
    
    /**
     * 设置 [子级税]
     */
    public String getChildren_tax_ids();

    /**
     * 获取 [子级税]脏标记
     */
    public boolean getChildren_tax_idsDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示在发票上]
     */
    public void setDescription(String description);
    
    /**
     * 设置 [显示在发票上]
     */
    public String getDescription();

    /**
     * 获取 [显示在发票上]脏标记
     */
    public boolean getDescriptionDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [影响后续税收]
     */
    public void setInclude_base_amount(String include_base_amount);
    
    /**
     * 设置 [影响后续税收]
     */
    public String getInclude_base_amount();

    /**
     * 获取 [影响后续税收]脏标记
     */
    public boolean getInclude_base_amountDirtyFlag();
    /**
     * 获取 [税率名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [税率名称]
     */
    public String getName();

    /**
     * 获取 [税率名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [包含在价格中]
     */
    public void setPrice_include(String price_include);
    
    /**
     * 设置 [包含在价格中]
     */
    public String getPrice_include();

    /**
     * 获取 [包含在价格中]脏标记
     */
    public boolean getPrice_includeDirtyFlag();
    /**
     * 获取 [退款的税金科目]
     */
    public void setRefund_account_id(Integer refund_account_id);
    
    /**
     * 设置 [退款的税金科目]
     */
    public Integer getRefund_account_id();

    /**
     * 获取 [退款的税金科目]脏标记
     */
    public boolean getRefund_account_idDirtyFlag();
    /**
     * 获取 [退款的税金科目]
     */
    public void setRefund_account_id_text(String refund_account_id_text);
    
    /**
     * 设置 [退款的税金科目]
     */
    public String getRefund_account_id_text();

    /**
     * 获取 [退款的税金科目]脏标记
     */
    public boolean getRefund_account_id_textDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [科目标签]
     */
    public void setTag_ids(String tag_ids);
    
    /**
     * 设置 [科目标签]
     */
    public String getTag_ids();

    /**
     * 获取 [科目标签]脏标记
     */
    public boolean getTag_idsDirtyFlag();
    /**
     * 获取 [应有税金]
     */
    public void setTax_exigibility(String tax_exigibility);
    
    /**
     * 设置 [应有税金]
     */
    public String getTax_exigibility();

    /**
     * 获取 [应有税金]脏标记
     */
    public boolean getTax_exigibilityDirtyFlag();
    /**
     * 获取 [税组]
     */
    public void setTax_group_id(Integer tax_group_id);
    
    /**
     * 设置 [税组]
     */
    public Integer getTax_group_id();

    /**
     * 获取 [税组]脏标记
     */
    public boolean getTax_group_idDirtyFlag();
    /**
     * 获取 [税组]
     */
    public void setTax_group_id_text(String tax_group_id_text);
    
    /**
     * 设置 [税组]
     */
    public String getTax_group_id_text();

    /**
     * 获取 [税组]脏标记
     */
    public boolean getTax_group_id_textDirtyFlag();
    /**
     * 获取 [税范围]
     */
    public void setType_tax_use(String type_tax_use);
    
    /**
     * 设置 [税范围]
     */
    public String getType_tax_use();

    /**
     * 获取 [税范围]脏标记
     */
    public boolean getType_tax_useDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
