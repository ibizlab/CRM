package cn.ibizlab.odoo.core.odoo_event.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_confirm;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_confirmSearchContext;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_confirmService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_event.client.event_confirmFeignClient;

/**
 * 实体[活动确认] 服务对象接口实现
 */
@Slf4j
@Service
public class Event_confirmServiceImpl implements IEvent_confirmService {

    @Autowired
    event_confirmFeignClient event_confirmFeignClient;


    @Override
    public Event_confirm getDraft(Event_confirm et) {
        et=event_confirmFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=event_confirmFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        event_confirmFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Event_confirm et) {
        Event_confirm rt = event_confirmFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Event_confirm> list){
        event_confirmFeignClient.updateBatch(list) ;
    }

    @Override
    public Event_confirm get(Integer id) {
		Event_confirm et=event_confirmFeignClient.get(id);
        if(et==null){
            et=new Event_confirm();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Event_confirm et) {
        Event_confirm rt = event_confirmFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Event_confirm> list){
        event_confirmFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Event_confirm> searchDefault(Event_confirmSearchContext context) {
        Page<Event_confirm> event_confirms=event_confirmFeignClient.searchDefault(context);
        return event_confirms;
    }


}


