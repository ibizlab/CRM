package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [mail_channel_partner] 对象
 */
public interface Imail_channel_partner {

    /**
     * 获取 [渠道]
     */
    public void setChannel_id(Integer channel_id);
    
    /**
     * 设置 [渠道]
     */
    public Integer getChannel_id();

    /**
     * 获取 [渠道]脏标记
     */
    public boolean getChannel_idDirtyFlag();
    /**
     * 获取 [渠道]
     */
    public void setChannel_id_text(String channel_id_text);
    
    /**
     * 设置 [渠道]
     */
    public String getChannel_id_text();

    /**
     * 获取 [渠道]脏标记
     */
    public boolean getChannel_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [对话收拢状态]
     */
    public void setFold_state(String fold_state);
    
    /**
     * 设置 [对话收拢状态]
     */
    public String getFold_state();

    /**
     * 获取 [对话收拢状态]脏标记
     */
    public boolean getFold_stateDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [黑名单]
     */
    public void setIs_blacklisted(String is_blacklisted);
    
    /**
     * 设置 [黑名单]
     */
    public String getIs_blacklisted();

    /**
     * 获取 [黑名单]脏标记
     */
    public boolean getIs_blacklistedDirtyFlag();
    /**
     * 获取 [对话已最小化]
     */
    public void setIs_minimized(String is_minimized);
    
    /**
     * 设置 [对话已最小化]
     */
    public String getIs_minimized();

    /**
     * 获取 [对话已最小化]脏标记
     */
    public boolean getIs_minimizedDirtyFlag();
    /**
     * 获取 [是否置顶]
     */
    public void setIs_pinned(String is_pinned);
    
    /**
     * 设置 [是否置顶]
     */
    public String getIs_pinned();

    /**
     * 获取 [是否置顶]脏标记
     */
    public boolean getIs_pinnedDirtyFlag();
    /**
     * 获取 [EMail]
     */
    public void setPartner_email(String partner_email);
    
    /**
     * 设置 [EMail]
     */
    public String getPartner_email();

    /**
     * 获取 [EMail]脏标记
     */
    public boolean getPartner_emailDirtyFlag();
    /**
     * 获取 [收件人]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [收件人]
     */
    public Integer getPartner_id();

    /**
     * 获取 [收件人]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [收件人]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [收件人]
     */
    public String getPartner_id_text();

    /**
     * 获取 [收件人]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [最近一次查阅]
     */
    public void setSeen_message_id(Integer seen_message_id);
    
    /**
     * 设置 [最近一次查阅]
     */
    public Integer getSeen_message_id();

    /**
     * 获取 [最近一次查阅]脏标记
     */
    public boolean getSeen_message_idDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
