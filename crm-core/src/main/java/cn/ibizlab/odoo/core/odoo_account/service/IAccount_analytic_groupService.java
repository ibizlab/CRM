package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_group;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_groupSearchContext;


/**
 * 实体[Account_analytic_group] 服务对象接口
 */
public interface IAccount_analytic_groupService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Account_analytic_group et) ;
    void createBatch(List<Account_analytic_group> list) ;
    boolean update(Account_analytic_group et) ;
    void updateBatch(List<Account_analytic_group> list) ;
    Account_analytic_group getDraft(Account_analytic_group et) ;
    Account_analytic_group get(Integer key) ;
    Page<Account_analytic_group> searchDefault(Account_analytic_groupSearchContext context) ;

}



