package cn.ibizlab.odoo.core.odoo_crm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead_tag;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lead_tagSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[crm_lead_tag] 服务对象接口
 */
@FeignClient(value = "odoo-crm", contextId = "crm-lead-tag", fallback = crm_lead_tagFallback.class)
public interface crm_lead_tagFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead_tags/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead_tags/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/crm_lead_tags/{id}")
    Crm_lead_tag get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/crm_lead_tags/{id}")
    Crm_lead_tag update(@PathVariable("id") Integer id,@RequestBody Crm_lead_tag crm_lead_tag);

    @RequestMapping(method = RequestMethod.PUT, value = "/crm_lead_tags/batch")
    Boolean updateBatch(@RequestBody List<Crm_lead_tag> crm_lead_tags);



    @RequestMapping(method = RequestMethod.POST, value = "/crm_lead_tags")
    Crm_lead_tag create(@RequestBody Crm_lead_tag crm_lead_tag);

    @RequestMapping(method = RequestMethod.POST, value = "/crm_lead_tags/batch")
    Boolean createBatch(@RequestBody List<Crm_lead_tag> crm_lead_tags);



    @RequestMapping(method = RequestMethod.POST, value = "/crm_lead_tags/searchdefault")
    Page<Crm_lead_tag> searchDefault(@RequestBody Crm_lead_tagSearchContext context);




    @RequestMapping(method = RequestMethod.GET, value = "/crm_lead_tags/select")
    Page<Crm_lead_tag> select();


    @RequestMapping(method = RequestMethod.GET, value = "/crm_lead_tags/getdraft")
    Crm_lead_tag getDraft();


}
