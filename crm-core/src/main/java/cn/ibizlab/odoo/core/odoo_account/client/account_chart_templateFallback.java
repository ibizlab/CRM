package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_chart_template;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_chart_templateSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_chart_template] 服务对象接口
 */
@Component
public class account_chart_templateFallback implements account_chart_templateFeignClient{


    public Page<Account_chart_template> searchDefault(Account_chart_templateSearchContext context){
            return null;
     }


    public Account_chart_template create(Account_chart_template account_chart_template){
            return null;
     }
    public Boolean createBatch(List<Account_chart_template> account_chart_templates){
            return false;
     }

    public Account_chart_template get(Integer id){
            return null;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Account_chart_template update(Integer id, Account_chart_template account_chart_template){
            return null;
     }
    public Boolean updateBatch(List<Account_chart_template> account_chart_templates){
            return false;
     }



    public Page<Account_chart_template> select(){
            return null;
     }

    public Account_chart_template getDraft(){
            return null;
    }



}
