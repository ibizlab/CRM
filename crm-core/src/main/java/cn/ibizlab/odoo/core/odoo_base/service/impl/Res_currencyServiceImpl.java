package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_currency;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_currencySearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_currencyService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.res_currencyFeignClient;

/**
 * 实体[币种] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_currencyServiceImpl implements IRes_currencyService {

    @Autowired
    res_currencyFeignClient res_currencyFeignClient;


    @Override
    public Res_currency get(Integer id) {
		Res_currency et=res_currencyFeignClient.get(id);
        if(et==null){
            et=new Res_currency();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Res_currency et) {
        Res_currency rt = res_currencyFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Res_currency> list){
        res_currencyFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=res_currencyFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        res_currencyFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Res_currency et) {
        Res_currency rt = res_currencyFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_currency> list){
        res_currencyFeignClient.createBatch(list) ;
    }

    @Override
    public Res_currency getDraft(Res_currency et) {
        et=res_currencyFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_currency> searchDefault(Res_currencySearchContext context) {
        Page<Res_currency> res_currencys=res_currencyFeignClient.searchDefault(context);
        return res_currencys;
    }


}


