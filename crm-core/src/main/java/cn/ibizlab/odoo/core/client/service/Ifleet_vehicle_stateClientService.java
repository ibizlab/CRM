package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_state;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[fleet_vehicle_state] 服务对象接口
 */
public interface Ifleet_vehicle_stateClientService{

    public Ifleet_vehicle_state createModel() ;

    public void createBatch(List<Ifleet_vehicle_state> fleet_vehicle_states);

    public void create(Ifleet_vehicle_state fleet_vehicle_state);

    public void updateBatch(List<Ifleet_vehicle_state> fleet_vehicle_states);

    public void removeBatch(List<Ifleet_vehicle_state> fleet_vehicle_states);

    public void remove(Ifleet_vehicle_state fleet_vehicle_state);

    public Page<Ifleet_vehicle_state> fetchDefault(SearchContext context);

    public void update(Ifleet_vehicle_state fleet_vehicle_state);

    public void get(Ifleet_vehicle_state fleet_vehicle_state);

    public Page<Ifleet_vehicle_state> select(SearchContext context);

    public void getDraft(Ifleet_vehicle_state fleet_vehicle_state);

}
