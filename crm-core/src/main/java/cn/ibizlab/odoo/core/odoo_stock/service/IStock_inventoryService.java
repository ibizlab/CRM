package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_inventory;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_inventorySearchContext;


/**
 * 实体[Stock_inventory] 服务对象接口
 */
public interface IStock_inventoryService{

    boolean create(Stock_inventory et) ;
    void createBatch(List<Stock_inventory> list) ;
    Stock_inventory getDraft(Stock_inventory et) ;
    Stock_inventory get(Integer key) ;
    boolean update(Stock_inventory et) ;
    void updateBatch(List<Stock_inventory> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Stock_inventory> searchDefault(Stock_inventorySearchContext context) ;

}



