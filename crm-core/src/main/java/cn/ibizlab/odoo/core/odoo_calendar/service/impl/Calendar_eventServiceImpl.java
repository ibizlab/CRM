package cn.ibizlab.odoo.core.odoo_calendar.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_event;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_eventSearchContext;
import cn.ibizlab.odoo.core.odoo_calendar.service.ICalendar_eventService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_calendar.client.calendar_eventFeignClient;

/**
 * 实体[活动] 服务对象接口实现
 */
@Slf4j
@Service
public class Calendar_eventServiceImpl implements ICalendar_eventService {

    @Autowired
    calendar_eventFeignClient calendar_eventFeignClient;


    @Override
    public boolean update(Calendar_event et) {
        Calendar_event rt = calendar_eventFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Calendar_event> list){
        calendar_eventFeignClient.updateBatch(list) ;
    }

    @Override
    public Calendar_event get(Integer id) {
		Calendar_event et=calendar_eventFeignClient.get(id);
        if(et==null){
            et=new Calendar_event();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=calendar_eventFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        calendar_eventFeignClient.removeBatch(idList);
    }

    @Override
    public Calendar_event getDraft(Calendar_event et) {
        et=calendar_eventFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Calendar_event et) {
        Calendar_event rt = calendar_eventFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Calendar_event> list){
        calendar_eventFeignClient.createBatch(list) ;
    }

    @Override
    @Transactional
    public boolean save(Calendar_event et) {
        if(et.getId()==null) et.setId((Integer)et.getDefaultKey(true));
        if(!calendar_eventFeignClient.save(et))
            return false;
        return true;
    }

    @Override
    public void saveBatch(List<Calendar_event> list) {
        calendar_eventFeignClient.saveBatch(list) ;
    }

    @Override
    public boolean checkKey(Calendar_event et) {
        return calendar_eventFeignClient.checkKey(et);
    }




    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Calendar_event> searchDefault(Calendar_eventSearchContext context) {
        Page<Calendar_event> calendar_events=calendar_eventFeignClient.searchDefault(context);
        return calendar_events;
    }


}


