package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Icalendar_alarm_manager;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[calendar_alarm_manager] 服务对象接口
 */
public interface Icalendar_alarm_managerClientService{

    public Icalendar_alarm_manager createModel() ;

    public void updateBatch(List<Icalendar_alarm_manager> calendar_alarm_managers);

    public void update(Icalendar_alarm_manager calendar_alarm_manager);

    public void removeBatch(List<Icalendar_alarm_manager> calendar_alarm_managers);

    public void get(Icalendar_alarm_manager calendar_alarm_manager);

    public void remove(Icalendar_alarm_manager calendar_alarm_manager);

    public void create(Icalendar_alarm_manager calendar_alarm_manager);

    public Page<Icalendar_alarm_manager> fetchDefault(SearchContext context);

    public void createBatch(List<Icalendar_alarm_manager> calendar_alarm_managers);

    public Page<Icalendar_alarm_manager> select(SearchContext context);

    public void getDraft(Icalendar_alarm_manager calendar_alarm_manager);

}
