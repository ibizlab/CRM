package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_wizard_invite;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_wizard_inviteSearchContext;


/**
 * 实体[Mail_wizard_invite] 服务对象接口
 */
public interface IMail_wizard_inviteService{

    Mail_wizard_invite getDraft(Mail_wizard_invite et) ;
    Mail_wizard_invite get(Integer key) ;
    boolean create(Mail_wizard_invite et) ;
    void createBatch(List<Mail_wizard_invite> list) ;
    boolean update(Mail_wizard_invite et) ;
    void updateBatch(List<Mail_wizard_invite> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Mail_wizard_invite> searchDefault(Mail_wizard_inviteSearchContext context) ;

}



