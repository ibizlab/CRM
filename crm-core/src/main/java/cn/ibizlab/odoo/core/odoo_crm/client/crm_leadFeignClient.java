package cn.ibizlab.odoo.core.odoo_crm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_leadSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[crm_lead] 服务对象接口
 */
@FeignClient(value = "odoo-crm", contextId = "crm-lead", fallback = crm_leadFallback.class)
public interface crm_leadFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/crm_leads/{id}")
    Crm_lead update(@PathVariable("id") Integer id,@RequestBody Crm_lead crm_lead);

    @RequestMapping(method = RequestMethod.PUT, value = "/crm_leads/batch")
    Boolean updateBatch(@RequestBody List<Crm_lead> crm_leads);




    @RequestMapping(method = RequestMethod.POST, value = "/crm_leads/searchdefault")
    Page<Crm_lead> searchDefault(@RequestBody Crm_leadSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/crm_leads")
    Crm_lead create(@RequestBody Crm_lead crm_lead);

    @RequestMapping(method = RequestMethod.POST, value = "/crm_leads/batch")
    Boolean createBatch(@RequestBody List<Crm_lead> crm_leads);



    @RequestMapping(method = RequestMethod.GET, value = "/crm_leads/{id}")
    Crm_lead get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.DELETE, value = "/crm_leads/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/crm_leads/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/crm_leads/select")
    Page<Crm_lead> select();


    @RequestMapping(method = RequestMethod.GET, value = "/crm_leads/getdraft")
    Crm_lead getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/crm_leads/save")
    Boolean save(@RequestBody Crm_lead crm_lead);

    @RequestMapping(method = RequestMethod.POST, value = "/crm_leads/save")
    Boolean saveBatch(@RequestBody List<Crm_lead> crm_leads);


    @RequestMapping(method = RequestMethod.POST, value = "/crm_leads/checkkey")
    Boolean checkKey(@RequestBody Crm_lead crm_lead);


}
