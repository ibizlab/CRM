package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iasset_category;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[asset_category] 服务对象接口
 */
public interface Iasset_categoryClientService{

    public Iasset_category createModel() ;

    public void create(Iasset_category asset_category);

    public void get(Iasset_category asset_category);

    public Page<Iasset_category> fetchDefault(SearchContext context);

    public void remove(Iasset_category asset_category);

    public void removeBatch(List<Iasset_category> asset_categories);

    public void createBatch(List<Iasset_category> asset_categories);

    public void update(Iasset_category asset_category);

    public void updateBatch(List<Iasset_category> asset_categories);

    public Page<Iasset_category> select(SearchContext context);

    public void getDraft(Iasset_category asset_category);

}
