package cn.ibizlab.odoo.core.odoo_base.valuerule.anno.base_automation_lead_test;

import cn.ibizlab.odoo.core.odoo_base.valuerule.validator.base_automation_lead_test.Base_automation_lead_testDisplay_nameDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Base_automation_lead_test
 * 属性：Display_name
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Base_automation_lead_testDisplay_nameDefaultValidator.class})
public @interface Base_automation_lead_testDisplay_nameDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[100]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
