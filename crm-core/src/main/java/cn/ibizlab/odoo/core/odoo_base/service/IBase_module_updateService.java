package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Base_module_update;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_module_updateSearchContext;


/**
 * 实体[Base_module_update] 服务对象接口
 */
public interface IBase_module_updateService{

    Base_module_update getDraft(Base_module_update et) ;
    Base_module_update get(Integer key) ;
    boolean update(Base_module_update et) ;
    void updateBatch(List<Base_module_update> list) ;
    boolean create(Base_module_update et) ;
    void createBatch(List<Base_module_update> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Base_module_update> searchDefault(Base_module_updateSearchContext context) ;

}



