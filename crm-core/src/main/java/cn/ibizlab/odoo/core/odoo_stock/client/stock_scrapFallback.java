package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_scrap;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_scrapSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_scrap] 服务对象接口
 */
@Component
public class stock_scrapFallback implements stock_scrapFeignClient{

    public Page<Stock_scrap> searchDefault(Stock_scrapSearchContext context){
            return null;
     }




    public Stock_scrap get(Integer id){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Stock_scrap create(Stock_scrap stock_scrap){
            return null;
     }
    public Boolean createBatch(List<Stock_scrap> stock_scraps){
            return false;
     }


    public Stock_scrap update(Integer id, Stock_scrap stock_scrap){
            return null;
     }
    public Boolean updateBatch(List<Stock_scrap> stock_scraps){
            return false;
     }


    public Page<Stock_scrap> select(){
            return null;
     }

    public Stock_scrap getDraft(){
            return null;
    }



}
