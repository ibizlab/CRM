package cn.ibizlab.odoo.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [税率科目调整模板] 对象
 */
@Data
public class Account_fiscal_position_tax_template extends EntityClient implements Serializable {

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 税源
     */
    @JSONField(name = "tax_src_id_text")
    @JsonProperty("tax_src_id_text")
    private String taxSrcIdText;

    /**
     * 税科目调整
     */
    @JSONField(name = "position_id_text")
    @JsonProperty("position_id_text")
    private String positionIdText;

    /**
     * 替代税
     */
    @JSONField(name = "tax_dest_id_text")
    @JsonProperty("tax_dest_id_text")
    private String taxDestIdText;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 税科目调整
     */
    @DEField(name = "position_id")
    @JSONField(name = "position_id")
    @JsonProperty("position_id")
    private Integer positionId;

    /**
     * 税源
     */
    @DEField(name = "tax_src_id")
    @JSONField(name = "tax_src_id")
    @JsonProperty("tax_src_id")
    private Integer taxSrcId;

    /**
     * 替代税
     */
    @DEField(name = "tax_dest_id")
    @JSONField(name = "tax_dest_id")
    @JsonProperty("tax_dest_id")
    private Integer taxDestId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 
     */
    @JSONField(name = "odooposition")
    @JsonProperty("odooposition")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position_template odooPosition;

    /**
     * 
     */
    @JSONField(name = "odootaxdest")
    @JsonProperty("odootaxdest")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_tax_template odooTaxDest;

    /**
     * 
     */
    @JSONField(name = "odootaxsrc")
    @JsonProperty("odootaxsrc")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_tax_template odooTaxSrc;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [税科目调整]
     */
    public void setPositionId(Integer positionId){
        this.positionId = positionId ;
        this.modify("position_id",positionId);
    }
    /**
     * 设置 [税源]
     */
    public void setTaxSrcId(Integer taxSrcId){
        this.taxSrcId = taxSrcId ;
        this.modify("tax_src_id",taxSrcId);
    }
    /**
     * 设置 [替代税]
     */
    public void setTaxDestId(Integer taxDestId){
        this.taxDestId = taxDestId ;
        this.modify("tax_dest_id",taxDestId);
    }

}


