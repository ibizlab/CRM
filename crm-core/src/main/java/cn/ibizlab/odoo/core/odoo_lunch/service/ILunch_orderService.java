package cn.ibizlab.odoo.core.odoo_lunch.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_order;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_orderSearchContext;


/**
 * 实体[Lunch_order] 服务对象接口
 */
public interface ILunch_orderService{

    Lunch_order getDraft(Lunch_order et) ;
    Lunch_order get(Integer key) ;
    boolean create(Lunch_order et) ;
    void createBatch(List<Lunch_order> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Lunch_order et) ;
    void updateBatch(List<Lunch_order> list) ;
    Page<Lunch_order> searchDefault(Lunch_orderSearchContext context) ;

}



