package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iportal_share;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[portal_share] 服务对象接口
 */
public interface Iportal_shareClientService{

    public Iportal_share createModel() ;

    public void create(Iportal_share portal_share);

    public void get(Iportal_share portal_share);

    public void removeBatch(List<Iportal_share> portal_shares);

    public void updateBatch(List<Iportal_share> portal_shares);

    public void createBatch(List<Iportal_share> portal_shares);

    public void update(Iportal_share portal_share);

    public void remove(Iportal_share portal_share);

    public Page<Iportal_share> fetchDefault(SearchContext context);

    public Page<Iportal_share> select(SearchContext context);

    public void getDraft(Iportal_share portal_share);

}
