package cn.ibizlab.odoo.core.odoo_survey.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_user_input_line;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_user_input_lineSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[survey_user_input_line] 服务对象接口
 */
@FeignClient(value = "odoo-survey", contextId = "survey-user-input-line", fallback = survey_user_input_lineFallback.class)
public interface survey_user_input_lineFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/survey_user_input_lines/searchdefault")
    Page<Survey_user_input_line> searchDefault(@RequestBody Survey_user_input_lineSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/survey_user_input_lines/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/survey_user_input_lines/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/survey_user_input_lines/{id}")
    Survey_user_input_line get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/survey_user_input_lines/{id}")
    Survey_user_input_line update(@PathVariable("id") Integer id,@RequestBody Survey_user_input_line survey_user_input_line);

    @RequestMapping(method = RequestMethod.PUT, value = "/survey_user_input_lines/batch")
    Boolean updateBatch(@RequestBody List<Survey_user_input_line> survey_user_input_lines);



    @RequestMapping(method = RequestMethod.POST, value = "/survey_user_input_lines")
    Survey_user_input_line create(@RequestBody Survey_user_input_line survey_user_input_line);

    @RequestMapping(method = RequestMethod.POST, value = "/survey_user_input_lines/batch")
    Boolean createBatch(@RequestBody List<Survey_user_input_line> survey_user_input_lines);



    @RequestMapping(method = RequestMethod.GET, value = "/survey_user_input_lines/select")
    Page<Survey_user_input_line> select();


    @RequestMapping(method = RequestMethod.GET, value = "/survey_user_input_lines/getdraft")
    Survey_user_input_line getDraft();


}
