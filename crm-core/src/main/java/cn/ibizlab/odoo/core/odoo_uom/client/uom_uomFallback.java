package cn.ibizlab.odoo.core.odoo_uom.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_uom.domain.Uom_uom;
import cn.ibizlab.odoo.core.odoo_uom.filter.Uom_uomSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[uom_uom] 服务对象接口
 */
@Component
public class uom_uomFallback implements uom_uomFeignClient{


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Uom_uom create(Uom_uom uom_uom){
            return null;
     }
    public Boolean createBatch(List<Uom_uom> uom_uoms){
            return false;
     }

    public Page<Uom_uom> searchDefault(Uom_uomSearchContext context){
            return null;
     }


    public Uom_uom get(Integer id){
            return null;
     }



    public Uom_uom update(Integer id, Uom_uom uom_uom){
            return null;
     }
    public Boolean updateBatch(List<Uom_uom> uom_uoms){
            return false;
     }


    public Page<Uom_uom> select(){
            return null;
     }

    public Uom_uom getDraft(){
            return null;
    }



}
