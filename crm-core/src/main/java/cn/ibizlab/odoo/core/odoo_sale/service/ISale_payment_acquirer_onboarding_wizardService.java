package cn.ibizlab.odoo.core.odoo_sale.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_payment_acquirer_onboarding_wizardSearchContext;


/**
 * 实体[Sale_payment_acquirer_onboarding_wizard] 服务对象接口
 */
public interface ISale_payment_acquirer_onboarding_wizardService{

    Sale_payment_acquirer_onboarding_wizard get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Sale_payment_acquirer_onboarding_wizard getDraft(Sale_payment_acquirer_onboarding_wizard et) ;
    boolean update(Sale_payment_acquirer_onboarding_wizard et) ;
    void updateBatch(List<Sale_payment_acquirer_onboarding_wizard> list) ;
    boolean create(Sale_payment_acquirer_onboarding_wizard et) ;
    void createBatch(List<Sale_payment_acquirer_onboarding_wizard> list) ;
    Page<Sale_payment_acquirer_onboarding_wizard> searchDefault(Sale_payment_acquirer_onboarding_wizardSearchContext context) ;

}



