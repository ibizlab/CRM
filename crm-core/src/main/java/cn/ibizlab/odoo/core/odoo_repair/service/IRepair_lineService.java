package cn.ibizlab.odoo.core.odoo_repair.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_line;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_lineSearchContext;


/**
 * 实体[Repair_line] 服务对象接口
 */
public interface IRepair_lineService{

    Repair_line get(Integer key) ;
    Repair_line getDraft(Repair_line et) ;
    boolean update(Repair_line et) ;
    void updateBatch(List<Repair_line> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Repair_line et) ;
    void createBatch(List<Repair_line> list) ;
    Page<Repair_line> searchDefault(Repair_lineSearchContext context) ;

}



