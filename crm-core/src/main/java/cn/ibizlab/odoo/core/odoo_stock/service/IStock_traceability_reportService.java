package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_traceability_report;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_traceability_reportSearchContext;


/**
 * 实体[Stock_traceability_report] 服务对象接口
 */
public interface IStock_traceability_reportService{

    boolean create(Stock_traceability_report et) ;
    void createBatch(List<Stock_traceability_report> list) ;
    Stock_traceability_report getDraft(Stock_traceability_report et) ;
    Stock_traceability_report get(Integer key) ;
    boolean update(Stock_traceability_report et) ;
    void updateBatch(List<Stock_traceability_report> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Stock_traceability_report> searchDefault(Stock_traceability_reportSearchContext context) ;

}



