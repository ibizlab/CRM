package cn.ibizlab.odoo.core.odoo_crm.valuerule.anno.crm_lost_reason;

import cn.ibizlab.odoo.core.odoo_crm.valuerule.validator.crm_lost_reason.Crm_lost_reasonActiveDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Crm_lost_reason
 * 属性：Active
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Crm_lost_reasonActiveDefaultValidator.class})
public @interface Crm_lost_reasonActiveDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
