package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_contact;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_contactSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailing_contactService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_mass_mailing_contactFeignClient;

/**
 * 实体[群发邮件联系人] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_mass_mailing_contactServiceImpl implements IMail_mass_mailing_contactService {

    @Autowired
    mail_mass_mailing_contactFeignClient mail_mass_mailing_contactFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=mail_mass_mailing_contactFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_mass_mailing_contactFeignClient.removeBatch(idList);
    }

    @Override
    public Mail_mass_mailing_contact get(Integer id) {
		Mail_mass_mailing_contact et=mail_mass_mailing_contactFeignClient.get(id);
        if(et==null){
            et=new Mail_mass_mailing_contact();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Mail_mass_mailing_contact et) {
        Mail_mass_mailing_contact rt = mail_mass_mailing_contactFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_mass_mailing_contact> list){
        mail_mass_mailing_contactFeignClient.updateBatch(list) ;
    }

    @Override
    public Mail_mass_mailing_contact getDraft(Mail_mass_mailing_contact et) {
        et=mail_mass_mailing_contactFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Mail_mass_mailing_contact et) {
        Mail_mass_mailing_contact rt = mail_mass_mailing_contactFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_mass_mailing_contact> list){
        mail_mass_mailing_contactFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_mass_mailing_contact> searchDefault(Mail_mass_mailing_contactSearchContext context) {
        Page<Mail_mass_mailing_contact> mail_mass_mailing_contacts=mail_mass_mailing_contactFeignClient.searchDefault(context);
        return mail_mass_mailing_contacts;
    }


}


