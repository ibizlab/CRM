package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Isurvey_page;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[survey_page] 服务对象接口
 */
public interface Isurvey_pageClientService{

    public Isurvey_page createModel() ;

    public void get(Isurvey_page survey_page);

    public void updateBatch(List<Isurvey_page> survey_pages);

    public void update(Isurvey_page survey_page);

    public void createBatch(List<Isurvey_page> survey_pages);

    public void removeBatch(List<Isurvey_page> survey_pages);

    public Page<Isurvey_page> fetchDefault(SearchContext context);

    public void remove(Isurvey_page survey_page);

    public void create(Isurvey_page survey_page);

    public Page<Isurvey_page> select(SearchContext context);

    public void getDraft(Isurvey_page survey_page);

}
