package cn.ibizlab.odoo.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_task_parts_line;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_task_parts_lineSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mro_task_parts_line] 服务对象接口
 */
@FeignClient(value = "odoo-mro", contextId = "mro-task-parts-line", fallback = mro_task_parts_lineFallback.class)
public interface mro_task_parts_lineFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/mro_task_parts_lines")
    Mro_task_parts_line create(@RequestBody Mro_task_parts_line mro_task_parts_line);

    @RequestMapping(method = RequestMethod.POST, value = "/mro_task_parts_lines/batch")
    Boolean createBatch(@RequestBody List<Mro_task_parts_line> mro_task_parts_lines);



    @RequestMapping(method = RequestMethod.POST, value = "/mro_task_parts_lines/searchdefault")
    Page<Mro_task_parts_line> searchDefault(@RequestBody Mro_task_parts_lineSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/mro_task_parts_lines/{id}")
    Mro_task_parts_line get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_task_parts_lines/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_task_parts_lines/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/mro_task_parts_lines/{id}")
    Mro_task_parts_line update(@PathVariable("id") Integer id,@RequestBody Mro_task_parts_line mro_task_parts_line);

    @RequestMapping(method = RequestMethod.PUT, value = "/mro_task_parts_lines/batch")
    Boolean updateBatch(@RequestBody List<Mro_task_parts_line> mro_task_parts_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/mro_task_parts_lines/select")
    Page<Mro_task_parts_line> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mro_task_parts_lines/getdraft")
    Mro_task_parts_line getDraft();


}
