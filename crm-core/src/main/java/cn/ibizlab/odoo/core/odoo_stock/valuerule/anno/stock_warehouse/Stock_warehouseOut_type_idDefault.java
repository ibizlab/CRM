package cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_warehouse;

import cn.ibizlab.odoo.core.odoo_stock.valuerule.validator.stock_warehouse.Stock_warehouseOut_type_idDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Stock_warehouse
 * 属性：Out_type_id
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Stock_warehouseOut_type_idDefaultValidator.class})
public @interface Stock_warehouseOut_type_idDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
