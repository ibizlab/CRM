package cn.ibizlab.odoo.core.odoo_event.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [登记邮件调度] 对象
 */
@Data
public class Event_mail_registration extends EntityClient implements Serializable {

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * EMail发送
     */
    @DEField(name = "mail_sent")
    @JSONField(name = "mail_sent")
    @JsonProperty("mail_sent")
    private String mailSent;

    /**
     * 定期时间
     */
    @DEField(name = "scheduled_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduled_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduled_date")
    private Timestamp scheduledDate;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 出席者
     */
    @JSONField(name = "registration_id_text")
    @JsonProperty("registration_id_text")
    private String registrationIdText;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 出席者
     */
    @DEField(name = "registration_id")
    @JSONField(name = "registration_id")
    @JsonProperty("registration_id")
    private Integer registrationId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 邮件调度
     */
    @DEField(name = "scheduler_id")
    @JSONField(name = "scheduler_id")
    @JsonProperty("scheduler_id")
    private Integer schedulerId;


    /**
     * 
     */
    @JSONField(name = "odooscheduler")
    @JsonProperty("odooscheduler")
    private cn.ibizlab.odoo.core.odoo_event.domain.Event_mail odooScheduler;

    /**
     * 
     */
    @JSONField(name = "odooregistration")
    @JsonProperty("odooregistration")
    private cn.ibizlab.odoo.core.odoo_event.domain.Event_registration odooRegistration;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [EMail发送]
     */
    public void setMailSent(String mailSent){
        this.mailSent = mailSent ;
        this.modify("mail_sent",mailSent);
    }
    /**
     * 设置 [定期时间]
     */
    public void setScheduledDate(Timestamp scheduledDate){
        this.scheduledDate = scheduledDate ;
        this.modify("scheduled_date",scheduledDate);
    }
    /**
     * 设置 [出席者]
     */
    public void setRegistrationId(Integer registrationId){
        this.registrationId = registrationId ;
        this.modify("registration_id",registrationId);
    }
    /**
     * 设置 [邮件调度]
     */
    public void setSchedulerId(Integer schedulerId){
        this.schedulerId = schedulerId ;
        this.modify("scheduler_id",schedulerId);
    }

}


