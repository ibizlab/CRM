package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iproduct_style;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_style] 服务对象接口
 */
public interface Iproduct_styleClientService{

    public Iproduct_style createModel() ;

    public void update(Iproduct_style product_style);

    public void createBatch(List<Iproduct_style> product_styles);

    public Page<Iproduct_style> fetchDefault(SearchContext context);

    public void create(Iproduct_style product_style);

    public void remove(Iproduct_style product_style);

    public void removeBatch(List<Iproduct_style> product_styles);

    public void get(Iproduct_style product_style);

    public void updateBatch(List<Iproduct_style> product_styles);

    public Page<Iproduct_style> select(SearchContext context);

    public void getDraft(Iproduct_style product_style);

}
