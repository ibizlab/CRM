package cn.ibizlab.odoo.core.odoo_base_import.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_char_stillreadonly;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_char_stillreadonlySearchContext;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_tests_models_char_stillreadonlyService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base_import.client.base_import_tests_models_char_stillreadonlyFeignClient;

/**
 * 实体[测试:基本导入模型，字符仍然只读] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_import_tests_models_char_stillreadonlyServiceImpl implements IBase_import_tests_models_char_stillreadonlyService {

    @Autowired
    base_import_tests_models_char_stillreadonlyFeignClient base_import_tests_models_char_stillreadonlyFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=base_import_tests_models_char_stillreadonlyFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        base_import_tests_models_char_stillreadonlyFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Base_import_tests_models_char_stillreadonly et) {
        Base_import_tests_models_char_stillreadonly rt = base_import_tests_models_char_stillreadonlyFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_import_tests_models_char_stillreadonly> list){
        base_import_tests_models_char_stillreadonlyFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Base_import_tests_models_char_stillreadonly et) {
        Base_import_tests_models_char_stillreadonly rt = base_import_tests_models_char_stillreadonlyFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Base_import_tests_models_char_stillreadonly> list){
        base_import_tests_models_char_stillreadonlyFeignClient.updateBatch(list) ;
    }

    @Override
    public Base_import_tests_models_char_stillreadonly get(Integer id) {
		Base_import_tests_models_char_stillreadonly et=base_import_tests_models_char_stillreadonlyFeignClient.get(id);
        if(et==null){
            et=new Base_import_tests_models_char_stillreadonly();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Base_import_tests_models_char_stillreadonly getDraft(Base_import_tests_models_char_stillreadonly et) {
        et=base_import_tests_models_char_stillreadonlyFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_import_tests_models_char_stillreadonly> searchDefault(Base_import_tests_models_char_stillreadonlySearchContext context) {
        Page<Base_import_tests_models_char_stillreadonly> base_import_tests_models_char_stillreadonlys=base_import_tests_models_char_stillreadonlyFeignClient.searchDefault(context);
        return base_import_tests_models_char_stillreadonlys;
    }


}


