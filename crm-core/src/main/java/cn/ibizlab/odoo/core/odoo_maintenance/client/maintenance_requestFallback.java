package cn.ibizlab.odoo.core.odoo_maintenance.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_request;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_requestSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[maintenance_request] 服务对象接口
 */
@Component
public class maintenance_requestFallback implements maintenance_requestFeignClient{


    public Maintenance_request create(Maintenance_request maintenance_request){
            return null;
     }
    public Boolean createBatch(List<Maintenance_request> maintenance_requests){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Maintenance_request get(Integer id){
            return null;
     }


    public Page<Maintenance_request> searchDefault(Maintenance_requestSearchContext context){
            return null;
     }


    public Maintenance_request update(Integer id, Maintenance_request maintenance_request){
            return null;
     }
    public Boolean updateBatch(List<Maintenance_request> maintenance_requests){
            return false;
     }



    public Page<Maintenance_request> select(){
            return null;
     }

    public Maintenance_request getDraft(){
            return null;
    }



}
