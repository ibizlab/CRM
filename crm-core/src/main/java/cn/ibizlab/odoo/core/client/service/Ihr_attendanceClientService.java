package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ihr_attendance;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_attendance] 服务对象接口
 */
public interface Ihr_attendanceClientService{

    public Ihr_attendance createModel() ;

    public void update(Ihr_attendance hr_attendance);

    public void createBatch(List<Ihr_attendance> hr_attendances);

    public void create(Ihr_attendance hr_attendance);

    public void remove(Ihr_attendance hr_attendance);

    public void updateBatch(List<Ihr_attendance> hr_attendances);

    public void removeBatch(List<Ihr_attendance> hr_attendances);

    public void get(Ihr_attendance hr_attendance);

    public Page<Ihr_attendance> fetchDefault(SearchContext context);

    public Page<Ihr_attendance> select(SearchContext context);

    public void getDraft(Ihr_attendance hr_attendance);

}
