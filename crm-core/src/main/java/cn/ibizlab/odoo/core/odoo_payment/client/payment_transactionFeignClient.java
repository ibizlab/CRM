package cn.ibizlab.odoo.core.odoo_payment.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_transaction;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_transactionSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[payment_transaction] 服务对象接口
 */
@FeignClient(value = "odoo-payment", contextId = "payment-transaction", fallback = payment_transactionFallback.class)
public interface payment_transactionFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/payment_transactions")
    Payment_transaction create(@RequestBody Payment_transaction payment_transaction);

    @RequestMapping(method = RequestMethod.POST, value = "/payment_transactions/batch")
    Boolean createBatch(@RequestBody List<Payment_transaction> payment_transactions);



    @RequestMapping(method = RequestMethod.POST, value = "/payment_transactions/searchdefault")
    Page<Payment_transaction> searchDefault(@RequestBody Payment_transactionSearchContext context);




    @RequestMapping(method = RequestMethod.GET, value = "/payment_transactions/{id}")
    Payment_transaction get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/payment_transactions/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/payment_transactions/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/payment_transactions/{id}")
    Payment_transaction update(@PathVariable("id") Integer id,@RequestBody Payment_transaction payment_transaction);

    @RequestMapping(method = RequestMethod.PUT, value = "/payment_transactions/batch")
    Boolean updateBatch(@RequestBody List<Payment_transaction> payment_transactions);


    @RequestMapping(method = RequestMethod.GET, value = "/payment_transactions/select")
    Page<Payment_transaction> select();


    @RequestMapping(method = RequestMethod.GET, value = "/payment_transactions/getdraft")
    Payment_transaction getDraft();


}
