package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailingSearchContext;


/**
 * 实体[Mail_mass_mailing] 服务对象接口
 */
public interface IMail_mass_mailingService{

    boolean update(Mail_mass_mailing et) ;
    void updateBatch(List<Mail_mass_mailing> list) ;
    boolean create(Mail_mass_mailing et) ;
    void createBatch(List<Mail_mass_mailing> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Mail_mass_mailing get(Integer key) ;
    Mail_mass_mailing getDraft(Mail_mass_mailing et) ;
    Page<Mail_mass_mailing> searchDefault(Mail_mass_mailingSearchContext context) ;

}



