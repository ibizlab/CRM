package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_activitySearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_activityService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_activityFeignClient;

/**
 * 实体[活动] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_activityServiceImpl implements IMail_activityService {

    @Autowired
    mail_activityFeignClient mail_activityFeignClient;


    @Override
    public Mail_activity getDraft(Mail_activity et) {
        et=mail_activityFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Mail_activity et) {
        Mail_activity rt = mail_activityFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_activity> list){
        mail_activityFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean checkKey(Mail_activity et) {
        return mail_activityFeignClient.checkKey(et);
    }
    @Override
    @Transactional
    public boolean save(Mail_activity et) {
        if(et.getId()==null) et.setId((Integer)et.getDefaultKey(true));
        if(!mail_activityFeignClient.save(et))
            return false;
        return true;
    }

    @Override
    public void saveBatch(List<Mail_activity> list) {
        mail_activityFeignClient.saveBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mail_activityFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_activityFeignClient.removeBatch(idList);
    }

    @Override
    public Mail_activity get(Integer id) {
		Mail_activity et=mail_activityFeignClient.get(id);
        if(et==null){
            et=new Mail_activity();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Mail_activity et) {
        Mail_activity rt = mail_activityFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_activity> list){
        mail_activityFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_activity> searchDefault(Mail_activitySearchContext context) {
        Page<Mail_activity> mail_activitys=mail_activityFeignClient.searchDefault(context);
        return mail_activitys;
    }


}


