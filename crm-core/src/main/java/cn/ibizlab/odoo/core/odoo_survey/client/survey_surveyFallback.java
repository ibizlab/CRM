package cn.ibizlab.odoo.core.odoo_survey.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_survey;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_surveySearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[survey_survey] 服务对象接口
 */
@Component
public class survey_surveyFallback implements survey_surveyFeignClient{

    public Survey_survey create(Survey_survey survey_survey){
            return null;
     }
    public Boolean createBatch(List<Survey_survey> survey_surveys){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Survey_survey update(Integer id, Survey_survey survey_survey){
            return null;
     }
    public Boolean updateBatch(List<Survey_survey> survey_surveys){
            return false;
     }




    public Survey_survey get(Integer id){
            return null;
     }


    public Page<Survey_survey> searchDefault(Survey_surveySearchContext context){
            return null;
     }


    public Page<Survey_survey> select(){
            return null;
     }

    public Survey_survey getDraft(){
            return null;
    }



}
