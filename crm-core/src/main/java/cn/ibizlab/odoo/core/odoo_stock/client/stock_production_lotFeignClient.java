package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_production_lot;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_production_lotSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_production_lot] 服务对象接口
 */
@FeignClient(value = "odoo-stock", contextId = "stock-production-lot", fallback = stock_production_lotFallback.class)
public interface stock_production_lotFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_production_lots/{id}")
    Stock_production_lot update(@PathVariable("id") Integer id,@RequestBody Stock_production_lot stock_production_lot);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_production_lots/batch")
    Boolean updateBatch(@RequestBody List<Stock_production_lot> stock_production_lots);





    @RequestMapping(method = RequestMethod.POST, value = "/stock_production_lots/searchdefault")
    Page<Stock_production_lot> searchDefault(@RequestBody Stock_production_lotSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_production_lots/{id}")
    Stock_production_lot get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_production_lots")
    Stock_production_lot create(@RequestBody Stock_production_lot stock_production_lot);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_production_lots/batch")
    Boolean createBatch(@RequestBody List<Stock_production_lot> stock_production_lots);


    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_production_lots/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_production_lots/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_production_lots/select")
    Page<Stock_production_lot> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_production_lots/getdraft")
    Stock_production_lot getDraft();


}
