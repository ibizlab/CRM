package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity_type;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_activity_typeSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_activity_typeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_activity_typeFeignClient;

/**
 * 实体[活动类型] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_activity_typeServiceImpl implements IMail_activity_typeService {

    @Autowired
    mail_activity_typeFeignClient mail_activity_typeFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=mail_activity_typeFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_activity_typeFeignClient.removeBatch(idList);
    }

    @Override
    public Mail_activity_type get(Integer id) {
		Mail_activity_type et=mail_activity_typeFeignClient.get(id);
        if(et==null){
            et=new Mail_activity_type();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Mail_activity_type getDraft(Mail_activity_type et) {
        et=mail_activity_typeFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean checkKey(Mail_activity_type et) {
        return mail_activity_typeFeignClient.checkKey(et);
    }
    @Override
    public boolean create(Mail_activity_type et) {
        Mail_activity_type rt = mail_activity_typeFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_activity_type> list){
        mail_activity_typeFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Mail_activity_type et) {
        Mail_activity_type rt = mail_activity_typeFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_activity_type> list){
        mail_activity_typeFeignClient.updateBatch(list) ;
    }

    @Override
    @Transactional
    public boolean save(Mail_activity_type et) {
        if(et.getId()==null) et.setId((Integer)et.getDefaultKey(true));
        if(!mail_activity_typeFeignClient.save(et))
            return false;
        return true;
    }

    @Override
    public void saveBatch(List<Mail_activity_type> list) {
        mail_activity_typeFeignClient.saveBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_activity_type> searchDefault(Mail_activity_typeSearchContext context) {
        Page<Mail_activity_type> mail_activity_types=mail_activity_typeFeignClient.searchDefault(context);
        return mail_activity_types;
    }


}


