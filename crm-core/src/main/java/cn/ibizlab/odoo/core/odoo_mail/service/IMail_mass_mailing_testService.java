package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_test;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_testSearchContext;


/**
 * 实体[Mail_mass_mailing_test] 服务对象接口
 */
public interface IMail_mass_mailing_testService{

    boolean update(Mail_mass_mailing_test et) ;
    void updateBatch(List<Mail_mass_mailing_test> list) ;
    boolean create(Mail_mass_mailing_test et) ;
    void createBatch(List<Mail_mass_mailing_test> list) ;
    Mail_mass_mailing_test getDraft(Mail_mass_mailing_test et) ;
    Mail_mass_mailing_test get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Mail_mass_mailing_test> searchDefault(Mail_mass_mailing_testSearchContext context) ;

}



