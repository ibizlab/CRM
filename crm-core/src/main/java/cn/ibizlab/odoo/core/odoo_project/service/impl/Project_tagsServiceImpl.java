package cn.ibizlab.odoo.core.odoo_project.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_project.domain.Project_tags;
import cn.ibizlab.odoo.core.odoo_project.filter.Project_tagsSearchContext;
import cn.ibizlab.odoo.core.odoo_project.service.IProject_tagsService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_project.client.project_tagsFeignClient;

/**
 * 实体[项目标签] 服务对象接口实现
 */
@Slf4j
@Service
public class Project_tagsServiceImpl implements IProject_tagsService {

    @Autowired
    project_tagsFeignClient project_tagsFeignClient;


    @Override
    public Project_tags get(Integer id) {
		Project_tags et=project_tagsFeignClient.get(id);
        if(et==null){
            et=new Project_tags();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=project_tagsFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        project_tagsFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Project_tags et) {
        Project_tags rt = project_tagsFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Project_tags> list){
        project_tagsFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Project_tags et) {
        Project_tags rt = project_tagsFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Project_tags> list){
        project_tagsFeignClient.updateBatch(list) ;
    }

    @Override
    public Project_tags getDraft(Project_tags et) {
        et=project_tagsFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Project_tags> searchDefault(Project_tagsSearchContext context) {
        Page<Project_tags> project_tagss=project_tagsFeignClient.searchDefault(context);
        return project_tagss;
    }


}


