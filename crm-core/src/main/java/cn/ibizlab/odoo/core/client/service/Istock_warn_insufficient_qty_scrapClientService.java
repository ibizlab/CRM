package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_warn_insufficient_qty_scrap;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_warn_insufficient_qty_scrap] 服务对象接口
 */
public interface Istock_warn_insufficient_qty_scrapClientService{

    public Istock_warn_insufficient_qty_scrap createModel() ;

    public Page<Istock_warn_insufficient_qty_scrap> fetchDefault(SearchContext context);

    public void updateBatch(List<Istock_warn_insufficient_qty_scrap> stock_warn_insufficient_qty_scraps);

    public void create(Istock_warn_insufficient_qty_scrap stock_warn_insufficient_qty_scrap);

    public void createBatch(List<Istock_warn_insufficient_qty_scrap> stock_warn_insufficient_qty_scraps);

    public void remove(Istock_warn_insufficient_qty_scrap stock_warn_insufficient_qty_scrap);

    public void removeBatch(List<Istock_warn_insufficient_qty_scrap> stock_warn_insufficient_qty_scraps);

    public void update(Istock_warn_insufficient_qty_scrap stock_warn_insufficient_qty_scrap);

    public void get(Istock_warn_insufficient_qty_scrap stock_warn_insufficient_qty_scrap);

    public Page<Istock_warn_insufficient_qty_scrap> select(SearchContext context);

    public void getDraft(Istock_warn_insufficient_qty_scrap stock_warn_insufficient_qty_scrap);

}
