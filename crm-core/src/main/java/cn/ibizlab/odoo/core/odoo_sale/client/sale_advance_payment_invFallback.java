package cn.ibizlab.odoo.core.odoo_sale.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_advance_payment_inv;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_advance_payment_invSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[sale_advance_payment_inv] 服务对象接口
 */
@Component
public class sale_advance_payment_invFallback implements sale_advance_payment_invFeignClient{

    public Sale_advance_payment_inv create(Sale_advance_payment_inv sale_advance_payment_inv){
            return null;
     }
    public Boolean createBatch(List<Sale_advance_payment_inv> sale_advance_payment_invs){
            return false;
     }


    public Page<Sale_advance_payment_inv> searchDefault(Sale_advance_payment_invSearchContext context){
            return null;
     }



    public Sale_advance_payment_inv get(Integer id){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Sale_advance_payment_inv update(Integer id, Sale_advance_payment_inv sale_advance_payment_inv){
            return null;
     }
    public Boolean updateBatch(List<Sale_advance_payment_inv> sale_advance_payment_invs){
            return false;
     }



    public Page<Sale_advance_payment_inv> select(){
            return null;
     }

    public Sale_advance_payment_inv getDraft(){
            return null;
    }



}
