package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_import_wizard;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_import_wizardSearchContext;


/**
 * 实体[Account_invoice_import_wizard] 服务对象接口
 */
public interface IAccount_invoice_import_wizardService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Account_invoice_import_wizard et) ;
    void updateBatch(List<Account_invoice_import_wizard> list) ;
    Account_invoice_import_wizard get(Integer key) ;
    Account_invoice_import_wizard getDraft(Account_invoice_import_wizard et) ;
    boolean create(Account_invoice_import_wizard et) ;
    void createBatch(List<Account_invoice_import_wizard> list) ;
    Page<Account_invoice_import_wizard> searchDefault(Account_invoice_import_wizardSearchContext context) ;

}



