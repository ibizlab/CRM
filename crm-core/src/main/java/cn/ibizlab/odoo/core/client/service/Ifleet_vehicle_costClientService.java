package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_cost;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[fleet_vehicle_cost] 服务对象接口
 */
public interface Ifleet_vehicle_costClientService{

    public Ifleet_vehicle_cost createModel() ;

    public Page<Ifleet_vehicle_cost> fetchDefault(SearchContext context);

    public void createBatch(List<Ifleet_vehicle_cost> fleet_vehicle_costs);

    public void create(Ifleet_vehicle_cost fleet_vehicle_cost);

    public void remove(Ifleet_vehicle_cost fleet_vehicle_cost);

    public void update(Ifleet_vehicle_cost fleet_vehicle_cost);

    public void get(Ifleet_vehicle_cost fleet_vehicle_cost);

    public void removeBatch(List<Ifleet_vehicle_cost> fleet_vehicle_costs);

    public void updateBatch(List<Ifleet_vehicle_cost> fleet_vehicle_costs);

    public Page<Ifleet_vehicle_cost> select(SearchContext context);

    public void getDraft(Ifleet_vehicle_cost fleet_vehicle_cost);

}
