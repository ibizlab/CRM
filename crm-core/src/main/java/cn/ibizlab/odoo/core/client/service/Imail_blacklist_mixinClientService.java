package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_blacklist_mixin;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_blacklist_mixin] 服务对象接口
 */
public interface Imail_blacklist_mixinClientService{

    public Imail_blacklist_mixin createModel() ;

    public void create(Imail_blacklist_mixin mail_blacklist_mixin);

    public Page<Imail_blacklist_mixin> fetchDefault(SearchContext context);

    public void createBatch(List<Imail_blacklist_mixin> mail_blacklist_mixins);

    public void updateBatch(List<Imail_blacklist_mixin> mail_blacklist_mixins);

    public void remove(Imail_blacklist_mixin mail_blacklist_mixin);

    public void removeBatch(List<Imail_blacklist_mixin> mail_blacklist_mixins);

    public void update(Imail_blacklist_mixin mail_blacklist_mixin);

    public void get(Imail_blacklist_mixin mail_blacklist_mixin);

    public Page<Imail_blacklist_mixin> select(SearchContext context);

    public void getDraft(Imail_blacklist_mixin mail_blacklist_mixin);

}
