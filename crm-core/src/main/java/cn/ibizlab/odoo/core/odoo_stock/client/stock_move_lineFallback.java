package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_move_line;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_move_lineSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_move_line] 服务对象接口
 */
@Component
public class stock_move_lineFallback implements stock_move_lineFeignClient{


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Stock_move_line create(Stock_move_line stock_move_line){
            return null;
     }
    public Boolean createBatch(List<Stock_move_line> stock_move_lines){
            return false;
     }


    public Page<Stock_move_line> searchDefault(Stock_move_lineSearchContext context){
            return null;
     }



    public Stock_move_line get(Integer id){
            return null;
     }


    public Stock_move_line update(Integer id, Stock_move_line stock_move_line){
            return null;
     }
    public Boolean updateBatch(List<Stock_move_line> stock_move_lines){
            return false;
     }


    public Page<Stock_move_line> select(){
            return null;
     }

    public Stock_move_line getDraft(){
            return null;
    }



}
