package cn.ibizlab.odoo.core.odoo_sale.valuerule.anno.sale_payment_acquirer_onboarding_wizard;

import cn.ibizlab.odoo.core.odoo_sale.valuerule.validator.sale_payment_acquirer_onboarding_wizard.Sale_payment_acquirer_onboarding_wizardPaypal_pdt_tokenDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Sale_payment_acquirer_onboarding_wizard
 * 属性：Paypal_pdt_token
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Sale_payment_acquirer_onboarding_wizardPaypal_pdt_tokenDefaultValidator.class})
public @interface Sale_payment_acquirer_onboarding_wizardPaypal_pdt_tokenDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[100]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
