package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Isale_advance_payment_inv;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[sale_advance_payment_inv] 服务对象接口
 */
public interface Isale_advance_payment_invClientService{

    public Isale_advance_payment_inv createModel() ;

    public void create(Isale_advance_payment_inv sale_advance_payment_inv);

    public void removeBatch(List<Isale_advance_payment_inv> sale_advance_payment_invs);

    public Page<Isale_advance_payment_inv> fetchDefault(SearchContext context);

    public void updateBatch(List<Isale_advance_payment_inv> sale_advance_payment_invs);

    public void get(Isale_advance_payment_inv sale_advance_payment_inv);

    public void remove(Isale_advance_payment_inv sale_advance_payment_inv);

    public void update(Isale_advance_payment_inv sale_advance_payment_inv);

    public void createBatch(List<Isale_advance_payment_inv> sale_advance_payment_invs);

    public Page<Isale_advance_payment_inv> select(SearchContext context);

    public void getDraft(Isale_advance_payment_inv sale_advance_payment_inv);

}
