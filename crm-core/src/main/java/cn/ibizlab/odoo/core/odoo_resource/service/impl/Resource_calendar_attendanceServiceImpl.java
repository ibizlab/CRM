package cn.ibizlab.odoo.core.odoo_resource.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar_attendance;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_calendar_attendanceSearchContext;
import cn.ibizlab.odoo.core.odoo_resource.service.IResource_calendar_attendanceService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_resource.client.resource_calendar_attendanceFeignClient;

/**
 * 实体[工作细节] 服务对象接口实现
 */
@Slf4j
@Service
public class Resource_calendar_attendanceServiceImpl implements IResource_calendar_attendanceService {

    @Autowired
    resource_calendar_attendanceFeignClient resource_calendar_attendanceFeignClient;


    @Override
    public Resource_calendar_attendance getDraft(Resource_calendar_attendance et) {
        et=resource_calendar_attendanceFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=resource_calendar_attendanceFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        resource_calendar_attendanceFeignClient.removeBatch(idList);
    }

    @Override
    public Resource_calendar_attendance get(Integer id) {
		Resource_calendar_attendance et=resource_calendar_attendanceFeignClient.get(id);
        if(et==null){
            et=new Resource_calendar_attendance();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Resource_calendar_attendance et) {
        Resource_calendar_attendance rt = resource_calendar_attendanceFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Resource_calendar_attendance> list){
        resource_calendar_attendanceFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Resource_calendar_attendance et) {
        Resource_calendar_attendance rt = resource_calendar_attendanceFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Resource_calendar_attendance> list){
        resource_calendar_attendanceFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Resource_calendar_attendance> searchDefault(Resource_calendar_attendanceSearchContext context) {
        Page<Resource_calendar_attendance> resource_calendar_attendances=resource_calendar_attendanceFeignClient.searchDefault(context);
        return resource_calendar_attendances;
    }


}


