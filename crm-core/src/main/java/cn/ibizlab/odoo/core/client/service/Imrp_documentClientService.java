package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imrp_document;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mrp_document] 服务对象接口
 */
public interface Imrp_documentClientService{

    public Imrp_document createModel() ;

    public Page<Imrp_document> fetchDefault(SearchContext context);

    public void removeBatch(List<Imrp_document> mrp_documents);

    public void create(Imrp_document mrp_document);

    public void updateBatch(List<Imrp_document> mrp_documents);

    public void createBatch(List<Imrp_document> mrp_documents);

    public void get(Imrp_document mrp_document);

    public void remove(Imrp_document mrp_document);

    public void update(Imrp_document mrp_document);

    public Page<Imrp_document> select(SearchContext context);

    public void getDraft(Imrp_document mrp_document);

}
