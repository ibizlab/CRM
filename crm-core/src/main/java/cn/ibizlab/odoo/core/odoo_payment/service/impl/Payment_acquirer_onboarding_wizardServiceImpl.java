package cn.ibizlab.odoo.core.odoo_payment.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_acquirer_onboarding_wizardSearchContext;
import cn.ibizlab.odoo.core.odoo_payment.service.IPayment_acquirer_onboarding_wizardService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_payment.client.payment_acquirer_onboarding_wizardFeignClient;

/**
 * 实体[付款获得onboarding向导] 服务对象接口实现
 */
@Slf4j
@Service
public class Payment_acquirer_onboarding_wizardServiceImpl implements IPayment_acquirer_onboarding_wizardService {

    @Autowired
    payment_acquirer_onboarding_wizardFeignClient payment_acquirer_onboarding_wizardFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=payment_acquirer_onboarding_wizardFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        payment_acquirer_onboarding_wizardFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Payment_acquirer_onboarding_wizard et) {
        Payment_acquirer_onboarding_wizard rt = payment_acquirer_onboarding_wizardFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Payment_acquirer_onboarding_wizard> list){
        payment_acquirer_onboarding_wizardFeignClient.createBatch(list) ;
    }

    @Override
    public Payment_acquirer_onboarding_wizard get(Integer id) {
		Payment_acquirer_onboarding_wizard et=payment_acquirer_onboarding_wizardFeignClient.get(id);
        if(et==null){
            et=new Payment_acquirer_onboarding_wizard();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Payment_acquirer_onboarding_wizard getDraft(Payment_acquirer_onboarding_wizard et) {
        et=payment_acquirer_onboarding_wizardFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Payment_acquirer_onboarding_wizard et) {
        Payment_acquirer_onboarding_wizard rt = payment_acquirer_onboarding_wizardFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Payment_acquirer_onboarding_wizard> list){
        payment_acquirer_onboarding_wizardFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Payment_acquirer_onboarding_wizard> searchDefault(Payment_acquirer_onboarding_wizardSearchContext context) {
        Page<Payment_acquirer_onboarding_wizard> payment_acquirer_onboarding_wizards=payment_acquirer_onboarding_wizardFeignClient.searchDefault(context);
        return payment_acquirer_onboarding_wizards;
    }


}


