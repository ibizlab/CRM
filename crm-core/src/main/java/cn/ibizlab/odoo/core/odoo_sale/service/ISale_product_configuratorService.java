package cn.ibizlab.odoo.core.odoo_sale.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_product_configurator;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_product_configuratorSearchContext;


/**
 * 实体[Sale_product_configurator] 服务对象接口
 */
public interface ISale_product_configuratorService{

    Sale_product_configurator getDraft(Sale_product_configurator et) ;
    boolean create(Sale_product_configurator et) ;
    void createBatch(List<Sale_product_configurator> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Sale_product_configurator get(Integer key) ;
    boolean update(Sale_product_configurator et) ;
    void updateBatch(List<Sale_product_configurator> list) ;
    Page<Sale_product_configurator> searchDefault(Sale_product_configuratorSearchContext context) ;

}



