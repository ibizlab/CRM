package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mail;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mailSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mailService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_mailFeignClient;

/**
 * 实体[寄出邮件] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_mailServiceImpl implements IMail_mailService {

    @Autowired
    mail_mailFeignClient mail_mailFeignClient;


    @Override
    public boolean update(Mail_mail et) {
        Mail_mail rt = mail_mailFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_mail> list){
        mail_mailFeignClient.updateBatch(list) ;
    }

    @Override
    public Mail_mail getDraft(Mail_mail et) {
        et=mail_mailFeignClient.getDraft();
        return et;
    }

    @Override
    public Mail_mail get(Integer id) {
		Mail_mail et=mail_mailFeignClient.get(id);
        if(et==null){
            et=new Mail_mail();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mail_mailFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_mailFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Mail_mail et) {
        Mail_mail rt = mail_mailFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_mail> list){
        mail_mailFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_mail> searchDefault(Mail_mailSearchContext context) {
        Page<Mail_mail> mail_mails=mail_mailFeignClient.searchDefault(context);
        return mail_mails;
    }


}


