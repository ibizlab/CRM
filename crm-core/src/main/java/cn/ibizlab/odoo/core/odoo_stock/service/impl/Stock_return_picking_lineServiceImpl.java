package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_return_picking_line;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_return_picking_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_return_picking_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_return_picking_lineFeignClient;

/**
 * 实体[退料明细行] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_return_picking_lineServiceImpl implements IStock_return_picking_lineService {

    @Autowired
    stock_return_picking_lineFeignClient stock_return_picking_lineFeignClient;


    @Override
    public boolean update(Stock_return_picking_line et) {
        Stock_return_picking_line rt = stock_return_picking_lineFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_return_picking_line> list){
        stock_return_picking_lineFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Stock_return_picking_line et) {
        Stock_return_picking_line rt = stock_return_picking_lineFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_return_picking_line> list){
        stock_return_picking_lineFeignClient.createBatch(list) ;
    }

    @Override
    public Stock_return_picking_line get(Integer id) {
		Stock_return_picking_line et=stock_return_picking_lineFeignClient.get(id);
        if(et==null){
            et=new Stock_return_picking_line();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=stock_return_picking_lineFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_return_picking_lineFeignClient.removeBatch(idList);
    }

    @Override
    public Stock_return_picking_line getDraft(Stock_return_picking_line et) {
        et=stock_return_picking_lineFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_return_picking_line> searchDefault(Stock_return_picking_lineSearchContext context) {
        Page<Stock_return_picking_line> stock_return_picking_lines=stock_return_picking_lineFeignClient.searchDefault(context);
        return stock_return_picking_lines;
    }


}


