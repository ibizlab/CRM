package cn.ibizlab.odoo.core.odoo_sale.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_payment_acquirer_onboarding_wizardSearchContext;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_payment_acquirer_onboarding_wizardService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_sale.client.sale_payment_acquirer_onboarding_wizardFeignClient;

/**
 * 实体[销售付款获得在线向导] 服务对象接口实现
 */
@Slf4j
@Service
public class Sale_payment_acquirer_onboarding_wizardServiceImpl implements ISale_payment_acquirer_onboarding_wizardService {

    @Autowired
    sale_payment_acquirer_onboarding_wizardFeignClient sale_payment_acquirer_onboarding_wizardFeignClient;


    @Override
    public Sale_payment_acquirer_onboarding_wizard get(Integer id) {
		Sale_payment_acquirer_onboarding_wizard et=sale_payment_acquirer_onboarding_wizardFeignClient.get(id);
        if(et==null){
            et=new Sale_payment_acquirer_onboarding_wizard();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=sale_payment_acquirer_onboarding_wizardFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        sale_payment_acquirer_onboarding_wizardFeignClient.removeBatch(idList);
    }

    @Override
    public Sale_payment_acquirer_onboarding_wizard getDraft(Sale_payment_acquirer_onboarding_wizard et) {
        et=sale_payment_acquirer_onboarding_wizardFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Sale_payment_acquirer_onboarding_wizard et) {
        Sale_payment_acquirer_onboarding_wizard rt = sale_payment_acquirer_onboarding_wizardFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Sale_payment_acquirer_onboarding_wizard> list){
        sale_payment_acquirer_onboarding_wizardFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Sale_payment_acquirer_onboarding_wizard et) {
        Sale_payment_acquirer_onboarding_wizard rt = sale_payment_acquirer_onboarding_wizardFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Sale_payment_acquirer_onboarding_wizard> list){
        sale_payment_acquirer_onboarding_wizardFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Sale_payment_acquirer_onboarding_wizard> searchDefault(Sale_payment_acquirer_onboarding_wizardSearchContext context) {
        Page<Sale_payment_acquirer_onboarding_wizard> sale_payment_acquirer_onboarding_wizards=sale_payment_acquirer_onboarding_wizardFeignClient.searchDefault(context);
        return sale_payment_acquirer_onboarding_wizards;
    }


}


