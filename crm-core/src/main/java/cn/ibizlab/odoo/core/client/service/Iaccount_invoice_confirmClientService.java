package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_confirm;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_invoice_confirm] 服务对象接口
 */
public interface Iaccount_invoice_confirmClientService{

    public Iaccount_invoice_confirm createModel() ;

    public Page<Iaccount_invoice_confirm> fetchDefault(SearchContext context);

    public void create(Iaccount_invoice_confirm account_invoice_confirm);

    public void updateBatch(List<Iaccount_invoice_confirm> account_invoice_confirms);

    public void removeBatch(List<Iaccount_invoice_confirm> account_invoice_confirms);

    public void update(Iaccount_invoice_confirm account_invoice_confirm);

    public void createBatch(List<Iaccount_invoice_confirm> account_invoice_confirms);

    public void get(Iaccount_invoice_confirm account_invoice_confirm);

    public void remove(Iaccount_invoice_confirm account_invoice_confirm);

    public Page<Iaccount_invoice_confirm> select(SearchContext context);

    public void getDraft(Iaccount_invoice_confirm account_invoice_confirm);

}
