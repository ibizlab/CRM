package cn.ibizlab.odoo.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_pricelist;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_pricelistSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[product_pricelist] 服务对象接口
 */
@FeignClient(value = "odoo-product", contextId = "product-pricelist", fallback = product_pricelistFallback.class)
public interface product_pricelistFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/product_pricelists/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/product_pricelists/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/product_pricelists/{id}")
    Product_pricelist get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/product_pricelists/searchdefault")
    Page<Product_pricelist> searchDefault(@RequestBody Product_pricelistSearchContext context);





    @RequestMapping(method = RequestMethod.POST, value = "/product_pricelists")
    Product_pricelist create(@RequestBody Product_pricelist product_pricelist);

    @RequestMapping(method = RequestMethod.POST, value = "/product_pricelists/batch")
    Boolean createBatch(@RequestBody List<Product_pricelist> product_pricelists);


    @RequestMapping(method = RequestMethod.PUT, value = "/product_pricelists/{id}")
    Product_pricelist update(@PathVariable("id") Integer id,@RequestBody Product_pricelist product_pricelist);

    @RequestMapping(method = RequestMethod.PUT, value = "/product_pricelists/batch")
    Boolean updateBatch(@RequestBody List<Product_pricelist> product_pricelists);


    @RequestMapping(method = RequestMethod.GET, value = "/product_pricelists/select")
    Page<Product_pricelist> select();


    @RequestMapping(method = RequestMethod.POST, value = "/product_pricelists/save")
    Boolean save(@RequestBody Product_pricelist product_pricelist);

    @RequestMapping(method = RequestMethod.POST, value = "/product_pricelists/save")
    Boolean saveBatch(@RequestBody List<Product_pricelist> product_pricelists);


    @RequestMapping(method = RequestMethod.GET, value = "/product_pricelists/getdraft")
    Product_pricelist getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/product_pricelists/checkkey")
    Boolean checkKey(@RequestBody Product_pricelist product_pricelist);


}
