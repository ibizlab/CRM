package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_mail;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_mail] 服务对象接口
 */
public interface Imail_mailClientService{

    public Imail_mail createModel() ;

    public void removeBatch(List<Imail_mail> mail_mails);

    public void updateBatch(List<Imail_mail> mail_mails);

    public void remove(Imail_mail mail_mail);

    public void update(Imail_mail mail_mail);

    public void createBatch(List<Imail_mail> mail_mails);

    public Page<Imail_mail> fetchDefault(SearchContext context);

    public void get(Imail_mail mail_mail);

    public void create(Imail_mail mail_mail);

    public Page<Imail_mail> select(SearchContext context);

    public void getDraft(Imail_mail mail_mail);

}
