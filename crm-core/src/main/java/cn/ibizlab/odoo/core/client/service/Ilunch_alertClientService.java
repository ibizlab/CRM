package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ilunch_alert;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[lunch_alert] 服务对象接口
 */
public interface Ilunch_alertClientService{

    public Ilunch_alert createModel() ;

    public void removeBatch(List<Ilunch_alert> lunch_alerts);

    public void createBatch(List<Ilunch_alert> lunch_alerts);

    public void remove(Ilunch_alert lunch_alert);

    public void get(Ilunch_alert lunch_alert);

    public Page<Ilunch_alert> fetchDefault(SearchContext context);

    public void updateBatch(List<Ilunch_alert> lunch_alerts);

    public void update(Ilunch_alert lunch_alert);

    public void create(Ilunch_alert lunch_alert);

    public Page<Ilunch_alert> select(SearchContext context);

    public void getDraft(Ilunch_alert lunch_alert);

}
