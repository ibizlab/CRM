package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [mrp_routing_workcenter] 对象
 */
public interface Imrp_routing_workcenter {

    /**
     * 获取 [下一作业]
     */
    public void setBatch(String batch);
    
    /**
     * 设置 [下一作业]
     */
    public String getBatch();

    /**
     * 获取 [下一作业]脏标记
     */
    public boolean getBatchDirtyFlag();
    /**
     * 获取 [待处理的数量]
     */
    public void setBatch_size(Double batch_size);
    
    /**
     * 设置 [待处理的数量]
     */
    public Double getBatch_size();

    /**
     * 获取 [待处理的数量]脏标记
     */
    public boolean getBatch_sizeDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [操作]
     */
    public void setName(String name);
    
    /**
     * 设置 [操作]
     */
    public String getName();

    /**
     * 获取 [操作]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [说明]
     */
    public void setNote(String note);
    
    /**
     * 设置 [说明]
     */
    public String getNote();

    /**
     * 获取 [说明]脏标记
     */
    public boolean getNoteDirtyFlag();
    /**
     * 获取 [父级工艺路线]
     */
    public void setRouting_id(Integer routing_id);
    
    /**
     * 设置 [父级工艺路线]
     */
    public Integer getRouting_id();

    /**
     * 获取 [父级工艺路线]脏标记
     */
    public boolean getRouting_idDirtyFlag();
    /**
     * 获取 [父级工艺路线]
     */
    public void setRouting_id_text(String routing_id_text);
    
    /**
     * 设置 [父级工艺路线]
     */
    public String getRouting_id_text();

    /**
     * 获取 [父级工艺路线]脏标记
     */
    public boolean getRouting_id_textDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [时长]
     */
    public void setTime_cycle(Double time_cycle);
    
    /**
     * 设置 [时长]
     */
    public Double getTime_cycle();

    /**
     * 获取 [时长]脏标记
     */
    public boolean getTime_cycleDirtyFlag();
    /**
     * 获取 [人工时长]
     */
    public void setTime_cycle_manual(Double time_cycle_manual);
    
    /**
     * 设置 [人工时长]
     */
    public Double getTime_cycle_manual();

    /**
     * 获取 [人工时长]脏标记
     */
    public boolean getTime_cycle_manualDirtyFlag();
    /**
     * 获取 [时长计算]
     */
    public void setTime_mode(String time_mode);
    
    /**
     * 设置 [时长计算]
     */
    public String getTime_mode();

    /**
     * 获取 [时长计算]脏标记
     */
    public boolean getTime_modeDirtyFlag();
    /**
     * 获取 [基于]
     */
    public void setTime_mode_batch(Integer time_mode_batch);
    
    /**
     * 设置 [基于]
     */
    public Integer getTime_mode_batch();

    /**
     * 获取 [基于]脏标记
     */
    public boolean getTime_mode_batchDirtyFlag();
    /**
     * 获取 [工作中心]
     */
    public void setWorkcenter_id(Integer workcenter_id);
    
    /**
     * 设置 [工作中心]
     */
    public Integer getWorkcenter_id();

    /**
     * 获取 [工作中心]脏标记
     */
    public boolean getWorkcenter_idDirtyFlag();
    /**
     * 获取 [工作中心]
     */
    public void setWorkcenter_id_text(String workcenter_id_text);
    
    /**
     * 设置 [工作中心]
     */
    public String getWorkcenter_id_text();

    /**
     * 获取 [工作中心]脏标记
     */
    public boolean getWorkcenter_id_textDirtyFlag();
    /**
     * 获取 [# 工单]
     */
    public void setWorkorder_count(Integer workorder_count);
    
    /**
     * 设置 [# 工单]
     */
    public Integer getWorkorder_count();

    /**
     * 获取 [# 工单]脏标记
     */
    public boolean getWorkorder_countDirtyFlag();
    /**
     * 获取 [工单]
     */
    public void setWorkorder_ids(String workorder_ids);
    
    /**
     * 设置 [工单]
     */
    public String getWorkorder_ids();

    /**
     * 获取 [工单]脏标记
     */
    public boolean getWorkorder_idsDirtyFlag();
    /**
     * 获取 [工作记录表]
     */
    public void setWorksheet(byte[] worksheet);
    
    /**
     * 设置 [工作记录表]
     */
    public byte[] getWorksheet();

    /**
     * 获取 [工作记录表]脏标记
     */
    public boolean getWorksheetDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
