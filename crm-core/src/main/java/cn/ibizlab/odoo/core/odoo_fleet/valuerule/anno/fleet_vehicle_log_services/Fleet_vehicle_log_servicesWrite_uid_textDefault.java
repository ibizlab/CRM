package cn.ibizlab.odoo.core.odoo_fleet.valuerule.anno.fleet_vehicle_log_services;

import cn.ibizlab.odoo.core.odoo_fleet.valuerule.validator.fleet_vehicle_log_services.Fleet_vehicle_log_servicesWrite_uid_textDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Fleet_vehicle_log_services
 * 属性：Write_uid_text
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Fleet_vehicle_log_servicesWrite_uid_textDefaultValidator.class})
public @interface Fleet_vehicle_log_servicesWrite_uid_textDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
