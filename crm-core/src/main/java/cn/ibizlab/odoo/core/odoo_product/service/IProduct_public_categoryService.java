package cn.ibizlab.odoo.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_product.domain.Product_public_category;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_public_categorySearchContext;


/**
 * 实体[Product_public_category] 服务对象接口
 */
public interface IProduct_public_categoryService{

    Product_public_category get(Integer key) ;
    Product_public_category getDraft(Product_public_category et) ;
    boolean create(Product_public_category et) ;
    void createBatch(List<Product_public_category> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Product_public_category et) ;
    void updateBatch(List<Product_public_category> list) ;
    Page<Product_public_category> searchDefault(Product_public_categorySearchContext context) ;

}



