package cn.ibizlab.odoo.core.odoo_crm.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lost_reason;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lost_reasonSearchContext;


/**
 * 实体[Crm_lost_reason] 服务对象接口
 */
public interface ICrm_lost_reasonService{

    Crm_lost_reason get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Crm_lost_reason et) ;
    void updateBatch(List<Crm_lost_reason> list) ;
    boolean create(Crm_lost_reason et) ;
    void createBatch(List<Crm_lost_reason> list) ;
    Crm_lost_reason getDraft(Crm_lost_reason et) ;
    Page<Crm_lost_reason> searchDefault(Crm_lost_reasonSearchContext context) ;

}



