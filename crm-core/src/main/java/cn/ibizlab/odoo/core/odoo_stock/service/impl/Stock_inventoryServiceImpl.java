package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_inventory;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_inventorySearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_inventoryService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_inventoryFeignClient;

/**
 * 实体[库存] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_inventoryServiceImpl implements IStock_inventoryService {

    @Autowired
    stock_inventoryFeignClient stock_inventoryFeignClient;


    @Override
    public boolean create(Stock_inventory et) {
        Stock_inventory rt = stock_inventoryFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_inventory> list){
        stock_inventoryFeignClient.createBatch(list) ;
    }

    @Override
    public Stock_inventory getDraft(Stock_inventory et) {
        et=stock_inventoryFeignClient.getDraft();
        return et;
    }

    @Override
    public Stock_inventory get(Integer id) {
		Stock_inventory et=stock_inventoryFeignClient.get(id);
        if(et==null){
            et=new Stock_inventory();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Stock_inventory et) {
        Stock_inventory rt = stock_inventoryFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_inventory> list){
        stock_inventoryFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=stock_inventoryFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_inventoryFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_inventory> searchDefault(Stock_inventorySearchContext context) {
        Page<Stock_inventory> stock_inventorys=stock_inventoryFeignClient.searchDefault(context);
        return stock_inventorys;
    }


}


