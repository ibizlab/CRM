package cn.ibizlab.odoo.core.odoo_survey.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_mail_compose_message;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_mail_compose_messageSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[survey_mail_compose_message] 服务对象接口
 */
@Component
public class survey_mail_compose_messageFallback implements survey_mail_compose_messageFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Survey_mail_compose_message update(Integer id, Survey_mail_compose_message survey_mail_compose_message){
            return null;
     }
    public Boolean updateBatch(List<Survey_mail_compose_message> survey_mail_compose_messages){
            return false;
     }


    public Survey_mail_compose_message get(Integer id){
            return null;
     }




    public Survey_mail_compose_message create(Survey_mail_compose_message survey_mail_compose_message){
            return null;
     }
    public Boolean createBatch(List<Survey_mail_compose_message> survey_mail_compose_messages){
            return false;
     }

    public Page<Survey_mail_compose_message> searchDefault(Survey_mail_compose_messageSearchContext context){
            return null;
     }


    public Page<Survey_mail_compose_message> select(){
            return null;
     }

    public Survey_mail_compose_message getDraft(){
            return null;
    }



}
