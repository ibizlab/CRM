package cn.ibizlab.odoo.core.odoo_digest.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_digest.domain.Digest_digest;
import cn.ibizlab.odoo.core.odoo_digest.filter.Digest_digestSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[digest_digest] 服务对象接口
 */
@FeignClient(value = "odoo-digest", contextId = "digest-digest", fallback = digest_digestFallback.class)
public interface digest_digestFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/digest_digests")
    Digest_digest create(@RequestBody Digest_digest digest_digest);

    @RequestMapping(method = RequestMethod.POST, value = "/digest_digests/batch")
    Boolean createBatch(@RequestBody List<Digest_digest> digest_digests);


    @RequestMapping(method = RequestMethod.GET, value = "/digest_digests/{id}")
    Digest_digest get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.PUT, value = "/digest_digests/{id}")
    Digest_digest update(@PathVariable("id") Integer id,@RequestBody Digest_digest digest_digest);

    @RequestMapping(method = RequestMethod.PUT, value = "/digest_digests/batch")
    Boolean updateBatch(@RequestBody List<Digest_digest> digest_digests);



    @RequestMapping(method = RequestMethod.POST, value = "/digest_digests/searchdefault")
    Page<Digest_digest> searchDefault(@RequestBody Digest_digestSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/digest_digests/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/digest_digests/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/digest_digests/select")
    Page<Digest_digest> select();


    @RequestMapping(method = RequestMethod.GET, value = "/digest_digests/getdraft")
    Digest_digest getDraft();


}
