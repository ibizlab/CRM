package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_return_picking;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_return_picking] 服务对象接口
 */
public interface Istock_return_pickingClientService{

    public Istock_return_picking createModel() ;

    public void updateBatch(List<Istock_return_picking> stock_return_pickings);

    public void remove(Istock_return_picking stock_return_picking);

    public Page<Istock_return_picking> fetchDefault(SearchContext context);

    public void removeBatch(List<Istock_return_picking> stock_return_pickings);

    public void createBatch(List<Istock_return_picking> stock_return_pickings);

    public void create(Istock_return_picking stock_return_picking);

    public void update(Istock_return_picking stock_return_picking);

    public void get(Istock_return_picking stock_return_picking);

    public Page<Istock_return_picking> select(SearchContext context);

    public void getDraft(Istock_return_picking stock_return_picking);

}
