package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [web_editor_converter_test] 对象
 */
public interface Iweb_editor_converter_test {

    /**
     * 获取 [Binary]
     */
    public void setBinary(byte[] binary);
    
    /**
     * 设置 [Binary]
     */
    public byte[] getBinary();

    /**
     * 获取 [Binary]脏标记
     */
    public boolean getBinaryDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [日期]
     */
    public void setDate(Timestamp date);
    
    /**
     * 设置 [日期]
     */
    public Timestamp getDate();

    /**
     * 获取 [日期]脏标记
     */
    public boolean getDateDirtyFlag();
    /**
     * 获取 [Datetime]
     */
    public void setDatetime(Timestamp datetime);
    
    /**
     * 设置 [Datetime]
     */
    public Timestamp getDatetime();

    /**
     * 获取 [Datetime]脏标记
     */
    public boolean getDatetimeDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [Html]
     */
    public void setHtml(String html);
    
    /**
     * 设置 [Html]
     */
    public String getHtml();

    /**
     * 获取 [Html]脏标记
     */
    public boolean getHtmlDirtyFlag();
    /**
     * 获取 [Char]
     */
    public void setIbizchar(String ibizchar);
    
    /**
     * 设置 [Char]
     */
    public String getIbizchar();

    /**
     * 获取 [Char]脏标记
     */
    public boolean getIbizcharDirtyFlag();
    /**
     * 获取 [浮点数]
     */
    public void setIbizfloat(Double ibizfloat);
    
    /**
     * 设置 [浮点数]
     */
    public Double getIbizfloat();

    /**
     * 获取 [浮点数]脏标记
     */
    public boolean getIbizfloatDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [Integer]
     */
    public void setInteger(Integer integer);
    
    /**
     * 设置 [Integer]
     */
    public Integer getInteger();

    /**
     * 获取 [Integer]脏标记
     */
    public boolean getIntegerDirtyFlag();
    /**
     * 获取 [Many2One]
     */
    public void setMany2one(Integer many2one);
    
    /**
     * 设置 [Many2One]
     */
    public Integer getMany2one();

    /**
     * 获取 [Many2One]脏标记
     */
    public boolean getMany2oneDirtyFlag();
    /**
     * 获取 [Many2One]
     */
    public void setMany2one_text(String many2one_text);
    
    /**
     * 设置 [Many2One]
     */
    public String getMany2one_text();

    /**
     * 获取 [Many2One]脏标记
     */
    public boolean getMany2one_textDirtyFlag();
    /**
     * 获取 [Numeric]
     */
    public void setNumeric(Double numeric);
    
    /**
     * 设置 [Numeric]
     */
    public Double getNumeric();

    /**
     * 获取 [Numeric]脏标记
     */
    public boolean getNumericDirtyFlag();
    /**
     * 获取 [Selection]
     */
    public void setSelection(String selection);
    
    /**
     * 设置 [Selection]
     */
    public String getSelection();

    /**
     * 获取 [Selection]脏标记
     */
    public boolean getSelectionDirtyFlag();
    /**
     * 获取 [Lorsqu'un pancake prend l'avion à destination de Toronto et qu'il fait une escale technique à St Claude, on dit:]
     */
    public void setSelection_str(String selection_str);
    
    /**
     * 设置 [Lorsqu'un pancake prend l'avion à destination de Toronto et qu'il fait une escale technique à St Claude, on dit:]
     */
    public String getSelection_str();

    /**
     * 获取 [Lorsqu'un pancake prend l'avion à destination de Toronto et qu'il fait une escale technique à St Claude, on dit:]脏标记
     */
    public boolean getSelection_strDirtyFlag();
    /**
     * 获取 [Text]
     */
    public void setText(String text);
    
    /**
     * 设置 [Text]
     */
    public String getText();

    /**
     * 获取 [Text]脏标记
     */
    public boolean getTextDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
