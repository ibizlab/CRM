package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_shortcode;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_shortcodeSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_shortcodeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_shortcodeFeignClient;

/**
 * 实体[自动回复] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_shortcodeServiceImpl implements IMail_shortcodeService {

    @Autowired
    mail_shortcodeFeignClient mail_shortcodeFeignClient;


    @Override
    public Mail_shortcode get(Integer id) {
		Mail_shortcode et=mail_shortcodeFeignClient.get(id);
        if(et==null){
            et=new Mail_shortcode();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Mail_shortcode et) {
        Mail_shortcode rt = mail_shortcodeFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_shortcode> list){
        mail_shortcodeFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mail_shortcodeFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_shortcodeFeignClient.removeBatch(idList);
    }

    @Override
    public Mail_shortcode getDraft(Mail_shortcode et) {
        et=mail_shortcodeFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Mail_shortcode et) {
        Mail_shortcode rt = mail_shortcodeFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_shortcode> list){
        mail_shortcodeFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_shortcode> searchDefault(Mail_shortcodeSearchContext context) {
        Page<Mail_shortcode> mail_shortcodes=mail_shortcodeFeignClient.searchDefault(context);
        return mail_shortcodes;
    }


}


