package cn.ibizlab.odoo.core.odoo_repair.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_fee;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_feeSearchContext;


/**
 * 实体[Repair_fee] 服务对象接口
 */
public interface IRepair_feeService{

    boolean create(Repair_fee et) ;
    void createBatch(List<Repair_fee> list) ;
    Repair_fee getDraft(Repair_fee et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Repair_fee get(Integer key) ;
    boolean update(Repair_fee et) ;
    void updateBatch(List<Repair_fee> list) ;
    Page<Repair_fee> searchDefault(Repair_feeSearchContext context) ;

}



