package cn.ibizlab.odoo.core.odoo_maintenance.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_team;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_teamSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[maintenance_team] 服务对象接口
 */
@FeignClient(value = "odoo-maintenance", contextId = "maintenance-team", fallback = maintenance_teamFallback.class)
public interface maintenance_teamFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/maintenance_teams/{id}")
    Maintenance_team get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_teams/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_teams/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/maintenance_teams/searchdefault")
    Page<Maintenance_team> searchDefault(@RequestBody Maintenance_teamSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/maintenance_teams/{id}")
    Maintenance_team update(@PathVariable("id") Integer id,@RequestBody Maintenance_team maintenance_team);

    @RequestMapping(method = RequestMethod.PUT, value = "/maintenance_teams/batch")
    Boolean updateBatch(@RequestBody List<Maintenance_team> maintenance_teams);




    @RequestMapping(method = RequestMethod.POST, value = "/maintenance_teams")
    Maintenance_team create(@RequestBody Maintenance_team maintenance_team);

    @RequestMapping(method = RequestMethod.POST, value = "/maintenance_teams/batch")
    Boolean createBatch(@RequestBody List<Maintenance_team> maintenance_teams);



    @RequestMapping(method = RequestMethod.GET, value = "/maintenance_teams/select")
    Page<Maintenance_team> select();


    @RequestMapping(method = RequestMethod.GET, value = "/maintenance_teams/getdraft")
    Maintenance_team getDraft();


}
