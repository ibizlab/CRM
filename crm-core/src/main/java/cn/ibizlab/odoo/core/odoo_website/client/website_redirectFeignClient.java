package cn.ibizlab.odoo.core.odoo_website.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_redirect;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_redirectSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[website_redirect] 服务对象接口
 */
@FeignClient(value = "odoo-website", contextId = "website-redirect", fallback = website_redirectFallback.class)
public interface website_redirectFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/website_redirects/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/website_redirects/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/website_redirects/{id}")
    Website_redirect update(@PathVariable("id") Integer id,@RequestBody Website_redirect website_redirect);

    @RequestMapping(method = RequestMethod.PUT, value = "/website_redirects/batch")
    Boolean updateBatch(@RequestBody List<Website_redirect> website_redirects);



    @RequestMapping(method = RequestMethod.POST, value = "/website_redirects/searchdefault")
    Page<Website_redirect> searchDefault(@RequestBody Website_redirectSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/website_redirects/{id}")
    Website_redirect get(@PathVariable("id") Integer id);




    @RequestMapping(method = RequestMethod.POST, value = "/website_redirects")
    Website_redirect create(@RequestBody Website_redirect website_redirect);

    @RequestMapping(method = RequestMethod.POST, value = "/website_redirects/batch")
    Boolean createBatch(@RequestBody List<Website_redirect> website_redirects);



    @RequestMapping(method = RequestMethod.GET, value = "/website_redirects/select")
    Page<Website_redirect> select();


    @RequestMapping(method = RequestMethod.GET, value = "/website_redirects/getdraft")
    Website_redirect getDraft();


}
