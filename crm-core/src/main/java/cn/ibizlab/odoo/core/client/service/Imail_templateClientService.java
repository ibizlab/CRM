package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_template;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_template] 服务对象接口
 */
public interface Imail_templateClientService{

    public Imail_template createModel() ;

    public void updateBatch(List<Imail_template> mail_templates);

    public void create(Imail_template mail_template);

    public void createBatch(List<Imail_template> mail_templates);

    public Page<Imail_template> fetchDefault(SearchContext context);

    public void remove(Imail_template mail_template);

    public void update(Imail_template mail_template);

    public void get(Imail_template mail_template);

    public void removeBatch(List<Imail_template> mail_templates);

    public Page<Imail_template> select(SearchContext context);

    public void getDraft(Imail_template mail_template);

}
