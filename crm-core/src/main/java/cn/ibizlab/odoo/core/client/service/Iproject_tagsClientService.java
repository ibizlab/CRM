package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iproject_tags;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[project_tags] 服务对象接口
 */
public interface Iproject_tagsClientService{

    public Iproject_tags createModel() ;

    public Page<Iproject_tags> fetchDefault(SearchContext context);

    public void remove(Iproject_tags project_tags);

    public void update(Iproject_tags project_tags);

    public void removeBatch(List<Iproject_tags> project_tags);

    public void create(Iproject_tags project_tags);

    public void createBatch(List<Iproject_tags> project_tags);

    public void get(Iproject_tags project_tags);

    public void updateBatch(List<Iproject_tags> project_tags);

    public Page<Iproject_tags> select(SearchContext context);

    public void getDraft(Iproject_tags project_tags);

}
