package cn.ibizlab.odoo.core.odoo_base.valuerule.anno.res_partner;

import cn.ibizlab.odoo.core.odoo_base.valuerule.validator.res_partner.Res_partnerMessage_attachment_countDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Res_partner
 * 属性：Message_attachment_count
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Res_partnerMessage_attachment_countDefaultValidator.class})
public @interface Res_partnerMessage_attachment_countDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
