package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_automation_line_test;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_automation_line_testSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[base_automation_line_test] 服务对象接口
 */
@Component
public class base_automation_line_testFallback implements base_automation_line_testFeignClient{


    public Page<Base_automation_line_test> searchDefault(Base_automation_line_testSearchContext context){
            return null;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Base_automation_line_test get(Integer id){
            return null;
     }


    public Base_automation_line_test create(Base_automation_line_test base_automation_line_test){
            return null;
     }
    public Boolean createBatch(List<Base_automation_line_test> base_automation_line_tests){
            return false;
     }

    public Base_automation_line_test update(Integer id, Base_automation_line_test base_automation_line_test){
            return null;
     }
    public Boolean updateBatch(List<Base_automation_line_test> base_automation_line_tests){
            return false;
     }


    public Page<Base_automation_line_test> select(){
            return null;
     }

    public Base_automation_line_test getDraft(){
            return null;
    }



}
