package cn.ibizlab.odoo.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave_type;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leave_typeSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[hr_leave_type] 服务对象接口
 */
@FeignClient(value = "odoo-hr", contextId = "hr-leave-type", fallback = hr_leave_typeFallback.class)
public interface hr_leave_typeFeignClient {


    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_leave_types/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_leave_types/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/hr_leave_types")
    Hr_leave_type create(@RequestBody Hr_leave_type hr_leave_type);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_leave_types/batch")
    Boolean createBatch(@RequestBody List<Hr_leave_type> hr_leave_types);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_leave_types/searchdefault")
    Page<Hr_leave_type> searchDefault(@RequestBody Hr_leave_typeSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/hr_leave_types/{id}")
    Hr_leave_type get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/hr_leave_types/{id}")
    Hr_leave_type update(@PathVariable("id") Integer id,@RequestBody Hr_leave_type hr_leave_type);

    @RequestMapping(method = RequestMethod.PUT, value = "/hr_leave_types/batch")
    Boolean updateBatch(@RequestBody List<Hr_leave_type> hr_leave_types);




    @RequestMapping(method = RequestMethod.GET, value = "/hr_leave_types/select")
    Page<Hr_leave_type> select();


    @RequestMapping(method = RequestMethod.GET, value = "/hr_leave_types/getdraft")
    Hr_leave_type getDraft();


}
