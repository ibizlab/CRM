package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ires_config_installer;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_config_installer] 服务对象接口
 */
public interface Ires_config_installerClientService{

    public Ires_config_installer createModel() ;

    public void updateBatch(List<Ires_config_installer> res_config_installers);

    public void remove(Ires_config_installer res_config_installer);

    public void create(Ires_config_installer res_config_installer);

    public Page<Ires_config_installer> fetchDefault(SearchContext context);

    public void removeBatch(List<Ires_config_installer> res_config_installers);

    public void update(Ires_config_installer res_config_installer);

    public void get(Ires_config_installer res_config_installer);

    public void createBatch(List<Ires_config_installer> res_config_installers);

    public Page<Ires_config_installer> select(SearchContext context);

    public void getDraft(Ires_config_installer res_config_installer);

}
