package cn.ibizlab.odoo.core.odoo_project.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [项目] 对象
 */
@Data
public class Project_project extends EntityClient implements Serializable {

    /**
     * 访问警告
     */
    @JSONField(name = "access_warning")
    @JsonProperty("access_warning")
    private String accessWarning;

    /**
     * 附件数量
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 过期日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 任务活动
     */
    @JSONField(name = "tasks")
    @JsonProperty("tasks")
    private String tasks;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 好 % 任务
     */
    @DEField(name = "percentage_satisfaction_task")
    @JSONField(name = "percentage_satisfaction_task")
    @JsonProperty("percentage_satisfaction_task")
    private Integer percentageSatisfactionTask;

    /**
     * 门户访问网址
     */
    @JSONField(name = "access_url")
    @JsonProperty("access_url")
    private String accessUrl;

    /**
     * 任务阶段
     */
    @JSONField(name = "type_ids")
    @JsonProperty("type_ids")
    private String typeIds;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 安全令牌
     */
    @DEField(name = "access_token")
    @JSONField(name = "access_token")
    @JsonProperty("access_token")
    private String accessToken;

    /**
     * 是关注者
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 隐私
     */
    @DEField(name = "privacy_visibility")
    @JSONField(name = "privacy_visibility")
    @JsonProperty("privacy_visibility")
    private String privacyVisibility;

    /**
     * 关注者
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 点评频率
     */
    @DEField(name = "rating_status_period")
    @JSONField(name = "rating_status_period")
    @JsonProperty("rating_status_period")
    private String ratingStatusPeriod;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 好 % 项目
     */
    @DEField(name = "percentage_satisfaction_project")
    @JSONField(name = "percentage_satisfaction_project")
    @JsonProperty("percentage_satisfaction_project")
    private Integer percentageSatisfactionProject;

    /**
     * 附件数量
     */
    @JSONField(name = "doc_count")
    @JsonProperty("doc_count")
    private Integer docCount;

    /**
     * 会员
     */
    @JSONField(name = "favorite_user_ids")
    @JsonProperty("favorite_user_ids")
    private String favoriteUserIds;

    /**
     * 颜色索引
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 网站信息
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 任务统计
     */
    @JSONField(name = "task_count")
    @JsonProperty("task_count")
    private Integer taskCount;

    /**
     * 在仪表板显示项目
     */
    @JSONField(name = "is_favorite")
    @JsonProperty("is_favorite")
    private String isFavorite;

    /**
     * 公开评级
     */
    @DEField(name = "portal_show_rating")
    @JSONField(name = "portal_show_rating")
    @JsonProperty("portal_show_rating")
    private String portalShowRating;

    /**
     * 评级请求截止日期
     */
    @DEField(name = "rating_request_deadline")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "rating_request_deadline" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("rating_request_deadline")
    private Timestamp ratingRequestDeadline;

    /**
     * 消息
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 行动数量
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 关注者(业务伙伴)
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 关注者(渠道)
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 需要采取行动
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 未读消息计数器
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 序号
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 消息递送错误
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 未读消息
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 客户点评
     */
    @DEField(name = "rating_status")
    @JSONField(name = "rating_status")
    @JsonProperty("rating_status")
    private String ratingStatus;

    /**
     * 用任务来
     */
    @DEField(name = "label_tasks")
    @JSONField(name = "label_tasks")
    @JsonProperty("label_tasks")
    private String labelTasks;

    /**
     * 任务
     */
    @JSONField(name = "task_ids")
    @JsonProperty("task_ids")
    private String taskIds;

    /**
     * 错误数
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 开始日期
     */
    @DEField(name = "date_start")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_start" , format="yyyy-MM-dd")
    @JsonProperty("date_start")
    private Timestamp dateStart;

    /**
     * 客户
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 币种
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 上级记录线程ID
     */
    @JSONField(name = "alias_parent_thread_id")
    @JsonProperty("alias_parent_thread_id")
    private Integer aliasParentThreadId;

    /**
     * 记录线索ID
     */
    @JSONField(name = "alias_force_thread_id")
    @JsonProperty("alias_force_thread_id")
    private Integer aliasForceThreadId;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 别名网域
     */
    @JSONField(name = "alias_domain")
    @JsonProperty("alias_domain")
    private String aliasDomain;

    /**
     * 别名联系人安全
     */
    @JSONField(name = "alias_contact")
    @JsonProperty("alias_contact")
    private String aliasContact;

    /**
     * 上级模型
     */
    @JSONField(name = "alias_parent_model_id")
    @JsonProperty("alias_parent_model_id")
    private Integer aliasParentModelId;

    /**
     * 别名的模型
     */
    @JSONField(name = "alias_model_id")
    @JsonProperty("alias_model_id")
    private Integer aliasModelId;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 所有者
     */
    @JSONField(name = "alias_user_id")
    @JsonProperty("alias_user_id")
    private Integer aliasUserId;

    /**
     * 默认值
     */
    @JSONField(name = "alias_defaults")
    @JsonProperty("alias_defaults")
    private String aliasDefaults;

    /**
     * 工作时间
     */
    @JSONField(name = "resource_calendar_id_text")
    @JsonProperty("resource_calendar_id_text")
    private String resourceCalendarIdText;

    /**
     * 子任务项目
     */
    @JSONField(name = "subtask_project_id_text")
    @JsonProperty("subtask_project_id_text")
    private String subtaskProjectIdText;

    /**
     * 别名
     */
    @JSONField(name = "alias_name")
    @JsonProperty("alias_name")
    private String aliasName;

    /**
     * 项目管理员
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 项目管理员
     */
    @DEField(name = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;

    /**
     * 客户
     */
    @DEField(name = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 子任务项目
     */
    @DEField(name = "subtask_project_id")
    @JSONField(name = "subtask_project_id")
    @JsonProperty("subtask_project_id")
    private Integer subtaskProjectId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 工作时间
     */
    @DEField(name = "resource_calendar_id")
    @JSONField(name = "resource_calendar_id")
    @JsonProperty("resource_calendar_id")
    private Integer resourceCalendarId;

    /**
     * 别名
     */
    @DEField(name = "alias_id")
    @JSONField(name = "alias_id")
    @JsonProperty("alias_id")
    private Integer aliasId;


    /**
     * 
     */
    @JSONField(name = "odooalias")
    @JsonProperty("odooalias")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_alias odooAlias;

    /**
     * 
     */
    @JSONField(name = "odoosubtaskproject")
    @JsonProperty("odoosubtaskproject")
    private cn.ibizlab.odoo.core.odoo_project.domain.Project_project odooSubtaskProject;

    /**
     * 
     */
    @JSONField(name = "odooresourcecalendar")
    @JsonProperty("odooresourcecalendar")
    private cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar odooResourceCalendar;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoopartner")
    @JsonProperty("odoopartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoouser")
    @JsonProperty("odoouser")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [过期日期]
     */
    public void setDate(Timestamp date){
        this.date = date ;
        this.modify("date",date);
    }
    /**
     * 设置 [好 % 任务]
     */
    public void setPercentageSatisfactionTask(Integer percentageSatisfactionTask){
        this.percentageSatisfactionTask = percentageSatisfactionTask ;
        this.modify("percentage_satisfaction_task",percentageSatisfactionTask);
    }
    /**
     * 设置 [安全令牌]
     */
    public void setAccessToken(String accessToken){
        this.accessToken = accessToken ;
        this.modify("access_token",accessToken);
    }
    /**
     * 设置 [隐私]
     */
    public void setPrivacyVisibility(String privacyVisibility){
        this.privacyVisibility = privacyVisibility ;
        this.modify("privacy_visibility",privacyVisibility);
    }
    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [点评频率]
     */
    public void setRatingStatusPeriod(String ratingStatusPeriod){
        this.ratingStatusPeriod = ratingStatusPeriod ;
        this.modify("rating_status_period",ratingStatusPeriod);
    }
    /**
     * 设置 [好 % 项目]
     */
    public void setPercentageSatisfactionProject(Integer percentageSatisfactionProject){
        this.percentageSatisfactionProject = percentageSatisfactionProject ;
        this.modify("percentage_satisfaction_project",percentageSatisfactionProject);
    }
    /**
     * 设置 [颜色索引]
     */
    public void setColor(Integer color){
        this.color = color ;
        this.modify("color",color);
    }
    /**
     * 设置 [公开评级]
     */
    public void setPortalShowRating(String portalShowRating){
        this.portalShowRating = portalShowRating ;
        this.modify("portal_show_rating",portalShowRating);
    }
    /**
     * 设置 [评级请求截止日期]
     */
    public void setRatingRequestDeadline(Timestamp ratingRequestDeadline){
        this.ratingRequestDeadline = ratingRequestDeadline ;
        this.modify("rating_request_deadline",ratingRequestDeadline);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }
    /**
     * 设置 [客户点评]
     */
    public void setRatingStatus(String ratingStatus){
        this.ratingStatus = ratingStatus ;
        this.modify("rating_status",ratingStatus);
    }
    /**
     * 设置 [用任务来]
     */
    public void setLabelTasks(String labelTasks){
        this.labelTasks = labelTasks ;
        this.modify("label_tasks",labelTasks);
    }
    /**
     * 设置 [开始日期]
     */
    public void setDateStart(Timestamp dateStart){
        this.dateStart = dateStart ;
        this.modify("date_start",dateStart);
    }
    /**
     * 设置 [项目管理员]
     */
    public void setUserId(Integer userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }
    /**
     * 设置 [客户]
     */
    public void setPartnerId(Integer partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [子任务项目]
     */
    public void setSubtaskProjectId(Integer subtaskProjectId){
        this.subtaskProjectId = subtaskProjectId ;
        this.modify("subtask_project_id",subtaskProjectId);
    }
    /**
     * 设置 [工作时间]
     */
    public void setResourceCalendarId(Integer resourceCalendarId){
        this.resourceCalendarId = resourceCalendarId ;
        this.modify("resource_calendar_id",resourceCalendarId);
    }
    /**
     * 设置 [别名]
     */
    public void setAliasId(Integer aliasId){
        this.aliasId = aliasId ;
        this.modify("alias_id",aliasId);
    }

}


