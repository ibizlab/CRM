package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_tax;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_tax] 服务对象接口
 */
public interface Iaccount_taxClientService{

    public Iaccount_tax createModel() ;

    public void get(Iaccount_tax account_tax);

    public void create(Iaccount_tax account_tax);

    public void createBatch(List<Iaccount_tax> account_taxes);

    public void update(Iaccount_tax account_tax);

    public void updateBatch(List<Iaccount_tax> account_taxes);

    public Page<Iaccount_tax> fetchDefault(SearchContext context);

    public void removeBatch(List<Iaccount_tax> account_taxes);

    public void remove(Iaccount_tax account_tax);

    public Page<Iaccount_tax> select(SearchContext context);

    public void getDraft(Iaccount_tax account_tax);

}
