package cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_quant_package;

import cn.ibizlab.odoo.core.odoo_stock.valuerule.validator.stock_quant_package.Stock_quant_packageNameDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Stock_quant_package
 * 属性：Name
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Stock_quant_packageNameDefaultValidator.class})
public @interface Stock_quant_packageNameDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[100]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
