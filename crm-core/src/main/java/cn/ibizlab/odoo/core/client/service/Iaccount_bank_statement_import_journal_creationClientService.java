package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_bank_statement_import_journal_creation;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_bank_statement_import_journal_creation] 服务对象接口
 */
public interface Iaccount_bank_statement_import_journal_creationClientService{

    public Iaccount_bank_statement_import_journal_creation createModel() ;

    public void create(Iaccount_bank_statement_import_journal_creation account_bank_statement_import_journal_creation);

    public void get(Iaccount_bank_statement_import_journal_creation account_bank_statement_import_journal_creation);

    public Page<Iaccount_bank_statement_import_journal_creation> fetchDefault(SearchContext context);

    public void updateBatch(List<Iaccount_bank_statement_import_journal_creation> account_bank_statement_import_journal_creations);

    public void removeBatch(List<Iaccount_bank_statement_import_journal_creation> account_bank_statement_import_journal_creations);

    public void createBatch(List<Iaccount_bank_statement_import_journal_creation> account_bank_statement_import_journal_creations);

    public void remove(Iaccount_bank_statement_import_journal_creation account_bank_statement_import_journal_creation);

    public void update(Iaccount_bank_statement_import_journal_creation account_bank_statement_import_journal_creation);

    public Page<Iaccount_bank_statement_import_journal_creation> select(SearchContext context);

    public void getDraft(Iaccount_bank_statement_import_journal_creation account_bank_statement_import_journal_creation);

}
