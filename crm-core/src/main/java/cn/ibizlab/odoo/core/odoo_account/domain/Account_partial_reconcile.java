package cn.ibizlab.odoo.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [部分核销] 对象
 */
@Data
public class Account_partial_reconcile extends EntityClient implements Serializable {

    /**
     * 外币金额
     */
    @DEField(name = "amount_currency")
    @JSONField(name = "amount_currency")
    @JsonProperty("amount_currency")
    private Double amountCurrency;

    /**
     * 金额
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 匹配明细的最大时间
     */
    @DEField(name = "max_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "max_date" , format="yyyy-MM-dd")
    @JsonProperty("max_date")
    private Timestamp maxDate;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 贷方凭证
     */
    @JSONField(name = "credit_move_id_text")
    @JsonProperty("credit_move_id_text")
    private String creditMoveIdText;

    /**
     * 公司货币
     */
    @JSONField(name = "company_currency_id")
    @JsonProperty("company_currency_id")
    private Integer companyCurrencyId;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 借方移动
     */
    @JSONField(name = "debit_move_id_text")
    @JsonProperty("debit_move_id_text")
    private String debitMoveIdText;

    /**
     * 币种
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 完全核销
     */
    @JSONField(name = "full_reconcile_id_text")
    @JsonProperty("full_reconcile_id_text")
    private String fullReconcileIdText;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 完全核销
     */
    @DEField(name = "full_reconcile_id")
    @JSONField(name = "full_reconcile_id")
    @JsonProperty("full_reconcile_id")
    private Integer fullReconcileId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 借方移动
     */
    @DEField(name = "debit_move_id")
    @JSONField(name = "debit_move_id")
    @JsonProperty("debit_move_id")
    private Integer debitMoveId;

    /**
     * 贷方凭证
     */
    @DEField(name = "credit_move_id")
    @JSONField(name = "credit_move_id")
    @JsonProperty("credit_move_id")
    private Integer creditMoveId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odoofullreconcile")
    @JsonProperty("odoofullreconcile")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_full_reconcile odooFullReconcile;

    /**
     * 
     */
    @JSONField(name = "odoocreditmove")
    @JsonProperty("odoocreditmove")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_move_line odooCreditMove;

    /**
     * 
     */
    @JSONField(name = "odoodebitmove")
    @JsonProperty("odoodebitmove")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_move_line odooDebitMove;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoocurrency")
    @JsonProperty("odoocurrency")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [外币金额]
     */
    public void setAmountCurrency(Double amountCurrency){
        this.amountCurrency = amountCurrency ;
        this.modify("amount_currency",amountCurrency);
    }
    /**
     * 设置 [金额]
     */
    public void setAmount(Double amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }
    /**
     * 设置 [匹配明细的最大时间]
     */
    public void setMaxDate(Timestamp maxDate){
        this.maxDate = maxDate ;
        this.modify("max_date",maxDate);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Integer currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }
    /**
     * 设置 [完全核销]
     */
    public void setFullReconcileId(Integer fullReconcileId){
        this.fullReconcileId = fullReconcileId ;
        this.modify("full_reconcile_id",fullReconcileId);
    }
    /**
     * 设置 [借方移动]
     */
    public void setDebitMoveId(Integer debitMoveId){
        this.debitMoveId = debitMoveId ;
        this.modify("debit_move_id",debitMoveId);
    }
    /**
     * 设置 [贷方凭证]
     */
    public void setCreditMoveId(Integer creditMoveId){
        this.creditMoveId = creditMoveId ;
        this.modify("credit_move_id",creditMoveId);
    }

}


