package cn.ibizlab.odoo.core.odoo_rating.valuerule.anno.rating_mixin;

import cn.ibizlab.odoo.core.odoo_rating.valuerule.validator.rating_mixin.Rating_mixinRating_countDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Rating_mixin
 * 属性：Rating_count
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Rating_mixinRating_countDefaultValidator.class})
public @interface Rating_mixinRating_countDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
