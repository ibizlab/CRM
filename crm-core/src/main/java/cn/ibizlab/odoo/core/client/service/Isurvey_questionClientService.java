package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Isurvey_question;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[survey_question] 服务对象接口
 */
public interface Isurvey_questionClientService{

    public Isurvey_question createModel() ;

    public Page<Isurvey_question> fetchDefault(SearchContext context);

    public void remove(Isurvey_question survey_question);

    public void update(Isurvey_question survey_question);

    public void get(Isurvey_question survey_question);

    public void create(Isurvey_question survey_question);

    public void createBatch(List<Isurvey_question> survey_questions);

    public void updateBatch(List<Isurvey_question> survey_questions);

    public void removeBatch(List<Isurvey_question> survey_questions);

    public Page<Isurvey_question> select(SearchContext context);

    public void getDraft(Isurvey_question survey_question);

}
