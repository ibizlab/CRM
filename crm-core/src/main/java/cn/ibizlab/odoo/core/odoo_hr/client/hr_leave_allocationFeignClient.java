package cn.ibizlab.odoo.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave_allocation;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leave_allocationSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[hr_leave_allocation] 服务对象接口
 */
@FeignClient(value = "odoo-hr", contextId = "hr-leave-allocation", fallback = hr_leave_allocationFallback.class)
public interface hr_leave_allocationFeignClient {


    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_leave_allocations/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_leave_allocations/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/hr_leave_allocations/{id}")
    Hr_leave_allocation get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_leave_allocations/searchdefault")
    Page<Hr_leave_allocation> searchDefault(@RequestBody Hr_leave_allocationSearchContext context);



    @RequestMapping(method = RequestMethod.PUT, value = "/hr_leave_allocations/{id}")
    Hr_leave_allocation update(@PathVariable("id") Integer id,@RequestBody Hr_leave_allocation hr_leave_allocation);

    @RequestMapping(method = RequestMethod.PUT, value = "/hr_leave_allocations/batch")
    Boolean updateBatch(@RequestBody List<Hr_leave_allocation> hr_leave_allocations);


    @RequestMapping(method = RequestMethod.POST, value = "/hr_leave_allocations")
    Hr_leave_allocation create(@RequestBody Hr_leave_allocation hr_leave_allocation);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_leave_allocations/batch")
    Boolean createBatch(@RequestBody List<Hr_leave_allocation> hr_leave_allocations);



    @RequestMapping(method = RequestMethod.GET, value = "/hr_leave_allocations/select")
    Page<Hr_leave_allocation> select();


    @RequestMapping(method = RequestMethod.GET, value = "/hr_leave_allocations/getdraft")
    Hr_leave_allocation getDraft();


}
