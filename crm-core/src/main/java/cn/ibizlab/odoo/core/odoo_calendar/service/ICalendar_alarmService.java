package cn.ibizlab.odoo.core.odoo_calendar.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_alarm;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_alarmSearchContext;


/**
 * 实体[Calendar_alarm] 服务对象接口
 */
public interface ICalendar_alarmService{

    Calendar_alarm get(Integer key) ;
    Calendar_alarm getDraft(Calendar_alarm et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Calendar_alarm et) ;
    void createBatch(List<Calendar_alarm> list) ;
    boolean update(Calendar_alarm et) ;
    void updateBatch(List<Calendar_alarm> list) ;
    Page<Calendar_alarm> searchDefault(Calendar_alarmSearchContext context) ;

}



