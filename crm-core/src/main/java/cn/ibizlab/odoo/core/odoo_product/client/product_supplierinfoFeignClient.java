package cn.ibizlab.odoo.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_supplierinfo;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_supplierinfoSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[product_supplierinfo] 服务对象接口
 */
@FeignClient(value = "odoo-product", contextId = "product-supplierinfo", fallback = product_supplierinfoFallback.class)
public interface product_supplierinfoFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/product_supplierinfos/searchdefault")
    Page<Product_supplierinfo> searchDefault(@RequestBody Product_supplierinfoSearchContext context);



    @RequestMapping(method = RequestMethod.PUT, value = "/product_supplierinfos/{id}")
    Product_supplierinfo update(@PathVariable("id") Integer id,@RequestBody Product_supplierinfo product_supplierinfo);

    @RequestMapping(method = RequestMethod.PUT, value = "/product_supplierinfos/batch")
    Boolean updateBatch(@RequestBody List<Product_supplierinfo> product_supplierinfos);


    @RequestMapping(method = RequestMethod.DELETE, value = "/product_supplierinfos/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/product_supplierinfos/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);




    @RequestMapping(method = RequestMethod.POST, value = "/product_supplierinfos")
    Product_supplierinfo create(@RequestBody Product_supplierinfo product_supplierinfo);

    @RequestMapping(method = RequestMethod.POST, value = "/product_supplierinfos/batch")
    Boolean createBatch(@RequestBody List<Product_supplierinfo> product_supplierinfos);


    @RequestMapping(method = RequestMethod.GET, value = "/product_supplierinfos/{id}")
    Product_supplierinfo get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/product_supplierinfos/select")
    Page<Product_supplierinfo> select();


    @RequestMapping(method = RequestMethod.GET, value = "/product_supplierinfos/getdraft")
    Product_supplierinfo getDraft();


}
