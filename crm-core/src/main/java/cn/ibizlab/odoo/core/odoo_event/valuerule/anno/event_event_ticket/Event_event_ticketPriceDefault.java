package cn.ibizlab.odoo.core.odoo_event.valuerule.anno.event_event_ticket;

import cn.ibizlab.odoo.core.odoo_event.valuerule.validator.event_event_ticket.Event_event_ticketPriceDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Event_event_ticket
 * 属性：Price
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Event_event_ticketPriceDefaultValidator.class})
public @interface Event_event_ticketPriceDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
