package cn.ibizlab.odoo.core.odoo_maintenance.valuerule.anno.maintenance_request;

import cn.ibizlab.odoo.core.odoo_maintenance.valuerule.validator.maintenance_request.Maintenance_requestOwner_user_idDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Maintenance_request
 * 属性：Owner_user_id
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Maintenance_requestOwner_user_idDefaultValidator.class})
public @interface Maintenance_requestOwner_user_idDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
