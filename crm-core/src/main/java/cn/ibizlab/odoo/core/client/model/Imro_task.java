package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [mro_task] 对象
 */
public interface Imro_task {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [Asset Category]
     */
    public void setCategory_id(Integer category_id);
    
    /**
     * 设置 [Asset Category]
     */
    public Integer getCategory_id();

    /**
     * 获取 [Asset Category]脏标记
     */
    public boolean getCategory_idDirtyFlag();
    /**
     * 获取 [Asset Category]
     */
    public void setCategory_id_text(String category_id_text);
    
    /**
     * 设置 [Asset Category]
     */
    public String getCategory_id_text();

    /**
     * 获取 [Asset Category]脏标记
     */
    public boolean getCategory_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [Documentation Description]
     */
    public void setDocumentation_description(String documentation_description);
    
    /**
     * 设置 [Documentation Description]
     */
    public String getDocumentation_description();

    /**
     * 获取 [Documentation Description]脏标记
     */
    public boolean getDocumentation_descriptionDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [Labor Description]
     */
    public void setLabor_description(String labor_description);
    
    /**
     * 设置 [Labor Description]
     */
    public String getLabor_description();

    /**
     * 获取 [Labor Description]脏标记
     */
    public boolean getLabor_descriptionDirtyFlag();
    /**
     * 获取 [保养类型]
     */
    public void setMaintenance_type(String maintenance_type);
    
    /**
     * 设置 [保养类型]
     */
    public String getMaintenance_type();

    /**
     * 获取 [保养类型]脏标记
     */
    public boolean getMaintenance_typeDirtyFlag();
    /**
     * 获取 [说明]
     */
    public void setName(String name);
    
    /**
     * 设置 [说明]
     */
    public String getName();

    /**
     * 获取 [说明]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [Operations Description]
     */
    public void setOperations_description(String operations_description);
    
    /**
     * 设置 [Operations Description]
     */
    public String getOperations_description();

    /**
     * 获取 [Operations Description]脏标记
     */
    public boolean getOperations_descriptionDirtyFlag();
    /**
     * 获取 [零件]
     */
    public void setParts_lines(String parts_lines);
    
    /**
     * 设置 [零件]
     */
    public String getParts_lines();

    /**
     * 获取 [零件]脏标记
     */
    public boolean getParts_linesDirtyFlag();
    /**
     * 获取 [Tools Description]
     */
    public void setTools_description(String tools_description);
    
    /**
     * 设置 [Tools Description]
     */
    public String getTools_description();

    /**
     * 获取 [Tools Description]脏标记
     */
    public boolean getTools_descriptionDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
