package cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_picking_type;

import cn.ibizlab.odoo.core.odoo_stock.valuerule.validator.stock_picking_type.Stock_picking_typeCount_picking_readyDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Stock_picking_type
 * 属性：Count_picking_ready
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Stock_picking_typeCount_picking_readyDefaultValidator.class})
public @interface Stock_picking_typeCount_picking_readyDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
