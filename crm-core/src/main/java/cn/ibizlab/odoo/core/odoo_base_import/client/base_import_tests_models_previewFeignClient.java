package cn.ibizlab.odoo.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_preview;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_previewSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_import_tests_models_preview] 服务对象接口
 */
@FeignClient(value = "odoo-base-import", contextId = "base-import-tests-models-preview", fallback = base_import_tests_models_previewFallback.class)
public interface base_import_tests_models_previewFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_previews/searchdefault")
    Page<Base_import_tests_models_preview> searchDefault(@RequestBody Base_import_tests_models_previewSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_previews")
    Base_import_tests_models_preview create(@RequestBody Base_import_tests_models_preview base_import_tests_models_preview);

    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_previews/batch")
    Boolean createBatch(@RequestBody List<Base_import_tests_models_preview> base_import_tests_models_previews);


    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_previews/{id}")
    Base_import_tests_models_preview update(@PathVariable("id") Integer id,@RequestBody Base_import_tests_models_preview base_import_tests_models_preview);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_previews/batch")
    Boolean updateBatch(@RequestBody List<Base_import_tests_models_preview> base_import_tests_models_previews);


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_previews/{id}")
    Base_import_tests_models_preview get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_previews/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_previews/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_previews/select")
    Page<Base_import_tests_models_preview> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_previews/getdraft")
    Base_import_tests_models_preview getDraft();


}
