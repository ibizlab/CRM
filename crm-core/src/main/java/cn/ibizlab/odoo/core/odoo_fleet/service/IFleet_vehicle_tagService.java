package cn.ibizlab.odoo.core.odoo_fleet.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_tag;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_tagSearchContext;


/**
 * 实体[Fleet_vehicle_tag] 服务对象接口
 */
public interface IFleet_vehicle_tagService{

    Fleet_vehicle_tag getDraft(Fleet_vehicle_tag et) ;
    Fleet_vehicle_tag get(Integer key) ;
    boolean update(Fleet_vehicle_tag et) ;
    void updateBatch(List<Fleet_vehicle_tag> list) ;
    boolean create(Fleet_vehicle_tag et) ;
    void createBatch(List<Fleet_vehicle_tag> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Fleet_vehicle_tag> searchDefault(Fleet_vehicle_tagSearchContext context) ;

}



