package cn.ibizlab.odoo.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_convert_order;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_convert_orderSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mro_convert_order] 服务对象接口
 */
@FeignClient(value = "odoo-mro", contextId = "mro-convert-order", fallback = mro_convert_orderFallback.class)
public interface mro_convert_orderFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/mro_convert_orders")
    Mro_convert_order create(@RequestBody Mro_convert_order mro_convert_order);

    @RequestMapping(method = RequestMethod.POST, value = "/mro_convert_orders/batch")
    Boolean createBatch(@RequestBody List<Mro_convert_order> mro_convert_orders);



    @RequestMapping(method = RequestMethod.GET, value = "/mro_convert_orders/{id}")
    Mro_convert_order get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_convert_orders/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_convert_orders/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/mro_convert_orders/searchdefault")
    Page<Mro_convert_order> searchDefault(@RequestBody Mro_convert_orderSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/mro_convert_orders/{id}")
    Mro_convert_order update(@PathVariable("id") Integer id,@RequestBody Mro_convert_order mro_convert_order);

    @RequestMapping(method = RequestMethod.PUT, value = "/mro_convert_orders/batch")
    Boolean updateBatch(@RequestBody List<Mro_convert_order> mro_convert_orders);




    @RequestMapping(method = RequestMethod.GET, value = "/mro_convert_orders/select")
    Page<Mro_convert_order> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mro_convert_orders/getdraft")
    Mro_convert_order getDraft();


}
