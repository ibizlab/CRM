package cn.ibizlab.odoo.core.odoo_lunch.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [工作餐产品] 对象
 */
@Data
public class Lunch_product extends EntityClient implements Serializable {

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 说明
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 可用
     */
    @JSONField(name = "available")
    @JsonProperty("available")
    private String available;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 价格
     */
    @JSONField(name = "price")
    @JsonProperty("price")
    private Double price;

    /**
     * 产品
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 供应商
     */
    @JSONField(name = "supplier_text")
    @JsonProperty("supplier_text")
    private String supplierText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 产品种类
     */
    @JSONField(name = "category_id_text")
    @JsonProperty("category_id_text")
    private String categoryIdText;

    /**
     * 供应商
     */
    @JSONField(name = "supplier")
    @JsonProperty("supplier")
    private Integer supplier;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 产品种类
     */
    @DEField(name = "category_id")
    @JSONField(name = "category_id")
    @JsonProperty("category_id")
    private Integer categoryId;


    /**
     * 
     */
    @JSONField(name = "odoocategory")
    @JsonProperty("odoocategory")
    private cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_product_category odooCategory;

    /**
     * 
     */
    @JSONField(name = "odoosupplier")
    @JsonProperty("odoosupplier")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooSupplier;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }
    /**
     * 设置 [价格]
     */
    public void setPrice(Double price){
        this.price = price ;
        this.modify("price",price);
    }
    /**
     * 设置 [产品]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [供应商]
     */
    public void setSupplier(Integer supplier){
        this.supplier = supplier ;
        this.modify("supplier",supplier);
    }
    /**
     * 设置 [产品种类]
     */
    public void setCategoryId(Integer categoryId){
        this.categoryId = categoryId ;
        this.modify("category_id",categoryId);
    }

}


