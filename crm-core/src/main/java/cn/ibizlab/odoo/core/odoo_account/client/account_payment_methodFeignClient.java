package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_method;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_payment_methodSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_payment_method] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-payment-method", fallback = account_payment_methodFallback.class)
public interface account_payment_methodFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/account_payment_methods/{id}")
    Account_payment_method get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.DELETE, value = "/account_payment_methods/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_payment_methods/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/account_payment_methods")
    Account_payment_method create(@RequestBody Account_payment_method account_payment_method);

    @RequestMapping(method = RequestMethod.POST, value = "/account_payment_methods/batch")
    Boolean createBatch(@RequestBody List<Account_payment_method> account_payment_methods);




    @RequestMapping(method = RequestMethod.POST, value = "/account_payment_methods/searchdefault")
    Page<Account_payment_method> searchDefault(@RequestBody Account_payment_methodSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_payment_methods/{id}")
    Account_payment_method update(@PathVariable("id") Integer id,@RequestBody Account_payment_method account_payment_method);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_payment_methods/batch")
    Boolean updateBatch(@RequestBody List<Account_payment_method> account_payment_methods);



    @RequestMapping(method = RequestMethod.GET, value = "/account_payment_methods/select")
    Page<Account_payment_method> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_payment_methods/getdraft")
    Account_payment_method getDraft();


}
