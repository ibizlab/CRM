package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Res_config_installer;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_config_installerSearchContext;


/**
 * 实体[Res_config_installer] 服务对象接口
 */
public interface IRes_config_installerService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Res_config_installer et) ;
    void createBatch(List<Res_config_installer> list) ;
    boolean update(Res_config_installer et) ;
    void updateBatch(List<Res_config_installer> list) ;
    Res_config_installer getDraft(Res_config_installer et) ;
    Res_config_installer get(Integer key) ;
    Page<Res_config_installer> searchDefault(Res_config_installerSearchContext context) ;

}



