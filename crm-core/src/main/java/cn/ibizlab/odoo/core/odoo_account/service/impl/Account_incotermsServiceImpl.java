package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_incoterms;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_incotermsSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_incotermsService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_incotermsFeignClient;

/**
 * 实体[贸易条款] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_incotermsServiceImpl implements IAccount_incotermsService {

    @Autowired
    account_incotermsFeignClient account_incotermsFeignClient;


    @Override
    public Account_incoterms getDraft(Account_incoterms et) {
        et=account_incotermsFeignClient.getDraft();
        return et;
    }

    @Override
    public Account_incoterms get(Integer id) {
		Account_incoterms et=account_incotermsFeignClient.get(id);
        if(et==null){
            et=new Account_incoterms();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Account_incoterms et) {
        Account_incoterms rt = account_incotermsFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_incoterms> list){
        account_incotermsFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_incotermsFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_incotermsFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Account_incoterms et) {
        Account_incoterms rt = account_incotermsFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_incoterms> list){
        account_incotermsFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_incoterms> searchDefault(Account_incotermsSearchContext context) {
        Page<Account_incoterms> account_incotermss=account_incotermsFeignClient.searchDefault(context);
        return account_incotermss;
    }


}


