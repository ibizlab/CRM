package cn.ibizlab.odoo.core.odoo_sale.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_option;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_order_optionSearchContext;


/**
 * 实体[Sale_order_option] 服务对象接口
 */
public interface ISale_order_optionService{

    Sale_order_option getDraft(Sale_order_option et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Sale_order_option get(Integer key) ;
    boolean create(Sale_order_option et) ;
    void createBatch(List<Sale_order_option> list) ;
    boolean update(Sale_order_option et) ;
    void updateBatch(List<Sale_order_option> list) ;
    Page<Sale_order_option> searchDefault(Sale_order_optionSearchContext context) ;

}



