package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_module_upgrade;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_module_upgradeSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_module_upgradeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.base_module_upgradeFeignClient;

/**
 * 实体[升级模块] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_module_upgradeServiceImpl implements IBase_module_upgradeService {

    @Autowired
    base_module_upgradeFeignClient base_module_upgradeFeignClient;


    @Override
    public Base_module_upgrade get(Integer id) {
		Base_module_upgrade et=base_module_upgradeFeignClient.get(id);
        if(et==null){
            et=new Base_module_upgrade();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Base_module_upgrade et) {
        Base_module_upgrade rt = base_module_upgradeFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Base_module_upgrade> list){
        base_module_upgradeFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Base_module_upgrade et) {
        Base_module_upgrade rt = base_module_upgradeFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_module_upgrade> list){
        base_module_upgradeFeignClient.createBatch(list) ;
    }

    @Override
    public Base_module_upgrade getDraft(Base_module_upgrade et) {
        et=base_module_upgradeFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=base_module_upgradeFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        base_module_upgradeFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_module_upgrade> searchDefault(Base_module_upgradeSearchContext context) {
        Page<Base_module_upgrade> base_module_upgrades=base_module_upgradeFeignClient.searchDefault(context);
        return base_module_upgrades;
    }


}


