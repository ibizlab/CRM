package cn.ibizlab.odoo.core.odoo_product.valuerule.anno.product_category;

import cn.ibizlab.odoo.core.odoo_product.valuerule.validator.product_category.Product_categoryProperty_stock_journalDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Product_category
 * 属性：Property_stock_journal
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Product_categoryProperty_stock_journalDefaultValidator.class})
public @interface Product_categoryProperty_stock_journalDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
