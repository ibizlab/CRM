package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_compose_message;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_compose_message] 服务对象接口
 */
public interface Imail_compose_messageClientService{

    public Imail_compose_message createModel() ;

    public void remove(Imail_compose_message mail_compose_message);

    public void update(Imail_compose_message mail_compose_message);

    public void removeBatch(List<Imail_compose_message> mail_compose_messages);

    public Page<Imail_compose_message> fetchDefault(SearchContext context);

    public void updateBatch(List<Imail_compose_message> mail_compose_messages);

    public void create(Imail_compose_message mail_compose_message);

    public void createBatch(List<Imail_compose_message> mail_compose_messages);

    public void get(Imail_compose_message mail_compose_message);

    public Page<Imail_compose_message> select(SearchContext context);

    public void getDraft(Imail_compose_message mail_compose_message);

}
