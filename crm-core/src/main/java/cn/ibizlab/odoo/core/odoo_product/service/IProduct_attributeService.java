package cn.ibizlab.odoo.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_product.domain.Product_attribute;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_attributeSearchContext;


/**
 * 实体[Product_attribute] 服务对象接口
 */
public interface IProduct_attributeService{

    boolean create(Product_attribute et) ;
    void createBatch(List<Product_attribute> list) ;
    boolean update(Product_attribute et) ;
    void updateBatch(List<Product_attribute> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Product_attribute getDraft(Product_attribute et) ;
    Product_attribute get(Integer key) ;
    Page<Product_attribute> searchDefault(Product_attributeSearchContext context) ;

}



