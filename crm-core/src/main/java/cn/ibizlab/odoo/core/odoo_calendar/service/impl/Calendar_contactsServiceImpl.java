package cn.ibizlab.odoo.core.odoo_calendar.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_contacts;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_contactsSearchContext;
import cn.ibizlab.odoo.core.odoo_calendar.service.ICalendar_contactsService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_calendar.client.calendar_contactsFeignClient;

/**
 * 实体[日历联系人] 服务对象接口实现
 */
@Slf4j
@Service
public class Calendar_contactsServiceImpl implements ICalendar_contactsService {

    @Autowired
    calendar_contactsFeignClient calendar_contactsFeignClient;


    @Override
    public Calendar_contacts get(Integer id) {
		Calendar_contacts et=calendar_contactsFeignClient.get(id);
        if(et==null){
            et=new Calendar_contacts();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Calendar_contacts getDraft(Calendar_contacts et) {
        et=calendar_contactsFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=calendar_contactsFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        calendar_contactsFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Calendar_contacts et) {
        Calendar_contacts rt = calendar_contactsFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Calendar_contacts> list){
        calendar_contactsFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Calendar_contacts et) {
        Calendar_contacts rt = calendar_contactsFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Calendar_contacts> list){
        calendar_contactsFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Calendar_contacts> searchDefault(Calendar_contactsSearchContext context) {
        Page<Calendar_contacts> calendar_contactss=calendar_contactsFeignClient.searchDefault(context);
        return calendar_contactss;
    }


}


