package cn.ibizlab.odoo.core.odoo_event.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_type_mail;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_type_mailSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[event_type_mail] 服务对象接口
 */
@FeignClient(value = "odoo-event", contextId = "event-type-mail", fallback = event_type_mailFallback.class)
public interface event_type_mailFeignClient {


    @RequestMapping(method = RequestMethod.PUT, value = "/event_type_mails/{id}")
    Event_type_mail update(@PathVariable("id") Integer id,@RequestBody Event_type_mail event_type_mail);

    @RequestMapping(method = RequestMethod.PUT, value = "/event_type_mails/batch")
    Boolean updateBatch(@RequestBody List<Event_type_mail> event_type_mails);


    @RequestMapping(method = RequestMethod.DELETE, value = "/event_type_mails/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/event_type_mails/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);





    @RequestMapping(method = RequestMethod.POST, value = "/event_type_mails/searchdefault")
    Page<Event_type_mail> searchDefault(@RequestBody Event_type_mailSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/event_type_mails/{id}")
    Event_type_mail get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/event_type_mails")
    Event_type_mail create(@RequestBody Event_type_mail event_type_mail);

    @RequestMapping(method = RequestMethod.POST, value = "/event_type_mails/batch")
    Boolean createBatch(@RequestBody List<Event_type_mail> event_type_mails);


    @RequestMapping(method = RequestMethod.GET, value = "/event_type_mails/select")
    Page<Event_type_mail> select();


    @RequestMapping(method = RequestMethod.GET, value = "/event_type_mails/getdraft")
    Event_type_mail getDraft();


}
