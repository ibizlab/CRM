package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Isale_report;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[sale_report] 服务对象接口
 */
public interface Isale_reportClientService{

    public Isale_report createModel() ;

    public void get(Isale_report sale_report);

    public void removeBatch(List<Isale_report> sale_reports);

    public void update(Isale_report sale_report);

    public Page<Isale_report> fetchDefault(SearchContext context);

    public void remove(Isale_report sale_report);

    public void create(Isale_report sale_report);

    public void createBatch(List<Isale_report> sale_reports);

    public void updateBatch(List<Isale_report> sale_reports);

    public Page<Isale_report> select(SearchContext context);

    public void getDraft(Isale_report sale_report);

}
