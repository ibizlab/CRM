package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_tax_template;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_tax_template] 服务对象接口
 */
public interface Iaccount_tax_templateClientService{

    public Iaccount_tax_template createModel() ;

    public void update(Iaccount_tax_template account_tax_template);

    public void create(Iaccount_tax_template account_tax_template);

    public void get(Iaccount_tax_template account_tax_template);

    public void removeBatch(List<Iaccount_tax_template> account_tax_templates);

    public void updateBatch(List<Iaccount_tax_template> account_tax_templates);

    public void remove(Iaccount_tax_template account_tax_template);

    public void createBatch(List<Iaccount_tax_template> account_tax_templates);

    public Page<Iaccount_tax_template> fetchDefault(SearchContext context);

    public Page<Iaccount_tax_template> select(SearchContext context);

    public void getDraft(Iaccount_tax_template account_tax_template);

}
