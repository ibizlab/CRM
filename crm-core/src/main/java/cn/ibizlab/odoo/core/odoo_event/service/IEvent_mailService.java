package cn.ibizlab.odoo.core.odoo_event.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_event.domain.Event_mail;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_mailSearchContext;


/**
 * 实体[Event_mail] 服务对象接口
 */
public interface IEvent_mailService{

    boolean create(Event_mail et) ;
    void createBatch(List<Event_mail> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Event_mail getDraft(Event_mail et) ;
    boolean update(Event_mail et) ;
    void updateBatch(List<Event_mail> list) ;
    Event_mail get(Integer key) ;
    Page<Event_mail> searchDefault(Event_mailSearchContext context) ;

}



