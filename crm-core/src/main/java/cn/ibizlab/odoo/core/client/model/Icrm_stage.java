package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [crm_stage] 对象
 */
public interface Icrm_stage {

    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [在漏斗中折叠]
     */
    public void setFold(String fold);
    
    /**
     * 设置 [在漏斗中折叠]
     */
    public String getFold();

    /**
     * 获取 [在漏斗中折叠]脏标记
     */
    public boolean getFoldDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [优先级管理解释]
     */
    public void setLegend_priority(String legend_priority);
    
    /**
     * 设置 [优先级管理解释]
     */
    public String getLegend_priority();

    /**
     * 获取 [优先级管理解释]脏标记
     */
    public boolean getLegend_priorityDirtyFlag();
    /**
     * 获取 [阶段名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [阶段名称]
     */
    public String getName();

    /**
     * 获取 [阶段名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [自动修改概率]
     */
    public void setOn_change(String on_change);
    
    /**
     * 设置 [自动修改概率]
     */
    public String getOn_change();

    /**
     * 获取 [自动修改概率]脏标记
     */
    public boolean getOn_changeDirtyFlag();
    /**
     * 获取 [概率(%)]
     */
    public void setProbability(Double probability);
    
    /**
     * 设置 [概率(%)]
     */
    public Double getProbability();

    /**
     * 获取 [概率(%)]脏标记
     */
    public boolean getProbabilityDirtyFlag();
    /**
     * 获取 [要求]
     */
    public void setRequirements(String requirements);
    
    /**
     * 设置 [要求]
     */
    public String getRequirements();

    /**
     * 获取 [要求]脏标记
     */
    public boolean getRequirementsDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [team_count]
     */
    public void setTeam_count(Integer team_count);
    
    /**
     * 设置 [team_count]
     */
    public Integer getTeam_count();

    /**
     * 获取 [team_count]脏标记
     */
    public boolean getTeam_countDirtyFlag();
    /**
     * 获取 [销售团队]
     */
    public void setTeam_id(Integer team_id);
    
    /**
     * 设置 [销售团队]
     */
    public Integer getTeam_id();

    /**
     * 获取 [销售团队]脏标记
     */
    public boolean getTeam_idDirtyFlag();
    /**
     * 获取 [销售团队]
     */
    public void setTeam_id_text(String team_id_text);
    
    /**
     * 设置 [销售团队]
     */
    public String getTeam_id_text();

    /**
     * 获取 [销售团队]脏标记
     */
    public boolean getTeam_id_textDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
