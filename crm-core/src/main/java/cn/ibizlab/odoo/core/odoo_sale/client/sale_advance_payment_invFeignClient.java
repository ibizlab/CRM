package cn.ibizlab.odoo.core.odoo_sale.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_advance_payment_inv;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_advance_payment_invSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[sale_advance_payment_inv] 服务对象接口
 */
@FeignClient(value = "odoo-sale", contextId = "sale-advance-payment-inv", fallback = sale_advance_payment_invFallback.class)
public interface sale_advance_payment_invFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/sale_advance_payment_invs")
    Sale_advance_payment_inv create(@RequestBody Sale_advance_payment_inv sale_advance_payment_inv);

    @RequestMapping(method = RequestMethod.POST, value = "/sale_advance_payment_invs/batch")
    Boolean createBatch(@RequestBody List<Sale_advance_payment_inv> sale_advance_payment_invs);




    @RequestMapping(method = RequestMethod.POST, value = "/sale_advance_payment_invs/searchdefault")
    Page<Sale_advance_payment_inv> searchDefault(@RequestBody Sale_advance_payment_invSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/sale_advance_payment_invs/{id}")
    Sale_advance_payment_inv get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/sale_advance_payment_invs/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/sale_advance_payment_invs/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/sale_advance_payment_invs/{id}")
    Sale_advance_payment_inv update(@PathVariable("id") Integer id,@RequestBody Sale_advance_payment_inv sale_advance_payment_inv);

    @RequestMapping(method = RequestMethod.PUT, value = "/sale_advance_payment_invs/batch")
    Boolean updateBatch(@RequestBody List<Sale_advance_payment_inv> sale_advance_payment_invs);



    @RequestMapping(method = RequestMethod.GET, value = "/sale_advance_payment_invs/select")
    Page<Sale_advance_payment_inv> select();


    @RequestMapping(method = RequestMethod.GET, value = "/sale_advance_payment_invs/getdraft")
    Sale_advance_payment_inv getDraft();


}
