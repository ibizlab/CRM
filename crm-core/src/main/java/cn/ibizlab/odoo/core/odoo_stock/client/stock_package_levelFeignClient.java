package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_package_level;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_package_levelSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_package_level] 服务对象接口
 */
@FeignClient(value = "odoo-stock", contextId = "stock-package-level", fallback = stock_package_levelFallback.class)
public interface stock_package_levelFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/stock_package_levels")
    Stock_package_level create(@RequestBody Stock_package_level stock_package_level);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_package_levels/batch")
    Boolean createBatch(@RequestBody List<Stock_package_level> stock_package_levels);


    @RequestMapping(method = RequestMethod.PUT, value = "/stock_package_levels/{id}")
    Stock_package_level update(@PathVariable("id") Integer id,@RequestBody Stock_package_level stock_package_level);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_package_levels/batch")
    Boolean updateBatch(@RequestBody List<Stock_package_level> stock_package_levels);





    @RequestMapping(method = RequestMethod.POST, value = "/stock_package_levels/searchdefault")
    Page<Stock_package_level> searchDefault(@RequestBody Stock_package_levelSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_package_levels/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_package_levels/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_package_levels/{id}")
    Stock_package_level get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_package_levels/select")
    Page<Stock_package_level> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_package_levels/getdraft")
    Stock_package_level getDraft();


}
