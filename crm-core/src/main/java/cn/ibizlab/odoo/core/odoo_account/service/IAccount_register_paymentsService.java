package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_register_payments;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_register_paymentsSearchContext;


/**
 * 实体[Account_register_payments] 服务对象接口
 */
public interface IAccount_register_paymentsService{

    boolean checkKey(Account_register_payments et) ;
    boolean save(Account_register_payments et) ;
    void saveBatch(List<Account_register_payments> list) ;
    Account_register_payments get(Integer key) ;
    boolean create(Account_register_payments et) ;
    void createBatch(List<Account_register_payments> list) ;
    boolean update(Account_register_payments et) ;
    void updateBatch(List<Account_register_payments> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Account_register_payments getDraft(Account_register_payments et) ;
    Page<Account_register_payments> searchDefault(Account_register_paymentsSearchContext context) ;

}



