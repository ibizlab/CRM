package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_alias;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_aliasSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_alias] 服务对象接口
 */
@Component
public class mail_aliasFallback implements mail_aliasFeignClient{

    public Page<Mail_alias> searchDefault(Mail_aliasSearchContext context){
            return null;
     }



    public Mail_alias update(Integer id, Mail_alias mail_alias){
            return null;
     }
    public Boolean updateBatch(List<Mail_alias> mail_aliases){
            return false;
     }



    public Mail_alias get(Integer id){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Mail_alias create(Mail_alias mail_alias){
            return null;
     }
    public Boolean createBatch(List<Mail_alias> mail_aliases){
            return false;
     }


    public Page<Mail_alias> select(){
            return null;
     }

    public Mail_alias getDraft(){
            return null;
    }



}
