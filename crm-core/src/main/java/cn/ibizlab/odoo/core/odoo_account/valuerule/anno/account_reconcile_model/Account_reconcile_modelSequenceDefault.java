package cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_reconcile_model;

import cn.ibizlab.odoo.core.odoo_account.valuerule.validator.account_reconcile_model.Account_reconcile_modelSequenceDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Account_reconcile_model
 * 属性：Sequence
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Account_reconcile_modelSequenceDefaultValidator.class})
public @interface Account_reconcile_modelSequenceDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
