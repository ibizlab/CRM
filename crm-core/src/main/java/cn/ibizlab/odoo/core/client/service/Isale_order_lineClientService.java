package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Isale_order_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[sale_order_line] 服务对象接口
 */
public interface Isale_order_lineClientService{

    public Isale_order_line createModel() ;

    public void removeBatch(List<Isale_order_line> sale_order_lines);

    public void update(Isale_order_line sale_order_line);

    public void createBatch(List<Isale_order_line> sale_order_lines);

    public void remove(Isale_order_line sale_order_line);

    public void get(Isale_order_line sale_order_line);

    public void create(Isale_order_line sale_order_line);

    public void updateBatch(List<Isale_order_line> sale_order_lines);

    public Page<Isale_order_line> fetchDefault(SearchContext context);

    public Page<Isale_order_line> select(SearchContext context);

    public void getDraft(Isale_order_line sale_order_line);

    public void checkKey(Isale_order_line sale_order_line);

    public void save(Isale_order_line sale_order_line);

}
