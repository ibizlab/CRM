package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ihr_expense;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_expense] 服务对象接口
 */
public interface Ihr_expenseClientService{

    public Ihr_expense createModel() ;

    public void removeBatch(List<Ihr_expense> hr_expenses);

    public void create(Ihr_expense hr_expense);

    public void get(Ihr_expense hr_expense);

    public Page<Ihr_expense> fetchDefault(SearchContext context);

    public void createBatch(List<Ihr_expense> hr_expenses);

    public void updateBatch(List<Ihr_expense> hr_expenses);

    public void remove(Ihr_expense hr_expense);

    public void update(Ihr_expense hr_expense);

    public Page<Ihr_expense> select(SearchContext context);

    public void getDraft(Ihr_expense hr_expense);

}
