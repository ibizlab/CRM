package cn.ibizlab.odoo.core.odoo_web_editor.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [Web编辑器转换器测试] 对象
 */
@Data
public class Web_editor_converter_test extends EntityClient implements Serializable {

    /**
     * Text
     */
    @JSONField(name = "text")
    @JsonProperty("text")
    private String text;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * Lorsqu'un pancake prend l'avion à destination de Toronto et
     */
    @DEField(name = "selection_str")
    @JSONField(name = "selection_str")
    @JsonProperty("selection_str")
    private String selectionStr;

    /**
     * Numeric
     */
    @JSONField(name = "numeric")
    @JsonProperty("numeric")
    private Double numeric;

    /**
     * Integer
     */
    @JSONField(name = "integer")
    @JsonProperty("integer")
    private Integer integer;

    /**
     * Binary
     */
    @JSONField(name = "binary")
    @JsonProperty("binary")
    private byte[] binary;

    /**
     * 日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 浮点数
     */
    @JSONField(name = "ibizfloat")
    @JsonProperty("ibizfloat")
    private Double ibizfloat;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * Datetime
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "datetime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("datetime")
    private Timestamp datetime;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * Html
     */
    @JSONField(name = "html")
    @JsonProperty("html")
    private String html;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * Char
     */
    @JSONField(name = "ibizchar")
    @JsonProperty("ibizchar")
    private String ibizchar;

    /**
     * Selection
     */
    @JSONField(name = "selection")
    @JsonProperty("selection")
    private String selection;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * Many2One
     */
    @JSONField(name = "many2one_text")
    @JsonProperty("many2one_text")
    private String many2oneText;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * Many2One
     */
    @JSONField(name = "many2one")
    @JsonProperty("many2one")
    private Integer many2one;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoomany2one")
    @JsonProperty("odoomany2one")
    private cn.ibizlab.odoo.core.odoo_web_editor.domain.Web_editor_converter_test_sub odooMany2one;




    /**
     * 设置 [Text]
     */
    public void setText(String text){
        this.text = text ;
        this.modify("text",text);
    }
    /**
     * 设置 [Lorsqu'un pancake prend l'avion à destination de Toronto et]
     */
    public void setSelectionStr(String selectionStr){
        this.selectionStr = selectionStr ;
        this.modify("selection_str",selectionStr);
    }
    /**
     * 设置 [Numeric]
     */
    public void setNumeric(Double numeric){
        this.numeric = numeric ;
        this.modify("numeric",numeric);
    }
    /**
     * 设置 [Integer]
     */
    public void setInteger(Integer integer){
        this.integer = integer ;
        this.modify("integer",integer);
    }
    /**
     * 设置 [日期]
     */
    public void setDate(Timestamp date){
        this.date = date ;
        this.modify("date",date);
    }
    /**
     * 设置 [浮点数]
     */
    public void setIbizfloat(Double ibizfloat){
        this.ibizfloat = ibizfloat ;
        this.modify("ibizfloat",ibizfloat);
    }
    /**
     * 设置 [Datetime]
     */
    public void setDatetime(Timestamp datetime){
        this.datetime = datetime ;
        this.modify("datetime",datetime);
    }
    /**
     * 设置 [Html]
     */
    public void setHtml(String html){
        this.html = html ;
        this.modify("html",html);
    }
    /**
     * 设置 [Char]
     */
    public void setIbizchar(String ibizchar){
        this.ibizchar = ibizchar ;
        this.modify("ibizchar",ibizchar);
    }
    /**
     * 设置 [Selection]
     */
    public void setSelection(String selection){
        this.selection = selection ;
        this.modify("selection",selection);
    }
    /**
     * 设置 [Many2One]
     */
    public void setMany2one(Integer many2one){
        this.many2one = many2one ;
        this.modify("many2one",many2one);
    }

}


