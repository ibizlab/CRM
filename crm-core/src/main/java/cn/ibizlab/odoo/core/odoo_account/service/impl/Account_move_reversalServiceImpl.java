package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_move_reversal;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_move_reversalSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_move_reversalService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_move_reversalFeignClient;

/**
 * 实体[会计凭证逆转] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_move_reversalServiceImpl implements IAccount_move_reversalService {

    @Autowired
    account_move_reversalFeignClient account_move_reversalFeignClient;


    @Override
    public Account_move_reversal get(Integer id) {
		Account_move_reversal et=account_move_reversalFeignClient.get(id);
        if(et==null){
            et=new Account_move_reversal();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Account_move_reversal et) {
        Account_move_reversal rt = account_move_reversalFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_move_reversal> list){
        account_move_reversalFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_move_reversalFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_move_reversalFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Account_move_reversal et) {
        Account_move_reversal rt = account_move_reversalFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_move_reversal> list){
        account_move_reversalFeignClient.createBatch(list) ;
    }

    @Override
    public Account_move_reversal getDraft(Account_move_reversal et) {
        et=account_move_reversalFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_move_reversal> searchDefault(Account_move_reversalSearchContext context) {
        Page<Account_move_reversal> account_move_reversals=account_move_reversalFeignClient.searchDefault(context);
        return account_move_reversals;
    }


}


