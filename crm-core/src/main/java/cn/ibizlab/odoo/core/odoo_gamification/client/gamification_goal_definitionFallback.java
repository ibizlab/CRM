package cn.ibizlab.odoo.core.odoo_gamification.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal_definition;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_goal_definitionSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[gamification_goal_definition] 服务对象接口
 */
@Component
public class gamification_goal_definitionFallback implements gamification_goal_definitionFeignClient{


    public Page<Gamification_goal_definition> searchDefault(Gamification_goal_definitionSearchContext context){
            return null;
     }


    public Gamification_goal_definition update(Integer id, Gamification_goal_definition gamification_goal_definition){
            return null;
     }
    public Boolean updateBatch(List<Gamification_goal_definition> gamification_goal_definitions){
            return false;
     }


    public Gamification_goal_definition get(Integer id){
            return null;
     }



    public Gamification_goal_definition create(Gamification_goal_definition gamification_goal_definition){
            return null;
     }
    public Boolean createBatch(List<Gamification_goal_definition> gamification_goal_definitions){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Page<Gamification_goal_definition> select(){
            return null;
     }

    public Gamification_goal_definition getDraft(){
            return null;
    }



}
