package cn.ibizlab.odoo.core.odoo_survey.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_stage;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_stageSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[survey_stage] 服务对象接口
 */
@Component
public class survey_stageFallback implements survey_stageFeignClient{



    public Survey_stage create(Survey_stage survey_stage){
            return null;
     }
    public Boolean createBatch(List<Survey_stage> survey_stages){
            return false;
     }

    public Page<Survey_stage> searchDefault(Survey_stageSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Survey_stage update(Integer id, Survey_stage survey_stage){
            return null;
     }
    public Boolean updateBatch(List<Survey_stage> survey_stages){
            return false;
     }


    public Survey_stage get(Integer id){
            return null;
     }


    public Page<Survey_stage> select(){
            return null;
     }

    public Survey_stage getDraft(){
            return null;
    }



}
