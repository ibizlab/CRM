package cn.ibizlab.odoo.core.odoo_rating.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_rating.domain.Rating_rating;
import cn.ibizlab.odoo.core.odoo_rating.filter.Rating_ratingSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[rating_rating] 服务对象接口
 */
@Component
public class rating_ratingFallback implements rating_ratingFeignClient{

    public Rating_rating get(Integer id){
            return null;
     }


    public Rating_rating update(Integer id, Rating_rating rating_rating){
            return null;
     }
    public Boolean updateBatch(List<Rating_rating> rating_ratings){
            return false;
     }


    public Rating_rating create(Rating_rating rating_rating){
            return null;
     }
    public Boolean createBatch(List<Rating_rating> rating_ratings){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }



    public Page<Rating_rating> searchDefault(Rating_ratingSearchContext context){
            return null;
     }



    public Page<Rating_rating> select(){
            return null;
     }

    public Rating_rating getDraft(){
            return null;
    }



}
