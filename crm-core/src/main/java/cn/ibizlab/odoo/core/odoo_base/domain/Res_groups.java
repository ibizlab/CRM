package cn.ibizlab.odoo.core.odoo_base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [访问群] 对象
 */
@Data
public class Res_groups extends EntityClient implements Serializable {

    /**
     * 用户
     */
    @JSONField(name = "users")
    @JsonProperty("users")
    private String users;

    /**
     * 名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 继承
     */
    @JSONField(name = "implied_ids")
    @JsonProperty("implied_ids")
    private String impliedIds;

    /**
     * 及物继承
     */
    @JSONField(name = "trans_implied_ids")
    @JsonProperty("trans_implied_ids")
    private String transImpliedIds;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 视图
     */
    @JSONField(name = "view_access")
    @JsonProperty("view_access")
    private String viewAccess;

    /**
     * 规则
     */
    @JSONField(name = "rule_groups")
    @JsonProperty("rule_groups")
    private String ruleGroups;

    /**
     * 访问控制
     */
    @JSONField(name = "model_access")
    @JsonProperty("model_access")
    private String modelAccess;

    /**
     * 应用
     */
    @DEField(name = "category_id")
    @JSONField(name = "category_id")
    @JsonProperty("category_id")
    private Integer categoryId;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 访问菜单
     */
    @JSONField(name = "menu_access")
    @JsonProperty("menu_access")
    private String menuAccess;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 备注
     */
    @JSONField(name = "comment")
    @JsonProperty("comment")
    private String comment;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 共享用户组
     */
    @JSONField(name = "share")
    @JsonProperty("share")
    private String share;

    /**
     * 颜色索引
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 群组名称
     */
    @JSONField(name = "full_name")
    @JsonProperty("full_name")
    private String fullName;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [应用]
     */
    public void setCategoryId(Integer categoryId){
        this.categoryId = categoryId ;
        this.modify("category_id",categoryId);
    }
    /**
     * 设置 [备注]
     */
    public void setComment(String comment){
        this.comment = comment ;
        this.modify("comment",comment);
    }
    /**
     * 设置 [共享用户组]
     */
    public void setShare(String share){
        this.share = share ;
        this.modify("share",share);
    }
    /**
     * 设置 [颜色索引]
     */
    public void setColor(Integer color){
        this.color = color ;
        this.modify("color",color);
    }

}


