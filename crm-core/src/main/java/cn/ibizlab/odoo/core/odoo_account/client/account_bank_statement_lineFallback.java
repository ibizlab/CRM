package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_lineSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_bank_statement_line] 服务对象接口
 */
@Component
public class account_bank_statement_lineFallback implements account_bank_statement_lineFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Page<Account_bank_statement_line> searchDefault(Account_bank_statement_lineSearchContext context){
            return null;
     }



    public Account_bank_statement_line get(Integer id){
            return null;
     }



    public Account_bank_statement_line update(Integer id, Account_bank_statement_line account_bank_statement_line){
            return null;
     }
    public Boolean updateBatch(List<Account_bank_statement_line> account_bank_statement_lines){
            return false;
     }


    public Account_bank_statement_line create(Account_bank_statement_line account_bank_statement_line){
            return null;
     }
    public Boolean createBatch(List<Account_bank_statement_line> account_bank_statement_lines){
            return false;
     }

    public Page<Account_bank_statement_line> select(){
            return null;
     }

    public Account_bank_statement_line getDraft(){
            return null;
    }



}
