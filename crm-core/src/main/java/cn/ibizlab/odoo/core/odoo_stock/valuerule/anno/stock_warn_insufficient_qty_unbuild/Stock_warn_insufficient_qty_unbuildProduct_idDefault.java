package cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_warn_insufficient_qty_unbuild;

import cn.ibizlab.odoo.core.odoo_stock.valuerule.validator.stock_warn_insufficient_qty_unbuild.Stock_warn_insufficient_qty_unbuildProduct_idDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Stock_warn_insufficient_qty_unbuild
 * 属性：Product_id
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Stock_warn_insufficient_qty_unbuildProduct_idDefaultValidator.class})
public @interface Stock_warn_insufficient_qty_unbuildProduct_idDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
