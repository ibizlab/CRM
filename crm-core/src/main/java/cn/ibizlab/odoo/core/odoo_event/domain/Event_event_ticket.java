package cn.ibizlab.odoo.core.odoo_event.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [活动入场券] 对象
 */
@Data
public class Event_event_ticket extends EntityClient implements Serializable {

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 可用席位
     */
    @DEField(name = "seats_available")
    @JSONField(name = "seats_available")
    @JsonProperty("seats_available")
    private Integer seatsAvailable;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 减税后价格
     */
    @JSONField(name = "price_reduce_taxinc")
    @JsonProperty("price_reduce_taxinc")
    private Double priceReduceTaxinc;

    /**
     * 降价
     */
    @JSONField(name = "price_reduce")
    @JsonProperty("price_reduce")
    private Double priceReduce;

    /**
     * 登记
     */
    @JSONField(name = "registration_ids")
    @JsonProperty("registration_ids")
    private String registrationIds;

    /**
     * 未确认的席位预订
     */
    @DEField(name = "seats_unconfirmed")
    @JSONField(name = "seats_unconfirmed")
    @JsonProperty("seats_unconfirmed")
    private Integer seatsUnconfirmed;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 预订席位
     */
    @DEField(name = "seats_reserved")
    @JSONField(name = "seats_reserved")
    @JsonProperty("seats_reserved")
    private Integer seatsReserved;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 价格
     */
    @JSONField(name = "price")
    @JsonProperty("price")
    private Double price;

    /**
     * 座椅
     */
    @DEField(name = "seats_used")
    @JSONField(name = "seats_used")
    @JsonProperty("seats_used")
    private Integer seatsUsed;

    /**
     * 销售结束
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "deadline" , format="yyyy-MM-dd")
    @JsonProperty("deadline")
    private Timestamp deadline;

    /**
     * 可用席位
     */
    @DEField(name = "seats_availability")
    @JSONField(name = "seats_availability")
    @JsonProperty("seats_availability")
    private String seatsAvailability;

    /**
     * 已结束
     */
    @JSONField(name = "is_expired")
    @JsonProperty("is_expired")
    private String isExpired;

    /**
     * 最多可用席位
     */
    @DEField(name = "seats_max")
    @JSONField(name = "seats_max")
    @JsonProperty("seats_max")
    private Integer seatsMax;

    /**
     * 活动类别
     */
    @JSONField(name = "event_type_id_text")
    @JsonProperty("event_type_id_text")
    private String eventTypeIdText;

    /**
     * 活动
     */
    @JSONField(name = "event_id_text")
    @JsonProperty("event_id_text")
    private String eventIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 产品
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;

    /**
     * 产品
     */
    @DEField(name = "product_id")
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Integer productId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 活动
     */
    @DEField(name = "event_id")
    @JSONField(name = "event_id")
    @JsonProperty("event_id")
    private Integer eventId;

    /**
     * 活动类别
     */
    @DEField(name = "event_type_id")
    @JSONField(name = "event_type_id")
    @JsonProperty("event_type_id")
    private Integer eventTypeId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odooevent")
    @JsonProperty("odooevent")
    private cn.ibizlab.odoo.core.odoo_event.domain.Event_event odooEvent;

    /**
     * 
     */
    @JSONField(name = "odooeventtype")
    @JsonProperty("odooeventtype")
    private cn.ibizlab.odoo.core.odoo_event.domain.Event_type odooEventType;

    /**
     * 
     */
    @JSONField(name = "odooproduct")
    @JsonProperty("odooproduct")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_product odooProduct;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;


    /**
     * 销售订单行
     */
    @JSONField(name = "sale_order_lines")
    @JsonProperty("sale_order_lines")
    private List<cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_line> saleOrderLine;



    /**
     * 设置 [可用席位]
     */
    public void setSeatsAvailable(Integer seatsAvailable){
        this.seatsAvailable = seatsAvailable ;
        this.modify("seats_available",seatsAvailable);
    }
    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [未确认的席位预订]
     */
    public void setSeatsUnconfirmed(Integer seatsUnconfirmed){
        this.seatsUnconfirmed = seatsUnconfirmed ;
        this.modify("seats_unconfirmed",seatsUnconfirmed);
    }
    /**
     * 设置 [预订席位]
     */
    public void setSeatsReserved(Integer seatsReserved){
        this.seatsReserved = seatsReserved ;
        this.modify("seats_reserved",seatsReserved);
    }
    /**
     * 设置 [价格]
     */
    public void setPrice(Double price){
        this.price = price ;
        this.modify("price",price);
    }
    /**
     * 设置 [座椅]
     */
    public void setSeatsUsed(Integer seatsUsed){
        this.seatsUsed = seatsUsed ;
        this.modify("seats_used",seatsUsed);
    }
    /**
     * 设置 [销售结束]
     */
    public void setDeadline(Timestamp deadline){
        this.deadline = deadline ;
        this.modify("deadline",deadline);
    }
    /**
     * 设置 [可用席位]
     */
    public void setSeatsAvailability(String seatsAvailability){
        this.seatsAvailability = seatsAvailability ;
        this.modify("seats_availability",seatsAvailability);
    }
    /**
     * 设置 [最多可用席位]
     */
    public void setSeatsMax(Integer seatsMax){
        this.seatsMax = seatsMax ;
        this.modify("seats_max",seatsMax);
    }
    /**
     * 设置 [产品]
     */
    public void setProductId(Integer productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }
    /**
     * 设置 [活动]
     */
    public void setEventId(Integer eventId){
        this.eventId = eventId ;
        this.modify("event_id",eventId);
    }
    /**
     * 设置 [活动类别]
     */
    public void setEventTypeId(Integer eventTypeId){
        this.eventTypeId = eventTypeId ;
        this.modify("event_type_id",eventTypeId);
    }

}


