package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_move_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_move_lineSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_move_line] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-move-line", fallback = account_move_lineFallback.class)
public interface account_move_lineFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/account_move_lines/{id}")
    Account_move_line update(@PathVariable("id") Integer id,@RequestBody Account_move_line account_move_line);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_move_lines/batch")
    Boolean updateBatch(@RequestBody List<Account_move_line> account_move_lines);




    @RequestMapping(method = RequestMethod.POST, value = "/account_move_lines/searchdefault")
    Page<Account_move_line> searchDefault(@RequestBody Account_move_lineSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/account_move_lines")
    Account_move_line create(@RequestBody Account_move_line account_move_line);

    @RequestMapping(method = RequestMethod.POST, value = "/account_move_lines/batch")
    Boolean createBatch(@RequestBody List<Account_move_line> account_move_lines);


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_move_lines/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_move_lines/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/account_move_lines/{id}")
    Account_move_line get(@PathVariable("id") Integer id);




    @RequestMapping(method = RequestMethod.GET, value = "/account_move_lines/select")
    Page<Account_move_line> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_move_lines/getdraft")
    Account_move_line getDraft();


}
