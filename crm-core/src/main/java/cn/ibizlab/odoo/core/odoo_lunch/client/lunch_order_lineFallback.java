package cn.ibizlab.odoo.core.odoo_lunch.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_order_line;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_order_lineSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[lunch_order_line] 服务对象接口
 */
@Component
public class lunch_order_lineFallback implements lunch_order_lineFeignClient{

    public Lunch_order_line get(Integer id){
            return null;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Lunch_order_line update(Integer id, Lunch_order_line lunch_order_line){
            return null;
     }
    public Boolean updateBatch(List<Lunch_order_line> lunch_order_lines){
            return false;
     }


    public Page<Lunch_order_line> searchDefault(Lunch_order_lineSearchContext context){
            return null;
     }


    public Lunch_order_line create(Lunch_order_line lunch_order_line){
            return null;
     }
    public Boolean createBatch(List<Lunch_order_line> lunch_order_lines){
            return false;
     }


    public Page<Lunch_order_line> select(){
            return null;
     }

    public Lunch_order_line getDraft(){
            return null;
    }



}
