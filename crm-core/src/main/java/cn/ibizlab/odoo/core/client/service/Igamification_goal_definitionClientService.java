package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Igamification_goal_definition;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[gamification_goal_definition] 服务对象接口
 */
public interface Igamification_goal_definitionClientService{

    public Igamification_goal_definition createModel() ;

    public void updateBatch(List<Igamification_goal_definition> gamification_goal_definitions);

    public Page<Igamification_goal_definition> fetchDefault(SearchContext context);

    public void update(Igamification_goal_definition gamification_goal_definition);

    public void get(Igamification_goal_definition gamification_goal_definition);

    public void removeBatch(List<Igamification_goal_definition> gamification_goal_definitions);

    public void create(Igamification_goal_definition gamification_goal_definition);

    public void remove(Igamification_goal_definition gamification_goal_definition);

    public void createBatch(List<Igamification_goal_definition> gamification_goal_definitions);

    public Page<Igamification_goal_definition> select(SearchContext context);

    public void getDraft(Igamification_goal_definition gamification_goal_definition);

}
