package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_move_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_move_line] 服务对象接口
 */
public interface Istock_move_lineClientService{

    public Istock_move_line createModel() ;

    public void removeBatch(List<Istock_move_line> stock_move_lines);

    public void remove(Istock_move_line stock_move_line);

    public void create(Istock_move_line stock_move_line);

    public void createBatch(List<Istock_move_line> stock_move_lines);

    public Page<Istock_move_line> fetchDefault(SearchContext context);

    public void updateBatch(List<Istock_move_line> stock_move_lines);

    public void get(Istock_move_line stock_move_line);

    public void update(Istock_move_line stock_move_line);

    public Page<Istock_move_line> select(SearchContext context);

    public void getDraft(Istock_move_line stock_move_line);

}
