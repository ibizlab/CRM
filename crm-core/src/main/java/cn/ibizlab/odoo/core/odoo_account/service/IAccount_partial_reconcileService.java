package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_partial_reconcile;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_partial_reconcileSearchContext;


/**
 * 实体[Account_partial_reconcile] 服务对象接口
 */
public interface IAccount_partial_reconcileService{

    boolean create(Account_partial_reconcile et) ;
    void createBatch(List<Account_partial_reconcile> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Account_partial_reconcile et) ;
    void updateBatch(List<Account_partial_reconcile> list) ;
    Account_partial_reconcile get(Integer key) ;
    Account_partial_reconcile getDraft(Account_partial_reconcile et) ;
    Page<Account_partial_reconcile> searchDefault(Account_partial_reconcileSearchContext context) ;

}



