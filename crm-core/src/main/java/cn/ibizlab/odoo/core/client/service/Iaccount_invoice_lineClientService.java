package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_invoice_line] 服务对象接口
 */
public interface Iaccount_invoice_lineClientService{

    public Iaccount_invoice_line createModel() ;

    public void get(Iaccount_invoice_line account_invoice_line);

    public void create(Iaccount_invoice_line account_invoice_line);

    public Page<Iaccount_invoice_line> fetchDefault(SearchContext context);

    public void createBatch(List<Iaccount_invoice_line> account_invoice_lines);

    public void update(Iaccount_invoice_line account_invoice_line);

    public void remove(Iaccount_invoice_line account_invoice_line);

    public void updateBatch(List<Iaccount_invoice_line> account_invoice_lines);

    public void removeBatch(List<Iaccount_invoice_line> account_invoice_lines);

    public Page<Iaccount_invoice_line> select(SearchContext context);

    public void checkKey(Iaccount_invoice_line account_invoice_line);

    public void getDraft(Iaccount_invoice_line account_invoice_line);

    public void save(Iaccount_invoice_line account_invoice_line);

}
