package cn.ibizlab.odoo.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_contract_type;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_contract_typeSearchContext;


/**
 * 实体[Hr_contract_type] 服务对象接口
 */
public interface IHr_contract_typeService{

    Hr_contract_type getDraft(Hr_contract_type et) ;
    boolean create(Hr_contract_type et) ;
    void createBatch(List<Hr_contract_type> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Hr_contract_type get(Integer key) ;
    boolean update(Hr_contract_type et) ;
    void updateBatch(List<Hr_contract_type> list) ;
    Page<Hr_contract_type> searchDefault(Hr_contract_typeSearchContext context) ;

}



