package cn.ibizlab.odoo.core.odoo_mail.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.odoo.util.filter.SearchContextBase;

/**
 * ServiceApi数据实体[Mail_activity_type] 查询条件对象
 */
@Slf4j
@Data
public class Mail_activity_typeSearchContext extends SearchContextBase {
	private String n_category_eq;//[类别]

	private String n_delay_from_eq;//[延迟类型]

	private String n_name_like;//[名称]

	private String n_decoration_type_eq;//[排版类型]

	private String n_delay_unit_eq;//[延迟单位]

	private String n_default_next_type_id_text_eq;//[设置默认下一个活动]

	private String n_default_next_type_id_text_like;//[设置默认下一个活动]

	private String n_write_uid_text_eq;//[最后更新者]

	private String n_write_uid_text_like;//[最后更新者]

	private String n_create_uid_text_eq;//[创建人]

	private String n_create_uid_text_like;//[创建人]

	private Integer n_create_uid_eq;//[创建人]

	private Integer n_write_uid_eq;//[最后更新者]

	private Integer n_default_next_type_id_eq;//[设置默认下一个活动]

}



