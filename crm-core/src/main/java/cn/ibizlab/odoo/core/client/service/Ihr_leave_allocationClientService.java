package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ihr_leave_allocation;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_leave_allocation] 服务对象接口
 */
public interface Ihr_leave_allocationClientService{

    public Ihr_leave_allocation createModel() ;

    public void removeBatch(List<Ihr_leave_allocation> hr_leave_allocations);

    public void remove(Ihr_leave_allocation hr_leave_allocation);

    public void get(Ihr_leave_allocation hr_leave_allocation);

    public Page<Ihr_leave_allocation> fetchDefault(SearchContext context);

    public void updateBatch(List<Ihr_leave_allocation> hr_leave_allocations);

    public void update(Ihr_leave_allocation hr_leave_allocation);

    public void create(Ihr_leave_allocation hr_leave_allocation);

    public void createBatch(List<Ihr_leave_allocation> hr_leave_allocations);

    public Page<Ihr_leave_allocation> select(SearchContext context);

    public void getDraft(Ihr_leave_allocation hr_leave_allocation);

}
