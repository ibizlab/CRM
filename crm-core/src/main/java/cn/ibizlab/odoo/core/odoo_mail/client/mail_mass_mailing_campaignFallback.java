package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_campaign;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_campaignSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_mass_mailing_campaign] 服务对象接口
 */
@Component
public class mail_mass_mailing_campaignFallback implements mail_mass_mailing_campaignFeignClient{

    public Mail_mass_mailing_campaign update(Integer id, Mail_mass_mailing_campaign mail_mass_mailing_campaign){
            return null;
     }
    public Boolean updateBatch(List<Mail_mass_mailing_campaign> mail_mass_mailing_campaigns){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Mail_mass_mailing_campaign> searchDefault(Mail_mass_mailing_campaignSearchContext context){
            return null;
     }




    public Mail_mass_mailing_campaign get(Integer id){
            return null;
     }


    public Mail_mass_mailing_campaign create(Mail_mass_mailing_campaign mail_mass_mailing_campaign){
            return null;
     }
    public Boolean createBatch(List<Mail_mass_mailing_campaign> mail_mass_mailing_campaigns){
            return false;
     }


    public Page<Mail_mass_mailing_campaign> select(){
            return null;
     }

    public Mail_mass_mailing_campaign getDraft(){
            return null;
    }



}
