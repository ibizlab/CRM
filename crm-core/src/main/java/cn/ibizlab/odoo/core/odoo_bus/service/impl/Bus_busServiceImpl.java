package cn.ibizlab.odoo.core.odoo_bus.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_bus.domain.Bus_bus;
import cn.ibizlab.odoo.core.odoo_bus.filter.Bus_busSearchContext;
import cn.ibizlab.odoo.core.odoo_bus.service.IBus_busService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_bus.client.bus_busFeignClient;

/**
 * 实体[通讯总线] 服务对象接口实现
 */
@Slf4j
@Service
public class Bus_busServiceImpl implements IBus_busService {

    @Autowired
    bus_busFeignClient bus_busFeignClient;


    @Override
    public Bus_bus get(Integer id) {
		Bus_bus et=bus_busFeignClient.get(id);
        if(et==null){
            et=new Bus_bus();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Bus_bus et) {
        Bus_bus rt = bus_busFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Bus_bus> list){
        bus_busFeignClient.updateBatch(list) ;
    }

    @Override
    public Bus_bus getDraft(Bus_bus et) {
        et=bus_busFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=bus_busFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        bus_busFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Bus_bus et) {
        Bus_bus rt = bus_busFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Bus_bus> list){
        bus_busFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Bus_bus> searchDefault(Bus_busSearchContext context) {
        Page<Bus_bus> bus_buss=bus_busFeignClient.searchDefault(context);
        return bus_buss;
    }


}


