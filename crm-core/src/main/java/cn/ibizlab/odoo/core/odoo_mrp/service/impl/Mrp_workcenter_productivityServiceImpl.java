package cn.ibizlab.odoo.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenter_productivitySearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_workcenter_productivityService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mrp.client.mrp_workcenter_productivityFeignClient;

/**
 * 实体[工作中心生产力日志] 服务对象接口实现
 */
@Slf4j
@Service
public class Mrp_workcenter_productivityServiceImpl implements IMrp_workcenter_productivityService {

    @Autowired
    mrp_workcenter_productivityFeignClient mrp_workcenter_productivityFeignClient;


    @Override
    public boolean create(Mrp_workcenter_productivity et) {
        Mrp_workcenter_productivity rt = mrp_workcenter_productivityFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mrp_workcenter_productivity> list){
        mrp_workcenter_productivityFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Mrp_workcenter_productivity et) {
        Mrp_workcenter_productivity rt = mrp_workcenter_productivityFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mrp_workcenter_productivity> list){
        mrp_workcenter_productivityFeignClient.updateBatch(list) ;
    }

    @Override
    public Mrp_workcenter_productivity get(Integer id) {
		Mrp_workcenter_productivity et=mrp_workcenter_productivityFeignClient.get(id);
        if(et==null){
            et=new Mrp_workcenter_productivity();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mrp_workcenter_productivityFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mrp_workcenter_productivityFeignClient.removeBatch(idList);
    }

    @Override
    public Mrp_workcenter_productivity getDraft(Mrp_workcenter_productivity et) {
        et=mrp_workcenter_productivityFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mrp_workcenter_productivity> searchDefault(Mrp_workcenter_productivitySearchContext context) {
        Page<Mrp_workcenter_productivity> mrp_workcenter_productivitys=mrp_workcenter_productivityFeignClient.searchDefault(context);
        return mrp_workcenter_productivitys;
    }


}


