package cn.ibizlab.odoo.core.odoo_stock.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [库存路线] 对象
 */
@Data
public class Stock_location_route extends EntityClient implements Serializable {

    /**
     * 在销售订单行上可选
     */
    @DEField(name = "sale_selectable")
    @JSONField(name = "sale_selectable")
    @JsonProperty("sale_selectable")
    private String saleSelectable;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 产品
     */
    @JSONField(name = "product_ids")
    @JsonProperty("product_ids")
    private String productIds;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 路线
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 产品类别
     */
    @JSONField(name = "categ_ids")
    @JsonProperty("categ_ids")
    private String categIds;

    /**
     * 可应用于仓库
     */
    @DEField(name = "warehouse_selectable")
    @JSONField(name = "warehouse_selectable")
    @JsonProperty("warehouse_selectable")
    private String warehouseSelectable;

    /**
     * 序号
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 规则
     */
    @JSONField(name = "rule_ids")
    @JsonProperty("rule_ids")
    private String ruleIds;

    /**
     * 可应用于产品
     */
    @DEField(name = "product_selectable")
    @JSONField(name = "product_selectable")
    @JsonProperty("product_selectable")
    private String productSelectable;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 可应用于产品类别
     */
    @DEField(name = "product_categ_selectable")
    @JSONField(name = "product_categ_selectable")
    @JsonProperty("product_categ_selectable")
    private String productCategSelectable;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 仓库
     */
    @JSONField(name = "warehouse_ids")
    @JsonProperty("warehouse_ids")
    private String warehouseIds;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 供应的仓库
     */
    @JSONField(name = "supplied_wh_id_text")
    @JsonProperty("supplied_wh_id_text")
    private String suppliedWhIdText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 供应仓库
     */
    @JSONField(name = "supplier_wh_id_text")
    @JsonProperty("supplier_wh_id_text")
    private String supplierWhIdText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 供应仓库
     */
    @DEField(name = "supplier_wh_id")
    @JSONField(name = "supplier_wh_id")
    @JsonProperty("supplier_wh_id")
    private Integer supplierWhId;

    /**
     * 供应的仓库
     */
    @DEField(name = "supplied_wh_id")
    @JSONField(name = "supplied_wh_id")
    @JsonProperty("supplied_wh_id")
    private Integer suppliedWhId;


    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoosuppliedwh")
    @JsonProperty("odoosuppliedwh")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warehouse odooSuppliedWh;

    /**
     * 
     */
    @JSONField(name = "odoosupplierwh")
    @JsonProperty("odoosupplierwh")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warehouse odooSupplierWh;


    /**
     * 销售订单行
     */
    @JSONField(name = "sale_order_lines")
    @JsonProperty("sale_order_lines")
    private List<cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_line> saleOrderLine;



    /**
     * 设置 [在销售订单行上可选]
     */
    public void setSaleSelectable(String saleSelectable){
        this.saleSelectable = saleSelectable ;
        this.modify("sale_selectable",saleSelectable);
    }
    /**
     * 设置 [路线]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [可应用于仓库]
     */
    public void setWarehouseSelectable(String warehouseSelectable){
        this.warehouseSelectable = warehouseSelectable ;
        this.modify("warehouse_selectable",warehouseSelectable);
    }
    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }
    /**
     * 设置 [可应用于产品]
     */
    public void setProductSelectable(String productSelectable){
        this.productSelectable = productSelectable ;
        this.modify("product_selectable",productSelectable);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [可应用于产品类别]
     */
    public void setProductCategSelectable(String productCategSelectable){
        this.productCategSelectable = productCategSelectable ;
        this.modify("product_categ_selectable",productCategSelectable);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [供应仓库]
     */
    public void setSupplierWhId(Integer supplierWhId){
        this.supplierWhId = supplierWhId ;
        this.modify("supplier_wh_id",supplierWhId);
    }
    /**
     * 设置 [供应的仓库]
     */
    public void setSuppliedWhId(Integer suppliedWhId){
        this.suppliedWhId = suppliedWhId ;
        this.modify("supplied_wh_id",suppliedWhId);
    }

}


