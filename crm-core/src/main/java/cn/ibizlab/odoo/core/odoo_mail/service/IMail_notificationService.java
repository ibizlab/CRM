package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_notification;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_notificationSearchContext;


/**
 * 实体[Mail_notification] 服务对象接口
 */
public interface IMail_notificationService{

    Mail_notification getDraft(Mail_notification et) ;
    boolean update(Mail_notification et) ;
    void updateBatch(List<Mail_notification> list) ;
    boolean create(Mail_notification et) ;
    void createBatch(List<Mail_notification> list) ;
    Mail_notification get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Mail_notification> searchDefault(Mail_notificationSearchContext context) ;

}



