package cn.ibizlab.odoo.core.odoo_fleet.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_fuel;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_log_fuelSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[fleet_vehicle_log_fuel] 服务对象接口
 */
@Component
public class fleet_vehicle_log_fuelFallback implements fleet_vehicle_log_fuelFeignClient{


    public Fleet_vehicle_log_fuel update(Integer id, Fleet_vehicle_log_fuel fleet_vehicle_log_fuel){
            return null;
     }
    public Boolean updateBatch(List<Fleet_vehicle_log_fuel> fleet_vehicle_log_fuels){
            return false;
     }


    public Fleet_vehicle_log_fuel create(Fleet_vehicle_log_fuel fleet_vehicle_log_fuel){
            return null;
     }
    public Boolean createBatch(List<Fleet_vehicle_log_fuel> fleet_vehicle_log_fuels){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Fleet_vehicle_log_fuel get(Integer id){
            return null;
     }



    public Page<Fleet_vehicle_log_fuel> searchDefault(Fleet_vehicle_log_fuelSearchContext context){
            return null;
     }


    public Page<Fleet_vehicle_log_fuel> select(){
            return null;
     }

    public Fleet_vehicle_log_fuel getDraft(){
            return null;
    }



}
