package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [website_menu] 对象
 */
public interface Iwebsite_menu {

    /**
     * 获取 [下级菜单]
     */
    public void setChild_id(String child_id);
    
    /**
     * 设置 [下级菜单]
     */
    public String getChild_id();

    /**
     * 获取 [下级菜单]脏标记
     */
    public boolean getChild_idDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [可见]
     */
    public void setIs_visible(String is_visible);
    
    /**
     * 设置 [可见]
     */
    public String getIs_visible();

    /**
     * 获取 [可见]脏标记
     */
    public boolean getIs_visibleDirtyFlag();
    /**
     * 获取 [菜单]
     */
    public void setName(String name);
    
    /**
     * 设置 [菜单]
     */
    public String getName();

    /**
     * 获取 [菜单]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [新窗口]
     */
    public void setNew_window(String new_window);
    
    /**
     * 设置 [新窗口]
     */
    public String getNew_window();

    /**
     * 获取 [新窗口]脏标记
     */
    public boolean getNew_windowDirtyFlag();
    /**
     * 获取 [相关页面]
     */
    public void setPage_id(Integer page_id);
    
    /**
     * 设置 [相关页面]
     */
    public Integer getPage_id();

    /**
     * 获取 [相关页面]脏标记
     */
    public boolean getPage_idDirtyFlag();
    /**
     * 获取 [相关页面]
     */
    public void setPage_id_text(String page_id_text);
    
    /**
     * 设置 [相关页面]
     */
    public String getPage_id_text();

    /**
     * 获取 [相关页面]脏标记
     */
    public boolean getPage_id_textDirtyFlag();
    /**
     * 获取 [上级菜单]
     */
    public void setParent_id(Integer parent_id);
    
    /**
     * 设置 [上级菜单]
     */
    public Integer getParent_id();

    /**
     * 获取 [上级菜单]脏标记
     */
    public boolean getParent_idDirtyFlag();
    /**
     * 获取 [上级菜单]
     */
    public void setParent_id_text(String parent_id_text);
    
    /**
     * 设置 [上级菜单]
     */
    public String getParent_id_text();

    /**
     * 获取 [上级菜单]脏标记
     */
    public boolean getParent_id_textDirtyFlag();
    /**
     * 获取 [上级路径]
     */
    public void setParent_path(String parent_path);
    
    /**
     * 设置 [上级路径]
     */
    public String getParent_path();

    /**
     * 获取 [上级路径]脏标记
     */
    public boolean getParent_pathDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [主题模板]
     */
    public void setTheme_template_id(Integer theme_template_id);
    
    /**
     * 设置 [主题模板]
     */
    public Integer getTheme_template_id();

    /**
     * 获取 [主题模板]脏标记
     */
    public boolean getTheme_template_idDirtyFlag();
    /**
     * 获取 [Url]
     */
    public void setUrl(String url);
    
    /**
     * 设置 [Url]
     */
    public String getUrl();

    /**
     * 获取 [Url]脏标记
     */
    public boolean getUrlDirtyFlag();
    /**
     * 获取 [网站]
     */
    public void setWebsite_id(Integer website_id);
    
    /**
     * 设置 [网站]
     */
    public Integer getWebsite_id();

    /**
     * 获取 [网站]脏标记
     */
    public boolean getWebsite_idDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
