package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [maintenance_team] 对象
 */
public interface Imaintenance_team {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [颜色索引]
     */
    public void setColor(Integer color);
    
    /**
     * 设置 [颜色索引]
     */
    public Integer getColor();

    /**
     * 获取 [颜色索引]脏标记
     */
    public boolean getColorDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [设备]
     */
    public void setEquipment_ids(String equipment_ids);
    
    /**
     * 设置 [设备]
     */
    public String getEquipment_ids();

    /**
     * 获取 [设备]脏标记
     */
    public boolean getEquipment_idsDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [团队成员]
     */
    public void setMember_ids(String member_ids);
    
    /**
     * 设置 [团队成员]
     */
    public String getMember_ids();

    /**
     * 获取 [团队成员]脏标记
     */
    public boolean getMember_idsDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [请求]
     */
    public void setRequest_ids(String request_ids);
    
    /**
     * 设置 [请求]
     */
    public String getRequest_ids();

    /**
     * 获取 [请求]脏标记
     */
    public boolean getRequest_idsDirtyFlag();
    /**
     * 获取 [请求数量]
     */
    public void setTodo_request_count(Integer todo_request_count);
    
    /**
     * 设置 [请求数量]
     */
    public Integer getTodo_request_count();

    /**
     * 获取 [请求数量]脏标记
     */
    public boolean getTodo_request_countDirtyFlag();
    /**
     * 获取 [已阻止请求的数量]
     */
    public void setTodo_request_count_block(Integer todo_request_count_block);
    
    /**
     * 设置 [已阻止请求的数量]
     */
    public Integer getTodo_request_count_block();

    /**
     * 获取 [已阻止请求的数量]脏标记
     */
    public boolean getTodo_request_count_blockDirtyFlag();
    /**
     * 获取 [已计划请求的数量]
     */
    public void setTodo_request_count_date(Integer todo_request_count_date);
    
    /**
     * 设置 [已计划请求的数量]
     */
    public Integer getTodo_request_count_date();

    /**
     * 获取 [已计划请求的数量]脏标记
     */
    public boolean getTodo_request_count_dateDirtyFlag();
    /**
     * 获取 [高优先级的请求数量]
     */
    public void setTodo_request_count_high_priority(Integer todo_request_count_high_priority);
    
    /**
     * 设置 [高优先级的请求数量]
     */
    public Integer getTodo_request_count_high_priority();

    /**
     * 获取 [高优先级的请求数量]脏标记
     */
    public boolean getTodo_request_count_high_priorityDirtyFlag();
    /**
     * 获取 [未计划请求的数量]
     */
    public void setTodo_request_count_unscheduled(Integer todo_request_count_unscheduled);
    
    /**
     * 设置 [未计划请求的数量]
     */
    public Integer getTodo_request_count_unscheduled();

    /**
     * 获取 [未计划请求的数量]脏标记
     */
    public boolean getTodo_request_count_unscheduledDirtyFlag();
    /**
     * 获取 [请求]
     */
    public void setTodo_request_ids(String todo_request_ids);
    
    /**
     * 设置 [请求]
     */
    public String getTodo_request_ids();

    /**
     * 获取 [请求]脏标记
     */
    public boolean getTodo_request_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
