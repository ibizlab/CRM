package cn.ibizlab.odoo.core.odoo_portal.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_share;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_shareSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[portal_share] 服务对象接口
 */
@FeignClient(value = "odoo-portal", contextId = "portal-share", fallback = portal_shareFallback.class)
public interface portal_shareFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/portal_shares")
    Portal_share create(@RequestBody Portal_share portal_share);

    @RequestMapping(method = RequestMethod.POST, value = "/portal_shares/batch")
    Boolean createBatch(@RequestBody List<Portal_share> portal_shares);


    @RequestMapping(method = RequestMethod.GET, value = "/portal_shares/{id}")
    Portal_share get(@PathVariable("id") Integer id);





    @RequestMapping(method = RequestMethod.PUT, value = "/portal_shares/{id}")
    Portal_share update(@PathVariable("id") Integer id,@RequestBody Portal_share portal_share);

    @RequestMapping(method = RequestMethod.PUT, value = "/portal_shares/batch")
    Boolean updateBatch(@RequestBody List<Portal_share> portal_shares);


    @RequestMapping(method = RequestMethod.DELETE, value = "/portal_shares/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/portal_shares/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/portal_shares/searchdefault")
    Page<Portal_share> searchDefault(@RequestBody Portal_shareSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/portal_shares/select")
    Page<Portal_share> select();


    @RequestMapping(method = RequestMethod.GET, value = "/portal_shares/getdraft")
    Portal_share getDraft();


}
