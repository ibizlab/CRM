package cn.ibizlab.odoo.core.odoo_mail.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.odoo.util.filter.SearchContextBase;

/**
 * ServiceApi数据实体[Mail_activity] 查询条件对象
 */
@Slf4j
@Data
public class Mail_activitySearchContext extends SearchContextBase {
	private String n_display_name_like;//[显示名称]

	private String n_state_eq;//[状态]

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Timestamp n_create_date_gtandeq;//[创建时间]

    @JsonFormat(pattern="yyyy-MM-dd", timezone="GMT+8")
    @JSONField(format="yyyy-MM-dd")
	private Timestamp n_date_deadline_ltandeq;//[到期时间]

	private String n_user_id_text_eq;//[分派给]

	private String n_user_id_text_like;//[分派给]

	private String n_activity_type_id_text_eq;//[活动]

	private String n_activity_type_id_text_like;//[活动]

	private String n_previous_activity_type_id_text_eq;//[前一活动类型]

	private String n_previous_activity_type_id_text_like;//[前一活动类型]

	private String n_note_id_text_eq;//[相关便签]

	private String n_note_id_text_like;//[相关便签]

	private String n_create_user_id_text_eq;//[建立者]

	private String n_create_user_id_text_like;//[建立者]

	private String n_create_uid_text_eq;//[创建人]

	private String n_create_uid_text_like;//[创建人]

	private String n_write_uid_text_eq;//[最后更新者]

	private String n_write_uid_text_like;//[最后更新者]

	private String n_recommended_activity_type_id_text_eq;//[推荐的活动类型]

	private String n_recommended_activity_type_id_text_like;//[推荐的活动类型]

	private String n_calendar_event_id_text_eq;//[日历会议]

	private String n_calendar_event_id_text_like;//[日历会议]

	private Integer n_recommended_activity_type_id_eq;//[推荐的活动类型]

	private Integer n_activity_type_id_eq;//[活动]

	private Integer n_create_user_id_eq;//[建立者]

	private Integer n_create_uid_eq;//[创建人]

	private Integer n_user_id_eq;//[分派给]

	private Integer n_note_id_eq;//[相关便签]

	private Integer n_previous_activity_type_id_eq;//[前一活动类型]

	private Integer n_calendar_event_id_eq;//[日历会议]

	private Integer n_write_uid_eq;//[最后更新者]

}



