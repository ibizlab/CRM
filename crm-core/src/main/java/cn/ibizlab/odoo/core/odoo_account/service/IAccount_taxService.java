package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_tax;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_taxSearchContext;


/**
 * 实体[Account_tax] 服务对象接口
 */
public interface IAccount_taxService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Account_tax et) ;
    void updateBatch(List<Account_tax> list) ;
    boolean create(Account_tax et) ;
    void createBatch(List<Account_tax> list) ;
    Account_tax get(Integer key) ;
    Account_tax getDraft(Account_tax et) ;
    Page<Account_tax> searchDefault(Account_taxSearchContext context) ;

}



