package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_channel_partner;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_channel_partnerSearchContext;


/**
 * 实体[Mail_channel_partner] 服务对象接口
 */
public interface IMail_channel_partnerService{

    Mail_channel_partner getDraft(Mail_channel_partner et) ;
    boolean create(Mail_channel_partner et) ;
    void createBatch(List<Mail_channel_partner> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Mail_channel_partner get(Integer key) ;
    boolean update(Mail_channel_partner et) ;
    void updateBatch(List<Mail_channel_partner> list) ;
    Page<Mail_channel_partner> searchDefault(Mail_channel_partnerSearchContext context) ;

}



