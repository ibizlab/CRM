package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_fixed_putaway_strat;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_fixed_putaway_stratSearchContext;


/**
 * 实体[Stock_fixed_putaway_strat] 服务对象接口
 */
public interface IStock_fixed_putaway_stratService{

    Stock_fixed_putaway_strat getDraft(Stock_fixed_putaway_strat et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Stock_fixed_putaway_strat get(Integer key) ;
    boolean update(Stock_fixed_putaway_strat et) ;
    void updateBatch(List<Stock_fixed_putaway_strat> list) ;
    boolean create(Stock_fixed_putaway_strat et) ;
    void createBatch(List<Stock_fixed_putaway_strat> list) ;
    Page<Stock_fixed_putaway_strat> searchDefault(Stock_fixed_putaway_stratSearchContext context) ;

}



