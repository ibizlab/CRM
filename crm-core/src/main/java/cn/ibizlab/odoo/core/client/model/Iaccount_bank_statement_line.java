package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [account_bank_statement_line] 对象
 */
public interface Iaccount_bank_statement_line {

    /**
     * 获取 [对方科目]
     */
    public void setAccount_id(Integer account_id);
    
    /**
     * 设置 [对方科目]
     */
    public Integer getAccount_id();

    /**
     * 获取 [对方科目]脏标记
     */
    public boolean getAccount_idDirtyFlag();
    /**
     * 获取 [对方科目]
     */
    public void setAccount_id_text(String account_id_text);
    
    /**
     * 设置 [对方科目]
     */
    public String getAccount_id_text();

    /**
     * 获取 [对方科目]脏标记
     */
    public boolean getAccount_id_textDirtyFlag();
    /**
     * 获取 [银行账户号码]
     */
    public void setAccount_number(String account_number);
    
    /**
     * 设置 [银行账户号码]
     */
    public String getAccount_number();

    /**
     * 获取 [银行账户号码]脏标记
     */
    public boolean getAccount_numberDirtyFlag();
    /**
     * 获取 [金额]
     */
    public void setAmount(Double amount);
    
    /**
     * 设置 [金额]
     */
    public Double getAmount();

    /**
     * 获取 [金额]脏标记
     */
    public boolean getAmountDirtyFlag();
    /**
     * 获取 [货币金额]
     */
    public void setAmount_currency(Double amount_currency);
    
    /**
     * 设置 [货币金额]
     */
    public Double getAmount_currency();

    /**
     * 获取 [货币金额]脏标记
     */
    public boolean getAmount_currencyDirtyFlag();
    /**
     * 获取 [银行账户]
     */
    public void setBank_account_id(Integer bank_account_id);
    
    /**
     * 设置 [银行账户]
     */
    public Integer getBank_account_id();

    /**
     * 获取 [银行账户]脏标记
     */
    public boolean getBank_account_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id_text(String currency_id_text);
    
    /**
     * 设置 [币种]
     */
    public String getCurrency_id_text();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_id_textDirtyFlag();
    /**
     * 获取 [日期]
     */
    public void setDate(Timestamp date);
    
    /**
     * 设置 [日期]
     */
    public Timestamp getDate();

    /**
     * 获取 [日期]脏标记
     */
    public boolean getDateDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [现金分类账]
     */
    public void setJournal_currency_id(Integer journal_currency_id);
    
    /**
     * 设置 [现金分类账]
     */
    public Integer getJournal_currency_id();

    /**
     * 获取 [现金分类账]脏标记
     */
    public boolean getJournal_currency_idDirtyFlag();
    /**
     * 获取 [日记账项目]
     */
    public void setJournal_entry_ids(String journal_entry_ids);
    
    /**
     * 设置 [日记账项目]
     */
    public String getJournal_entry_ids();

    /**
     * 获取 [日记账项目]脏标记
     */
    public boolean getJournal_entry_idsDirtyFlag();
    /**
     * 获取 [日记账]
     */
    public void setJournal_id(Integer journal_id);
    
    /**
     * 设置 [日记账]
     */
    public Integer getJournal_id();

    /**
     * 获取 [日记账]脏标记
     */
    public boolean getJournal_idDirtyFlag();
    /**
     * 获取 [日记账]
     */
    public void setJournal_id_text(String journal_id_text);
    
    /**
     * 设置 [日记账]
     */
    public String getJournal_id_text();

    /**
     * 获取 [日记账]脏标记
     */
    public boolean getJournal_id_textDirtyFlag();
    /**
     * 获取 [日记账分录名称]
     */
    public void setMove_name(String move_name);
    
    /**
     * 设置 [日记账分录名称]
     */
    public String getMove_name();

    /**
     * 获取 [日记账分录名称]脏标记
     */
    public boolean getMove_nameDirtyFlag();
    /**
     * 获取 [标签]
     */
    public void setName(String name);
    
    /**
     * 设置 [标签]
     */
    public String getName();

    /**
     * 获取 [标签]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [备注]
     */
    public void setNote(String note);
    
    /**
     * 设置 [备注]
     */
    public String getNote();

    /**
     * 获取 [备注]脏标记
     */
    public boolean getNoteDirtyFlag();
    /**
     * 获取 [业务伙伴]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [业务伙伴]
     */
    public Integer getPartner_id();

    /**
     * 获取 [业务伙伴]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [业务伙伴]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [业务伙伴]
     */
    public String getPartner_id_text();

    /**
     * 获取 [业务伙伴]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [合作伙伴名称]
     */
    public void setPartner_name(String partner_name);
    
    /**
     * 设置 [合作伙伴名称]
     */
    public String getPartner_name();

    /**
     * 获取 [合作伙伴名称]脏标记
     */
    public boolean getPartner_nameDirtyFlag();
    /**
     * 获取 [POS报表]
     */
    public void setPos_statement_id(Integer pos_statement_id);
    
    /**
     * 设置 [POS报表]
     */
    public Integer getPos_statement_id();

    /**
     * 获取 [POS报表]脏标记
     */
    public boolean getPos_statement_idDirtyFlag();
    /**
     * 获取 [参考]
     */
    public void setRef(String ref);
    
    /**
     * 设置 [参考]
     */
    public String getRef();

    /**
     * 获取 [参考]脏标记
     */
    public boolean getRefDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [报告]
     */
    public void setStatement_id(Integer statement_id);
    
    /**
     * 设置 [报告]
     */
    public Integer getStatement_id();

    /**
     * 获取 [报告]脏标记
     */
    public boolean getStatement_idDirtyFlag();
    /**
     * 获取 [报告]
     */
    public void setStatement_id_text(String statement_id_text);
    
    /**
     * 设置 [报告]
     */
    public String getStatement_id_text();

    /**
     * 获取 [报告]脏标记
     */
    public boolean getStatement_id_textDirtyFlag();
    /**
     * 获取 [导入ID]
     */
    public void setUnique_import_id(String unique_import_id);
    
    /**
     * 设置 [导入ID]
     */
    public String getUnique_import_id();

    /**
     * 获取 [导入ID]脏标记
     */
    public boolean getUnique_import_idDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
