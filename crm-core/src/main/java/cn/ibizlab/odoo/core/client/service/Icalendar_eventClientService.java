package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Icalendar_event;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[calendar_event] 服务对象接口
 */
public interface Icalendar_eventClientService{

    public Icalendar_event createModel() ;

    public void get(Icalendar_event calendar_event);

    public Page<Icalendar_event> fetchDefault(SearchContext context);

    public void update(Icalendar_event calendar_event);

    public void createBatch(List<Icalendar_event> calendar_events);

    public void updateBatch(List<Icalendar_event> calendar_events);

    public void removeBatch(List<Icalendar_event> calendar_events);

    public void create(Icalendar_event calendar_event);

    public void remove(Icalendar_event calendar_event);

    public Page<Icalendar_event> select(SearchContext context);

    public void getDraft(Icalendar_event calendar_event);

    public void save(Icalendar_event calendar_event);

    public void checkKey(Icalendar_event calendar_event);

}
