package cn.ibizlab.odoo.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_pricelist;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_pricelistSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[product_pricelist] 服务对象接口
 */
@Component
public class product_pricelistFallback implements product_pricelistFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Product_pricelist get(Integer id){
            return null;
     }


    public Page<Product_pricelist> searchDefault(Product_pricelistSearchContext context){
            return null;
     }





    public Product_pricelist create(Product_pricelist product_pricelist){
            return null;
     }
    public Boolean createBatch(List<Product_pricelist> product_pricelists){
            return false;
     }

    public Product_pricelist update(Integer id, Product_pricelist product_pricelist){
            return null;
     }
    public Boolean updateBatch(List<Product_pricelist> product_pricelists){
            return false;
     }


    public Page<Product_pricelist> select(){
            return null;
     }

    public Boolean save(Product_pricelist product_pricelist){
            return false;
     }
    public Boolean saveBatch(List<Product_pricelist> product_pricelists){
            return false;
     }

    public Product_pricelist getDraft(){
            return null;
    }



    public Boolean checkKey(Product_pricelist product_pricelist){
            return false;
     }


}
