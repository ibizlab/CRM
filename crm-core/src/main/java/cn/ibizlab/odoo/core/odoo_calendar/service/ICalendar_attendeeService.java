package cn.ibizlab.odoo.core.odoo_calendar.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_attendee;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_attendeeSearchContext;


/**
 * 实体[Calendar_attendee] 服务对象接口
 */
public interface ICalendar_attendeeService{

    Calendar_attendee getDraft(Calendar_attendee et) ;
    boolean update(Calendar_attendee et) ;
    void updateBatch(List<Calendar_attendee> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Calendar_attendee et) ;
    void createBatch(List<Calendar_attendee> list) ;
    Calendar_attendee get(Integer key) ;
    Page<Calendar_attendee> searchDefault(Calendar_attendeeSearchContext context) ;

}



