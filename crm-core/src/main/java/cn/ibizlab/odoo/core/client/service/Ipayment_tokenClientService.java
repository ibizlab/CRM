package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ipayment_token;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[payment_token] 服务对象接口
 */
public interface Ipayment_tokenClientService{

    public Ipayment_token createModel() ;

    public void get(Ipayment_token payment_token);

    public void updateBatch(List<Ipayment_token> payment_tokens);

    public void remove(Ipayment_token payment_token);

    public void createBatch(List<Ipayment_token> payment_tokens);

    public void create(Ipayment_token payment_token);

    public void removeBatch(List<Ipayment_token> payment_tokens);

    public Page<Ipayment_token> fetchDefault(SearchContext context);

    public void update(Ipayment_token payment_token);

    public Page<Ipayment_token> select(SearchContext context);

    public void getDraft(Ipayment_token payment_token);

}
