package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_m2o_related;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_import_tests_models_m2o_related] 服务对象接口
 */
public interface Ibase_import_tests_models_m2o_relatedClientService{

    public Ibase_import_tests_models_m2o_related createModel() ;

    public Page<Ibase_import_tests_models_m2o_related> fetchDefault(SearchContext context);

    public void removeBatch(List<Ibase_import_tests_models_m2o_related> base_import_tests_models_m2o_relateds);

    public void updateBatch(List<Ibase_import_tests_models_m2o_related> base_import_tests_models_m2o_relateds);

    public void createBatch(List<Ibase_import_tests_models_m2o_related> base_import_tests_models_m2o_relateds);

    public void update(Ibase_import_tests_models_m2o_related base_import_tests_models_m2o_related);

    public void remove(Ibase_import_tests_models_m2o_related base_import_tests_models_m2o_related);

    public void create(Ibase_import_tests_models_m2o_related base_import_tests_models_m2o_related);

    public void get(Ibase_import_tests_models_m2o_related base_import_tests_models_m2o_related);

    public Page<Ibase_import_tests_models_m2o_related> select(SearchContext context);

    public void getDraft(Ibase_import_tests_models_m2o_related base_import_tests_models_m2o_related);

}
