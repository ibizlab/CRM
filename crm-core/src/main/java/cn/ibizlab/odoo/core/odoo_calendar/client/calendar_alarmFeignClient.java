package cn.ibizlab.odoo.core.odoo_calendar.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_alarm;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_alarmSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[calendar_alarm] 服务对象接口
 */
@FeignClient(value = "odoo-calendar", contextId = "calendar-alarm", fallback = calendar_alarmFallback.class)
public interface calendar_alarmFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/calendar_alarms")
    Calendar_alarm create(@RequestBody Calendar_alarm calendar_alarm);

    @RequestMapping(method = RequestMethod.POST, value = "/calendar_alarms/batch")
    Boolean createBatch(@RequestBody List<Calendar_alarm> calendar_alarms);


    @RequestMapping(method = RequestMethod.PUT, value = "/calendar_alarms/{id}")
    Calendar_alarm update(@PathVariable("id") Integer id,@RequestBody Calendar_alarm calendar_alarm);

    @RequestMapping(method = RequestMethod.PUT, value = "/calendar_alarms/batch")
    Boolean updateBatch(@RequestBody List<Calendar_alarm> calendar_alarms);


    @RequestMapping(method = RequestMethod.DELETE, value = "/calendar_alarms/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/calendar_alarms/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);




    @RequestMapping(method = RequestMethod.POST, value = "/calendar_alarms/searchdefault")
    Page<Calendar_alarm> searchDefault(@RequestBody Calendar_alarmSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/calendar_alarms/{id}")
    Calendar_alarm get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/calendar_alarms/select")
    Page<Calendar_alarm> select();


    @RequestMapping(method = RequestMethod.GET, value = "/calendar_alarms/getdraft")
    Calendar_alarm getDraft();


}
