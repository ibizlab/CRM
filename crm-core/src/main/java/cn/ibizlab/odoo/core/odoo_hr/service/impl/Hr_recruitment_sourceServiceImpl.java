package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_recruitment_source;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_recruitment_sourceSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_recruitment_sourceService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_hr.client.hr_recruitment_sourceFeignClient;

/**
 * 实体[应聘者来源] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_recruitment_sourceServiceImpl implements IHr_recruitment_sourceService {

    @Autowired
    hr_recruitment_sourceFeignClient hr_recruitment_sourceFeignClient;


    @Override
    public Hr_recruitment_source get(Integer id) {
		Hr_recruitment_source et=hr_recruitment_sourceFeignClient.get(id);
        if(et==null){
            et=new Hr_recruitment_source();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Hr_recruitment_source getDraft(Hr_recruitment_source et) {
        et=hr_recruitment_sourceFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Hr_recruitment_source et) {
        Hr_recruitment_source rt = hr_recruitment_sourceFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Hr_recruitment_source> list){
        hr_recruitment_sourceFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Hr_recruitment_source et) {
        Hr_recruitment_source rt = hr_recruitment_sourceFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_recruitment_source> list){
        hr_recruitment_sourceFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=hr_recruitment_sourceFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        hr_recruitment_sourceFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_recruitment_source> searchDefault(Hr_recruitment_sourceSearchContext context) {
        Page<Hr_recruitment_source> hr_recruitment_sources=hr_recruitment_sourceFeignClient.searchDefault(context);
        return hr_recruitment_sources;
    }


}


