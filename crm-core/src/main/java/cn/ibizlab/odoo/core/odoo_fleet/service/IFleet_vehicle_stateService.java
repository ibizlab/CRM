package cn.ibizlab.odoo.core.odoo_fleet.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_state;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_stateSearchContext;


/**
 * 实体[Fleet_vehicle_state] 服务对象接口
 */
public interface IFleet_vehicle_stateService{

    Fleet_vehicle_state get(Integer key) ;
    boolean create(Fleet_vehicle_state et) ;
    void createBatch(List<Fleet_vehicle_state> list) ;
    boolean update(Fleet_vehicle_state et) ;
    void updateBatch(List<Fleet_vehicle_state> list) ;
    Fleet_vehicle_state getDraft(Fleet_vehicle_state et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Fleet_vehicle_state> searchDefault(Fleet_vehicle_stateSearchContext context) ;

}



