package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_country;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_countrySearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_countryService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.res_countryFeignClient;

/**
 * 实体[国家/地区] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_countryServiceImpl implements IRes_countryService {

    @Autowired
    res_countryFeignClient res_countryFeignClient;


    @Override
    public Res_country getDraft(Res_country et) {
        et=res_countryFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Res_country et) {
        Res_country rt = res_countryFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Res_country> list){
        res_countryFeignClient.updateBatch(list) ;
    }

    @Override
    public Res_country get(Integer id) {
		Res_country et=res_countryFeignClient.get(id);
        if(et==null){
            et=new Res_country();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Res_country et) {
        Res_country rt = res_countryFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_country> list){
        res_countryFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=res_countryFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        res_countryFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_country> searchDefault(Res_countrySearchContext context) {
        Page<Res_country> res_countrys=res_countryFeignClient.searchDefault(context);
        return res_countrys;
    }


}


