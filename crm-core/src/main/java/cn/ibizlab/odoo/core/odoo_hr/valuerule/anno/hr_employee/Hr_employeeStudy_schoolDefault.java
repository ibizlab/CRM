package cn.ibizlab.odoo.core.odoo_hr.valuerule.anno.hr_employee;

import cn.ibizlab.odoo.core.odoo_hr.valuerule.validator.hr_employee.Hr_employeeStudy_schoolDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Hr_employee
 * 属性：Study_school
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Hr_employeeStudy_schoolDefaultValidator.class})
public @interface Hr_employeeStudy_schoolDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[100]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
