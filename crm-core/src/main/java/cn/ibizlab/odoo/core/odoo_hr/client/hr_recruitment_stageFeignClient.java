package cn.ibizlab.odoo.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_recruitment_stage;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_recruitment_stageSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[hr_recruitment_stage] 服务对象接口
 */
@FeignClient(value = "odoo-hr", contextId = "hr-recruitment-stage", fallback = hr_recruitment_stageFallback.class)
public interface hr_recruitment_stageFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_stages/searchdefault")
    Page<Hr_recruitment_stage> searchDefault(@RequestBody Hr_recruitment_stageSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_stages")
    Hr_recruitment_stage create(@RequestBody Hr_recruitment_stage hr_recruitment_stage);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_stages/batch")
    Boolean createBatch(@RequestBody List<Hr_recruitment_stage> hr_recruitment_stages);


    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_recruitment_stages/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_recruitment_stages/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/hr_recruitment_stages/{id}")
    Hr_recruitment_stage get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.PUT, value = "/hr_recruitment_stages/{id}")
    Hr_recruitment_stage update(@PathVariable("id") Integer id,@RequestBody Hr_recruitment_stage hr_recruitment_stage);

    @RequestMapping(method = RequestMethod.PUT, value = "/hr_recruitment_stages/batch")
    Boolean updateBatch(@RequestBody List<Hr_recruitment_stage> hr_recruitment_stages);



    @RequestMapping(method = RequestMethod.GET, value = "/hr_recruitment_stages/select")
    Page<Hr_recruitment_stage> select();


    @RequestMapping(method = RequestMethod.GET, value = "/hr_recruitment_stages/getdraft")
    Hr_recruitment_stage getDraft();


}
