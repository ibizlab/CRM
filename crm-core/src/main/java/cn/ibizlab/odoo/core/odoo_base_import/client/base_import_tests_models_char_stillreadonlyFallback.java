package cn.ibizlab.odoo.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_char_stillreadonly;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_char_stillreadonlySearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[base_import_tests_models_char_stillreadonly] 服务对象接口
 */
@Component
public class base_import_tests_models_char_stillreadonlyFallback implements base_import_tests_models_char_stillreadonlyFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Base_import_tests_models_char_stillreadonly get(Integer id){
            return null;
     }


    public Base_import_tests_models_char_stillreadonly create(Base_import_tests_models_char_stillreadonly base_import_tests_models_char_stillreadonly){
            return null;
     }
    public Boolean createBatch(List<Base_import_tests_models_char_stillreadonly> base_import_tests_models_char_stillreadonlies){
            return false;
     }



    public Page<Base_import_tests_models_char_stillreadonly> searchDefault(Base_import_tests_models_char_stillreadonlySearchContext context){
            return null;
     }



    public Base_import_tests_models_char_stillreadonly update(Integer id, Base_import_tests_models_char_stillreadonly base_import_tests_models_char_stillreadonly){
            return null;
     }
    public Boolean updateBatch(List<Base_import_tests_models_char_stillreadonly> base_import_tests_models_char_stillreadonlies){
            return false;
     }


    public Page<Base_import_tests_models_char_stillreadonly> select(){
            return null;
     }

    public Base_import_tests_models_char_stillreadonly getDraft(){
            return null;
    }



}
