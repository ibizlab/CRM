package cn.ibizlab.odoo.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_replan;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_replanSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mro_pm_replan] 服务对象接口
 */
@Component
public class mro_pm_replanFallback implements mro_pm_replanFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Mro_pm_replan update(Integer id, Mro_pm_replan mro_pm_replan){
            return null;
     }
    public Boolean updateBatch(List<Mro_pm_replan> mro_pm_replans){
            return false;
     }


    public Mro_pm_replan get(Integer id){
            return null;
     }



    public Page<Mro_pm_replan> searchDefault(Mro_pm_replanSearchContext context){
            return null;
     }




    public Mro_pm_replan create(Mro_pm_replan mro_pm_replan){
            return null;
     }
    public Boolean createBatch(List<Mro_pm_replan> mro_pm_replans){
            return false;
     }

    public Page<Mro_pm_replan> select(){
            return null;
     }

    public Mro_pm_replan getDraft(){
            return null;
    }



}
