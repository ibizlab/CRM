package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_account_type;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_account_type] 服务对象接口
 */
public interface Iaccount_account_typeClientService{

    public Iaccount_account_type createModel() ;

    public void createBatch(List<Iaccount_account_type> account_account_types);

    public void removeBatch(List<Iaccount_account_type> account_account_types);

    public void create(Iaccount_account_type account_account_type);

    public Page<Iaccount_account_type> fetchDefault(SearchContext context);

    public void updateBatch(List<Iaccount_account_type> account_account_types);

    public void remove(Iaccount_account_type account_account_type);

    public void update(Iaccount_account_type account_account_type);

    public void get(Iaccount_account_type account_account_type);

    public Page<Iaccount_account_type> select(SearchContext context);

    public void getDraft(Iaccount_account_type account_account_type);

}
