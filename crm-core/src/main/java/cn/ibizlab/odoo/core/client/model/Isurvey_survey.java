package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [survey_survey] 对象
 */
public interface Isurvey_survey {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [下一活动截止日期]
     */
    public void setActivity_date_deadline(Timestamp activity_date_deadline);
    
    /**
     * 设置 [下一活动截止日期]
     */
    public Timestamp getActivity_date_deadline();

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    public boolean getActivity_date_deadlineDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setActivity_ids(String activity_ids);
    
    /**
     * 设置 [活动]
     */
    public String getActivity_ids();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getActivity_idsDirtyFlag();
    /**
     * 获取 [活动状态]
     */
    public void setActivity_state(String activity_state);
    
    /**
     * 设置 [活动状态]
     */
    public String getActivity_state();

    /**
     * 获取 [活动状态]脏标记
     */
    public boolean getActivity_stateDirtyFlag();
    /**
     * 获取 [下一活动摘要]
     */
    public void setActivity_summary(String activity_summary);
    
    /**
     * 设置 [下一活动摘要]
     */
    public String getActivity_summary();

    /**
     * 获取 [下一活动摘要]脏标记
     */
    public boolean getActivity_summaryDirtyFlag();
    /**
     * 获取 [下一活动类型]
     */
    public void setActivity_type_id(Integer activity_type_id);
    
    /**
     * 设置 [下一活动类型]
     */
    public Integer getActivity_type_id();

    /**
     * 获取 [下一活动类型]脏标记
     */
    public boolean getActivity_type_idDirtyFlag();
    /**
     * 获取 [责任用户]
     */
    public void setActivity_user_id(Integer activity_user_id);
    
    /**
     * 设置 [责任用户]
     */
    public Integer getActivity_user_id();

    /**
     * 获取 [责任用户]脏标记
     */
    public boolean getActivity_user_idDirtyFlag();
    /**
     * 获取 [需要登录]
     */
    public void setAuth_required(String auth_required);
    
    /**
     * 设置 [需要登录]
     */
    public String getAuth_required();

    /**
     * 获取 [需要登录]脏标记
     */
    public boolean getAuth_requiredDirtyFlag();
    /**
     * 获取 [颜色索引]
     */
    public void setColor(Integer color);
    
    /**
     * 设置 [颜色索引]
     */
    public Integer getColor();

    /**
     * 获取 [颜色索引]脏标记
     */
    public boolean getColorDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [说明]
     */
    public void setDescription(String description);
    
    /**
     * 设置 [说明]
     */
    public String getDescription();

    /**
     * 获取 [说明]脏标记
     */
    public boolean getDescriptionDirtyFlag();
    /**
     * 获取 [已经设计了]
     */
    public void setDesigned(String designed);
    
    /**
     * 设置 [已经设计了]
     */
    public String getDesigned();

    /**
     * 获取 [已经设计了]脏标记
     */
    public boolean getDesignedDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [邮件模板]
     */
    public void setEmail_template_id(Integer email_template_id);
    
    /**
     * 设置 [邮件模板]
     */
    public Integer getEmail_template_id();

    /**
     * 获取 [邮件模板]脏标记
     */
    public boolean getEmail_template_idDirtyFlag();
    /**
     * 获取 [邮件模板]
     */
    public void setEmail_template_id_text(String email_template_id_text);
    
    /**
     * 设置 [邮件模板]
     */
    public String getEmail_template_id_text();

    /**
     * 获取 [邮件模板]脏标记
     */
    public boolean getEmail_template_id_textDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [已关闭]
     */
    public void setIs_closed(String is_closed);
    
    /**
     * 设置 [已关闭]
     */
    public String getIs_closed();

    /**
     * 获取 [已关闭]脏标记
     */
    public boolean getIs_closedDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [是关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [是关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [是关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [采取行动]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [采取行动]
     */
    public String getMessage_needaction();

    /**
     * 获取 [采取行动]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [行动数量]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [行动数量]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [行动数量]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [页面]
     */
    public void setPage_ids(String page_ids);
    
    /**
     * 设置 [页面]
     */
    public String getPage_ids();

    /**
     * 获取 [页面]脏标记
     */
    public boolean getPage_idsDirtyFlag();
    /**
     * 获取 [打印链接]
     */
    public void setPrint_url(String print_url);
    
    /**
     * 设置 [打印链接]
     */
    public String getPrint_url();

    /**
     * 获取 [打印链接]脏标记
     */
    public boolean getPrint_urlDirtyFlag();
    /**
     * 获取 [公开链接]
     */
    public void setPublic_url(String public_url);
    
    /**
     * 设置 [公开链接]
     */
    public String getPublic_url();

    /**
     * 获取 [公开链接]脏标记
     */
    public boolean getPublic_urlDirtyFlag();
    /**
     * 获取 [公开链接（HTML版）]
     */
    public void setPublic_url_html(String public_url_html);
    
    /**
     * 设置 [公开链接（HTML版）]
     */
    public String getPublic_url_html();

    /**
     * 获取 [公开链接（HTML版）]脏标记
     */
    public boolean getPublic_url_htmlDirtyFlag();
    /**
     * 获取 [测验模式]
     */
    public void setQuizz_mode(String quizz_mode);
    
    /**
     * 设置 [测验模式]
     */
    public String getQuizz_mode();

    /**
     * 获取 [测验模式]脏标记
     */
    public boolean getQuizz_modeDirtyFlag();
    /**
     * 获取 [结果链接]
     */
    public void setResult_url(String result_url);
    
    /**
     * 设置 [结果链接]
     */
    public String getResult_url();

    /**
     * 获取 [结果链接]脏标记
     */
    public boolean getResult_urlDirtyFlag();
    /**
     * 获取 [阶段]
     */
    public void setStage_id(Integer stage_id);
    
    /**
     * 设置 [阶段]
     */
    public Integer getStage_id();

    /**
     * 获取 [阶段]脏标记
     */
    public boolean getStage_idDirtyFlag();
    /**
     * 获取 [阶段]
     */
    public void setStage_id_text(String stage_id_text);
    
    /**
     * 设置 [阶段]
     */
    public String getStage_id_text();

    /**
     * 获取 [阶段]脏标记
     */
    public boolean getStage_id_textDirtyFlag();
    /**
     * 获取 [感谢留言]
     */
    public void setThank_you_message(String thank_you_message);
    
    /**
     * 设置 [感谢留言]
     */
    public String getThank_you_message();

    /**
     * 获取 [感谢留言]脏标记
     */
    public boolean getThank_you_messageDirtyFlag();
    /**
     * 获取 [称谓]
     */
    public void setTitle(String title);
    
    /**
     * 设置 [称谓]
     */
    public String getTitle();

    /**
     * 获取 [称谓]脏标记
     */
    public boolean getTitleDirtyFlag();
    /**
     * 获取 [已完成的调查数量]
     */
    public void setTot_comp_survey(Integer tot_comp_survey);
    
    /**
     * 设置 [已完成的调查数量]
     */
    public Integer getTot_comp_survey();

    /**
     * 获取 [已完成的调查数量]脏标记
     */
    public boolean getTot_comp_surveyDirtyFlag();
    /**
     * 获取 [已发送调查者数量]
     */
    public void setTot_sent_survey(Integer tot_sent_survey);
    
    /**
     * 设置 [已发送调查者数量]
     */
    public Integer getTot_sent_survey();

    /**
     * 获取 [已发送调查者数量]脏标记
     */
    public boolean getTot_sent_surveyDirtyFlag();
    /**
     * 获取 [已开始的调查数量]
     */
    public void setTot_start_survey(Integer tot_start_survey);
    
    /**
     * 设置 [已开始的调查数量]
     */
    public Integer getTot_start_survey();

    /**
     * 获取 [已开始的调查数量]脏标记
     */
    public boolean getTot_start_surveyDirtyFlag();
    /**
     * 获取 [用户可返回]
     */
    public void setUsers_can_go_back(String users_can_go_back);
    
    /**
     * 设置 [用户可返回]
     */
    public String getUsers_can_go_back();

    /**
     * 获取 [用户可返回]脏标记
     */
    public boolean getUsers_can_go_backDirtyFlag();
    /**
     * 获取 [用户回应]
     */
    public void setUser_input_ids(String user_input_ids);
    
    /**
     * 设置 [用户回应]
     */
    public String getUser_input_ids();

    /**
     * 获取 [用户回应]脏标记
     */
    public boolean getUser_input_idsDirtyFlag();
    /**
     * 获取 [网站信息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站信息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站信息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
