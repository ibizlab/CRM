package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_currency_rate;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_currency_rateSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[res_currency_rate] 服务对象接口
 */
@FeignClient(value = "odoo-base", contextId = "res-currency-rate", fallback = res_currency_rateFallback.class)
public interface res_currency_rateFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/res_currency_rates/{id}")
    Res_currency_rate get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/res_currency_rates")
    Res_currency_rate create(@RequestBody Res_currency_rate res_currency_rate);

    @RequestMapping(method = RequestMethod.POST, value = "/res_currency_rates/batch")
    Boolean createBatch(@RequestBody List<Res_currency_rate> res_currency_rates);


    @RequestMapping(method = RequestMethod.PUT, value = "/res_currency_rates/{id}")
    Res_currency_rate update(@PathVariable("id") Integer id,@RequestBody Res_currency_rate res_currency_rate);

    @RequestMapping(method = RequestMethod.PUT, value = "/res_currency_rates/batch")
    Boolean updateBatch(@RequestBody List<Res_currency_rate> res_currency_rates);



    @RequestMapping(method = RequestMethod.DELETE, value = "/res_currency_rates/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/res_currency_rates/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);




    @RequestMapping(method = RequestMethod.POST, value = "/res_currency_rates/searchdefault")
    Page<Res_currency_rate> searchDefault(@RequestBody Res_currency_rateSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/res_currency_rates/select")
    Page<Res_currency_rate> select();


    @RequestMapping(method = RequestMethod.GET, value = "/res_currency_rates/getdraft")
    Res_currency_rate getDraft();


}
