package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_resend_message;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_resend_messageSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_resend_messageService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_resend_messageFeignClient;

/**
 * 实体[EMail重发向导] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_resend_messageServiceImpl implements IMail_resend_messageService {

    @Autowired
    mail_resend_messageFeignClient mail_resend_messageFeignClient;


    @Override
    public Mail_resend_message get(Integer id) {
		Mail_resend_message et=mail_resend_messageFeignClient.get(id);
        if(et==null){
            et=new Mail_resend_message();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Mail_resend_message getDraft(Mail_resend_message et) {
        et=mail_resend_messageFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mail_resend_messageFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_resend_messageFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Mail_resend_message et) {
        Mail_resend_message rt = mail_resend_messageFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_resend_message> list){
        mail_resend_messageFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Mail_resend_message et) {
        Mail_resend_message rt = mail_resend_messageFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_resend_message> list){
        mail_resend_messageFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_resend_message> searchDefault(Mail_resend_messageSearchContext context) {
        Page<Mail_resend_message> mail_resend_messages=mail_resend_messageFeignClient.searchDefault(context);
        return mail_resend_messages;
    }


}


