package cn.ibizlab.odoo.core.odoo_bus.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_bus.domain.Bus_bus;
import cn.ibizlab.odoo.core.odoo_bus.filter.Bus_busSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[bus_bus] 服务对象接口
 */
@FeignClient(value = "odoo-bus", contextId = "bus-bus", fallback = bus_busFallback.class)
public interface bus_busFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/bus_buses/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/bus_buses/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/bus_buses/{id}")
    Bus_bus update(@PathVariable("id") Integer id,@RequestBody Bus_bus bus_bus);

    @RequestMapping(method = RequestMethod.PUT, value = "/bus_buses/batch")
    Boolean updateBatch(@RequestBody List<Bus_bus> bus_buses);


    @RequestMapping(method = RequestMethod.POST, value = "/bus_buses")
    Bus_bus create(@RequestBody Bus_bus bus_bus);

    @RequestMapping(method = RequestMethod.POST, value = "/bus_buses/batch")
    Boolean createBatch(@RequestBody List<Bus_bus> bus_buses);



    @RequestMapping(method = RequestMethod.GET, value = "/bus_buses/{id}")
    Bus_bus get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/bus_buses/searchdefault")
    Page<Bus_bus> searchDefault(@RequestBody Bus_busSearchContext context);




    @RequestMapping(method = RequestMethod.GET, value = "/bus_buses/select")
    Page<Bus_bus> select();


    @RequestMapping(method = RequestMethod.GET, value = "/bus_buses/getdraft")
    Bus_bus getDraft();


}
