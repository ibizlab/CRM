package cn.ibizlab.odoo.core.odoo_payment.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_icon;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_iconSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[payment_icon] 服务对象接口
 */
@Component
public class payment_iconFallback implements payment_iconFeignClient{


    public Page<Payment_icon> searchDefault(Payment_iconSearchContext context){
            return null;
     }


    public Payment_icon create(Payment_icon payment_icon){
            return null;
     }
    public Boolean createBatch(List<Payment_icon> payment_icons){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Payment_icon get(Integer id){
            return null;
     }


    public Payment_icon update(Integer id, Payment_icon payment_icon){
            return null;
     }
    public Boolean updateBatch(List<Payment_icon> payment_icons){
            return false;
     }


    public Page<Payment_icon> select(){
            return null;
     }

    public Payment_icon getDraft(){
            return null;
    }



}
