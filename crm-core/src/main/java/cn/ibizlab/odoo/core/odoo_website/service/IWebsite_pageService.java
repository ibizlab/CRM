package cn.ibizlab.odoo.core.odoo_website.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_website.domain.Website_page;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_pageSearchContext;


/**
 * 实体[Website_page] 服务对象接口
 */
public interface IWebsite_pageService{

    boolean update(Website_page et) ;
    void updateBatch(List<Website_page> list) ;
    Website_page get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Website_page getDraft(Website_page et) ;
    boolean create(Website_page et) ;
    void createBatch(List<Website_page> list) ;
    Page<Website_page> searchDefault(Website_pageSearchContext context) ;

}



