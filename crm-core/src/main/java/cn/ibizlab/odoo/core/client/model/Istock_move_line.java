package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [stock_move_line] 对象
 */
public interface Istock_move_line {

    /**
     * 获取 [消耗行]
     */
    public void setConsume_line_ids(String consume_line_ids);
    
    /**
     * 设置 [消耗行]
     */
    public String getConsume_line_ids();

    /**
     * 获取 [消耗行]脏标记
     */
    public boolean getConsume_line_idsDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [日期]
     */
    public void setDate(Timestamp date);
    
    /**
     * 设置 [日期]
     */
    public Timestamp getDate();

    /**
     * 获取 [日期]脏标记
     */
    public boolean getDateDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [移动完成]
     */
    public void setDone_move(String done_move);
    
    /**
     * 设置 [移动完成]
     */
    public String getDone_move();

    /**
     * 获取 [移动完成]脏标记
     */
    public boolean getDone_moveDirtyFlag();
    /**
     * 获取 [完成工单]
     */
    public void setDone_wo(String done_wo);
    
    /**
     * 设置 [完成工单]
     */
    public String getDone_wo();

    /**
     * 获取 [完成工单]脏标记
     */
    public boolean getDone_woDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [初始需求是否可以编辑]
     */
    public void setIs_initial_demand_editable(String is_initial_demand_editable);
    
    /**
     * 设置 [初始需求是否可以编辑]
     */
    public String getIs_initial_demand_editable();

    /**
     * 获取 [初始需求是否可以编辑]脏标记
     */
    public boolean getIs_initial_demand_editableDirtyFlag();
    /**
     * 获取 [是锁定]
     */
    public void setIs_locked(String is_locked);
    
    /**
     * 设置 [是锁定]
     */
    public String getIs_locked();

    /**
     * 获取 [是锁定]脏标记
     */
    public boolean getIs_lockedDirtyFlag();
    /**
     * 获取 [至]
     */
    public void setLocation_dest_id(Integer location_dest_id);
    
    /**
     * 设置 [至]
     */
    public Integer getLocation_dest_id();

    /**
     * 获取 [至]脏标记
     */
    public boolean getLocation_dest_idDirtyFlag();
    /**
     * 获取 [至]
     */
    public void setLocation_dest_id_text(String location_dest_id_text);
    
    /**
     * 设置 [至]
     */
    public String getLocation_dest_id_text();

    /**
     * 获取 [至]脏标记
     */
    public boolean getLocation_dest_id_textDirtyFlag();
    /**
     * 获取 [从]
     */
    public void setLocation_id(Integer location_id);
    
    /**
     * 设置 [从]
     */
    public Integer getLocation_id();

    /**
     * 获取 [从]脏标记
     */
    public boolean getLocation_idDirtyFlag();
    /**
     * 获取 [从]
     */
    public void setLocation_id_text(String location_id_text);
    
    /**
     * 设置 [从]
     */
    public String getLocation_id_text();

    /**
     * 获取 [从]脏标记
     */
    public boolean getLocation_id_textDirtyFlag();
    /**
     * 获取 [批次可见]
     */
    public void setLots_visible(String lots_visible);
    
    /**
     * 设置 [批次可见]
     */
    public String getLots_visible();

    /**
     * 获取 [批次可见]脏标记
     */
    public boolean getLots_visibleDirtyFlag();
    /**
     * 获取 [批次/序列号码]
     */
    public void setLot_id(Integer lot_id);
    
    /**
     * 设置 [批次/序列号码]
     */
    public Integer getLot_id();

    /**
     * 获取 [批次/序列号码]脏标记
     */
    public boolean getLot_idDirtyFlag();
    /**
     * 获取 [批次/序列号码]
     */
    public void setLot_id_text(String lot_id_text);
    
    /**
     * 设置 [批次/序列号码]
     */
    public String getLot_id_text();

    /**
     * 获取 [批次/序列号码]脏标记
     */
    public boolean getLot_id_textDirtyFlag();
    /**
     * 获取 [批次/序列号 名称]
     */
    public void setLot_name(String lot_name);
    
    /**
     * 设置 [批次/序列号 名称]
     */
    public String getLot_name();

    /**
     * 获取 [批次/序列号 名称]脏标记
     */
    public boolean getLot_nameDirtyFlag();
    /**
     * 获取 [完工批次/序列号]
     */
    public void setLot_produced_id(Integer lot_produced_id);
    
    /**
     * 设置 [完工批次/序列号]
     */
    public Integer getLot_produced_id();

    /**
     * 获取 [完工批次/序列号]脏标记
     */
    public boolean getLot_produced_idDirtyFlag();
    /**
     * 获取 [完工批次/序列号]
     */
    public void setLot_produced_id_text(String lot_produced_id_text);
    
    /**
     * 设置 [完工批次/序列号]
     */
    public String getLot_produced_id_text();

    /**
     * 获取 [完工批次/序列号]脏标记
     */
    public boolean getLot_produced_id_textDirtyFlag();
    /**
     * 获取 [产成品数量]
     */
    public void setLot_produced_qty(Double lot_produced_qty);
    
    /**
     * 设置 [产成品数量]
     */
    public Double getLot_produced_qty();

    /**
     * 获取 [产成品数量]脏标记
     */
    public boolean getLot_produced_qtyDirtyFlag();
    /**
     * 获取 [库存移动]
     */
    public void setMove_id(Integer move_id);
    
    /**
     * 设置 [库存移动]
     */
    public Integer getMove_id();

    /**
     * 获取 [库存移动]脏标记
     */
    public boolean getMove_idDirtyFlag();
    /**
     * 获取 [库存移动]
     */
    public void setMove_id_text(String move_id_text);
    
    /**
     * 设置 [库存移动]
     */
    public String getMove_id_text();

    /**
     * 获取 [库存移动]脏标记
     */
    public boolean getMove_id_textDirtyFlag();
    /**
     * 获取 [所有者]
     */
    public void setOwner_id(Integer owner_id);
    
    /**
     * 设置 [所有者]
     */
    public Integer getOwner_id();

    /**
     * 获取 [所有者]脏标记
     */
    public boolean getOwner_idDirtyFlag();
    /**
     * 获取 [所有者]
     */
    public void setOwner_id_text(String owner_id_text);
    
    /**
     * 设置 [所有者]
     */
    public String getOwner_id_text();

    /**
     * 获取 [所有者]脏标记
     */
    public boolean getOwner_id_textDirtyFlag();
    /**
     * 获取 [源包裹]
     */
    public void setPackage_id(Integer package_id);
    
    /**
     * 设置 [源包裹]
     */
    public Integer getPackage_id();

    /**
     * 获取 [源包裹]脏标记
     */
    public boolean getPackage_idDirtyFlag();
    /**
     * 获取 [源包裹]
     */
    public void setPackage_id_text(String package_id_text);
    
    /**
     * 设置 [源包裹]
     */
    public String getPackage_id_text();

    /**
     * 获取 [源包裹]脏标记
     */
    public boolean getPackage_id_textDirtyFlag();
    /**
     * 获取 [包裹层级]
     */
    public void setPackage_level_id(Integer package_level_id);
    
    /**
     * 设置 [包裹层级]
     */
    public Integer getPackage_level_id();

    /**
     * 获取 [包裹层级]脏标记
     */
    public boolean getPackage_level_idDirtyFlag();
    /**
     * 获取 [库存拣货]
     */
    public void setPicking_id(Integer picking_id);
    
    /**
     * 设置 [库存拣货]
     */
    public Integer getPicking_id();

    /**
     * 获取 [库存拣货]脏标记
     */
    public boolean getPicking_idDirtyFlag();
    /**
     * 获取 [库存拣货]
     */
    public void setPicking_id_text(String picking_id_text);
    
    /**
     * 设置 [库存拣货]
     */
    public String getPicking_id_text();

    /**
     * 获取 [库存拣货]脏标记
     */
    public boolean getPicking_id_textDirtyFlag();
    /**
     * 获取 [移动整个包裹]
     */
    public void setPicking_type_entire_packs(String picking_type_entire_packs);
    
    /**
     * 设置 [移动整个包裹]
     */
    public String getPicking_type_entire_packs();

    /**
     * 获取 [移动整个包裹]脏标记
     */
    public boolean getPicking_type_entire_packsDirtyFlag();
    /**
     * 获取 [创建新批次/序列号码]
     */
    public void setPicking_type_use_create_lots(String picking_type_use_create_lots);
    
    /**
     * 设置 [创建新批次/序列号码]
     */
    public String getPicking_type_use_create_lots();

    /**
     * 获取 [创建新批次/序列号码]脏标记
     */
    public boolean getPicking_type_use_create_lotsDirtyFlag();
    /**
     * 获取 [使用已有批次/序列号码]
     */
    public void setPicking_type_use_existing_lots(String picking_type_use_existing_lots);
    
    /**
     * 设置 [使用已有批次/序列号码]
     */
    public String getPicking_type_use_existing_lots();

    /**
     * 获取 [使用已有批次/序列号码]脏标记
     */
    public boolean getPicking_type_use_existing_lotsDirtyFlag();
    /**
     * 获取 [生产行]
     */
    public void setProduce_line_ids(String produce_line_ids);
    
    /**
     * 设置 [生产行]
     */
    public String getProduce_line_ids();

    /**
     * 获取 [生产行]脏标记
     */
    public boolean getProduce_line_idsDirtyFlag();
    /**
     * 获取 [生产单]
     */
    public void setProduction_id(Integer production_id);
    
    /**
     * 设置 [生产单]
     */
    public Integer getProduction_id();

    /**
     * 获取 [生产单]脏标记
     */
    public boolean getProduction_idDirtyFlag();
    /**
     * 获取 [生产单]
     */
    public void setProduction_id_text(String production_id_text);
    
    /**
     * 设置 [生产单]
     */
    public String getProduction_id_text();

    /**
     * 获取 [生产单]脏标记
     */
    public boolean getProduction_id_textDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id(Integer product_id);
    
    /**
     * 设置 [产品]
     */
    public Integer getProduct_id();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_idDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id_text(String product_id_text);
    
    /**
     * 设置 [产品]
     */
    public String getProduct_id_text();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_id_textDirtyFlag();
    /**
     * 获取 [实际预留数量]
     */
    public void setProduct_qty(Double product_qty);
    
    /**
     * 设置 [实际预留数量]
     */
    public Double getProduct_qty();

    /**
     * 获取 [实际预留数量]脏标记
     */
    public boolean getProduct_qtyDirtyFlag();
    /**
     * 获取 [单位]
     */
    public void setProduct_uom_id(Integer product_uom_id);
    
    /**
     * 设置 [单位]
     */
    public Integer getProduct_uom_id();

    /**
     * 获取 [单位]脏标记
     */
    public boolean getProduct_uom_idDirtyFlag();
    /**
     * 获取 [单位]
     */
    public void setProduct_uom_id_text(String product_uom_id_text);
    
    /**
     * 设置 [单位]
     */
    public String getProduct_uom_id_text();

    /**
     * 获取 [单位]脏标记
     */
    public boolean getProduct_uom_id_textDirtyFlag();
    /**
     * 获取 [已保留]
     */
    public void setProduct_uom_qty(Double product_uom_qty);
    
    /**
     * 设置 [已保留]
     */
    public Double getProduct_uom_qty();

    /**
     * 获取 [已保留]脏标记
     */
    public boolean getProduct_uom_qtyDirtyFlag();
    /**
     * 获取 [完成]
     */
    public void setQty_done(Double qty_done);
    
    /**
     * 设置 [完成]
     */
    public Double getQty_done();

    /**
     * 获取 [完成]脏标记
     */
    public boolean getQty_doneDirtyFlag();
    /**
     * 获取 [编号]
     */
    public void setReference(String reference);
    
    /**
     * 设置 [编号]
     */
    public String getReference();

    /**
     * 获取 [编号]脏标记
     */
    public boolean getReferenceDirtyFlag();
    /**
     * 获取 [目的地包裹]
     */
    public void setResult_package_id(Integer result_package_id);
    
    /**
     * 设置 [目的地包裹]
     */
    public Integer getResult_package_id();

    /**
     * 获取 [目的地包裹]脏标记
     */
    public boolean getResult_package_idDirtyFlag();
    /**
     * 获取 [目的地包裹]
     */
    public void setResult_package_id_text(String result_package_id_text);
    
    /**
     * 设置 [目的地包裹]
     */
    public String getResult_package_id_text();

    /**
     * 获取 [目的地包裹]脏标记
     */
    public boolean getResult_package_id_textDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [追踪]
     */
    public void setTracking(String tracking);
    
    /**
     * 设置 [追踪]
     */
    public String getTracking();

    /**
     * 获取 [追踪]脏标记
     */
    public boolean getTrackingDirtyFlag();
    /**
     * 获取 [工单]
     */
    public void setWorkorder_id(Integer workorder_id);
    
    /**
     * 设置 [工单]
     */
    public Integer getWorkorder_id();

    /**
     * 获取 [工单]脏标记
     */
    public boolean getWorkorder_idDirtyFlag();
    /**
     * 获取 [工单]
     */
    public void setWorkorder_id_text(String workorder_id_text);
    
    /**
     * 设置 [工单]
     */
    public String getWorkorder_id_text();

    /**
     * 获取 [工单]脏标记
     */
    public boolean getWorkorder_id_textDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
