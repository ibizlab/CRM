package cn.ibizlab.odoo.core.odoo_event.valuerule.anno.event_event;

import cn.ibizlab.odoo.core.odoo_event.valuerule.validator.event_event.Event_eventDate_tzDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Event_event
 * 属性：Date_tz
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Event_eventDate_tzDefaultValidator.class})
public @interface Event_eventDate_tzDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
