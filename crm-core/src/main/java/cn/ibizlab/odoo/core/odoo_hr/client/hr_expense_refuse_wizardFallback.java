package cn.ibizlab.odoo.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense_refuse_wizard;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_expense_refuse_wizardSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[hr_expense_refuse_wizard] 服务对象接口
 */
@Component
public class hr_expense_refuse_wizardFallback implements hr_expense_refuse_wizardFeignClient{

    public Page<Hr_expense_refuse_wizard> searchDefault(Hr_expense_refuse_wizardSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Hr_expense_refuse_wizard get(Integer id){
            return null;
     }




    public Hr_expense_refuse_wizard update(Integer id, Hr_expense_refuse_wizard hr_expense_refuse_wizard){
            return null;
     }
    public Boolean updateBatch(List<Hr_expense_refuse_wizard> hr_expense_refuse_wizards){
            return false;
     }


    public Hr_expense_refuse_wizard create(Hr_expense_refuse_wizard hr_expense_refuse_wizard){
            return null;
     }
    public Boolean createBatch(List<Hr_expense_refuse_wizard> hr_expense_refuse_wizards){
            return false;
     }

    public Page<Hr_expense_refuse_wizard> select(){
            return null;
     }

    public Hr_expense_refuse_wizard getDraft(){
            return null;
    }



}
