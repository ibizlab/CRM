package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iportal_mixin;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[portal_mixin] 服务对象接口
 */
public interface Iportal_mixinClientService{

    public Iportal_mixin createModel() ;

    public void create(Iportal_mixin portal_mixin);

    public Page<Iportal_mixin> fetchDefault(SearchContext context);

    public void createBatch(List<Iportal_mixin> portal_mixins);

    public void update(Iportal_mixin portal_mixin);

    public void remove(Iportal_mixin portal_mixin);

    public void updateBatch(List<Iportal_mixin> portal_mixins);

    public void removeBatch(List<Iportal_mixin> portal_mixins);

    public void get(Iportal_mixin portal_mixin);

    public Page<Iportal_mixin> select(SearchContext context);

    public void getDraft(Iportal_mixin portal_mixin);

}
