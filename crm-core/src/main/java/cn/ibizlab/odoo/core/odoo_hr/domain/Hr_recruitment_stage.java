package cn.ibizlab.odoo.core.odoo_hr.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [招聘阶段] 对象
 */
@Data
public class Hr_recruitment_stage extends EntityClient implements Serializable {

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 要求
     */
    @JSONField(name = "requirements")
    @JsonProperty("requirements")
    private String requirements;

    /**
     * 灰色看板标签
     */
    @DEField(name = "legend_normal")
    @JSONField(name = "legend_normal")
    @JsonProperty("legend_normal")
    private String legendNormal;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 阶段名
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 在招聘管道收起
     */
    @JSONField(name = "fold")
    @JsonProperty("fold")
    private String fold;

    /**
     * 序号
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 绿色看板标签
     */
    @DEField(name = "legend_done")
    @JSONField(name = "legend_done")
    @JsonProperty("legend_done")
    private String legendDone;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 红色的看板标签
     */
    @DEField(name = "legend_blocked")
    @JSONField(name = "legend_blocked")
    @JsonProperty("legend_blocked")
    private String legendBlocked;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 自动发邮件
     */
    @JSONField(name = "template_id_text")
    @JsonProperty("template_id_text")
    private String templateIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 具体职位
     */
    @JSONField(name = "job_id_text")
    @JsonProperty("job_id_text")
    private String jobIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 具体职位
     */
    @DEField(name = "job_id")
    @JSONField(name = "job_id")
    @JsonProperty("job_id")
    private Integer jobId;

    /**
     * 自动发邮件
     */
    @DEField(name = "template_id")
    @JSONField(name = "template_id")
    @JsonProperty("template_id")
    private Integer templateId;


    /**
     * 
     */
    @JSONField(name = "odoojob")
    @JsonProperty("odoojob")
    private cn.ibizlab.odoo.core.odoo_hr.domain.Hr_job odooJob;

    /**
     * 
     */
    @JSONField(name = "odootemplate")
    @JsonProperty("odootemplate")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_template odooTemplate;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [要求]
     */
    public void setRequirements(String requirements){
        this.requirements = requirements ;
        this.modify("requirements",requirements);
    }
    /**
     * 设置 [灰色看板标签]
     */
    public void setLegendNormal(String legendNormal){
        this.legendNormal = legendNormal ;
        this.modify("legend_normal",legendNormal);
    }
    /**
     * 设置 [阶段名]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [在招聘管道收起]
     */
    public void setFold(String fold){
        this.fold = fold ;
        this.modify("fold",fold);
    }
    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }
    /**
     * 设置 [绿色看板标签]
     */
    public void setLegendDone(String legendDone){
        this.legendDone = legendDone ;
        this.modify("legend_done",legendDone);
    }
    /**
     * 设置 [红色的看板标签]
     */
    public void setLegendBlocked(String legendBlocked){
        this.legendBlocked = legendBlocked ;
        this.modify("legend_blocked",legendBlocked);
    }
    /**
     * 设置 [具体职位]
     */
    public void setJobId(Integer jobId){
        this.jobId = jobId ;
        this.modify("job_id",jobId);
    }
    /**
     * 设置 [自动发邮件]
     */
    public void setTemplateId(Integer templateId){
        this.templateId = templateId ;
        this.modify("template_id",templateId);
    }

}


