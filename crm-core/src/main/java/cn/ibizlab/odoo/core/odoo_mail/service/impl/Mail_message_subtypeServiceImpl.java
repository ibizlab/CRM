package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_message_subtype;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_message_subtypeSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_message_subtypeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_message_subtypeFeignClient;

/**
 * 实体[消息子类型] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_message_subtypeServiceImpl implements IMail_message_subtypeService {

    @Autowired
    mail_message_subtypeFeignClient mail_message_subtypeFeignClient;


    @Override
    public Mail_message_subtype get(Integer id) {
		Mail_message_subtype et=mail_message_subtypeFeignClient.get(id);
        if(et==null){
            et=new Mail_message_subtype();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mail_message_subtypeFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_message_subtypeFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Mail_message_subtype et) {
        Mail_message_subtype rt = mail_message_subtypeFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_message_subtype> list){
        mail_message_subtypeFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Mail_message_subtype et) {
        Mail_message_subtype rt = mail_message_subtypeFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_message_subtype> list){
        mail_message_subtypeFeignClient.updateBatch(list) ;
    }

    @Override
    public Mail_message_subtype getDraft(Mail_message_subtype et) {
        et=mail_message_subtypeFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_message_subtype> searchDefault(Mail_message_subtypeSearchContext context) {
        Page<Mail_message_subtype> mail_message_subtypes=mail_message_subtypeFeignClient.searchDefault(context);
        return mail_message_subtypes;
    }


}


