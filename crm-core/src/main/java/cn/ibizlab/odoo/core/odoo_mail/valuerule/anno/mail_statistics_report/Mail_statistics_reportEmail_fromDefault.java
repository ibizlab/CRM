package cn.ibizlab.odoo.core.odoo_mail.valuerule.anno.mail_statistics_report;

import cn.ibizlab.odoo.core.odoo_mail.valuerule.validator.mail_statistics_report.Mail_statistics_reportEmail_fromDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Mail_statistics_report
 * 属性：Email_from
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Mail_statistics_reportEmail_fromDefaultValidator.class})
public @interface Mail_statistics_reportEmail_fromDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[100]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
