package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ires_partner_bank;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_partner_bank] 服务对象接口
 */
public interface Ires_partner_bankClientService{

    public Ires_partner_bank createModel() ;

    public void update(Ires_partner_bank res_partner_bank);

    public void removeBatch(List<Ires_partner_bank> res_partner_banks);

    public void get(Ires_partner_bank res_partner_bank);

    public void updateBatch(List<Ires_partner_bank> res_partner_banks);

    public Page<Ires_partner_bank> fetchDefault(SearchContext context);

    public void remove(Ires_partner_bank res_partner_bank);

    public void createBatch(List<Ires_partner_bank> res_partner_banks);

    public void create(Ires_partner_bank res_partner_bank);

    public Page<Ires_partner_bank> select(SearchContext context);

    public void getDraft(Ires_partner_bank res_partner_bank);

}
