package cn.ibizlab.odoo.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_holidays_summary_employee;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_holidays_summary_employeeSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[hr_holidays_summary_employee] 服务对象接口
 */
@Component
public class hr_holidays_summary_employeeFallback implements hr_holidays_summary_employeeFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Hr_holidays_summary_employee> searchDefault(Hr_holidays_summary_employeeSearchContext context){
            return null;
     }





    public Hr_holidays_summary_employee update(Integer id, Hr_holidays_summary_employee hr_holidays_summary_employee){
            return null;
     }
    public Boolean updateBatch(List<Hr_holidays_summary_employee> hr_holidays_summary_employees){
            return false;
     }


    public Hr_holidays_summary_employee get(Integer id){
            return null;
     }


    public Hr_holidays_summary_employee create(Hr_holidays_summary_employee hr_holidays_summary_employee){
            return null;
     }
    public Boolean createBatch(List<Hr_holidays_summary_employee> hr_holidays_summary_employees){
            return false;
     }

    public Page<Hr_holidays_summary_employee> select(){
            return null;
     }

    public Hr_holidays_summary_employee getDraft(){
            return null;
    }



}
