package cn.ibizlab.odoo.core.repository ;

import java.util.Collection;
import java.util.List;
import java.io.Serializable;

public interface Repository<T> {

	T createPO();

	T get(String key);


    boolean create(T po);

    boolean update(T po);

    boolean remove(String key);

    void createBatch(List poLists, int batchSize);

    void updateBatch(List poLists, int batchSize);

    void removeBatch(Collection<? extends Serializable> idList);

}
