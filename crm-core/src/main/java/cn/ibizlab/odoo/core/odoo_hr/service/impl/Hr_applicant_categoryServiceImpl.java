package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_applicant_category;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_applicant_categorySearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_applicant_categoryService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_hr.client.hr_applicant_categoryFeignClient;

/**
 * 实体[申请人类别] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_applicant_categoryServiceImpl implements IHr_applicant_categoryService {

    @Autowired
    hr_applicant_categoryFeignClient hr_applicant_categoryFeignClient;


    @Override
    public boolean update(Hr_applicant_category et) {
        Hr_applicant_category rt = hr_applicant_categoryFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Hr_applicant_category> list){
        hr_applicant_categoryFeignClient.updateBatch(list) ;
    }

    @Override
    public Hr_applicant_category get(Integer id) {
		Hr_applicant_category et=hr_applicant_categoryFeignClient.get(id);
        if(et==null){
            et=new Hr_applicant_category();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Hr_applicant_category et) {
        Hr_applicant_category rt = hr_applicant_categoryFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_applicant_category> list){
        hr_applicant_categoryFeignClient.createBatch(list) ;
    }

    @Override
    public Hr_applicant_category getDraft(Hr_applicant_category et) {
        et=hr_applicant_categoryFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=hr_applicant_categoryFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        hr_applicant_categoryFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_applicant_category> searchDefault(Hr_applicant_categorySearchContext context) {
        Page<Hr_applicant_category> hr_applicant_categorys=hr_applicant_categoryFeignClient.searchDefault(context);
        return hr_applicant_categorys;
    }


}


