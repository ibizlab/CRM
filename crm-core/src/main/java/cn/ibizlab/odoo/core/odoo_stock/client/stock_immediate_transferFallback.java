package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_immediate_transfer;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_immediate_transferSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_immediate_transfer] 服务对象接口
 */
@Component
public class stock_immediate_transferFallback implements stock_immediate_transferFeignClient{


    public Stock_immediate_transfer create(Stock_immediate_transfer stock_immediate_transfer){
            return null;
     }
    public Boolean createBatch(List<Stock_immediate_transfer> stock_immediate_transfers){
            return false;
     }

    public Stock_immediate_transfer update(Integer id, Stock_immediate_transfer stock_immediate_transfer){
            return null;
     }
    public Boolean updateBatch(List<Stock_immediate_transfer> stock_immediate_transfers){
            return false;
     }


    public Stock_immediate_transfer get(Integer id){
            return null;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Stock_immediate_transfer> searchDefault(Stock_immediate_transferSearchContext context){
            return null;
     }



    public Page<Stock_immediate_transfer> select(){
            return null;
     }

    public Stock_immediate_transfer getDraft(){
            return null;
    }



}
