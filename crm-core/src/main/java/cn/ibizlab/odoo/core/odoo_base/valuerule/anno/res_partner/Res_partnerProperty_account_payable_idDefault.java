package cn.ibizlab.odoo.core.odoo_base.valuerule.anno.res_partner;

import cn.ibizlab.odoo.core.odoo_base.valuerule.validator.res_partner.Res_partnerProperty_account_payable_idDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Res_partner
 * 属性：Property_account_payable_id
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Res_partnerProperty_account_payable_idDefaultValidator.class})
public @interface Res_partnerProperty_account_payable_idDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
