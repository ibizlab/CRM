package cn.ibizlab.odoo.core.odoo_sale.valuerule.anno.sale_order;

import cn.ibizlab.odoo.core.odoo_sale.valuerule.validator.sale_order.Sale_orderAccess_urlDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Sale_order
 * 属性：Access_url
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Sale_orderAccess_urlDefaultValidator.class})
public @interface Sale_orderAccess_urlDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[100]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
