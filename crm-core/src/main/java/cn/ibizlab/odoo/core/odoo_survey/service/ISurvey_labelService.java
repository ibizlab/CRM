package cn.ibizlab.odoo.core.odoo_survey.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_label;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_labelSearchContext;


/**
 * 实体[Survey_label] 服务对象接口
 */
public interface ISurvey_labelService{

    boolean create(Survey_label et) ;
    void createBatch(List<Survey_label> list) ;
    boolean update(Survey_label et) ;
    void updateBatch(List<Survey_label> list) ;
    Survey_label get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Survey_label getDraft(Survey_label et) ;
    Page<Survey_label> searchDefault(Survey_labelSearchContext context) ;

}



