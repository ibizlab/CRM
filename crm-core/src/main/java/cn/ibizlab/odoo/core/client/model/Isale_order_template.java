package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [sale_order_template] 对象
 */
public interface Isale_order_template {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [确认邮件]
     */
    public void setMail_template_id(Integer mail_template_id);
    
    /**
     * 设置 [确认邮件]
     */
    public Integer getMail_template_id();

    /**
     * 获取 [确认邮件]脏标记
     */
    public boolean getMail_template_idDirtyFlag();
    /**
     * 获取 [确认邮件]
     */
    public void setMail_template_id_text(String mail_template_id_text);
    
    /**
     * 设置 [确认邮件]
     */
    public String getMail_template_id_text();

    /**
     * 获取 [确认邮件]脏标记
     */
    public boolean getMail_template_id_textDirtyFlag();
    /**
     * 获取 [报价单模板]
     */
    public void setName(String name);
    
    /**
     * 设置 [报价单模板]
     */
    public String getName();

    /**
     * 获取 [报价单模板]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [条款和条件]
     */
    public void setNote(String note);
    
    /**
     * 设置 [条款和条件]
     */
    public String getNote();

    /**
     * 获取 [条款和条件]脏标记
     */
    public boolean getNoteDirtyFlag();
    /**
     * 获取 [报价单时长]
     */
    public void setNumber_of_days(Integer number_of_days);
    
    /**
     * 设置 [报价单时长]
     */
    public Integer getNumber_of_days();

    /**
     * 获取 [报价单时长]脏标记
     */
    public boolean getNumber_of_daysDirtyFlag();
    /**
     * 获取 [在线支付]
     */
    public void setRequire_payment(String require_payment);
    
    /**
     * 设置 [在线支付]
     */
    public String getRequire_payment();

    /**
     * 获取 [在线支付]脏标记
     */
    public boolean getRequire_paymentDirtyFlag();
    /**
     * 获取 [在线签名]
     */
    public void setRequire_signature(String require_signature);
    
    /**
     * 设置 [在线签名]
     */
    public String getRequire_signature();

    /**
     * 获取 [在线签名]脏标记
     */
    public boolean getRequire_signatureDirtyFlag();
    /**
     * 获取 [明细行]
     */
    public void setSale_order_template_line_ids(String sale_order_template_line_ids);
    
    /**
     * 设置 [明细行]
     */
    public String getSale_order_template_line_ids();

    /**
     * 获取 [明细行]脏标记
     */
    public boolean getSale_order_template_line_idsDirtyFlag();
    /**
     * 获取 [可选产品]
     */
    public void setSale_order_template_option_ids(String sale_order_template_option_ids);
    
    /**
     * 设置 [可选产品]
     */
    public String getSale_order_template_option_ids();

    /**
     * 获取 [可选产品]脏标记
     */
    public boolean getSale_order_template_option_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
