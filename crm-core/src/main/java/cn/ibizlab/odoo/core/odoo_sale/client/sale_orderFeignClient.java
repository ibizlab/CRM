package cn.ibizlab.odoo.core.odoo_sale.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_orderSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[sale_order] 服务对象接口
 */
@FeignClient(value = "odoo-sale", contextId = "sale-order", fallback = sale_orderFallback.class)
public interface sale_orderFeignClient {


    @RequestMapping(method = RequestMethod.PUT, value = "/sale_orders/{id}")
    Sale_order update(@PathVariable("id") Integer id,@RequestBody Sale_order sale_order);

    @RequestMapping(method = RequestMethod.PUT, value = "/sale_orders/batch")
    Boolean updateBatch(@RequestBody List<Sale_order> sale_orders);



    @RequestMapping(method = RequestMethod.POST, value = "/sale_orders/searchdefault")
    Page<Sale_order> searchDefault(@RequestBody Sale_orderSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/sale_orders")
    Sale_order create(@RequestBody Sale_order sale_order);

    @RequestMapping(method = RequestMethod.POST, value = "/sale_orders/batch")
    Boolean createBatch(@RequestBody List<Sale_order> sale_orders);



    @RequestMapping(method = RequestMethod.GET, value = "/sale_orders/{id}")
    Sale_order get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/sale_orders/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/sale_orders/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/sale_orders/select")
    Page<Sale_order> select();


    @RequestMapping(method = RequestMethod.POST, value = "/sale_orders/save")
    Boolean save(@RequestBody Sale_order sale_order);

    @RequestMapping(method = RequestMethod.POST, value = "/sale_orders/save")
    Boolean saveBatch(@RequestBody List<Sale_order> sale_orders);


    @RequestMapping(method = RequestMethod.POST, value = "/sale_orders/checkkey")
    Boolean checkKey(@RequestBody Sale_order sale_order);


    @RequestMapping(method = RequestMethod.GET, value = "/sale_orders/getdraft")
    Sale_order getDraft();


}
