package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_confirm;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_confirmSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoice_confirmService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_invoice_confirmFeignClient;

/**
 * 实体[确认选定的发票] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_invoice_confirmServiceImpl implements IAccount_invoice_confirmService {

    @Autowired
    account_invoice_confirmFeignClient account_invoice_confirmFeignClient;


    @Override
    public boolean create(Account_invoice_confirm et) {
        Account_invoice_confirm rt = account_invoice_confirmFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_invoice_confirm> list){
        account_invoice_confirmFeignClient.createBatch(list) ;
    }

    @Override
    public Account_invoice_confirm getDraft(Account_invoice_confirm et) {
        et=account_invoice_confirmFeignClient.getDraft();
        return et;
    }

    @Override
    public Account_invoice_confirm get(Integer id) {
		Account_invoice_confirm et=account_invoice_confirmFeignClient.get(id);
        if(et==null){
            et=new Account_invoice_confirm();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Account_invoice_confirm et) {
        Account_invoice_confirm rt = account_invoice_confirmFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_invoice_confirm> list){
        account_invoice_confirmFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_invoice_confirmFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_invoice_confirmFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_invoice_confirm> searchDefault(Account_invoice_confirmSearchContext context) {
        Page<Account_invoice_confirm> account_invoice_confirms=account_invoice_confirmFeignClient.searchDefault(context);
        return account_invoice_confirms;
    }


}


