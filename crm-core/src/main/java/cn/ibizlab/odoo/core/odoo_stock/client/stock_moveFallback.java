package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_move;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_moveSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_move] 服务对象接口
 */
@Component
public class stock_moveFallback implements stock_moveFeignClient{



    public Stock_move get(Integer id){
            return null;
     }


    public Stock_move create(Stock_move stock_move){
            return null;
     }
    public Boolean createBatch(List<Stock_move> stock_moves){
            return false;
     }

    public Stock_move update(Integer id, Stock_move stock_move){
            return null;
     }
    public Boolean updateBatch(List<Stock_move> stock_moves){
            return false;
     }



    public Page<Stock_move> searchDefault(Stock_moveSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Stock_move> select(){
            return null;
     }

    public Stock_move getDraft(){
            return null;
    }



}
