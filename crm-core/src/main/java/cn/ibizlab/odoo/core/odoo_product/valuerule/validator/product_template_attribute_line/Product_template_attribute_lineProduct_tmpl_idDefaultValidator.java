package cn.ibizlab.odoo.core.odoo_product.valuerule.validator.product_template_attribute_line;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import cn.ibizlab.odoo.util.valuerule.DefaultValueRule;
import cn.ibizlab.odoo.util.valuerule.VRCondition;
import cn.ibizlab.odoo.util.valuerule.condition.*;
import cn.ibizlab.odoo.core.odoo_product.valuerule.anno.product_template_attribute_line.Product_template_attribute_lineProduct_tmpl_idDefault;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 值规则注解解析类
 * 实体：Product_template_attribute_line
 * 属性：Product_tmpl_id
 * 值规则：Default
 * 值规则信息：默认规则
 */
@Slf4j
@IBIZLog
@Component("Product_template_attribute_lineProduct_tmpl_idDefaultValidator")
public class Product_template_attribute_lineProduct_tmpl_idDefaultValidator implements ConstraintValidator<Product_template_attribute_lineProduct_tmpl_idDefault, Integer>,Validator {
    private static final String MESSAGE = "值规则校验失败：【默认规则】";

    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {
        boolean isValid = doValidate(value);
        if(!isValid) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(MESSAGE)
                    .addConstraintViolation();
        }
        return doValidate(value);
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }

    @Override
    public void validate(Object o, Errors errors) {
        if( o!=null && supports(o.getClass())){
            if (!doValidate((Integer) o)){
                errors.reject(MESSAGE);
            }
        }
    }

    public boolean doValidate(Integer value) {
        DefaultValueRule<Integer> valueRule = new DefaultValueRule<>("默认值规则",MESSAGE,"Product_tmpl_id",value)
                //字符串长度，重复检查模式，重复值范围，基础值规则，是否递归检查。
                .init(-1,"NONE",null,null,false);
        return valueRule.isValid();
    }
}

