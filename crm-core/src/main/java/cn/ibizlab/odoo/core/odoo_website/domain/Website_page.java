package cn.ibizlab.odoo.core.odoo_website.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [页] 对象
 */
@Data
public class Website_page extends EntityClient implements Serializable {

    /**
     * 视图类型
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 视图结构
     */
    @JSONField(name = "arch")
    @JsonProperty("arch")
    private String arch;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 网站元说明
     */
    @JSONField(name = "website_meta_description")
    @JsonProperty("website_meta_description")
    private String websiteMetaDescription;

    /**
     * 视图名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 网站网址
     */
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    private String websiteUrl;

    /**
     * 序号
     */
    @JSONField(name = "priority")
    @JsonProperty("priority")
    private Integer priority;

    /**
     * 外部 ID
     */
    @JSONField(name = "xml_id")
    @JsonProperty("xml_id")
    private String xmlId;

    /**
     * 页面已索引
     */
    @DEField(name = "website_indexed")
    @JSONField(name = "website_indexed")
    @JsonProperty("website_indexed")
    private String websiteIndexed;

    /**
     * 视图
     */
    @DEField(name = "view_id")
    @JSONField(name = "view_id")
    @JsonProperty("view_id")
    private Integer viewId;

    /**
     * 主题模板
     */
    @DEField(name = "theme_template_id")
    @JSONField(name = "theme_template_id")
    @JsonProperty("theme_template_id")
    private Integer themeTemplateId;

    /**
     * 网站opengraph图像
     */
    @JSONField(name = "website_meta_og_img")
    @JsonProperty("website_meta_og_img")
    private String websiteMetaOgImg;

    /**
     * 作为可选继承显示
     */
    @JSONField(name = "customize_show")
    @JsonProperty("customize_show")
    private String customizeShow;

    /**
     * 模型
     */
    @JSONField(name = "model")
    @JsonProperty("model")
    private String model;

    /**
     * 下级字段
     */
    @JSONField(name = "field_parent")
    @JsonProperty("field_parent")
    private String fieldParent;

    /**
     * Arch Blob
     */
    @JSONField(name = "arch_db")
    @JsonProperty("arch_db")
    private String archDb;

    /**
     * 主页
     */
    @JSONField(name = "is_homepage")
    @JsonProperty("is_homepage")
    private String isHomepage;

    /**
     * 在当前网站显示
     */
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private String websitePublished;

    /**
     * 网站meta标题
     */
    @JSONField(name = "website_meta_title")
    @JsonProperty("website_meta_title")
    private String websiteMetaTitle;

    /**
     * 模型数据
     */
    @JSONField(name = "model_data_id")
    @JsonProperty("model_data_id")
    private Integer modelDataId;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 视图继承模式
     */
    @JSONField(name = "mode")
    @JsonProperty("mode")
    private String mode;

    /**
     * 相关菜单
     */
    @JSONField(name = "menu_ids")
    @JsonProperty("menu_ids")
    private String menuIds;

    /**
     * 继承于此的视图
     */
    @JSONField(name = "inherit_children_ids")
    @JsonProperty("inherit_children_ids")
    private String inheritChildrenIds;

    /**
     * 基础视图结构
     */
    @JSONField(name = "arch_base")
    @JsonProperty("arch_base")
    private String archBase;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 可见
     */
    @JSONField(name = "is_visible")
    @JsonProperty("is_visible")
    private String isVisible;

    /**
     * 网站页面
     */
    @JSONField(name = "first_page_id")
    @JsonProperty("first_page_id")
    private Integer firstPageId;

    /**
     * SEO优化
     */
    @JSONField(name = "is_seo_optimized")
    @JsonProperty("is_seo_optimized")
    private String isSeoOptimized;

    /**
     * 标题颜色
     */
    @DEField(name = "header_color")
    @JSONField(name = "header_color")
    @JsonProperty("header_color")
    private String headerColor;

    /**
     * 已发布
     */
    @DEField(name = "is_published")
    @JSONField(name = "is_published")
    @JsonProperty("is_published")
    private String isPublished;

    /**
     * 页
     */
    @JSONField(name = "page_ids")
    @JsonProperty("page_ids")
    private String pageIds;

    /**
     * 网站
     */
    @DEField(name = "website_id")
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;

    /**
     * 页面 URL
     */
    @JSONField(name = "url")
    @JsonProperty("url")
    private String url;

    /**
     * 标题覆盖层
     */
    @DEField(name = "header_overlay")
    @JSONField(name = "header_overlay")
    @JsonProperty("header_overlay")
    private String headerOverlay;

    /**
     * 网站meta关键词
     */
    @JSONField(name = "website_meta_keywords")
    @JsonProperty("website_meta_keywords")
    private String websiteMetaKeywords;

    /**
     * 发布日期
     */
    @DEField(name = "date_publish")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_publish" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_publish")
    private Timestamp datePublish;

    /**
     * 群组
     */
    @JSONField(name = "groups_id")
    @JsonProperty("groups_id")
    private String groupsId;

    /**
     * 键
     */
    @JSONField(name = "key")
    @JsonProperty("key")
    private String key;

    /**
     * 继承的视图
     */
    @JSONField(name = "inherit_id")
    @JsonProperty("inherit_id")
    private Integer inheritId;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 模型
     */
    @JSONField(name = "model_ids")
    @JsonProperty("model_ids")
    private String modelIds;

    /**
     * Arch 文件名
     */
    @JSONField(name = "arch_fs")
    @JsonProperty("arch_fs")
    private String archFs;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [页面已索引]
     */
    public void setWebsiteIndexed(String websiteIndexed){
        this.websiteIndexed = websiteIndexed ;
        this.modify("website_indexed",websiteIndexed);
    }
    /**
     * 设置 [视图]
     */
    public void setViewId(Integer viewId){
        this.viewId = viewId ;
        this.modify("view_id",viewId);
    }
    /**
     * 设置 [主题模板]
     */
    public void setThemeTemplateId(Integer themeTemplateId){
        this.themeTemplateId = themeTemplateId ;
        this.modify("theme_template_id",themeTemplateId);
    }
    /**
     * 设置 [标题颜色]
     */
    public void setHeaderColor(String headerColor){
        this.headerColor = headerColor ;
        this.modify("header_color",headerColor);
    }
    /**
     * 设置 [已发布]
     */
    public void setIsPublished(String isPublished){
        this.isPublished = isPublished ;
        this.modify("is_published",isPublished);
    }
    /**
     * 设置 [网站]
     */
    public void setWebsiteId(Integer websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }
    /**
     * 设置 [页面 URL]
     */
    public void setUrl(String url){
        this.url = url ;
        this.modify("url",url);
    }
    /**
     * 设置 [标题覆盖层]
     */
    public void setHeaderOverlay(String headerOverlay){
        this.headerOverlay = headerOverlay ;
        this.modify("header_overlay",headerOverlay);
    }
    /**
     * 设置 [发布日期]
     */
    public void setDatePublish(Timestamp datePublish){
        this.datePublish = datePublish ;
        this.modify("date_publish",datePublish);
    }

}


