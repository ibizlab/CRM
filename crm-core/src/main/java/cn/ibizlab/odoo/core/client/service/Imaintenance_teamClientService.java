package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imaintenance_team;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[maintenance_team] 服务对象接口
 */
public interface Imaintenance_teamClientService{

    public Imaintenance_team createModel() ;

    public void get(Imaintenance_team maintenance_team);

    public void remove(Imaintenance_team maintenance_team);

    public Page<Imaintenance_team> fetchDefault(SearchContext context);

    public void update(Imaintenance_team maintenance_team);

    public void createBatch(List<Imaintenance_team> maintenance_teams);

    public void updateBatch(List<Imaintenance_team> maintenance_teams);

    public void create(Imaintenance_team maintenance_team);

    public void removeBatch(List<Imaintenance_team> maintenance_teams);

    public Page<Imaintenance_team> select(SearchContext context);

    public void getDraft(Imaintenance_team maintenance_team);

}
