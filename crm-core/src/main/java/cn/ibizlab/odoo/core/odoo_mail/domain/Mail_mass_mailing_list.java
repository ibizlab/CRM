package cn.ibizlab.odoo.core.odoo_mail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [邮件列表] 对象
 */
@Data
public class Mail_mass_mailing_list extends EntityClient implements Serializable {

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 联系人人数
     */
    @JSONField(name = "contact_nbr")
    @JsonProperty("contact_nbr")
    private Integer contactNbr;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 订阅信息
     */
    @JSONField(name = "subscription_contact_ids")
    @JsonProperty("subscription_contact_ids")
    private String subscriptionContactIds;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 邮件列表
     */
    @JSONField(name = "contact_ids")
    @JsonProperty("contact_ids")
    private String contactIds;

    /**
     * 网站弹出内容
     */
    @DEField(name = "popup_content")
    @JSONField(name = "popup_content")
    @JsonProperty("popup_content")
    private String popupContent;

    /**
     * 公开
     */
    @DEField(name = "is_public")
    @JSONField(name = "is_public")
    @JsonProperty("is_public")
    private String isPublic;

    /**
     * 邮件列表
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 网站弹出重新定向网址
     */
    @DEField(name = "popup_redirect_url")
    @JSONField(name = "popup_redirect_url")
    @JsonProperty("popup_redirect_url")
    private String popupRedirectUrl;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [网站弹出内容]
     */
    public void setPopupContent(String popupContent){
        this.popupContent = popupContent ;
        this.modify("popup_content",popupContent);
    }
    /**
     * 设置 [公开]
     */
    public void setIsPublic(String isPublic){
        this.isPublic = isPublic ;
        this.modify("is_public",isPublic);
    }
    /**
     * 设置 [邮件列表]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [网站弹出重新定向网址]
     */
    public void setPopupRedirectUrl(String popupRedirectUrl){
        this.popupRedirectUrl = popupRedirectUrl ;
        this.modify("popup_redirect_url",popupRedirectUrl);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }

}


