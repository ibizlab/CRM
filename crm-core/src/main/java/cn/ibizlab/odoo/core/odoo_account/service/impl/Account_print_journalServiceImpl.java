package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_print_journal;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_print_journalSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_print_journalService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_print_journalFeignClient;

/**
 * 实体[会计打印日记账] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_print_journalServiceImpl implements IAccount_print_journalService {

    @Autowired
    account_print_journalFeignClient account_print_journalFeignClient;


    @Override
    public Account_print_journal getDraft(Account_print_journal et) {
        et=account_print_journalFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_print_journalFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_print_journalFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Account_print_journal et) {
        Account_print_journal rt = account_print_journalFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_print_journal> list){
        account_print_journalFeignClient.updateBatch(list) ;
    }

    @Override
    public Account_print_journal get(Integer id) {
		Account_print_journal et=account_print_journalFeignClient.get(id);
        if(et==null){
            et=new Account_print_journal();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Account_print_journal et) {
        Account_print_journal rt = account_print_journalFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_print_journal> list){
        account_print_journalFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_print_journal> searchDefault(Account_print_journalSearchContext context) {
        Page<Account_print_journal> account_print_journals=account_print_journalFeignClient.searchDefault(context);
        return account_print_journals;
    }


}


