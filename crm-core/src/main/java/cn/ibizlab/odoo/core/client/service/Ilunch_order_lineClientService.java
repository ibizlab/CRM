package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ilunch_order_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[lunch_order_line] 服务对象接口
 */
public interface Ilunch_order_lineClientService{

    public Ilunch_order_line createModel() ;

    public void get(Ilunch_order_line lunch_order_line);

    public void createBatch(List<Ilunch_order_line> lunch_order_lines);

    public void remove(Ilunch_order_line lunch_order_line);

    public void removeBatch(List<Ilunch_order_line> lunch_order_lines);

    public void update(Ilunch_order_line lunch_order_line);

    public Page<Ilunch_order_line> fetchDefault(SearchContext context);

    public void create(Ilunch_order_line lunch_order_line);

    public void updateBatch(List<Ilunch_order_line> lunch_order_lines);

    public Page<Ilunch_order_line> select(SearchContext context);

    public void getDraft(Ilunch_order_line lunch_order_line);

}
