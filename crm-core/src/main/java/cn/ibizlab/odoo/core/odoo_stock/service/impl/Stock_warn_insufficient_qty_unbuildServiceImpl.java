package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty_unbuild;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warn_insufficient_qty_unbuildSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_warn_insufficient_qty_unbuildService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_warn_insufficient_qty_unbuildFeignClient;

/**
 * 实体[拆解数量短缺警告] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_warn_insufficient_qty_unbuildServiceImpl implements IStock_warn_insufficient_qty_unbuildService {

    @Autowired
    stock_warn_insufficient_qty_unbuildFeignClient stock_warn_insufficient_qty_unbuildFeignClient;


    @Override
    public boolean update(Stock_warn_insufficient_qty_unbuild et) {
        Stock_warn_insufficient_qty_unbuild rt = stock_warn_insufficient_qty_unbuildFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_warn_insufficient_qty_unbuild> list){
        stock_warn_insufficient_qty_unbuildFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Stock_warn_insufficient_qty_unbuild et) {
        Stock_warn_insufficient_qty_unbuild rt = stock_warn_insufficient_qty_unbuildFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_warn_insufficient_qty_unbuild> list){
        stock_warn_insufficient_qty_unbuildFeignClient.createBatch(list) ;
    }

    @Override
    public Stock_warn_insufficient_qty_unbuild get(Integer id) {
		Stock_warn_insufficient_qty_unbuild et=stock_warn_insufficient_qty_unbuildFeignClient.get(id);
        if(et==null){
            et=new Stock_warn_insufficient_qty_unbuild();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=stock_warn_insufficient_qty_unbuildFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_warn_insufficient_qty_unbuildFeignClient.removeBatch(idList);
    }

    @Override
    public Stock_warn_insufficient_qty_unbuild getDraft(Stock_warn_insufficient_qty_unbuild et) {
        et=stock_warn_insufficient_qty_unbuildFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_warn_insufficient_qty_unbuild> searchDefault(Stock_warn_insufficient_qty_unbuildSearchContext context) {
        Page<Stock_warn_insufficient_qty_unbuild> stock_warn_insufficient_qty_unbuilds=stock_warn_insufficient_qty_unbuildFeignClient.searchDefault(context);
        return stock_warn_insufficient_qty_unbuilds;
    }


}


