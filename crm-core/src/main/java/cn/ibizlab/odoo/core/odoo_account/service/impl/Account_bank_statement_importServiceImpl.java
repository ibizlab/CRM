package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_import;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_importSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_bank_statement_importService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_bank_statement_importFeignClient;

/**
 * 实体[导入银行对账单] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_bank_statement_importServiceImpl implements IAccount_bank_statement_importService {

    @Autowired
    account_bank_statement_importFeignClient account_bank_statement_importFeignClient;


    @Override
    public Account_bank_statement_import getDraft(Account_bank_statement_import et) {
        et=account_bank_statement_importFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_bank_statement_importFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_bank_statement_importFeignClient.removeBatch(idList);
    }

    @Override
    public Account_bank_statement_import get(Integer id) {
		Account_bank_statement_import et=account_bank_statement_importFeignClient.get(id);
        if(et==null){
            et=new Account_bank_statement_import();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Account_bank_statement_import et) {
        Account_bank_statement_import rt = account_bank_statement_importFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_bank_statement_import> list){
        account_bank_statement_importFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Account_bank_statement_import et) {
        Account_bank_statement_import rt = account_bank_statement_importFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_bank_statement_import> list){
        account_bank_statement_importFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_bank_statement_import> searchDefault(Account_bank_statement_importSearchContext context) {
        Page<Account_bank_statement_import> account_bank_statement_imports=account_bank_statement_importFeignClient.searchDefault(context);
        return account_bank_statement_imports;
    }


}


