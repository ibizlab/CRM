package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_cashbox;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_cashboxSearchContext;


/**
 * 实体[Account_bank_statement_cashbox] 服务对象接口
 */
public interface IAccount_bank_statement_cashboxService{

    Account_bank_statement_cashbox getDraft(Account_bank_statement_cashbox et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Account_bank_statement_cashbox et) ;
    void updateBatch(List<Account_bank_statement_cashbox> list) ;
    boolean create(Account_bank_statement_cashbox et) ;
    void createBatch(List<Account_bank_statement_cashbox> list) ;
    Account_bank_statement_cashbox get(Integer key) ;
    Page<Account_bank_statement_cashbox> searchDefault(Account_bank_statement_cashboxSearchContext context) ;

}



