package cn.ibizlab.odoo.core.odoo_event.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_registration;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_registrationSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[event_registration] 服务对象接口
 */
@Component
public class event_registrationFallback implements event_registrationFeignClient{

    public Event_registration update(Integer id, Event_registration event_registration){
            return null;
     }
    public Boolean updateBatch(List<Event_registration> event_registrations){
            return false;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Event_registration get(Integer id){
            return null;
     }


    public Event_registration create(Event_registration event_registration){
            return null;
     }
    public Boolean createBatch(List<Event_registration> event_registrations){
            return false;
     }

    public Page<Event_registration> searchDefault(Event_registrationSearchContext context){
            return null;
     }




    public Page<Event_registration> select(){
            return null;
     }

    public Event_registration getDraft(){
            return null;
    }



}
