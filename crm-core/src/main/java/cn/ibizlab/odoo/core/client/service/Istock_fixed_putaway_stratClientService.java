package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_fixed_putaway_strat;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_fixed_putaway_strat] 服务对象接口
 */
public interface Istock_fixed_putaway_stratClientService{

    public Istock_fixed_putaway_strat createModel() ;

    public void updateBatch(List<Istock_fixed_putaway_strat> stock_fixed_putaway_strats);

    public void removeBatch(List<Istock_fixed_putaway_strat> stock_fixed_putaway_strats);

    public void update(Istock_fixed_putaway_strat stock_fixed_putaway_strat);

    public void create(Istock_fixed_putaway_strat stock_fixed_putaway_strat);

    public void get(Istock_fixed_putaway_strat stock_fixed_putaway_strat);

    public void createBatch(List<Istock_fixed_putaway_strat> stock_fixed_putaway_strats);

    public Page<Istock_fixed_putaway_strat> fetchDefault(SearchContext context);

    public void remove(Istock_fixed_putaway_strat stock_fixed_putaway_strat);

    public Page<Istock_fixed_putaway_strat> select(SearchContext context);

    public void getDraft(Istock_fixed_putaway_strat stock_fixed_putaway_strat);

}
