package cn.ibizlab.odoo.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_routing_workcenter;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_routing_workcenterSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mrp_routing_workcenter] 服务对象接口
 */
@FeignClient(value = "odoo-mrp", contextId = "mrp-routing-workcenter", fallback = mrp_routing_workcenterFallback.class)
public interface mrp_routing_workcenterFeignClient {


    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_routing_workcenters/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_routing_workcenters/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_routing_workcenters/{id}")
    Mrp_routing_workcenter get(@PathVariable("id") Integer id);





    @RequestMapping(method = RequestMethod.POST, value = "/mrp_routing_workcenters/searchdefault")
    Page<Mrp_routing_workcenter> searchDefault(@RequestBody Mrp_routing_workcenterSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_routing_workcenters/{id}")
    Mrp_routing_workcenter update(@PathVariable("id") Integer id,@RequestBody Mrp_routing_workcenter mrp_routing_workcenter);

    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_routing_workcenters/batch")
    Boolean updateBatch(@RequestBody List<Mrp_routing_workcenter> mrp_routing_workcenters);


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_routing_workcenters")
    Mrp_routing_workcenter create(@RequestBody Mrp_routing_workcenter mrp_routing_workcenter);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_routing_workcenters/batch")
    Boolean createBatch(@RequestBody List<Mrp_routing_workcenter> mrp_routing_workcenters);


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_routing_workcenters/select")
    Page<Mrp_routing_workcenter> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_routing_workcenters/getdraft")
    Mrp_routing_workcenter getDraft();


}
