package cn.ibizlab.odoo.core.odoo_event.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_type;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_typeSearchContext;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_typeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_event.client.event_typeFeignClient;

/**
 * 实体[活动类别] 服务对象接口实现
 */
@Slf4j
@Service
public class Event_typeServiceImpl implements IEvent_typeService {

    @Autowired
    event_typeFeignClient event_typeFeignClient;


    @Override
    public Event_type get(Integer id) {
		Event_type et=event_typeFeignClient.get(id);
        if(et==null){
            et=new Event_type();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=event_typeFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        event_typeFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Event_type et) {
        Event_type rt = event_typeFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Event_type> list){
        event_typeFeignClient.updateBatch(list) ;
    }

    @Override
    public Event_type getDraft(Event_type et) {
        et=event_typeFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Event_type et) {
        Event_type rt = event_typeFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Event_type> list){
        event_typeFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Event_type> searchDefault(Event_typeSearchContext context) {
        Page<Event_type> event_types=event_typeFeignClient.searchDefault(context);
        return event_types;
    }


}


