package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ibarcodes_barcode_events_mixin;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[barcodes_barcode_events_mixin] 服务对象接口
 */
public interface Ibarcodes_barcode_events_mixinClientService{

    public Ibarcodes_barcode_events_mixin createModel() ;

    public void updateBatch(List<Ibarcodes_barcode_events_mixin> barcodes_barcode_events_mixins);

    public void create(Ibarcodes_barcode_events_mixin barcodes_barcode_events_mixin);

    public void update(Ibarcodes_barcode_events_mixin barcodes_barcode_events_mixin);

    public void remove(Ibarcodes_barcode_events_mixin barcodes_barcode_events_mixin);

    public void get(Ibarcodes_barcode_events_mixin barcodes_barcode_events_mixin);

    public void removeBatch(List<Ibarcodes_barcode_events_mixin> barcodes_barcode_events_mixins);

    public Page<Ibarcodes_barcode_events_mixin> fetchDefault(SearchContext context);

    public void createBatch(List<Ibarcodes_barcode_events_mixin> barcodes_barcode_events_mixins);

    public Page<Ibarcodes_barcode_events_mixin> select(SearchContext context);

    public void getDraft(Ibarcodes_barcode_events_mixin barcodes_barcode_events_mixin);

}
