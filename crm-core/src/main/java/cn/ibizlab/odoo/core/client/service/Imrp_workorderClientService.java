package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imrp_workorder;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mrp_workorder] 服务对象接口
 */
public interface Imrp_workorderClientService{

    public Imrp_workorder createModel() ;

    public void updateBatch(List<Imrp_workorder> mrp_workorders);

    public void removeBatch(List<Imrp_workorder> mrp_workorders);

    public Page<Imrp_workorder> fetchDefault(SearchContext context);

    public void create(Imrp_workorder mrp_workorder);

    public void createBatch(List<Imrp_workorder> mrp_workorders);

    public void remove(Imrp_workorder mrp_workorder);

    public void update(Imrp_workorder mrp_workorder);

    public void get(Imrp_workorder mrp_workorder);

    public Page<Imrp_workorder> select(SearchContext context);

    public void getDraft(Imrp_workorder mrp_workorder);

}
