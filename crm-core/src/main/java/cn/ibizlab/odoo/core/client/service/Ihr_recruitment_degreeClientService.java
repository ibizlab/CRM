package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ihr_recruitment_degree;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_recruitment_degree] 服务对象接口
 */
public interface Ihr_recruitment_degreeClientService{

    public Ihr_recruitment_degree createModel() ;

    public Page<Ihr_recruitment_degree> fetchDefault(SearchContext context);

    public void create(Ihr_recruitment_degree hr_recruitment_degree);

    public void updateBatch(List<Ihr_recruitment_degree> hr_recruitment_degrees);

    public void removeBatch(List<Ihr_recruitment_degree> hr_recruitment_degrees);

    public void remove(Ihr_recruitment_degree hr_recruitment_degree);

    public void update(Ihr_recruitment_degree hr_recruitment_degree);

    public void get(Ihr_recruitment_degree hr_recruitment_degree);

    public void createBatch(List<Ihr_recruitment_degree> hr_recruitment_degrees);

    public Page<Ihr_recruitment_degree> select(SearchContext context);

    public void getDraft(Ihr_recruitment_degree hr_recruitment_degree);

}
