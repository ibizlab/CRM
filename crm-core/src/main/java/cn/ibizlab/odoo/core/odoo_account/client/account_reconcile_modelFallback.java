package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_reconcile_model;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_reconcile_modelSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_reconcile_model] 服务对象接口
 */
@Component
public class account_reconcile_modelFallback implements account_reconcile_modelFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Account_reconcile_model create(Account_reconcile_model account_reconcile_model){
            return null;
     }
    public Boolean createBatch(List<Account_reconcile_model> account_reconcile_models){
            return false;
     }

    public Account_reconcile_model update(Integer id, Account_reconcile_model account_reconcile_model){
            return null;
     }
    public Boolean updateBatch(List<Account_reconcile_model> account_reconcile_models){
            return false;
     }




    public Account_reconcile_model get(Integer id){
            return null;
     }



    public Page<Account_reconcile_model> searchDefault(Account_reconcile_modelSearchContext context){
            return null;
     }


    public Page<Account_reconcile_model> select(){
            return null;
     }

    public Account_reconcile_model getDraft(){
            return null;
    }



}
