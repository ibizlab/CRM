package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_account_tag;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_account_tag] 服务对象接口
 */
public interface Iaccount_account_tagClientService{

    public Iaccount_account_tag createModel() ;

    public void removeBatch(List<Iaccount_account_tag> account_account_tags);

    public void create(Iaccount_account_tag account_account_tag);

    public void updateBatch(List<Iaccount_account_tag> account_account_tags);

    public void createBatch(List<Iaccount_account_tag> account_account_tags);

    public Page<Iaccount_account_tag> fetchDefault(SearchContext context);

    public void update(Iaccount_account_tag account_account_tag);

    public void remove(Iaccount_account_tag account_account_tag);

    public void get(Iaccount_account_tag account_account_tag);

    public Page<Iaccount_account_tag> select(SearchContext context);

    public void getDraft(Iaccount_account_tag account_account_tag);

}
