package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_automation;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_automationSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_automationService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.base_automationFeignClient;

/**
 * 实体[自动动作] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_automationServiceImpl implements IBase_automationService {

    @Autowired
    base_automationFeignClient base_automationFeignClient;


    @Override
    public Base_automation getDraft(Base_automation et) {
        et=base_automationFeignClient.getDraft();
        return et;
    }

    @Override
    public Base_automation get(Integer id) {
		Base_automation et=base_automationFeignClient.get(id);
        if(et==null){
            et=new Base_automation();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Base_automation et) {
        Base_automation rt = base_automationFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_automation> list){
        base_automationFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Base_automation et) {
        Base_automation rt = base_automationFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Base_automation> list){
        base_automationFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=base_automationFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        base_automationFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_automation> searchDefault(Base_automationSearchContext context) {
        Page<Base_automation> base_automations=base_automationFeignClient.searchDefault(context);
        return base_automations;
    }


}


