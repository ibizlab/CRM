package cn.ibizlab.odoo.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_product.domain.Product_supplierinfo;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_supplierinfoSearchContext;


/**
 * 实体[Product_supplierinfo] 服务对象接口
 */
public interface IProduct_supplierinfoService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Product_supplierinfo getDraft(Product_supplierinfo et) ;
    boolean create(Product_supplierinfo et) ;
    void createBatch(List<Product_supplierinfo> list) ;
    Product_supplierinfo get(Integer key) ;
    boolean update(Product_supplierinfo et) ;
    void updateBatch(List<Product_supplierinfo> list) ;
    Page<Product_supplierinfo> searchDefault(Product_supplierinfoSearchContext context) ;

}



