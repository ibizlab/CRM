package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ires_partner_title;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_partner_title] 服务对象接口
 */
public interface Ires_partner_titleClientService{

    public Ires_partner_title createModel() ;

    public void remove(Ires_partner_title res_partner_title);

    public void updateBatch(List<Ires_partner_title> res_partner_titles);

    public void update(Ires_partner_title res_partner_title);

    public Page<Ires_partner_title> fetchDefault(SearchContext context);

    public void create(Ires_partner_title res_partner_title);

    public void get(Ires_partner_title res_partner_title);

    public void createBatch(List<Ires_partner_title> res_partner_titles);

    public void removeBatch(List<Ires_partner_title> res_partner_titles);

    public Page<Ires_partner_title> select(SearchContext context);

    public void getDraft(Ires_partner_title res_partner_title);

}
