package cn.ibizlab.odoo.core.odoo_stock.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [退回拣货] 对象
 */
@Data
public class Stock_return_picking extends EntityClient implements Serializable {

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 移动
     */
    @JSONField(name = "product_return_moves")
    @JsonProperty("product_return_moves")
    private String productReturnMoves;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 链接的移动已存在
     */
    @DEField(name = "move_dest_exists")
    @JSONField(name = "move_dest_exists")
    @JsonProperty("move_dest_exists")
    private String moveDestExists;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 原始位置
     */
    @JSONField(name = "original_location_id_text")
    @JsonProperty("original_location_id_text")
    private String originalLocationIdText;

    /**
     * 退回位置
     */
    @JSONField(name = "location_id_text")
    @JsonProperty("location_id_text")
    private String locationIdText;

    /**
     * 上级位置
     */
    @JSONField(name = "parent_location_id_text")
    @JsonProperty("parent_location_id_text")
    private String parentLocationIdText;

    /**
     * 分拣
     */
    @JSONField(name = "picking_id_text")
    @JsonProperty("picking_id_text")
    private String pickingIdText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 上级位置
     */
    @DEField(name = "parent_location_id")
    @JSONField(name = "parent_location_id")
    @JsonProperty("parent_location_id")
    private Integer parentLocationId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 原始位置
     */
    @DEField(name = "original_location_id")
    @JSONField(name = "original_location_id")
    @JsonProperty("original_location_id")
    private Integer originalLocationId;

    /**
     * 退回位置
     */
    @DEField(name = "location_id")
    @JSONField(name = "location_id")
    @JsonProperty("location_id")
    private Integer locationId;

    /**
     * 分拣
     */
    @DEField(name = "picking_id")
    @JSONField(name = "picking_id")
    @JsonProperty("picking_id")
    private Integer pickingId;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoolocation")
    @JsonProperty("odoolocation")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location odooLocation;

    /**
     * 
     */
    @JSONField(name = "odoooriginallocation")
    @JsonProperty("odoooriginallocation")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location odooOriginalLocation;

    /**
     * 
     */
    @JSONField(name = "odooparentlocation")
    @JsonProperty("odooparentlocation")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location odooParentLocation;

    /**
     * 
     */
    @JSONField(name = "odoopicking")
    @JsonProperty("odoopicking")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking odooPicking;




    /**
     * 设置 [链接的移动已存在]
     */
    public void setMoveDestExists(String moveDestExists){
        this.moveDestExists = moveDestExists ;
        this.modify("move_dest_exists",moveDestExists);
    }
    /**
     * 设置 [上级位置]
     */
    public void setParentLocationId(Integer parentLocationId){
        this.parentLocationId = parentLocationId ;
        this.modify("parent_location_id",parentLocationId);
    }
    /**
     * 设置 [原始位置]
     */
    public void setOriginalLocationId(Integer originalLocationId){
        this.originalLocationId = originalLocationId ;
        this.modify("original_location_id",originalLocationId);
    }
    /**
     * 设置 [退回位置]
     */
    public void setLocationId(Integer locationId){
        this.locationId = locationId ;
        this.modify("location_id",locationId);
    }
    /**
     * 设置 [分拣]
     */
    public void setPickingId(Integer pickingId){
        this.pickingId = pickingId ;
        this.modify("picking_id",pickingId);
    }

}


