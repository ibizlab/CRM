package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_shortcode;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_shortcode] 服务对象接口
 */
public interface Imail_shortcodeClientService{

    public Imail_shortcode createModel() ;

    public void updateBatch(List<Imail_shortcode> mail_shortcodes);

    public void get(Imail_shortcode mail_shortcode);

    public Page<Imail_shortcode> fetchDefault(SearchContext context);

    public void createBatch(List<Imail_shortcode> mail_shortcodes);

    public void create(Imail_shortcode mail_shortcode);

    public void removeBatch(List<Imail_shortcode> mail_shortcodes);

    public void update(Imail_shortcode mail_shortcode);

    public void remove(Imail_shortcode mail_shortcode);

    public Page<Imail_shortcode> select(SearchContext context);

    public void getDraft(Imail_shortcode mail_shortcode);

}
