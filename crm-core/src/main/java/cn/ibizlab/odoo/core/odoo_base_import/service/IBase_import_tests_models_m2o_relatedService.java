package cn.ibizlab.odoo.core.odoo_base_import.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_m2o_related;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_m2o_relatedSearchContext;


/**
 * 实体[Base_import_tests_models_m2o_related] 服务对象接口
 */
public interface IBase_import_tests_models_m2o_relatedService{

    boolean update(Base_import_tests_models_m2o_related et) ;
    void updateBatch(List<Base_import_tests_models_m2o_related> list) ;
    Base_import_tests_models_m2o_related get(Integer key) ;
    Base_import_tests_models_m2o_related getDraft(Base_import_tests_models_m2o_related et) ;
    boolean create(Base_import_tests_models_m2o_related et) ;
    void createBatch(List<Base_import_tests_models_m2o_related> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Base_import_tests_models_m2o_related> searchDefault(Base_import_tests_models_m2o_relatedSearchContext context) ;

}



