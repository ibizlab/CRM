package cn.ibizlab.odoo.core.odoo_project.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_project.domain.Project_project;
import cn.ibizlab.odoo.core.odoo_project.filter.Project_projectSearchContext;
import cn.ibizlab.odoo.core.odoo_project.service.IProject_projectService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_project.client.project_projectFeignClient;

/**
 * 实体[项目] 服务对象接口实现
 */
@Slf4j
@Service
public class Project_projectServiceImpl implements IProject_projectService {

    @Autowired
    project_projectFeignClient project_projectFeignClient;


    @Override
    public Project_project get(Integer id) {
		Project_project et=project_projectFeignClient.get(id);
        if(et==null){
            et=new Project_project();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Project_project et) {
        Project_project rt = project_projectFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Project_project> list){
        project_projectFeignClient.updateBatch(list) ;
    }

    @Override
    public Project_project getDraft(Project_project et) {
        et=project_projectFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Project_project et) {
        Project_project rt = project_projectFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Project_project> list){
        project_projectFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=project_projectFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        project_projectFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Project_project> searchDefault(Project_projectSearchContext context) {
        Page<Project_project> project_projects=project_projectFeignClient.searchDefault(context);
        return project_projects;
    }


}


