package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mail;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mailSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_mail] 服务对象接口
 */
@FeignClient(value = "odoo-mail", contextId = "mail-mail", fallback = mail_mailFallback.class)
public interface mail_mailFeignClient {



    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_mails/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_mails/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/mail_mails/{id}")
    Mail_mail update(@PathVariable("id") Integer id,@RequestBody Mail_mail mail_mail);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_mails/batch")
    Boolean updateBatch(@RequestBody List<Mail_mail> mail_mails);




    @RequestMapping(method = RequestMethod.POST, value = "/mail_mails/searchdefault")
    Page<Mail_mail> searchDefault(@RequestBody Mail_mailSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_mails/{id}")
    Mail_mail get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_mails")
    Mail_mail create(@RequestBody Mail_mail mail_mail);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_mails/batch")
    Boolean createBatch(@RequestBody List<Mail_mail> mail_mails);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_mails/select")
    Page<Mail_mail> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_mails/getdraft")
    Mail_mail getDraft();


}
