package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_shortcode;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_shortcodeSearchContext;


/**
 * 实体[Mail_shortcode] 服务对象接口
 */
public interface IMail_shortcodeService{

    Mail_shortcode get(Integer key) ;
    boolean create(Mail_shortcode et) ;
    void createBatch(List<Mail_shortcode> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Mail_shortcode getDraft(Mail_shortcode et) ;
    boolean update(Mail_shortcode et) ;
    void updateBatch(List<Mail_shortcode> list) ;
    Page<Mail_shortcode> searchDefault(Mail_shortcodeSearchContext context) ;

}



