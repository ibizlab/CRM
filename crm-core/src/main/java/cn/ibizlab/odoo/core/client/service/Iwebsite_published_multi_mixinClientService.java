package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iwebsite_published_multi_mixin;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[website_published_multi_mixin] 服务对象接口
 */
public interface Iwebsite_published_multi_mixinClientService{

    public Iwebsite_published_multi_mixin createModel() ;

    public Page<Iwebsite_published_multi_mixin> fetchDefault(SearchContext context);

    public void createBatch(List<Iwebsite_published_multi_mixin> website_published_multi_mixins);

    public void get(Iwebsite_published_multi_mixin website_published_multi_mixin);

    public void remove(Iwebsite_published_multi_mixin website_published_multi_mixin);

    public void updateBatch(List<Iwebsite_published_multi_mixin> website_published_multi_mixins);

    public void create(Iwebsite_published_multi_mixin website_published_multi_mixin);

    public void update(Iwebsite_published_multi_mixin website_published_multi_mixin);

    public void removeBatch(List<Iwebsite_published_multi_mixin> website_published_multi_mixins);

    public Page<Iwebsite_published_multi_mixin> select(SearchContext context);

    public void getDraft(Iwebsite_published_multi_mixin website_published_multi_mixin);

}
