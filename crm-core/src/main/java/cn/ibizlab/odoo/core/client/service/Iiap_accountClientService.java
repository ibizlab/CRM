package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iiap_account;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[iap_account] 服务对象接口
 */
public interface Iiap_accountClientService{

    public Iiap_account createModel() ;

    public Page<Iiap_account> fetchDefault(SearchContext context);

    public void createBatch(List<Iiap_account> iap_accounts);

    public void create(Iiap_account iap_account);

    public void removeBatch(List<Iiap_account> iap_accounts);

    public void updateBatch(List<Iiap_account> iap_accounts);

    public void remove(Iiap_account iap_account);

    public void update(Iiap_account iap_account);

    public void get(Iiap_account iap_account);

    public Page<Iiap_account> select(SearchContext context);

    public void getDraft(Iiap_account iap_account);

}
