package cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_return_picking;

import cn.ibizlab.odoo.core.odoo_stock.valuerule.validator.stock_return_picking.Stock_return_pickingParent_location_id_textDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Stock_return_picking
 * 属性：Parent_location_id_text
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Stock_return_pickingParent_location_id_textDefaultValidator.class})
public @interface Stock_return_pickingParent_location_id_textDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
