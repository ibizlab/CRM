package cn.ibizlab.odoo.core.odoo_website.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_page;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_pageSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[website_page] 服务对象接口
 */
@FeignClient(value = "odoo-website", contextId = "website-page", fallback = website_pageFallback.class)
public interface website_pageFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/website_pages/searchdefault")
    Page<Website_page> searchDefault(@RequestBody Website_pageSearchContext context);



    @RequestMapping(method = RequestMethod.PUT, value = "/website_pages/{id}")
    Website_page update(@PathVariable("id") Integer id,@RequestBody Website_page website_page);

    @RequestMapping(method = RequestMethod.PUT, value = "/website_pages/batch")
    Boolean updateBatch(@RequestBody List<Website_page> website_pages);


    @RequestMapping(method = RequestMethod.GET, value = "/website_pages/{id}")
    Website_page get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/website_pages")
    Website_page create(@RequestBody Website_page website_page);

    @RequestMapping(method = RequestMethod.POST, value = "/website_pages/batch")
    Boolean createBatch(@RequestBody List<Website_page> website_pages);




    @RequestMapping(method = RequestMethod.DELETE, value = "/website_pages/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/website_pages/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/website_pages/select")
    Page<Website_page> select();


    @RequestMapping(method = RequestMethod.GET, value = "/website_pages/getdraft")
    Website_page getDraft();


}
