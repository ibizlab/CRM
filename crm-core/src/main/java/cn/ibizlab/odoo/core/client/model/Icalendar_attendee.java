package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [calendar_attendee] 对象
 */
public interface Icalendar_attendee {

    /**
     * 获取 [邀请令牌]
     */
    public void setAccess_token(String access_token);
    
    /**
     * 设置 [邀请令牌]
     */
    public String getAccess_token();

    /**
     * 获取 [邀请令牌]脏标记
     */
    public boolean getAccess_tokenDirtyFlag();
    /**
     * 获取 [空闲/忙碌]
     */
    public void setAvailability(String availability);
    
    /**
     * 设置 [空闲/忙碌]
     */
    public String getAvailability();

    /**
     * 获取 [空闲/忙碌]脏标记
     */
    public boolean getAvailabilityDirtyFlag();
    /**
     * 获取 [通用名称]
     */
    public void setCommon_name(String common_name);
    
    /**
     * 设置 [通用名称]
     */
    public String getCommon_name();

    /**
     * 获取 [通用名称]脏标记
     */
    public boolean getCommon_nameDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [EMail]
     */
    public void setEmail(String email);
    
    /**
     * 设置 [EMail]
     */
    public String getEmail();

    /**
     * 获取 [EMail]脏标记
     */
    public boolean getEmailDirtyFlag();
    /**
     * 获取 [会议已连接]
     */
    public void setEvent_id(Integer event_id);
    
    /**
     * 设置 [会议已连接]
     */
    public Integer getEvent_id();

    /**
     * 获取 [会议已连接]脏标记
     */
    public boolean getEvent_idDirtyFlag();
    /**
     * 获取 [会议已连接]
     */
    public void setEvent_id_text(String event_id_text);
    
    /**
     * 设置 [会议已连接]
     */
    public String getEvent_id_text();

    /**
     * 获取 [会议已连接]脏标记
     */
    public boolean getEvent_id_textDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [联系]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [联系]
     */
    public Integer getPartner_id();

    /**
     * 获取 [联系]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [联系]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [联系]
     */
    public String getPartner_id_text();

    /**
     * 获取 [联系]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
