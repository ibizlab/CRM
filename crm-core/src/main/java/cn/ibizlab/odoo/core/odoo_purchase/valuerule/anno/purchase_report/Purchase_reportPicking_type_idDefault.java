package cn.ibizlab.odoo.core.odoo_purchase.valuerule.anno.purchase_report;

import cn.ibizlab.odoo.core.odoo_purchase.valuerule.validator.purchase_report.Purchase_reportPicking_type_idDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Purchase_report
 * 属性：Picking_type_id
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Purchase_reportPicking_type_idDefaultValidator.class})
public @interface Purchase_reportPicking_type_idDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
