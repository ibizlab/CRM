package cn.ibizlab.odoo.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_product_produce;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_product_produceSearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_product_produceService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mrp.client.mrp_product_produceFeignClient;

/**
 * 实体[记录生产] 服务对象接口实现
 */
@Slf4j
@Service
public class Mrp_product_produceServiceImpl implements IMrp_product_produceService {

    @Autowired
    mrp_product_produceFeignClient mrp_product_produceFeignClient;


    @Override
    public boolean create(Mrp_product_produce et) {
        Mrp_product_produce rt = mrp_product_produceFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mrp_product_produce> list){
        mrp_product_produceFeignClient.createBatch(list) ;
    }

    @Override
    public Mrp_product_produce getDraft(Mrp_product_produce et) {
        et=mrp_product_produceFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Mrp_product_produce et) {
        Mrp_product_produce rt = mrp_product_produceFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mrp_product_produce> list){
        mrp_product_produceFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mrp_product_produceFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mrp_product_produceFeignClient.removeBatch(idList);
    }

    @Override
    public Mrp_product_produce get(Integer id) {
		Mrp_product_produce et=mrp_product_produceFeignClient.get(id);
        if(et==null){
            et=new Mrp_product_produce();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mrp_product_produce> searchDefault(Mrp_product_produceSearchContext context) {
        Page<Mrp_product_produce> mrp_product_produces=mrp_product_produceFeignClient.searchDefault(context);
        return mrp_product_produces;
    }


}


