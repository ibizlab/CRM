package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imaintenance_equipment_category;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[maintenance_equipment_category] 服务对象接口
 */
public interface Imaintenance_equipment_categoryClientService{

    public Imaintenance_equipment_category createModel() ;

    public void createBatch(List<Imaintenance_equipment_category> maintenance_equipment_categories);

    public void create(Imaintenance_equipment_category maintenance_equipment_category);

    public void update(Imaintenance_equipment_category maintenance_equipment_category);

    public void get(Imaintenance_equipment_category maintenance_equipment_category);

    public void updateBatch(List<Imaintenance_equipment_category> maintenance_equipment_categories);

    public void removeBatch(List<Imaintenance_equipment_category> maintenance_equipment_categories);

    public Page<Imaintenance_equipment_category> fetchDefault(SearchContext context);

    public void remove(Imaintenance_equipment_category maintenance_equipment_category);

    public Page<Imaintenance_equipment_category> select(SearchContext context);

    public void getDraft(Imaintenance_equipment_category maintenance_equipment_category);

}
