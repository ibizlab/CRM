package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [fleet_vehicle_cost] 对象
 */
public interface Ifleet_vehicle_cost {

    /**
     * 获取 [总价]
     */
    public void setAmount(Double amount);
    
    /**
     * 设置 [总价]
     */
    public Double getAmount();

    /**
     * 获取 [总价]脏标记
     */
    public boolean getAmountDirtyFlag();
    /**
     * 获取 [自动生成]
     */
    public void setAuto_generated(String auto_generated);
    
    /**
     * 设置 [自动生成]
     */
    public String getAuto_generated();

    /**
     * 获取 [自动生成]脏标记
     */
    public boolean getAuto_generatedDirtyFlag();
    /**
     * 获取 [合同]
     */
    public void setContract_id(Integer contract_id);
    
    /**
     * 设置 [合同]
     */
    public Integer getContract_id();

    /**
     * 获取 [合同]脏标记
     */
    public boolean getContract_idDirtyFlag();
    /**
     * 获取 [合同]
     */
    public void setContract_id_text(String contract_id_text);
    
    /**
     * 设置 [合同]
     */
    public String getContract_id_text();

    /**
     * 获取 [合同]脏标记
     */
    public boolean getContract_id_textDirtyFlag();
    /**
     * 获取 [包括服务]
     */
    public void setCost_ids(String cost_ids);
    
    /**
     * 设置 [包括服务]
     */
    public String getCost_ids();

    /**
     * 获取 [包括服务]脏标记
     */
    public boolean getCost_idsDirtyFlag();
    /**
     * 获取 [类型]
     */
    public void setCost_subtype_id(Integer cost_subtype_id);
    
    /**
     * 设置 [类型]
     */
    public Integer getCost_subtype_id();

    /**
     * 获取 [类型]脏标记
     */
    public boolean getCost_subtype_idDirtyFlag();
    /**
     * 获取 [类型]
     */
    public void setCost_subtype_id_text(String cost_subtype_id_text);
    
    /**
     * 设置 [类型]
     */
    public String getCost_subtype_id_text();

    /**
     * 获取 [类型]脏标记
     */
    public boolean getCost_subtype_id_textDirtyFlag();
    /**
     * 获取 [费用所属类别]
     */
    public void setCost_type(String cost_type);
    
    /**
     * 设置 [费用所属类别]
     */
    public String getCost_type();

    /**
     * 获取 [费用所属类别]脏标记
     */
    public boolean getCost_typeDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [日期]
     */
    public void setDate(Timestamp date);
    
    /**
     * 设置 [日期]
     */
    public Timestamp getDate();

    /**
     * 获取 [日期]脏标记
     */
    public boolean getDateDirtyFlag();
    /**
     * 获取 [成本说明]
     */
    public void setDescription(String description);
    
    /**
     * 设置 [成本说明]
     */
    public String getDescription();

    /**
     * 获取 [成本说明]脏标记
     */
    public boolean getDescriptionDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [里程表数值]
     */
    public void setOdometer(Double odometer);
    
    /**
     * 设置 [里程表数值]
     */
    public Double getOdometer();

    /**
     * 获取 [里程表数值]脏标记
     */
    public boolean getOdometerDirtyFlag();
    /**
     * 获取 [里程表]
     */
    public void setOdometer_id(Integer odometer_id);
    
    /**
     * 设置 [里程表]
     */
    public Integer getOdometer_id();

    /**
     * 获取 [里程表]脏标记
     */
    public boolean getOdometer_idDirtyFlag();
    /**
     * 获取 [里程表]
     */
    public void setOdometer_id_text(String odometer_id_text);
    
    /**
     * 设置 [里程表]
     */
    public String getOdometer_id_text();

    /**
     * 获取 [里程表]脏标记
     */
    public boolean getOdometer_id_textDirtyFlag();
    /**
     * 获取 [单位]
     */
    public void setOdometer_unit(String odometer_unit);
    
    /**
     * 设置 [单位]
     */
    public String getOdometer_unit();

    /**
     * 获取 [单位]脏标记
     */
    public boolean getOdometer_unitDirtyFlag();
    /**
     * 获取 [上级]
     */
    public void setParent_id(Integer parent_id);
    
    /**
     * 设置 [上级]
     */
    public Integer getParent_id();

    /**
     * 获取 [上级]脏标记
     */
    public boolean getParent_idDirtyFlag();
    /**
     * 获取 [上级]
     */
    public void setParent_id_text(String parent_id_text);
    
    /**
     * 设置 [上级]
     */
    public String getParent_id_text();

    /**
     * 获取 [上级]脏标记
     */
    public boolean getParent_id_textDirtyFlag();
    /**
     * 获取 [车辆]
     */
    public void setVehicle_id(Integer vehicle_id);
    
    /**
     * 设置 [车辆]
     */
    public Integer getVehicle_id();

    /**
     * 获取 [车辆]脏标记
     */
    public boolean getVehicle_idDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
