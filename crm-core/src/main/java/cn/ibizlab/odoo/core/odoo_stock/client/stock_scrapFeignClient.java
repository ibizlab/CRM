package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_scrap;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_scrapSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_scrap] 服务对象接口
 */
@FeignClient(value = "odoo-stock", contextId = "stock-scrap", fallback = stock_scrapFallback.class)
public interface stock_scrapFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/stock_scraps/searchdefault")
    Page<Stock_scrap> searchDefault(@RequestBody Stock_scrapSearchContext context);




    @RequestMapping(method = RequestMethod.GET, value = "/stock_scraps/{id}")
    Stock_scrap get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_scraps/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_scraps/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_scraps")
    Stock_scrap create(@RequestBody Stock_scrap stock_scrap);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_scraps/batch")
    Boolean createBatch(@RequestBody List<Stock_scrap> stock_scraps);



    @RequestMapping(method = RequestMethod.PUT, value = "/stock_scraps/{id}")
    Stock_scrap update(@PathVariable("id") Integer id,@RequestBody Stock_scrap stock_scrap);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_scraps/batch")
    Boolean updateBatch(@RequestBody List<Stock_scrap> stock_scraps);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_scraps/select")
    Page<Stock_scrap> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_scraps/getdraft")
    Stock_scrap getDraft();


}
