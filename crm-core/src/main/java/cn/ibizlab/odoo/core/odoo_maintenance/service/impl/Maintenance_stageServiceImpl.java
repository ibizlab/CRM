package cn.ibizlab.odoo.core.odoo_maintenance.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_stage;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_stageSearchContext;
import cn.ibizlab.odoo.core.odoo_maintenance.service.IMaintenance_stageService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_maintenance.client.maintenance_stageFeignClient;

/**
 * 实体[保养阶段] 服务对象接口实现
 */
@Slf4j
@Service
public class Maintenance_stageServiceImpl implements IMaintenance_stageService {

    @Autowired
    maintenance_stageFeignClient maintenance_stageFeignClient;


    @Override
    public boolean create(Maintenance_stage et) {
        Maintenance_stage rt = maintenance_stageFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Maintenance_stage> list){
        maintenance_stageFeignClient.createBatch(list) ;
    }

    @Override
    public Maintenance_stage get(Integer id) {
		Maintenance_stage et=maintenance_stageFeignClient.get(id);
        if(et==null){
            et=new Maintenance_stage();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Maintenance_stage et) {
        Maintenance_stage rt = maintenance_stageFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Maintenance_stage> list){
        maintenance_stageFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=maintenance_stageFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        maintenance_stageFeignClient.removeBatch(idList);
    }

    @Override
    public Maintenance_stage getDraft(Maintenance_stage et) {
        et=maintenance_stageFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Maintenance_stage> searchDefault(Maintenance_stageSearchContext context) {
        Page<Maintenance_stage> maintenance_stages=maintenance_stageFeignClient.searchDefault(context);
        return maintenance_stages;
    }


}


