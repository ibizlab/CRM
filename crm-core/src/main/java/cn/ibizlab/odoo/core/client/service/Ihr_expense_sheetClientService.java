package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ihr_expense_sheet;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_expense_sheet] 服务对象接口
 */
public interface Ihr_expense_sheetClientService{

    public Ihr_expense_sheet createModel() ;

    public void create(Ihr_expense_sheet hr_expense_sheet);

    public void createBatch(List<Ihr_expense_sheet> hr_expense_sheets);

    public void updateBatch(List<Ihr_expense_sheet> hr_expense_sheets);

    public void update(Ihr_expense_sheet hr_expense_sheet);

    public Page<Ihr_expense_sheet> fetchDefault(SearchContext context);

    public void get(Ihr_expense_sheet hr_expense_sheet);

    public void remove(Ihr_expense_sheet hr_expense_sheet);

    public void removeBatch(List<Ihr_expense_sheet> hr_expense_sheets);

    public Page<Ihr_expense_sheet> select(SearchContext context);

    public void getDraft(Ihr_expense_sheet hr_expense_sheet);

}
