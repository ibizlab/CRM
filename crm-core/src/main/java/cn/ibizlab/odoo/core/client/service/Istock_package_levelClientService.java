package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_package_level;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_package_level] 服务对象接口
 */
public interface Istock_package_levelClientService{

    public Istock_package_level createModel() ;

    public void createBatch(List<Istock_package_level> stock_package_levels);

    public void create(Istock_package_level stock_package_level);

    public void update(Istock_package_level stock_package_level);

    public void updateBatch(List<Istock_package_level> stock_package_levels);

    public void removeBatch(List<Istock_package_level> stock_package_levels);

    public Page<Istock_package_level> fetchDefault(SearchContext context);

    public void remove(Istock_package_level stock_package_level);

    public void get(Istock_package_level stock_package_level);

    public Page<Istock_package_level> select(SearchContext context);

    public void getDraft(Istock_package_level stock_package_level);

}
