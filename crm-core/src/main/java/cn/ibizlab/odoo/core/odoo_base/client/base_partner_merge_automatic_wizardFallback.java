package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_partner_merge_automatic_wizard;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_partner_merge_automatic_wizardSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[base_partner_merge_automatic_wizard] 服务对象接口
 */
@Component
public class base_partner_merge_automatic_wizardFallback implements base_partner_merge_automatic_wizardFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Base_partner_merge_automatic_wizard> searchDefault(Base_partner_merge_automatic_wizardSearchContext context){
            return null;
     }


    public Base_partner_merge_automatic_wizard update(Integer id, Base_partner_merge_automatic_wizard base_partner_merge_automatic_wizard){
            return null;
     }
    public Boolean updateBatch(List<Base_partner_merge_automatic_wizard> base_partner_merge_automatic_wizards){
            return false;
     }




    public Base_partner_merge_automatic_wizard get(Integer id){
            return null;
     }


    public Base_partner_merge_automatic_wizard create(Base_partner_merge_automatic_wizard base_partner_merge_automatic_wizard){
            return null;
     }
    public Boolean createBatch(List<Base_partner_merge_automatic_wizard> base_partner_merge_automatic_wizards){
            return false;
     }


    public Page<Base_partner_merge_automatic_wizard> select(){
            return null;
     }

    public Base_partner_merge_automatic_wizard getDraft(){
            return null;
    }



}
