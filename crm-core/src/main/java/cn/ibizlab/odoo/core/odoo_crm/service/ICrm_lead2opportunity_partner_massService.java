package cn.ibizlab.odoo.core.odoo_crm.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead2opportunity_partner_mass;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lead2opportunity_partner_massSearchContext;


/**
 * 实体[Crm_lead2opportunity_partner_mass] 服务对象接口
 */
public interface ICrm_lead2opportunity_partner_massService{

    Crm_lead2opportunity_partner_mass getDraft(Crm_lead2opportunity_partner_mass et) ;
    Crm_lead2opportunity_partner_mass get(Integer key) ;
    boolean create(Crm_lead2opportunity_partner_mass et) ;
    void createBatch(List<Crm_lead2opportunity_partner_mass> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Crm_lead2opportunity_partner_mass et) ;
    void updateBatch(List<Crm_lead2opportunity_partner_mass> list) ;
    Page<Crm_lead2opportunity_partner_mass> searchDefault(Crm_lead2opportunity_partner_massSearchContext context) ;

}



