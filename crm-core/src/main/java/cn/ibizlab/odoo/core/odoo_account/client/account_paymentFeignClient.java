package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_payment;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_paymentSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_payment] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-payment", fallback = account_paymentFallback.class)
public interface account_paymentFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/account_payments")
    Account_payment create(@RequestBody Account_payment account_payment);

    @RequestMapping(method = RequestMethod.POST, value = "/account_payments/batch")
    Boolean createBatch(@RequestBody List<Account_payment> account_payments);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_payments/{id}")
    Account_payment update(@PathVariable("id") Integer id,@RequestBody Account_payment account_payment);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_payments/batch")
    Boolean updateBatch(@RequestBody List<Account_payment> account_payments);


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_payments/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_payments/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);





    @RequestMapping(method = RequestMethod.POST, value = "/account_payments/searchdefault")
    Page<Account_payment> searchDefault(@RequestBody Account_paymentSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/account_payments/{id}")
    Account_payment get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/account_payments/select")
    Page<Account_payment> select();


    @RequestMapping(method = RequestMethod.POST, value = "/account_payments/checkkey")
    Boolean checkKey(@RequestBody Account_payment account_payment);


    @RequestMapping(method = RequestMethod.GET, value = "/account_payments/getdraft")
    Account_payment getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/account_payments/save")
    Boolean save(@RequestBody Account_payment account_payment);

    @RequestMapping(method = RequestMethod.POST, value = "/account_payments/save")
    Boolean saveBatch(@RequestBody List<Account_payment> account_payments);


}
