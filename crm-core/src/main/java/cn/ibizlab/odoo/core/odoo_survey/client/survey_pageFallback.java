package cn.ibizlab.odoo.core.odoo_survey.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_page;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_pageSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[survey_page] 服务对象接口
 */
@Component
public class survey_pageFallback implements survey_pageFeignClient{

    public Survey_page get(Integer id){
            return null;
     }



    public Survey_page update(Integer id, Survey_page survey_page){
            return null;
     }
    public Boolean updateBatch(List<Survey_page> survey_pages){
            return false;
     }




    public Page<Survey_page> searchDefault(Survey_pageSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Survey_page create(Survey_page survey_page){
            return null;
     }
    public Boolean createBatch(List<Survey_page> survey_pages){
            return false;
     }

    public Page<Survey_page> select(){
            return null;
     }

    public Survey_page getDraft(){
            return null;
    }



}
