package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_category;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_categorySearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[res_partner_category] 服务对象接口
 */
@FeignClient(value = "odoo-base", contextId = "res-partner-category", fallback = res_partner_categoryFallback.class)
public interface res_partner_categoryFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/res_partner_categories/{id}")
    Res_partner_category get(@PathVariable("id") Integer id);





    @RequestMapping(method = RequestMethod.POST, value = "/res_partner_categories/searchdefault")
    Page<Res_partner_category> searchDefault(@RequestBody Res_partner_categorySearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/res_partner_categories/{id}")
    Res_partner_category update(@PathVariable("id") Integer id,@RequestBody Res_partner_category res_partner_category);

    @RequestMapping(method = RequestMethod.PUT, value = "/res_partner_categories/batch")
    Boolean updateBatch(@RequestBody List<Res_partner_category> res_partner_categories);



    @RequestMapping(method = RequestMethod.POST, value = "/res_partner_categories")
    Res_partner_category create(@RequestBody Res_partner_category res_partner_category);

    @RequestMapping(method = RequestMethod.POST, value = "/res_partner_categories/batch")
    Boolean createBatch(@RequestBody List<Res_partner_category> res_partner_categories);


    @RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_categories/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_categories/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/res_partner_categories/select")
    Page<Res_partner_category> select();


    @RequestMapping(method = RequestMethod.GET, value = "/res_partner_categories/getdraft")
    Res_partner_category getDraft();


}
