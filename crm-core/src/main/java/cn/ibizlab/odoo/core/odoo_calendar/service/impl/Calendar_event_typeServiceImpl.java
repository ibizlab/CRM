package cn.ibizlab.odoo.core.odoo_calendar.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_event_type;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_event_typeSearchContext;
import cn.ibizlab.odoo.core.odoo_calendar.service.ICalendar_event_typeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_calendar.client.calendar_event_typeFeignClient;

/**
 * 实体[事件会议类型] 服务对象接口实现
 */
@Slf4j
@Service
public class Calendar_event_typeServiceImpl implements ICalendar_event_typeService {

    @Autowired
    calendar_event_typeFeignClient calendar_event_typeFeignClient;


    @Override
    public Calendar_event_type get(Integer id) {
		Calendar_event_type et=calendar_event_typeFeignClient.get(id);
        if(et==null){
            et=new Calendar_event_type();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Calendar_event_type et) {
        Calendar_event_type rt = calendar_event_typeFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Calendar_event_type> list){
        calendar_event_typeFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=calendar_event_typeFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        calendar_event_typeFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Calendar_event_type et) {
        Calendar_event_type rt = calendar_event_typeFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Calendar_event_type> list){
        calendar_event_typeFeignClient.updateBatch(list) ;
    }

    @Override
    public Calendar_event_type getDraft(Calendar_event_type et) {
        et=calendar_event_typeFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Calendar_event_type> searchDefault(Calendar_event_typeSearchContext context) {
        Page<Calendar_event_type> calendar_event_types=calendar_event_typeFeignClient.searchDefault(context);
        return calendar_event_types;
    }


}


