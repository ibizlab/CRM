package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoiceSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_invoice] 服务对象接口
 */
@Component
public class account_invoiceFallback implements account_invoiceFeignClient{

    public Account_invoice update(Integer id, Account_invoice account_invoice){
            return null;
     }
    public Boolean updateBatch(List<Account_invoice> account_invoices){
            return false;
     }


    public Page<Account_invoice> searchDefault(Account_invoiceSearchContext context){
            return null;
     }




    public Account_invoice create(Account_invoice account_invoice){
            return null;
     }
    public Boolean createBatch(List<Account_invoice> account_invoices){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Account_invoice get(Integer id){
            return null;
     }


    public Page<Account_invoice> select(){
            return null;
     }

    public Boolean checkKey(Account_invoice account_invoice){
            return false;
     }


    public Account_invoice getDraft(){
            return null;
    }



    public Boolean save(Account_invoice account_invoice){
            return false;
     }
    public Boolean saveBatch(List<Account_invoice> account_invoices){
            return false;
     }

}
