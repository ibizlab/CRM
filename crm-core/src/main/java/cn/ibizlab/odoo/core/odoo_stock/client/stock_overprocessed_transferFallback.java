package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_overprocessed_transfer;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_overprocessed_transferSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_overprocessed_transfer] 服务对象接口
 */
@Component
public class stock_overprocessed_transferFallback implements stock_overprocessed_transferFeignClient{

    public Stock_overprocessed_transfer create(Stock_overprocessed_transfer stock_overprocessed_transfer){
            return null;
     }
    public Boolean createBatch(List<Stock_overprocessed_transfer> stock_overprocessed_transfers){
            return false;
     }

    public Stock_overprocessed_transfer get(Integer id){
            return null;
     }



    public Stock_overprocessed_transfer update(Integer id, Stock_overprocessed_transfer stock_overprocessed_transfer){
            return null;
     }
    public Boolean updateBatch(List<Stock_overprocessed_transfer> stock_overprocessed_transfers){
            return false;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Page<Stock_overprocessed_transfer> searchDefault(Stock_overprocessed_transferSearchContext context){
            return null;
     }


    public Page<Stock_overprocessed_transfer> select(){
            return null;
     }

    public Stock_overprocessed_transfer getDraft(){
            return null;
    }



}
