package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Inote_stage;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[note_stage] 服务对象接口
 */
public interface Inote_stageClientService{

    public Inote_stage createModel() ;

    public void removeBatch(List<Inote_stage> note_stages);

    public void get(Inote_stage note_stage);

    public void update(Inote_stage note_stage);

    public void create(Inote_stage note_stage);

    public void remove(Inote_stage note_stage);

    public Page<Inote_stage> fetchDefault(SearchContext context);

    public void updateBatch(List<Inote_stage> note_stages);

    public void createBatch(List<Inote_stage> note_stages);

    public Page<Inote_stage> select(SearchContext context);

    public void getDraft(Inote_stage note_stage);

}
