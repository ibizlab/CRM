package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [base_partner_merge_automatic_wizard] 对象
 */
public interface Ibase_partner_merge_automatic_wizard {

    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [当前行]
     */
    public void setCurrent_line_id(Integer current_line_id);
    
    /**
     * 设置 [当前行]
     */
    public Integer getCurrent_line_id();

    /**
     * 获取 [当前行]脏标记
     */
    public boolean getCurrent_line_idDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [目的地之联系人]
     */
    public void setDst_partner_id(Integer dst_partner_id);
    
    /**
     * 设置 [目的地之联系人]
     */
    public Integer getDst_partner_id();

    /**
     * 获取 [目的地之联系人]脏标记
     */
    public boolean getDst_partner_idDirtyFlag();
    /**
     * 获取 [目的地之联系人]
     */
    public void setDst_partner_id_text(String dst_partner_id_text);
    
    /**
     * 设置 [目的地之联系人]
     */
    public String getDst_partner_id_text();

    /**
     * 获取 [目的地之联系人]脏标记
     */
    public boolean getDst_partner_id_textDirtyFlag();
    /**
     * 获取 [与系统用户相关的联系人]
     */
    public void setExclude_contact(String exclude_contact);
    
    /**
     * 设置 [与系统用户相关的联系人]
     */
    public String getExclude_contact();

    /**
     * 获取 [与系统用户相关的联系人]脏标记
     */
    public boolean getExclude_contactDirtyFlag();
    /**
     * 获取 [与日记账相关的联系人]
     */
    public void setExclude_journal_item(String exclude_journal_item);
    
    /**
     * 设置 [与日记账相关的联系人]
     */
    public String getExclude_journal_item();

    /**
     * 获取 [与日记账相关的联系人]脏标记
     */
    public boolean getExclude_journal_itemDirtyFlag();
    /**
     * 获取 [EMail]
     */
    public void setGroup_by_email(String group_by_email);
    
    /**
     * 设置 [EMail]
     */
    public String getGroup_by_email();

    /**
     * 获取 [EMail]脏标记
     */
    public boolean getGroup_by_emailDirtyFlag();
    /**
     * 获取 [是公司]
     */
    public void setGroup_by_is_company(String group_by_is_company);
    
    /**
     * 设置 [是公司]
     */
    public String getGroup_by_is_company();

    /**
     * 获取 [是公司]脏标记
     */
    public boolean getGroup_by_is_companyDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setGroup_by_name(String group_by_name);
    
    /**
     * 设置 [名称]
     */
    public String getGroup_by_name();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getGroup_by_nameDirtyFlag();
    /**
     * 获取 [上级公司]
     */
    public void setGroup_by_parent_id(String group_by_parent_id);
    
    /**
     * 设置 [上级公司]
     */
    public String getGroup_by_parent_id();

    /**
     * 获取 [上级公司]脏标记
     */
    public boolean getGroup_by_parent_idDirtyFlag();
    /**
     * 获取 [增值税]
     */
    public void setGroup_by_vat(String group_by_vat);
    
    /**
     * 设置 [增值税]
     */
    public String getGroup_by_vat();

    /**
     * 获取 [增值税]脏标记
     */
    public boolean getGroup_by_vatDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [明细行]
     */
    public void setLine_ids(String line_ids);
    
    /**
     * 设置 [明细行]
     */
    public String getLine_ids();

    /**
     * 获取 [明细行]脏标记
     */
    public boolean getLine_idsDirtyFlag();
    /**
     * 获取 [联系人组的最多联系人数量]
     */
    public void setMaximum_group(Integer maximum_group);
    
    /**
     * 设置 [联系人组的最多联系人数量]
     */
    public Integer getMaximum_group();

    /**
     * 获取 [联系人组的最多联系人数量]脏标记
     */
    public boolean getMaximum_groupDirtyFlag();
    /**
     * 获取 [联系人组]
     */
    public void setNumber_group(Integer number_group);
    
    /**
     * 设置 [联系人组]
     */
    public Integer getNumber_group();

    /**
     * 获取 [联系人组]脏标记
     */
    public boolean getNumber_groupDirtyFlag();
    /**
     * 获取 [联系人]
     */
    public void setPartner_ids(String partner_ids);
    
    /**
     * 设置 [联系人]
     */
    public String getPartner_ids();

    /**
     * 获取 [联系人]脏标记
     */
    public boolean getPartner_idsDirtyFlag();
    /**
     * 获取 [省/ 州]
     */
    public void setState(String state);
    
    /**
     * 设置 [省/ 州]
     */
    public String getState();

    /**
     * 获取 [省/ 州]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
