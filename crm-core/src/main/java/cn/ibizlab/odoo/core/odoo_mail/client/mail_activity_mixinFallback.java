package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity_mixin;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_activity_mixinSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_activity_mixin] 服务对象接口
 */
@Component
public class mail_activity_mixinFallback implements mail_activity_mixinFeignClient{

    public Mail_activity_mixin create(Mail_activity_mixin mail_activity_mixin){
            return null;
     }
    public Boolean createBatch(List<Mail_activity_mixin> mail_activity_mixins){
            return false;
     }

    public Page<Mail_activity_mixin> searchDefault(Mail_activity_mixinSearchContext context){
            return null;
     }


    public Mail_activity_mixin get(Integer id){
            return null;
     }



    public Mail_activity_mixin update(Integer id, Mail_activity_mixin mail_activity_mixin){
            return null;
     }
    public Boolean updateBatch(List<Mail_activity_mixin> mail_activity_mixins){
            return false;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Page<Mail_activity_mixin> select(){
            return null;
     }

    public Mail_activity_mixin getDraft(){
            return null;
    }



}
