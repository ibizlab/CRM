package cn.ibizlab.odoo.core.odoo_website.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_seo_metadata;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_seo_metadataSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[website_seo_metadata] 服务对象接口
 */
@Component
public class website_seo_metadataFallback implements website_seo_metadataFeignClient{


    public Page<Website_seo_metadata> searchDefault(Website_seo_metadataSearchContext context){
            return null;
     }


    public Website_seo_metadata create(Website_seo_metadata website_seo_metadata){
            return null;
     }
    public Boolean createBatch(List<Website_seo_metadata> website_seo_metadata){
            return false;
     }

    public Website_seo_metadata get(Integer id){
            return null;
     }




    public Website_seo_metadata update(Integer id, Website_seo_metadata website_seo_metadata){
            return null;
     }
    public Boolean updateBatch(List<Website_seo_metadata> website_seo_metadata){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Website_seo_metadata> select(){
            return null;
     }

    public Website_seo_metadata getDraft(){
            return null;
    }



}
