package cn.ibizlab.odoo.core.odoo_utm.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [UTM Mixin] 对象
 */
@Data
public class Utm_mixin extends EntityClient implements Serializable {

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 媒体
     */
    @JSONField(name = "medium_id_text")
    @JsonProperty("medium_id_text")
    private String mediumIdText;

    /**
     * 来源
     */
    @JSONField(name = "source_id_text")
    @JsonProperty("source_id_text")
    private String sourceIdText;

    /**
     * 营销
     */
    @JSONField(name = "campaign_id_text")
    @JsonProperty("campaign_id_text")
    private String campaignIdText;

    /**
     * 媒体
     */
    @DEField(name = "medium_id")
    @JSONField(name = "medium_id")
    @JsonProperty("medium_id")
    private Integer mediumId;

    /**
     * 来源
     */
    @DEField(name = "source_id")
    @JSONField(name = "source_id")
    @JsonProperty("source_id")
    private Integer sourceId;

    /**
     * 营销
     */
    @DEField(name = "campaign_id")
    @JSONField(name = "campaign_id")
    @JsonProperty("campaign_id")
    private Integer campaignId;


    /**
     * 
     */
    @JSONField(name = "odoocampaign")
    @JsonProperty("odoocampaign")
    private cn.ibizlab.odoo.core.odoo_utm.domain.Utm_campaign odooCampaign;

    /**
     * 
     */
    @JSONField(name = "odoomedium")
    @JsonProperty("odoomedium")
    private cn.ibizlab.odoo.core.odoo_utm.domain.Utm_medium odooMedium;

    /**
     * 
     */
    @JSONField(name = "odoosource")
    @JsonProperty("odoosource")
    private cn.ibizlab.odoo.core.odoo_utm.domain.Utm_source odooSource;




    /**
     * 设置 [媒体]
     */
    public void setMediumId(Integer mediumId){
        this.mediumId = mediumId ;
        this.modify("medium_id",mediumId);
    }
    /**
     * 设置 [来源]
     */
    public void setSourceId(Integer sourceId){
        this.sourceId = sourceId ;
        this.modify("source_id",sourceId);
    }
    /**
     * 设置 [营销]
     */
    public void setCampaignId(Integer campaignId){
        this.campaignId = campaignId ;
        this.modify("campaign_id",campaignId);
    }

}


