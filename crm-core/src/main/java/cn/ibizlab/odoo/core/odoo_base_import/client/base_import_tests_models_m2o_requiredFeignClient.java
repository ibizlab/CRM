package cn.ibizlab.odoo.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_m2o_required;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_m2o_requiredSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_import_tests_models_m2o_required] 服务对象接口
 */
@FeignClient(value = "odoo-base-import", contextId = "base-import-tests-models-m2o-required", fallback = base_import_tests_models_m2o_requiredFallback.class)
public interface base_import_tests_models_m2o_requiredFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_m2o_requireds")
    Base_import_tests_models_m2o_required create(@RequestBody Base_import_tests_models_m2o_required base_import_tests_models_m2o_required);

    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_m2o_requireds/batch")
    Boolean createBatch(@RequestBody List<Base_import_tests_models_m2o_required> base_import_tests_models_m2o_requireds);



    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_m2o_requireds/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_m2o_requireds/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_m2o_requireds/searchdefault")
    Page<Base_import_tests_models_m2o_required> searchDefault(@RequestBody Base_import_tests_models_m2o_requiredSearchContext context);



    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_m2o_requireds/{id}")
    Base_import_tests_models_m2o_required update(@PathVariable("id") Integer id,@RequestBody Base_import_tests_models_m2o_required base_import_tests_models_m2o_required);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_m2o_requireds/batch")
    Boolean updateBatch(@RequestBody List<Base_import_tests_models_m2o_required> base_import_tests_models_m2o_requireds);


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_m2o_requireds/{id}")
    Base_import_tests_models_m2o_required get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_m2o_requireds/select")
    Page<Base_import_tests_models_m2o_required> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_m2o_requireds/getdraft")
    Base_import_tests_models_m2o_required getDraft();


}
