package cn.ibizlab.odoo.core.odoo_sale.valuerule.validator.sale_order_line;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import cn.ibizlab.odoo.util.valuerule.DefaultValueRule;
import cn.ibizlab.odoo.util.valuerule.VRCondition;
import cn.ibizlab.odoo.util.valuerule.condition.*;
import cn.ibizlab.odoo.core.odoo_sale.valuerule.anno.sale_order_line.Sale_order_linePrice_reduce_taxexclDefault;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 值规则注解解析类
 * 实体：Sale_order_line
 * 属性：Price_reduce_taxexcl
 * 值规则：Default
 * 值规则信息：默认规则
 */
@Slf4j
@IBIZLog
@Component("Sale_order_linePrice_reduce_taxexclDefaultValidator")
public class Sale_order_linePrice_reduce_taxexclDefaultValidator implements ConstraintValidator<Sale_order_linePrice_reduce_taxexclDefault, Double>,Validator {
    private static final String MESSAGE = "值规则校验失败：【默认规则】";

    @Override
    public boolean isValid(Double value, ConstraintValidatorContext context) {
        boolean isValid = doValidate(value);
        if(!isValid) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(MESSAGE)
                    .addConstraintViolation();
        }
        return doValidate(value);
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }

    @Override
    public void validate(Object o, Errors errors) {
        if( o!=null && supports(o.getClass())){
            if (!doValidate((Double) o)){
                errors.reject(MESSAGE);
            }
        }
    }

    public boolean doValidate(Double value) {
        DefaultValueRule<Double> valueRule = new DefaultValueRule<>("默认值规则",MESSAGE,"Price_reduce_taxexcl",value)
                //字符串长度，重复检查模式，重复值范围，基础值规则，是否递归检查。
                .init(-1,"NONE",null,null,false);
        return valueRule.isValid();
    }
}

