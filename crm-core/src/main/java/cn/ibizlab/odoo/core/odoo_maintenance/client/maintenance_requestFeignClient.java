package cn.ibizlab.odoo.core.odoo_maintenance.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_request;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_requestSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[maintenance_request] 服务对象接口
 */
@FeignClient(value = "odoo-maintenance", contextId = "maintenance-request", fallback = maintenance_requestFallback.class)
public interface maintenance_requestFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/maintenance_requests")
    Maintenance_request create(@RequestBody Maintenance_request maintenance_request);

    @RequestMapping(method = RequestMethod.POST, value = "/maintenance_requests/batch")
    Boolean createBatch(@RequestBody List<Maintenance_request> maintenance_requests);


    @RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_requests/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_requests/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/maintenance_requests/{id}")
    Maintenance_request get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/maintenance_requests/searchdefault")
    Page<Maintenance_request> searchDefault(@RequestBody Maintenance_requestSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/maintenance_requests/{id}")
    Maintenance_request update(@PathVariable("id") Integer id,@RequestBody Maintenance_request maintenance_request);

    @RequestMapping(method = RequestMethod.PUT, value = "/maintenance_requests/batch")
    Boolean updateBatch(@RequestBody List<Maintenance_request> maintenance_requests);



    @RequestMapping(method = RequestMethod.GET, value = "/maintenance_requests/select")
    Page<Maintenance_request> select();


    @RequestMapping(method = RequestMethod.GET, value = "/maintenance_requests/getdraft")
    Maintenance_request getDraft();


}
