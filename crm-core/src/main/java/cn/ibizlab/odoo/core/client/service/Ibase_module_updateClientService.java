package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ibase_module_update;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_module_update] 服务对象接口
 */
public interface Ibase_module_updateClientService{

    public Ibase_module_update createModel() ;

    public void updateBatch(List<Ibase_module_update> base_module_updates);

    public void update(Ibase_module_update base_module_update);

    public void createBatch(List<Ibase_module_update> base_module_updates);

    public void create(Ibase_module_update base_module_update);

    public void remove(Ibase_module_update base_module_update);

    public Page<Ibase_module_update> fetchDefault(SearchContext context);

    public void get(Ibase_module_update base_module_update);

    public void removeBatch(List<Ibase_module_update> base_module_updates);

    public Page<Ibase_module_update> select(SearchContext context);

    public void getDraft(Ibase_module_update base_module_update);

}
