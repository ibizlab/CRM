package cn.ibizlab.odoo.core.odoo_maintenance.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_stage;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_stageSearchContext;


/**
 * 实体[Maintenance_stage] 服务对象接口
 */
public interface IMaintenance_stageService{

    boolean create(Maintenance_stage et) ;
    void createBatch(List<Maintenance_stage> list) ;
    Maintenance_stage get(Integer key) ;
    boolean update(Maintenance_stage et) ;
    void updateBatch(List<Maintenance_stage> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Maintenance_stage getDraft(Maintenance_stage et) ;
    Page<Maintenance_stage> searchDefault(Maintenance_stageSearchContext context) ;

}



