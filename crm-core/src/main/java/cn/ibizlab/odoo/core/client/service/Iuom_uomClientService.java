package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iuom_uom;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[uom_uom] 服务对象接口
 */
public interface Iuom_uomClientService{

    public Iuom_uom createModel() ;

    public void createBatch(List<Iuom_uom> uom_uoms);

    public void remove(Iuom_uom uom_uom);

    public void updateBatch(List<Iuom_uom> uom_uoms);

    public void create(Iuom_uom uom_uom);

    public Page<Iuom_uom> fetchDefault(SearchContext context);

    public void get(Iuom_uom uom_uom);

    public void removeBatch(List<Iuom_uom> uom_uoms);

    public void update(Iuom_uom uom_uom);

    public Page<Iuom_uom> select(SearchContext context);

    public void getDraft(Iuom_uom uom_uom);

}
