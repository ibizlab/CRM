package cn.ibizlab.odoo.core.odoo_note.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_note.domain.Note_stage;
import cn.ibizlab.odoo.core.odoo_note.filter.Note_stageSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[note_stage] 服务对象接口
 */
@FeignClient(value = "odoo-note", contextId = "note-stage", fallback = note_stageFallback.class)
public interface note_stageFeignClient {


    @RequestMapping(method = RequestMethod.GET, value = "/note_stages/{id}")
    Note_stage get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/note_stages/{id}")
    Note_stage update(@PathVariable("id") Integer id,@RequestBody Note_stage note_stage);

    @RequestMapping(method = RequestMethod.PUT, value = "/note_stages/batch")
    Boolean updateBatch(@RequestBody List<Note_stage> note_stages);


    @RequestMapping(method = RequestMethod.POST, value = "/note_stages")
    Note_stage create(@RequestBody Note_stage note_stage);

    @RequestMapping(method = RequestMethod.POST, value = "/note_stages/batch")
    Boolean createBatch(@RequestBody List<Note_stage> note_stages);


    @RequestMapping(method = RequestMethod.DELETE, value = "/note_stages/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/note_stages/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/note_stages/searchdefault")
    Page<Note_stage> searchDefault(@RequestBody Note_stageSearchContext context);




    @RequestMapping(method = RequestMethod.GET, value = "/note_stages/select")
    Page<Note_stage> select();


    @RequestMapping(method = RequestMethod.GET, value = "/note_stages/getdraft")
    Note_stage getDraft();


}
