package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_unreconcile;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_unreconcileSearchContext;


/**
 * 实体[Account_unreconcile] 服务对象接口
 */
public interface IAccount_unreconcileService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Account_unreconcile get(Integer key) ;
    boolean update(Account_unreconcile et) ;
    void updateBatch(List<Account_unreconcile> list) ;
    boolean create(Account_unreconcile et) ;
    void createBatch(List<Account_unreconcile> list) ;
    Account_unreconcile getDraft(Account_unreconcile et) ;
    Page<Account_unreconcile> searchDefault(Account_unreconcileSearchContext context) ;

}



