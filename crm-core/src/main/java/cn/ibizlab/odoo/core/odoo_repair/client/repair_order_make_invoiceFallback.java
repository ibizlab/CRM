package cn.ibizlab.odoo.core.odoo_repair.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_order_make_invoice;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_order_make_invoiceSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[repair_order_make_invoice] 服务对象接口
 */
@Component
public class repair_order_make_invoiceFallback implements repair_order_make_invoiceFeignClient{

    public Repair_order_make_invoice update(Integer id, Repair_order_make_invoice repair_order_make_invoice){
            return null;
     }
    public Boolean updateBatch(List<Repair_order_make_invoice> repair_order_make_invoices){
            return false;
     }


    public Repair_order_make_invoice create(Repair_order_make_invoice repair_order_make_invoice){
            return null;
     }
    public Boolean createBatch(List<Repair_order_make_invoice> repair_order_make_invoices){
            return false;
     }

    public Repair_order_make_invoice get(Integer id){
            return null;
     }





    public Page<Repair_order_make_invoice> searchDefault(Repair_order_make_invoiceSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Repair_order_make_invoice> select(){
            return null;
     }

    public Repair_order_make_invoice getDraft(){
            return null;
    }



}
