package cn.ibizlab.odoo.core.odoo_digest.valuerule.anno.digest_digest;

import cn.ibizlab.odoo.core.odoo_digest.valuerule.validator.digest_digest.Digest_digestIs_subscribedDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Digest_digest
 * 属性：Is_subscribed
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Digest_digestIs_subscribedDefaultValidator.class})
public @interface Digest_digestIs_subscribedDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
