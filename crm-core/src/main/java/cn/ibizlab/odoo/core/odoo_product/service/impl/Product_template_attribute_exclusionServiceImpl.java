package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_template_attribute_exclusion;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_template_attribute_exclusionSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_template_attribute_exclusionService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_product.client.product_template_attribute_exclusionFeignClient;

/**
 * 实体[产品模板属性排除] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_template_attribute_exclusionServiceImpl implements IProduct_template_attribute_exclusionService {

    @Autowired
    product_template_attribute_exclusionFeignClient product_template_attribute_exclusionFeignClient;


    @Override
    public boolean create(Product_template_attribute_exclusion et) {
        Product_template_attribute_exclusion rt = product_template_attribute_exclusionFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_template_attribute_exclusion> list){
        product_template_attribute_exclusionFeignClient.createBatch(list) ;
    }

    @Override
    public Product_template_attribute_exclusion get(Integer id) {
		Product_template_attribute_exclusion et=product_template_attribute_exclusionFeignClient.get(id);
        if(et==null){
            et=new Product_template_attribute_exclusion();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Product_template_attribute_exclusion et) {
        Product_template_attribute_exclusion rt = product_template_attribute_exclusionFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Product_template_attribute_exclusion> list){
        product_template_attribute_exclusionFeignClient.updateBatch(list) ;
    }

    @Override
    public Product_template_attribute_exclusion getDraft(Product_template_attribute_exclusion et) {
        et=product_template_attribute_exclusionFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=product_template_attribute_exclusionFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        product_template_attribute_exclusionFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_template_attribute_exclusion> searchDefault(Product_template_attribute_exclusionSearchContext context) {
        Page<Product_template_attribute_exclusion> product_template_attribute_exclusions=product_template_attribute_exclusionFeignClient.searchDefault(context);
        return product_template_attribute_exclusions;
    }


}


