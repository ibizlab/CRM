package cn.ibizlab.odoo.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_recruitment_stage;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_recruitment_stageSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[hr_recruitment_stage] 服务对象接口
 */
@Component
public class hr_recruitment_stageFallback implements hr_recruitment_stageFeignClient{


    public Page<Hr_recruitment_stage> searchDefault(Hr_recruitment_stageSearchContext context){
            return null;
     }


    public Hr_recruitment_stage create(Hr_recruitment_stage hr_recruitment_stage){
            return null;
     }
    public Boolean createBatch(List<Hr_recruitment_stage> hr_recruitment_stages){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Hr_recruitment_stage get(Integer id){
            return null;
     }



    public Hr_recruitment_stage update(Integer id, Hr_recruitment_stage hr_recruitment_stage){
            return null;
     }
    public Boolean updateBatch(List<Hr_recruitment_stage> hr_recruitment_stages){
            return false;
     }



    public Page<Hr_recruitment_stage> select(){
            return null;
     }

    public Hr_recruitment_stage getDraft(){
            return null;
    }



}
