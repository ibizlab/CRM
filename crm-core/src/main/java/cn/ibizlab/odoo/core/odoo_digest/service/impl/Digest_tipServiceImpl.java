package cn.ibizlab.odoo.core.odoo_digest.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_digest.domain.Digest_tip;
import cn.ibizlab.odoo.core.odoo_digest.filter.Digest_tipSearchContext;
import cn.ibizlab.odoo.core.odoo_digest.service.IDigest_tipService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_digest.client.digest_tipFeignClient;

/**
 * 实体[摘要提示] 服务对象接口实现
 */
@Slf4j
@Service
public class Digest_tipServiceImpl implements IDigest_tipService {

    @Autowired
    digest_tipFeignClient digest_tipFeignClient;


    @Override
    public boolean create(Digest_tip et) {
        Digest_tip rt = digest_tipFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Digest_tip> list){
        digest_tipFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=digest_tipFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        digest_tipFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Digest_tip et) {
        Digest_tip rt = digest_tipFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Digest_tip> list){
        digest_tipFeignClient.updateBatch(list) ;
    }

    @Override
    public Digest_tip getDraft(Digest_tip et) {
        et=digest_tipFeignClient.getDraft();
        return et;
    }

    @Override
    public Digest_tip get(Integer id) {
		Digest_tip et=digest_tipFeignClient.get(id);
        if(et==null){
            et=new Digest_tip();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Digest_tip> searchDefault(Digest_tipSearchContext context) {
        Page<Digest_tip> digest_tips=digest_tipFeignClient.searchDefault(context);
        return digest_tips;
    }


}


