package cn.ibizlab.odoo.core.odoo_asset.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_asset.domain.Asset_state;
import cn.ibizlab.odoo.core.odoo_asset.filter.Asset_stateSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[asset_state] 服务对象接口
 */
@Component
public class asset_stateFallback implements asset_stateFeignClient{


    public Asset_state get(Integer id){
            return null;
     }


    public Asset_state create(Asset_state asset_state){
            return null;
     }
    public Boolean createBatch(List<Asset_state> asset_states){
            return false;
     }

    public Page<Asset_state> searchDefault(Asset_stateSearchContext context){
            return null;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Asset_state update(Integer id, Asset_state asset_state){
            return null;
     }
    public Boolean updateBatch(List<Asset_state> asset_states){
            return false;
     }


    public Page<Asset_state> select(){
            return null;
     }

    public Asset_state getDraft(){
            return null;
    }



}
