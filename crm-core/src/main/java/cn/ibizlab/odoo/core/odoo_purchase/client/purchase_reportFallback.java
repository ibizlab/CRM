package cn.ibizlab.odoo.core.odoo_purchase.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_report;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_reportSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[purchase_report] 服务对象接口
 */
@Component
public class purchase_reportFallback implements purchase_reportFeignClient{


    public Purchase_report update(Integer id, Purchase_report purchase_report){
            return null;
     }
    public Boolean updateBatch(List<Purchase_report> purchase_reports){
            return false;
     }



    public Purchase_report get(Integer id){
            return null;
     }


    public Purchase_report create(Purchase_report purchase_report){
            return null;
     }
    public Boolean createBatch(List<Purchase_report> purchase_reports){
            return false;
     }

    public Page<Purchase_report> searchDefault(Purchase_reportSearchContext context){
            return null;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Purchase_report> select(){
            return null;
     }

    public Purchase_report getDraft(){
            return null;
    }



}
