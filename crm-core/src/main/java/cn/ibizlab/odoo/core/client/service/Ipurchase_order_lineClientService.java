package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ipurchase_order_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[purchase_order_line] 服务对象接口
 */
public interface Ipurchase_order_lineClientService{

    public Ipurchase_order_line createModel() ;

    public void createBatch(List<Ipurchase_order_line> purchase_order_lines);

    public void updateBatch(List<Ipurchase_order_line> purchase_order_lines);

    public void remove(Ipurchase_order_line purchase_order_line);

    public Page<Ipurchase_order_line> fetchDefault(SearchContext context);

    public void removeBatch(List<Ipurchase_order_line> purchase_order_lines);

    public void get(Ipurchase_order_line purchase_order_line);

    public void create(Ipurchase_order_line purchase_order_line);

    public void update(Ipurchase_order_line purchase_order_line);

    public Page<Ipurchase_order_line> select(SearchContext context);

    public void getDraft(Ipurchase_order_line purchase_order_line);

}
