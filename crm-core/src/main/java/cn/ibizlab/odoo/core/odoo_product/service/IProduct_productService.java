package cn.ibizlab.odoo.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_product.domain.Product_product;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_productSearchContext;


/**
 * 实体[Product_product] 服务对象接口
 */
public interface IProduct_productService{

    Product_product get(Integer key) ;
    boolean save(Product_product et) ;
    void saveBatch(List<Product_product> list) ;
    boolean create(Product_product et) ;
    void createBatch(List<Product_product> list) ;
    boolean checkKey(Product_product et) ;
    boolean update(Product_product et) ;
    void updateBatch(List<Product_product> list) ;
    Product_product getDraft(Product_product et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Product_product> searchDefault(Product_productSearchContext context) ;

}



