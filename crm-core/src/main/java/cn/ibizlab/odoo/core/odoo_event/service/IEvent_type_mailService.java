package cn.ibizlab.odoo.core.odoo_event.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_event.domain.Event_type_mail;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_type_mailSearchContext;


/**
 * 实体[Event_type_mail] 服务对象接口
 */
public interface IEvent_type_mailService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Event_type_mail et) ;
    void createBatch(List<Event_type_mail> list) ;
    boolean update(Event_type_mail et) ;
    void updateBatch(List<Event_type_mail> list) ;
    Event_type_mail getDraft(Event_type_mail et) ;
    Event_type_mail get(Integer key) ;
    Page<Event_type_mail> searchDefault(Event_type_mailSearchContext context) ;

}



