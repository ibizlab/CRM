package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Res_country_state;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_country_stateSearchContext;


/**
 * 实体[Res_country_state] 服务对象接口
 */
public interface IRes_country_stateService{

    Res_country_state get(Integer key) ;
    boolean update(Res_country_state et) ;
    void updateBatch(List<Res_country_state> list) ;
    boolean create(Res_country_state et) ;
    void createBatch(List<Res_country_state> list) ;
    Res_country_state getDraft(Res_country_state et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Res_country_state> searchDefault(Res_country_stateSearchContext context) ;

}



