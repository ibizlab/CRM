package cn.ibizlab.odoo.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_contract_type;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_contract_typeSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[hr_contract_type] 服务对象接口
 */
@FeignClient(value = "odoo-hr", contextId = "hr-contract-type", fallback = hr_contract_typeFallback.class)
public interface hr_contract_typeFeignClient {


    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_contract_types/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_contract_types/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);




    @RequestMapping(method = RequestMethod.PUT, value = "/hr_contract_types/{id}")
    Hr_contract_type update(@PathVariable("id") Integer id,@RequestBody Hr_contract_type hr_contract_type);

    @RequestMapping(method = RequestMethod.PUT, value = "/hr_contract_types/batch")
    Boolean updateBatch(@RequestBody List<Hr_contract_type> hr_contract_types);


    @RequestMapping(method = RequestMethod.POST, value = "/hr_contract_types")
    Hr_contract_type create(@RequestBody Hr_contract_type hr_contract_type);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_contract_types/batch")
    Boolean createBatch(@RequestBody List<Hr_contract_type> hr_contract_types);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_contract_types/searchdefault")
    Page<Hr_contract_type> searchDefault(@RequestBody Hr_contract_typeSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/hr_contract_types/{id}")
    Hr_contract_type get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/hr_contract_types/select")
    Page<Hr_contract_type> select();


    @RequestMapping(method = RequestMethod.GET, value = "/hr_contract_types/getdraft")
    Hr_contract_type getDraft();


}
