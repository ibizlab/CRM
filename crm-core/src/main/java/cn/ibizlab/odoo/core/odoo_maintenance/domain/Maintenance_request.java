package cn.ibizlab.odoo.core.odoo_maintenance.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [保养请求] 对象
 */
@Data
public class Maintenance_request extends EntityClient implements Serializable {

    /**
     * 需要采取行动
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 主题
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 下一活动截止日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 下一活动摘要
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;

    /**
     * 持续时间
     */
    @JSONField(name = "duration")
    @JsonProperty("duration")
    private Double duration;

    /**
     * 行动数量
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 计划日期
     */
    @DEField(name = "schedule_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "schedule_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("schedule_date")
    private Timestamp scheduleDate;

    /**
     * 看板状态
     */
    @DEField(name = "kanban_state")
    @JSONField(name = "kanban_state")
    @JsonProperty("kanban_state")
    private String kanbanState;

    /**
     * 关注者(业务伙伴)
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 是关注者
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 保养类型
     */
    @DEField(name = "maintenance_type")
    @JSONField(name = "maintenance_type")
    @JsonProperty("maintenance_type")
    private String maintenanceType;

    /**
     * 网站消息
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 未读消息
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 消息递送错误
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 关闭日期
     */
    @DEField(name = "close_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "close_date" , format="yyyy-MM-dd")
    @JsonProperty("close_date")
    private Timestamp closeDate;

    /**
     * 活动
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;

    /**
     * 未读消息计数器
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 责任用户
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 活动状态
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;

    /**
     * 下一活动类型
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 关注者
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 请求日期
     */
    @DEField(name = "request_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "request_date" , format="yyyy-MM-dd")
    @JsonProperty("request_date")
    private Timestamp requestDate;

    /**
     * 归档
     */
    @JSONField(name = "archive")
    @JsonProperty("archive")
    private String archive;

    /**
     * 消息
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 错误数
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 颜色索引
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 关注者(渠道)
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 优先级
     */
    @JSONField(name = "priority")
    @JsonProperty("priority")
    private String priority;

    /**
     * 说明
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 附件数量
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 创建人
     */
    @JSONField(name = "owner_user_id_text")
    @JsonProperty("owner_user_id_text")
    private String ownerUserIdText;

    /**
     * 员工
     */
    @JSONField(name = "employee_id_text")
    @JsonProperty("employee_id_text")
    private String employeeIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 团队
     */
    @JSONField(name = "maintenance_team_id_text")
    @JsonProperty("maintenance_team_id_text")
    private String maintenanceTeamIdText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 设备
     */
    @JSONField(name = "equipment_id_text")
    @JsonProperty("equipment_id_text")
    private String equipmentIdText;

    /**
     * 阶段
     */
    @JSONField(name = "stage_id_text")
    @JsonProperty("stage_id_text")
    private String stageIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 类别
     */
    @JSONField(name = "category_id_text")
    @JsonProperty("category_id_text")
    private String categoryIdText;

    /**
     * 技术员
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 部门
     */
    @JSONField(name = "department_id_text")
    @JsonProperty("department_id_text")
    private String departmentIdText;

    /**
     * 员工
     */
    @DEField(name = "employee_id")
    @JSONField(name = "employee_id")
    @JsonProperty("employee_id")
    private Integer employeeId;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 设备
     */
    @DEField(name = "equipment_id")
    @JSONField(name = "equipment_id")
    @JsonProperty("equipment_id")
    private Integer equipmentId;

    /**
     * 阶段
     */
    @DEField(name = "stage_id")
    @JSONField(name = "stage_id")
    @JsonProperty("stage_id")
    private Integer stageId;

    /**
     * 创建人
     */
    @DEField(name = "owner_user_id")
    @JSONField(name = "owner_user_id")
    @JsonProperty("owner_user_id")
    private Integer ownerUserId;

    /**
     * 类别
     */
    @DEField(name = "category_id")
    @JSONField(name = "category_id")
    @JsonProperty("category_id")
    private Integer categoryId;

    /**
     * 部门
     */
    @DEField(name = "department_id")
    @JSONField(name = "department_id")
    @JsonProperty("department_id")
    private Integer departmentId;

    /**
     * 技术员
     */
    @DEField(name = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 团队
     */
    @DEField(name = "maintenance_team_id")
    @JSONField(name = "maintenance_team_id")
    @JsonProperty("maintenance_team_id")
    private Integer maintenanceTeamId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 
     */
    @JSONField(name = "odoodepartment")
    @JsonProperty("odoodepartment")
    private cn.ibizlab.odoo.core.odoo_hr.domain.Hr_department odooDepartment;

    /**
     * 
     */
    @JSONField(name = "odooemployee")
    @JsonProperty("odooemployee")
    private cn.ibizlab.odoo.core.odoo_hr.domain.Hr_employee odooEmployee;

    /**
     * 
     */
    @JSONField(name = "odoocategory")
    @JsonProperty("odoocategory")
    private cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_equipment_category odooCategory;

    /**
     * 
     */
    @JSONField(name = "odooequipment")
    @JsonProperty("odooequipment")
    private cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_equipment odooEquipment;

    /**
     * 
     */
    @JSONField(name = "odoostage")
    @JsonProperty("odoostage")
    private cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_stage odooStage;

    /**
     * 
     */
    @JSONField(name = "odoomaintenanceteam")
    @JsonProperty("odoomaintenanceteam")
    private cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_team odooMaintenanceTeam;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odooowneruser")
    @JsonProperty("odooowneruser")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooOwnerUser;

    /**
     * 
     */
    @JSONField(name = "odoouser")
    @JsonProperty("odoouser")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [主题]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [持续时间]
     */
    public void setDuration(Double duration){
        this.duration = duration ;
        this.modify("duration",duration);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }
    /**
     * 设置 [计划日期]
     */
    public void setScheduleDate(Timestamp scheduleDate){
        this.scheduleDate = scheduleDate ;
        this.modify("schedule_date",scheduleDate);
    }
    /**
     * 设置 [看板状态]
     */
    public void setKanbanState(String kanbanState){
        this.kanbanState = kanbanState ;
        this.modify("kanban_state",kanbanState);
    }
    /**
     * 设置 [保养类型]
     */
    public void setMaintenanceType(String maintenanceType){
        this.maintenanceType = maintenanceType ;
        this.modify("maintenance_type",maintenanceType);
    }
    /**
     * 设置 [关闭日期]
     */
    public void setCloseDate(Timestamp closeDate){
        this.closeDate = closeDate ;
        this.modify("close_date",closeDate);
    }
    /**
     * 设置 [请求日期]
     */
    public void setRequestDate(Timestamp requestDate){
        this.requestDate = requestDate ;
        this.modify("request_date",requestDate);
    }
    /**
     * 设置 [归档]
     */
    public void setArchive(String archive){
        this.archive = archive ;
        this.modify("archive",archive);
    }
    /**
     * 设置 [颜色索引]
     */
    public void setColor(Integer color){
        this.color = color ;
        this.modify("color",color);
    }
    /**
     * 设置 [优先级]
     */
    public void setPriority(String priority){
        this.priority = priority ;
        this.modify("priority",priority);
    }
    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }
    /**
     * 设置 [员工]
     */
    public void setEmployeeId(Integer employeeId){
        this.employeeId = employeeId ;
        this.modify("employee_id",employeeId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [设备]
     */
    public void setEquipmentId(Integer equipmentId){
        this.equipmentId = equipmentId ;
        this.modify("equipment_id",equipmentId);
    }
    /**
     * 设置 [阶段]
     */
    public void setStageId(Integer stageId){
        this.stageId = stageId ;
        this.modify("stage_id",stageId);
    }
    /**
     * 设置 [创建人]
     */
    public void setOwnerUserId(Integer ownerUserId){
        this.ownerUserId = ownerUserId ;
        this.modify("owner_user_id",ownerUserId);
    }
    /**
     * 设置 [类别]
     */
    public void setCategoryId(Integer categoryId){
        this.categoryId = categoryId ;
        this.modify("category_id",categoryId);
    }
    /**
     * 设置 [部门]
     */
    public void setDepartmentId(Integer departmentId){
        this.departmentId = departmentId ;
        this.modify("department_id",departmentId);
    }
    /**
     * 设置 [技术员]
     */
    public void setUserId(Integer userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }
    /**
     * 设置 [团队]
     */
    public void setMaintenanceTeamId(Integer maintenanceTeamId){
        this.maintenanceTeamId = maintenanceTeamId ;
        this.modify("maintenance_team_id",maintenanceTeamId);
    }

}


