package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_partner_merge_line;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_partner_merge_lineSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_partner_merge_line] 服务对象接口
 */
@FeignClient(value = "odoo-base", contextId = "base-partner-merge-line", fallback = base_partner_merge_lineFallback.class)
public interface base_partner_merge_lineFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/base_partner_merge_lines")
    Base_partner_merge_line create(@RequestBody Base_partner_merge_line base_partner_merge_line);

    @RequestMapping(method = RequestMethod.POST, value = "/base_partner_merge_lines/batch")
    Boolean createBatch(@RequestBody List<Base_partner_merge_line> base_partner_merge_lines);


    @RequestMapping(method = RequestMethod.PUT, value = "/base_partner_merge_lines/{id}")
    Base_partner_merge_line update(@PathVariable("id") Integer id,@RequestBody Base_partner_merge_line base_partner_merge_line);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_partner_merge_lines/batch")
    Boolean updateBatch(@RequestBody List<Base_partner_merge_line> base_partner_merge_lines);



    @RequestMapping(method = RequestMethod.DELETE, value = "/base_partner_merge_lines/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_partner_merge_lines/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);





    @RequestMapping(method = RequestMethod.POST, value = "/base_partner_merge_lines/searchdefault")
    Page<Base_partner_merge_line> searchDefault(@RequestBody Base_partner_merge_lineSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/base_partner_merge_lines/{id}")
    Base_partner_merge_line get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/base_partner_merge_lines/select")
    Page<Base_partner_merge_line> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_partner_merge_lines/getdraft")
    Base_partner_merge_line getDraft();


}
