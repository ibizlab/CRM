package cn.ibizlab.odoo.core.odoo_payment.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_token;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_tokenSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[payment_token] 服务对象接口
 */
@Component
public class payment_tokenFallback implements payment_tokenFeignClient{

    public Payment_token get(Integer id){
            return null;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Payment_token create(Payment_token payment_token){
            return null;
     }
    public Boolean createBatch(List<Payment_token> payment_tokens){
            return false;
     }


    public Page<Payment_token> searchDefault(Payment_tokenSearchContext context){
            return null;
     }


    public Payment_token update(Integer id, Payment_token payment_token){
            return null;
     }
    public Boolean updateBatch(List<Payment_token> payment_tokens){
            return false;
     }


    public Page<Payment_token> select(){
            return null;
     }

    public Payment_token getDraft(){
            return null;
    }



}
