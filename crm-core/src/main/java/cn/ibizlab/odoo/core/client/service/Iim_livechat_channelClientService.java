package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iim_livechat_channel;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[im_livechat_channel] 服务对象接口
 */
public interface Iim_livechat_channelClientService{

    public Iim_livechat_channel createModel() ;

    public void createBatch(List<Iim_livechat_channel> im_livechat_channels);

    public void remove(Iim_livechat_channel im_livechat_channel);

    public Page<Iim_livechat_channel> fetchDefault(SearchContext context);

    public void create(Iim_livechat_channel im_livechat_channel);

    public void removeBatch(List<Iim_livechat_channel> im_livechat_channels);

    public void update(Iim_livechat_channel im_livechat_channel);

    public void updateBatch(List<Iim_livechat_channel> im_livechat_channels);

    public void get(Iim_livechat_channel im_livechat_channel);

    public Page<Iim_livechat_channel> select(SearchContext context);

    public void getDraft(Iim_livechat_channel im_livechat_channel);

}
