package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iwebsite_menu;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[website_menu] 服务对象接口
 */
public interface Iwebsite_menuClientService{

    public Iwebsite_menu createModel() ;

    public void get(Iwebsite_menu website_menu);

    public void removeBatch(List<Iwebsite_menu> website_menus);

    public void updateBatch(List<Iwebsite_menu> website_menus);

    public void create(Iwebsite_menu website_menu);

    public void remove(Iwebsite_menu website_menu);

    public void update(Iwebsite_menu website_menu);

    public Page<Iwebsite_menu> fetchDefault(SearchContext context);

    public void createBatch(List<Iwebsite_menu> website_menus);

    public Page<Iwebsite_menu> select(SearchContext context);

    public void getDraft(Iwebsite_menu website_menu);

}
