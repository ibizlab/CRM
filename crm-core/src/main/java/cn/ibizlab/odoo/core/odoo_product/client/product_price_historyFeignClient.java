package cn.ibizlab.odoo.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_price_history;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_price_historySearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[product_price_history] 服务对象接口
 */
@FeignClient(value = "odoo-product", contextId = "product-price-history", fallback = product_price_historyFallback.class)
public interface product_price_historyFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/product_price_histories/searchdefault")
    Page<Product_price_history> searchDefault(@RequestBody Product_price_historySearchContext context);




    @RequestMapping(method = RequestMethod.DELETE, value = "/product_price_histories/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/product_price_histories/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/product_price_histories")
    Product_price_history create(@RequestBody Product_price_history product_price_history);

    @RequestMapping(method = RequestMethod.POST, value = "/product_price_histories/batch")
    Boolean createBatch(@RequestBody List<Product_price_history> product_price_histories);


    @RequestMapping(method = RequestMethod.GET, value = "/product_price_histories/{id}")
    Product_price_history get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/product_price_histories/{id}")
    Product_price_history update(@PathVariable("id") Integer id,@RequestBody Product_price_history product_price_history);

    @RequestMapping(method = RequestMethod.PUT, value = "/product_price_histories/batch")
    Boolean updateBatch(@RequestBody List<Product_price_history> product_price_histories);



    @RequestMapping(method = RequestMethod.GET, value = "/product_price_histories/select")
    Page<Product_price_history> select();


    @RequestMapping(method = RequestMethod.GET, value = "/product_price_histories/getdraft")
    Product_price_history getDraft();


}
