package cn.ibizlab.odoo.core.odoo_lunch.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_order;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_orderSearchContext;
import cn.ibizlab.odoo.core.odoo_lunch.service.ILunch_orderService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_lunch.client.lunch_orderFeignClient;

/**
 * 实体[午餐订单] 服务对象接口实现
 */
@Slf4j
@Service
public class Lunch_orderServiceImpl implements ILunch_orderService {

    @Autowired
    lunch_orderFeignClient lunch_orderFeignClient;


    @Override
    public Lunch_order getDraft(Lunch_order et) {
        et=lunch_orderFeignClient.getDraft();
        return et;
    }

    @Override
    public Lunch_order get(Integer id) {
		Lunch_order et=lunch_orderFeignClient.get(id);
        if(et==null){
            et=new Lunch_order();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Lunch_order et) {
        Lunch_order rt = lunch_orderFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Lunch_order> list){
        lunch_orderFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=lunch_orderFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        lunch_orderFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Lunch_order et) {
        Lunch_order rt = lunch_orderFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Lunch_order> list){
        lunch_orderFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Lunch_order> searchDefault(Lunch_orderSearchContext context) {
        Page<Lunch_order> lunch_orders=lunch_orderFeignClient.searchDefault(context);
        return lunch_orders;
    }


}


