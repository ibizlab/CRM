package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_lineSearchContext;


/**
 * 实体[Account_bank_statement_line] 服务对象接口
 */
public interface IAccount_bank_statement_lineService{

    boolean update(Account_bank_statement_line et) ;
    void updateBatch(List<Account_bank_statement_line> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Account_bank_statement_line et) ;
    void createBatch(List<Account_bank_statement_line> list) ;
    Account_bank_statement_line getDraft(Account_bank_statement_line et) ;
    Account_bank_statement_line get(Integer key) ;
    Page<Account_bank_statement_line> searchDefault(Account_bank_statement_lineSearchContext context) ;

}



