package cn.ibizlab.odoo.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [发票税率] 对象
 */
@Data
public class Account_invoice_tax extends EntityClient implements Serializable {

    /**
     * 手动
     */
    @JSONField(name = "manual")
    @JsonProperty("manual")
    private String manual;

    /**
     * 总金额
     */
    @JSONField(name = "amount_total")
    @JsonProperty("amount_total")
    private Double amountTotal;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 基础
     */
    @JSONField(name = "base")
    @JsonProperty("base")
    private Double base;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 税率金额
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;

    /**
     * 税说明
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 序号
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 分析标签
     */
    @JSONField(name = "analytic_tag_ids")
    @JsonProperty("analytic_tag_ids")
    private String analyticTagIds;

    /**
     * 金额差异
     */
    @DEField(name = "amount_rounding")
    @JSONField(name = "amount_rounding")
    @JsonProperty("amount_rounding")
    private Double amountRounding;

    /**
     * 税率
     */
    @JSONField(name = "tax_id_text")
    @JsonProperty("tax_id_text")
    private String taxIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 发票
     */
    @JSONField(name = "invoice_id_text")
    @JsonProperty("invoice_id_text")
    private String invoiceIdText;

    /**
     * 币种
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 分析账户
     */
    @JSONField(name = "account_analytic_id_text")
    @JsonProperty("account_analytic_id_text")
    private String accountAnalyticIdText;

    /**
     * 税率科目
     */
    @JSONField(name = "account_id_text")
    @JsonProperty("account_id_text")
    private String accountIdText;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 税率科目
     */
    @DEField(name = "account_id")
    @JSONField(name = "account_id")
    @JsonProperty("account_id")
    private Integer accountId;

    /**
     * 发票
     */
    @DEField(name = "invoice_id")
    @JSONField(name = "invoice_id")
    @JsonProperty("invoice_id")
    private Integer invoiceId;

    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 分析账户
     */
    @DEField(name = "account_analytic_id")
    @JSONField(name = "account_analytic_id")
    @JsonProperty("account_analytic_id")
    private Integer accountAnalyticId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 税率
     */
    @DEField(name = "tax_id")
    @JSONField(name = "tax_id")
    @JsonProperty("tax_id")
    private Integer taxId;


    /**
     * 
     */
    @JSONField(name = "odooaccount")
    @JsonProperty("odooaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account odooAccount;

    /**
     * 
     */
    @JSONField(name = "odooaccountanalytic")
    @JsonProperty("odooaccountanalytic")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_account odooAccountAnalytic;

    /**
     * 
     */
    @JSONField(name = "odooinvoice")
    @JsonProperty("odooinvoice")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice odooInvoice;

    /**
     * 
     */
    @JSONField(name = "odootax")
    @JsonProperty("odootax")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_tax odooTax;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoocurrency")
    @JsonProperty("odoocurrency")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [手动]
     */
    public void setManual(String manual){
        this.manual = manual ;
        this.modify("manual",manual);
    }
    /**
     * 设置 [基础]
     */
    public void setBase(Double base){
        this.base = base ;
        this.modify("base",base);
    }
    /**
     * 设置 [税率金额]
     */
    public void setAmount(Double amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }
    /**
     * 设置 [税说明]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }
    /**
     * 设置 [金额差异]
     */
    public void setAmountRounding(Double amountRounding){
        this.amountRounding = amountRounding ;
        this.modify("amount_rounding",amountRounding);
    }
    /**
     * 设置 [税率科目]
     */
    public void setAccountId(Integer accountId){
        this.accountId = accountId ;
        this.modify("account_id",accountId);
    }
    /**
     * 设置 [发票]
     */
    public void setInvoiceId(Integer invoiceId){
        this.invoiceId = invoiceId ;
        this.modify("invoice_id",invoiceId);
    }
    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Integer currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }
    /**
     * 设置 [分析账户]
     */
    public void setAccountAnalyticId(Integer accountAnalyticId){
        this.accountAnalyticId = accountAnalyticId ;
        this.modify("account_analytic_id",accountAnalyticId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [税率]
     */
    public void setTaxId(Integer taxId){
        this.taxId = taxId ;
        this.modify("tax_id",taxId);
    }

}


