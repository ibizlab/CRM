package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [mail_resend_message] 对象
 */
public interface Imail_resend_message {

    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [已取消]
     */
    public void setHas_cancel(String has_cancel);
    
    /**
     * 设置 [已取消]
     */
    public String getHas_cancel();

    /**
     * 获取 [已取消]脏标记
     */
    public boolean getHas_cancelDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMail_message_id(Integer mail_message_id);
    
    /**
     * 设置 [消息]
     */
    public Integer getMail_message_id();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMail_message_idDirtyFlag();
    /**
     * 获取 [通知]
     */
    public void setNotification_ids(String notification_ids);
    
    /**
     * 设置 [通知]
     */
    public String getNotification_ids();

    /**
     * 获取 [通知]脏标记
     */
    public boolean getNotification_idsDirtyFlag();
    /**
     * 获取 [收件人]
     */
    public void setPartner_ids(String partner_ids);
    
    /**
     * 设置 [收件人]
     */
    public String getPartner_ids();

    /**
     * 获取 [收件人]脏标记
     */
    public boolean getPartner_idsDirtyFlag();
    /**
     * 获取 [业务合作伙伴只读]
     */
    public void setPartner_readonly(String partner_readonly);
    
    /**
     * 设置 [业务合作伙伴只读]
     */
    public String getPartner_readonly();

    /**
     * 获取 [业务合作伙伴只读]脏标记
     */
    public boolean getPartner_readonlyDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
