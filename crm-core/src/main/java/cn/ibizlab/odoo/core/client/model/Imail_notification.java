package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [mail_notification] 对象
 */
public interface Imail_notification {

    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [EMail状态]
     */
    public void setEmail_status(String email_status);
    
    /**
     * 设置 [EMail状态]
     */
    public String getEmail_status();

    /**
     * 获取 [EMail状态]脏标记
     */
    public boolean getEmail_statusDirtyFlag();
    /**
     * 获取 [失败原因]
     */
    public void setFailure_reason(String failure_reason);
    
    /**
     * 设置 [失败原因]
     */
    public String getFailure_reason();

    /**
     * 获取 [失败原因]脏标记
     */
    public boolean getFailure_reasonDirtyFlag();
    /**
     * 获取 [失败类型]
     */
    public void setFailure_type(String failure_type);
    
    /**
     * 设置 [失败类型]
     */
    public String getFailure_type();

    /**
     * 获取 [失败类型]脏标记
     */
    public boolean getFailure_typeDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [以邮件发送]
     */
    public void setIs_email(String is_email);
    
    /**
     * 设置 [以邮件发送]
     */
    public String getIs_email();

    /**
     * 获取 [以邮件发送]脏标记
     */
    public boolean getIs_emailDirtyFlag();
    /**
     * 获取 [已读]
     */
    public void setIs_read(String is_read);
    
    /**
     * 设置 [已读]
     */
    public String getIs_read();

    /**
     * 获取 [已读]脏标记
     */
    public boolean getIs_readDirtyFlag();
    /**
     * 获取 [邮件]
     */
    public void setMail_id(Integer mail_id);
    
    /**
     * 设置 [邮件]
     */
    public Integer getMail_id();

    /**
     * 获取 [邮件]脏标记
     */
    public boolean getMail_idDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMail_message_id(Integer mail_message_id);
    
    /**
     * 设置 [消息]
     */
    public Integer getMail_message_id();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMail_message_idDirtyFlag();
    /**
     * 获取 [需收件人]
     */
    public void setRes_partner_id(Integer res_partner_id);
    
    /**
     * 设置 [需收件人]
     */
    public Integer getRes_partner_id();

    /**
     * 获取 [需收件人]脏标记
     */
    public boolean getRes_partner_idDirtyFlag();
    /**
     * 获取 [需收件人]
     */
    public void setRes_partner_id_text(String res_partner_id_text);
    
    /**
     * 设置 [需收件人]
     */
    public String getRes_partner_id_text();

    /**
     * 获取 [需收件人]脏标记
     */
    public boolean getRes_partner_id_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
