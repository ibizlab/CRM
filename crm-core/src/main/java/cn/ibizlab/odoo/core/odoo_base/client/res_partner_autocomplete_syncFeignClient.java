package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_autocomplete_sync;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_autocomplete_syncSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[res_partner_autocomplete_sync] 服务对象接口
 */
@FeignClient(value = "odoo-base", contextId = "res-partner-autocomplete-sync", fallback = res_partner_autocomplete_syncFallback.class)
public interface res_partner_autocomplete_syncFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/res_partner_autocomplete_syncs/{id}")
    Res_partner_autocomplete_sync update(@PathVariable("id") Integer id,@RequestBody Res_partner_autocomplete_sync res_partner_autocomplete_sync);

    @RequestMapping(method = RequestMethod.PUT, value = "/res_partner_autocomplete_syncs/batch")
    Boolean updateBatch(@RequestBody List<Res_partner_autocomplete_sync> res_partner_autocomplete_syncs);



    @RequestMapping(method = RequestMethod.GET, value = "/res_partner_autocomplete_syncs/{id}")
    Res_partner_autocomplete_sync get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/res_partner_autocomplete_syncs")
    Res_partner_autocomplete_sync create(@RequestBody Res_partner_autocomplete_sync res_partner_autocomplete_sync);

    @RequestMapping(method = RequestMethod.POST, value = "/res_partner_autocomplete_syncs/batch")
    Boolean createBatch(@RequestBody List<Res_partner_autocomplete_sync> res_partner_autocomplete_syncs);




    @RequestMapping(method = RequestMethod.POST, value = "/res_partner_autocomplete_syncs/searchdefault")
    Page<Res_partner_autocomplete_sync> searchDefault(@RequestBody Res_partner_autocomplete_syncSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_autocomplete_syncs/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_autocomplete_syncs/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/res_partner_autocomplete_syncs/select")
    Page<Res_partner_autocomplete_sync> select();


    @RequestMapping(method = RequestMethod.GET, value = "/res_partner_autocomplete_syncs/getdraft")
    Res_partner_autocomplete_sync getDraft();


}
