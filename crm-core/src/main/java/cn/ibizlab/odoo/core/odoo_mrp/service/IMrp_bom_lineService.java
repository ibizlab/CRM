package cn.ibizlab.odoo.core.odoo_mrp.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_bom_line;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_bom_lineSearchContext;


/**
 * 实体[Mrp_bom_line] 服务对象接口
 */
public interface IMrp_bom_lineService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Mrp_bom_line et) ;
    void updateBatch(List<Mrp_bom_line> list) ;
    boolean create(Mrp_bom_line et) ;
    void createBatch(List<Mrp_bom_line> list) ;
    Mrp_bom_line getDraft(Mrp_bom_line et) ;
    Mrp_bom_line get(Integer key) ;
    Page<Mrp_bom_line> searchDefault(Mrp_bom_lineSearchContext context) ;

}



