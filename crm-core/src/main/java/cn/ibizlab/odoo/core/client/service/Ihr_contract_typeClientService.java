package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ihr_contract_type;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_contract_type] 服务对象接口
 */
public interface Ihr_contract_typeClientService{

    public Ihr_contract_type createModel() ;

    public void removeBatch(List<Ihr_contract_type> hr_contract_types);

    public void remove(Ihr_contract_type hr_contract_type);

    public void createBatch(List<Ihr_contract_type> hr_contract_types);

    public void updateBatch(List<Ihr_contract_type> hr_contract_types);

    public void update(Ihr_contract_type hr_contract_type);

    public void create(Ihr_contract_type hr_contract_type);

    public Page<Ihr_contract_type> fetchDefault(SearchContext context);

    public void get(Ihr_contract_type hr_contract_type);

    public Page<Ihr_contract_type> select(SearchContext context);

    public void getDraft(Ihr_contract_type hr_contract_type);

}
