package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [event_mail_registration] 对象
 */
public interface Ievent_mail_registration {

    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [EMail发送]
     */
    public void setMail_sent(String mail_sent);
    
    /**
     * 设置 [EMail发送]
     */
    public String getMail_sent();

    /**
     * 获取 [EMail发送]脏标记
     */
    public boolean getMail_sentDirtyFlag();
    /**
     * 获取 [出席者]
     */
    public void setRegistration_id(Integer registration_id);
    
    /**
     * 设置 [出席者]
     */
    public Integer getRegistration_id();

    /**
     * 获取 [出席者]脏标记
     */
    public boolean getRegistration_idDirtyFlag();
    /**
     * 获取 [出席者]
     */
    public void setRegistration_id_text(String registration_id_text);
    
    /**
     * 设置 [出席者]
     */
    public String getRegistration_id_text();

    /**
     * 获取 [出席者]脏标记
     */
    public boolean getRegistration_id_textDirtyFlag();
    /**
     * 获取 [定期时间]
     */
    public void setScheduled_date(Timestamp scheduled_date);
    
    /**
     * 设置 [定期时间]
     */
    public Timestamp getScheduled_date();

    /**
     * 获取 [定期时间]脏标记
     */
    public boolean getScheduled_dateDirtyFlag();
    /**
     * 获取 [邮件调度]
     */
    public void setScheduler_id(Integer scheduler_id);
    
    /**
     * 设置 [邮件调度]
     */
    public Integer getScheduler_id();

    /**
     * 获取 [邮件调度]脏标记
     */
    public boolean getScheduler_idDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
