package cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_invoice_send;

import cn.ibizlab.odoo.core.odoo_account.valuerule.validator.account_invoice_send.Account_invoice_sendSnailmail_costDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Account_invoice_send
 * 属性：Snailmail_cost
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Account_invoice_sendSnailmail_costDefaultValidator.class})
public @interface Account_invoice_sendSnailmail_costDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
