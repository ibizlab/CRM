package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_payment_term_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_payment_term_line] 服务对象接口
 */
public interface Iaccount_payment_term_lineClientService{

    public Iaccount_payment_term_line createModel() ;

    public void createBatch(List<Iaccount_payment_term_line> account_payment_term_lines);

    public void removeBatch(List<Iaccount_payment_term_line> account_payment_term_lines);

    public void updateBatch(List<Iaccount_payment_term_line> account_payment_term_lines);

    public void get(Iaccount_payment_term_line account_payment_term_line);

    public Page<Iaccount_payment_term_line> fetchDefault(SearchContext context);

    public void create(Iaccount_payment_term_line account_payment_term_line);

    public void update(Iaccount_payment_term_line account_payment_term_line);

    public void remove(Iaccount_payment_term_line account_payment_term_line);

    public Page<Iaccount_payment_term_line> select(SearchContext context);

    public void getDraft(Iaccount_payment_term_line account_payment_term_line);

}
