package cn.ibizlab.odoo.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_o2m_child;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_o2m_childSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[base_import_tests_models_o2m_child] 服务对象接口
 */
@Component
public class base_import_tests_models_o2m_childFallback implements base_import_tests_models_o2m_childFeignClient{

    public Base_import_tests_models_o2m_child create(Base_import_tests_models_o2m_child base_import_tests_models_o2m_child){
            return null;
     }
    public Boolean createBatch(List<Base_import_tests_models_o2m_child> base_import_tests_models_o2m_children){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }




    public Page<Base_import_tests_models_o2m_child> searchDefault(Base_import_tests_models_o2m_childSearchContext context){
            return null;
     }


    public Base_import_tests_models_o2m_child get(Integer id){
            return null;
     }


    public Base_import_tests_models_o2m_child update(Integer id, Base_import_tests_models_o2m_child base_import_tests_models_o2m_child){
            return null;
     }
    public Boolean updateBatch(List<Base_import_tests_models_o2m_child> base_import_tests_models_o2m_children){
            return false;
     }


    public Page<Base_import_tests_models_o2m_child> select(){
            return null;
     }

    public Base_import_tests_models_o2m_child getDraft(){
            return null;
    }



}
