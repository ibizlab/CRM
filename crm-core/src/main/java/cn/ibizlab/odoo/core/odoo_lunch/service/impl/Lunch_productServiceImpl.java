package cn.ibizlab.odoo.core.odoo_lunch.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_product;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_productSearchContext;
import cn.ibizlab.odoo.core.odoo_lunch.service.ILunch_productService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_lunch.client.lunch_productFeignClient;

/**
 * 实体[工作餐产品] 服务对象接口实现
 */
@Slf4j
@Service
public class Lunch_productServiceImpl implements ILunch_productService {

    @Autowired
    lunch_productFeignClient lunch_productFeignClient;


    @Override
    public Lunch_product get(Integer id) {
		Lunch_product et=lunch_productFeignClient.get(id);
        if(et==null){
            et=new Lunch_product();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Lunch_product et) {
        Lunch_product rt = lunch_productFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Lunch_product> list){
        lunch_productFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Lunch_product et) {
        Lunch_product rt = lunch_productFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Lunch_product> list){
        lunch_productFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=lunch_productFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        lunch_productFeignClient.removeBatch(idList);
    }

    @Override
    public Lunch_product getDraft(Lunch_product et) {
        et=lunch_productFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Lunch_product> searchDefault(Lunch_productSearchContext context) {
        Page<Lunch_product> lunch_products=lunch_productFeignClient.searchDefault(context);
        return lunch_products;
    }


}


