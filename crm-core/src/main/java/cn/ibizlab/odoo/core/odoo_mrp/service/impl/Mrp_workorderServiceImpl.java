package cn.ibizlab.odoo.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workorder;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workorderSearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_workorderService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mrp.client.mrp_workorderFeignClient;

/**
 * 实体[工单] 服务对象接口实现
 */
@Slf4j
@Service
public class Mrp_workorderServiceImpl implements IMrp_workorderService {

    @Autowired
    mrp_workorderFeignClient mrp_workorderFeignClient;


    @Override
    public boolean update(Mrp_workorder et) {
        Mrp_workorder rt = mrp_workorderFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mrp_workorder> list){
        mrp_workorderFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mrp_workorderFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mrp_workorderFeignClient.removeBatch(idList);
    }

    @Override
    public Mrp_workorder getDraft(Mrp_workorder et) {
        et=mrp_workorderFeignClient.getDraft();
        return et;
    }

    @Override
    public Mrp_workorder get(Integer id) {
		Mrp_workorder et=mrp_workorderFeignClient.get(id);
        if(et==null){
            et=new Mrp_workorder();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Mrp_workorder et) {
        Mrp_workorder rt = mrp_workorderFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mrp_workorder> list){
        mrp_workorderFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mrp_workorder> searchDefault(Mrp_workorderSearchContext context) {
        Page<Mrp_workorder> mrp_workorders=mrp_workorderFeignClient.searchDefault(context);
        return mrp_workorders;
    }


}


