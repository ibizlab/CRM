package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imaintenance_stage;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[maintenance_stage] 服务对象接口
 */
public interface Imaintenance_stageClientService{

    public Imaintenance_stage createModel() ;

    public Page<Imaintenance_stage> fetchDefault(SearchContext context);

    public void get(Imaintenance_stage maintenance_stage);

    public void create(Imaintenance_stage maintenance_stage);

    public void remove(Imaintenance_stage maintenance_stage);

    public void update(Imaintenance_stage maintenance_stage);

    public void removeBatch(List<Imaintenance_stage> maintenance_stages);

    public void updateBatch(List<Imaintenance_stage> maintenance_stages);

    public void createBatch(List<Imaintenance_stage> maintenance_stages);

    public Page<Imaintenance_stage> select(SearchContext context);

    public void getDraft(Imaintenance_stage maintenance_stage);

}
