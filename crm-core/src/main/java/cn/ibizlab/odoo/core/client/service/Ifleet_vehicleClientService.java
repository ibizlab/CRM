package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[fleet_vehicle] 服务对象接口
 */
public interface Ifleet_vehicleClientService{

    public Ifleet_vehicle createModel() ;

    public void removeBatch(List<Ifleet_vehicle> fleet_vehicles);

    public void remove(Ifleet_vehicle fleet_vehicle);

    public void createBatch(List<Ifleet_vehicle> fleet_vehicles);

    public void updateBatch(List<Ifleet_vehicle> fleet_vehicles);

    public void update(Ifleet_vehicle fleet_vehicle);

    public void get(Ifleet_vehicle fleet_vehicle);

    public Page<Ifleet_vehicle> fetchDefault(SearchContext context);

    public void create(Ifleet_vehicle fleet_vehicle);

    public Page<Ifleet_vehicle> select(SearchContext context);

    public void getDraft(Ifleet_vehicle fleet_vehicle);

}
