package cn.ibizlab.odoo.core.odoo_survey.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_survey;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_surveySearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[survey_survey] 服务对象接口
 */
@FeignClient(value = "odoo-survey", contextId = "survey-survey", fallback = survey_surveyFallback.class)
public interface survey_surveyFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/survey_surveys")
    Survey_survey create(@RequestBody Survey_survey survey_survey);

    @RequestMapping(method = RequestMethod.POST, value = "/survey_surveys/batch")
    Boolean createBatch(@RequestBody List<Survey_survey> survey_surveys);


    @RequestMapping(method = RequestMethod.DELETE, value = "/survey_surveys/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/survey_surveys/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.PUT, value = "/survey_surveys/{id}")
    Survey_survey update(@PathVariable("id") Integer id,@RequestBody Survey_survey survey_survey);

    @RequestMapping(method = RequestMethod.PUT, value = "/survey_surveys/batch")
    Boolean updateBatch(@RequestBody List<Survey_survey> survey_surveys);




    @RequestMapping(method = RequestMethod.GET, value = "/survey_surveys/{id}")
    Survey_survey get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/survey_surveys/searchdefault")
    Page<Survey_survey> searchDefault(@RequestBody Survey_surveySearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/survey_surveys/select")
    Page<Survey_survey> select();


    @RequestMapping(method = RequestMethod.GET, value = "/survey_surveys/getdraft")
    Survey_survey getDraft();


}
