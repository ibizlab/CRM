package cn.ibizlab.odoo.core.odoo_fleet.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_assignation_log;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_assignation_logSearchContext;


/**
 * 实体[Fleet_vehicle_assignation_log] 服务对象接口
 */
public interface IFleet_vehicle_assignation_logService{

    Fleet_vehicle_assignation_log get(Integer key) ;
    boolean create(Fleet_vehicle_assignation_log et) ;
    void createBatch(List<Fleet_vehicle_assignation_log> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Fleet_vehicle_assignation_log getDraft(Fleet_vehicle_assignation_log et) ;
    boolean update(Fleet_vehicle_assignation_log et) ;
    void updateBatch(List<Fleet_vehicle_assignation_log> list) ;
    Page<Fleet_vehicle_assignation_log> searchDefault(Fleet_vehicle_assignation_logSearchContext context) ;

}



