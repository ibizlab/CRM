package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [crm_activity_report] 对象
 */
public interface Icrm_activity_report {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setAuthor_id(Integer author_id);
    
    /**
     * 设置 [创建人]
     */
    public Integer getAuthor_id();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getAuthor_idDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setAuthor_id_text(String author_id_text);
    
    /**
     * 设置 [创建人]
     */
    public String getAuthor_id_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getAuthor_id_textDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [国家]
     */
    public void setCountry_id(Integer country_id);
    
    /**
     * 设置 [国家]
     */
    public Integer getCountry_id();

    /**
     * 获取 [国家]脏标记
     */
    public boolean getCountry_idDirtyFlag();
    /**
     * 获取 [国家]
     */
    public void setCountry_id_text(String country_id_text);
    
    /**
     * 设置 [国家]
     */
    public String getCountry_id_text();

    /**
     * 获取 [国家]脏标记
     */
    public boolean getCountry_id_textDirtyFlag();
    /**
     * 获取 [日期]
     */
    public void setDate(Timestamp date);
    
    /**
     * 设置 [日期]
     */
    public Timestamp getDate();

    /**
     * 获取 [日期]脏标记
     */
    public boolean getDateDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [线索]
     */
    public void setLead_id(Integer lead_id);
    
    /**
     * 设置 [线索]
     */
    public Integer getLead_id();

    /**
     * 获取 [线索]脏标记
     */
    public boolean getLead_idDirtyFlag();
    /**
     * 获取 [线索]
     */
    public void setLead_id_text(String lead_id_text);
    
    /**
     * 设置 [线索]
     */
    public String getLead_id_text();

    /**
     * 获取 [线索]脏标记
     */
    public boolean getLead_id_textDirtyFlag();
    /**
     * 获取 [类型]
     */
    public void setLead_type(String lead_type);
    
    /**
     * 设置 [类型]
     */
    public String getLead_type();

    /**
     * 获取 [类型]脏标记
     */
    public boolean getLead_typeDirtyFlag();
    /**
     * 获取 [活动类型]
     */
    public void setMail_activity_type_id(Integer mail_activity_type_id);
    
    /**
     * 设置 [活动类型]
     */
    public Integer getMail_activity_type_id();

    /**
     * 获取 [活动类型]脏标记
     */
    public boolean getMail_activity_type_idDirtyFlag();
    /**
     * 获取 [活动类型]
     */
    public void setMail_activity_type_id_text(String mail_activity_type_id_text);
    
    /**
     * 设置 [活动类型]
     */
    public String getMail_activity_type_id_text();

    /**
     * 获取 [活动类型]脏标记
     */
    public boolean getMail_activity_type_id_textDirtyFlag();
    /**
     * 获取 [业务合作伙伴/客户]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [业务合作伙伴/客户]
     */
    public Integer getPartner_id();

    /**
     * 获取 [业务合作伙伴/客户]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [业务合作伙伴/客户]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [业务合作伙伴/客户]
     */
    public String getPartner_id_text();

    /**
     * 获取 [业务合作伙伴/客户]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [概率]
     */
    public void setProbability(Double probability);
    
    /**
     * 设置 [概率]
     */
    public Double getProbability();

    /**
     * 获取 [概率]脏标记
     */
    public boolean getProbabilityDirtyFlag();
    /**
     * 获取 [阶段]
     */
    public void setStage_id(Integer stage_id);
    
    /**
     * 设置 [阶段]
     */
    public Integer getStage_id();

    /**
     * 获取 [阶段]脏标记
     */
    public boolean getStage_idDirtyFlag();
    /**
     * 获取 [阶段]
     */
    public void setStage_id_text(String stage_id_text);
    
    /**
     * 设置 [阶段]
     */
    public String getStage_id_text();

    /**
     * 获取 [阶段]脏标记
     */
    public boolean getStage_id_textDirtyFlag();
    /**
     * 获取 [摘要]
     */
    public void setSubject(String subject);
    
    /**
     * 设置 [摘要]
     */
    public String getSubject();

    /**
     * 获取 [摘要]脏标记
     */
    public boolean getSubjectDirtyFlag();
    /**
     * 获取 [子类型]
     */
    public void setSubtype_id(Integer subtype_id);
    
    /**
     * 设置 [子类型]
     */
    public Integer getSubtype_id();

    /**
     * 获取 [子类型]脏标记
     */
    public boolean getSubtype_idDirtyFlag();
    /**
     * 获取 [子类型]
     */
    public void setSubtype_id_text(String subtype_id_text);
    
    /**
     * 设置 [子类型]
     */
    public String getSubtype_id_text();

    /**
     * 获取 [子类型]脏标记
     */
    public boolean getSubtype_id_textDirtyFlag();
    /**
     * 获取 [销售团队]
     */
    public void setTeam_id(Integer team_id);
    
    /**
     * 设置 [销售团队]
     */
    public Integer getTeam_id();

    /**
     * 获取 [销售团队]脏标记
     */
    public boolean getTeam_idDirtyFlag();
    /**
     * 获取 [销售团队]
     */
    public void setTeam_id_text(String team_id_text);
    
    /**
     * 设置 [销售团队]
     */
    public String getTeam_id_text();

    /**
     * 获取 [销售团队]脏标记
     */
    public boolean getTeam_id_textDirtyFlag();
    /**
     * 获取 [销售员]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [销售员]
     */
    public Integer getUser_id();

    /**
     * 获取 [销售员]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [销售员]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [销售员]
     */
    public String getUser_id_text();

    /**
     * 获取 [销售员]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
