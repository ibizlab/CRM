package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ibase_automation_lead_test;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_automation_lead_test] 服务对象接口
 */
public interface Ibase_automation_lead_testClientService{

    public Ibase_automation_lead_test createModel() ;

    public void createBatch(List<Ibase_automation_lead_test> base_automation_lead_tests);

    public Page<Ibase_automation_lead_test> fetchDefault(SearchContext context);

    public void updateBatch(List<Ibase_automation_lead_test> base_automation_lead_tests);

    public void update(Ibase_automation_lead_test base_automation_lead_test);

    public void create(Ibase_automation_lead_test base_automation_lead_test);

    public void removeBatch(List<Ibase_automation_lead_test> base_automation_lead_tests);

    public void remove(Ibase_automation_lead_test base_automation_lead_test);

    public void get(Ibase_automation_lead_test base_automation_lead_test);

    public Page<Ibase_automation_lead_test> select(SearchContext context);

    public void getDraft(Ibase_automation_lead_test base_automation_lead_test);

}
