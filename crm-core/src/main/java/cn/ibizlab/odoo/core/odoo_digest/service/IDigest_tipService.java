package cn.ibizlab.odoo.core.odoo_digest.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_digest.domain.Digest_tip;
import cn.ibizlab.odoo.core.odoo_digest.filter.Digest_tipSearchContext;


/**
 * 实体[Digest_tip] 服务对象接口
 */
public interface IDigest_tipService{

    boolean create(Digest_tip et) ;
    void createBatch(List<Digest_tip> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Digest_tip et) ;
    void updateBatch(List<Digest_tip> list) ;
    Digest_tip getDraft(Digest_tip et) ;
    Digest_tip get(Integer key) ;
    Page<Digest_tip> searchDefault(Digest_tipSearchContext context) ;

}



