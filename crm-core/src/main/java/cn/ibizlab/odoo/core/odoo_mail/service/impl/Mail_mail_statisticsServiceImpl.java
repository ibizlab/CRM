package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mail_statistics;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mail_statisticsSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mail_statisticsService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_mail_statisticsFeignClient;

/**
 * 实体[邮件统计] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_mail_statisticsServiceImpl implements IMail_mail_statisticsService {

    @Autowired
    mail_mail_statisticsFeignClient mail_mail_statisticsFeignClient;


    @Override
    public Mail_mail_statistics getDraft(Mail_mail_statistics et) {
        et=mail_mail_statisticsFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Mail_mail_statistics et) {
        Mail_mail_statistics rt = mail_mail_statisticsFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_mail_statistics> list){
        mail_mail_statisticsFeignClient.updateBatch(list) ;
    }

    @Override
    public Mail_mail_statistics get(Integer id) {
		Mail_mail_statistics et=mail_mail_statisticsFeignClient.get(id);
        if(et==null){
            et=new Mail_mail_statistics();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mail_mail_statisticsFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_mail_statisticsFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Mail_mail_statistics et) {
        Mail_mail_statistics rt = mail_mail_statisticsFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_mail_statistics> list){
        mail_mail_statisticsFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_mail_statistics> searchDefault(Mail_mail_statisticsSearchContext context) {
        Page<Mail_mail_statistics> mail_mail_statisticss=mail_mail_statisticsFeignClient.searchDefault(context);
        return mail_mail_statisticss;
    }


}


