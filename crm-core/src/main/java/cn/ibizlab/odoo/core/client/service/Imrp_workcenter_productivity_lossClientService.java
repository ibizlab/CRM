package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imrp_workcenter_productivity_loss;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mrp_workcenter_productivity_loss] 服务对象接口
 */
public interface Imrp_workcenter_productivity_lossClientService{

    public Imrp_workcenter_productivity_loss createModel() ;

    public void update(Imrp_workcenter_productivity_loss mrp_workcenter_productivity_loss);

    public void create(Imrp_workcenter_productivity_loss mrp_workcenter_productivity_loss);

    public void remove(Imrp_workcenter_productivity_loss mrp_workcenter_productivity_loss);

    public void removeBatch(List<Imrp_workcenter_productivity_loss> mrp_workcenter_productivity_losses);

    public Page<Imrp_workcenter_productivity_loss> fetchDefault(SearchContext context);

    public void createBatch(List<Imrp_workcenter_productivity_loss> mrp_workcenter_productivity_losses);

    public void updateBatch(List<Imrp_workcenter_productivity_loss> mrp_workcenter_productivity_losses);

    public void get(Imrp_workcenter_productivity_loss mrp_workcenter_productivity_loss);

    public Page<Imrp_workcenter_productivity_loss> select(SearchContext context);

    public void getDraft(Imrp_workcenter_productivity_loss mrp_workcenter_productivity_loss);

}
