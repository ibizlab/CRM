package cn.ibizlab.odoo.core.odoo_maintenance.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_equipment;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_equipmentSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[maintenance_equipment] 服务对象接口
 */
@Component
public class maintenance_equipmentFallback implements maintenance_equipmentFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Page<Maintenance_equipment> searchDefault(Maintenance_equipmentSearchContext context){
            return null;
     }


    public Maintenance_equipment create(Maintenance_equipment maintenance_equipment){
            return null;
     }
    public Boolean createBatch(List<Maintenance_equipment> maintenance_equipments){
            return false;
     }


    public Maintenance_equipment update(Integer id, Maintenance_equipment maintenance_equipment){
            return null;
     }
    public Boolean updateBatch(List<Maintenance_equipment> maintenance_equipments){
            return false;
     }


    public Maintenance_equipment get(Integer id){
            return null;
     }



    public Page<Maintenance_equipment> select(){
            return null;
     }

    public Maintenance_equipment getDraft(){
            return null;
    }



}
