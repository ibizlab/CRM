package cn.ibizlab.odoo.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_putaway;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_putawaySearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[product_putaway] 服务对象接口
 */
@FeignClient(value = "odoo-product", contextId = "product-putaway", fallback = product_putawayFallback.class)
public interface product_putawayFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/product_putaways")
    Product_putaway create(@RequestBody Product_putaway product_putaway);

    @RequestMapping(method = RequestMethod.POST, value = "/product_putaways/batch")
    Boolean createBatch(@RequestBody List<Product_putaway> product_putaways);



    @RequestMapping(method = RequestMethod.POST, value = "/product_putaways/searchdefault")
    Page<Product_putaway> searchDefault(@RequestBody Product_putawaySearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/product_putaways/{id}")
    Product_putaway update(@PathVariable("id") Integer id,@RequestBody Product_putaway product_putaway);

    @RequestMapping(method = RequestMethod.PUT, value = "/product_putaways/batch")
    Boolean updateBatch(@RequestBody List<Product_putaway> product_putaways);



    @RequestMapping(method = RequestMethod.DELETE, value = "/product_putaways/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/product_putaways/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/product_putaways/{id}")
    Product_putaway get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/product_putaways/select")
    Page<Product_putaway> select();


    @RequestMapping(method = RequestMethod.GET, value = "/product_putaways/getdraft")
    Product_putaway getDraft();


}
