package cn.ibizlab.odoo.core.odoo_sale.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_advance_payment_inv;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_advance_payment_invSearchContext;


/**
 * 实体[Sale_advance_payment_inv] 服务对象接口
 */
public interface ISale_advance_payment_invService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Sale_advance_payment_inv et) ;
    void updateBatch(List<Sale_advance_payment_inv> list) ;
    Sale_advance_payment_inv getDraft(Sale_advance_payment_inv et) ;
    Sale_advance_payment_inv get(Integer key) ;
    boolean create(Sale_advance_payment_inv et) ;
    void createBatch(List<Sale_advance_payment_inv> list) ;
    Page<Sale_advance_payment_inv> searchDefault(Sale_advance_payment_invSearchContext context) ;

}



