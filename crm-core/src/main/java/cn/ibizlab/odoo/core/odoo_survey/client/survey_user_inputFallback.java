package cn.ibizlab.odoo.core.odoo_survey.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_user_input;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_user_inputSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[survey_user_input] 服务对象接口
 */
@Component
public class survey_user_inputFallback implements survey_user_inputFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Survey_user_input> searchDefault(Survey_user_inputSearchContext context){
            return null;
     }


    public Survey_user_input create(Survey_user_input survey_user_input){
            return null;
     }
    public Boolean createBatch(List<Survey_user_input> survey_user_inputs){
            return false;
     }


    public Survey_user_input update(Integer id, Survey_user_input survey_user_input){
            return null;
     }
    public Boolean updateBatch(List<Survey_user_input> survey_user_inputs){
            return false;
     }



    public Survey_user_input get(Integer id){
            return null;
     }



    public Page<Survey_user_input> select(){
            return null;
     }

    public Survey_user_input getDraft(){
            return null;
    }



}
