package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_warehouse_orderpoint;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_warehouse_orderpoint] 服务对象接口
 */
public interface Istock_warehouse_orderpointClientService{

    public Istock_warehouse_orderpoint createModel() ;

    public void createBatch(List<Istock_warehouse_orderpoint> stock_warehouse_orderpoints);

    public void remove(Istock_warehouse_orderpoint stock_warehouse_orderpoint);

    public void updateBatch(List<Istock_warehouse_orderpoint> stock_warehouse_orderpoints);

    public void get(Istock_warehouse_orderpoint stock_warehouse_orderpoint);

    public void removeBatch(List<Istock_warehouse_orderpoint> stock_warehouse_orderpoints);

    public void update(Istock_warehouse_orderpoint stock_warehouse_orderpoint);

    public void create(Istock_warehouse_orderpoint stock_warehouse_orderpoint);

    public Page<Istock_warehouse_orderpoint> fetchDefault(SearchContext context);

    public Page<Istock_warehouse_orderpoint> select(SearchContext context);

    public void getDraft(Istock_warehouse_orderpoint stock_warehouse_orderpoint);

}
