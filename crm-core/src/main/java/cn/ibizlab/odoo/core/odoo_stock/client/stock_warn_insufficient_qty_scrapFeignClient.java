package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty_scrap;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warn_insufficient_qty_scrapSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_warn_insufficient_qty_scrap] 服务对象接口
 */
@FeignClient(value = "odoo-stock", contextId = "stock-warn-insufficient-qty-scrap", fallback = stock_warn_insufficient_qty_scrapFallback.class)
public interface stock_warn_insufficient_qty_scrapFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_scraps/searchdefault")
    Page<Stock_warn_insufficient_qty_scrap> searchDefault(@RequestBody Stock_warn_insufficient_qty_scrapSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_scraps")
    Stock_warn_insufficient_qty_scrap create(@RequestBody Stock_warn_insufficient_qty_scrap stock_warn_insufficient_qty_scrap);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_scraps/batch")
    Boolean createBatch(@RequestBody List<Stock_warn_insufficient_qty_scrap> stock_warn_insufficient_qty_scraps);



    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_warn_insufficient_qty_scraps/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_warn_insufficient_qty_scraps/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.PUT, value = "/stock_warn_insufficient_qty_scraps/{id}")
    Stock_warn_insufficient_qty_scrap update(@PathVariable("id") Integer id,@RequestBody Stock_warn_insufficient_qty_scrap stock_warn_insufficient_qty_scrap);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_warn_insufficient_qty_scraps/batch")
    Boolean updateBatch(@RequestBody List<Stock_warn_insufficient_qty_scrap> stock_warn_insufficient_qty_scraps);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_warn_insufficient_qty_scraps/{id}")
    Stock_warn_insufficient_qty_scrap get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_warn_insufficient_qty_scraps/select")
    Page<Stock_warn_insufficient_qty_scrap> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_warn_insufficient_qty_scraps/getdraft")
    Stock_warn_insufficient_qty_scrap getDraft();


}
