package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_reconcile_model_template;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_reconcile_model_template] 服务对象接口
 */
public interface Iaccount_reconcile_model_templateClientService{

    public Iaccount_reconcile_model_template createModel() ;

    public void remove(Iaccount_reconcile_model_template account_reconcile_model_template);

    public Page<Iaccount_reconcile_model_template> fetchDefault(SearchContext context);

    public void create(Iaccount_reconcile_model_template account_reconcile_model_template);

    public void update(Iaccount_reconcile_model_template account_reconcile_model_template);

    public void updateBatch(List<Iaccount_reconcile_model_template> account_reconcile_model_templates);

    public void get(Iaccount_reconcile_model_template account_reconcile_model_template);

    public void removeBatch(List<Iaccount_reconcile_model_template> account_reconcile_model_templates);

    public void createBatch(List<Iaccount_reconcile_model_template> account_reconcile_model_templates);

    public Page<Iaccount_reconcile_model_template> select(SearchContext context);

    public void getDraft(Iaccount_reconcile_model_template account_reconcile_model_template);

}
