package cn.ibizlab.odoo.core.odoo_web_tour.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_web_tour.domain.Web_tour_tour;
import cn.ibizlab.odoo.core.odoo_web_tour.filter.Web_tour_tourSearchContext;


/**
 * 实体[Web_tour_tour] 服务对象接口
 */
public interface IWeb_tour_tourService{

    boolean update(Web_tour_tour et) ;
    void updateBatch(List<Web_tour_tour> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Web_tour_tour get(Integer key) ;
    boolean create(Web_tour_tour et) ;
    void createBatch(List<Web_tour_tour> list) ;
    Web_tour_tour getDraft(Web_tour_tour et) ;
    Page<Web_tour_tour> searchDefault(Web_tour_tourSearchContext context) ;

}



