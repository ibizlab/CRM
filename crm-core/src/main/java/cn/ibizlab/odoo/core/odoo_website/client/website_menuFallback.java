package cn.ibizlab.odoo.core.odoo_website.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_menu;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_menuSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[website_menu] 服务对象接口
 */
@Component
public class website_menuFallback implements website_menuFeignClient{

    public Website_menu get(Integer id){
            return null;
     }




    public Website_menu create(Website_menu website_menu){
            return null;
     }
    public Boolean createBatch(List<Website_menu> website_menus){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Website_menu update(Integer id, Website_menu website_menu){
            return null;
     }
    public Boolean updateBatch(List<Website_menu> website_menus){
            return false;
     }


    public Page<Website_menu> searchDefault(Website_menuSearchContext context){
            return null;
     }



    public Page<Website_menu> select(){
            return null;
     }

    public Website_menu getDraft(){
            return null;
    }



}
