package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ipayment_acquirer;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[payment_acquirer] 服务对象接口
 */
public interface Ipayment_acquirerClientService{

    public Ipayment_acquirer createModel() ;

    public void update(Ipayment_acquirer payment_acquirer);

    public void create(Ipayment_acquirer payment_acquirer);

    public void createBatch(List<Ipayment_acquirer> payment_acquirers);

    public void get(Ipayment_acquirer payment_acquirer);

    public void removeBatch(List<Ipayment_acquirer> payment_acquirers);

    public void remove(Ipayment_acquirer payment_acquirer);

    public void updateBatch(List<Ipayment_acquirer> payment_acquirers);

    public Page<Ipayment_acquirer> fetchDefault(SearchContext context);

    public Page<Ipayment_acquirer> select(SearchContext context);

    public void getDraft(Ipayment_acquirer payment_acquirer);

}
