package cn.ibizlab.odoo.core.odoo_crm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_team;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_teamSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[crm_team] 服务对象接口
 */
@Component
public class crm_teamFallback implements crm_teamFeignClient{

    public Page<Crm_team> searchDefault(Crm_teamSearchContext context){
            return null;
     }


    public Crm_team create(Crm_team crm_team){
            return null;
     }
    public Boolean createBatch(List<Crm_team> crm_teams){
            return false;
     }




    public Crm_team get(Integer id){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Crm_team update(Integer id, Crm_team crm_team){
            return null;
     }
    public Boolean updateBatch(List<Crm_team> crm_teams){
            return false;
     }


    public Page<Crm_team> select(){
            return null;
     }

    public Boolean checkKey(Crm_team crm_team){
            return false;
     }


    public Crm_team getDraft(){
            return null;
    }



    public Boolean save(Crm_team crm_team){
            return false;
     }
    public Boolean saveBatch(List<Crm_team> crm_teams){
            return false;
     }

}
