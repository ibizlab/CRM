package cn.ibizlab.odoo.core.odoo_base.valuerule.anno.res_company;

import cn.ibizlab.odoo.core.odoo_base.valuerule.validator.res_company.Res_companyWebsite_theme_onboarding_doneDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Res_company
 * 属性：Website_theme_onboarding_done
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Res_companyWebsite_theme_onboarding_doneDefaultValidator.class})
public @interface Res_companyWebsite_theme_onboarding_doneDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
