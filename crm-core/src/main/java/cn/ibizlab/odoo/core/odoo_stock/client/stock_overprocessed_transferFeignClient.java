package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_overprocessed_transfer;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_overprocessed_transferSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_overprocessed_transfer] 服务对象接口
 */
@FeignClient(value = "odoo-stock", contextId = "stock-overprocessed-transfer", fallback = stock_overprocessed_transferFallback.class)
public interface stock_overprocessed_transferFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/stock_overprocessed_transfers")
    Stock_overprocessed_transfer create(@RequestBody Stock_overprocessed_transfer stock_overprocessed_transfer);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_overprocessed_transfers/batch")
    Boolean createBatch(@RequestBody List<Stock_overprocessed_transfer> stock_overprocessed_transfers);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_overprocessed_transfers/{id}")
    Stock_overprocessed_transfer get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.PUT, value = "/stock_overprocessed_transfers/{id}")
    Stock_overprocessed_transfer update(@PathVariable("id") Integer id,@RequestBody Stock_overprocessed_transfer stock_overprocessed_transfer);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_overprocessed_transfers/batch")
    Boolean updateBatch(@RequestBody List<Stock_overprocessed_transfer> stock_overprocessed_transfers);



    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_overprocessed_transfers/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_overprocessed_transfers/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);




    @RequestMapping(method = RequestMethod.POST, value = "/stock_overprocessed_transfers/searchdefault")
    Page<Stock_overprocessed_transfer> searchDefault(@RequestBody Stock_overprocessed_transferSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_overprocessed_transfers/select")
    Page<Stock_overprocessed_transfer> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_overprocessed_transfers/getdraft")
    Stock_overprocessed_transfer getDraft();


}
