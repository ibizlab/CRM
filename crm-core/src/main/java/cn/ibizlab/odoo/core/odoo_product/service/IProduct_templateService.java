package cn.ibizlab.odoo.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_product.domain.Product_template;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_templateSearchContext;


/**
 * 实体[Product_template] 服务对象接口
 */
public interface IProduct_templateService{

    boolean create(Product_template et) ;
    void createBatch(List<Product_template> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Product_template get(Integer key) ;
    Product_template getDraft(Product_template et) ;
    boolean update(Product_template et) ;
    void updateBatch(List<Product_template> list) ;
    Page<Product_template> searchDefault(Product_templateSearchContext context) ;

}



