package cn.ibizlab.odoo.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_holidays_summary_employee;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_holidays_summary_employeeSearchContext;


/**
 * 实体[Hr_holidays_summary_employee] 服务对象接口
 */
public interface IHr_holidays_summary_employeeService{

    boolean update(Hr_holidays_summary_employee et) ;
    void updateBatch(List<Hr_holidays_summary_employee> list) ;
    Hr_holidays_summary_employee get(Integer key) ;
    boolean create(Hr_holidays_summary_employee et) ;
    void createBatch(List<Hr_holidays_summary_employee> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Hr_holidays_summary_employee getDraft(Hr_holidays_summary_employee et) ;
    Page<Hr_holidays_summary_employee> searchDefault(Hr_holidays_summary_employeeSearchContext context) ;

}



