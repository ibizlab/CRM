package cn.ibizlab.odoo.core.odoo_event.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_event;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_eventSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[event_event] 服务对象接口
 */
@Component
public class event_eventFallback implements event_eventFeignClient{

    public Event_event create(Event_event event_event){
            return null;
     }
    public Boolean createBatch(List<Event_event> event_events){
            return false;
     }

    public Event_event get(Integer id){
            return null;
     }


    public Page<Event_event> searchDefault(Event_eventSearchContext context){
            return null;
     }




    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Event_event update(Integer id, Event_event event_event){
            return null;
     }
    public Boolean updateBatch(List<Event_event> event_events){
            return false;
     }



    public Page<Event_event> select(){
            return null;
     }

    public Event_event getDraft(){
            return null;
    }



}
