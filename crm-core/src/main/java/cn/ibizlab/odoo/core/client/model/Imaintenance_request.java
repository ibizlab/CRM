package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [maintenance_request] 对象
 */
public interface Imaintenance_request {

    /**
     * 获取 [下一活动截止日期]
     */
    public void setActivity_date_deadline(Timestamp activity_date_deadline);
    
    /**
     * 设置 [下一活动截止日期]
     */
    public Timestamp getActivity_date_deadline();

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    public boolean getActivity_date_deadlineDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setActivity_ids(String activity_ids);
    
    /**
     * 设置 [活动]
     */
    public String getActivity_ids();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getActivity_idsDirtyFlag();
    /**
     * 获取 [活动状态]
     */
    public void setActivity_state(String activity_state);
    
    /**
     * 设置 [活动状态]
     */
    public String getActivity_state();

    /**
     * 获取 [活动状态]脏标记
     */
    public boolean getActivity_stateDirtyFlag();
    /**
     * 获取 [下一活动摘要]
     */
    public void setActivity_summary(String activity_summary);
    
    /**
     * 设置 [下一活动摘要]
     */
    public String getActivity_summary();

    /**
     * 获取 [下一活动摘要]脏标记
     */
    public boolean getActivity_summaryDirtyFlag();
    /**
     * 获取 [下一活动类型]
     */
    public void setActivity_type_id(Integer activity_type_id);
    
    /**
     * 设置 [下一活动类型]
     */
    public Integer getActivity_type_id();

    /**
     * 获取 [下一活动类型]脏标记
     */
    public boolean getActivity_type_idDirtyFlag();
    /**
     * 获取 [责任用户]
     */
    public void setActivity_user_id(Integer activity_user_id);
    
    /**
     * 设置 [责任用户]
     */
    public Integer getActivity_user_id();

    /**
     * 获取 [责任用户]脏标记
     */
    public boolean getActivity_user_idDirtyFlag();
    /**
     * 获取 [归档]
     */
    public void setArchive(String archive);
    
    /**
     * 设置 [归档]
     */
    public String getArchive();

    /**
     * 获取 [归档]脏标记
     */
    public boolean getArchiveDirtyFlag();
    /**
     * 获取 [类别]
     */
    public void setCategory_id(Integer category_id);
    
    /**
     * 设置 [类别]
     */
    public Integer getCategory_id();

    /**
     * 获取 [类别]脏标记
     */
    public boolean getCategory_idDirtyFlag();
    /**
     * 获取 [类别]
     */
    public void setCategory_id_text(String category_id_text);
    
    /**
     * 设置 [类别]
     */
    public String getCategory_id_text();

    /**
     * 获取 [类别]脏标记
     */
    public boolean getCategory_id_textDirtyFlag();
    /**
     * 获取 [关闭日期]
     */
    public void setClose_date(Timestamp close_date);
    
    /**
     * 设置 [关闭日期]
     */
    public Timestamp getClose_date();

    /**
     * 获取 [关闭日期]脏标记
     */
    public boolean getClose_dateDirtyFlag();
    /**
     * 获取 [颜色索引]
     */
    public void setColor(Integer color);
    
    /**
     * 设置 [颜色索引]
     */
    public Integer getColor();

    /**
     * 获取 [颜色索引]脏标记
     */
    public boolean getColorDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [部门]
     */
    public void setDepartment_id(Integer department_id);
    
    /**
     * 设置 [部门]
     */
    public Integer getDepartment_id();

    /**
     * 获取 [部门]脏标记
     */
    public boolean getDepartment_idDirtyFlag();
    /**
     * 获取 [部门]
     */
    public void setDepartment_id_text(String department_id_text);
    
    /**
     * 设置 [部门]
     */
    public String getDepartment_id_text();

    /**
     * 获取 [部门]脏标记
     */
    public boolean getDepartment_id_textDirtyFlag();
    /**
     * 获取 [说明]
     */
    public void setDescription(String description);
    
    /**
     * 设置 [说明]
     */
    public String getDescription();

    /**
     * 获取 [说明]脏标记
     */
    public boolean getDescriptionDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [持续时间]
     */
    public void setDuration(Double duration);
    
    /**
     * 设置 [持续时间]
     */
    public Double getDuration();

    /**
     * 获取 [持续时间]脏标记
     */
    public boolean getDurationDirtyFlag();
    /**
     * 获取 [员工]
     */
    public void setEmployee_id(Integer employee_id);
    
    /**
     * 设置 [员工]
     */
    public Integer getEmployee_id();

    /**
     * 获取 [员工]脏标记
     */
    public boolean getEmployee_idDirtyFlag();
    /**
     * 获取 [员工]
     */
    public void setEmployee_id_text(String employee_id_text);
    
    /**
     * 设置 [员工]
     */
    public String getEmployee_id_text();

    /**
     * 获取 [员工]脏标记
     */
    public boolean getEmployee_id_textDirtyFlag();
    /**
     * 获取 [设备]
     */
    public void setEquipment_id(Integer equipment_id);
    
    /**
     * 设置 [设备]
     */
    public Integer getEquipment_id();

    /**
     * 获取 [设备]脏标记
     */
    public boolean getEquipment_idDirtyFlag();
    /**
     * 获取 [设备]
     */
    public void setEquipment_id_text(String equipment_id_text);
    
    /**
     * 设置 [设备]
     */
    public String getEquipment_id_text();

    /**
     * 获取 [设备]脏标记
     */
    public boolean getEquipment_id_textDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [看板状态]
     */
    public void setKanban_state(String kanban_state);
    
    /**
     * 设置 [看板状态]
     */
    public String getKanban_state();

    /**
     * 获取 [看板状态]脏标记
     */
    public boolean getKanban_stateDirtyFlag();
    /**
     * 获取 [团队]
     */
    public void setMaintenance_team_id(Integer maintenance_team_id);
    
    /**
     * 设置 [团队]
     */
    public Integer getMaintenance_team_id();

    /**
     * 获取 [团队]脏标记
     */
    public boolean getMaintenance_team_idDirtyFlag();
    /**
     * 获取 [团队]
     */
    public void setMaintenance_team_id_text(String maintenance_team_id_text);
    
    /**
     * 设置 [团队]
     */
    public String getMaintenance_team_id_text();

    /**
     * 获取 [团队]脏标记
     */
    public boolean getMaintenance_team_id_textDirtyFlag();
    /**
     * 获取 [保养类型]
     */
    public void setMaintenance_type(String maintenance_type);
    
    /**
     * 设置 [保养类型]
     */
    public String getMaintenance_type();

    /**
     * 获取 [保养类型]脏标记
     */
    public boolean getMaintenance_typeDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [是关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [是关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [是关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要采取行动]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要采取行动]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要采取行动]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [行动数量]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [行动数量]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [行动数量]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [主题]
     */
    public void setName(String name);
    
    /**
     * 设置 [主题]
     */
    public String getName();

    /**
     * 获取 [主题]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setOwner_user_id(Integer owner_user_id);
    
    /**
     * 设置 [创建人]
     */
    public Integer getOwner_user_id();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getOwner_user_idDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setOwner_user_id_text(String owner_user_id_text);
    
    /**
     * 设置 [创建人]
     */
    public String getOwner_user_id_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getOwner_user_id_textDirtyFlag();
    /**
     * 获取 [优先级]
     */
    public void setPriority(String priority);
    
    /**
     * 设置 [优先级]
     */
    public String getPriority();

    /**
     * 获取 [优先级]脏标记
     */
    public boolean getPriorityDirtyFlag();
    /**
     * 获取 [请求日期]
     */
    public void setRequest_date(Timestamp request_date);
    
    /**
     * 设置 [请求日期]
     */
    public Timestamp getRequest_date();

    /**
     * 获取 [请求日期]脏标记
     */
    public boolean getRequest_dateDirtyFlag();
    /**
     * 获取 [计划日期]
     */
    public void setSchedule_date(Timestamp schedule_date);
    
    /**
     * 设置 [计划日期]
     */
    public Timestamp getSchedule_date();

    /**
     * 获取 [计划日期]脏标记
     */
    public boolean getSchedule_dateDirtyFlag();
    /**
     * 获取 [阶段]
     */
    public void setStage_id(Integer stage_id);
    
    /**
     * 设置 [阶段]
     */
    public Integer getStage_id();

    /**
     * 获取 [阶段]脏标记
     */
    public boolean getStage_idDirtyFlag();
    /**
     * 获取 [阶段]
     */
    public void setStage_id_text(String stage_id_text);
    
    /**
     * 设置 [阶段]
     */
    public String getStage_id_text();

    /**
     * 获取 [阶段]脏标记
     */
    public boolean getStage_id_textDirtyFlag();
    /**
     * 获取 [技术员]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [技术员]
     */
    public Integer getUser_id();

    /**
     * 获取 [技术员]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [技术员]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [技术员]
     */
    public String getUser_id_text();

    /**
     * 获取 [技术员]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [网站消息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站消息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站消息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
