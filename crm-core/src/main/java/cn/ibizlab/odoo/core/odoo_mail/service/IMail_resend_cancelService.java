package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_resend_cancel;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_resend_cancelSearchContext;


/**
 * 实体[Mail_resend_cancel] 服务对象接口
 */
public interface IMail_resend_cancelService{

    Mail_resend_cancel getDraft(Mail_resend_cancel et) ;
    boolean update(Mail_resend_cancel et) ;
    void updateBatch(List<Mail_resend_cancel> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Mail_resend_cancel et) ;
    void createBatch(List<Mail_resend_cancel> list) ;
    Mail_resend_cancel get(Integer key) ;
    Page<Mail_resend_cancel> searchDefault(Mail_resend_cancelSearchContext context) ;

}



