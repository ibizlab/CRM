package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_tracking_value;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_tracking_valueSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_tracking_valueService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_tracking_valueFeignClient;

/**
 * 实体[邮件跟踪值] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_tracking_valueServiceImpl implements IMail_tracking_valueService {

    @Autowired
    mail_tracking_valueFeignClient mail_tracking_valueFeignClient;


    @Override
    public boolean update(Mail_tracking_value et) {
        Mail_tracking_value rt = mail_tracking_valueFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_tracking_value> list){
        mail_tracking_valueFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Mail_tracking_value et) {
        Mail_tracking_value rt = mail_tracking_valueFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_tracking_value> list){
        mail_tracking_valueFeignClient.createBatch(list) ;
    }

    @Override
    public Mail_tracking_value get(Integer id) {
		Mail_tracking_value et=mail_tracking_valueFeignClient.get(id);
        if(et==null){
            et=new Mail_tracking_value();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Mail_tracking_value getDraft(Mail_tracking_value et) {
        et=mail_tracking_valueFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mail_tracking_valueFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_tracking_valueFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_tracking_value> searchDefault(Mail_tracking_valueSearchContext context) {
        Page<Mail_tracking_value> mail_tracking_values=mail_tracking_valueFeignClient.searchDefault(context);
        return mail_tracking_values;
    }


}


