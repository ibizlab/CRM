package cn.ibizlab.odoo.core.odoo_payment.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_acquirer_onboarding_wizardSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[payment_acquirer_onboarding_wizard] 服务对象接口
 */
@Component
public class payment_acquirer_onboarding_wizardFallback implements payment_acquirer_onboarding_wizardFeignClient{


    public Payment_acquirer_onboarding_wizard create(Payment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard){
            return null;
     }
    public Boolean createBatch(List<Payment_acquirer_onboarding_wizard> payment_acquirer_onboarding_wizards){
            return false;
     }

    public Page<Payment_acquirer_onboarding_wizard> searchDefault(Payment_acquirer_onboarding_wizardSearchContext context){
            return null;
     }


    public Payment_acquirer_onboarding_wizard update(Integer id, Payment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard){
            return null;
     }
    public Boolean updateBatch(List<Payment_acquirer_onboarding_wizard> payment_acquirer_onboarding_wizards){
            return false;
     }



    public Payment_acquirer_onboarding_wizard get(Integer id){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Page<Payment_acquirer_onboarding_wizard> select(){
            return null;
     }

    public Payment_acquirer_onboarding_wizard getDraft(){
            return null;
    }



}
