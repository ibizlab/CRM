package cn.ibizlab.odoo.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_meterSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mro_pm_meter] 服务对象接口
 */
@Component
public class mro_pm_meterFallback implements mro_pm_meterFeignClient{

    public Mro_pm_meter update(Integer id, Mro_pm_meter mro_pm_meter){
            return null;
     }
    public Boolean updateBatch(List<Mro_pm_meter> mro_pm_meters){
            return false;
     }



    public Mro_pm_meter create(Mro_pm_meter mro_pm_meter){
            return null;
     }
    public Boolean createBatch(List<Mro_pm_meter> mro_pm_meters){
            return false;
     }

    public Mro_pm_meter get(Integer id){
            return null;
     }



    public Page<Mro_pm_meter> searchDefault(Mro_pm_meterSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Page<Mro_pm_meter> select(){
            return null;
     }

    public Mro_pm_meter getDraft(){
            return null;
    }



}
