package cn.ibizlab.odoo.core.odoo_uom.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_uom.domain.Uom_category;
import cn.ibizlab.odoo.core.odoo_uom.filter.Uom_categorySearchContext;
import cn.ibizlab.odoo.core.odoo_uom.service.IUom_categoryService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_uom.client.uom_categoryFeignClient;

/**
 * 实体[产品计量单位 类别] 服务对象接口实现
 */
@Slf4j
@Service
public class Uom_categoryServiceImpl implements IUom_categoryService {

    @Autowired
    uom_categoryFeignClient uom_categoryFeignClient;


    @Override
    public boolean create(Uom_category et) {
        Uom_category rt = uom_categoryFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Uom_category> list){
        uom_categoryFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Uom_category et) {
        Uom_category rt = uom_categoryFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Uom_category> list){
        uom_categoryFeignClient.updateBatch(list) ;
    }

    @Override
    public Uom_category get(Integer id) {
		Uom_category et=uom_categoryFeignClient.get(id);
        if(et==null){
            et=new Uom_category();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Uom_category getDraft(Uom_category et) {
        et=uom_categoryFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=uom_categoryFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        uom_categoryFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Uom_category> searchDefault(Uom_categorySearchContext context) {
        Page<Uom_category> uom_categorys=uom_categoryFeignClient.searchDefault(context);
        return uom_categorys;
    }


}


