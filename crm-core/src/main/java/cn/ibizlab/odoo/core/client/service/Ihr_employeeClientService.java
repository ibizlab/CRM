package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ihr_employee;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_employee] 服务对象接口
 */
public interface Ihr_employeeClientService{

    public Ihr_employee createModel() ;

    public Page<Ihr_employee> fetchDefault(SearchContext context);

    public void createBatch(List<Ihr_employee> hr_employees);

    public void update(Ihr_employee hr_employee);

    public void updateBatch(List<Ihr_employee> hr_employees);

    public void removeBatch(List<Ihr_employee> hr_employees);

    public void remove(Ihr_employee hr_employee);

    public void create(Ihr_employee hr_employee);

    public void get(Ihr_employee hr_employee);

    public Page<Ihr_employee> select(SearchContext context);

    public void getDraft(Ihr_employee hr_employee);

}
