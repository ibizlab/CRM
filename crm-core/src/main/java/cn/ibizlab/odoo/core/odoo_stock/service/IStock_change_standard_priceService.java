package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_change_standard_price;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_change_standard_priceSearchContext;


/**
 * 实体[Stock_change_standard_price] 服务对象接口
 */
public interface IStock_change_standard_priceService{

    Stock_change_standard_price getDraft(Stock_change_standard_price et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Stock_change_standard_price get(Integer key) ;
    boolean create(Stock_change_standard_price et) ;
    void createBatch(List<Stock_change_standard_price> list) ;
    boolean update(Stock_change_standard_price et) ;
    void updateBatch(List<Stock_change_standard_price> list) ;
    Page<Stock_change_standard_price> searchDefault(Stock_change_standard_priceSearchContext context) ;

}



