package cn.ibizlab.odoo.core.odoo_website.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_website.domain.Website_sale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_sale_payment_acquirer_onboarding_wizardSearchContext;


/**
 * 实体[Website_sale_payment_acquirer_onboarding_wizard] 服务对象接口
 */
public interface IWebsite_sale_payment_acquirer_onboarding_wizardService{

    Website_sale_payment_acquirer_onboarding_wizard get(Integer key) ;
    Website_sale_payment_acquirer_onboarding_wizard getDraft(Website_sale_payment_acquirer_onboarding_wizard et) ;
    boolean update(Website_sale_payment_acquirer_onboarding_wizard et) ;
    void updateBatch(List<Website_sale_payment_acquirer_onboarding_wizard> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Website_sale_payment_acquirer_onboarding_wizard et) ;
    void createBatch(List<Website_sale_payment_acquirer_onboarding_wizard> list) ;
    Page<Website_sale_payment_acquirer_onboarding_wizard> searchDefault(Website_sale_payment_acquirer_onboarding_wizardSearchContext context) ;

}



