package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_holidays_summary_employee;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_holidays_summary_employeeSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_holidays_summary_employeeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_hr.client.hr_holidays_summary_employeeFeignClient;

/**
 * 实体[按员工的休假摘要报告] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_holidays_summary_employeeServiceImpl implements IHr_holidays_summary_employeeService {

    @Autowired
    hr_holidays_summary_employeeFeignClient hr_holidays_summary_employeeFeignClient;


    @Override
    public boolean update(Hr_holidays_summary_employee et) {
        Hr_holidays_summary_employee rt = hr_holidays_summary_employeeFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Hr_holidays_summary_employee> list){
        hr_holidays_summary_employeeFeignClient.updateBatch(list) ;
    }

    @Override
    public Hr_holidays_summary_employee get(Integer id) {
		Hr_holidays_summary_employee et=hr_holidays_summary_employeeFeignClient.get(id);
        if(et==null){
            et=new Hr_holidays_summary_employee();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Hr_holidays_summary_employee et) {
        Hr_holidays_summary_employee rt = hr_holidays_summary_employeeFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_holidays_summary_employee> list){
        hr_holidays_summary_employeeFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=hr_holidays_summary_employeeFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        hr_holidays_summary_employeeFeignClient.removeBatch(idList);
    }

    @Override
    public Hr_holidays_summary_employee getDraft(Hr_holidays_summary_employee et) {
        et=hr_holidays_summary_employeeFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_holidays_summary_employee> searchDefault(Hr_holidays_summary_employeeSearchContext context) {
        Page<Hr_holidays_summary_employee> hr_holidays_summary_employees=hr_holidays_summary_employeeFeignClient.searchDefault(context);
        return hr_holidays_summary_employees;
    }


}


