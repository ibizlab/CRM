package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_account;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_accountSearchContext;


/**
 * 实体[Account_account] 服务对象接口
 */
public interface IAccount_accountService{

    Account_account get(Integer key) ;
    Account_account getDraft(Account_account et) ;
    boolean update(Account_account et) ;
    void updateBatch(List<Account_account> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Account_account et) ;
    void createBatch(List<Account_account> list) ;
    Page<Account_account> searchDefault(Account_accountSearchContext context) ;

}



