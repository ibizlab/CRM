package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_package_destination;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_package_destination] 服务对象接口
 */
public interface Istock_package_destinationClientService{

    public Istock_package_destination createModel() ;

    public Page<Istock_package_destination> fetchDefault(SearchContext context);

    public void update(Istock_package_destination stock_package_destination);

    public void removeBatch(List<Istock_package_destination> stock_package_destinations);

    public void get(Istock_package_destination stock_package_destination);

    public void create(Istock_package_destination stock_package_destination);

    public void remove(Istock_package_destination stock_package_destination);

    public void updateBatch(List<Istock_package_destination> stock_package_destinations);

    public void createBatch(List<Istock_package_destination> stock_package_destinations);

    public Page<Istock_package_destination> select(SearchContext context);

    public void getDraft(Istock_package_destination stock_package_destination);

}
