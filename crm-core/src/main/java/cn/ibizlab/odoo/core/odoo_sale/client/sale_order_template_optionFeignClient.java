package cn.ibizlab.odoo.core.odoo_sale.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_template_option;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_order_template_optionSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[sale_order_template_option] 服务对象接口
 */
@FeignClient(value = "odoo-sale", contextId = "sale-order-template-option", fallback = sale_order_template_optionFallback.class)
public interface sale_order_template_optionFeignClient {




    @RequestMapping(method = RequestMethod.DELETE, value = "/sale_order_template_options/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/sale_order_template_options/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/sale_order_template_options/{id}")
    Sale_order_template_option update(@PathVariable("id") Integer id,@RequestBody Sale_order_template_option sale_order_template_option);

    @RequestMapping(method = RequestMethod.PUT, value = "/sale_order_template_options/batch")
    Boolean updateBatch(@RequestBody List<Sale_order_template_option> sale_order_template_options);



    @RequestMapping(method = RequestMethod.POST, value = "/sale_order_template_options/searchdefault")
    Page<Sale_order_template_option> searchDefault(@RequestBody Sale_order_template_optionSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/sale_order_template_options")
    Sale_order_template_option create(@RequestBody Sale_order_template_option sale_order_template_option);

    @RequestMapping(method = RequestMethod.POST, value = "/sale_order_template_options/batch")
    Boolean createBatch(@RequestBody List<Sale_order_template_option> sale_order_template_options);


    @RequestMapping(method = RequestMethod.GET, value = "/sale_order_template_options/{id}")
    Sale_order_template_option get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/sale_order_template_options/select")
    Page<Sale_order_template_option> select();


    @RequestMapping(method = RequestMethod.GET, value = "/sale_order_template_options/getdraft")
    Sale_order_template_option getDraft();


}
