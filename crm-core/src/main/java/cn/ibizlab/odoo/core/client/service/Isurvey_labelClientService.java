package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Isurvey_label;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[survey_label] 服务对象接口
 */
public interface Isurvey_labelClientService{

    public Isurvey_label createModel() ;

    public void remove(Isurvey_label survey_label);

    public void updateBatch(List<Isurvey_label> survey_labels);

    public void update(Isurvey_label survey_label);

    public void get(Isurvey_label survey_label);

    public Page<Isurvey_label> fetchDefault(SearchContext context);

    public void removeBatch(List<Isurvey_label> survey_labels);

    public void createBatch(List<Isurvey_label> survey_labels);

    public void create(Isurvey_label survey_label);

    public Page<Isurvey_label> select(SearchContext context);

    public void getDraft(Isurvey_label survey_label);

}
