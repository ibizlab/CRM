package cn.ibizlab.odoo.core.odoo_fetchmail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_fetchmail.domain.Fetchmail_server;
import cn.ibizlab.odoo.core.odoo_fetchmail.filter.Fetchmail_serverSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[fetchmail_server] 服务对象接口
 */
@FeignClient(value = "odoo-fetchmail", contextId = "fetchmail-server", fallback = fetchmail_serverFallback.class)
public interface fetchmail_serverFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/fetchmail_servers/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/fetchmail_servers/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/fetchmail_servers")
    Fetchmail_server create(@RequestBody Fetchmail_server fetchmail_server);

    @RequestMapping(method = RequestMethod.POST, value = "/fetchmail_servers/batch")
    Boolean createBatch(@RequestBody List<Fetchmail_server> fetchmail_servers);


    @RequestMapping(method = RequestMethod.PUT, value = "/fetchmail_servers/{id}")
    Fetchmail_server update(@PathVariable("id") Integer id,@RequestBody Fetchmail_server fetchmail_server);

    @RequestMapping(method = RequestMethod.PUT, value = "/fetchmail_servers/batch")
    Boolean updateBatch(@RequestBody List<Fetchmail_server> fetchmail_servers);



    @RequestMapping(method = RequestMethod.POST, value = "/fetchmail_servers/searchdefault")
    Page<Fetchmail_server> searchDefault(@RequestBody Fetchmail_serverSearchContext context);




    @RequestMapping(method = RequestMethod.GET, value = "/fetchmail_servers/{id}")
    Fetchmail_server get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/fetchmail_servers/select")
    Page<Fetchmail_server> select();


    @RequestMapping(method = RequestMethod.GET, value = "/fetchmail_servers/getdraft")
    Fetchmail_server getDraft();


}
