package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_backorder_confirmation;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_backorder_confirmationSearchContext;


/**
 * 实体[Stock_backorder_confirmation] 服务对象接口
 */
public interface IStock_backorder_confirmationService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Stock_backorder_confirmation get(Integer key) ;
    Stock_backorder_confirmation getDraft(Stock_backorder_confirmation et) ;
    boolean update(Stock_backorder_confirmation et) ;
    void updateBatch(List<Stock_backorder_confirmation> list) ;
    boolean create(Stock_backorder_confirmation et) ;
    void createBatch(List<Stock_backorder_confirmation> list) ;
    Page<Stock_backorder_confirmation> searchDefault(Stock_backorder_confirmationSearchContext context) ;

}



