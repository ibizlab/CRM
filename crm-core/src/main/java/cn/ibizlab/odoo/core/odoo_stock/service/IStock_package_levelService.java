package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_package_level;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_package_levelSearchContext;


/**
 * 实体[Stock_package_level] 服务对象接口
 */
public interface IStock_package_levelService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Stock_package_level getDraft(Stock_package_level et) ;
    Stock_package_level get(Integer key) ;
    boolean create(Stock_package_level et) ;
    void createBatch(List<Stock_package_level> list) ;
    boolean update(Stock_package_level et) ;
    void updateBatch(List<Stock_package_level> list) ;
    Page<Stock_package_level> searchDefault(Stock_package_levelSearchContext context) ;

}



