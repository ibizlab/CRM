package cn.ibizlab.odoo.core.odoo_mail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [邮件跟踪值] 对象
 */
@Data
public class Mail_tracking_value extends EntityClient implements Serializable {

    /**
     * 字段说明
     */
    @DEField(name = "field_desc")
    @JSONField(name = "field_desc")
    @JsonProperty("field_desc")
    private String fieldDesc;

    /**
     * 旧字符值
     */
    @DEField(name = "old_value_char")
    @JSONField(name = "old_value_char")
    @JsonProperty("old_value_char")
    private String oldValueChar;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 新日期时间值
     */
    @DEField(name = "new_value_datetime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "new_value_datetime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("new_value_datetime")
    private Timestamp newValueDatetime;

    /**
     * 旧货币值
     */
    @DEField(name = "old_value_monetary")
    @JSONField(name = "old_value_monetary")
    @JsonProperty("old_value_monetary")
    private Double oldValueMonetary;

    /**
     * 新字符值
     */
    @DEField(name = "new_value_char")
    @JSONField(name = "new_value_char")
    @JsonProperty("new_value_char")
    private String newValueChar;

    /**
     * 新文本值
     */
    @DEField(name = "new_value_text")
    @JSONField(name = "new_value_text")
    @JsonProperty("new_value_text")
    private String newValueText;

    /**
     * 跟踪字段次序
     */
    @DEField(name = "track_sequence")
    @JSONField(name = "track_sequence")
    @JsonProperty("track_sequence")
    private Integer trackSequence;

    /**
     * 新货币值
     */
    @DEField(name = "new_value_monetary")
    @JSONField(name = "new_value_monetary")
    @JsonProperty("new_value_monetary")
    private Double newValueMonetary;

    /**
     * 旧日期时间值
     */
    @DEField(name = "old_value_datetime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "old_value_datetime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("old_value_datetime")
    private Timestamp oldValueDatetime;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 旧整数值
     */
    @DEField(name = "old_value_integer")
    @JSONField(name = "old_value_integer")
    @JsonProperty("old_value_integer")
    private Integer oldValueInteger;

    /**
     * 旧文本值
     */
    @DEField(name = "old_value_text")
    @JSONField(name = "old_value_text")
    @JsonProperty("old_value_text")
    private String oldValueText;

    /**
     * 字段类型
     */
    @DEField(name = "field_type")
    @JSONField(name = "field_type")
    @JsonProperty("field_type")
    private String fieldType;

    /**
     * 新整数值
     */
    @DEField(name = "new_value_integer")
    @JSONField(name = "new_value_integer")
    @JsonProperty("new_value_integer")
    private Integer newValueInteger;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 新浮点值
     */
    @DEField(name = "new_value_float")
    @JSONField(name = "new_value_float")
    @JsonProperty("new_value_float")
    private Double newValueFloat;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 更改的字段
     */
    @JSONField(name = "field")
    @JsonProperty("field")
    private String field;

    /**
     * 旧浮点值
     */
    @DEField(name = "old_value_float")
    @JSONField(name = "old_value_float")
    @JsonProperty("old_value_float")
    private Double oldValueFloat;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 邮件消息ID
     */
    @DEField(name = "mail_message_id")
    @JSONField(name = "mail_message_id")
    @JsonProperty("mail_message_id")
    private Integer mailMessageId;


    /**
     * 
     */
    @JSONField(name = "odoomailmessage")
    @JsonProperty("odoomailmessage")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_message odooMailMessage;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [字段说明]
     */
    public void setFieldDesc(String fieldDesc){
        this.fieldDesc = fieldDesc ;
        this.modify("field_desc",fieldDesc);
    }
    /**
     * 设置 [旧字符值]
     */
    public void setOldValueChar(String oldValueChar){
        this.oldValueChar = oldValueChar ;
        this.modify("old_value_char",oldValueChar);
    }
    /**
     * 设置 [新日期时间值]
     */
    public void setNewValueDatetime(Timestamp newValueDatetime){
        this.newValueDatetime = newValueDatetime ;
        this.modify("new_value_datetime",newValueDatetime);
    }
    /**
     * 设置 [旧货币值]
     */
    public void setOldValueMonetary(Double oldValueMonetary){
        this.oldValueMonetary = oldValueMonetary ;
        this.modify("old_value_monetary",oldValueMonetary);
    }
    /**
     * 设置 [新字符值]
     */
    public void setNewValueChar(String newValueChar){
        this.newValueChar = newValueChar ;
        this.modify("new_value_char",newValueChar);
    }
    /**
     * 设置 [新文本值]
     */
    public void setNewValueText(String newValueText){
        this.newValueText = newValueText ;
        this.modify("new_value_text",newValueText);
    }
    /**
     * 设置 [跟踪字段次序]
     */
    public void setTrackSequence(Integer trackSequence){
        this.trackSequence = trackSequence ;
        this.modify("track_sequence",trackSequence);
    }
    /**
     * 设置 [新货币值]
     */
    public void setNewValueMonetary(Double newValueMonetary){
        this.newValueMonetary = newValueMonetary ;
        this.modify("new_value_monetary",newValueMonetary);
    }
    /**
     * 设置 [旧日期时间值]
     */
    public void setOldValueDatetime(Timestamp oldValueDatetime){
        this.oldValueDatetime = oldValueDatetime ;
        this.modify("old_value_datetime",oldValueDatetime);
    }
    /**
     * 设置 [旧整数值]
     */
    public void setOldValueInteger(Integer oldValueInteger){
        this.oldValueInteger = oldValueInteger ;
        this.modify("old_value_integer",oldValueInteger);
    }
    /**
     * 设置 [旧文本值]
     */
    public void setOldValueText(String oldValueText){
        this.oldValueText = oldValueText ;
        this.modify("old_value_text",oldValueText);
    }
    /**
     * 设置 [字段类型]
     */
    public void setFieldType(String fieldType){
        this.fieldType = fieldType ;
        this.modify("field_type",fieldType);
    }
    /**
     * 设置 [新整数值]
     */
    public void setNewValueInteger(Integer newValueInteger){
        this.newValueInteger = newValueInteger ;
        this.modify("new_value_integer",newValueInteger);
    }
    /**
     * 设置 [新浮点值]
     */
    public void setNewValueFloat(Double newValueFloat){
        this.newValueFloat = newValueFloat ;
        this.modify("new_value_float",newValueFloat);
    }
    /**
     * 设置 [更改的字段]
     */
    public void setField(String field){
        this.field = field ;
        this.modify("field",field);
    }
    /**
     * 设置 [旧浮点值]
     */
    public void setOldValueFloat(Double oldValueFloat){
        this.oldValueFloat = oldValueFloat ;
        this.modify("old_value_float",oldValueFloat);
    }
    /**
     * 设置 [邮件消息ID]
     */
    public void setMailMessageId(Integer mailMessageId){
        this.mailMessageId = mailMessageId ;
        this.modify("mail_message_id",mailMessageId);
    }

}


