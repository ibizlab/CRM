package cn.ibizlab.odoo.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_recruitment_degree;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_recruitment_degreeSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[hr_recruitment_degree] 服务对象接口
 */
@Component
public class hr_recruitment_degreeFallback implements hr_recruitment_degreeFeignClient{

    public Page<Hr_recruitment_degree> searchDefault(Hr_recruitment_degreeSearchContext context){
            return null;
     }


    public Hr_recruitment_degree create(Hr_recruitment_degree hr_recruitment_degree){
            return null;
     }
    public Boolean createBatch(List<Hr_recruitment_degree> hr_recruitment_degrees){
            return false;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Hr_recruitment_degree update(Integer id, Hr_recruitment_degree hr_recruitment_degree){
            return null;
     }
    public Boolean updateBatch(List<Hr_recruitment_degree> hr_recruitment_degrees){
            return false;
     }


    public Hr_recruitment_degree get(Integer id){
            return null;
     }



    public Page<Hr_recruitment_degree> select(){
            return null;
     }

    public Hr_recruitment_degree getDraft(){
            return null;
    }



}
