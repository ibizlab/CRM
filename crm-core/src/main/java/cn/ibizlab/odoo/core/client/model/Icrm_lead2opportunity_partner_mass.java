package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [crm_lead2opportunity_partner_mass] 对象
 */
public interface Icrm_lead2opportunity_partner_mass {

    /**
     * 获取 [相关客户]
     */
    public void setAction(String action);
    
    /**
     * 设置 [相关客户]
     */
    public String getAction();

    /**
     * 获取 [相关客户]脏标记
     */
    public boolean getActionDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [删除重复内容]
     */
    public void setDeduplicate(String deduplicate);
    
    /**
     * 设置 [删除重复内容]
     */
    public String getDeduplicate();

    /**
     * 获取 [删除重复内容]脏标记
     */
    public boolean getDeduplicateDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [强制分配]
     */
    public void setForce_assignation(String force_assignation);
    
    /**
     * 设置 [强制分配]
     */
    public String getForce_assignation();

    /**
     * 获取 [强制分配]脏标记
     */
    public boolean getForce_assignationDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [转换动作]
     */
    public void setName(String name);
    
    /**
     * 设置 [转换动作]
     */
    public String getName();

    /**
     * 获取 [转换动作]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [商机]
     */
    public void setOpportunity_ids(String opportunity_ids);
    
    /**
     * 设置 [商机]
     */
    public String getOpportunity_ids();

    /**
     * 获取 [商机]脏标记
     */
    public boolean getOpportunity_idsDirtyFlag();
    /**
     * 获取 [客户]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [客户]
     */
    public Integer getPartner_id();

    /**
     * 获取 [客户]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [客户]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [客户]
     */
    public String getPartner_id_text();

    /**
     * 获取 [客户]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [销售团队]
     */
    public void setTeam_id(Integer team_id);
    
    /**
     * 设置 [销售团队]
     */
    public Integer getTeam_id();

    /**
     * 获取 [销售团队]脏标记
     */
    public boolean getTeam_idDirtyFlag();
    /**
     * 获取 [销售团队]
     */
    public void setTeam_id_text(String team_id_text);
    
    /**
     * 设置 [销售团队]
     */
    public String getTeam_id_text();

    /**
     * 获取 [销售团队]脏标记
     */
    public boolean getTeam_id_textDirtyFlag();
    /**
     * 获取 [销售员]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [销售员]
     */
    public Integer getUser_id();

    /**
     * 获取 [销售员]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [销售员]
     */
    public void setUser_ids(String user_ids);
    
    /**
     * 设置 [销售员]
     */
    public String getUser_ids();

    /**
     * 获取 [销售员]脏标记
     */
    public boolean getUser_idsDirtyFlag();
    /**
     * 获取 [销售员]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [销售员]
     */
    public String getUser_id_text();

    /**
     * 获取 [销售员]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
