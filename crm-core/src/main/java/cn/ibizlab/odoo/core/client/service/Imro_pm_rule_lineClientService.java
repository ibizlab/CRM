package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imro_pm_rule_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mro_pm_rule_line] 服务对象接口
 */
public interface Imro_pm_rule_lineClientService{

    public Imro_pm_rule_line createModel() ;

    public void remove(Imro_pm_rule_line mro_pm_rule_line);

    public void updateBatch(List<Imro_pm_rule_line> mro_pm_rule_lines);

    public void removeBatch(List<Imro_pm_rule_line> mro_pm_rule_lines);

    public void create(Imro_pm_rule_line mro_pm_rule_line);

    public void createBatch(List<Imro_pm_rule_line> mro_pm_rule_lines);

    public Page<Imro_pm_rule_line> fetchDefault(SearchContext context);

    public void update(Imro_pm_rule_line mro_pm_rule_line);

    public void get(Imro_pm_rule_line mro_pm_rule_line);

    public Page<Imro_pm_rule_line> select(SearchContext context);

    public void getDraft(Imro_pm_rule_line mro_pm_rule_line);

}
