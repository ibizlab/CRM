package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_move_reversal;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_move_reversalSearchContext;


/**
 * 实体[Account_move_reversal] 服务对象接口
 */
public interface IAccount_move_reversalService{

    Account_move_reversal get(Integer key) ;
    boolean update(Account_move_reversal et) ;
    void updateBatch(List<Account_move_reversal> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Account_move_reversal et) ;
    void createBatch(List<Account_move_reversal> list) ;
    Account_move_reversal getDraft(Account_move_reversal et) ;
    Page<Account_move_reversal> searchDefault(Account_move_reversalSearchContext context) ;

}



