package cn.ibizlab.odoo.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_product.domain.Product_template_attribute_exclusion;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_template_attribute_exclusionSearchContext;


/**
 * 实体[Product_template_attribute_exclusion] 服务对象接口
 */
public interface IProduct_template_attribute_exclusionService{

    boolean create(Product_template_attribute_exclusion et) ;
    void createBatch(List<Product_template_attribute_exclusion> list) ;
    Product_template_attribute_exclusion get(Integer key) ;
    boolean update(Product_template_attribute_exclusion et) ;
    void updateBatch(List<Product_template_attribute_exclusion> list) ;
    Product_template_attribute_exclusion getDraft(Product_template_attribute_exclusion et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Product_template_attribute_exclusion> searchDefault(Product_template_attribute_exclusionSearchContext context) ;

}



