package cn.ibizlab.odoo.core.odoo_utm.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_utm.domain.Utm_medium;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_mediumSearchContext;


/**
 * 实体[Utm_medium] 服务对象接口
 */
public interface IUtm_mediumService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Utm_medium et) ;
    void createBatch(List<Utm_medium> list) ;
    boolean update(Utm_medium et) ;
    void updateBatch(List<Utm_medium> list) ;
    Utm_medium get(Integer key) ;
    Utm_medium getDraft(Utm_medium et) ;
    Page<Utm_medium> searchDefault(Utm_mediumSearchContext context) ;

}



