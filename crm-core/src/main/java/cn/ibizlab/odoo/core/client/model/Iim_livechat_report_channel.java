package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [im_livechat_report_channel] 对象
 */
public interface Iim_livechat_report_channel {

    /**
     * 获取 [对话]
     */
    public void setChannel_id(Integer channel_id);
    
    /**
     * 设置 [对话]
     */
    public Integer getChannel_id();

    /**
     * 获取 [对话]脏标记
     */
    public boolean getChannel_idDirtyFlag();
    /**
     * 获取 [对话]
     */
    public void setChannel_id_text(String channel_id_text);
    
    /**
     * 设置 [对话]
     */
    public String getChannel_id_text();

    /**
     * 获取 [对话]脏标记
     */
    public boolean getChannel_id_textDirtyFlag();
    /**
     * 获取 [渠道名称]
     */
    public void setChannel_name(String channel_name);
    
    /**
     * 设置 [渠道名称]
     */
    public String getChannel_name();

    /**
     * 获取 [渠道名称]脏标记
     */
    public boolean getChannel_nameDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [平均时间]
     */
    public void setDuration(Double duration);
    
    /**
     * 设置 [平均时间]
     */
    public Double getDuration();

    /**
     * 获取 [平均时间]脏标记
     */
    public boolean getDurationDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [渠道]
     */
    public void setLivechat_channel_id(Integer livechat_channel_id);
    
    /**
     * 设置 [渠道]
     */
    public Integer getLivechat_channel_id();

    /**
     * 获取 [渠道]脏标记
     */
    public boolean getLivechat_channel_idDirtyFlag();
    /**
     * 获取 [渠道]
     */
    public void setLivechat_channel_id_text(String livechat_channel_id_text);
    
    /**
     * 设置 [渠道]
     */
    public String getLivechat_channel_id_text();

    /**
     * 获取 [渠道]脏标记
     */
    public boolean getLivechat_channel_id_textDirtyFlag();
    /**
     * 获取 [每个消息]
     */
    public void setNbr_message(Integer nbr_message);
    
    /**
     * 设置 [每个消息]
     */
    public Integer getNbr_message();

    /**
     * 获取 [每个消息]脏标记
     */
    public boolean getNbr_messageDirtyFlag();
    /**
     * 获取 [讲解人]
     */
    public void setNbr_speaker(Integer nbr_speaker);
    
    /**
     * 设置 [讲解人]
     */
    public Integer getNbr_speaker();

    /**
     * 获取 [讲解人]脏标记
     */
    public boolean getNbr_speakerDirtyFlag();
    /**
     * 获取 [运算符]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [运算符]
     */
    public Integer getPartner_id();

    /**
     * 获取 [运算符]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [运算符]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [运算符]
     */
    public String getPartner_id_text();

    /**
     * 获取 [运算符]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [会话的开始日期]
     */
    public void setStart_date(Timestamp start_date);
    
    /**
     * 设置 [会话的开始日期]
     */
    public Timestamp getStart_date();

    /**
     * 获取 [会话的开始日期]脏标记
     */
    public boolean getStart_dateDirtyFlag();
    /**
     * 获取 [会话开始的小时数]
     */
    public void setStart_date_hour(String start_date_hour);
    
    /**
     * 设置 [会话开始的小时数]
     */
    public String getStart_date_hour();

    /**
     * 获取 [会话开始的小时数]脏标记
     */
    public boolean getStart_date_hourDirtyFlag();
    /**
     * 获取 [代号]
     */
    public void setTechnical_name(String technical_name);
    
    /**
     * 设置 [代号]
     */
    public String getTechnical_name();

    /**
     * 获取 [代号]脏标记
     */
    public boolean getTechnical_nameDirtyFlag();
    /**
     * 获取 [UUID]
     */
    public void setUuid(String uuid);
    
    /**
     * 设置 [UUID]
     */
    public String getUuid();

    /**
     * 获取 [UUID]脏标记
     */
    public boolean getUuidDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
