package cn.ibizlab.odoo.core.odoo_lunch.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_product_category;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_product_categorySearchContext;


/**
 * 实体[Lunch_product_category] 服务对象接口
 */
public interface ILunch_product_categoryService{

    Lunch_product_category get(Integer key) ;
    boolean update(Lunch_product_category et) ;
    void updateBatch(List<Lunch_product_category> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Lunch_product_category getDraft(Lunch_product_category et) ;
    boolean create(Lunch_product_category et) ;
    void createBatch(List<Lunch_product_category> list) ;
    Page<Lunch_product_category> searchDefault(Lunch_product_categorySearchContext context) ;

}



