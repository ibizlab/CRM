package cn.ibizlab.odoo.core.odoo_resource.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_mixin;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_mixinSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[resource_mixin] 服务对象接口
 */
@FeignClient(value = "odoo-resource", contextId = "resource-mixin", fallback = resource_mixinFallback.class)
public interface resource_mixinFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/resource_mixins")
    Resource_mixin create(@RequestBody Resource_mixin resource_mixin);

    @RequestMapping(method = RequestMethod.POST, value = "/resource_mixins/batch")
    Boolean createBatch(@RequestBody List<Resource_mixin> resource_mixins);




    @RequestMapping(method = RequestMethod.POST, value = "/resource_mixins/searchdefault")
    Page<Resource_mixin> searchDefault(@RequestBody Resource_mixinSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/resource_mixins/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/resource_mixins/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/resource_mixins/{id}")
    Resource_mixin get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/resource_mixins/{id}")
    Resource_mixin update(@PathVariable("id") Integer id,@RequestBody Resource_mixin resource_mixin);

    @RequestMapping(method = RequestMethod.PUT, value = "/resource_mixins/batch")
    Boolean updateBatch(@RequestBody List<Resource_mixin> resource_mixins);



    @RequestMapping(method = RequestMethod.GET, value = "/resource_mixins/select")
    Page<Resource_mixin> select();


    @RequestMapping(method = RequestMethod.GET, value = "/resource_mixins/getdraft")
    Resource_mixin getDraft();


}
