package cn.ibizlab.odoo.core.odoo_sms.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_sms.domain.Sms_send_sms;
import cn.ibizlab.odoo.core.odoo_sms.filter.Sms_send_smsSearchContext;
import cn.ibizlab.odoo.core.odoo_sms.service.ISms_send_smsService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_sms.client.sms_send_smsFeignClient;

/**
 * 实体[发送短信] 服务对象接口实现
 */
@Slf4j
@Service
public class Sms_send_smsServiceImpl implements ISms_send_smsService {

    @Autowired
    sms_send_smsFeignClient sms_send_smsFeignClient;


    @Override
    public boolean update(Sms_send_sms et) {
        Sms_send_sms rt = sms_send_smsFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Sms_send_sms> list){
        sms_send_smsFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Sms_send_sms et) {
        Sms_send_sms rt = sms_send_smsFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Sms_send_sms> list){
        sms_send_smsFeignClient.createBatch(list) ;
    }

    @Override
    public Sms_send_sms getDraft(Sms_send_sms et) {
        et=sms_send_smsFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=sms_send_smsFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        sms_send_smsFeignClient.removeBatch(idList);
    }

    @Override
    public Sms_send_sms get(Integer id) {
		Sms_send_sms et=sms_send_smsFeignClient.get(id);
        if(et==null){
            et=new Sms_send_sms();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Sms_send_sms> searchDefault(Sms_send_smsSearchContext context) {
        Page<Sms_send_sms> sms_send_smss=sms_send_smsFeignClient.searchDefault(context);
        return sms_send_smss;
    }


}


