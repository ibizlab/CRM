package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_term_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_payment_term_lineSearchContext;


/**
 * 实体[Account_payment_term_line] 服务对象接口
 */
public interface IAccount_payment_term_lineService{

    boolean update(Account_payment_term_line et) ;
    void updateBatch(List<Account_payment_term_line> list) ;
    boolean create(Account_payment_term_line et) ;
    void createBatch(List<Account_payment_term_line> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Account_payment_term_line get(Integer key) ;
    Account_payment_term_line getDraft(Account_payment_term_line et) ;
    Page<Account_payment_term_line> searchDefault(Account_payment_term_lineSearchContext context) ;

}



