package cn.ibizlab.odoo.core.odoo_asset.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_asset.domain.Asset_asset;
import cn.ibizlab.odoo.core.odoo_asset.filter.Asset_assetSearchContext;


/**
 * 实体[Asset_asset] 服务对象接口
 */
public interface IAsset_assetService{

    boolean update(Asset_asset et) ;
    void updateBatch(List<Asset_asset> list) ;
    Asset_asset get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Asset_asset getDraft(Asset_asset et) ;
    boolean create(Asset_asset et) ;
    void createBatch(List<Asset_asset> list) ;
    Page<Asset_asset> searchDefault(Asset_assetSearchContext context) ;

}



