package cn.ibizlab.odoo.core.odoo_fleet.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_model_brand;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_model_brandSearchContext;


/**
 * 实体[Fleet_vehicle_model_brand] 服务对象接口
 */
public interface IFleet_vehicle_model_brandService{

    boolean create(Fleet_vehicle_model_brand et) ;
    void createBatch(List<Fleet_vehicle_model_brand> list) ;
    boolean update(Fleet_vehicle_model_brand et) ;
    void updateBatch(List<Fleet_vehicle_model_brand> list) ;
    Fleet_vehicle_model_brand get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Fleet_vehicle_model_brand getDraft(Fleet_vehicle_model_brand et) ;
    Page<Fleet_vehicle_model_brand> searchDefault(Fleet_vehicle_model_brandSearchContext context) ;

}



