package cn.ibizlab.odoo.core.odoo_lunch.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_order;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_orderSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[lunch_order] 服务对象接口
 */
@Component
public class lunch_orderFallback implements lunch_orderFeignClient{



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Lunch_order> searchDefault(Lunch_orderSearchContext context){
            return null;
     }


    public Lunch_order create(Lunch_order lunch_order){
            return null;
     }
    public Boolean createBatch(List<Lunch_order> lunch_orders){
            return false;
     }


    public Lunch_order get(Integer id){
            return null;
     }


    public Lunch_order update(Integer id, Lunch_order lunch_order){
            return null;
     }
    public Boolean updateBatch(List<Lunch_order> lunch_orders){
            return false;
     }


    public Page<Lunch_order> select(){
            return null;
     }

    public Lunch_order getDraft(){
            return null;
    }



}
