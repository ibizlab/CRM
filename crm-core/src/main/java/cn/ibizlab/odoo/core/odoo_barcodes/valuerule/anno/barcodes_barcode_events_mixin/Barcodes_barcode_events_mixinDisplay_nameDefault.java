package cn.ibizlab.odoo.core.odoo_barcodes.valuerule.anno.barcodes_barcode_events_mixin;

import cn.ibizlab.odoo.core.odoo_barcodes.valuerule.validator.barcodes_barcode_events_mixin.Barcodes_barcode_events_mixinDisplay_nameDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Barcodes_barcode_events_mixin
 * 属性：Display_name
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Barcodes_barcode_events_mixinDisplay_nameDefaultValidator.class})
public @interface Barcodes_barcode_events_mixinDisplay_nameDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[100]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
