package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_autocomplete_sync;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_autocomplete_syncSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[res_partner_autocomplete_sync] 服务对象接口
 */
@Component
public class res_partner_autocomplete_syncFallback implements res_partner_autocomplete_syncFeignClient{

    public Res_partner_autocomplete_sync update(Integer id, Res_partner_autocomplete_sync res_partner_autocomplete_sync){
            return null;
     }
    public Boolean updateBatch(List<Res_partner_autocomplete_sync> res_partner_autocomplete_syncs){
            return false;
     }



    public Res_partner_autocomplete_sync get(Integer id){
            return null;
     }


    public Res_partner_autocomplete_sync create(Res_partner_autocomplete_sync res_partner_autocomplete_sync){
            return null;
     }
    public Boolean createBatch(List<Res_partner_autocomplete_sync> res_partner_autocomplete_syncs){
            return false;
     }


    public Page<Res_partner_autocomplete_sync> searchDefault(Res_partner_autocomplete_syncSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Page<Res_partner_autocomplete_sync> select(){
            return null;
     }

    public Res_partner_autocomplete_sync getDraft(){
            return null;
    }



}
