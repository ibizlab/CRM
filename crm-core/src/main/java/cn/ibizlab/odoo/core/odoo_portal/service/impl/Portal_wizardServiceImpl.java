package cn.ibizlab.odoo.core.odoo_portal.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_wizard;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_wizardSearchContext;
import cn.ibizlab.odoo.core.odoo_portal.service.IPortal_wizardService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_portal.client.portal_wizardFeignClient;

/**
 * 实体[授予门户访问] 服务对象接口实现
 */
@Slf4j
@Service
public class Portal_wizardServiceImpl implements IPortal_wizardService {

    @Autowired
    portal_wizardFeignClient portal_wizardFeignClient;


    @Override
    public boolean update(Portal_wizard et) {
        Portal_wizard rt = portal_wizardFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Portal_wizard> list){
        portal_wizardFeignClient.updateBatch(list) ;
    }

    @Override
    public Portal_wizard getDraft(Portal_wizard et) {
        et=portal_wizardFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=portal_wizardFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        portal_wizardFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Portal_wizard et) {
        Portal_wizard rt = portal_wizardFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Portal_wizard> list){
        portal_wizardFeignClient.createBatch(list) ;
    }

    @Override
    public Portal_wizard get(Integer id) {
		Portal_wizard et=portal_wizardFeignClient.get(id);
        if(et==null){
            et=new Portal_wizard();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Portal_wizard> searchDefault(Portal_wizardSearchContext context) {
        Page<Portal_wizard> portal_wizards=portal_wizardFeignClient.searchDefault(context);
        return portal_wizards;
    }


}


