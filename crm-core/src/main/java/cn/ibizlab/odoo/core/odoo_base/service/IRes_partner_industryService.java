package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_industry;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_industrySearchContext;


/**
 * 实体[Res_partner_industry] 服务对象接口
 */
public interface IRes_partner_industryService{

    boolean update(Res_partner_industry et) ;
    void updateBatch(List<Res_partner_industry> list) ;
    boolean create(Res_partner_industry et) ;
    void createBatch(List<Res_partner_industry> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Res_partner_industry get(Integer key) ;
    Res_partner_industry getDraft(Res_partner_industry et) ;
    Page<Res_partner_industry> searchDefault(Res_partner_industrySearchContext context) ;

}



