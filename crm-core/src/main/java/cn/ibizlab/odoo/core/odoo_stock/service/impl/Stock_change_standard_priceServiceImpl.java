package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_change_standard_price;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_change_standard_priceSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_change_standard_priceService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_change_standard_priceFeignClient;

/**
 * 实体[更改标准价] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_change_standard_priceServiceImpl implements IStock_change_standard_priceService {

    @Autowired
    stock_change_standard_priceFeignClient stock_change_standard_priceFeignClient;


    @Override
    public Stock_change_standard_price getDraft(Stock_change_standard_price et) {
        et=stock_change_standard_priceFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=stock_change_standard_priceFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_change_standard_priceFeignClient.removeBatch(idList);
    }

    @Override
    public Stock_change_standard_price get(Integer id) {
		Stock_change_standard_price et=stock_change_standard_priceFeignClient.get(id);
        if(et==null){
            et=new Stock_change_standard_price();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Stock_change_standard_price et) {
        Stock_change_standard_price rt = stock_change_standard_priceFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_change_standard_price> list){
        stock_change_standard_priceFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Stock_change_standard_price et) {
        Stock_change_standard_price rt = stock_change_standard_priceFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_change_standard_price> list){
        stock_change_standard_priceFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_change_standard_price> searchDefault(Stock_change_standard_priceSearchContext context) {
        Page<Stock_change_standard_price> stock_change_standard_prices=stock_change_standard_priceFeignClient.searchDefault(context);
        return stock_change_standard_prices;
    }


}


