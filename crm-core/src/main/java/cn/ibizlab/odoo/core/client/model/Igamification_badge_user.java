package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [gamification_badge_user] 对象
 */
public interface Igamification_badge_user {

    /**
     * 获取 [徽章]
     */
    public void setBadge_id(Integer badge_id);
    
    /**
     * 设置 [徽章]
     */
    public Integer getBadge_id();

    /**
     * 获取 [徽章]脏标记
     */
    public boolean getBadge_idDirtyFlag();
    /**
     * 获取 [徽章名称]
     */
    public void setBadge_name(String badge_name);
    
    /**
     * 设置 [徽章名称]
     */
    public String getBadge_name();

    /**
     * 获取 [徽章名称]脏标记
     */
    public boolean getBadge_nameDirtyFlag();
    /**
     * 获取 [挑战源于]
     */
    public void setChallenge_id(Integer challenge_id);
    
    /**
     * 设置 [挑战源于]
     */
    public Integer getChallenge_id();

    /**
     * 获取 [挑战源于]脏标记
     */
    public boolean getChallenge_idDirtyFlag();
    /**
     * 获取 [挑战源于]
     */
    public void setChallenge_id_text(String challenge_id_text);
    
    /**
     * 设置 [挑战源于]
     */
    public String getChallenge_id_text();

    /**
     * 获取 [挑战源于]脏标记
     */
    public boolean getChallenge_id_textDirtyFlag();
    /**
     * 获取 [备注]
     */
    public void setComment(String comment);
    
    /**
     * 设置 [备注]
     */
    public String getComment();

    /**
     * 获取 [备注]脏标记
     */
    public boolean getCommentDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [员工]
     */
    public void setEmployee_id(Integer employee_id);
    
    /**
     * 设置 [员工]
     */
    public Integer getEmployee_id();

    /**
     * 获取 [员工]脏标记
     */
    public boolean getEmployee_idDirtyFlag();
    /**
     * 获取 [员工]
     */
    public void setEmployee_id_text(String employee_id_text);
    
    /**
     * 设置 [员工]
     */
    public String getEmployee_id_text();

    /**
     * 获取 [员工]脏标记
     */
    public boolean getEmployee_id_textDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [论坛徽章等级]
     */
    public void setLevel(String level);
    
    /**
     * 设置 [论坛徽章等级]
     */
    public String getLevel();

    /**
     * 获取 [论坛徽章等级]脏标记
     */
    public boolean getLevelDirtyFlag();
    /**
     * 获取 [发送者]
     */
    public void setSender_id(Integer sender_id);
    
    /**
     * 设置 [发送者]
     */
    public Integer getSender_id();

    /**
     * 获取 [发送者]脏标记
     */
    public boolean getSender_idDirtyFlag();
    /**
     * 获取 [发送者]
     */
    public void setSender_id_text(String sender_id_text);
    
    /**
     * 设置 [发送者]
     */
    public String getSender_id_text();

    /**
     * 获取 [发送者]脏标记
     */
    public boolean getSender_id_textDirtyFlag();
    /**
     * 获取 [用户]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [用户]
     */
    public Integer getUser_id();

    /**
     * 获取 [用户]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [用户]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [用户]
     */
    public String getUser_id_text();

    /**
     * 获取 [用户]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
