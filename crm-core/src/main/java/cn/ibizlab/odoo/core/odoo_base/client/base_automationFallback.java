package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_automation;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_automationSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[base_automation] 服务对象接口
 */
@Component
public class base_automationFallback implements base_automationFeignClient{




    public Base_automation create(Base_automation base_automation){
            return null;
     }
    public Boolean createBatch(List<Base_automation> base_automations){
            return false;
     }

    public Base_automation get(Integer id){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Base_automation update(Integer id, Base_automation base_automation){
            return null;
     }
    public Boolean updateBatch(List<Base_automation> base_automations){
            return false;
     }


    public Page<Base_automation> searchDefault(Base_automationSearchContext context){
            return null;
     }


    public Page<Base_automation> select(){
            return null;
     }

    public Base_automation getDraft(){
            return null;
    }



}
