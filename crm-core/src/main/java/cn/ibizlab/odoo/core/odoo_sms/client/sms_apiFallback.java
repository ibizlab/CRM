package cn.ibizlab.odoo.core.odoo_sms.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_sms.domain.Sms_api;
import cn.ibizlab.odoo.core.odoo_sms.filter.Sms_apiSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[sms_api] 服务对象接口
 */
@Component
public class sms_apiFallback implements sms_apiFeignClient{

    public Sms_api create(Sms_api sms_api){
            return null;
     }
    public Boolean createBatch(List<Sms_api> sms_apis){
            return false;
     }



    public Page<Sms_api> searchDefault(Sms_apiSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Sms_api get(Integer id){
            return null;
     }


    public Sms_api update(Integer id, Sms_api sms_api){
            return null;
     }
    public Boolean updateBatch(List<Sms_api> sms_apis){
            return false;
     }



    public Page<Sms_api> select(){
            return null;
     }

    public Sms_api getDraft(){
            return null;
    }



}
