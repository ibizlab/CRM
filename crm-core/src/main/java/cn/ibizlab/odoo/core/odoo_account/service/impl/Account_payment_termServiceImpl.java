package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_term;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_payment_termSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_payment_termService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_payment_termFeignClient;

/**
 * 实体[付款条款] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_payment_termServiceImpl implements IAccount_payment_termService {

    @Autowired
    account_payment_termFeignClient account_payment_termFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=account_payment_termFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_payment_termFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Account_payment_term et) {
        Account_payment_term rt = account_payment_termFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_payment_term> list){
        account_payment_termFeignClient.createBatch(list) ;
    }

    @Override
    public Account_payment_term get(Integer id) {
		Account_payment_term et=account_payment_termFeignClient.get(id);
        if(et==null){
            et=new Account_payment_term();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Account_payment_term getDraft(Account_payment_term et) {
        et=account_payment_termFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Account_payment_term et) {
        Account_payment_term rt = account_payment_termFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_payment_term> list){
        account_payment_termFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_payment_term> searchDefault(Account_payment_termSearchContext context) {
        Page<Account_payment_term> account_payment_terms=account_payment_termFeignClient.searchDefault(context);
        return account_payment_terms;
    }


}


