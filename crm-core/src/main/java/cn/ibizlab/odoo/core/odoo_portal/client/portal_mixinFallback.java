package cn.ibizlab.odoo.core.odoo_portal.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_mixin;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_mixinSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[portal_mixin] 服务对象接口
 */
@Component
public class portal_mixinFallback implements portal_mixinFeignClient{

    public Portal_mixin create(Portal_mixin portal_mixin){
            return null;
     }
    public Boolean createBatch(List<Portal_mixin> portal_mixins){
            return false;
     }

    public Page<Portal_mixin> searchDefault(Portal_mixinSearchContext context){
            return null;
     }



    public Portal_mixin update(Integer id, Portal_mixin portal_mixin){
            return null;
     }
    public Boolean updateBatch(List<Portal_mixin> portal_mixins){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }



    public Portal_mixin get(Integer id){
            return null;
     }


    public Page<Portal_mixin> select(){
            return null;
     }

    public Portal_mixin getDraft(){
            return null;
    }



}
