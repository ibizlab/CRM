package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_style;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_styleSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_styleService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_product.client.product_styleFeignClient;

/**
 * 实体[产品风格] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_styleServiceImpl implements IProduct_styleService {

    @Autowired
    product_styleFeignClient product_styleFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=product_styleFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        product_styleFeignClient.removeBatch(idList);
    }

    @Override
    public Product_style get(Integer id) {
		Product_style et=product_styleFeignClient.get(id);
        if(et==null){
            et=new Product_style();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Product_style getDraft(Product_style et) {
        et=product_styleFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Product_style et) {
        Product_style rt = product_styleFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_style> list){
        product_styleFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Product_style et) {
        Product_style rt = product_styleFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Product_style> list){
        product_styleFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_style> searchDefault(Product_styleSearchContext context) {
        Page<Product_style> product_styles=product_styleFeignClient.searchDefault(context);
        return product_styles;
    }


}


