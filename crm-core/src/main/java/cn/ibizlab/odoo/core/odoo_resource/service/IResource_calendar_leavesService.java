package cn.ibizlab.odoo.core.odoo_resource.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar_leaves;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_calendar_leavesSearchContext;


/**
 * 实体[Resource_calendar_leaves] 服务对象接口
 */
public interface IResource_calendar_leavesService{

    boolean create(Resource_calendar_leaves et) ;
    void createBatch(List<Resource_calendar_leaves> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Resource_calendar_leaves et) ;
    void updateBatch(List<Resource_calendar_leaves> list) ;
    Resource_calendar_leaves getDraft(Resource_calendar_leaves et) ;
    Resource_calendar_leaves get(Integer key) ;
    Page<Resource_calendar_leaves> searchDefault(Resource_calendar_leavesSearchContext context) ;

}



