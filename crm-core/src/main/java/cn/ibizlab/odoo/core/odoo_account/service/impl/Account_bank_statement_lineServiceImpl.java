package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_bank_statement_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_bank_statement_lineFeignClient;

/**
 * 实体[银行对账单明细] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_bank_statement_lineServiceImpl implements IAccount_bank_statement_lineService {

    @Autowired
    account_bank_statement_lineFeignClient account_bank_statement_lineFeignClient;


    @Override
    public boolean update(Account_bank_statement_line et) {
        Account_bank_statement_line rt = account_bank_statement_lineFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_bank_statement_line> list){
        account_bank_statement_lineFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_bank_statement_lineFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_bank_statement_lineFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Account_bank_statement_line et) {
        Account_bank_statement_line rt = account_bank_statement_lineFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_bank_statement_line> list){
        account_bank_statement_lineFeignClient.createBatch(list) ;
    }

    @Override
    public Account_bank_statement_line getDraft(Account_bank_statement_line et) {
        et=account_bank_statement_lineFeignClient.getDraft();
        return et;
    }

    @Override
    public Account_bank_statement_line get(Integer id) {
		Account_bank_statement_line et=account_bank_statement_lineFeignClient.get(id);
        if(et==null){
            et=new Account_bank_statement_line();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_bank_statement_line> searchDefault(Account_bank_statement_lineSearchContext context) {
        Page<Account_bank_statement_line> account_bank_statement_lines=account_bank_statement_lineFeignClient.searchDefault(context);
        return account_bank_statement_lines;
    }


}


