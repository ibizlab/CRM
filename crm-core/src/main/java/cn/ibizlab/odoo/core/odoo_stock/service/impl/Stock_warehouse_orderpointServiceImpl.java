package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warehouse_orderpoint;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warehouse_orderpointSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_warehouse_orderpointService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_warehouse_orderpointFeignClient;

/**
 * 实体[最小库存规则] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_warehouse_orderpointServiceImpl implements IStock_warehouse_orderpointService {

    @Autowired
    stock_warehouse_orderpointFeignClient stock_warehouse_orderpointFeignClient;


    @Override
    public boolean update(Stock_warehouse_orderpoint et) {
        Stock_warehouse_orderpoint rt = stock_warehouse_orderpointFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_warehouse_orderpoint> list){
        stock_warehouse_orderpointFeignClient.updateBatch(list) ;
    }

    @Override
    public Stock_warehouse_orderpoint get(Integer id) {
		Stock_warehouse_orderpoint et=stock_warehouse_orderpointFeignClient.get(id);
        if(et==null){
            et=new Stock_warehouse_orderpoint();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Stock_warehouse_orderpoint getDraft(Stock_warehouse_orderpoint et) {
        et=stock_warehouse_orderpointFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=stock_warehouse_orderpointFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_warehouse_orderpointFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Stock_warehouse_orderpoint et) {
        Stock_warehouse_orderpoint rt = stock_warehouse_orderpointFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_warehouse_orderpoint> list){
        stock_warehouse_orderpointFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_warehouse_orderpoint> searchDefault(Stock_warehouse_orderpointSearchContext context) {
        Page<Stock_warehouse_orderpoint> stock_warehouse_orderpoints=stock_warehouse_orderpointFeignClient.searchDefault(context);
        return stock_warehouse_orderpoints;
    }


}


