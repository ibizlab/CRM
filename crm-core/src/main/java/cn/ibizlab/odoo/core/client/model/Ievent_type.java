package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [event_type] 对象
 */
public interface Ievent_type {

    /**
     * 获取 [自动确认注册]
     */
    public void setAuto_confirm(String auto_confirm);
    
    /**
     * 设置 [自动确认注册]
     */
    public String getAuto_confirm();

    /**
     * 获取 [自动确认注册]脏标记
     */
    public boolean getAuto_confirmDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [Twitter主题标签]
     */
    public void setDefault_hashtag(String default_hashtag);
    
    /**
     * 设置 [Twitter主题标签]
     */
    public String getDefault_hashtag();

    /**
     * 获取 [Twitter主题标签]脏标记
     */
    public boolean getDefault_hashtagDirtyFlag();
    /**
     * 获取 [最大注册数]
     */
    public void setDefault_registration_max(Integer default_registration_max);
    
    /**
     * 设置 [最大注册数]
     */
    public Integer getDefault_registration_max();

    /**
     * 获取 [最大注册数]脏标记
     */
    public boolean getDefault_registration_maxDirtyFlag();
    /**
     * 获取 [最小注册数]
     */
    public void setDefault_registration_min(Integer default_registration_min);
    
    /**
     * 设置 [最小注册数]
     */
    public Integer getDefault_registration_min();

    /**
     * 获取 [最小注册数]脏标记
     */
    public boolean getDefault_registration_minDirtyFlag();
    /**
     * 获取 [时区]
     */
    public void setDefault_timezone(String default_timezone);
    
    /**
     * 设置 [时区]
     */
    public String getDefault_timezone();

    /**
     * 获取 [时区]脏标记
     */
    public boolean getDefault_timezoneDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [入场券]
     */
    public void setEvent_ticket_ids(String event_ticket_ids);
    
    /**
     * 设置 [入场券]
     */
    public String getEvent_ticket_ids();

    /**
     * 获取 [入场券]脏标记
     */
    public boolean getEvent_ticket_idsDirtyFlag();
    /**
     * 获取 [邮件排程]
     */
    public void setEvent_type_mail_ids(String event_type_mail_ids);
    
    /**
     * 设置 [邮件排程]
     */
    public String getEvent_type_mail_ids();

    /**
     * 获取 [邮件排程]脏标记
     */
    public boolean getEvent_type_mail_idsDirtyFlag();
    /**
     * 获取 [有限名额]
     */
    public void setHas_seats_limitation(String has_seats_limitation);
    
    /**
     * 设置 [有限名额]
     */
    public String getHas_seats_limitation();

    /**
     * 获取 [有限名额]脏标记
     */
    public boolean getHas_seats_limitationDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [在线活动]
     */
    public void setIs_online(String is_online);
    
    /**
     * 设置 [在线活动]
     */
    public String getIs_online();

    /**
     * 获取 [在线活动]脏标记
     */
    public boolean getIs_onlineDirtyFlag();
    /**
     * 获取 [活动类别]
     */
    public void setName(String name);
    
    /**
     * 设置 [活动类别]
     */
    public String getName();

    /**
     * 获取 [活动类别]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [使用默认主题标签]
     */
    public void setUse_hashtag(String use_hashtag);
    
    /**
     * 设置 [使用默认主题标签]
     */
    public String getUse_hashtag();

    /**
     * 获取 [使用默认主题标签]脏标记
     */
    public boolean getUse_hashtagDirtyFlag();
    /**
     * 获取 [自动发送EMail]
     */
    public void setUse_mail_schedule(String use_mail_schedule);
    
    /**
     * 设置 [自动发送EMail]
     */
    public String getUse_mail_schedule();

    /**
     * 获取 [自动发送EMail]脏标记
     */
    public boolean getUse_mail_scheduleDirtyFlag();
    /**
     * 获取 [出票]
     */
    public void setUse_ticketing(String use_ticketing);
    
    /**
     * 设置 [出票]
     */
    public String getUse_ticketing();

    /**
     * 获取 [出票]脏标记
     */
    public boolean getUse_ticketingDirtyFlag();
    /**
     * 获取 [使用默认时区]
     */
    public void setUse_timezone(String use_timezone);
    
    /**
     * 设置 [使用默认时区]
     */
    public String getUse_timezone();

    /**
     * 获取 [使用默认时区]脏标记
     */
    public boolean getUse_timezoneDirtyFlag();
    /**
     * 获取 [在网站上显示专用菜单]
     */
    public void setWebsite_menu(String website_menu);
    
    /**
     * 设置 [在网站上显示专用菜单]
     */
    public String getWebsite_menu();

    /**
     * 获取 [在网站上显示专用菜单]脏标记
     */
    public boolean getWebsite_menuDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
