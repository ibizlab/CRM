package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [product_supplierinfo] 对象
 */
public interface Iproduct_supplierinfo {

    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id_text(String currency_id_text);
    
    /**
     * 设置 [币种]
     */
    public String getCurrency_id_text();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_id_textDirtyFlag();
    /**
     * 获取 [结束日期]
     */
    public void setDate_end(Timestamp date_end);
    
    /**
     * 设置 [结束日期]
     */
    public Timestamp getDate_end();

    /**
     * 获取 [结束日期]脏标记
     */
    public boolean getDate_endDirtyFlag();
    /**
     * 获取 [开始日期]
     */
    public void setDate_start(Timestamp date_start);
    
    /**
     * 设置 [开始日期]
     */
    public Timestamp getDate_start();

    /**
     * 获取 [开始日期]脏标记
     */
    public boolean getDate_startDirtyFlag();
    /**
     * 获取 [交货提前时间]
     */
    public void setDelay(Integer delay);
    
    /**
     * 设置 [交货提前时间]
     */
    public Integer getDelay();

    /**
     * 获取 [交货提前时间]脏标记
     */
    public boolean getDelayDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [最少数量]
     */
    public void setMin_qty(Double min_qty);
    
    /**
     * 设置 [最少数量]
     */
    public Double getMin_qty();

    /**
     * 获取 [最少数量]脏标记
     */
    public boolean getMin_qtyDirtyFlag();
    /**
     * 获取 [供应商]
     */
    public void setName(Integer name);
    
    /**
     * 设置 [供应商]
     */
    public Integer getName();

    /**
     * 获取 [供应商]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [供应商]
     */
    public void setName_text(String name_text);
    
    /**
     * 设置 [供应商]
     */
    public String getName_text();

    /**
     * 获取 [供应商]脏标记
     */
    public boolean getName_textDirtyFlag();
    /**
     * 获取 [价格]
     */
    public void setPrice(Double price);
    
    /**
     * 设置 [价格]
     */
    public Double getPrice();

    /**
     * 获取 [价格]脏标记
     */
    public boolean getPriceDirtyFlag();
    /**
     * 获取 [供应商产品代码]
     */
    public void setProduct_code(String product_code);
    
    /**
     * 设置 [供应商产品代码]
     */
    public String getProduct_code();

    /**
     * 获取 [供应商产品代码]脏标记
     */
    public boolean getProduct_codeDirtyFlag();
    /**
     * 获取 [产品变体]
     */
    public void setProduct_id(Integer product_id);
    
    /**
     * 设置 [产品变体]
     */
    public Integer getProduct_id();

    /**
     * 获取 [产品变体]脏标记
     */
    public boolean getProduct_idDirtyFlag();
    /**
     * 获取 [产品变体]
     */
    public void setProduct_id_text(String product_id_text);
    
    /**
     * 设置 [产品变体]
     */
    public String getProduct_id_text();

    /**
     * 获取 [产品变体]脏标记
     */
    public boolean getProduct_id_textDirtyFlag();
    /**
     * 获取 [供应商产品名称]
     */
    public void setProduct_name(String product_name);
    
    /**
     * 设置 [供应商产品名称]
     */
    public String getProduct_name();

    /**
     * 获取 [供应商产品名称]脏标记
     */
    public boolean getProduct_nameDirtyFlag();
    /**
     * 获取 [产品模板]
     */
    public void setProduct_tmpl_id(Integer product_tmpl_id);
    
    /**
     * 设置 [产品模板]
     */
    public Integer getProduct_tmpl_id();

    /**
     * 获取 [产品模板]脏标记
     */
    public boolean getProduct_tmpl_idDirtyFlag();
    /**
     * 获取 [产品模板]
     */
    public void setProduct_tmpl_id_text(String product_tmpl_id_text);
    
    /**
     * 设置 [产品模板]
     */
    public String getProduct_tmpl_id_text();

    /**
     * 获取 [产品模板]脏标记
     */
    public boolean getProduct_tmpl_id_textDirtyFlag();
    /**
     * 获取 [计量单位]
     */
    public void setProduct_uom(Integer product_uom);
    
    /**
     * 设置 [计量单位]
     */
    public Integer getProduct_uom();

    /**
     * 获取 [计量单位]脏标记
     */
    public boolean getProduct_uomDirtyFlag();
    /**
     * 获取 [变体计数]
     */
    public void setProduct_variant_count(Integer product_variant_count);
    
    /**
     * 设置 [变体计数]
     */
    public Integer getProduct_variant_count();

    /**
     * 获取 [变体计数]脏标记
     */
    public boolean getProduct_variant_countDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
