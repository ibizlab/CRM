package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_chart_template;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_chart_template] 服务对象接口
 */
public interface Iaccount_chart_templateClientService{

    public Iaccount_chart_template createModel() ;

    public void updateBatch(List<Iaccount_chart_template> account_chart_templates);

    public Page<Iaccount_chart_template> fetchDefault(SearchContext context);

    public void create(Iaccount_chart_template account_chart_template);

    public void get(Iaccount_chart_template account_chart_template);

    public void removeBatch(List<Iaccount_chart_template> account_chart_templates);

    public void remove(Iaccount_chart_template account_chart_template);

    public void update(Iaccount_chart_template account_chart_template);

    public void createBatch(List<Iaccount_chart_template> account_chart_templates);

    public Page<Iaccount_chart_template> select(SearchContext context);

    public void getDraft(Iaccount_chart_template account_chart_template);

}
