package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Icrm_lead_lost;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[crm_lead_lost] 服务对象接口
 */
public interface Icrm_lead_lostClientService{

    public Icrm_lead_lost createModel() ;

    public void removeBatch(List<Icrm_lead_lost> crm_lead_losts);

    public void updateBatch(List<Icrm_lead_lost> crm_lead_losts);

    public void get(Icrm_lead_lost crm_lead_lost);

    public Page<Icrm_lead_lost> fetchDefault(SearchContext context);

    public void remove(Icrm_lead_lost crm_lead_lost);

    public void update(Icrm_lead_lost crm_lead_lost);

    public void createBatch(List<Icrm_lead_lost> crm_lead_losts);

    public void create(Icrm_lead_lost crm_lead_lost);

    public Page<Icrm_lead_lost> select(SearchContext context);

    public void getDraft(Icrm_lead_lost crm_lead_lost);

}
