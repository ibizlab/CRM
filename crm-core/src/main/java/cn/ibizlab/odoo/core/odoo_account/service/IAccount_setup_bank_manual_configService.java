package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_setup_bank_manual_config;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_setup_bank_manual_configSearchContext;


/**
 * 实体[Account_setup_bank_manual_config] 服务对象接口
 */
public interface IAccount_setup_bank_manual_configService{

    boolean create(Account_setup_bank_manual_config et) ;
    void createBatch(List<Account_setup_bank_manual_config> list) ;
    boolean update(Account_setup_bank_manual_config et) ;
    void updateBatch(List<Account_setup_bank_manual_config> list) ;
    Account_setup_bank_manual_config getDraft(Account_setup_bank_manual_config et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Account_setup_bank_manual_config get(Integer key) ;
    Page<Account_setup_bank_manual_config> searchDefault(Account_setup_bank_manual_configSearchContext context) ;

}



