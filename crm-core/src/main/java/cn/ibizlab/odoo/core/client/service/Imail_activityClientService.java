package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_activity;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_activity] 服务对象接口
 */
public interface Imail_activityClientService{

    public Imail_activity createModel() ;

    public void removeBatch(List<Imail_activity> mail_activities);

    public void get(Imail_activity mail_activity);

    public void remove(Imail_activity mail_activity);

    public void createBatch(List<Imail_activity> mail_activities);

    public void update(Imail_activity mail_activity);

    public void create(Imail_activity mail_activity);

    public void updateBatch(List<Imail_activity> mail_activities);

    public Page<Imail_activity> fetchDefault(SearchContext context);

    public Page<Imail_activity> select(SearchContext context);

    public void getDraft(Imail_activity mail_activity);

    public void checkKey(Imail_activity mail_activity);

    public void save(Imail_activity mail_activity);

}
