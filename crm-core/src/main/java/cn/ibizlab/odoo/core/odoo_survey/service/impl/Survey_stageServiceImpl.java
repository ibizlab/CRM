package cn.ibizlab.odoo.core.odoo_survey.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_stage;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_stageSearchContext;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_stageService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_survey.client.survey_stageFeignClient;

/**
 * 实体[调查阶段] 服务对象接口实现
 */
@Slf4j
@Service
public class Survey_stageServiceImpl implements ISurvey_stageService {

    @Autowired
    survey_stageFeignClient survey_stageFeignClient;


    @Override
    public Survey_stage getDraft(Survey_stage et) {
        et=survey_stageFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Survey_stage et) {
        Survey_stage rt = survey_stageFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Survey_stage> list){
        survey_stageFeignClient.updateBatch(list) ;
    }

    @Override
    public Survey_stage get(Integer id) {
		Survey_stage et=survey_stageFeignClient.get(id);
        if(et==null){
            et=new Survey_stage();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Survey_stage et) {
        Survey_stage rt = survey_stageFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Survey_stage> list){
        survey_stageFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=survey_stageFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        survey_stageFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Survey_stage> searchDefault(Survey_stageSearchContext context) {
        Page<Survey_stage> survey_stages=survey_stageFeignClient.searchDefault(context);
        return survey_stages;
    }


}


