package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ires_users;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_users] 服务对象接口
 */
public interface Ires_usersClientService{

    public Ires_users createModel() ;

    public void get(Ires_users res_users);

    public void removeBatch(List<Ires_users> res_users);

    public void remove(Ires_users res_users);

    public void create(Ires_users res_users);

    public Page<Ires_users> fetchDefault(SearchContext context);

    public void updateBatch(List<Ires_users> res_users);

    public void createBatch(List<Ires_users> res_users);

    public void update(Ires_users res_users);

    public Page<Ires_users> select(SearchContext context);

    public void save(Ires_users res_users);

    public void checkKey(Ires_users res_users);

    public void getDraft(Ires_users res_users);

}
