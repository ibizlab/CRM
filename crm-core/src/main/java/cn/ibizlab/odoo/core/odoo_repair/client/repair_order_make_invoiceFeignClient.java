package cn.ibizlab.odoo.core.odoo_repair.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_order_make_invoice;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_order_make_invoiceSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[repair_order_make_invoice] 服务对象接口
 */
@FeignClient(value = "odoo-repair", contextId = "repair-order-make-invoice", fallback = repair_order_make_invoiceFallback.class)
public interface repair_order_make_invoiceFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/repair_order_make_invoices/{id}")
    Repair_order_make_invoice update(@PathVariable("id") Integer id,@RequestBody Repair_order_make_invoice repair_order_make_invoice);

    @RequestMapping(method = RequestMethod.PUT, value = "/repair_order_make_invoices/batch")
    Boolean updateBatch(@RequestBody List<Repair_order_make_invoice> repair_order_make_invoices);


    @RequestMapping(method = RequestMethod.POST, value = "/repair_order_make_invoices")
    Repair_order_make_invoice create(@RequestBody Repair_order_make_invoice repair_order_make_invoice);

    @RequestMapping(method = RequestMethod.POST, value = "/repair_order_make_invoices/batch")
    Boolean createBatch(@RequestBody List<Repair_order_make_invoice> repair_order_make_invoices);


    @RequestMapping(method = RequestMethod.GET, value = "/repair_order_make_invoices/{id}")
    Repair_order_make_invoice get(@PathVariable("id") Integer id);






    @RequestMapping(method = RequestMethod.POST, value = "/repair_order_make_invoices/searchdefault")
    Page<Repair_order_make_invoice> searchDefault(@RequestBody Repair_order_make_invoiceSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/repair_order_make_invoices/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/repair_order_make_invoices/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/repair_order_make_invoices/select")
    Page<Repair_order_make_invoice> select();


    @RequestMapping(method = RequestMethod.GET, value = "/repair_order_make_invoices/getdraft")
    Repair_order_make_invoice getDraft();


}
