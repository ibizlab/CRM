package cn.ibizlab.odoo.core.odoo_portal.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_wizard_user;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_wizard_userSearchContext;


/**
 * 实体[Portal_wizard_user] 服务对象接口
 */
public interface IPortal_wizard_userService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Portal_wizard_user getDraft(Portal_wizard_user et) ;
    boolean create(Portal_wizard_user et) ;
    void createBatch(List<Portal_wizard_user> list) ;
    Portal_wizard_user get(Integer key) ;
    boolean update(Portal_wizard_user et) ;
    void updateBatch(List<Portal_wizard_user> list) ;
    Page<Portal_wizard_user> searchDefault(Portal_wizard_userSearchContext context) ;

}



