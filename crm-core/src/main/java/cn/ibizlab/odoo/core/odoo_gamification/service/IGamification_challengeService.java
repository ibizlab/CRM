package cn.ibizlab.odoo.core.odoo_gamification.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_challenge;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_challengeSearchContext;


/**
 * 实体[Gamification_challenge] 服务对象接口
 */
public interface IGamification_challengeService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Gamification_challenge get(Integer key) ;
    boolean update(Gamification_challenge et) ;
    void updateBatch(List<Gamification_challenge> list) ;
    boolean create(Gamification_challenge et) ;
    void createBatch(List<Gamification_challenge> list) ;
    Gamification_challenge getDraft(Gamification_challenge et) ;
    Page<Gamification_challenge> searchDefault(Gamification_challengeSearchContext context) ;

}



