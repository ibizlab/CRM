package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_message_subtype;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_message_subtypeSearchContext;


/**
 * 实体[Mail_message_subtype] 服务对象接口
 */
public interface IMail_message_subtypeService{

    Mail_message_subtype get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Mail_message_subtype et) ;
    void createBatch(List<Mail_message_subtype> list) ;
    boolean update(Mail_message_subtype et) ;
    void updateBatch(List<Mail_message_subtype> list) ;
    Mail_message_subtype getDraft(Mail_message_subtype et) ;
    Page<Mail_message_subtype> searchDefault(Mail_message_subtypeSearchContext context) ;

}



