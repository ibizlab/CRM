package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warehouse;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warehouseSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_warehouse] 服务对象接口
 */
@Component
public class stock_warehouseFallback implements stock_warehouseFeignClient{


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Stock_warehouse> searchDefault(Stock_warehouseSearchContext context){
            return null;
     }


    public Stock_warehouse update(Integer id, Stock_warehouse stock_warehouse){
            return null;
     }
    public Boolean updateBatch(List<Stock_warehouse> stock_warehouses){
            return false;
     }


    public Stock_warehouse create(Stock_warehouse stock_warehouse){
            return null;
     }
    public Boolean createBatch(List<Stock_warehouse> stock_warehouses){
            return false;
     }



    public Stock_warehouse get(Integer id){
            return null;
     }


    public Page<Stock_warehouse> select(){
            return null;
     }

    public Stock_warehouse getDraft(){
            return null;
    }



}
