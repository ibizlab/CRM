package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_immediate_transfer;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_immediate_transferSearchContext;


/**
 * 实体[Stock_immediate_transfer] 服务对象接口
 */
public interface IStock_immediate_transferService{

    boolean update(Stock_immediate_transfer et) ;
    void updateBatch(List<Stock_immediate_transfer> list) ;
    Stock_immediate_transfer get(Integer key) ;
    Stock_immediate_transfer getDraft(Stock_immediate_transfer et) ;
    boolean create(Stock_immediate_transfer et) ;
    void createBatch(List<Stock_immediate_transfer> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Stock_immediate_transfer> searchDefault(Stock_immediate_transferSearchContext context) ;

}



