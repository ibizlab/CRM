package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_common_journal_report;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_common_journal_reportSearchContext;


/**
 * 实体[Account_common_journal_report] 服务对象接口
 */
public interface IAccount_common_journal_reportService{

    Account_common_journal_report getDraft(Account_common_journal_report et) ;
    Account_common_journal_report get(Integer key) ;
    boolean update(Account_common_journal_report et) ;
    void updateBatch(List<Account_common_journal_report> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Account_common_journal_report et) ;
    void createBatch(List<Account_common_journal_report> list) ;
    Page<Account_common_journal_report> searchDefault(Account_common_journal_reportSearchContext context) ;

}



