package cn.ibizlab.odoo.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_holidays_summary_dept;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_holidays_summary_deptSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[hr_holidays_summary_dept] 服务对象接口
 */
@FeignClient(value = "odoo-hr", contextId = "hr-holidays-summary-dept", fallback = hr_holidays_summary_deptFallback.class)
public interface hr_holidays_summary_deptFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_holidays_summary_depts/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_holidays_summary_depts/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_holidays_summary_depts/searchdefault")
    Page<Hr_holidays_summary_dept> searchDefault(@RequestBody Hr_holidays_summary_deptSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/hr_holidays_summary_depts/{id}")
    Hr_holidays_summary_dept get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_holidays_summary_depts")
    Hr_holidays_summary_dept create(@RequestBody Hr_holidays_summary_dept hr_holidays_summary_dept);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_holidays_summary_depts/batch")
    Boolean createBatch(@RequestBody List<Hr_holidays_summary_dept> hr_holidays_summary_depts);



    @RequestMapping(method = RequestMethod.PUT, value = "/hr_holidays_summary_depts/{id}")
    Hr_holidays_summary_dept update(@PathVariable("id") Integer id,@RequestBody Hr_holidays_summary_dept hr_holidays_summary_dept);

    @RequestMapping(method = RequestMethod.PUT, value = "/hr_holidays_summary_depts/batch")
    Boolean updateBatch(@RequestBody List<Hr_holidays_summary_dept> hr_holidays_summary_depts);



    @RequestMapping(method = RequestMethod.GET, value = "/hr_holidays_summary_depts/select")
    Page<Hr_holidays_summary_dept> select();


    @RequestMapping(method = RequestMethod.GET, value = "/hr_holidays_summary_depts/getdraft")
    Hr_holidays_summary_dept getDraft();


}
