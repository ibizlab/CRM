package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position_account;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_position_accountSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_fiscal_position_account] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-fiscal-position-account", fallback = account_fiscal_position_accountFallback.class)
public interface account_fiscal_position_accountFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_accounts/searchdefault")
    Page<Account_fiscal_position_account> searchDefault(@RequestBody Account_fiscal_position_accountSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_accounts")
    Account_fiscal_position_account create(@RequestBody Account_fiscal_position_account account_fiscal_position_account);

    @RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_accounts/batch")
    Boolean createBatch(@RequestBody List<Account_fiscal_position_account> account_fiscal_position_accounts);



    @RequestMapping(method = RequestMethod.PUT, value = "/account_fiscal_position_accounts/{id}")
    Account_fiscal_position_account update(@PathVariable("id") Integer id,@RequestBody Account_fiscal_position_account account_fiscal_position_account);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_fiscal_position_accounts/batch")
    Boolean updateBatch(@RequestBody List<Account_fiscal_position_account> account_fiscal_position_accounts);


    @RequestMapping(method = RequestMethod.GET, value = "/account_fiscal_position_accounts/{id}")
    Account_fiscal_position_account get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_fiscal_position_accounts/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_fiscal_position_accounts/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/account_fiscal_position_accounts/select")
    Page<Account_fiscal_position_account> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_fiscal_position_accounts/getdraft")
    Account_fiscal_position_account getDraft();


}
