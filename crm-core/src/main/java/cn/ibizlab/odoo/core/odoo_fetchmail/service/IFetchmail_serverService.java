package cn.ibizlab.odoo.core.odoo_fetchmail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_fetchmail.domain.Fetchmail_server;
import cn.ibizlab.odoo.core.odoo_fetchmail.filter.Fetchmail_serverSearchContext;


/**
 * 实体[Fetchmail_server] 服务对象接口
 */
public interface IFetchmail_serverService{

    Fetchmail_server get(Integer key) ;
    boolean update(Fetchmail_server et) ;
    void updateBatch(List<Fetchmail_server> list) ;
    boolean create(Fetchmail_server et) ;
    void createBatch(List<Fetchmail_server> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Fetchmail_server getDraft(Fetchmail_server et) ;
    Page<Fetchmail_server> searchDefault(Fetchmail_serverSearchContext context) ;

}



