package cn.ibizlab.odoo.core.r7rt_dyna.valuerule.anno.dynachart;

import cn.ibizlab.odoo.core.r7rt_dyna.valuerule.validator.dynachart.DynaChartCreateManDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：DynaChart
 * 属性：CreateMan
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {DynaChartCreateManDefaultValidator.class})
public @interface DynaChartCreateManDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[60]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
