package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [mro_pm_meter] 对象
 */
public interface Imro_pm_meter {

    /**
     * 获取 [Asset]
     */
    public void setAsset_id(Integer asset_id);
    
    /**
     * 设置 [Asset]
     */
    public Integer getAsset_id();

    /**
     * 获取 [Asset]脏标记
     */
    public boolean getAsset_idDirtyFlag();
    /**
     * 获取 [Asset]
     */
    public void setAsset_id_text(String asset_id_text);
    
    /**
     * 设置 [Asset]
     */
    public String getAsset_id_text();

    /**
     * 获取 [Asset]脏标记
     */
    public boolean getAsset_id_textDirtyFlag();
    /**
     * 获取 [Averaging time (days)]
     */
    public void setAv_time(Double av_time);
    
    /**
     * 设置 [Averaging time (days)]
     */
    public Double getAv_time();

    /**
     * 获取 [Averaging time (days)]脏标记
     */
    public boolean getAv_timeDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [日期]
     */
    public void setDate(Timestamp date);
    
    /**
     * 设置 [日期]
     */
    public Timestamp getDate();

    /**
     * 获取 [日期]脏标记
     */
    public boolean getDateDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [Meters]
     */
    public void setMeter_line_ids(String meter_line_ids);
    
    /**
     * 设置 [Meters]
     */
    public String getMeter_line_ids();

    /**
     * 获取 [Meters]脏标记
     */
    public boolean getMeter_line_idsDirtyFlag();
    /**
     * 获取 [单位]
     */
    public void setMeter_uom(Integer meter_uom);
    
    /**
     * 设置 [单位]
     */
    public Integer getMeter_uom();

    /**
     * 获取 [单位]脏标记
     */
    public boolean getMeter_uomDirtyFlag();
    /**
     * 获取 [Min Utilization (per day)]
     */
    public void setMin_utilization(Double min_utilization);
    
    /**
     * 设置 [Min Utilization (per day)]
     */
    public Double getMin_utilization();

    /**
     * 获取 [Min Utilization (per day)]脏标记
     */
    public boolean getMin_utilizationDirtyFlag();
    /**
     * 获取 [Meter]
     */
    public void setName(Integer name);
    
    /**
     * 设置 [Meter]
     */
    public Integer getName();

    /**
     * 获取 [Meter]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [Meter]
     */
    public void setName_text(String name_text);
    
    /**
     * 设置 [Meter]
     */
    public String getName_text();

    /**
     * 获取 [Meter]脏标记
     */
    public boolean getName_textDirtyFlag();
    /**
     * 获取 [New value]
     */
    public void setNew_value(Double new_value);
    
    /**
     * 设置 [New value]
     */
    public Double getNew_value();

    /**
     * 获取 [New value]脏标记
     */
    public boolean getNew_valueDirtyFlag();
    /**
     * 获取 [Source Meter]
     */
    public void setParent_meter_id(Integer parent_meter_id);
    
    /**
     * 设置 [Source Meter]
     */
    public Integer getParent_meter_id();

    /**
     * 获取 [Source Meter]脏标记
     */
    public boolean getParent_meter_idDirtyFlag();
    /**
     * 获取 [Source Meter]
     */
    public void setParent_meter_id_text(String parent_meter_id_text);
    
    /**
     * 设置 [Source Meter]
     */
    public String getParent_meter_id_text();

    /**
     * 获取 [Source Meter]脏标记
     */
    public boolean getParent_meter_id_textDirtyFlag();
    /**
     * 获取 [Ratio to Source]
     */
    public void setParent_ratio_id(Integer parent_ratio_id);
    
    /**
     * 设置 [Ratio to Source]
     */
    public Integer getParent_ratio_id();

    /**
     * 获取 [Ratio to Source]脏标记
     */
    public boolean getParent_ratio_idDirtyFlag();
    /**
     * 获取 [Ratio to Source]
     */
    public void setParent_ratio_id_text(String parent_ratio_id_text);
    
    /**
     * 设置 [Ratio to Source]
     */
    public String getParent_ratio_id_text();

    /**
     * 获取 [Ratio to Source]脏标记
     */
    public boolean getParent_ratio_id_textDirtyFlag();
    /**
     * 获取 [Reading Type]
     */
    public void setReading_type(String reading_type);
    
    /**
     * 设置 [Reading Type]
     */
    public String getReading_type();

    /**
     * 获取 [Reading Type]脏标记
     */
    public boolean getReading_typeDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [Total Value]
     */
    public void setTotal_value(Double total_value);
    
    /**
     * 设置 [Total Value]
     */
    public Double getTotal_value();

    /**
     * 获取 [Total Value]脏标记
     */
    public boolean getTotal_valueDirtyFlag();
    /**
     * 获取 [Utilization (per day)]
     */
    public void setUtilization(Double utilization);
    
    /**
     * 设置 [Utilization (per day)]
     */
    public Double getUtilization();

    /**
     * 获取 [Utilization (per day)]脏标记
     */
    public boolean getUtilizationDirtyFlag();
    /**
     * 获取 [值]
     */
    public void setValue(Double value);
    
    /**
     * 设置 [值]
     */
    public Double getValue();

    /**
     * 获取 [值]脏标记
     */
    public boolean getValueDirtyFlag();
    /**
     * 获取 [View Line]
     */
    public void setView_line_ids(String view_line_ids);
    
    /**
     * 设置 [View Line]
     */
    public String getView_line_ids();

    /**
     * 获取 [View Line]脏标记
     */
    public boolean getView_line_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
