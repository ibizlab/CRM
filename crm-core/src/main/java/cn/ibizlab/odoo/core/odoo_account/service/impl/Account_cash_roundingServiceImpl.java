package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_cash_rounding;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_cash_roundingSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_cash_roundingService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_cash_roundingFeignClient;

/**
 * 实体[帐户现金舍入] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_cash_roundingServiceImpl implements IAccount_cash_roundingService {

    @Autowired
    account_cash_roundingFeignClient account_cash_roundingFeignClient;


    @Override
    public Account_cash_rounding getDraft(Account_cash_rounding et) {
        et=account_cash_roundingFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Account_cash_rounding et) {
        Account_cash_rounding rt = account_cash_roundingFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_cash_rounding> list){
        account_cash_roundingFeignClient.createBatch(list) ;
    }

    @Override
    public Account_cash_rounding get(Integer id) {
		Account_cash_rounding et=account_cash_roundingFeignClient.get(id);
        if(et==null){
            et=new Account_cash_rounding();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_cash_roundingFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_cash_roundingFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Account_cash_rounding et) {
        Account_cash_rounding rt = account_cash_roundingFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_cash_rounding> list){
        account_cash_roundingFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_cash_rounding> searchDefault(Account_cash_roundingSearchContext context) {
        Page<Account_cash_rounding> account_cash_roundings=account_cash_roundingFeignClient.searchDefault(context);
        return account_cash_roundings;
    }


}


