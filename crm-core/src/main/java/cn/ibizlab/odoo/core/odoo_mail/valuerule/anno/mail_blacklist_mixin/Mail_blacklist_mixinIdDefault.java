package cn.ibizlab.odoo.core.odoo_mail.valuerule.anno.mail_blacklist_mixin;

import cn.ibizlab.odoo.core.odoo_mail.valuerule.validator.mail_blacklist_mixin.Mail_blacklist_mixinIdDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Mail_blacklist_mixin
 * 属性：Id
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Mail_blacklist_mixinIdDefaultValidator.class})
public @interface Mail_blacklist_mixinIdDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
