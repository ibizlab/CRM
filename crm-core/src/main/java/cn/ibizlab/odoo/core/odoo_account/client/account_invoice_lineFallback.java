package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_lineSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_invoice_line] 服务对象接口
 */
@Component
public class account_invoice_lineFallback implements account_invoice_lineFeignClient{

    public Account_invoice_line get(Integer id){
            return null;
     }


    public Account_invoice_line create(Account_invoice_line account_invoice_line){
            return null;
     }
    public Boolean createBatch(List<Account_invoice_line> account_invoice_lines){
            return false;
     }

    public Page<Account_invoice_line> searchDefault(Account_invoice_lineSearchContext context){
            return null;
     }



    public Account_invoice_line update(Integer id, Account_invoice_line account_invoice_line){
            return null;
     }
    public Boolean updateBatch(List<Account_invoice_line> account_invoice_lines){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }



    public Page<Account_invoice_line> select(){
            return null;
     }

    public Boolean checkKey(Account_invoice_line account_invoice_line){
            return false;
     }


    public Account_invoice_line getDraft(){
            return null;
    }



    public Boolean save(Account_invoice_line account_invoice_line){
            return false;
     }
    public Boolean saveBatch(List<Account_invoice_line> account_invoice_lines){
            return false;
     }

}
