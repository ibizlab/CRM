package cn.ibizlab.odoo.core.odoo_base_import.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_o2m_child;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_o2m_childSearchContext;


/**
 * 实体[Base_import_tests_models_o2m_child] 服务对象接口
 */
public interface IBase_import_tests_models_o2m_childService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Base_import_tests_models_o2m_child et) ;
    void createBatch(List<Base_import_tests_models_o2m_child> list) ;
    Base_import_tests_models_o2m_child get(Integer key) ;
    boolean update(Base_import_tests_models_o2m_child et) ;
    void updateBatch(List<Base_import_tests_models_o2m_child> list) ;
    Base_import_tests_models_o2m_child getDraft(Base_import_tests_models_o2m_child et) ;
    Page<Base_import_tests_models_o2m_child> searchDefault(Base_import_tests_models_o2m_childSearchContext context) ;

}



