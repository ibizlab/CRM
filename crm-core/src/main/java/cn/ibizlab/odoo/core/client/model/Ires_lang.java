package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [res_lang] 对象
 */
public interface Ires_lang {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [地区代码]
     */
    public void setCode(String code);
    
    /**
     * 设置 [地区代码]
     */
    public String getCode();

    /**
     * 获取 [地区代码]脏标记
     */
    public boolean getCodeDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [日期格式]
     */
    public void setDate_format(String date_format);
    
    /**
     * 设置 [日期格式]
     */
    public String getDate_format();

    /**
     * 获取 [日期格式]脏标记
     */
    public boolean getDate_formatDirtyFlag();
    /**
     * 获取 [小数分割符]
     */
    public void setDecimal_point(String decimal_point);
    
    /**
     * 设置 [小数分割符]
     */
    public String getDecimal_point();

    /**
     * 获取 [小数分割符]脏标记
     */
    public boolean getDecimal_pointDirtyFlag();
    /**
     * 获取 [方向]
     */
    public void setDirection(String direction);
    
    /**
     * 设置 [方向]
     */
    public String getDirection();

    /**
     * 获取 [方向]脏标记
     */
    public boolean getDirectionDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [分割符格式]
     */
    public void setGrouping(String grouping);
    
    /**
     * 设置 [分割符格式]
     */
    public String getGrouping();

    /**
     * 获取 [分割符格式]脏标记
     */
    public boolean getGroupingDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [ISO代码]
     */
    public void setIso_code(String iso_code);
    
    /**
     * 设置 [ISO代码]
     */
    public String getIso_code();

    /**
     * 获取 [ISO代码]脏标记
     */
    public boolean getIso_codeDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [千位分隔符]
     */
    public void setThousands_sep(String thousands_sep);
    
    /**
     * 设置 [千位分隔符]
     */
    public String getThousands_sep();

    /**
     * 获取 [千位分隔符]脏标记
     */
    public boolean getThousands_sepDirtyFlag();
    /**
     * 获取 [时间格式]
     */
    public void setTime_format(String time_format);
    
    /**
     * 设置 [时间格式]
     */
    public String getTime_format();

    /**
     * 获取 [时间格式]脏标记
     */
    public boolean getTime_formatDirtyFlag();
    /**
     * 获取 [可翻译]
     */
    public void setTranslatable(String translatable);
    
    /**
     * 设置 [可翻译]
     */
    public String getTranslatable();

    /**
     * 获取 [可翻译]脏标记
     */
    public boolean getTranslatableDirtyFlag();
    /**
     * 获取 [一个星期的第一天是]
     */
    public void setWeek_start(String week_start);
    
    /**
     * 设置 [一个星期的第一天是]
     */
    public String getWeek_start();

    /**
     * 获取 [一个星期的第一天是]脏标记
     */
    public boolean getWeek_startDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
