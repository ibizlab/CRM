package cn.ibizlab.odoo.core.odoo_fleet.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_tag;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_tagSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[fleet_vehicle_tag] 服务对象接口
 */
@Component
public class fleet_vehicle_tagFallback implements fleet_vehicle_tagFeignClient{



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Fleet_vehicle_tag> searchDefault(Fleet_vehicle_tagSearchContext context){
            return null;
     }


    public Fleet_vehicle_tag create(Fleet_vehicle_tag fleet_vehicle_tag){
            return null;
     }
    public Boolean createBatch(List<Fleet_vehicle_tag> fleet_vehicle_tags){
            return false;
     }


    public Fleet_vehicle_tag update(Integer id, Fleet_vehicle_tag fleet_vehicle_tag){
            return null;
     }
    public Boolean updateBatch(List<Fleet_vehicle_tag> fleet_vehicle_tags){
            return false;
     }


    public Fleet_vehicle_tag get(Integer id){
            return null;
     }


    public Page<Fleet_vehicle_tag> select(){
            return null;
     }

    public Fleet_vehicle_tag getDraft(){
            return null;
    }



}
