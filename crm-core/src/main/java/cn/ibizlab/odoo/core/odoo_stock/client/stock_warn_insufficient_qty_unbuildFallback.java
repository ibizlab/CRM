package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty_unbuild;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warn_insufficient_qty_unbuildSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_warn_insufficient_qty_unbuild] 服务对象接口
 */
@Component
public class stock_warn_insufficient_qty_unbuildFallback implements stock_warn_insufficient_qty_unbuildFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Stock_warn_insufficient_qty_unbuild get(Integer id){
            return null;
     }



    public Stock_warn_insufficient_qty_unbuild create(Stock_warn_insufficient_qty_unbuild stock_warn_insufficient_qty_unbuild){
            return null;
     }
    public Boolean createBatch(List<Stock_warn_insufficient_qty_unbuild> stock_warn_insufficient_qty_unbuilds){
            return false;
     }

    public Page<Stock_warn_insufficient_qty_unbuild> searchDefault(Stock_warn_insufficient_qty_unbuildSearchContext context){
            return null;
     }



    public Stock_warn_insufficient_qty_unbuild update(Integer id, Stock_warn_insufficient_qty_unbuild stock_warn_insufficient_qty_unbuild){
            return null;
     }
    public Boolean updateBatch(List<Stock_warn_insufficient_qty_unbuild> stock_warn_insufficient_qty_unbuilds){
            return false;
     }


    public Page<Stock_warn_insufficient_qty_unbuild> select(){
            return null;
     }

    public Stock_warn_insufficient_qty_unbuild getDraft(){
            return null;
    }



}
