package cn.ibizlab.odoo.core.odoo_sale.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [销售付款获得在线向导] 对象
 */
@Data
public class Sale_payment_acquirer_onboarding_wizard extends EntityClient implements Serializable {

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 方法
     */
    @DEField(name = "manual_name")
    @JSONField(name = "manual_name")
    @JsonProperty("manual_name")
    private String manualName;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 支付说明
     */
    @DEField(name = "manual_post_msg")
    @JSONField(name = "manual_post_msg")
    @JsonProperty("manual_post_msg")
    private String manualPostMsg;

    /**
     * 账户号码
     */
    @DEField(name = "acc_number")
    @JSONField(name = "acc_number")
    @JsonProperty("acc_number")
    private String accNumber;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 银行名称
     */
    @DEField(name = "journal_name")
    @JSONField(name = "journal_name")
    @JsonProperty("journal_name")
    private String journalName;

    /**
     * Paypal 付款数据传输标记
     */
    @DEField(name = "paypal_pdt_token")
    @JSONField(name = "paypal_pdt_token")
    @JsonProperty("paypal_pdt_token")
    private String paypalPdtToken;

    /**
     * Stripe 密钥
     */
    @DEField(name = "stripe_secret_key")
    @JSONField(name = "stripe_secret_key")
    @JsonProperty("stripe_secret_key")
    private String stripeSecretKey;

    /**
     * Stripe 公钥
     */
    @DEField(name = "stripe_publishable_key")
    @JSONField(name = "stripe_publishable_key")
    @JsonProperty("stripe_publishable_key")
    private String stripePublishableKey;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 付款方法
     */
    @DEField(name = "payment_method")
    @JSONField(name = "payment_method")
    @JsonProperty("payment_method")
    private String paymentMethod;

    /**
     * PayPal EMailID
     */
    @DEField(name = "paypal_email_account")
    @JSONField(name = "paypal_email_account")
    @JsonProperty("paypal_email_account")
    private String paypalEmailAccount;

    /**
     * Paypal 销售商 ID
     */
    @DEField(name = "paypal_seller_account")
    @JSONField(name = "paypal_seller_account")
    @JsonProperty("paypal_seller_account")
    private String paypalSellerAccount;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [方法]
     */
    public void setManualName(String manualName){
        this.manualName = manualName ;
        this.modify("manual_name",manualName);
    }
    /**
     * 设置 [支付说明]
     */
    public void setManualPostMsg(String manualPostMsg){
        this.manualPostMsg = manualPostMsg ;
        this.modify("manual_post_msg",manualPostMsg);
    }
    /**
     * 设置 [账户号码]
     */
    public void setAccNumber(String accNumber){
        this.accNumber = accNumber ;
        this.modify("acc_number",accNumber);
    }
    /**
     * 设置 [银行名称]
     */
    public void setJournalName(String journalName){
        this.journalName = journalName ;
        this.modify("journal_name",journalName);
    }
    /**
     * 设置 [Paypal 付款数据传输标记]
     */
    public void setPaypalPdtToken(String paypalPdtToken){
        this.paypalPdtToken = paypalPdtToken ;
        this.modify("paypal_pdt_token",paypalPdtToken);
    }
    /**
     * 设置 [Stripe 密钥]
     */
    public void setStripeSecretKey(String stripeSecretKey){
        this.stripeSecretKey = stripeSecretKey ;
        this.modify("stripe_secret_key",stripeSecretKey);
    }
    /**
     * 设置 [Stripe 公钥]
     */
    public void setStripePublishableKey(String stripePublishableKey){
        this.stripePublishableKey = stripePublishableKey ;
        this.modify("stripe_publishable_key",stripePublishableKey);
    }
    /**
     * 设置 [付款方法]
     */
    public void setPaymentMethod(String paymentMethod){
        this.paymentMethod = paymentMethod ;
        this.modify("payment_method",paymentMethod);
    }
    /**
     * 设置 [PayPal EMailID]
     */
    public void setPaypalEmailAccount(String paypalEmailAccount){
        this.paypalEmailAccount = paypalEmailAccount ;
        this.modify("paypal_email_account",paypalEmailAccount);
    }
    /**
     * 设置 [Paypal 销售商 ID]
     */
    public void setPaypalSellerAccount(String paypalSellerAccount){
        this.paypalSellerAccount = paypalSellerAccount ;
        this.modify("paypal_seller_account",paypalSellerAccount);
    }

}


