package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ibase_import_mapping;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_import_mapping] 服务对象接口
 */
public interface Ibase_import_mappingClientService{

    public Ibase_import_mapping createModel() ;

    public void update(Ibase_import_mapping base_import_mapping);

    public void createBatch(List<Ibase_import_mapping> base_import_mappings);

    public void removeBatch(List<Ibase_import_mapping> base_import_mappings);

    public void remove(Ibase_import_mapping base_import_mapping);

    public void updateBatch(List<Ibase_import_mapping> base_import_mappings);

    public void create(Ibase_import_mapping base_import_mapping);

    public void get(Ibase_import_mapping base_import_mapping);

    public Page<Ibase_import_mapping> fetchDefault(SearchContext context);

    public Page<Ibase_import_mapping> select(SearchContext context);

    public void getDraft(Ibase_import_mapping base_import_mapping);

}
