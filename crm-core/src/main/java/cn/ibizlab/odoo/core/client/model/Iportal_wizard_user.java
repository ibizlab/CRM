package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [portal_wizard_user] 对象
 */
public interface Iportal_wizard_user {

    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [EMail]
     */
    public void setEmail(String email);
    
    /**
     * 设置 [EMail]
     */
    public String getEmail();

    /**
     * 获取 [EMail]脏标记
     */
    public boolean getEmailDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [在门户中]
     */
    public void setIn_portal(String in_portal);
    
    /**
     * 设置 [在门户中]
     */
    public String getIn_portal();

    /**
     * 获取 [在门户中]脏标记
     */
    public boolean getIn_portalDirtyFlag();
    /**
     * 获取 [联系]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [联系]
     */
    public Integer getPartner_id();

    /**
     * 获取 [联系]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [联系]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [联系]
     */
    public String getPartner_id_text();

    /**
     * 获取 [联系]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [登陆用户]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [登陆用户]
     */
    public Integer getUser_id();

    /**
     * 获取 [登陆用户]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [登陆用户]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [登陆用户]
     */
    public String getUser_id_text();

    /**
     * 获取 [登陆用户]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [向导]
     */
    public void setWizard_id(Integer wizard_id);
    
    /**
     * 设置 [向导]
     */
    public Integer getWizard_id();

    /**
     * 获取 [向导]脏标记
     */
    public boolean getWizard_idDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
