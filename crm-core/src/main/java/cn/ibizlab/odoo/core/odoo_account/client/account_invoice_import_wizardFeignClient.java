package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_import_wizard;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_import_wizardSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_invoice_import_wizard] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-invoice-import-wizard", fallback = account_invoice_import_wizardFallback.class)
public interface account_invoice_import_wizardFeignClient {


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_import_wizards/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_import_wizards/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_import_wizards")
    Account_invoice_import_wizard create(@RequestBody Account_invoice_import_wizard account_invoice_import_wizard);

    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_import_wizards/batch")
    Boolean createBatch(@RequestBody List<Account_invoice_import_wizard> account_invoice_import_wizards);




    @RequestMapping(method = RequestMethod.GET, value = "/account_invoice_import_wizards/{id}")
    Account_invoice_import_wizard get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_import_wizards/{id}")
    Account_invoice_import_wizard update(@PathVariable("id") Integer id,@RequestBody Account_invoice_import_wizard account_invoice_import_wizard);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_import_wizards/batch")
    Boolean updateBatch(@RequestBody List<Account_invoice_import_wizard> account_invoice_import_wizards);



    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_import_wizards/searchdefault")
    Page<Account_invoice_import_wizard> searchDefault(@RequestBody Account_invoice_import_wizardSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/account_invoice_import_wizards/select")
    Page<Account_invoice_import_wizard> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_invoice_import_wizards/getdraft")
    Account_invoice_import_wizard getDraft();


}
