package cn.ibizlab.odoo.core.odoo_base.valuerule.anno.base_automation;

import cn.ibizlab.odoo.core.odoo_base.valuerule.validator.base_automation.Base_automationActivity_noteDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Base_automation
 * 属性：Activity_note
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Base_automationActivity_noteDefaultValidator.class})
public @interface Base_automationActivity_noteDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[1048576]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
