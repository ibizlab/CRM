package cn.ibizlab.odoo.core.odoo_survey.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_label;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_labelSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[survey_label] 服务对象接口
 */
@FeignClient(value = "odoo-survey", contextId = "survey-label", fallback = survey_labelFallback.class)
public interface survey_labelFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/survey_labels/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/survey_labels/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.PUT, value = "/survey_labels/{id}")
    Survey_label update(@PathVariable("id") Integer id,@RequestBody Survey_label survey_label);

    @RequestMapping(method = RequestMethod.PUT, value = "/survey_labels/batch")
    Boolean updateBatch(@RequestBody List<Survey_label> survey_labels);


    @RequestMapping(method = RequestMethod.GET, value = "/survey_labels/{id}")
    Survey_label get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/survey_labels/searchdefault")
    Page<Survey_label> searchDefault(@RequestBody Survey_labelSearchContext context);




    @RequestMapping(method = RequestMethod.POST, value = "/survey_labels")
    Survey_label create(@RequestBody Survey_label survey_label);

    @RequestMapping(method = RequestMethod.POST, value = "/survey_labels/batch")
    Boolean createBatch(@RequestBody List<Survey_label> survey_labels);


    @RequestMapping(method = RequestMethod.GET, value = "/survey_labels/select")
    Page<Survey_label> select();


    @RequestMapping(method = RequestMethod.GET, value = "/survey_labels/getdraft")
    Survey_label getDraft();


}
