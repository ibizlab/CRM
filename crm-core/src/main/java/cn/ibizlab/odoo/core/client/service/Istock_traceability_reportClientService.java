package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_traceability_report;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_traceability_report] 服务对象接口
 */
public interface Istock_traceability_reportClientService{

    public Istock_traceability_report createModel() ;

    public void create(Istock_traceability_report stock_traceability_report);

    public void update(Istock_traceability_report stock_traceability_report);

    public void removeBatch(List<Istock_traceability_report> stock_traceability_reports);

    public void createBatch(List<Istock_traceability_report> stock_traceability_reports);

    public void remove(Istock_traceability_report stock_traceability_report);

    public void updateBatch(List<Istock_traceability_report> stock_traceability_reports);

    public void get(Istock_traceability_report stock_traceability_report);

    public Page<Istock_traceability_report> fetchDefault(SearchContext context);

    public Page<Istock_traceability_report> select(SearchContext context);

    public void getDraft(Istock_traceability_report stock_traceability_report);

}
