package cn.ibizlab.odoo.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter_ratio;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_meter_ratioSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mro_pm_meter_ratio] 服务对象接口
 */
@FeignClient(value = "odoo-mro", contextId = "mro-pm-meter-ratio", fallback = mro_pm_meter_ratioFallback.class)
public interface mro_pm_meter_ratioFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/mro_pm_meter_ratios/{id}")
    Mro_pm_meter_ratio get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_ratios")
    Mro_pm_meter_ratio create(@RequestBody Mro_pm_meter_ratio mro_pm_meter_ratio);

    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_ratios/batch")
    Boolean createBatch(@RequestBody List<Mro_pm_meter_ratio> mro_pm_meter_ratios);


    @RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_meter_ratios/{id}")
    Mro_pm_meter_ratio update(@PathVariable("id") Integer id,@RequestBody Mro_pm_meter_ratio mro_pm_meter_ratio);

    @RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_meter_ratios/batch")
    Boolean updateBatch(@RequestBody List<Mro_pm_meter_ratio> mro_pm_meter_ratios);



    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_meter_ratios/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_meter_ratios/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);





    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_ratios/searchdefault")
    Page<Mro_pm_meter_ratio> searchDefault(@RequestBody Mro_pm_meter_ratioSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/mro_pm_meter_ratios/select")
    Page<Mro_pm_meter_ratio> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mro_pm_meter_ratios/getdraft")
    Mro_pm_meter_ratio getDraft();


}
