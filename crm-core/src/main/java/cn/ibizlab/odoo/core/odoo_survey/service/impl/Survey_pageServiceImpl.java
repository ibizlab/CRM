package cn.ibizlab.odoo.core.odoo_survey.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_page;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_pageSearchContext;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_pageService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_survey.client.survey_pageFeignClient;

/**
 * 实体[问卷页面] 服务对象接口实现
 */
@Slf4j
@Service
public class Survey_pageServiceImpl implements ISurvey_pageService {

    @Autowired
    survey_pageFeignClient survey_pageFeignClient;


    @Override
    public boolean update(Survey_page et) {
        Survey_page rt = survey_pageFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Survey_page> list){
        survey_pageFeignClient.updateBatch(list) ;
    }

    @Override
    public Survey_page getDraft(Survey_page et) {
        et=survey_pageFeignClient.getDraft();
        return et;
    }

    @Override
    public Survey_page get(Integer id) {
		Survey_page et=survey_pageFeignClient.get(id);
        if(et==null){
            et=new Survey_page();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=survey_pageFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        survey_pageFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Survey_page et) {
        Survey_page rt = survey_pageFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Survey_page> list){
        survey_pageFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Survey_page> searchDefault(Survey_pageSearchContext context) {
        Page<Survey_page> survey_pages=survey_pageFeignClient.searchDefault(context);
        return survey_pages;
    }


}


