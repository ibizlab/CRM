package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_distribution;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_distributionSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_analytic_distributionService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_analytic_distributionFeignClient;

/**
 * 实体[分析账户分配] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_analytic_distributionServiceImpl implements IAccount_analytic_distributionService {

    @Autowired
    account_analytic_distributionFeignClient account_analytic_distributionFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=account_analytic_distributionFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_analytic_distributionFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Account_analytic_distribution et) {
        Account_analytic_distribution rt = account_analytic_distributionFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_analytic_distribution> list){
        account_analytic_distributionFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Account_analytic_distribution et) {
        Account_analytic_distribution rt = account_analytic_distributionFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_analytic_distribution> list){
        account_analytic_distributionFeignClient.updateBatch(list) ;
    }

    @Override
    public Account_analytic_distribution getDraft(Account_analytic_distribution et) {
        et=account_analytic_distributionFeignClient.getDraft();
        return et;
    }

    @Override
    public Account_analytic_distribution get(Integer id) {
		Account_analytic_distribution et=account_analytic_distributionFeignClient.get(id);
        if(et==null){
            et=new Account_analytic_distribution();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_analytic_distribution> searchDefault(Account_analytic_distributionSearchContext context) {
        Page<Account_analytic_distribution> account_analytic_distributions=account_analytic_distributionFeignClient.searchDefault(context);
        return account_analytic_distributions;
    }


}


