package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_company;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_companySearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[res_company] 服务对象接口
 */
@Component
public class res_companyFallback implements res_companyFeignClient{


    public Res_company create(Res_company res_company){
            return null;
     }
    public Boolean createBatch(List<Res_company> res_companies){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Res_company update(Integer id, Res_company res_company){
            return null;
     }
    public Boolean updateBatch(List<Res_company> res_companies){
            return false;
     }


    public Page<Res_company> searchDefault(Res_companySearchContext context){
            return null;
     }



    public Res_company get(Integer id){
            return null;
     }


    public Page<Res_company> select(){
            return null;
     }

    public Res_company getDraft(){
            return null;
    }



}
