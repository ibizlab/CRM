package cn.ibizlab.odoo.core.odoo_product.valuerule.anno.product_attribute_custom_value;

import cn.ibizlab.odoo.core.odoo_product.valuerule.validator.product_attribute_custom_value.Product_attribute_custom_valueCustom_valueDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Product_attribute_custom_value
 * 属性：Custom_value
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Product_attribute_custom_valueCustom_valueDefaultValidator.class})
public @interface Product_attribute_custom_valueCustom_valueDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[100]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
