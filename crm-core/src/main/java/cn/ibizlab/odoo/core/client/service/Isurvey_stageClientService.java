package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Isurvey_stage;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[survey_stage] 服务对象接口
 */
public interface Isurvey_stageClientService{

    public Isurvey_stage createModel() ;

    public void updateBatch(List<Isurvey_stage> survey_stages);

    public void removeBatch(List<Isurvey_stage> survey_stages);

    public void create(Isurvey_stage survey_stage);

    public Page<Isurvey_stage> fetchDefault(SearchContext context);

    public void remove(Isurvey_stage survey_stage);

    public void createBatch(List<Isurvey_stage> survey_stages);

    public void update(Isurvey_stage survey_stage);

    public void get(Isurvey_stage survey_stage);

    public Page<Isurvey_stage> select(SearchContext context);

    public void getDraft(Isurvey_stage survey_stage);

}
