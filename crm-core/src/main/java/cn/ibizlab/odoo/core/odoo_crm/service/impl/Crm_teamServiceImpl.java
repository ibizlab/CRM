package cn.ibizlab.odoo.core.odoo_crm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_team;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_teamSearchContext;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_teamService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_crm.client.crm_teamFeignClient;

/**
 * 实体[销售渠道] 服务对象接口实现
 */
@Slf4j
@Service
public class Crm_teamServiceImpl implements ICrm_teamService {

    @Autowired
    crm_teamFeignClient crm_teamFeignClient;


    @Override
    public Crm_team get(Integer id) {
		Crm_team et=crm_teamFeignClient.get(id);
        if(et==null){
            et=new Crm_team();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean checkKey(Crm_team et) {
        return crm_teamFeignClient.checkKey(et);
    }
    @Override
    public Crm_team getDraft(Crm_team et) {
        et=crm_teamFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Crm_team et) {
        Crm_team rt = crm_teamFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Crm_team> list){
        crm_teamFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=crm_teamFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        crm_teamFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Crm_team et) {
        Crm_team rt = crm_teamFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Crm_team> list){
        crm_teamFeignClient.createBatch(list) ;
    }

    @Override
    @Transactional
    public boolean save(Crm_team et) {
        if(et.getId()==null) et.setId((Integer)et.getDefaultKey(true));
        if(!crm_teamFeignClient.save(et))
            return false;
        return true;
    }

    @Override
    public void saveBatch(List<Crm_team> list) {
        crm_teamFeignClient.saveBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Crm_team> searchDefault(Crm_teamSearchContext context) {
        Page<Crm_team> crm_teams=crm_teamFeignClient.searchDefault(context);
        return crm_teams;
    }


}


