package cn.ibizlab.odoo.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_employee;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_employeeSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[hr_employee] 服务对象接口
 */
@FeignClient(value = "odoo-hr", contextId = "hr-employee", fallback = hr_employeeFallback.class)
public interface hr_employeeFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/hr_employees/searchdefault")
    Page<Hr_employee> searchDefault(@RequestBody Hr_employeeSearchContext context);



    @RequestMapping(method = RequestMethod.PUT, value = "/hr_employees/{id}")
    Hr_employee update(@PathVariable("id") Integer id,@RequestBody Hr_employee hr_employee);

    @RequestMapping(method = RequestMethod.PUT, value = "/hr_employees/batch")
    Boolean updateBatch(@RequestBody List<Hr_employee> hr_employees);




    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_employees/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_employees/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/hr_employees")
    Hr_employee create(@RequestBody Hr_employee hr_employee);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_employees/batch")
    Boolean createBatch(@RequestBody List<Hr_employee> hr_employees);


    @RequestMapping(method = RequestMethod.GET, value = "/hr_employees/{id}")
    Hr_employee get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/hr_employees/select")
    Page<Hr_employee> select();


    @RequestMapping(method = RequestMethod.GET, value = "/hr_employees/getdraft")
    Hr_employee getDraft();


}
