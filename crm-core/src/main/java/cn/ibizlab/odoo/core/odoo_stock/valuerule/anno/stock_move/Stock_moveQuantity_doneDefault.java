package cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_move;

import cn.ibizlab.odoo.core.odoo_stock.valuerule.validator.stock_move.Stock_moveQuantity_doneDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Stock_move
 * 属性：Quantity_done
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Stock_moveQuantity_doneDefaultValidator.class})
public @interface Stock_moveQuantity_doneDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
