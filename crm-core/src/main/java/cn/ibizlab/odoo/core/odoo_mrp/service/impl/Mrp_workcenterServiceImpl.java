package cn.ibizlab.odoo.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenterSearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_workcenterService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mrp.client.mrp_workcenterFeignClient;

/**
 * 实体[工作中心] 服务对象接口实现
 */
@Slf4j
@Service
public class Mrp_workcenterServiceImpl implements IMrp_workcenterService {

    @Autowired
    mrp_workcenterFeignClient mrp_workcenterFeignClient;


    @Override
    public Mrp_workcenter getDraft(Mrp_workcenter et) {
        et=mrp_workcenterFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Mrp_workcenter et) {
        Mrp_workcenter rt = mrp_workcenterFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mrp_workcenter> list){
        mrp_workcenterFeignClient.createBatch(list) ;
    }

    @Override
    public Mrp_workcenter get(Integer id) {
		Mrp_workcenter et=mrp_workcenterFeignClient.get(id);
        if(et==null){
            et=new Mrp_workcenter();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Mrp_workcenter et) {
        Mrp_workcenter rt = mrp_workcenterFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mrp_workcenter> list){
        mrp_workcenterFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mrp_workcenterFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mrp_workcenterFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mrp_workcenter> searchDefault(Mrp_workcenterSearchContext context) {
        Page<Mrp_workcenter> mrp_workcenters=mrp_workcenterFeignClient.searchDefault(context);
        return mrp_workcenters;
    }


}


