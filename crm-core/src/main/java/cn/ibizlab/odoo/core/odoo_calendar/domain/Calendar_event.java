package cn.ibizlab.odoo.core.odoo_calendar.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [活动] 对象
 */
@Data
public class Calendar_event extends EntityClient implements Serializable {

    /**
     * 地点
     */
    @JSONField(name = "location")
    @JsonProperty("location")
    private String location;

    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 说明
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 循环ID日期
     */
    @DEField(name = "recurrent_id_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "recurrent_id_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("recurrent_id_date")
    private Timestamp recurrentIdDate;

    /**
     * 周六
     */
    @JSONField(name = "sa")
    @JsonProperty("sa")
    private String sa;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 循环
     */
    @JSONField(name = "recurrency")
    @JsonProperty("recurrency")
    private String recurrency;

    /**
     * 重复
     */
    @JSONField(name = "count")
    @JsonProperty("count")
    private Integer count;

    /**
     * 周五
     */
    @JSONField(name = "fr")
    @JsonProperty("fr")
    private String fr;

    /**
     * 循环规则
     */
    @JSONField(name = "rrule")
    @JsonProperty("rrule")
    private String rrule;

    /**
     * 选项
     */
    @DEField(name = "month_by")
    @JSONField(name = "month_by")
    @JsonProperty("month_by")
    private String monthBy;

    /**
     * 工作日
     */
    @DEField(name = "week_list")
    @JSONField(name = "week_list")
    @JsonProperty("week_list")
    private String weekList;

    /**
     * 未读消息
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 停止
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "stop" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("stop")
    private Timestamp stop;

    /**
     * 文档ID
     */
    @DEField(name = "res_id")
    @JSONField(name = "res_id")
    @JsonProperty("res_id")
    private Integer resId;

    /**
     * 错误数
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 循环ID
     */
    @DEField(name = "recurrent_id")
    @JSONField(name = "recurrent_id")
    @JsonProperty("recurrent_id")
    private Integer recurrentId;

    /**
     * 消息递送错误
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 重复终止
     */
    @DEField(name = "end_type")
    @JSONField(name = "end_type")
    @JsonProperty("end_type")
    private String endType;

    /**
     * 参与者
     */
    @JSONField(name = "attendee_ids")
    @JsonProperty("attendee_ids")
    private String attendeeIds;

    /**
     * 出席者状态
     */
    @JSONField(name = "attendee_status")
    @JsonProperty("attendee_status")
    private String attendeeStatus;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 重新提起
     */
    @DEField(name = "rrule_type")
    @JSONField(name = "rrule_type")
    @JsonProperty("rrule_type")
    private String rruleType;

    /**
     * 重复
     */
    @JSONField(name = "interval")
    @JsonProperty("interval")
    private Integer interval;

    /**
     * 隐私
     */
    @JSONField(name = "privacy")
    @JsonProperty("privacy")
    private String privacy;

    /**
     * 持续时间
     */
    @JSONField(name = "duration")
    @JsonProperty("duration")
    private Double duration;

    /**
     * 开始时间
     */
    @DEField(name = "start_datetime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "start_datetime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("start_datetime")
    private Timestamp startDatetime;

    /**
     * 出席者
     */
    @JSONField(name = "is_attendee")
    @JsonProperty("is_attendee")
    private String isAttendee;

    /**
     * 开始日期
     */
    @DEField(name = "start_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "start_date" , format="yyyy-MM-dd")
    @JsonProperty("start_date")
    private Timestamp startDate;

    /**
     * 周一
     */
    @JSONField(name = "mo")
    @JsonProperty("mo")
    private String mo;

    /**
     * 状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 周三
     */
    @JSONField(name = "we")
    @JsonProperty("we")
    private String we;

    /**
     * 关注者(业务伙伴)
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 活动时间
     */
    @JSONField(name = "display_time")
    @JsonProperty("display_time")
    private String displayTime;

    /**
     * 日期
     */
    @DEField(name = "display_start")
    @JSONField(name = "display_start")
    @JsonProperty("display_start")
    private String displayStart;

    /**
     * 结束日期
     */
    @DEField(name = "stop_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "stop_date" , format="yyyy-MM-dd")
    @JsonProperty("stop_date")
    private Timestamp stopDate;

    /**
     * 需要采取行动
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 未读消息计数器
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 重复直到
     */
    @DEField(name = "final_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "final_date" , format="yyyy-MM-dd")
    @JsonProperty("final_date")
    private Timestamp finalDate;

    /**
     * 文档模型
     */
    @DEField(name = "res_model_id")
    @JSONField(name = "res_model_id")
    @JsonProperty("res_model_id")
    private Integer resModelId;

    /**
     * 事件是否突出显示
     */
    @JSONField(name = "is_highlighted")
    @JsonProperty("is_highlighted")
    private String isHighlighted;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 关注者(渠道)
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 结束日期时间
     */
    @DEField(name = "stop_datetime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "stop_datetime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("stop_datetime")
    private Timestamp stopDatetime;

    /**
     * 活动
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;

    /**
     * 文档模型名称
     */
    @DEField(name = "res_model")
    @JSONField(name = "res_model")
    @JsonProperty("res_model")
    private String resModel;

    /**
     * 周二
     */
    @JSONField(name = "tu")
    @JsonProperty("tu")
    private String tu;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 显示时间为
     */
    @DEField(name = "show_as")
    @JSONField(name = "show_as")
    @JsonProperty("show_as")
    private String showAs;

    /**
     * 标签
     */
    @JSONField(name = "categ_ids")
    @JsonProperty("categ_ids")
    private String categIds;

    /**
     * 提醒
     */
    @JSONField(name = "alarm_ids")
    @JsonProperty("alarm_ids")
    private String alarmIds;

    /**
     * 周四
     */
    @JSONField(name = "th")
    @JsonProperty("th")
    private String th;

    /**
     * 行动数量
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 按 天
     */
    @JSONField(name = "byday")
    @JsonProperty("byday")
    private String byday;

    /**
     * 日期
     */
    @JSONField(name = "day")
    @JsonProperty("day")
    private Integer day;

    /**
     * 开始
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "start" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("start")
    private Timestamp start;

    /**
     * 附件数量
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 与会者
     */
    @JSONField(name = "partner_ids")
    @JsonProperty("partner_ids")
    private String partnerIds;

    /**
     * 全天
     */
    @JSONField(name = "allday")
    @JsonProperty("allday")
    private String allday;

    /**
     * 消息
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 网站消息
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 关注者
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 周日
     */
    @JSONField(name = "su")
    @JsonProperty("su")
    private String su;

    /**
     * 是关注者
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 会议主题
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 商机
     */
    @JSONField(name = "opportunity_id_text")
    @JsonProperty("opportunity_id_text")
    private String opportunityIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 所有者
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 负责人
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 申请人
     */
    @JSONField(name = "applicant_id_text")
    @JsonProperty("applicant_id_text")
    private String applicantIdText;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 申请人
     */
    @DEField(name = "applicant_id")
    @JSONField(name = "applicant_id")
    @JsonProperty("applicant_id")
    private Integer applicantId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 所有者
     */
    @DEField(name = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;

    /**
     * 商机
     */
    @DEField(name = "opportunity_id")
    @JSONField(name = "opportunity_id")
    @JsonProperty("opportunity_id")
    private Integer opportunityId;


    /**
     * 
     */
    @JSONField(name = "odooopportunity")
    @JsonProperty("odooopportunity")
    private cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead odooOpportunity;

    /**
     * 
     */
    @JSONField(name = "odooapplicant")
    @JsonProperty("odooapplicant")
    private cn.ibizlab.odoo.core.odoo_hr.domain.Hr_applicant odooApplicant;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoouser")
    @JsonProperty("odoouser")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [地点]
     */
    public void setLocation(String location){
        this.location = location ;
        this.modify("location",location);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }
    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }
    /**
     * 设置 [循环ID日期]
     */
    public void setRecurrentIdDate(Timestamp recurrentIdDate){
        this.recurrentIdDate = recurrentIdDate ;
        this.modify("recurrent_id_date",recurrentIdDate);
    }
    /**
     * 设置 [周六]
     */
    public void setSa(String sa){
        this.sa = sa ;
        this.modify("sa",sa);
    }
    /**
     * 设置 [循环]
     */
    public void setRecurrency(String recurrency){
        this.recurrency = recurrency ;
        this.modify("recurrency",recurrency);
    }
    /**
     * 设置 [重复]
     */
    public void setCount(Integer count){
        this.count = count ;
        this.modify("count",count);
    }
    /**
     * 设置 [周五]
     */
    public void setFr(String fr){
        this.fr = fr ;
        this.modify("fr",fr);
    }
    /**
     * 设置 [循环规则]
     */
    public void setRrule(String rrule){
        this.rrule = rrule ;
        this.modify("rrule",rrule);
    }
    /**
     * 设置 [选项]
     */
    public void setMonthBy(String monthBy){
        this.monthBy = monthBy ;
        this.modify("month_by",monthBy);
    }
    /**
     * 设置 [工作日]
     */
    public void setWeekList(String weekList){
        this.weekList = weekList ;
        this.modify("week_list",weekList);
    }
    /**
     * 设置 [停止]
     */
    public void setStop(Timestamp stop){
        this.stop = stop ;
        this.modify("stop",stop);
    }
    /**
     * 设置 [文档ID]
     */
    public void setResId(Integer resId){
        this.resId = resId ;
        this.modify("res_id",resId);
    }
    /**
     * 设置 [循环ID]
     */
    public void setRecurrentId(Integer recurrentId){
        this.recurrentId = recurrentId ;
        this.modify("recurrent_id",recurrentId);
    }
    /**
     * 设置 [重复终止]
     */
    public void setEndType(String endType){
        this.endType = endType ;
        this.modify("end_type",endType);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [重新提起]
     */
    public void setRruleType(String rruleType){
        this.rruleType = rruleType ;
        this.modify("rrule_type",rruleType);
    }
    /**
     * 设置 [重复]
     */
    public void setInterval(Integer interval){
        this.interval = interval ;
        this.modify("interval",interval);
    }
    /**
     * 设置 [隐私]
     */
    public void setPrivacy(String privacy){
        this.privacy = privacy ;
        this.modify("privacy",privacy);
    }
    /**
     * 设置 [持续时间]
     */
    public void setDuration(Double duration){
        this.duration = duration ;
        this.modify("duration",duration);
    }
    /**
     * 设置 [开始时间]
     */
    public void setStartDatetime(Timestamp startDatetime){
        this.startDatetime = startDatetime ;
        this.modify("start_datetime",startDatetime);
    }
    /**
     * 设置 [开始日期]
     */
    public void setStartDate(Timestamp startDate){
        this.startDate = startDate ;
        this.modify("start_date",startDate);
    }
    /**
     * 设置 [周一]
     */
    public void setMo(String mo){
        this.mo = mo ;
        this.modify("mo",mo);
    }
    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }
    /**
     * 设置 [周三]
     */
    public void setWe(String we){
        this.we = we ;
        this.modify("we",we);
    }
    /**
     * 设置 [日期]
     */
    public void setDisplayStart(String displayStart){
        this.displayStart = displayStart ;
        this.modify("display_start",displayStart);
    }
    /**
     * 设置 [结束日期]
     */
    public void setStopDate(Timestamp stopDate){
        this.stopDate = stopDate ;
        this.modify("stop_date",stopDate);
    }
    /**
     * 设置 [重复直到]
     */
    public void setFinalDate(Timestamp finalDate){
        this.finalDate = finalDate ;
        this.modify("final_date",finalDate);
    }
    /**
     * 设置 [文档模型]
     */
    public void setResModelId(Integer resModelId){
        this.resModelId = resModelId ;
        this.modify("res_model_id",resModelId);
    }
    /**
     * 设置 [结束日期时间]
     */
    public void setStopDatetime(Timestamp stopDatetime){
        this.stopDatetime = stopDatetime ;
        this.modify("stop_datetime",stopDatetime);
    }
    /**
     * 设置 [文档模型名称]
     */
    public void setResModel(String resModel){
        this.resModel = resModel ;
        this.modify("res_model",resModel);
    }
    /**
     * 设置 [周二]
     */
    public void setTu(String tu){
        this.tu = tu ;
        this.modify("tu",tu);
    }
    /**
     * 设置 [显示时间为]
     */
    public void setShowAs(String showAs){
        this.showAs = showAs ;
        this.modify("show_as",showAs);
    }
    /**
     * 设置 [周四]
     */
    public void setTh(String th){
        this.th = th ;
        this.modify("th",th);
    }
    /**
     * 设置 [按 天]
     */
    public void setByday(String byday){
        this.byday = byday ;
        this.modify("byday",byday);
    }
    /**
     * 设置 [日期]
     */
    public void setDay(Integer day){
        this.day = day ;
        this.modify("day",day);
    }
    /**
     * 设置 [开始]
     */
    public void setStart(Timestamp start){
        this.start = start ;
        this.modify("start",start);
    }
    /**
     * 设置 [全天]
     */
    public void setAllday(String allday){
        this.allday = allday ;
        this.modify("allday",allday);
    }
    /**
     * 设置 [周日]
     */
    public void setSu(String su){
        this.su = su ;
        this.modify("su",su);
    }
    /**
     * 设置 [会议主题]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [申请人]
     */
    public void setApplicantId(Integer applicantId){
        this.applicantId = applicantId ;
        this.modify("applicant_id",applicantId);
    }
    /**
     * 设置 [所有者]
     */
    public void setUserId(Integer userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }
    /**
     * 设置 [商机]
     */
    public void setOpportunityId(Integer opportunityId){
        this.opportunityId = opportunityId ;
        this.modify("opportunity_id",opportunityId);
    }

}


