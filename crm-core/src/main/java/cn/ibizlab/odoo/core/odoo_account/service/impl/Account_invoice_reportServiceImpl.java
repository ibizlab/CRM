package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_report;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_reportSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoice_reportService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_invoice_reportFeignClient;

/**
 * 实体[发票统计] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_invoice_reportServiceImpl implements IAccount_invoice_reportService {

    @Autowired
    account_invoice_reportFeignClient account_invoice_reportFeignClient;


    @Override
    public Account_invoice_report get(Integer id) {
		Account_invoice_report et=account_invoice_reportFeignClient.get(id);
        if(et==null){
            et=new Account_invoice_report();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Account_invoice_report et) {
        Account_invoice_report rt = account_invoice_reportFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_invoice_report> list){
        account_invoice_reportFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_invoice_reportFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_invoice_reportFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Account_invoice_report et) {
        Account_invoice_report rt = account_invoice_reportFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_invoice_report> list){
        account_invoice_reportFeignClient.updateBatch(list) ;
    }

    @Override
    public Account_invoice_report getDraft(Account_invoice_report et) {
        et=account_invoice_reportFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_invoice_report> searchDefault(Account_invoice_reportSearchContext context) {
        Page<Account_invoice_report> account_invoice_reports=account_invoice_reportFeignClient.searchDefault(context);
        return account_invoice_reports;
    }


}


