package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity_type;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_activity_typeSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_activity_type] 服务对象接口
 */
@Component
public class mail_activity_typeFallback implements mail_activity_typeFeignClient{

    public Page<Mail_activity_type> searchDefault(Mail_activity_typeSearchContext context){
            return null;
     }


    public Mail_activity_type update(Integer id, Mail_activity_type mail_activity_type){
            return null;
     }
    public Boolean updateBatch(List<Mail_activity_type> mail_activity_types){
            return false;
     }


    public Mail_activity_type create(Mail_activity_type mail_activity_type){
            return null;
     }
    public Boolean createBatch(List<Mail_activity_type> mail_activity_types){
            return false;
     }

    public Mail_activity_type get(Integer id){
            return null;
     }




    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Page<Mail_activity_type> select(){
            return null;
     }

    public Mail_activity_type getDraft(){
            return null;
    }



    public Boolean checkKey(Mail_activity_type mail_activity_type){
            return false;
     }


    public Boolean save(Mail_activity_type mail_activity_type){
            return false;
     }
    public Boolean saveBatch(List<Mail_activity_type> mail_activity_types){
            return false;
     }

}
