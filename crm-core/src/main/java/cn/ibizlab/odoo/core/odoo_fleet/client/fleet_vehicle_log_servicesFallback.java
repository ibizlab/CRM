package cn.ibizlab.odoo.core.odoo_fleet.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_services;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_log_servicesSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[fleet_vehicle_log_services] 服务对象接口
 */
@Component
public class fleet_vehicle_log_servicesFallback implements fleet_vehicle_log_servicesFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Fleet_vehicle_log_services create(Fleet_vehicle_log_services fleet_vehicle_log_services){
            return null;
     }
    public Boolean createBatch(List<Fleet_vehicle_log_services> fleet_vehicle_log_services){
            return false;
     }

    public Fleet_vehicle_log_services get(Integer id){
            return null;
     }



    public Page<Fleet_vehicle_log_services> searchDefault(Fleet_vehicle_log_servicesSearchContext context){
            return null;
     }



    public Fleet_vehicle_log_services update(Integer id, Fleet_vehicle_log_services fleet_vehicle_log_services){
            return null;
     }
    public Boolean updateBatch(List<Fleet_vehicle_log_services> fleet_vehicle_log_services){
            return false;
     }



    public Page<Fleet_vehicle_log_services> select(){
            return null;
     }

    public Fleet_vehicle_log_services getDraft(){
            return null;
    }



}
