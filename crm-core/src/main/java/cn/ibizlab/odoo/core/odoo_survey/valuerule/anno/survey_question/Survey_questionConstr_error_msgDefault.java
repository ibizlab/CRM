package cn.ibizlab.odoo.core.odoo_survey.valuerule.anno.survey_question;

import cn.ibizlab.odoo.core.odoo_survey.valuerule.validator.survey_question.Survey_questionConstr_error_msgDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Survey_question
 * 属性：Constr_error_msg
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Survey_questionConstr_error_msgDefaultValidator.class})
public @interface Survey_questionConstr_error_msgDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[100]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
