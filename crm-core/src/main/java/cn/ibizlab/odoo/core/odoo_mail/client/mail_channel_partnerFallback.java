package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_channel_partner;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_channel_partnerSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_channel_partner] 服务对象接口
 */
@Component
public class mail_channel_partnerFallback implements mail_channel_partnerFeignClient{




    public Mail_channel_partner create(Mail_channel_partner mail_channel_partner){
            return null;
     }
    public Boolean createBatch(List<Mail_channel_partner> mail_channel_partners){
            return false;
     }

    public Mail_channel_partner update(Integer id, Mail_channel_partner mail_channel_partner){
            return null;
     }
    public Boolean updateBatch(List<Mail_channel_partner> mail_channel_partners){
            return false;
     }


    public Page<Mail_channel_partner> searchDefault(Mail_channel_partnerSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Mail_channel_partner get(Integer id){
            return null;
     }


    public Page<Mail_channel_partner> select(){
            return null;
     }

    public Mail_channel_partner getDraft(){
            return null;
    }



}
