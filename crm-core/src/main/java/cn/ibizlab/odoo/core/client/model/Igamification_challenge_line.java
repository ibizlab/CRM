package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [gamification_challenge_line] 对象
 */
public interface Igamification_challenge_line {

    /**
     * 获取 [挑战]
     */
    public void setChallenge_id(Integer challenge_id);
    
    /**
     * 设置 [挑战]
     */
    public Integer getChallenge_id();

    /**
     * 获取 [挑战]脏标记
     */
    public boolean getChallenge_idDirtyFlag();
    /**
     * 获取 [挑战]
     */
    public void setChallenge_id_text(String challenge_id_text);
    
    /**
     * 设置 [挑战]
     */
    public String getChallenge_id_text();

    /**
     * 获取 [挑战]脏标记
     */
    public boolean getChallenge_id_textDirtyFlag();
    /**
     * 获取 [目标绩效]
     */
    public void setCondition(String condition);
    
    /**
     * 设置 [目标绩效]
     */
    public String getCondition();

    /**
     * 获取 [目标绩效]脏标记
     */
    public boolean getConditionDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [后缀]
     */
    public void setDefinition_full_suffix(String definition_full_suffix);
    
    /**
     * 设置 [后缀]
     */
    public String getDefinition_full_suffix();

    /**
     * 获取 [后缀]脏标记
     */
    public boolean getDefinition_full_suffixDirtyFlag();
    /**
     * 获取 [目标定义]
     */
    public void setDefinition_id(Integer definition_id);
    
    /**
     * 设置 [目标定义]
     */
    public Integer getDefinition_id();

    /**
     * 获取 [目标定义]脏标记
     */
    public boolean getDefinition_idDirtyFlag();
    /**
     * 获取 [货币]
     */
    public void setDefinition_monetary(String definition_monetary);
    
    /**
     * 设置 [货币]
     */
    public String getDefinition_monetary();

    /**
     * 获取 [货币]脏标记
     */
    public boolean getDefinition_monetaryDirtyFlag();
    /**
     * 获取 [单位]
     */
    public void setDefinition_suffix(String definition_suffix);
    
    /**
     * 设置 [单位]
     */
    public String getDefinition_suffix();

    /**
     * 获取 [单位]脏标记
     */
    public boolean getDefinition_suffixDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [要达到的目标值]
     */
    public void setTarget_goal(Double target_goal);
    
    /**
     * 设置 [要达到的目标值]
     */
    public Double getTarget_goal();

    /**
     * 获取 [要达到的目标值]脏标记
     */
    public boolean getTarget_goalDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
