package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ires_partner;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_partner] 服务对象接口
 */
public interface Ires_partnerClientService{

    public Ires_partner createModel() ;

    public void get(Ires_partner res_partner);

    public void update(Ires_partner res_partner);

    public void create(Ires_partner res_partner);

    public void createBatch(List<Ires_partner> res_partners);

    public void removeBatch(List<Ires_partner> res_partners);

    public void updateBatch(List<Ires_partner> res_partners);

    public void remove(Ires_partner res_partner);

    public Page<Ires_partner> fetchDefault(SearchContext context);

    public Page<Ires_partner> select(SearchContext context);

    public void checkKey(Ires_partner res_partner);

    public void save(Ires_partner res_partner);

    public void getDraft(Ires_partner res_partner);

    public Page<Ires_partner> fetchContacts(SearchContext context);

    public Page<Ires_partner> fetchCompany(SearchContext context);

}
