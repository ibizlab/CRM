package cn.ibizlab.odoo.core.odoo_sms.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_sms.domain.Sms_send_sms;
import cn.ibizlab.odoo.core.odoo_sms.filter.Sms_send_smsSearchContext;


/**
 * 实体[Sms_send_sms] 服务对象接口
 */
public interface ISms_send_smsService{

    boolean update(Sms_send_sms et) ;
    void updateBatch(List<Sms_send_sms> list) ;
    boolean create(Sms_send_sms et) ;
    void createBatch(List<Sms_send_sms> list) ;
    Sms_send_sms getDraft(Sms_send_sms et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Sms_send_sms get(Integer key) ;
    Page<Sms_send_sms> searchDefault(Sms_send_smsSearchContext context) ;

}



