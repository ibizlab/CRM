package cn.ibizlab.odoo.core.odoo_sale.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_template_line;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_order_template_lineSearchContext;


/**
 * 实体[Sale_order_template_line] 服务对象接口
 */
public interface ISale_order_template_lineService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Sale_order_template_line getDraft(Sale_order_template_line et) ;
    boolean create(Sale_order_template_line et) ;
    void createBatch(List<Sale_order_template_line> list) ;
    Sale_order_template_line get(Integer key) ;
    boolean update(Sale_order_template_line et) ;
    void updateBatch(List<Sale_order_template_line> list) ;
    Page<Sale_order_template_line> searchDefault(Sale_order_template_lineSearchContext context) ;

}



