package cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_register_payments;

import cn.ibizlab.odoo.core.odoo_account.valuerule.validator.account_register_payments.Account_register_paymentsCommunicationDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Account_register_payments
 * 属性：Communication
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Account_register_paymentsCommunicationDefaultValidator.class})
public @interface Account_register_paymentsCommunicationDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[100]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
