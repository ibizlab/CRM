package cn.ibizlab.odoo.core.odoo_im_livechat.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_channel;
import cn.ibizlab.odoo.core.odoo_im_livechat.filter.Im_livechat_channelSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[im_livechat_channel] 服务对象接口
 */
@Component
public class im_livechat_channelFallback implements im_livechat_channelFeignClient{


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Im_livechat_channel> searchDefault(Im_livechat_channelSearchContext context){
            return null;
     }


    public Im_livechat_channel create(Im_livechat_channel im_livechat_channel){
            return null;
     }
    public Boolean createBatch(List<Im_livechat_channel> im_livechat_channels){
            return false;
     }


    public Im_livechat_channel update(Integer id, Im_livechat_channel im_livechat_channel){
            return null;
     }
    public Boolean updateBatch(List<Im_livechat_channel> im_livechat_channels){
            return false;
     }



    public Im_livechat_channel get(Integer id){
            return null;
     }


    public Page<Im_livechat_channel> select(){
            return null;
     }

    public Im_livechat_channel getDraft(){
            return null;
    }



}
