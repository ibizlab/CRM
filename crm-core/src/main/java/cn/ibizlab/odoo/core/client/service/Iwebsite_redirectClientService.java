package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iwebsite_redirect;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[website_redirect] 服务对象接口
 */
public interface Iwebsite_redirectClientService{

    public Iwebsite_redirect createModel() ;

    public void remove(Iwebsite_redirect website_redirect);

    public void update(Iwebsite_redirect website_redirect);

    public Page<Iwebsite_redirect> fetchDefault(SearchContext context);

    public void get(Iwebsite_redirect website_redirect);

    public void createBatch(List<Iwebsite_redirect> website_redirects);

    public void updateBatch(List<Iwebsite_redirect> website_redirects);

    public void create(Iwebsite_redirect website_redirect);

    public void removeBatch(List<Iwebsite_redirect> website_redirects);

    public Page<Iwebsite_redirect> select(SearchContext context);

    public void getDraft(Iwebsite_redirect website_redirect);

}
