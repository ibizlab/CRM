package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_cashbox_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_cashbox_lineSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_cashbox_line] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-cashbox-line", fallback = account_cashbox_lineFallback.class)
public interface account_cashbox_lineFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/account_cashbox_lines/{id}")
    Account_cashbox_line update(@PathVariable("id") Integer id,@RequestBody Account_cashbox_line account_cashbox_line);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_cashbox_lines/batch")
    Boolean updateBatch(@RequestBody List<Account_cashbox_line> account_cashbox_lines);




    @RequestMapping(method = RequestMethod.POST, value = "/account_cashbox_lines/searchdefault")
    Page<Account_cashbox_line> searchDefault(@RequestBody Account_cashbox_lineSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/account_cashbox_lines/{id}")
    Account_cashbox_line get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/account_cashbox_lines")
    Account_cashbox_line create(@RequestBody Account_cashbox_line account_cashbox_line);

    @RequestMapping(method = RequestMethod.POST, value = "/account_cashbox_lines/batch")
    Boolean createBatch(@RequestBody List<Account_cashbox_line> account_cashbox_lines);




    @RequestMapping(method = RequestMethod.DELETE, value = "/account_cashbox_lines/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_cashbox_lines/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/account_cashbox_lines/select")
    Page<Account_cashbox_line> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_cashbox_lines/getdraft")
    Account_cashbox_line getDraft();


}
