package cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_location_route;

import cn.ibizlab.odoo.core.odoo_stock.valuerule.validator.stock_location_route.Stock_location_routeWrite_uidDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Stock_location_route
 * 属性：Write_uid
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Stock_location_routeWrite_uidDefaultValidator.class})
public @interface Stock_location_routeWrite_uidDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
