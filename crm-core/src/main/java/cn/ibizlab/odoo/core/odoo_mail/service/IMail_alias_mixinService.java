package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_alias_mixin;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_alias_mixinSearchContext;


/**
 * 实体[Mail_alias_mixin] 服务对象接口
 */
public interface IMail_alias_mixinService{

    boolean create(Mail_alias_mixin et) ;
    void createBatch(List<Mail_alias_mixin> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Mail_alias_mixin getDraft(Mail_alias_mixin et) ;
    boolean update(Mail_alias_mixin et) ;
    void updateBatch(List<Mail_alias_mixin> list) ;
    Mail_alias_mixin get(Integer key) ;
    Page<Mail_alias_mixin> searchDefault(Mail_alias_mixinSearchContext context) ;

}



