package cn.ibizlab.odoo.core.odoo_asset.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [Asset] 对象
 */
@Data
public class Asset_asset extends EntityClient implements Serializable {

    /**
     * Map
     */
    @JSONField(name = "position")
    @JsonProperty("position")
    private String position;

    /**
     * Meter
     */
    @JSONField(name = "meter_ids")
    @JsonProperty("meter_ids")
    private String meterIds;

    /**
     * 附件数量
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 开始日期
     */
    @DEField(name = "start_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "start_date" , format="yyyy-MM-dd")
    @JsonProperty("start_date")
    private Timestamp startDate;

    /**
     * Maintenance Date
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "maintenance_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("maintenance_date")
    private Timestamp maintenanceDate;

    /**
     * 图像
     */
    @JSONField(name = "image")
    @JsonProperty("image")
    private byte[] image;

    /**
     * Asset Location
     */
    @JSONField(name = "property_stock_asset")
    @JsonProperty("property_stock_asset")
    private Integer propertyStockAsset;

    /**
     * 关注者(渠道)
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 模型
     */
    @JSONField(name = "model")
    @JsonProperty("model")
    private String model;

    /**
     * Asset Number
     */
    @DEField(name = "asset_number")
    @JSONField(name = "asset_number")
    @JsonProperty("asset_number")
    private String assetNumber;

    /**
     * 未读消息
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * Serial no.
     */
    @JSONField(name = "serial")
    @JsonProperty("serial")
    private String serial;

    /**
     * 错误个数
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 消息递送错误
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 关注者(业务伙伴)
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 关注者
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 操作次数
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * Purchase Date
     */
    @DEField(name = "purchase_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "purchase_date" , format="yyyy-MM-dd")
    @JsonProperty("purchase_date")
    private Timestamp purchaseDate;

    /**
     * 中等尺寸图像
     */
    @JSONField(name = "image_medium")
    @JsonProperty("image_medium")
    private byte[] imageMedium;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 标签
     */
    @JSONField(name = "category_ids")
    @JsonProperty("category_ids")
    private String categoryIds;

    /**
     * Warranty Start
     */
    @DEField(name = "warranty_start_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "warranty_start_date" , format="yyyy-MM-dd")
    @JsonProperty("warranty_start_date")
    private Timestamp warrantyStartDate;

    /**
     * 未读消息计数器
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * Warranty End
     */
    @DEField(name = "warranty_end_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "warranty_end_date" , format="yyyy-MM-dd")
    @JsonProperty("warranty_end_date")
    private Timestamp warrantyEndDate;

    /**
     * 网站消息
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 小尺寸图像
     */
    @JSONField(name = "image_small")
    @JsonProperty("image_small")
    private byte[] imageSmall;

    /**
     * Asset Name
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * Criticality
     */
    @JSONField(name = "criticality")
    @JsonProperty("criticality")
    private String criticality;

    /**
     * 消息
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 前置操作
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * # Maintenance
     */
    @JSONField(name = "mro_count")
    @JsonProperty("mro_count")
    private Integer mroCount;

    /**
     * 关注者
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 省/ 州
     */
    @JSONField(name = "warehouse_state_id_text")
    @JsonProperty("warehouse_state_id_text")
    private String warehouseStateIdText;

    /**
     * 分派给
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 颜色
     */
    @JSONField(name = "maintenance_state_color")
    @JsonProperty("maintenance_state_color")
    private String maintenanceStateColor;

    /**
     * 省/ 州
     */
    @JSONField(name = "accounting_state_id_text")
    @JsonProperty("accounting_state_id_text")
    private String accountingStateIdText;

    /**
     * 省/ 州
     */
    @JSONField(name = "manufacture_state_id_text")
    @JsonProperty("manufacture_state_id_text")
    private String manufactureStateIdText;

    /**
     * 省/ 州
     */
    @JSONField(name = "finance_state_id_text")
    @JsonProperty("finance_state_id_text")
    private String financeStateIdText;

    /**
     * 省/ 州
     */
    @JSONField(name = "maintenance_state_id_text")
    @JsonProperty("maintenance_state_id_text")
    private String maintenanceStateIdText;

    /**
     * Manufacturer
     */
    @JSONField(name = "manufacturer_id_text")
    @JsonProperty("manufacturer_id_text")
    private String manufacturerIdText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 供应商
     */
    @JSONField(name = "vendor_id_text")
    @JsonProperty("vendor_id_text")
    private String vendorIdText;

    /**
     * 省/ 州
     */
    @DEField(name = "manufacture_state_id")
    @JSONField(name = "manufacture_state_id")
    @JsonProperty("manufacture_state_id")
    private Integer manufactureStateId;

    /**
     * 省/ 州
     */
    @DEField(name = "accounting_state_id")
    @JSONField(name = "accounting_state_id")
    @JsonProperty("accounting_state_id")
    private Integer accountingStateId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 供应商
     */
    @DEField(name = "vendor_id")
    @JSONField(name = "vendor_id")
    @JsonProperty("vendor_id")
    private Integer vendorId;

    /**
     * 省/ 州
     */
    @DEField(name = "warehouse_state_id")
    @JSONField(name = "warehouse_state_id")
    @JsonProperty("warehouse_state_id")
    private Integer warehouseStateId;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 分派给
     */
    @DEField(name = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;

    /**
     * 省/ 州
     */
    @DEField(name = "finance_state_id")
    @JSONField(name = "finance_state_id")
    @JsonProperty("finance_state_id")
    private Integer financeStateId;

    /**
     * 省/ 州
     */
    @DEField(name = "maintenance_state_id")
    @JSONField(name = "maintenance_state_id")
    @JsonProperty("maintenance_state_id")
    private Integer maintenanceStateId;

    /**
     * Manufacturer
     */
    @DEField(name = "manufacturer_id")
    @JSONField(name = "manufacturer_id")
    @JsonProperty("manufacturer_id")
    private Integer manufacturerId;


    /**
     * 
     */
    @JSONField(name = "odooaccountingstate")
    @JsonProperty("odooaccountingstate")
    private cn.ibizlab.odoo.core.odoo_asset.domain.Asset_state odooAccountingState;

    /**
     * 
     */
    @JSONField(name = "odoofinancestate")
    @JsonProperty("odoofinancestate")
    private cn.ibizlab.odoo.core.odoo_asset.domain.Asset_state odooFinanceState;

    /**
     * 
     */
    @JSONField(name = "odoomaintenancestate")
    @JsonProperty("odoomaintenancestate")
    private cn.ibizlab.odoo.core.odoo_asset.domain.Asset_state odooMaintenanceState;

    /**
     * 
     */
    @JSONField(name = "odoomanufacturestate")
    @JsonProperty("odoomanufacturestate")
    private cn.ibizlab.odoo.core.odoo_asset.domain.Asset_state odooManufactureState;

    /**
     * 
     */
    @JSONField(name = "odoowarehousestate")
    @JsonProperty("odoowarehousestate")
    private cn.ibizlab.odoo.core.odoo_asset.domain.Asset_state odooWarehouseState;

    /**
     * 
     */
    @JSONField(name = "odoomanufacturer")
    @JsonProperty("odoomanufacturer")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooManufacturer;

    /**
     * 
     */
    @JSONField(name = "odoovendor")
    @JsonProperty("odoovendor")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooVendor;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoouser")
    @JsonProperty("odoouser")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [Map]
     */
    public void setPosition(String position){
        this.position = position ;
        this.modify("position",position);
    }
    /**
     * 设置 [开始日期]
     */
    public void setStartDate(Timestamp startDate){
        this.startDate = startDate ;
        this.modify("start_date",startDate);
    }
    /**
     * 设置 [模型]
     */
    public void setModel(String model){
        this.model = model ;
        this.modify("model",model);
    }
    /**
     * 设置 [Asset Number]
     */
    public void setAssetNumber(String assetNumber){
        this.assetNumber = assetNumber ;
        this.modify("asset_number",assetNumber);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }
    /**
     * 设置 [Serial no.]
     */
    public void setSerial(String serial){
        this.serial = serial ;
        this.modify("serial",serial);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [Purchase Date]
     */
    public void setPurchaseDate(Timestamp purchaseDate){
        this.purchaseDate = purchaseDate ;
        this.modify("purchase_date",purchaseDate);
    }
    /**
     * 设置 [Warranty Start]
     */
    public void setWarrantyStartDate(Timestamp warrantyStartDate){
        this.warrantyStartDate = warrantyStartDate ;
        this.modify("warranty_start_date",warrantyStartDate);
    }
    /**
     * 设置 [Warranty End]
     */
    public void setWarrantyEndDate(Timestamp warrantyEndDate){
        this.warrantyEndDate = warrantyEndDate ;
        this.modify("warranty_end_date",warrantyEndDate);
    }
    /**
     * 设置 [Asset Name]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [Criticality]
     */
    public void setCriticality(String criticality){
        this.criticality = criticality ;
        this.modify("criticality",criticality);
    }
    /**
     * 设置 [省/ 州]
     */
    public void setManufactureStateId(Integer manufactureStateId){
        this.manufactureStateId = manufactureStateId ;
        this.modify("manufacture_state_id",manufactureStateId);
    }
    /**
     * 设置 [省/ 州]
     */
    public void setAccountingStateId(Integer accountingStateId){
        this.accountingStateId = accountingStateId ;
        this.modify("accounting_state_id",accountingStateId);
    }
    /**
     * 设置 [供应商]
     */
    public void setVendorId(Integer vendorId){
        this.vendorId = vendorId ;
        this.modify("vendor_id",vendorId);
    }
    /**
     * 设置 [省/ 州]
     */
    public void setWarehouseStateId(Integer warehouseStateId){
        this.warehouseStateId = warehouseStateId ;
        this.modify("warehouse_state_id",warehouseStateId);
    }
    /**
     * 设置 [分派给]
     */
    public void setUserId(Integer userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }
    /**
     * 设置 [省/ 州]
     */
    public void setFinanceStateId(Integer financeStateId){
        this.financeStateId = financeStateId ;
        this.modify("finance_state_id",financeStateId);
    }
    /**
     * 设置 [省/ 州]
     */
    public void setMaintenanceStateId(Integer maintenanceStateId){
        this.maintenanceStateId = maintenanceStateId ;
        this.modify("maintenance_state_id",maintenanceStateId);
    }
    /**
     * 设置 [Manufacturer]
     */
    public void setManufacturerId(Integer manufacturerId){
        this.manufacturerId = manufacturerId ;
        this.modify("manufacturer_id",manufacturerId);
    }

}


