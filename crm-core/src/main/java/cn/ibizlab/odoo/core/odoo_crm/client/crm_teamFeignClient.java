package cn.ibizlab.odoo.core.odoo_crm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_team;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_teamSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[crm_team] 服务对象接口
 */
@FeignClient(value = "odoo-crm", contextId = "crm-team", fallback = crm_teamFallback.class)
public interface crm_teamFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/crm_teams/searchdefault")
    Page<Crm_team> searchDefault(@RequestBody Crm_teamSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/crm_teams")
    Crm_team create(@RequestBody Crm_team crm_team);

    @RequestMapping(method = RequestMethod.POST, value = "/crm_teams/batch")
    Boolean createBatch(@RequestBody List<Crm_team> crm_teams);





    @RequestMapping(method = RequestMethod.GET, value = "/crm_teams/{id}")
    Crm_team get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/crm_teams/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/crm_teams/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/crm_teams/{id}")
    Crm_team update(@PathVariable("id") Integer id,@RequestBody Crm_team crm_team);

    @RequestMapping(method = RequestMethod.PUT, value = "/crm_teams/batch")
    Boolean updateBatch(@RequestBody List<Crm_team> crm_teams);


    @RequestMapping(method = RequestMethod.GET, value = "/crm_teams/select")
    Page<Crm_team> select();


    @RequestMapping(method = RequestMethod.POST, value = "/crm_teams/checkkey")
    Boolean checkKey(@RequestBody Crm_team crm_team);


    @RequestMapping(method = RequestMethod.GET, value = "/crm_teams/getdraft")
    Crm_team getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/crm_teams/save")
    Boolean save(@RequestBody Crm_team crm_team);

    @RequestMapping(method = RequestMethod.POST, value = "/crm_teams/save")
    Boolean saveBatch(@RequestBody List<Crm_team> crm_teams);


}
