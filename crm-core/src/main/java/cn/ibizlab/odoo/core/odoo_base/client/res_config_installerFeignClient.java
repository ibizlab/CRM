package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_config_installer;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_config_installerSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[res_config_installer] 服务对象接口
 */
@FeignClient(value = "odoo-base", contextId = "res-config-installer", fallback = res_config_installerFallback.class)
public interface res_config_installerFeignClient {


    @RequestMapping(method = RequestMethod.DELETE, value = "/res_config_installers/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/res_config_installers/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/res_config_installers")
    Res_config_installer create(@RequestBody Res_config_installer res_config_installer);

    @RequestMapping(method = RequestMethod.POST, value = "/res_config_installers/batch")
    Boolean createBatch(@RequestBody List<Res_config_installer> res_config_installers);



    @RequestMapping(method = RequestMethod.POST, value = "/res_config_installers/searchdefault")
    Page<Res_config_installer> searchDefault(@RequestBody Res_config_installerSearchContext context);



    @RequestMapping(method = RequestMethod.PUT, value = "/res_config_installers/{id}")
    Res_config_installer update(@PathVariable("id") Integer id,@RequestBody Res_config_installer res_config_installer);

    @RequestMapping(method = RequestMethod.PUT, value = "/res_config_installers/batch")
    Boolean updateBatch(@RequestBody List<Res_config_installer> res_config_installers);


    @RequestMapping(method = RequestMethod.GET, value = "/res_config_installers/{id}")
    Res_config_installer get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.GET, value = "/res_config_installers/select")
    Page<Res_config_installer> select();


    @RequestMapping(method = RequestMethod.GET, value = "/res_config_installers/getdraft")
    Res_config_installer getDraft();


}
