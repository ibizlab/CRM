package cn.ibizlab.odoo.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_mapping;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_mappingSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[base_import_mapping] 服务对象接口
 */
@Component
public class base_import_mappingFallback implements base_import_mappingFeignClient{

    public Base_import_mapping update(Integer id, Base_import_mapping base_import_mapping){
            return null;
     }
    public Boolean updateBatch(List<Base_import_mapping> base_import_mappings){
            return false;
     }




    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Base_import_mapping create(Base_import_mapping base_import_mapping){
            return null;
     }
    public Boolean createBatch(List<Base_import_mapping> base_import_mappings){
            return false;
     }

    public Base_import_mapping get(Integer id){
            return null;
     }


    public Page<Base_import_mapping> searchDefault(Base_import_mappingSearchContext context){
            return null;
     }


    public Page<Base_import_mapping> select(){
            return null;
     }

    public Base_import_mapping getDraft(){
            return null;
    }



}
