package cn.ibizlab.odoo.core.odoo_maintenance.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_team;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_teamSearchContext;


/**
 * 实体[Maintenance_team] 服务对象接口
 */
public interface IMaintenance_teamService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Maintenance_team et) ;
    void updateBatch(List<Maintenance_team> list) ;
    Maintenance_team getDraft(Maintenance_team et) ;
    Maintenance_team get(Integer key) ;
    boolean create(Maintenance_team et) ;
    void createBatch(List<Maintenance_team> list) ;
    Page<Maintenance_team> searchDefault(Maintenance_teamSearchContext context) ;

}



