package cn.ibizlab.odoo.core.odoo_lunch.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_alert;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_alertSearchContext;
import cn.ibizlab.odoo.core.odoo_lunch.service.ILunch_alertService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_lunch.client.lunch_alertFeignClient;

/**
 * 实体[午餐提醒] 服务对象接口实现
 */
@Slf4j
@Service
public class Lunch_alertServiceImpl implements ILunch_alertService {

    @Autowired
    lunch_alertFeignClient lunch_alertFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=lunch_alertFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        lunch_alertFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Lunch_alert et) {
        Lunch_alert rt = lunch_alertFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Lunch_alert> list){
        lunch_alertFeignClient.updateBatch(list) ;
    }

    @Override
    public Lunch_alert get(Integer id) {
		Lunch_alert et=lunch_alertFeignClient.get(id);
        if(et==null){
            et=new Lunch_alert();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Lunch_alert getDraft(Lunch_alert et) {
        et=lunch_alertFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Lunch_alert et) {
        Lunch_alert rt = lunch_alertFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Lunch_alert> list){
        lunch_alertFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Lunch_alert> searchDefault(Lunch_alertSearchContext context) {
        Page<Lunch_alert> lunch_alerts=lunch_alertFeignClient.searchDefault(context);
        return lunch_alerts;
    }


}


