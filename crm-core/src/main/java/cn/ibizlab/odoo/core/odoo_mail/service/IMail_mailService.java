package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mail;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mailSearchContext;


/**
 * 实体[Mail_mail] 服务对象接口
 */
public interface IMail_mailService{

    boolean update(Mail_mail et) ;
    void updateBatch(List<Mail_mail> list) ;
    Mail_mail getDraft(Mail_mail et) ;
    Mail_mail get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Mail_mail et) ;
    void createBatch(List<Mail_mail> list) ;
    Page<Mail_mail> searchDefault(Mail_mailSearchContext context) ;

}



