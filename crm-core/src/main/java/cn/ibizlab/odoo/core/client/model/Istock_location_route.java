package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [stock_location_route] 对象
 */
public interface Istock_location_route {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [产品类别]
     */
    public void setCateg_ids(String categ_ids);
    
    /**
     * 设置 [产品类别]
     */
    public String getCateg_ids();

    /**
     * 获取 [产品类别]脏标记
     */
    public boolean getCateg_idsDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [路线]
     */
    public void setName(String name);
    
    /**
     * 设置 [路线]
     */
    public String getName();

    /**
     * 获取 [路线]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [可应用于产品类别]
     */
    public void setProduct_categ_selectable(String product_categ_selectable);
    
    /**
     * 设置 [可应用于产品类别]
     */
    public String getProduct_categ_selectable();

    /**
     * 获取 [可应用于产品类别]脏标记
     */
    public boolean getProduct_categ_selectableDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_ids(String product_ids);
    
    /**
     * 设置 [产品]
     */
    public String getProduct_ids();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_idsDirtyFlag();
    /**
     * 获取 [可应用于产品]
     */
    public void setProduct_selectable(String product_selectable);
    
    /**
     * 设置 [可应用于产品]
     */
    public String getProduct_selectable();

    /**
     * 获取 [可应用于产品]脏标记
     */
    public boolean getProduct_selectableDirtyFlag();
    /**
     * 获取 [规则]
     */
    public void setRule_ids(String rule_ids);
    
    /**
     * 设置 [规则]
     */
    public String getRule_ids();

    /**
     * 获取 [规则]脏标记
     */
    public boolean getRule_idsDirtyFlag();
    /**
     * 获取 [在销售订单行上可选]
     */
    public void setSale_selectable(String sale_selectable);
    
    /**
     * 设置 [在销售订单行上可选]
     */
    public String getSale_selectable();

    /**
     * 获取 [在销售订单行上可选]脏标记
     */
    public boolean getSale_selectableDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [供应的仓库]
     */
    public void setSupplied_wh_id(Integer supplied_wh_id);
    
    /**
     * 设置 [供应的仓库]
     */
    public Integer getSupplied_wh_id();

    /**
     * 获取 [供应的仓库]脏标记
     */
    public boolean getSupplied_wh_idDirtyFlag();
    /**
     * 获取 [供应的仓库]
     */
    public void setSupplied_wh_id_text(String supplied_wh_id_text);
    
    /**
     * 设置 [供应的仓库]
     */
    public String getSupplied_wh_id_text();

    /**
     * 获取 [供应的仓库]脏标记
     */
    public boolean getSupplied_wh_id_textDirtyFlag();
    /**
     * 获取 [供应仓库]
     */
    public void setSupplier_wh_id(Integer supplier_wh_id);
    
    /**
     * 设置 [供应仓库]
     */
    public Integer getSupplier_wh_id();

    /**
     * 获取 [供应仓库]脏标记
     */
    public boolean getSupplier_wh_idDirtyFlag();
    /**
     * 获取 [供应仓库]
     */
    public void setSupplier_wh_id_text(String supplier_wh_id_text);
    
    /**
     * 设置 [供应仓库]
     */
    public String getSupplier_wh_id_text();

    /**
     * 获取 [供应仓库]脏标记
     */
    public boolean getSupplier_wh_id_textDirtyFlag();
    /**
     * 获取 [仓库]
     */
    public void setWarehouse_ids(String warehouse_ids);
    
    /**
     * 设置 [仓库]
     */
    public String getWarehouse_ids();

    /**
     * 获取 [仓库]脏标记
     */
    public boolean getWarehouse_idsDirtyFlag();
    /**
     * 获取 [可应用于仓库]
     */
    public void setWarehouse_selectable(String warehouse_selectable);
    
    /**
     * 设置 [可应用于仓库]
     */
    public String getWarehouse_selectable();

    /**
     * 获取 [可应用于仓库]脏标记
     */
    public boolean getWarehouse_selectableDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
