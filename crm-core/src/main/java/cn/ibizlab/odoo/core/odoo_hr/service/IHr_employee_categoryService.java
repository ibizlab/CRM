package cn.ibizlab.odoo.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_employee_category;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_employee_categorySearchContext;


/**
 * 实体[Hr_employee_category] 服务对象接口
 */
public interface IHr_employee_categoryService{

    boolean create(Hr_employee_category et) ;
    void createBatch(List<Hr_employee_category> list) ;
    Hr_employee_category getDraft(Hr_employee_category et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Hr_employee_category et) ;
    void updateBatch(List<Hr_employee_category> list) ;
    Hr_employee_category get(Integer key) ;
    Page<Hr_employee_category> searchDefault(Hr_employee_categorySearchContext context) ;

}



