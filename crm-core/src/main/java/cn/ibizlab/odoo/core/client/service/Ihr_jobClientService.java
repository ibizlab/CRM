package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ihr_job;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_job] 服务对象接口
 */
public interface Ihr_jobClientService{

    public Ihr_job createModel() ;

    public void update(Ihr_job hr_job);

    public void createBatch(List<Ihr_job> hr_jobs);

    public void remove(Ihr_job hr_job);

    public void removeBatch(List<Ihr_job> hr_jobs);

    public void create(Ihr_job hr_job);

    public Page<Ihr_job> fetchDefault(SearchContext context);

    public void updateBatch(List<Ihr_job> hr_jobs);

    public void get(Ihr_job hr_job);

    public Page<Ihr_job> select(SearchContext context);

    public void getDraft(Ihr_job hr_job);

}
