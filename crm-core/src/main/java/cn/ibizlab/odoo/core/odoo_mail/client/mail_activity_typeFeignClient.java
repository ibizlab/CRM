package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity_type;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_activity_typeSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_activity_type] 服务对象接口
 */
@FeignClient(value = "odoo-mail", contextId = "mail-activity-type", fallback = mail_activity_typeFallback.class)
public interface mail_activity_typeFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/mail_activity_types/searchdefault")
    Page<Mail_activity_type> searchDefault(@RequestBody Mail_activity_typeSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/mail_activity_types/{id}")
    Mail_activity_type update(@PathVariable("id") Integer id,@RequestBody Mail_activity_type mail_activity_type);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_activity_types/batch")
    Boolean updateBatch(@RequestBody List<Mail_activity_type> mail_activity_types);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_activity_types")
    Mail_activity_type create(@RequestBody Mail_activity_type mail_activity_type);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_activity_types/batch")
    Boolean createBatch(@RequestBody List<Mail_activity_type> mail_activity_types);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_activity_types/{id}")
    Mail_activity_type get(@PathVariable("id") Integer id);




    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_activity_types/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_activity_types/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/mail_activity_types/select")
    Page<Mail_activity_type> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_activity_types/getdraft")
    Mail_activity_type getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mail_activity_types/checkkey")
    Boolean checkKey(@RequestBody Mail_activity_type mail_activity_type);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_activity_types/save")
    Boolean save(@RequestBody Mail_activity_type mail_activity_type);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_activity_types/save")
    Boolean saveBatch(@RequestBody List<Mail_activity_type> mail_activity_types);


}
