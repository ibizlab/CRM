package cn.ibizlab.odoo.core.odoo_base.valuerule.anno.base_partner_merge_line;

import cn.ibizlab.odoo.core.odoo_base.valuerule.validator.base_partner_merge_line.Base_partner_merge_lineCreate_uid_textDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Base_partner_merge_line
 * 属性：Create_uid_text
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Base_partner_merge_lineCreate_uid_textDefaultValidator.class})
public @interface Base_partner_merge_lineCreate_uid_textDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
