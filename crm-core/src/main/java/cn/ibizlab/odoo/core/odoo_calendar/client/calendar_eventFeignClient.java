package cn.ibizlab.odoo.core.odoo_calendar.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_event;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_eventSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[calendar_event] 服务对象接口
 */
@FeignClient(value = "odoo-calendar", contextId = "calendar-event", fallback = calendar_eventFallback.class)
public interface calendar_eventFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/calendar_events/{id}")
    Calendar_event get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/calendar_events/searchdefault")
    Page<Calendar_event> searchDefault(@RequestBody Calendar_eventSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/calendar_events/{id}")
    Calendar_event update(@PathVariable("id") Integer id,@RequestBody Calendar_event calendar_event);

    @RequestMapping(method = RequestMethod.PUT, value = "/calendar_events/batch")
    Boolean updateBatch(@RequestBody List<Calendar_event> calendar_events);





    @RequestMapping(method = RequestMethod.POST, value = "/calendar_events")
    Calendar_event create(@RequestBody Calendar_event calendar_event);

    @RequestMapping(method = RequestMethod.POST, value = "/calendar_events/batch")
    Boolean createBatch(@RequestBody List<Calendar_event> calendar_events);


    @RequestMapping(method = RequestMethod.DELETE, value = "/calendar_events/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/calendar_events/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/calendar_events/select")
    Page<Calendar_event> select();


    @RequestMapping(method = RequestMethod.GET, value = "/calendar_events/getdraft")
    Calendar_event getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/calendar_events/save")
    Boolean save(@RequestBody Calendar_event calendar_event);

    @RequestMapping(method = RequestMethod.POST, value = "/calendar_events/save")
    Boolean saveBatch(@RequestBody List<Calendar_event> calendar_events);


    @RequestMapping(method = RequestMethod.POST, value = "/calendar_events/checkkey")
    Boolean checkKey(@RequestBody Calendar_event calendar_event);


}
