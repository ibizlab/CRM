package cn.ibizlab.odoo.core.odoo_lunch.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_order_line;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_order_lineSearchContext;


/**
 * 实体[Lunch_order_line] 服务对象接口
 */
public interface ILunch_order_lineService{

    boolean update(Lunch_order_line et) ;
    void updateBatch(List<Lunch_order_line> list) ;
    Lunch_order_line get(Integer key) ;
    Lunch_order_line getDraft(Lunch_order_line et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Lunch_order_line et) ;
    void createBatch(List<Lunch_order_line> list) ;
    Page<Lunch_order_line> searchDefault(Lunch_order_lineSearchContext context) ;

}



