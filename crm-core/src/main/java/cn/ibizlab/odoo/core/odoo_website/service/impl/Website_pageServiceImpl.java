package cn.ibizlab.odoo.core.odoo_website.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_page;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_pageSearchContext;
import cn.ibizlab.odoo.core.odoo_website.service.IWebsite_pageService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_website.client.website_pageFeignClient;

/**
 * 实体[页] 服务对象接口实现
 */
@Slf4j
@Service
public class Website_pageServiceImpl implements IWebsite_pageService {

    @Autowired
    website_pageFeignClient website_pageFeignClient;


    @Override
    public boolean update(Website_page et) {
        Website_page rt = website_pageFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Website_page> list){
        website_pageFeignClient.updateBatch(list) ;
    }

    @Override
    public Website_page get(Integer id) {
		Website_page et=website_pageFeignClient.get(id);
        if(et==null){
            et=new Website_page();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=website_pageFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        website_pageFeignClient.removeBatch(idList);
    }

    @Override
    public Website_page getDraft(Website_page et) {
        et=website_pageFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Website_page et) {
        Website_page rt = website_pageFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Website_page> list){
        website_pageFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Website_page> searchDefault(Website_pageSearchContext context) {
        Page<Website_page> website_pages=website_pageFeignClient.searchDefault(context);
        return website_pages;
    }


}


