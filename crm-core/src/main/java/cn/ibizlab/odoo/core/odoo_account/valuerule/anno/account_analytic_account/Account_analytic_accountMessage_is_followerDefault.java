package cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_analytic_account;

import cn.ibizlab.odoo.core.odoo_account.valuerule.validator.account_analytic_account.Account_analytic_accountMessage_is_followerDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Account_analytic_account
 * 属性：Message_is_follower
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Account_analytic_accountMessage_is_followerDefaultValidator.class})
public @interface Account_analytic_accountMessage_is_followerDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
