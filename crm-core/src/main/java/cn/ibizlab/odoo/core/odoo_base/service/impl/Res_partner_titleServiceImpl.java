package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_title;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_titleSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_partner_titleService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.res_partner_titleFeignClient;

/**
 * 实体[业务伙伴称谓] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_partner_titleServiceImpl implements IRes_partner_titleService {

    @Autowired
    res_partner_titleFeignClient res_partner_titleFeignClient;


    @Override
    public Res_partner_title getDraft(Res_partner_title et) {
        et=res_partner_titleFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=res_partner_titleFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        res_partner_titleFeignClient.removeBatch(idList);
    }

    @Override
    public Res_partner_title get(Integer id) {
		Res_partner_title et=res_partner_titleFeignClient.get(id);
        if(et==null){
            et=new Res_partner_title();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Res_partner_title et) {
        Res_partner_title rt = res_partner_titleFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Res_partner_title> list){
        res_partner_titleFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Res_partner_title et) {
        Res_partner_title rt = res_partner_titleFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_partner_title> list){
        res_partner_titleFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_partner_title> searchDefault(Res_partner_titleSearchContext context) {
        Page<Res_partner_title> res_partner_titles=res_partner_titleFeignClient.searchDefault(context);
        return res_partner_titles;
    }


}


