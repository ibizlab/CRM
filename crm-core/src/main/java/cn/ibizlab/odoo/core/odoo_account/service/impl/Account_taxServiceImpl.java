package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_tax;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_taxSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_taxService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_taxFeignClient;

/**
 * 实体[税率] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_taxServiceImpl implements IAccount_taxService {

    @Autowired
    account_taxFeignClient account_taxFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=account_taxFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_taxFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Account_tax et) {
        Account_tax rt = account_taxFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_tax> list){
        account_taxFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Account_tax et) {
        Account_tax rt = account_taxFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_tax> list){
        account_taxFeignClient.createBatch(list) ;
    }

    @Override
    public Account_tax get(Integer id) {
		Account_tax et=account_taxFeignClient.get(id);
        if(et==null){
            et=new Account_tax();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Account_tax getDraft(Account_tax et) {
        et=account_taxFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_tax> searchDefault(Account_taxSearchContext context) {
        Page<Account_tax> account_taxs=account_taxFeignClient.searchDefault(context);
        return account_taxs;
    }


}


