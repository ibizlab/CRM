package cn.ibizlab.odoo.core.odoo_web_editor.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_web_editor.domain.Web_editor_converter_test_sub;
import cn.ibizlab.odoo.core.odoo_web_editor.filter.Web_editor_converter_test_subSearchContext;
import cn.ibizlab.odoo.core.odoo_web_editor.service.IWeb_editor_converter_test_subService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_web_editor.client.web_editor_converter_test_subFeignClient;

/**
 * 实体[Web编辑器转换器子测试] 服务对象接口实现
 */
@Slf4j
@Service
public class Web_editor_converter_test_subServiceImpl implements IWeb_editor_converter_test_subService {

    @Autowired
    web_editor_converter_test_subFeignClient web_editor_converter_test_subFeignClient;


    @Override
    public boolean update(Web_editor_converter_test_sub et) {
        Web_editor_converter_test_sub rt = web_editor_converter_test_subFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Web_editor_converter_test_sub> list){
        web_editor_converter_test_subFeignClient.updateBatch(list) ;
    }

    @Override
    public Web_editor_converter_test_sub getDraft(Web_editor_converter_test_sub et) {
        et=web_editor_converter_test_subFeignClient.getDraft();
        return et;
    }

    @Override
    public Web_editor_converter_test_sub get(Integer id) {
		Web_editor_converter_test_sub et=web_editor_converter_test_subFeignClient.get(id);
        if(et==null){
            et=new Web_editor_converter_test_sub();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Web_editor_converter_test_sub et) {
        Web_editor_converter_test_sub rt = web_editor_converter_test_subFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Web_editor_converter_test_sub> list){
        web_editor_converter_test_subFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=web_editor_converter_test_subFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        web_editor_converter_test_subFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Web_editor_converter_test_sub> searchDefault(Web_editor_converter_test_subSearchContext context) {
        Page<Web_editor_converter_test_sub> web_editor_converter_test_subs=web_editor_converter_test_subFeignClient.searchDefault(context);
        return web_editor_converter_test_subs;
    }


}


