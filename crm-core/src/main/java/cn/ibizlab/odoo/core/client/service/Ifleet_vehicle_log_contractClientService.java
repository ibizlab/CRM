package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_log_contract;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[fleet_vehicle_log_contract] 服务对象接口
 */
public interface Ifleet_vehicle_log_contractClientService{

    public Ifleet_vehicle_log_contract createModel() ;

    public void remove(Ifleet_vehicle_log_contract fleet_vehicle_log_contract);

    public Page<Ifleet_vehicle_log_contract> fetchDefault(SearchContext context);

    public void update(Ifleet_vehicle_log_contract fleet_vehicle_log_contract);

    public void get(Ifleet_vehicle_log_contract fleet_vehicle_log_contract);

    public void updateBatch(List<Ifleet_vehicle_log_contract> fleet_vehicle_log_contracts);

    public void createBatch(List<Ifleet_vehicle_log_contract> fleet_vehicle_log_contracts);

    public void create(Ifleet_vehicle_log_contract fleet_vehicle_log_contract);

    public void removeBatch(List<Ifleet_vehicle_log_contract> fleet_vehicle_log_contracts);

    public Page<Ifleet_vehicle_log_contract> select(SearchContext context);

    public void getDraft(Ifleet_vehicle_log_contract fleet_vehicle_log_contract);

}
