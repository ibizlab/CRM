package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imrp_routing;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mrp_routing] 服务对象接口
 */
public interface Imrp_routingClientService{

    public Imrp_routing createModel() ;

    public void update(Imrp_routing mrp_routing);

    public void get(Imrp_routing mrp_routing);

    public void create(Imrp_routing mrp_routing);

    public Page<Imrp_routing> fetchDefault(SearchContext context);

    public void createBatch(List<Imrp_routing> mrp_routings);

    public void removeBatch(List<Imrp_routing> mrp_routings);

    public void remove(Imrp_routing mrp_routing);

    public void updateBatch(List<Imrp_routing> mrp_routings);

    public Page<Imrp_routing> select(SearchContext context);

    public void getDraft(Imrp_routing mrp_routing);

}
