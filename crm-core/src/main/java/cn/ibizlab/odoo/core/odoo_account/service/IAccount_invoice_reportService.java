package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_report;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_reportSearchContext;


/**
 * 实体[Account_invoice_report] 服务对象接口
 */
public interface IAccount_invoice_reportService{

    Account_invoice_report get(Integer key) ;
    boolean create(Account_invoice_report et) ;
    void createBatch(List<Account_invoice_report> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Account_invoice_report et) ;
    void updateBatch(List<Account_invoice_report> list) ;
    Account_invoice_report getDraft(Account_invoice_report et) ;
    Page<Account_invoice_report> searchDefault(Account_invoice_reportSearchContext context) ;

}



