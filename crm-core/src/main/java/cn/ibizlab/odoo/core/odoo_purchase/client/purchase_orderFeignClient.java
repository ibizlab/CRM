package cn.ibizlab.odoo.core.odoo_purchase.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_order;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_orderSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[purchase_order] 服务对象接口
 */
@FeignClient(value = "odoo-purchase", contextId = "purchase-order", fallback = purchase_orderFallback.class)
public interface purchase_orderFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/purchase_orders/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/purchase_orders/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/searchdefault")
    Page<Purchase_order> searchDefault(@RequestBody Purchase_orderSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/purchase_orders/{id}")
    Purchase_order update(@PathVariable("id") Integer id,@RequestBody Purchase_order purchase_order);

    @RequestMapping(method = RequestMethod.PUT, value = "/purchase_orders/batch")
    Boolean updateBatch(@RequestBody List<Purchase_order> purchase_orders);





    @RequestMapping(method = RequestMethod.GET, value = "/purchase_orders/{id}")
    Purchase_order get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/purchase_orders")
    Purchase_order create(@RequestBody Purchase_order purchase_order);

    @RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/batch")
    Boolean createBatch(@RequestBody List<Purchase_order> purchase_orders);


    @RequestMapping(method = RequestMethod.GET, value = "/purchase_orders/select")
    Page<Purchase_order> select();


    @RequestMapping(method = RequestMethod.GET, value = "/purchase_orders/getdraft")
    Purchase_order getDraft();


}
