package cn.ibizlab.odoo.core.odoo_sale.valuerule.anno.sale_product_configurator;

import cn.ibizlab.odoo.core.odoo_sale.valuerule.validator.sale_product_configurator.Sale_product_configuratorProduct_template_idDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Sale_product_configurator
 * 属性：Product_template_id
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Sale_product_configuratorProduct_template_idDefaultValidator.class})
public @interface Sale_product_configuratorProduct_template_idDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
