package cn.ibizlab.odoo.core.odoo_asset.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_asset.domain.Asset_category;
import cn.ibizlab.odoo.core.odoo_asset.filter.Asset_categorySearchContext;


/**
 * 实体[Asset_category] 服务对象接口
 */
public interface IAsset_categoryService{

    Asset_category get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Asset_category getDraft(Asset_category et) ;
    boolean create(Asset_category et) ;
    void createBatch(List<Asset_category> list) ;
    boolean update(Asset_category et) ;
    void updateBatch(List<Asset_category> list) ;
    Page<Asset_category> searchDefault(Asset_categorySearchContext context) ;

}



