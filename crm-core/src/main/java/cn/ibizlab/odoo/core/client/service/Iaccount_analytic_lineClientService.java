package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_analytic_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_analytic_line] 服务对象接口
 */
public interface Iaccount_analytic_lineClientService{

    public Iaccount_analytic_line createModel() ;

    public void get(Iaccount_analytic_line account_analytic_line);

    public void create(Iaccount_analytic_line account_analytic_line);

    public void removeBatch(List<Iaccount_analytic_line> account_analytic_lines);

    public void createBatch(List<Iaccount_analytic_line> account_analytic_lines);

    public void remove(Iaccount_analytic_line account_analytic_line);

    public void updateBatch(List<Iaccount_analytic_line> account_analytic_lines);

    public void update(Iaccount_analytic_line account_analytic_line);

    public Page<Iaccount_analytic_line> fetchDefault(SearchContext context);

    public Page<Iaccount_analytic_line> select(SearchContext context);

    public void getDraft(Iaccount_analytic_line account_analytic_line);

}
