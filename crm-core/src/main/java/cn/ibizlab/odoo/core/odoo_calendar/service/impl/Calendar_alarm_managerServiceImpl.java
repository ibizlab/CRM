package cn.ibizlab.odoo.core.odoo_calendar.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_alarm_manager;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_alarm_managerSearchContext;
import cn.ibizlab.odoo.core.odoo_calendar.service.ICalendar_alarm_managerService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_calendar.client.calendar_alarm_managerFeignClient;

/**
 * 实体[活动提醒管理] 服务对象接口实现
 */
@Slf4j
@Service
public class Calendar_alarm_managerServiceImpl implements ICalendar_alarm_managerService {

    @Autowired
    calendar_alarm_managerFeignClient calendar_alarm_managerFeignClient;


    @Override
    public Calendar_alarm_manager getDraft(Calendar_alarm_manager et) {
        et=calendar_alarm_managerFeignClient.getDraft();
        return et;
    }

    @Override
    public Calendar_alarm_manager get(Integer id) {
		Calendar_alarm_manager et=calendar_alarm_managerFeignClient.get(id);
        if(et==null){
            et=new Calendar_alarm_manager();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Calendar_alarm_manager et) {
        Calendar_alarm_manager rt = calendar_alarm_managerFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Calendar_alarm_manager> list){
        calendar_alarm_managerFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=calendar_alarm_managerFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        calendar_alarm_managerFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Calendar_alarm_manager et) {
        Calendar_alarm_manager rt = calendar_alarm_managerFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Calendar_alarm_manager> list){
        calendar_alarm_managerFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Calendar_alarm_manager> searchDefault(Calendar_alarm_managerSearchContext context) {
        Page<Calendar_alarm_manager> calendar_alarm_managers=calendar_alarm_managerFeignClient.searchDefault(context);
        return calendar_alarm_managers;
    }


}


