package cn.ibizlab.odoo.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_category;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_categorySearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[res_partner_category] 服务对象接口
 */
@Component
public class res_partner_categoryFallback implements res_partner_categoryFeignClient{

    public Res_partner_category get(Integer id){
            return null;
     }




    public Page<Res_partner_category> searchDefault(Res_partner_categorySearchContext context){
            return null;
     }


    public Res_partner_category update(Integer id, Res_partner_category res_partner_category){
            return null;
     }
    public Boolean updateBatch(List<Res_partner_category> res_partner_categories){
            return false;
     }



    public Res_partner_category create(Res_partner_category res_partner_category){
            return null;
     }
    public Boolean createBatch(List<Res_partner_category> res_partner_categories){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Res_partner_category> select(){
            return null;
     }

    public Res_partner_category getDraft(){
            return null;
    }



}
