package cn.ibizlab.odoo.core.odoo_website.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_website.domain.Website_published_multi_mixin;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_published_multi_mixinSearchContext;


/**
 * 实体[Website_published_multi_mixin] 服务对象接口
 */
public interface IWebsite_published_multi_mixinService{

    boolean create(Website_published_multi_mixin et) ;
    void createBatch(List<Website_published_multi_mixin> list) ;
    Website_published_multi_mixin get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Website_published_multi_mixin getDraft(Website_published_multi_mixin et) ;
    boolean update(Website_published_multi_mixin et) ;
    void updateBatch(List<Website_published_multi_mixin> list) ;
    Page<Website_published_multi_mixin> searchDefault(Website_published_multi_mixinSearchContext context) ;

}



