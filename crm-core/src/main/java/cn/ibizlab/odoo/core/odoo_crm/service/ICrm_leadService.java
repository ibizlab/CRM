package cn.ibizlab.odoo.core.odoo_crm.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_leadSearchContext;


/**
 * 实体[Crm_lead] 服务对象接口
 */
public interface ICrm_leadService{

    Crm_lead getDraft(Crm_lead et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean save(Crm_lead et) ;
    void saveBatch(List<Crm_lead> list) ;
    Crm_lead get(Integer key) ;
    boolean create(Crm_lead et) ;
    void createBatch(List<Crm_lead> list) ;
    boolean checkKey(Crm_lead et) ;
    boolean update(Crm_lead et) ;
    void updateBatch(List<Crm_lead> list) ;
    Page<Crm_lead> searchDefault(Crm_leadSearchContext context) ;
    Page<Crm_lead> searchDefault2(Crm_leadSearchContext context) ;

}



