package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_account_tag;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_account_tagSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_account_tag] 服务对象接口
 */
@Component
public class account_account_tagFallback implements account_account_tagFeignClient{


    public Account_account_tag create(Account_account_tag account_account_tag){
            return null;
     }
    public Boolean createBatch(List<Account_account_tag> account_account_tags){
            return false;
     }



    public Page<Account_account_tag> searchDefault(Account_account_tagSearchContext context){
            return null;
     }


    public Account_account_tag update(Integer id, Account_account_tag account_account_tag){
            return null;
     }
    public Boolean updateBatch(List<Account_account_tag> account_account_tags){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Account_account_tag get(Integer id){
            return null;
     }


    public Page<Account_account_tag> select(){
            return null;
     }

    public Account_account_tag getDraft(){
            return null;
    }



}
