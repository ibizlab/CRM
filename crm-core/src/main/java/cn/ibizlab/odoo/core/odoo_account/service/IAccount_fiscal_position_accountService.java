package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position_account;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_position_accountSearchContext;


/**
 * 实体[Account_fiscal_position_account] 服务对象接口
 */
public interface IAccount_fiscal_position_accountService{

    boolean update(Account_fiscal_position_account et) ;
    void updateBatch(List<Account_fiscal_position_account> list) ;
    Account_fiscal_position_account getDraft(Account_fiscal_position_account et) ;
    Account_fiscal_position_account get(Integer key) ;
    boolean create(Account_fiscal_position_account et) ;
    void createBatch(List<Account_fiscal_position_account> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Account_fiscal_position_account> searchDefault(Account_fiscal_position_accountSearchContext context) ;

}



