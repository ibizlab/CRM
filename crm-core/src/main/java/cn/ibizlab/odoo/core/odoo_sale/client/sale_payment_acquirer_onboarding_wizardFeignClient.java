package cn.ibizlab.odoo.core.odoo_sale.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_payment_acquirer_onboarding_wizardSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[sale_payment_acquirer_onboarding_wizard] 服务对象接口
 */
@FeignClient(value = "odoo-sale", contextId = "sale-payment-acquirer-onboarding-wizard", fallback = sale_payment_acquirer_onboarding_wizardFallback.class)
public interface sale_payment_acquirer_onboarding_wizardFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/sale_payment_acquirer_onboarding_wizards/{id}")
    Sale_payment_acquirer_onboarding_wizard update(@PathVariable("id") Integer id,@RequestBody Sale_payment_acquirer_onboarding_wizard sale_payment_acquirer_onboarding_wizard);

    @RequestMapping(method = RequestMethod.PUT, value = "/sale_payment_acquirer_onboarding_wizards/batch")
    Boolean updateBatch(@RequestBody List<Sale_payment_acquirer_onboarding_wizard> sale_payment_acquirer_onboarding_wizards);



    @RequestMapping(method = RequestMethod.DELETE, value = "/sale_payment_acquirer_onboarding_wizards/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/sale_payment_acquirer_onboarding_wizards/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/sale_payment_acquirer_onboarding_wizards/{id}")
    Sale_payment_acquirer_onboarding_wizard get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/sale_payment_acquirer_onboarding_wizards/searchdefault")
    Page<Sale_payment_acquirer_onboarding_wizard> searchDefault(@RequestBody Sale_payment_acquirer_onboarding_wizardSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/sale_payment_acquirer_onboarding_wizards")
    Sale_payment_acquirer_onboarding_wizard create(@RequestBody Sale_payment_acquirer_onboarding_wizard sale_payment_acquirer_onboarding_wizard);

    @RequestMapping(method = RequestMethod.POST, value = "/sale_payment_acquirer_onboarding_wizards/batch")
    Boolean createBatch(@RequestBody List<Sale_payment_acquirer_onboarding_wizard> sale_payment_acquirer_onboarding_wizards);



    @RequestMapping(method = RequestMethod.GET, value = "/sale_payment_acquirer_onboarding_wizards/select")
    Page<Sale_payment_acquirer_onboarding_wizard> select();


    @RequestMapping(method = RequestMethod.GET, value = "/sale_payment_acquirer_onboarding_wizards/getdraft")
    Sale_payment_acquirer_onboarding_wizard getDraft();


}
