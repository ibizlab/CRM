package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_channel;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_channelSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_channel] 服务对象接口
 */
@Component
public class mail_channelFallback implements mail_channelFeignClient{


    public Mail_channel create(Mail_channel mail_channel){
            return null;
     }
    public Boolean createBatch(List<Mail_channel> mail_channels){
            return false;
     }

    public Page<Mail_channel> searchDefault(Mail_channelSearchContext context){
            return null;
     }


    public Mail_channel get(Integer id){
            return null;
     }




    public Mail_channel update(Integer id, Mail_channel mail_channel){
            return null;
     }
    public Boolean updateBatch(List<Mail_channel> mail_channels){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Mail_channel> select(){
            return null;
     }

    public Mail_channel getDraft(){
            return null;
    }



}
