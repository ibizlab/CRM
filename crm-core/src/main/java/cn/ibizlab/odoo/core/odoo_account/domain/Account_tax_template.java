package cn.ibizlab.odoo.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [税率模板] 对象
 */
@Data
public class Account_tax_template extends EntityClient implements Serializable {

    /**
     * 序号
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 显示在发票上
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 影响后续税收
     */
    @DEField(name = "include_base_amount")
    @JSONField(name = "include_base_amount")
    @JsonProperty("include_base_amount")
    private String includeBaseAmount;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 应有税金
     */
    @DEField(name = "tax_exigibility")
    @JSONField(name = "tax_exigibility")
    @JsonProperty("tax_exigibility")
    private String taxExigibility;

    /**
     * 税率名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 包含在价格中
     */
    @DEField(name = "price_include")
    @JSONField(name = "price_include")
    @JsonProperty("price_include")
    private String priceInclude;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 科目标签
     */
    @JSONField(name = "tag_ids")
    @JsonProperty("tag_ids")
    private String tagIds;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 税率计算
     */
    @DEField(name = "amount_type")
    @JSONField(name = "amount_type")
    @JsonProperty("amount_type")
    private String amountType;

    /**
     * 税范围
     */
    @DEField(name = "type_tax_use")
    @JSONField(name = "type_tax_use")
    @JsonProperty("type_tax_use")
    private String typeTaxUse;

    /**
     * 金额
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;

    /**
     * 分析成本
     */
    @JSONField(name = "analytic")
    @JsonProperty("analytic")
    private String analytic;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 子级税
     */
    @JSONField(name = "children_tax_ids")
    @JsonProperty("children_tax_ids")
    private String childrenTaxIds;

    /**
     * 税应收科目
     */
    @JSONField(name = "cash_basis_account_id_text")
    @JsonProperty("cash_basis_account_id_text")
    private String cashBasisAccountIdText;

    /**
     * 表模板
     */
    @JSONField(name = "chart_template_id_text")
    @JsonProperty("chart_template_id_text")
    private String chartTemplateIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 基本税应收科目
     */
    @JSONField(name = "cash_basis_base_account_id_text")
    @JsonProperty("cash_basis_base_account_id_text")
    private String cashBasisBaseAccountIdText;

    /**
     * 税组
     */
    @JSONField(name = "tax_group_id_text")
    @JsonProperty("tax_group_id_text")
    private String taxGroupIdText;

    /**
     * 税率科目
     */
    @JSONField(name = "account_id_text")
    @JsonProperty("account_id_text")
    private String accountIdText;

    /**
     * 退款的税金科目
     */
    @JSONField(name = "refund_account_id_text")
    @JsonProperty("refund_account_id_text")
    private String refundAccountIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 表模板
     */
    @DEField(name = "chart_template_id")
    @JSONField(name = "chart_template_id")
    @JsonProperty("chart_template_id")
    private Integer chartTemplateId;

    /**
     * 退款的税金科目
     */
    @DEField(name = "refund_account_id")
    @JSONField(name = "refund_account_id")
    @JsonProperty("refund_account_id")
    private Integer refundAccountId;

    /**
     * 税应收科目
     */
    @DEField(name = "cash_basis_account_id")
    @JSONField(name = "cash_basis_account_id")
    @JsonProperty("cash_basis_account_id")
    private Integer cashBasisAccountId;

    /**
     * 税率科目
     */
    @DEField(name = "account_id")
    @JSONField(name = "account_id")
    @JsonProperty("account_id")
    private Integer accountId;

    /**
     * 基本税应收科目
     */
    @DEField(name = "cash_basis_base_account_id")
    @JSONField(name = "cash_basis_base_account_id")
    @JsonProperty("cash_basis_base_account_id")
    private Integer cashBasisBaseAccountId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 税组
     */
    @DEField(name = "tax_group_id")
    @JSONField(name = "tax_group_id")
    @JsonProperty("tax_group_id")
    private Integer taxGroupId;


    /**
     * 
     */
    @JSONField(name = "odooaccount")
    @JsonProperty("odooaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account_template odooAccount;

    /**
     * 
     */
    @JSONField(name = "odoocashbasisaccount")
    @JsonProperty("odoocashbasisaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account_template odooCashBasisAccount;

    /**
     * 
     */
    @JSONField(name = "odoocashbasisbaseaccount")
    @JsonProperty("odoocashbasisbaseaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account_template odooCashBasisBaseAccount;

    /**
     * 
     */
    @JSONField(name = "odoorefundaccount")
    @JsonProperty("odoorefundaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account_template odooRefundAccount;

    /**
     * 
     */
    @JSONField(name = "odoocharttemplate")
    @JsonProperty("odoocharttemplate")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_chart_template odooChartTemplate;

    /**
     * 
     */
    @JSONField(name = "odootaxgroup")
    @JsonProperty("odootaxgroup")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_tax_group odooTaxGroup;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }
    /**
     * 设置 [显示在发票上]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }
    /**
     * 设置 [影响后续税收]
     */
    public void setIncludeBaseAmount(String includeBaseAmount){
        this.includeBaseAmount = includeBaseAmount ;
        this.modify("include_base_amount",includeBaseAmount);
    }
    /**
     * 设置 [应有税金]
     */
    public void setTaxExigibility(String taxExigibility){
        this.taxExigibility = taxExigibility ;
        this.modify("tax_exigibility",taxExigibility);
    }
    /**
     * 设置 [税率名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [包含在价格中]
     */
    public void setPriceInclude(String priceInclude){
        this.priceInclude = priceInclude ;
        this.modify("price_include",priceInclude);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [税率计算]
     */
    public void setAmountType(String amountType){
        this.amountType = amountType ;
        this.modify("amount_type",amountType);
    }
    /**
     * 设置 [税范围]
     */
    public void setTypeTaxUse(String typeTaxUse){
        this.typeTaxUse = typeTaxUse ;
        this.modify("type_tax_use",typeTaxUse);
    }
    /**
     * 设置 [金额]
     */
    public void setAmount(Double amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }
    /**
     * 设置 [分析成本]
     */
    public void setAnalytic(String analytic){
        this.analytic = analytic ;
        this.modify("analytic",analytic);
    }
    /**
     * 设置 [表模板]
     */
    public void setChartTemplateId(Integer chartTemplateId){
        this.chartTemplateId = chartTemplateId ;
        this.modify("chart_template_id",chartTemplateId);
    }
    /**
     * 设置 [退款的税金科目]
     */
    public void setRefundAccountId(Integer refundAccountId){
        this.refundAccountId = refundAccountId ;
        this.modify("refund_account_id",refundAccountId);
    }
    /**
     * 设置 [税应收科目]
     */
    public void setCashBasisAccountId(Integer cashBasisAccountId){
        this.cashBasisAccountId = cashBasisAccountId ;
        this.modify("cash_basis_account_id",cashBasisAccountId);
    }
    /**
     * 设置 [税率科目]
     */
    public void setAccountId(Integer accountId){
        this.accountId = accountId ;
        this.modify("account_id",accountId);
    }
    /**
     * 设置 [基本税应收科目]
     */
    public void setCashBasisBaseAccountId(Integer cashBasisBaseAccountId){
        this.cashBasisBaseAccountId = cashBasisBaseAccountId ;
        this.modify("cash_basis_base_account_id",cashBasisBaseAccountId);
    }
    /**
     * 设置 [税组]
     */
    public void setTaxGroupId(Integer taxGroupId){
        this.taxGroupId = taxGroupId ;
        this.modify("tax_group_id",taxGroupId);
    }

}


