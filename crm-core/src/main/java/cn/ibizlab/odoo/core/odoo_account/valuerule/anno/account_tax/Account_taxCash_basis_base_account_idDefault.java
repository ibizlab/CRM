package cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_tax;

import cn.ibizlab.odoo.core.odoo_account.valuerule.validator.account_tax.Account_taxCash_basis_base_account_idDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Account_tax
 * 属性：Cash_basis_base_account_id
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Account_taxCash_basis_base_account_idDefaultValidator.class})
public @interface Account_taxCash_basis_base_account_idDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
