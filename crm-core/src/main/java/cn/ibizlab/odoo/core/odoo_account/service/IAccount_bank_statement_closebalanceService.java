package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_closebalance;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_closebalanceSearchContext;


/**
 * 实体[Account_bank_statement_closebalance] 服务对象接口
 */
public interface IAccount_bank_statement_closebalanceService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Account_bank_statement_closebalance getDraft(Account_bank_statement_closebalance et) ;
    boolean create(Account_bank_statement_closebalance et) ;
    void createBatch(List<Account_bank_statement_closebalance> list) ;
    Account_bank_statement_closebalance get(Integer key) ;
    boolean update(Account_bank_statement_closebalance et) ;
    void updateBatch(List<Account_bank_statement_closebalance> list) ;
    Page<Account_bank_statement_closebalance> searchDefault(Account_bank_statement_closebalanceSearchContext context) ;

}



