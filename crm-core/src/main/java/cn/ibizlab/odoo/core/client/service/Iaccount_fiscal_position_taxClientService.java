package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_fiscal_position_tax;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_fiscal_position_tax] 服务对象接口
 */
public interface Iaccount_fiscal_position_taxClientService{

    public Iaccount_fiscal_position_tax createModel() ;

    public void removeBatch(List<Iaccount_fiscal_position_tax> account_fiscal_position_taxes);

    public void updateBatch(List<Iaccount_fiscal_position_tax> account_fiscal_position_taxes);

    public void get(Iaccount_fiscal_position_tax account_fiscal_position_tax);

    public void update(Iaccount_fiscal_position_tax account_fiscal_position_tax);

    public void createBatch(List<Iaccount_fiscal_position_tax> account_fiscal_position_taxes);

    public void create(Iaccount_fiscal_position_tax account_fiscal_position_tax);

    public void remove(Iaccount_fiscal_position_tax account_fiscal_position_tax);

    public Page<Iaccount_fiscal_position_tax> fetchDefault(SearchContext context);

    public Page<Iaccount_fiscal_position_tax> select(SearchContext context);

    public void getDraft(Iaccount_fiscal_position_tax account_fiscal_position_tax);

}
