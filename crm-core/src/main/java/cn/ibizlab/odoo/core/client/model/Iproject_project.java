package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [project_project] 对象
 */
public interface Iproject_project {

    /**
     * 获取 [安全令牌]
     */
    public void setAccess_token(String access_token);
    
    /**
     * 设置 [安全令牌]
     */
    public String getAccess_token();

    /**
     * 获取 [安全令牌]脏标记
     */
    public boolean getAccess_tokenDirtyFlag();
    /**
     * 获取 [门户访问网址]
     */
    public void setAccess_url(String access_url);
    
    /**
     * 设置 [门户访问网址]
     */
    public String getAccess_url();

    /**
     * 获取 [门户访问网址]脏标记
     */
    public boolean getAccess_urlDirtyFlag();
    /**
     * 获取 [访问警告]
     */
    public void setAccess_warning(String access_warning);
    
    /**
     * 设置 [访问警告]
     */
    public String getAccess_warning();

    /**
     * 获取 [访问警告]脏标记
     */
    public boolean getAccess_warningDirtyFlag();
    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [别名联系人安全]
     */
    public void setAlias_contact(String alias_contact);
    
    /**
     * 设置 [别名联系人安全]
     */
    public String getAlias_contact();

    /**
     * 获取 [别名联系人安全]脏标记
     */
    public boolean getAlias_contactDirtyFlag();
    /**
     * 获取 [默认值]
     */
    public void setAlias_defaults(String alias_defaults);
    
    /**
     * 设置 [默认值]
     */
    public String getAlias_defaults();

    /**
     * 获取 [默认值]脏标记
     */
    public boolean getAlias_defaultsDirtyFlag();
    /**
     * 获取 [别名网域]
     */
    public void setAlias_domain(String alias_domain);
    
    /**
     * 设置 [别名网域]
     */
    public String getAlias_domain();

    /**
     * 获取 [别名网域]脏标记
     */
    public boolean getAlias_domainDirtyFlag();
    /**
     * 获取 [记录线索ID]
     */
    public void setAlias_force_thread_id(Integer alias_force_thread_id);
    
    /**
     * 设置 [记录线索ID]
     */
    public Integer getAlias_force_thread_id();

    /**
     * 获取 [记录线索ID]脏标记
     */
    public boolean getAlias_force_thread_idDirtyFlag();
    /**
     * 获取 [别名]
     */
    public void setAlias_id(Integer alias_id);
    
    /**
     * 设置 [别名]
     */
    public Integer getAlias_id();

    /**
     * 获取 [别名]脏标记
     */
    public boolean getAlias_idDirtyFlag();
    /**
     * 获取 [别名的模型]
     */
    public void setAlias_model_id(Integer alias_model_id);
    
    /**
     * 设置 [别名的模型]
     */
    public Integer getAlias_model_id();

    /**
     * 获取 [别名的模型]脏标记
     */
    public boolean getAlias_model_idDirtyFlag();
    /**
     * 获取 [别名]
     */
    public void setAlias_name(String alias_name);
    
    /**
     * 设置 [别名]
     */
    public String getAlias_name();

    /**
     * 获取 [别名]脏标记
     */
    public boolean getAlias_nameDirtyFlag();
    /**
     * 获取 [上级模型]
     */
    public void setAlias_parent_model_id(Integer alias_parent_model_id);
    
    /**
     * 设置 [上级模型]
     */
    public Integer getAlias_parent_model_id();

    /**
     * 获取 [上级模型]脏标记
     */
    public boolean getAlias_parent_model_idDirtyFlag();
    /**
     * 获取 [上级记录线程ID]
     */
    public void setAlias_parent_thread_id(Integer alias_parent_thread_id);
    
    /**
     * 设置 [上级记录线程ID]
     */
    public Integer getAlias_parent_thread_id();

    /**
     * 获取 [上级记录线程ID]脏标记
     */
    public boolean getAlias_parent_thread_idDirtyFlag();
    /**
     * 获取 [所有者]
     */
    public void setAlias_user_id(Integer alias_user_id);
    
    /**
     * 设置 [所有者]
     */
    public Integer getAlias_user_id();

    /**
     * 获取 [所有者]脏标记
     */
    public boolean getAlias_user_idDirtyFlag();
    /**
     * 获取 [颜色索引]
     */
    public void setColor(Integer color);
    
    /**
     * 设置 [颜色索引]
     */
    public Integer getColor();

    /**
     * 获取 [颜色索引]脏标记
     */
    public boolean getColorDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [过期日期]
     */
    public void setDate(Timestamp date);
    
    /**
     * 设置 [过期日期]
     */
    public Timestamp getDate();

    /**
     * 获取 [过期日期]脏标记
     */
    public boolean getDateDirtyFlag();
    /**
     * 获取 [开始日期]
     */
    public void setDate_start(Timestamp date_start);
    
    /**
     * 设置 [开始日期]
     */
    public Timestamp getDate_start();

    /**
     * 获取 [开始日期]脏标记
     */
    public boolean getDate_startDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setDoc_count(Integer doc_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getDoc_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getDoc_countDirtyFlag();
    /**
     * 获取 [会员]
     */
    public void setFavorite_user_ids(String favorite_user_ids);
    
    /**
     * 设置 [会员]
     */
    public String getFavorite_user_ids();

    /**
     * 获取 [会员]脏标记
     */
    public boolean getFavorite_user_idsDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [在仪表板显示项目]
     */
    public void setIs_favorite(String is_favorite);
    
    /**
     * 设置 [在仪表板显示项目]
     */
    public String getIs_favorite();

    /**
     * 获取 [在仪表板显示项目]脏标记
     */
    public boolean getIs_favoriteDirtyFlag();
    /**
     * 获取 [用任务来]
     */
    public void setLabel_tasks(String label_tasks);
    
    /**
     * 设置 [用任务来]
     */
    public String getLabel_tasks();

    /**
     * 获取 [用任务来]脏标记
     */
    public boolean getLabel_tasksDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [是关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [是关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [是关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要采取行动]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要采取行动]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要采取行动]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [行动数量]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [行动数量]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [行动数量]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [客户]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [客户]
     */
    public Integer getPartner_id();

    /**
     * 获取 [客户]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [客户]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [客户]
     */
    public String getPartner_id_text();

    /**
     * 获取 [客户]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [好 % 项目]
     */
    public void setPercentage_satisfaction_project(Integer percentage_satisfaction_project);
    
    /**
     * 设置 [好 % 项目]
     */
    public Integer getPercentage_satisfaction_project();

    /**
     * 获取 [好 % 项目]脏标记
     */
    public boolean getPercentage_satisfaction_projectDirtyFlag();
    /**
     * 获取 [好 % 任务]
     */
    public void setPercentage_satisfaction_task(Integer percentage_satisfaction_task);
    
    /**
     * 设置 [好 % 任务]
     */
    public Integer getPercentage_satisfaction_task();

    /**
     * 获取 [好 % 任务]脏标记
     */
    public boolean getPercentage_satisfaction_taskDirtyFlag();
    /**
     * 获取 [公开评级]
     */
    public void setPortal_show_rating(String portal_show_rating);
    
    /**
     * 设置 [公开评级]
     */
    public String getPortal_show_rating();

    /**
     * 获取 [公开评级]脏标记
     */
    public boolean getPortal_show_ratingDirtyFlag();
    /**
     * 获取 [隐私]
     */
    public void setPrivacy_visibility(String privacy_visibility);
    
    /**
     * 设置 [隐私]
     */
    public String getPrivacy_visibility();

    /**
     * 获取 [隐私]脏标记
     */
    public boolean getPrivacy_visibilityDirtyFlag();
    /**
     * 获取 [评级请求截止日期]
     */
    public void setRating_request_deadline(Timestamp rating_request_deadline);
    
    /**
     * 设置 [评级请求截止日期]
     */
    public Timestamp getRating_request_deadline();

    /**
     * 获取 [评级请求截止日期]脏标记
     */
    public boolean getRating_request_deadlineDirtyFlag();
    /**
     * 获取 [客户点评]
     */
    public void setRating_status(String rating_status);
    
    /**
     * 设置 [客户点评]
     */
    public String getRating_status();

    /**
     * 获取 [客户点评]脏标记
     */
    public boolean getRating_statusDirtyFlag();
    /**
     * 获取 [点评频率]
     */
    public void setRating_status_period(String rating_status_period);
    
    /**
     * 设置 [点评频率]
     */
    public String getRating_status_period();

    /**
     * 获取 [点评频率]脏标记
     */
    public boolean getRating_status_periodDirtyFlag();
    /**
     * 获取 [工作时间]
     */
    public void setResource_calendar_id(Integer resource_calendar_id);
    
    /**
     * 设置 [工作时间]
     */
    public Integer getResource_calendar_id();

    /**
     * 获取 [工作时间]脏标记
     */
    public boolean getResource_calendar_idDirtyFlag();
    /**
     * 获取 [工作时间]
     */
    public void setResource_calendar_id_text(String resource_calendar_id_text);
    
    /**
     * 设置 [工作时间]
     */
    public String getResource_calendar_id_text();

    /**
     * 获取 [工作时间]脏标记
     */
    public boolean getResource_calendar_id_textDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [子任务项目]
     */
    public void setSubtask_project_id(Integer subtask_project_id);
    
    /**
     * 设置 [子任务项目]
     */
    public Integer getSubtask_project_id();

    /**
     * 获取 [子任务项目]脏标记
     */
    public boolean getSubtask_project_idDirtyFlag();
    /**
     * 获取 [子任务项目]
     */
    public void setSubtask_project_id_text(String subtask_project_id_text);
    
    /**
     * 设置 [子任务项目]
     */
    public String getSubtask_project_id_text();

    /**
     * 获取 [子任务项目]脏标记
     */
    public boolean getSubtask_project_id_textDirtyFlag();
    /**
     * 获取 [任务活动]
     */
    public void setTasks(String tasks);
    
    /**
     * 设置 [任务活动]
     */
    public String getTasks();

    /**
     * 获取 [任务活动]脏标记
     */
    public boolean getTasksDirtyFlag();
    /**
     * 获取 [任务统计]
     */
    public void setTask_count(Integer task_count);
    
    /**
     * 设置 [任务统计]
     */
    public Integer getTask_count();

    /**
     * 获取 [任务统计]脏标记
     */
    public boolean getTask_countDirtyFlag();
    /**
     * 获取 [任务]
     */
    public void setTask_ids(String task_ids);
    
    /**
     * 设置 [任务]
     */
    public String getTask_ids();

    /**
     * 获取 [任务]脏标记
     */
    public boolean getTask_idsDirtyFlag();
    /**
     * 获取 [任务阶段]
     */
    public void setType_ids(String type_ids);
    
    /**
     * 设置 [任务阶段]
     */
    public String getType_ids();

    /**
     * 获取 [任务阶段]脏标记
     */
    public boolean getType_idsDirtyFlag();
    /**
     * 获取 [项目管理员]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [项目管理员]
     */
    public Integer getUser_id();

    /**
     * 获取 [项目管理员]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [项目管理员]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [项目管理员]
     */
    public String getUser_id_text();

    /**
     * 获取 [项目管理员]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [网站信息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站信息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站信息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
