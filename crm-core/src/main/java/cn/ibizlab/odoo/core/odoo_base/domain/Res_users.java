package cn.ibizlab.odoo.core.odoo_base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [用户] 对象
 */
@Data
public class Res_users extends EntityClient implements Serializable {

    /**
     * 管理员
     */
    @JSONField(name = "is_moderator")
    @JsonProperty("is_moderator")
    private String isModerator;

    /**
     * 资源
     */
    @JSONField(name = "resource_ids")
    @JsonProperty("resource_ids")
    private String resourceIds;

    /**
     * 创建日期
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 标签
     */
    @JSONField(name = "category_id")
    @JsonProperty("category_id")
    private String categoryId;

    /**
     * 默认工作时间
     */
    @JSONField(name = "resource_calendar_id")
    @JsonProperty("resource_calendar_id")
    private Integer resourceCalendarId;

    /**
     * 用户登录记录
     */
    @JSONField(name = "log_ids")
    @JsonProperty("log_ids")
    private String logIds;

    /**
     * 消息
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 安全PIN
     */
    @DEField(name = "pos_security_pin")
    @JSONField(name = "pos_security_pin")
    @JsonProperty("pos_security_pin")
    private String posSecurityPin;

    /**
     * 徽章
     */
    @JSONField(name = "badge_ids")
    @JsonProperty("badge_ids")
    private String badgeIds;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 公司
     */
    @JSONField(name = "company_ids")
    @JsonProperty("company_ids")
    private String companyIds;

    /**
     * 联系人
     */
    @JSONField(name = "child_ids")
    @JsonProperty("child_ids")
    private String childIds;

    /**
     * 时区偏移
     */
    @JSONField(name = "tz_offset")
    @JsonProperty("tz_offset")
    private String tzOffset;

    /**
     * 活动达成
     */
    @DEField(name = "target_sales_done")
    @JSONField(name = "target_sales_done")
    @JsonProperty("target_sales_done")
    private Integer targetSalesDone;

    /**
     * 关注者(业务伙伴)
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 通知管理
     */
    @DEField(name = "notification_type")
    @JSONField(name = "notification_type")
    @JsonProperty("notification_type")
    private String notificationType;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * IM的状态
     */
    @JSONField(name = "im_status")
    @JsonProperty("im_status")
    private String imStatus;

    /**
     * 贡献值
     */
    @JSONField(name = "karma")
    @JsonProperty("karma")
    private Integer karma;

    /**
     * 登记网站
     */
    @DEField(name = "website_id")
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;

    /**
     * 活动
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;

    /**
     * 金质徽章个数
     */
    @JSONField(name = "gold_badge")
    @JsonProperty("gold_badge")
    private Integer goldBadge;

    /**
     * 相关的员工
     */
    @JSONField(name = "employee_ids")
    @JsonProperty("employee_ids")
    private String employeeIds;

    /**
     * 状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 管理频道
     */
    @JSONField(name = "moderation_channel_ids")
    @JsonProperty("moderation_channel_ids")
    private String moderationChannelIds;

    /**
     * 关注者(渠道)
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 银质徽章个数
     */
    @JSONField(name = "silver_badge")
    @JsonProperty("silver_badge")
    private Integer silverBadge;

    /**
     * 付款令牌
     */
    @JSONField(name = "payment_token_ids")
    @JsonProperty("payment_token_ids")
    private String paymentTokenIds;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 公司数量
     */
    @JSONField(name = "companies_count")
    @JsonProperty("companies_count")
    private Integer companiesCount;

    /**
     * 销售订单目标发票
     */
    @DEField(name = "target_sales_invoiced")
    @JSONField(name = "target_sales_invoiced")
    @JsonProperty("target_sales_invoiced")
    private Integer targetSalesInvoiced;

    /**
     * 关注者
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 最后连接
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "login_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("login_date")
    private Timestamp loginDate;

    /**
     * 渠道
     */
    @JSONField(name = "channel_ids")
    @JsonProperty("channel_ids")
    private String channelIds;

    /**
     * 群组
     */
    @JSONField(name = "groups_id")
    @JsonProperty("groups_id")
    private String groupsId;

    /**
     * 共享用户
     */
    @JSONField(name = "share")
    @JsonProperty("share")
    private String share;

    /**
     * 银行
     */
    @JSONField(name = "bank_ids")
    @JsonProperty("bank_ids")
    private String bankIds;

    /**
     * 销售订单
     */
    @JSONField(name = "sale_order_ids")
    @JsonProperty("sale_order_ids")
    private String saleOrderIds;

    /**
     * 设置密码
     */
    @JSONField(name = "new_password")
    @JsonProperty("new_password")
    private String newPassword;

    /**
     * OdooBot 状态
     */
    @DEField(name = "odoobot_state")
    @JSONField(name = "odoobot_state")
    @JsonProperty("odoobot_state")
    private String odoobotState;

    /**
     * 公司是指业务伙伴
     */
    @JSONField(name = "ref_company_ids")
    @JsonProperty("ref_company_ids")
    private String refCompanyIds;

    /**
     * 密码
     */
    @JSONField(name = "password")
    @JsonProperty("password")
    private String password;

    /**
     * 青铜徽章数目
     */
    @JSONField(name = "bronze_badge")
    @JsonProperty("bronze_badge")
    private Integer bronzeBadge;

    /**
     * 会议
     */
    @JSONField(name = "meeting_ids")
    @JsonProperty("meeting_ids")
    private String meetingIds;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 待发布的帖子
     */
    @JSONField(name = "forum_waiting_posts_count")
    @JsonProperty("forum_waiting_posts_count")
    private Integer forumWaitingPostsCount;

    /**
     * 目标
     */
    @JSONField(name = "goal_ids")
    @JsonProperty("goal_ids")
    private String goalIds;

    /**
     * 签单是商机的最终目标
     */
    @DEField(name = "target_sales_won")
    @JSONField(name = "target_sales_won")
    @JsonProperty("target_sales_won")
    private Integer targetSalesWon;

    /**
     * 网站消息
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 主页动作
     */
    @DEField(name = "action_id")
    @JSONField(name = "action_id")
    @JsonProperty("action_id")
    private Integer actionId;

    /**
     * 登录
     */
    @JSONField(name = "login")
    @JsonProperty("login")
    private String login;

    /**
     * 客户合同
     */
    @JSONField(name = "contract_ids")
    @JsonProperty("contract_ids")
    private String contractIds;

    /**
     * 审核数
     */
    @JSONField(name = "moderation_counter")
    @JsonProperty("moderation_counter")
    private Integer moderationCounter;

    /**
     * 签名
     */
    @JSONField(name = "signature")
    @JsonProperty("signature")
    private String signature;

    /**
     * 用户
     */
    @JSONField(name = "user_ids")
    @JsonProperty("user_ids")
    private String userIds;

    /**
     * 商机
     */
    @JSONField(name = "opportunity_ids")
    @JsonProperty("opportunity_ids")
    private String opportunityIds;

    /**
     * 任务
     */
    @JSONField(name = "task_ids")
    @JsonProperty("task_ids")
    private String taskIds;

    /**
     * 发票
     */
    @JSONField(name = "invoice_ids")
    @JsonProperty("invoice_ids")
    private String invoiceIds;

    /**
     * 内部参考
     */
    @JSONField(name = "ref")
    @JsonProperty("ref")
    private String ref;

    /**
     * 错误个数
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 最近的在线销售订单
     */
    @JSONField(name = "last_website_so_id")
    @JsonProperty("last_website_so_id")
    private Integer lastWebsiteSoId;

    /**
     * 日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 关注者
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 客户位置
     */
    @JSONField(name = "property_stock_customer")
    @JsonProperty("property_stock_customer")
    private Integer propertyStockCustomer;

    /**
     * 最近的发票和付款匹配时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "last_time_entries_checked" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("last_time_entries_checked")
    private Timestamp lastTimeEntriesChecked;

    /**
     * 语言
     */
    @JSONField(name = "lang")
    @JsonProperty("lang")
    private String lang;

    /**
     * 销售警告
     */
    @JSONField(name = "sale_warn")
    @JsonProperty("sale_warn")
    private String saleWarn;

    /**
     * #会议
     */
    @JSONField(name = "meeting_count")
    @JsonProperty("meeting_count")
    private Integer meetingCount;

    /**
     * 街道
     */
    @JSONField(name = "street")
    @JsonProperty("street")
    private String street;

    /**
     * 发票
     */
    @JSONField(name = "invoice_warn")
    @JsonProperty("invoice_warn")
    private String invoiceWarn;

    /**
     * 注册令牌 Token
     */
    @JSONField(name = "signup_token")
    @JsonProperty("signup_token")
    private String signupToken;

    /**
     * # 任务
     */
    @JSONField(name = "task_count")
    @JsonProperty("task_count")
    private Integer taskCount;

    /**
     * 注册令牌（ Token  ）是有效的
     */
    @JSONField(name = "signup_valid")
    @JsonProperty("signup_valid")
    private String signupValid;

    /**
     * 未读消息计数器
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 注册令牌（Token）类型
     */
    @JSONField(name = "signup_type")
    @JsonProperty("signup_type")
    private String signupType;

    /**
     * 应收账款
     */
    @JSONField(name = "property_account_receivable_id")
    @JsonProperty("property_account_receivable_id")
    private Integer propertyAccountReceivableId;

    /**
     * 网站opengraph图像
     */
    @JSONField(name = "website_meta_og_img")
    @JsonProperty("website_meta_og_img")
    private String websiteMetaOgImg;

    /**
     * 活动
     */
    @JSONField(name = "event_count")
    @JsonProperty("event_count")
    private Integer eventCount;

    /**
     * 日记账项目
     */
    @JSONField(name = "journal_item_count")
    @JsonProperty("journal_item_count")
    private Integer journalItemCount;

    /**
     * 上级名称
     */
    @JSONField(name = "parent_name")
    @JsonProperty("parent_name")
    private String parentName;

    /**
     * 用户的销售团队
     */
    @JSONField(name = "sale_team_id_text")
    @JsonProperty("sale_team_id_text")
    private String saleTeamIdText;

    /**
     * 应收总计
     */
    @JSONField(name = "credit")
    @JsonProperty("credit")
    private Double credit;

    /**
     * 商机
     */
    @JSONField(name = "opportunity_count")
    @JsonProperty("opportunity_count")
    private Integer opportunityCount;

    /**
     * 注册网址
     */
    @JSONField(name = "signup_url")
    @JsonProperty("signup_url")
    private String signupUrl;

    /**
     * 应付账款
     */
    @JSONField(name = "property_account_payable_id")
    @JsonProperty("property_account_payable_id")
    private Integer propertyAccountPayableId;

    /**
     * 省/ 州
     */
    @JSONField(name = "state_id")
    @JsonProperty("state_id")
    private Integer stateId;

    /**
     * 消息递送错误
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 销售订单消息
     */
    @JSONField(name = "sale_warn_msg")
    @JsonProperty("sale_warn_msg")
    private String saleWarnMsg;

    /**
     * 在当前网站显示
     */
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private String websitePublished;

    /**
     * 已开票总计
     */
    @JSONField(name = "total_invoiced")
    @JsonProperty("total_invoiced")
    private Double totalInvoiced;

    /**
     * 应付总计
     */
    @JSONField(name = "debit")
    @JsonProperty("debit")
    private Double debit;

    /**
     * 销售点订单计数
     */
    @JSONField(name = "pos_order_count")
    @JsonProperty("pos_order_count")
    private Integer posOrderCount;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 称谓
     */
    @JSONField(name = "title")
    @JsonProperty("title")
    private Integer title;

    /**
     * 未读消息
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * ＃供应商账单
     */
    @JSONField(name = "supplier_invoice_count")
    @JsonProperty("supplier_invoice_count")
    private Integer supplierInvoiceCount;

    /**
     * 城市
     */
    @JSONField(name = "city")
    @JsonProperty("city")
    private String city;

    /**
     * 库存拣货
     */
    @JSONField(name = "picking_warn")
    @JsonProperty("picking_warn")
    private String pickingWarn;

    /**
     * 附件
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 责任用户
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 附加信息
     */
    @JSONField(name = "additional_info")
    @JsonProperty("additional_info")
    private String additionalInfo;

    /**
     * 工作岗位
     */
    @JSONField(name = "ibizfunction")
    @JsonProperty("ibizfunction")
    private String ibizfunction;

    /**
     * 网站网址
     */
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    private String websiteUrl;

    /**
     * 最后的提醒已经标志为已读
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "calendar_last_notif_ack" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("calendar_last_notif_ack")
    private Timestamp calendarLastNotifAck;

    /**
     * 操作次数
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 下一活动截止日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 员工
     */
    @JSONField(name = "employee")
    @JsonProperty("employee")
    private String employee;

    /**
     * 活动状态
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;

    /**
     * 条码
     */
    @JSONField(name = "barcode")
    @JsonProperty("barcode")
    private String barcode;

    /**
     * 公司数据库ID
     */
    @JSONField(name = "partner_gid")
    @JsonProperty("partner_gid")
    private Integer partnerGid;

    /**
     * 地址类型
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;

    /**
     * 税号
     */
    @JSONField(name = "vat")
    @JsonProperty("vat")
    private String vat;

    /**
     * 采购订单消息
     */
    @JSONField(name = "purchase_warn_msg")
    @JsonProperty("purchase_warn_msg")
    private String purchaseWarnMsg;

    /**
     * 便签
     */
    @JSONField(name = "comment")
    @JsonProperty("comment")
    private String comment;

    /**
     * 供应商
     */
    @JSONField(name = "supplier")
    @JsonProperty("supplier")
    private String supplier;

    /**
     * 网站meta关键词
     */
    @JSONField(name = "website_meta_keywords")
    @JsonProperty("website_meta_keywords")
    private String websiteMetaKeywords;

    /**
     * 关联公司
     */
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    private Integer parentId;

    /**
     * 采购订单
     */
    @JSONField(name = "purchase_warn")
    @JsonProperty("purchase_warn")
    private String purchaseWarn;

    /**
     * 激活的合作伙伴
     */
    @JSONField(name = "active_partner")
    @JsonProperty("active_partner")
    private String activePartner;

    /**
     * 币种
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 工业
     */
    @JSONField(name = "industry_id")
    @JsonProperty("industry_id")
    private Integer industryId;

    /**
     * 供应商位置
     */
    @JSONField(name = "property_stock_supplier")
    @JsonProperty("property_stock_supplier")
    private Integer propertyStockSupplier;

    /**
     * 付款令牌计数
     */
    @JSONField(name = "payment_token_count")
    @JsonProperty("payment_token_count")
    private Integer paymentTokenCount;

    /**
     * 前置操作
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 销售员
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;

    /**
     * 客户
     */
    @JSONField(name = "customer")
    @JsonProperty("customer")
    private String customer;

    /**
     * 客户付款条款
     */
    @JSONField(name = "property_payment_term_id")
    @JsonProperty("property_payment_term_id")
    private Integer propertyPaymentTermId;

    /**
     * 下一活动类型
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 合同统计
     */
    @JSONField(name = "contracts_count")
    @JsonProperty("contracts_count")
    private Integer contractsCount;

    /**
     * 自己
     */
    @JSONField(name = "self")
    @JsonProperty("self")
    private Integer self;

    /**
     * 网站元说明
     */
    @JSONField(name = "website_meta_description")
    @JsonProperty("website_meta_description")
    private String websiteMetaDescription;

    /**
     * 图像
     */
    @JSONField(name = "image")
    @JsonProperty("image")
    private byte[] image;

    /**
     * EMail
     */
    @JSONField(name = "email")
    @JsonProperty("email")
    private String email;

    /**
     * 中等尺寸图像
     */
    @JSONField(name = "image_medium")
    @JsonProperty("image_medium")
    private byte[] imageMedium;

    /**
     * 下一个活动摘要
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;

    /**
     * 应付限额
     */
    @JSONField(name = "debit_limit")
    @JsonProperty("debit_limit")
    private Double debitLimit;

    /**
     * 国家/地区
     */
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    private Integer countryId;

    /**
     * 信用额度
     */
    @JSONField(name = "credit_limit")
    @JsonProperty("credit_limit")
    private Double creditLimit;

    /**
     * 公司名称实体
     */
    @JSONField(name = "commercial_company_name")
    @JsonProperty("commercial_company_name")
    private String commercialCompanyName;

    /**
     * 发票消息
     */
    @JSONField(name = "invoice_warn_msg")
    @JsonProperty("invoice_warn_msg")
    private String invoiceWarnMsg;

    /**
     * 已发布
     */
    @JSONField(name = "is_published")
    @JsonProperty("is_published")
    private String isPublished;

    /**
     * 对此债务人的信任度
     */
    @JSONField(name = "trust")
    @JsonProperty("trust")
    private String trust;

    /**
     * 手机
     */
    @JSONField(name = "mobile")
    @JsonProperty("mobile")
    private String mobile;

    /**
     * 格式化的邮件
     */
    @JSONField(name = "email_formatted")
    @JsonProperty("email_formatted")
    private String emailFormatted;

    /**
     * 公司
     */
    @JSONField(name = "is_company")
    @JsonProperty("is_company")
    private String isCompany;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 销售团队
     */
    @JSONField(name = "team_id")
    @JsonProperty("team_id")
    private Integer teamId;

    /**
     * 黑名单
     */
    @JSONField(name = "is_blacklisted")
    @JsonProperty("is_blacklisted")
    private String isBlacklisted;

    /**
     * 银行
     */
    @JSONField(name = "bank_account_count")
    @JsonProperty("bank_account_count")
    private Integer bankAccountCount;

    /**
     * 价格表
     */
    @JSONField(name = "property_product_pricelist")
    @JsonProperty("property_product_pricelist")
    private Integer propertyProductPricelist;

    /**
     * 名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 库存拣货单消息
     */
    @JSONField(name = "picking_warn_msg")
    @JsonProperty("picking_warn_msg")
    private String pickingWarnMsg;

    /**
     * 附件数量
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 网站
     */
    @JSONField(name = "website")
    @JsonProperty("website")
    private String website;

    /**
     * 电话
     */
    @JSONField(name = "phone")
    @JsonProperty("phone")
    private String phone;

    /**
     * 街道 2
     */
    @JSONField(name = "street2")
    @JsonProperty("street2")
    private String street2;

    /**
     * 有未核销的分录
     */
    @JSONField(name = "has_unreconciled_entries")
    @JsonProperty("has_unreconciled_entries")
    private String hasUnreconciledEntries;

    /**
     * 注册到期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "signup_expiration" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("signup_expiration")
    private Timestamp signupExpiration;

    /**
     * 时区
     */
    @JSONField(name = "tz")
    @JsonProperty("tz")
    private String tz;

    /**
     * 完整地址
     */
    @JSONField(name = "contact_address")
    @JsonProperty("contact_address")
    private String contactAddress;

    /**
     * 网站业务伙伴简介
     */
    @JSONField(name = "website_short_description")
    @JsonProperty("website_short_description")
    private String websiteShortDescription;

    /**
     * 共享合作伙伴
     */
    @JSONField(name = "partner_share")
    @JsonProperty("partner_share")
    private String partnerShare;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 颜色索引
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 邮政编码
     */
    @JSONField(name = "zip")
    @JsonProperty("zip")
    private String zip;

    /**
     * 销售订单个数
     */
    @JSONField(name = "sale_order_count")
    @JsonProperty("sale_order_count")
    private Integer saleOrderCount;

    /**
     * 公司类别
     */
    @JSONField(name = "company_type")
    @JsonProperty("company_type")
    private String companyType;

    /**
     * 税科目调整
     */
    @JSONField(name = "property_account_position_id")
    @JsonProperty("property_account_position_id")
    private Integer propertyAccountPositionId;

    /**
     * SEO优化
     */
    @JSONField(name = "is_seo_optimized")
    @JsonProperty("is_seo_optimized")
    private String isSeoOptimized;

    /**
     * 退回
     */
    @JSONField(name = "message_bounce")
    @JsonProperty("message_bounce")
    private Integer messageBounce;

    /**
     * 网站meta标题
     */
    @JSONField(name = "website_meta_title")
    @JsonProperty("website_meta_title")
    private String websiteMetaTitle;

    /**
     * 小尺寸图像
     */
    @JSONField(name = "image_small")
    @JsonProperty("image_small")
    private byte[] imageSmall;

    /**
     * 供应商货币
     */
    @JSONField(name = "property_purchase_currency_id")
    @JsonProperty("property_purchase_currency_id")
    private Integer propertyPurchaseCurrencyId;

    /**
     * 采购订单数
     */
    @JSONField(name = "purchase_order_count")
    @JsonProperty("purchase_order_count")
    private Integer purchaseOrderCount;

    /**
     * 网站业务伙伴的详细说明
     */
    @JSONField(name = "website_description")
    @JsonProperty("website_description")
    private String websiteDescription;

    /**
     * 供应商付款条款
     */
    @JSONField(name = "property_supplier_payment_term_id")
    @JsonProperty("property_supplier_payment_term_id")
    private Integer propertySupplierPaymentTermId;

    /**
     * 商业实体
     */
    @JSONField(name = "commercial_partner_id")
    @JsonProperty("commercial_partner_id")
    private Integer commercialPartnerId;

    /**
     * 安全联系人别名
     */
    @JSONField(name = "alias_contact")
    @JsonProperty("alias_contact")
    private String aliasContact;

    /**
     * 公司名称
     */
    @JSONField(name = "company_name")
    @JsonProperty("company_name")
    private String companyName;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 相关的业务伙伴
     */
    @DEField(name = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 别名
     */
    @DEField(name = "alias_id")
    @JSONField(name = "alias_id")
    @JsonProperty("alias_id")
    private Integer aliasId;

    /**
     * 用户的销售团队
     */
    @DEField(name = "sale_team_id")
    @JSONField(name = "sale_team_id")
    @JsonProperty("sale_team_id")
    private Integer saleTeamId;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 
     */
    @JSONField(name = "odoosaleteam")
    @JsonProperty("odoosaleteam")
    private cn.ibizlab.odoo.core.odoo_crm.domain.Crm_team odooSaleTeam;

    /**
     * 
     */
    @JSONField(name = "odooalias")
    @JsonProperty("odooalias")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_alias odooAlias;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoopartner")
    @JsonProperty("odoopartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;


    /**
     * 销售订单行
     */
    @JSONField(name = "createman_order_lines")
    @JsonProperty("createman_order_lines")
    private List<cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_line> createmanOrderLine;

    /**
     * 销售订单行
     */
    @JSONField(name = "salesman_order_lines")
    @JsonProperty("salesman_order_lines")
    private List<cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_line> salesmanOrderLine;

    /**
     * 销售订单行
     */
    @JSONField(name = "writeman_order_lines")
    @JsonProperty("writeman_order_lines")
    private List<cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_line> writemanOrderLine;



    /**
     * 设置 [安全PIN]
     */
    public void setPosSecurityPin(String posSecurityPin){
        this.posSecurityPin = posSecurityPin ;
        this.modify("pos_security_pin",posSecurityPin);
    }
    /**
     * 设置 [活动达成]
     */
    public void setTargetSalesDone(Integer targetSalesDone){
        this.targetSalesDone = targetSalesDone ;
        this.modify("target_sales_done",targetSalesDone);
    }
    /**
     * 设置 [通知管理]
     */
    public void setNotificationType(String notificationType){
        this.notificationType = notificationType ;
        this.modify("notification_type",notificationType);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [贡献值]
     */
    public void setKarma(Integer karma){
        this.karma = karma ;
        this.modify("karma",karma);
    }
    /**
     * 设置 [登记网站]
     */
    public void setWebsiteId(Integer websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }
    /**
     * 设置 [销售订单目标发票]
     */
    public void setTargetSalesInvoiced(Integer targetSalesInvoiced){
        this.targetSalesInvoiced = targetSalesInvoiced ;
        this.modify("target_sales_invoiced",targetSalesInvoiced);
    }
    /**
     * 设置 [共享用户]
     */
    public void setShare(String share){
        this.share = share ;
        this.modify("share",share);
    }
    /**
     * 设置 [OdooBot 状态]
     */
    public void setOdoobotState(String odoobotState){
        this.odoobotState = odoobotState ;
        this.modify("odoobot_state",odoobotState);
    }
    /**
     * 设置 [签单是商机的最终目标]
     */
    public void setTargetSalesWon(Integer targetSalesWon){
        this.targetSalesWon = targetSalesWon ;
        this.modify("target_sales_won",targetSalesWon);
    }
    /**
     * 设置 [主页动作]
     */
    public void setActionId(Integer actionId){
        this.actionId = actionId ;
        this.modify("action_id",actionId);
    }
    /**
     * 设置 [登录]
     */
    public void setLogin(String login){
        this.login = login ;
        this.modify("login",login);
    }
    /**
     * 设置 [签名]
     */
    public void setSignature(String signature){
        this.signature = signature ;
        this.modify("signature",signature);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [相关的业务伙伴]
     */
    public void setPartnerId(Integer partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }
    /**
     * 设置 [别名]
     */
    public void setAliasId(Integer aliasId){
        this.aliasId = aliasId ;
        this.modify("alias_id",aliasId);
    }
    /**
     * 设置 [用户的销售团队]
     */
    public void setSaleTeamId(Integer saleTeamId){
        this.saleTeamId = saleTeamId ;
        this.modify("sale_team_id",saleTeamId);
    }

}


