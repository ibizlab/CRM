package cn.ibizlab.odoo.core.odoo_payment.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_acquirer;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_acquirerSearchContext;


/**
 * 实体[Payment_acquirer] 服务对象接口
 */
public interface IPayment_acquirerService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Payment_acquirer et) ;
    void updateBatch(List<Payment_acquirer> list) ;
    Payment_acquirer get(Integer key) ;
    Payment_acquirer getDraft(Payment_acquirer et) ;
    boolean create(Payment_acquirer et) ;
    void createBatch(List<Payment_acquirer> list) ;
    Page<Payment_acquirer> searchDefault(Payment_acquirerSearchContext context) ;

}



