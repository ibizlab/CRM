package cn.ibizlab.odoo.core.odoo_calendar.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_alarm;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_alarmSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[calendar_alarm] 服务对象接口
 */
@Component
public class calendar_alarmFallback implements calendar_alarmFeignClient{


    public Calendar_alarm create(Calendar_alarm calendar_alarm){
            return null;
     }
    public Boolean createBatch(List<Calendar_alarm> calendar_alarms){
            return false;
     }

    public Calendar_alarm update(Integer id, Calendar_alarm calendar_alarm){
            return null;
     }
    public Boolean updateBatch(List<Calendar_alarm> calendar_alarms){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Page<Calendar_alarm> searchDefault(Calendar_alarmSearchContext context){
            return null;
     }



    public Calendar_alarm get(Integer id){
            return null;
     }


    public Page<Calendar_alarm> select(){
            return null;
     }

    public Calendar_alarm getDraft(){
            return null;
    }



}
