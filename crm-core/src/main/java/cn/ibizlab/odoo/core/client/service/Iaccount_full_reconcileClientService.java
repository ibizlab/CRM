package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_full_reconcile;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_full_reconcile] 服务对象接口
 */
public interface Iaccount_full_reconcileClientService{

    public Iaccount_full_reconcile createModel() ;

    public void create(Iaccount_full_reconcile account_full_reconcile);

    public void get(Iaccount_full_reconcile account_full_reconcile);

    public void removeBatch(List<Iaccount_full_reconcile> account_full_reconciles);

    public void remove(Iaccount_full_reconcile account_full_reconcile);

    public Page<Iaccount_full_reconcile> fetchDefault(SearchContext context);

    public void updateBatch(List<Iaccount_full_reconcile> account_full_reconciles);

    public void update(Iaccount_full_reconcile account_full_reconcile);

    public void createBatch(List<Iaccount_full_reconcile> account_full_reconciles);

    public Page<Iaccount_full_reconcile> select(SearchContext context);

    public void getDraft(Iaccount_full_reconcile account_full_reconcile);

}
