package cn.ibizlab.odoo.core.odoo_website.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_published_mixin;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_published_mixinSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[website_published_mixin] 服务对象接口
 */
@Component
public class website_published_mixinFallback implements website_published_mixinFeignClient{

    public Page<Website_published_mixin> searchDefault(Website_published_mixinSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }



    public Website_published_mixin create(Website_published_mixin website_published_mixin){
            return null;
     }
    public Boolean createBatch(List<Website_published_mixin> website_published_mixins){
            return false;
     }

    public Website_published_mixin update(Integer id, Website_published_mixin website_published_mixin){
            return null;
     }
    public Boolean updateBatch(List<Website_published_mixin> website_published_mixins){
            return false;
     }


    public Website_published_mixin get(Integer id){
            return null;
     }



    public Page<Website_published_mixin> select(){
            return null;
     }

    public Website_published_mixin getDraft(){
            return null;
    }



}
