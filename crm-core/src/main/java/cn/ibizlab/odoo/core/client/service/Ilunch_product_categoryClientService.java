package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ilunch_product_category;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[lunch_product_category] 服务对象接口
 */
public interface Ilunch_product_categoryClientService{

    public Ilunch_product_category createModel() ;

    public Page<Ilunch_product_category> fetchDefault(SearchContext context);

    public void create(Ilunch_product_category lunch_product_category);

    public void remove(Ilunch_product_category lunch_product_category);

    public void updateBatch(List<Ilunch_product_category> lunch_product_categories);

    public void update(Ilunch_product_category lunch_product_category);

    public void get(Ilunch_product_category lunch_product_category);

    public void createBatch(List<Ilunch_product_category> lunch_product_categories);

    public void removeBatch(List<Ilunch_product_category> lunch_product_categories);

    public Page<Ilunch_product_category> select(SearchContext context);

    public void getDraft(Ilunch_product_category lunch_product_category);

}
