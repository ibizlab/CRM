package cn.ibizlab.odoo.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenter_productivitySearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mrp_workcenter_productivity] 服务对象接口
 */
@Component
public class mrp_workcenter_productivityFallback implements mrp_workcenter_productivityFeignClient{


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Mrp_workcenter_productivity update(Integer id, Mrp_workcenter_productivity mrp_workcenter_productivity){
            return null;
     }
    public Boolean updateBatch(List<Mrp_workcenter_productivity> mrp_workcenter_productivities){
            return false;
     }


    public Page<Mrp_workcenter_productivity> searchDefault(Mrp_workcenter_productivitySearchContext context){
            return null;
     }


    public Mrp_workcenter_productivity create(Mrp_workcenter_productivity mrp_workcenter_productivity){
            return null;
     }
    public Boolean createBatch(List<Mrp_workcenter_productivity> mrp_workcenter_productivities){
            return false;
     }



    public Mrp_workcenter_productivity get(Integer id){
            return null;
     }


    public Page<Mrp_workcenter_productivity> select(){
            return null;
     }

    public Mrp_workcenter_productivity getDraft(){
            return null;
    }



}
