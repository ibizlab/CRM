package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [mail_mail_statistics] 对象
 */
public interface Imail_mail_statistics {

    /**
     * 获取 [被退回]
     */
    public void setBounced(Timestamp bounced);
    
    /**
     * 设置 [被退回]
     */
    public Timestamp getBounced();

    /**
     * 获取 [被退回]脏标记
     */
    public boolean getBouncedDirtyFlag();
    /**
     * 获取 [点击率]
     */
    public void setClicked(Timestamp clicked);
    
    /**
     * 设置 [点击率]
     */
    public Timestamp getClicked();

    /**
     * 获取 [点击率]脏标记
     */
    public boolean getClickedDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [收件人email地址]
     */
    public void setEmail(String email);
    
    /**
     * 设置 [收件人email地址]
     */
    public String getEmail();

    /**
     * 获取 [收件人email地址]脏标记
     */
    public boolean getEmailDirtyFlag();
    /**
     * 获取 [异常]
     */
    public void setException(Timestamp exception);
    
    /**
     * 设置 [异常]
     */
    public Timestamp getException();

    /**
     * 获取 [异常]脏标记
     */
    public boolean getExceptionDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [忽略]
     */
    public void setIgnored(Timestamp ignored);
    
    /**
     * 设置 [忽略]
     */
    public Timestamp getIgnored();

    /**
     * 获取 [忽略]脏标记
     */
    public boolean getIgnoredDirtyFlag();
    /**
     * 获取 [点击链接]
     */
    public void setLinks_click_ids(String links_click_ids);
    
    /**
     * 设置 [点击链接]
     */
    public String getLinks_click_ids();

    /**
     * 获取 [点击链接]脏标记
     */
    public boolean getLinks_click_idsDirtyFlag();
    /**
     * 获取 [邮件]
     */
    public void setMail_mail_id(Integer mail_mail_id);
    
    /**
     * 设置 [邮件]
     */
    public Integer getMail_mail_id();

    /**
     * 获取 [邮件]脏标记
     */
    public boolean getMail_mail_idDirtyFlag();
    /**
     * 获取 [邮件ID（技术）]
     */
    public void setMail_mail_id_int(Integer mail_mail_id_int);
    
    /**
     * 设置 [邮件ID（技术）]
     */
    public Integer getMail_mail_id_int();

    /**
     * 获取 [邮件ID（技术）]脏标记
     */
    public boolean getMail_mail_id_intDirtyFlag();
    /**
     * 获取 [群发邮件营销]
     */
    public void setMass_mailing_campaign_id(Integer mass_mailing_campaign_id);
    
    /**
     * 设置 [群发邮件营销]
     */
    public Integer getMass_mailing_campaign_id();

    /**
     * 获取 [群发邮件营销]脏标记
     */
    public boolean getMass_mailing_campaign_idDirtyFlag();
    /**
     * 获取 [群发邮件营销]
     */
    public void setMass_mailing_campaign_id_text(String mass_mailing_campaign_id_text);
    
    /**
     * 设置 [群发邮件营销]
     */
    public String getMass_mailing_campaign_id_text();

    /**
     * 获取 [群发邮件营销]脏标记
     */
    public boolean getMass_mailing_campaign_id_textDirtyFlag();
    /**
     * 获取 [群发邮件]
     */
    public void setMass_mailing_id(Integer mass_mailing_id);
    
    /**
     * 设置 [群发邮件]
     */
    public Integer getMass_mailing_id();

    /**
     * 获取 [群发邮件]脏标记
     */
    public boolean getMass_mailing_idDirtyFlag();
    /**
     * 获取 [群发邮件]
     */
    public void setMass_mailing_id_text(String mass_mailing_id_text);
    
    /**
     * 设置 [群发邮件]
     */
    public String getMass_mailing_id_text();

    /**
     * 获取 [群发邮件]脏标记
     */
    public boolean getMass_mailing_id_textDirtyFlag();
    /**
     * 获取 [消息ID]
     */
    public void setMessage_id(String message_id);
    
    /**
     * 设置 [消息ID]
     */
    public String getMessage_id();

    /**
     * 获取 [消息ID]脏标记
     */
    public boolean getMessage_idDirtyFlag();
    /**
     * 获取 [文档模型]
     */
    public void setModel(String model);
    
    /**
     * 设置 [文档模型]
     */
    public String getModel();

    /**
     * 获取 [文档模型]脏标记
     */
    public boolean getModelDirtyFlag();
    /**
     * 获取 [已开启]
     */
    public void setOpened(Timestamp opened);
    
    /**
     * 设置 [已开启]
     */
    public Timestamp getOpened();

    /**
     * 获取 [已开启]脏标记
     */
    public boolean getOpenedDirtyFlag();
    /**
     * 获取 [已回复]
     */
    public void setReplied(Timestamp replied);
    
    /**
     * 设置 [已回复]
     */
    public Timestamp getReplied();

    /**
     * 获取 [已回复]脏标记
     */
    public boolean getRepliedDirtyFlag();
    /**
     * 获取 [文档ID]
     */
    public void setRes_id(Integer res_id);
    
    /**
     * 设置 [文档ID]
     */
    public Integer getRes_id();

    /**
     * 获取 [文档ID]脏标记
     */
    public boolean getRes_idDirtyFlag();
    /**
     * 获取 [安排]
     */
    public void setScheduled(Timestamp scheduled);
    
    /**
     * 设置 [安排]
     */
    public Timestamp getScheduled();

    /**
     * 获取 [安排]脏标记
     */
    public boolean getScheduledDirtyFlag();
    /**
     * 获取 [已汇]
     */
    public void setSent(Timestamp sent);
    
    /**
     * 设置 [已汇]
     */
    public Timestamp getSent();

    /**
     * 获取 [已汇]脏标记
     */
    public boolean getSentDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [状态更新]
     */
    public void setState_update(Timestamp state_update);
    
    /**
     * 设置 [状态更新]
     */
    public Timestamp getState_update();

    /**
     * 获取 [状态更新]脏标记
     */
    public boolean getState_updateDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
