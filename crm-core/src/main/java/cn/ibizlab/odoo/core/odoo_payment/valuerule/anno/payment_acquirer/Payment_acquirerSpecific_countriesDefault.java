package cn.ibizlab.odoo.core.odoo_payment.valuerule.anno.payment_acquirer;

import cn.ibizlab.odoo.core.odoo_payment.valuerule.validator.payment_acquirer.Payment_acquirerSpecific_countriesDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Payment_acquirer
 * 属性：Specific_countries
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Payment_acquirerSpecific_countriesDefaultValidator.class})
public @interface Payment_acquirerSpecific_countriesDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
