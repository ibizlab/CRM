package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_tag;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_mass_mailing_tag] 服务对象接口
 */
public interface Imail_mass_mailing_tagClientService{

    public Imail_mass_mailing_tag createModel() ;

    public void removeBatch(List<Imail_mass_mailing_tag> mail_mass_mailing_tags);

    public void remove(Imail_mass_mailing_tag mail_mass_mailing_tag);

    public Page<Imail_mass_mailing_tag> fetchDefault(SearchContext context);

    public void updateBatch(List<Imail_mass_mailing_tag> mail_mass_mailing_tags);

    public void create(Imail_mass_mailing_tag mail_mass_mailing_tag);

    public void update(Imail_mass_mailing_tag mail_mass_mailing_tag);

    public void createBatch(List<Imail_mass_mailing_tag> mail_mass_mailing_tags);

    public void get(Imail_mass_mailing_tag mail_mass_mailing_tag);

    public Page<Imail_mass_mailing_tag> select(SearchContext context);

    public void getDraft(Imail_mass_mailing_tag mail_mass_mailing_tag);

}
