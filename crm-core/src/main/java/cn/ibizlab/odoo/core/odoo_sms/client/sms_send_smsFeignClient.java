package cn.ibizlab.odoo.core.odoo_sms.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_sms.domain.Sms_send_sms;
import cn.ibizlab.odoo.core.odoo_sms.filter.Sms_send_smsSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[sms_send_sms] 服务对象接口
 */
@FeignClient(value = "odoo-sms", contextId = "sms-send-sms", fallback = sms_send_smsFallback.class)
public interface sms_send_smsFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/sms_send_sms/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/sms_send_sms/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/sms_send_sms/{id}")
    Sms_send_sms get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/sms_send_sms")
    Sms_send_sms create(@RequestBody Sms_send_sms sms_send_sms);

    @RequestMapping(method = RequestMethod.POST, value = "/sms_send_sms/batch")
    Boolean createBatch(@RequestBody List<Sms_send_sms> sms_send_sms);



    @RequestMapping(method = RequestMethod.PUT, value = "/sms_send_sms/{id}")
    Sms_send_sms update(@PathVariable("id") Integer id,@RequestBody Sms_send_sms sms_send_sms);

    @RequestMapping(method = RequestMethod.PUT, value = "/sms_send_sms/batch")
    Boolean updateBatch(@RequestBody List<Sms_send_sms> sms_send_sms);




    @RequestMapping(method = RequestMethod.POST, value = "/sms_send_sms/searchdefault")
    Page<Sms_send_sms> searchDefault(@RequestBody Sms_send_smsSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/sms_send_sms/select")
    Page<Sms_send_sms> select();


    @RequestMapping(method = RequestMethod.GET, value = "/sms_send_sms/getdraft")
    Sms_send_sms getDraft();


}
