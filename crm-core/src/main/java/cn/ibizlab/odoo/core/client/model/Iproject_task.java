package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [project_task] 对象
 */
public interface Iproject_task {

    /**
     * 获取 [安全令牌]
     */
    public void setAccess_token(String access_token);
    
    /**
     * 设置 [安全令牌]
     */
    public String getAccess_token();

    /**
     * 获取 [安全令牌]脏标记
     */
    public boolean getAccess_tokenDirtyFlag();
    /**
     * 获取 [门户访问网址]
     */
    public void setAccess_url(String access_url);
    
    /**
     * 设置 [门户访问网址]
     */
    public String getAccess_url();

    /**
     * 获取 [门户访问网址]脏标记
     */
    public boolean getAccess_urlDirtyFlag();
    /**
     * 获取 [访问警告]
     */
    public void setAccess_warning(String access_warning);
    
    /**
     * 设置 [访问警告]
     */
    public String getAccess_warning();

    /**
     * 获取 [访问警告]脏标记
     */
    public boolean getAccess_warningDirtyFlag();
    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [下一活动截止日期]
     */
    public void setActivity_date_deadline(Timestamp activity_date_deadline);
    
    /**
     * 设置 [下一活动截止日期]
     */
    public Timestamp getActivity_date_deadline();

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    public boolean getActivity_date_deadlineDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setActivity_ids(String activity_ids);
    
    /**
     * 设置 [活动]
     */
    public String getActivity_ids();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getActivity_idsDirtyFlag();
    /**
     * 获取 [活动状态]
     */
    public void setActivity_state(String activity_state);
    
    /**
     * 设置 [活动状态]
     */
    public String getActivity_state();

    /**
     * 获取 [活动状态]脏标记
     */
    public boolean getActivity_stateDirtyFlag();
    /**
     * 获取 [下一活动摘要]
     */
    public void setActivity_summary(String activity_summary);
    
    /**
     * 设置 [下一活动摘要]
     */
    public String getActivity_summary();

    /**
     * 获取 [下一活动摘要]脏标记
     */
    public boolean getActivity_summaryDirtyFlag();
    /**
     * 获取 [下一活动类型]
     */
    public void setActivity_type_id(Integer activity_type_id);
    
    /**
     * 设置 [下一活动类型]
     */
    public Integer getActivity_type_id();

    /**
     * 获取 [下一活动类型]脏标记
     */
    public boolean getActivity_type_idDirtyFlag();
    /**
     * 获取 [责任用户]
     */
    public void setActivity_user_id(Integer activity_user_id);
    
    /**
     * 设置 [责任用户]
     */
    public Integer getActivity_user_id();

    /**
     * 获取 [责任用户]脏标记
     */
    public boolean getActivity_user_idDirtyFlag();
    /**
     * 获取 [主要附件]
     */
    public void setAttachment_ids(String attachment_ids);
    
    /**
     * 设置 [主要附件]
     */
    public String getAttachment_ids();

    /**
     * 获取 [主要附件]脏标记
     */
    public boolean getAttachment_idsDirtyFlag();
    /**
     * 获取 [子任务]
     */
    public void setChild_ids(String child_ids);
    
    /**
     * 设置 [子任务]
     */
    public String getChild_ids();

    /**
     * 获取 [子任务]脏标记
     */
    public boolean getChild_idsDirtyFlag();
    /**
     * 获取 [颜色索引]
     */
    public void setColor(Integer color);
    
    /**
     * 设置 [颜色索引]
     */
    public Integer getColor();

    /**
     * 获取 [颜色索引]脏标记
     */
    public boolean getColorDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建于]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建于]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建于]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [分派日期]
     */
    public void setDate_assign(Timestamp date_assign);
    
    /**
     * 设置 [分派日期]
     */
    public Timestamp getDate_assign();

    /**
     * 获取 [分派日期]脏标记
     */
    public boolean getDate_assignDirtyFlag();
    /**
     * 获取 [截止日期]
     */
    public void setDate_deadline(Timestamp date_deadline);
    
    /**
     * 设置 [截止日期]
     */
    public Timestamp getDate_deadline();

    /**
     * 获取 [截止日期]脏标记
     */
    public boolean getDate_deadlineDirtyFlag();
    /**
     * 获取 [期末日期]
     */
    public void setDate_end(Timestamp date_end);
    
    /**
     * 设置 [期末日期]
     */
    public Timestamp getDate_end();

    /**
     * 获取 [期末日期]脏标记
     */
    public boolean getDate_endDirtyFlag();
    /**
     * 获取 [最后阶段更新]
     */
    public void setDate_last_stage_update(Timestamp date_last_stage_update);
    
    /**
     * 设置 [最后阶段更新]
     */
    public Timestamp getDate_last_stage_update();

    /**
     * 获取 [最后阶段更新]脏标记
     */
    public boolean getDate_last_stage_updateDirtyFlag();
    /**
     * 获取 [起始日期]
     */
    public void setDate_start(Timestamp date_start);
    
    /**
     * 设置 [起始日期]
     */
    public Timestamp getDate_start();

    /**
     * 获取 [起始日期]脏标记
     */
    public boolean getDate_startDirtyFlag();
    /**
     * 获取 [说明]
     */
    public void setDescription(String description);
    
    /**
     * 设置 [说明]
     */
    public String getDescription();

    /**
     * 获取 [说明]脏标记
     */
    public boolean getDescriptionDirtyFlag();
    /**
     * 获取 [封面图像]
     */
    public void setDisplayed_image_id(Integer displayed_image_id);
    
    /**
     * 设置 [封面图像]
     */
    public Integer getDisplayed_image_id();

    /**
     * 获取 [封面图像]脏标记
     */
    public boolean getDisplayed_image_idDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [关注者的EMail]
     */
    public void setEmail_cc(String email_cc);
    
    /**
     * 设置 [关注者的EMail]
     */
    public String getEmail_cc();

    /**
     * 获取 [关注者的EMail]脏标记
     */
    public boolean getEmail_ccDirtyFlag();
    /**
     * 获取 [EMail]
     */
    public void setEmail_from(String email_from);
    
    /**
     * 设置 [EMail]
     */
    public String getEmail_from();

    /**
     * 获取 [EMail]脏标记
     */
    public boolean getEmail_fromDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [看板状态]
     */
    public void setKanban_state(String kanban_state);
    
    /**
     * 设置 [看板状态]
     */
    public String getKanban_state();

    /**
     * 获取 [看板状态]脏标记
     */
    public boolean getKanban_stateDirtyFlag();
    /**
     * 获取 [看板状态标签]
     */
    public void setKanban_state_label(String kanban_state_label);
    
    /**
     * 设置 [看板状态标签]
     */
    public String getKanban_state_label();

    /**
     * 获取 [看板状态标签]脏标记
     */
    public boolean getKanban_state_labelDirtyFlag();
    /**
     * 获取 [看板阻塞说明]
     */
    public void setLegend_blocked(String legend_blocked);
    
    /**
     * 设置 [看板阻塞说明]
     */
    public String getLegend_blocked();

    /**
     * 获取 [看板阻塞说明]脏标记
     */
    public boolean getLegend_blockedDirtyFlag();
    /**
     * 获取 [看板有效解释]
     */
    public void setLegend_done(String legend_done);
    
    /**
     * 设置 [看板有效解释]
     */
    public String getLegend_done();

    /**
     * 获取 [看板有效解释]脏标记
     */
    public boolean getLegend_doneDirtyFlag();
    /**
     * 获取 [看板进展中说明]
     */
    public void setLegend_normal(String legend_normal);
    
    /**
     * 设置 [看板进展中说明]
     */
    public String getLegend_normal();

    /**
     * 获取 [看板进展中说明]脏标记
     */
    public boolean getLegend_normalDirtyFlag();
    /**
     * 获取 [项目管理员]
     */
    public void setManager_id(Integer manager_id);
    
    /**
     * 设置 [项目管理员]
     */
    public Integer getManager_id();

    /**
     * 获取 [项目管理员]脏标记
     */
    public boolean getManager_idDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [是关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [是关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [是关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要采取行动]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要采取行动]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要采取行动]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [行动数量]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [行动数量]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [行动数量]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [称谓]
     */
    public void setName(String name);
    
    /**
     * 设置 [称谓]
     */
    public String getName();

    /**
     * 获取 [称谓]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [便签]
     */
    public void setNotes(String notes);
    
    /**
     * 设置 [便签]
     */
    public String getNotes();

    /**
     * 获取 [便签]脏标记
     */
    public boolean getNotesDirtyFlag();
    /**
     * 获取 [上级任务]
     */
    public void setParent_id(Integer parent_id);
    
    /**
     * 设置 [上级任务]
     */
    public Integer getParent_id();

    /**
     * 获取 [上级任务]脏标记
     */
    public boolean getParent_idDirtyFlag();
    /**
     * 获取 [上级任务]
     */
    public void setParent_id_text(String parent_id_text);
    
    /**
     * 设置 [上级任务]
     */
    public String getParent_id_text();

    /**
     * 获取 [上级任务]脏标记
     */
    public boolean getParent_id_textDirtyFlag();
    /**
     * 获取 [客户]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [客户]
     */
    public Integer getPartner_id();

    /**
     * 获取 [客户]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [客户]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [客户]
     */
    public String getPartner_id_text();

    /**
     * 获取 [客户]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [已计划的时数]
     */
    public void setPlanned_hours(Double planned_hours);
    
    /**
     * 设置 [已计划的时数]
     */
    public Double getPlanned_hours();

    /**
     * 获取 [已计划的时数]脏标记
     */
    public boolean getPlanned_hoursDirtyFlag();
    /**
     * 获取 [优先级]
     */
    public void setPriority(String priority);
    
    /**
     * 设置 [优先级]
     */
    public String getPriority();

    /**
     * 获取 [优先级]脏标记
     */
    public boolean getPriorityDirtyFlag();
    /**
     * 获取 [项目]
     */
    public void setProject_id(Integer project_id);
    
    /**
     * 设置 [项目]
     */
    public Integer getProject_id();

    /**
     * 获取 [项目]脏标记
     */
    public boolean getProject_idDirtyFlag();
    /**
     * 获取 [项目]
     */
    public void setProject_id_text(String project_id_text);
    
    /**
     * 设置 [项目]
     */
    public String getProject_id_text();

    /**
     * 获取 [项目]脏标记
     */
    public boolean getProject_id_textDirtyFlag();
    /**
     * 获取 [点评数]
     */
    public void setRating_count(Integer rating_count);
    
    /**
     * 设置 [点评数]
     */
    public Integer getRating_count();

    /**
     * 获取 [点评数]脏标记
     */
    public boolean getRating_countDirtyFlag();
    /**
     * 获取 [点评]
     */
    public void setRating_ids(String rating_ids);
    
    /**
     * 设置 [点评]
     */
    public String getRating_ids();

    /**
     * 获取 [点评]脏标记
     */
    public boolean getRating_idsDirtyFlag();
    /**
     * 获取 [最新反馈评级]
     */
    public void setRating_last_feedback(String rating_last_feedback);
    
    /**
     * 设置 [最新反馈评级]
     */
    public String getRating_last_feedback();

    /**
     * 获取 [最新反馈评级]脏标记
     */
    public boolean getRating_last_feedbackDirtyFlag();
    /**
     * 获取 [最新图像评级]
     */
    public void setRating_last_image(byte[] rating_last_image);
    
    /**
     * 设置 [最新图像评级]
     */
    public byte[] getRating_last_image();

    /**
     * 获取 [最新图像评级]脏标记
     */
    public boolean getRating_last_imageDirtyFlag();
    /**
     * 获取 [最新值评级]
     */
    public void setRating_last_value(Double rating_last_value);
    
    /**
     * 设置 [最新值评级]
     */
    public Double getRating_last_value();

    /**
     * 获取 [最新值评级]脏标记
     */
    public boolean getRating_last_valueDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [阶段]
     */
    public void setStage_id(Integer stage_id);
    
    /**
     * 设置 [阶段]
     */
    public Integer getStage_id();

    /**
     * 获取 [阶段]脏标记
     */
    public boolean getStage_idDirtyFlag();
    /**
     * 获取 [阶段]
     */
    public void setStage_id_text(String stage_id_text);
    
    /**
     * 设置 [阶段]
     */
    public String getStage_id_text();

    /**
     * 获取 [阶段]脏标记
     */
    public boolean getStage_id_textDirtyFlag();
    /**
     * 获取 [子任务数]
     */
    public void setSubtask_count(Integer subtask_count);
    
    /**
     * 设置 [子任务数]
     */
    public Integer getSubtask_count();

    /**
     * 获取 [子任务数]脏标记
     */
    public boolean getSubtask_countDirtyFlag();
    /**
     * 获取 [子任务]
     */
    public void setSubtask_planned_hours(Double subtask_planned_hours);
    
    /**
     * 设置 [子任务]
     */
    public Double getSubtask_planned_hours();

    /**
     * 获取 [子任务]脏标记
     */
    public boolean getSubtask_planned_hoursDirtyFlag();
    /**
     * 获取 [子任务项目]
     */
    public void setSubtask_project_id(Integer subtask_project_id);
    
    /**
     * 设置 [子任务项目]
     */
    public Integer getSubtask_project_id();

    /**
     * 获取 [子任务项目]脏标记
     */
    public boolean getSubtask_project_idDirtyFlag();
    /**
     * 获取 [标签]
     */
    public void setTag_ids(String tag_ids);
    
    /**
     * 设置 [标签]
     */
    public String getTag_ids();

    /**
     * 获取 [标签]脏标记
     */
    public boolean getTag_idsDirtyFlag();
    /**
     * 获取 [用户EMail]
     */
    public void setUser_email(String user_email);
    
    /**
     * 设置 [用户EMail]
     */
    public String getUser_email();

    /**
     * 获取 [用户EMail]脏标记
     */
    public boolean getUser_emailDirtyFlag();
    /**
     * 获取 [分派给]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [分派给]
     */
    public Integer getUser_id();

    /**
     * 获取 [分派给]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [分派给]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [分派给]
     */
    public String getUser_id_text();

    /**
     * 获取 [分派给]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [网站信息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站信息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站信息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [工作日结束]
     */
    public void setWorking_days_close(Double working_days_close);
    
    /**
     * 设置 [工作日结束]
     */
    public Double getWorking_days_close();

    /**
     * 获取 [工作日结束]脏标记
     */
    public boolean getWorking_days_closeDirtyFlag();
    /**
     * 获取 [工作日分配]
     */
    public void setWorking_days_open(Double working_days_open);
    
    /**
     * 设置 [工作日分配]
     */
    public Double getWorking_days_open();

    /**
     * 获取 [工作日分配]脏标记
     */
    public boolean getWorking_days_openDirtyFlag();
    /**
     * 获取 [工作时间结束]
     */
    public void setWorking_hours_close(Double working_hours_close);
    
    /**
     * 设置 [工作时间结束]
     */
    public Double getWorking_hours_close();

    /**
     * 获取 [工作时间结束]脏标记
     */
    public boolean getWorking_hours_closeDirtyFlag();
    /**
     * 获取 [分配的工作时间]
     */
    public void setWorking_hours_open(Double working_hours_open);
    
    /**
     * 设置 [分配的工作时间]
     */
    public Double getWorking_hours_open();

    /**
     * 获取 [分配的工作时间]脏标记
     */
    public boolean getWorking_hours_openDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
