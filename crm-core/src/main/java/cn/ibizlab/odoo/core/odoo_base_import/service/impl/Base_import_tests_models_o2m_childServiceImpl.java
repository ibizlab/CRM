package cn.ibizlab.odoo.core.odoo_base_import.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_o2m_child;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_o2m_childSearchContext;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_tests_models_o2m_childService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base_import.client.base_import_tests_models_o2m_childFeignClient;

/**
 * 实体[测试:基本导入模型，一对多] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_import_tests_models_o2m_childServiceImpl implements IBase_import_tests_models_o2m_childService {

    @Autowired
    base_import_tests_models_o2m_childFeignClient base_import_tests_models_o2m_childFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=base_import_tests_models_o2m_childFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        base_import_tests_models_o2m_childFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Base_import_tests_models_o2m_child et) {
        Base_import_tests_models_o2m_child rt = base_import_tests_models_o2m_childFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_import_tests_models_o2m_child> list){
        base_import_tests_models_o2m_childFeignClient.createBatch(list) ;
    }

    @Override
    public Base_import_tests_models_o2m_child get(Integer id) {
		Base_import_tests_models_o2m_child et=base_import_tests_models_o2m_childFeignClient.get(id);
        if(et==null){
            et=new Base_import_tests_models_o2m_child();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Base_import_tests_models_o2m_child et) {
        Base_import_tests_models_o2m_child rt = base_import_tests_models_o2m_childFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Base_import_tests_models_o2m_child> list){
        base_import_tests_models_o2m_childFeignClient.updateBatch(list) ;
    }

    @Override
    public Base_import_tests_models_o2m_child getDraft(Base_import_tests_models_o2m_child et) {
        et=base_import_tests_models_o2m_childFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_import_tests_models_o2m_child> searchDefault(Base_import_tests_models_o2m_childSearchContext context) {
        Page<Base_import_tests_models_o2m_child> base_import_tests_models_o2m_childs=base_import_tests_models_o2m_childFeignClient.searchDefault(context);
        return base_import_tests_models_o2m_childs;
    }


}


