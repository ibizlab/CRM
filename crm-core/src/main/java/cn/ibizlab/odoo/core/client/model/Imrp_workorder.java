package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [mrp_workorder] 对象
 */
public interface Imrp_workorder {

    /**
     * 获取 [操作凭证行]
     */
    public void setActive_move_line_ids(String active_move_line_ids);
    
    /**
     * 设置 [操作凭证行]
     */
    public String getActive_move_line_ids();

    /**
     * 获取 [操作凭证行]脏标记
     */
    public boolean getActive_move_line_idsDirtyFlag();
    /**
     * 获取 [容量]
     */
    public void setCapacity(Double capacity);
    
    /**
     * 设置 [容量]
     */
    public Double getCapacity();

    /**
     * 获取 [容量]脏标记
     */
    public boolean getCapacityDirtyFlag();
    /**
     * 获取 [颜色]
     */
    public void setColor(Integer color);
    
    /**
     * 设置 [颜色]
     */
    public Integer getColor();

    /**
     * 获取 [颜色]脏标记
     */
    public boolean getColorDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [实际结束日期]
     */
    public void setDate_finished(Timestamp date_finished);
    
    /**
     * 设置 [实际结束日期]
     */
    public Timestamp getDate_finished();

    /**
     * 获取 [实际结束日期]脏标记
     */
    public boolean getDate_finishedDirtyFlag();
    /**
     * 获取 [安排的完工日期]
     */
    public void setDate_planned_finished(Timestamp date_planned_finished);
    
    /**
     * 设置 [安排的完工日期]
     */
    public Timestamp getDate_planned_finished();

    /**
     * 获取 [安排的完工日期]脏标记
     */
    public boolean getDate_planned_finishedDirtyFlag();
    /**
     * 获取 [安排的开始日期]
     */
    public void setDate_planned_start(Timestamp date_planned_start);
    
    /**
     * 设置 [安排的开始日期]
     */
    public Timestamp getDate_planned_start();

    /**
     * 获取 [安排的开始日期]脏标记
     */
    public boolean getDate_planned_startDirtyFlag();
    /**
     * 获取 [实际开始日期]
     */
    public void setDate_start(Timestamp date_start);
    
    /**
     * 设置 [实际开始日期]
     */
    public Timestamp getDate_start();

    /**
     * 获取 [实际开始日期]脏标记
     */
    public boolean getDate_startDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [实际时长]
     */
    public void setDuration(Double duration);
    
    /**
     * 设置 [实际时长]
     */
    public Double getDuration();

    /**
     * 获取 [实际时长]脏标记
     */
    public boolean getDurationDirtyFlag();
    /**
     * 获取 [预计时长]
     */
    public void setDuration_expected(Double duration_expected);
    
    /**
     * 设置 [预计时长]
     */
    public Double getDuration_expected();

    /**
     * 获取 [预计时长]脏标记
     */
    public boolean getDuration_expectedDirtyFlag();
    /**
     * 获取 [时长偏差(%)]
     */
    public void setDuration_percent(Integer duration_percent);
    
    /**
     * 设置 [时长偏差(%)]
     */
    public Integer getDuration_percent();

    /**
     * 获取 [时长偏差(%)]脏标记
     */
    public boolean getDuration_percentDirtyFlag();
    /**
     * 获取 [每单位时长]
     */
    public void setDuration_unit(Double duration_unit);
    
    /**
     * 设置 [每单位时长]
     */
    public Double getDuration_unit();

    /**
     * 获取 [每单位时长]脏标记
     */
    public boolean getDuration_unitDirtyFlag();
    /**
     * 获取 [批次/序列号码]
     */
    public void setFinal_lot_id(Integer final_lot_id);
    
    /**
     * 设置 [批次/序列号码]
     */
    public Integer getFinal_lot_id();

    /**
     * 获取 [批次/序列号码]脏标记
     */
    public boolean getFinal_lot_idDirtyFlag();
    /**
     * 获取 [批次/序列号码]
     */
    public void setFinal_lot_id_text(String final_lot_id_text);
    
    /**
     * 设置 [批次/序列号码]
     */
    public String getFinal_lot_id_text();

    /**
     * 获取 [批次/序列号码]脏标记
     */
    public boolean getFinal_lot_id_textDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [Is the first WO to produce]
     */
    public void setIs_first_wo(String is_first_wo);
    
    /**
     * 设置 [Is the first WO to produce]
     */
    public String getIs_first_wo();

    /**
     * 获取 [Is the first WO to produce]脏标记
     */
    public boolean getIs_first_woDirtyFlag();
    /**
     * 获取 [已生产]
     */
    public void setIs_produced(String is_produced);
    
    /**
     * 设置 [已生产]
     */
    public String getIs_produced();

    /**
     * 获取 [已生产]脏标记
     */
    public boolean getIs_producedDirtyFlag();
    /**
     * 获取 [当前用户正在工作吗？]
     */
    public void setIs_user_working(String is_user_working);
    
    /**
     * 设置 [当前用户正在工作吗？]
     */
    public String getIs_user_working();

    /**
     * 获取 [当前用户正在工作吗？]脏标记
     */
    public boolean getIs_user_workingDirtyFlag();
    /**
     * 获取 [上一个在此工单工作的用户]
     */
    public void setLast_working_user_id(String last_working_user_id);
    
    /**
     * 设置 [上一个在此工单工作的用户]
     */
    public String getLast_working_user_id();

    /**
     * 获取 [上一个在此工单工作的用户]脏标记
     */
    public boolean getLast_working_user_idDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [是关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [是关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [是关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要采取行动]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要采取行动]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要采取行动]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [行动数量]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [行动数量]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [行动数量]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [待追踪的产品]
     */
    public void setMove_line_ids(String move_line_ids);
    
    /**
     * 设置 [待追踪的产品]
     */
    public String getMove_line_ids();

    /**
     * 获取 [待追踪的产品]脏标记
     */
    public boolean getMove_line_idsDirtyFlag();
    /**
     * 获取 [移动]
     */
    public void setMove_raw_ids(String move_raw_ids);
    
    /**
     * 设置 [移动]
     */
    public String getMove_raw_ids();

    /**
     * 获取 [移动]脏标记
     */
    public boolean getMove_raw_idsDirtyFlag();
    /**
     * 获取 [工单]
     */
    public void setName(String name);
    
    /**
     * 设置 [工单]
     */
    public String getName();

    /**
     * 获取 [工单]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [下一工单]
     */
    public void setNext_work_order_id(Integer next_work_order_id);
    
    /**
     * 设置 [下一工单]
     */
    public Integer getNext_work_order_id();

    /**
     * 获取 [下一工单]脏标记
     */
    public boolean getNext_work_order_idDirtyFlag();
    /**
     * 获取 [下一工单]
     */
    public void setNext_work_order_id_text(String next_work_order_id_text);
    
    /**
     * 设置 [下一工单]
     */
    public String getNext_work_order_id_text();

    /**
     * 获取 [下一工单]脏标记
     */
    public boolean getNext_work_order_id_textDirtyFlag();
    /**
     * 获取 [操作]
     */
    public void setOperation_id(Integer operation_id);
    
    /**
     * 设置 [操作]
     */
    public Integer getOperation_id();

    /**
     * 获取 [操作]脏标记
     */
    public boolean getOperation_idDirtyFlag();
    /**
     * 获取 [操作]
     */
    public void setOperation_id_text(String operation_id_text);
    
    /**
     * 设置 [操作]
     */
    public String getOperation_id_text();

    /**
     * 获取 [操作]脏标记
     */
    public boolean getOperation_id_textDirtyFlag();
    /**
     * 获取 [材料可用性]
     */
    public void setProduction_availability(String production_availability);
    
    /**
     * 设置 [材料可用性]
     */
    public String getProduction_availability();

    /**
     * 获取 [材料可用性]脏标记
     */
    public boolean getProduction_availabilityDirtyFlag();
    /**
     * 获取 [生产日期]
     */
    public void setProduction_date(Timestamp production_date);
    
    /**
     * 设置 [生产日期]
     */
    public Timestamp getProduction_date();

    /**
     * 获取 [生产日期]脏标记
     */
    public boolean getProduction_dateDirtyFlag();
    /**
     * 获取 [制造订单]
     */
    public void setProduction_id(Integer production_id);
    
    /**
     * 设置 [制造订单]
     */
    public Integer getProduction_id();

    /**
     * 获取 [制造订单]脏标记
     */
    public boolean getProduction_idDirtyFlag();
    /**
     * 获取 [制造订单]
     */
    public void setProduction_id_text(String production_id_text);
    
    /**
     * 设置 [制造订单]
     */
    public String getProduction_id_text();

    /**
     * 获取 [制造订单]脏标记
     */
    public boolean getProduction_id_textDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setProduction_state(String production_state);
    
    /**
     * 设置 [状态]
     */
    public String getProduction_state();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getProduction_stateDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id(Integer product_id);
    
    /**
     * 设置 [产品]
     */
    public Integer getProduct_id();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_idDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id_text(String product_id_text);
    
    /**
     * 设置 [产品]
     */
    public String getProduct_id_text();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_id_textDirtyFlag();
    /**
     * 获取 [追踪]
     */
    public void setProduct_tracking(String product_tracking);
    
    /**
     * 设置 [追踪]
     */
    public String getProduct_tracking();

    /**
     * 获取 [追踪]脏标记
     */
    public boolean getProduct_trackingDirtyFlag();
    /**
     * 获取 [计量单位]
     */
    public void setProduct_uom_id(Integer product_uom_id);
    
    /**
     * 设置 [计量单位]
     */
    public Integer getProduct_uom_id();

    /**
     * 获取 [计量单位]脏标记
     */
    public boolean getProduct_uom_idDirtyFlag();
    /**
     * 获取 [数量]
     */
    public void setQty_produced(Double qty_produced);
    
    /**
     * 设置 [数量]
     */
    public Double getQty_produced();

    /**
     * 获取 [数量]脏标记
     */
    public boolean getQty_producedDirtyFlag();
    /**
     * 获取 [当前的已生产数量]
     */
    public void setQty_producing(Double qty_producing);
    
    /**
     * 设置 [当前的已生产数量]
     */
    public Double getQty_producing();

    /**
     * 获取 [当前的已生产数量]脏标记
     */
    public boolean getQty_producingDirtyFlag();
    /**
     * 获取 [原始生产数量]
     */
    public void setQty_production(Double qty_production);
    
    /**
     * 设置 [原始生产数量]
     */
    public Double getQty_production();

    /**
     * 获取 [原始生产数量]脏标记
     */
    public boolean getQty_productionDirtyFlag();
    /**
     * 获取 [将被生产的数量]
     */
    public void setQty_remaining(Double qty_remaining);
    
    /**
     * 设置 [将被生产的数量]
     */
    public Double getQty_remaining();

    /**
     * 获取 [将被生产的数量]脏标记
     */
    public boolean getQty_remainingDirtyFlag();
    /**
     * 获取 [报废转移]
     */
    public void setScrap_count(Integer scrap_count);
    
    /**
     * 设置 [报废转移]
     */
    public Integer getScrap_count();

    /**
     * 获取 [报废转移]脏标记
     */
    public boolean getScrap_countDirtyFlag();
    /**
     * 获取 [报废]
     */
    public void setScrap_ids(String scrap_ids);
    
    /**
     * 设置 [报废]
     */
    public String getScrap_ids();

    /**
     * 获取 [报废]脏标记
     */
    public boolean getScrap_idsDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [时间]
     */
    public void setTime_ids(String time_ids);
    
    /**
     * 设置 [时间]
     */
    public String getTime_ids();

    /**
     * 获取 [时间]脏标记
     */
    public boolean getTime_idsDirtyFlag();
    /**
     * 获取 [网站信息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站信息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站信息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [工作中心]
     */
    public void setWorkcenter_id(Integer workcenter_id);
    
    /**
     * 设置 [工作中心]
     */
    public Integer getWorkcenter_id();

    /**
     * 获取 [工作中心]脏标记
     */
    public boolean getWorkcenter_idDirtyFlag();
    /**
     * 获取 [工作中心]
     */
    public void setWorkcenter_id_text(String workcenter_id_text);
    
    /**
     * 设置 [工作中心]
     */
    public String getWorkcenter_id_text();

    /**
     * 获取 [工作中心]脏标记
     */
    public boolean getWorkcenter_id_textDirtyFlag();
    /**
     * 获取 [工作中心状态]
     */
    public void setWorking_state(String working_state);
    
    /**
     * 设置 [工作中心状态]
     */
    public String getWorking_state();

    /**
     * 获取 [工作中心状态]脏标记
     */
    public boolean getWorking_stateDirtyFlag();
    /**
     * 获取 [在此工单工作的用户]
     */
    public void setWorking_user_ids(String working_user_ids);
    
    /**
     * 设置 [在此工单工作的用户]
     */
    public String getWorking_user_ids();

    /**
     * 获取 [在此工单工作的用户]脏标记
     */
    public boolean getWorking_user_idsDirtyFlag();
    /**
     * 获取 [工作记录表]
     */
    public void setWorksheet(byte[] worksheet);
    
    /**
     * 设置 [工作记录表]
     */
    public byte[] getWorksheet();

    /**
     * 获取 [工作记录表]脏标记
     */
    public boolean getWorksheetDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
