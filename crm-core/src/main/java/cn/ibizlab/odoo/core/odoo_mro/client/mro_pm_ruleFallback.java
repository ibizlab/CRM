package cn.ibizlab.odoo.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_rule;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_ruleSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mro_pm_rule] 服务对象接口
 */
@Component
public class mro_pm_ruleFallback implements mro_pm_ruleFeignClient{

    public Mro_pm_rule create(Mro_pm_rule mro_pm_rule){
            return null;
     }
    public Boolean createBatch(List<Mro_pm_rule> mro_pm_rules){
            return false;
     }

    public Mro_pm_rule get(Integer id){
            return null;
     }




    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Mro_pm_rule update(Integer id, Mro_pm_rule mro_pm_rule){
            return null;
     }
    public Boolean updateBatch(List<Mro_pm_rule> mro_pm_rules){
            return false;
     }


    public Page<Mro_pm_rule> searchDefault(Mro_pm_ruleSearchContext context){
            return null;
     }



    public Page<Mro_pm_rule> select(){
            return null;
     }

    public Mro_pm_rule getDraft(){
            return null;
    }



}
