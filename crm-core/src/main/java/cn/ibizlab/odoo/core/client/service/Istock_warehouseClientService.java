package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_warehouse;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_warehouse] 服务对象接口
 */
public interface Istock_warehouseClientService{

    public Istock_warehouse createModel() ;

    public void updateBatch(List<Istock_warehouse> stock_warehouses);

    public void remove(Istock_warehouse stock_warehouse);

    public Page<Istock_warehouse> fetchDefault(SearchContext context);

    public void update(Istock_warehouse stock_warehouse);

    public void create(Istock_warehouse stock_warehouse);

    public void createBatch(List<Istock_warehouse> stock_warehouses);

    public void removeBatch(List<Istock_warehouse> stock_warehouses);

    public void get(Istock_warehouse stock_warehouse);

    public Page<Istock_warehouse> select(SearchContext context);

    public void getDraft(Istock_warehouse stock_warehouse);

}
