package cn.ibizlab.odoo.core.odoo_digest.valuerule.anno.digest_digest;

import cn.ibizlab.odoo.core.odoo_digest.valuerule.validator.digest_digest.Digest_digestKpi_account_total_revenue_valueDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Digest_digest
 * 属性：Kpi_account_total_revenue_value
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Digest_digestKpi_account_total_revenue_valueDefaultValidator.class})
public @interface Digest_digestKpi_account_total_revenue_valueDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
