package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [product_pricelist_item] 对象
 */
public interface Iproduct_pricelist_item {

    /**
     * 获取 [应用于]
     */
    public void setApplied_on(String applied_on);
    
    /**
     * 设置 [应用于]
     */
    public String getApplied_on();

    /**
     * 获取 [应用于]脏标记
     */
    public boolean getApplied_onDirtyFlag();
    /**
     * 获取 [基于]
     */
    public void setBase(String base);
    
    /**
     * 设置 [基于]
     */
    public String getBase();

    /**
     * 获取 [基于]脏标记
     */
    public boolean getBaseDirtyFlag();
    /**
     * 获取 [其他价格表]
     */
    public void setBase_pricelist_id(Integer base_pricelist_id);
    
    /**
     * 设置 [其他价格表]
     */
    public Integer getBase_pricelist_id();

    /**
     * 获取 [其他价格表]脏标记
     */
    public boolean getBase_pricelist_idDirtyFlag();
    /**
     * 获取 [其他价格表]
     */
    public void setBase_pricelist_id_text(String base_pricelist_id_text);
    
    /**
     * 设置 [其他价格表]
     */
    public String getBase_pricelist_id_text();

    /**
     * 获取 [其他价格表]脏标记
     */
    public boolean getBase_pricelist_id_textDirtyFlag();
    /**
     * 获取 [产品种类]
     */
    public void setCateg_id(Integer categ_id);
    
    /**
     * 设置 [产品种类]
     */
    public Integer getCateg_id();

    /**
     * 获取 [产品种类]脏标记
     */
    public boolean getCateg_idDirtyFlag();
    /**
     * 获取 [产品种类]
     */
    public void setCateg_id_text(String categ_id_text);
    
    /**
     * 设置 [产品种类]
     */
    public String getCateg_id_text();

    /**
     * 获取 [产品种类]脏标记
     */
    public boolean getCateg_id_textDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [计算价格]
     */
    public void setCompute_price(String compute_price);
    
    /**
     * 设置 [计算价格]
     */
    public String getCompute_price();

    /**
     * 获取 [计算价格]脏标记
     */
    public boolean getCompute_priceDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id_text(String currency_id_text);
    
    /**
     * 设置 [币种]
     */
    public String getCurrency_id_text();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_id_textDirtyFlag();
    /**
     * 获取 [结束日期]
     */
    public void setDate_end(Timestamp date_end);
    
    /**
     * 设置 [结束日期]
     */
    public Timestamp getDate_end();

    /**
     * 获取 [结束日期]脏标记
     */
    public boolean getDate_endDirtyFlag();
    /**
     * 获取 [开始日期]
     */
    public void setDate_start(Timestamp date_start);
    
    /**
     * 设置 [开始日期]
     */
    public Timestamp getDate_start();

    /**
     * 获取 [开始日期]脏标记
     */
    public boolean getDate_startDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [固定价格]
     */
    public void setFixed_price(Double fixed_price);
    
    /**
     * 设置 [固定价格]
     */
    public Double getFixed_price();

    /**
     * 获取 [固定价格]脏标记
     */
    public boolean getFixed_priceDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [最小数量]
     */
    public void setMin_quantity(Integer min_quantity);
    
    /**
     * 设置 [最小数量]
     */
    public Integer getMin_quantity();

    /**
     * 获取 [最小数量]脏标记
     */
    public boolean getMin_quantityDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [百分比价格]
     */
    public void setPercent_price(Double percent_price);
    
    /**
     * 设置 [百分比价格]
     */
    public Double getPercent_price();

    /**
     * 获取 [百分比价格]脏标记
     */
    public boolean getPercent_priceDirtyFlag();
    /**
     * 获取 [价格]
     */
    public void setPrice(String price);
    
    /**
     * 设置 [价格]
     */
    public String getPrice();

    /**
     * 获取 [价格]脏标记
     */
    public boolean getPriceDirtyFlag();
    /**
     * 获取 [价格表]
     */
    public void setPricelist_id(Integer pricelist_id);
    
    /**
     * 设置 [价格表]
     */
    public Integer getPricelist_id();

    /**
     * 获取 [价格表]脏标记
     */
    public boolean getPricelist_idDirtyFlag();
    /**
     * 获取 [价格表]
     */
    public void setPricelist_id_text(String pricelist_id_text);
    
    /**
     * 设置 [价格表]
     */
    public String getPricelist_id_text();

    /**
     * 获取 [价格表]脏标记
     */
    public boolean getPricelist_id_textDirtyFlag();
    /**
     * 获取 [价格折扣]
     */
    public void setPrice_discount(Double price_discount);
    
    /**
     * 设置 [价格折扣]
     */
    public Double getPrice_discount();

    /**
     * 获取 [价格折扣]脏标记
     */
    public boolean getPrice_discountDirtyFlag();
    /**
     * 获取 [最大价格毛利]
     */
    public void setPrice_max_margin(Double price_max_margin);
    
    /**
     * 设置 [最大价格毛利]
     */
    public Double getPrice_max_margin();

    /**
     * 获取 [最大价格毛利]脏标记
     */
    public boolean getPrice_max_marginDirtyFlag();
    /**
     * 获取 [最小价格毛利]
     */
    public void setPrice_min_margin(Double price_min_margin);
    
    /**
     * 设置 [最小价格毛利]
     */
    public Double getPrice_min_margin();

    /**
     * 获取 [最小价格毛利]脏标记
     */
    public boolean getPrice_min_marginDirtyFlag();
    /**
     * 获取 [价格舍入]
     */
    public void setPrice_round(Double price_round);
    
    /**
     * 设置 [价格舍入]
     */
    public Double getPrice_round();

    /**
     * 获取 [价格舍入]脏标记
     */
    public boolean getPrice_roundDirtyFlag();
    /**
     * 获取 [价格附加费用]
     */
    public void setPrice_surcharge(Double price_surcharge);
    
    /**
     * 设置 [价格附加费用]
     */
    public Double getPrice_surcharge();

    /**
     * 获取 [价格附加费用]脏标记
     */
    public boolean getPrice_surchargeDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id(Integer product_id);
    
    /**
     * 设置 [产品]
     */
    public Integer getProduct_id();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_idDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id_text(String product_id_text);
    
    /**
     * 设置 [产品]
     */
    public String getProduct_id_text();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_id_textDirtyFlag();
    /**
     * 获取 [产品模板]
     */
    public void setProduct_tmpl_id(Integer product_tmpl_id);
    
    /**
     * 设置 [产品模板]
     */
    public Integer getProduct_tmpl_id();

    /**
     * 获取 [产品模板]脏标记
     */
    public boolean getProduct_tmpl_idDirtyFlag();
    /**
     * 获取 [产品模板]
     */
    public void setProduct_tmpl_id_text(String product_tmpl_id_text);
    
    /**
     * 设置 [产品模板]
     */
    public String getProduct_tmpl_id_text();

    /**
     * 获取 [产品模板]脏标记
     */
    public boolean getProduct_tmpl_id_textDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
