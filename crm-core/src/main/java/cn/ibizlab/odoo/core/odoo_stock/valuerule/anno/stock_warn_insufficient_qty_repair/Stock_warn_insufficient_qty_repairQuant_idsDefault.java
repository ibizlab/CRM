package cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_warn_insufficient_qty_repair;

import cn.ibizlab.odoo.core.odoo_stock.valuerule.validator.stock_warn_insufficient_qty_repair.Stock_warn_insufficient_qty_repairQuant_idsDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Stock_warn_insufficient_qty_repair
 * 属性：Quant_ids
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Stock_warn_insufficient_qty_repairQuant_idsDefaultValidator.class})
public @interface Stock_warn_insufficient_qty_repairQuant_idsDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[1048576]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
