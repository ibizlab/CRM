package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_recruitment_degree;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_recruitment_degreeSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_recruitment_degreeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_hr.client.hr_recruitment_degreeFeignClient;

/**
 * 实体[申请人学历] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_recruitment_degreeServiceImpl implements IHr_recruitment_degreeService {

    @Autowired
    hr_recruitment_degreeFeignClient hr_recruitment_degreeFeignClient;


    @Override
    public Hr_recruitment_degree getDraft(Hr_recruitment_degree et) {
        et=hr_recruitment_degreeFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Hr_recruitment_degree et) {
        Hr_recruitment_degree rt = hr_recruitment_degreeFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Hr_recruitment_degree> list){
        hr_recruitment_degreeFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=hr_recruitment_degreeFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        hr_recruitment_degreeFeignClient.removeBatch(idList);
    }

    @Override
    public Hr_recruitment_degree get(Integer id) {
		Hr_recruitment_degree et=hr_recruitment_degreeFeignClient.get(id);
        if(et==null){
            et=new Hr_recruitment_degree();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Hr_recruitment_degree et) {
        Hr_recruitment_degree rt = hr_recruitment_degreeFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_recruitment_degree> list){
        hr_recruitment_degreeFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_recruitment_degree> searchDefault(Hr_recruitment_degreeSearchContext context) {
        Page<Hr_recruitment_degree> hr_recruitment_degrees=hr_recruitment_degreeFeignClient.searchDefault(context);
        return hr_recruitment_degrees;
    }


}


