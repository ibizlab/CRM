package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_positionSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_fiscal_positionService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_fiscal_positionFeignClient;

/**
 * 实体[税科目调整] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_fiscal_positionServiceImpl implements IAccount_fiscal_positionService {

    @Autowired
    account_fiscal_positionFeignClient account_fiscal_positionFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=account_fiscal_positionFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_fiscal_positionFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Account_fiscal_position et) {
        Account_fiscal_position rt = account_fiscal_positionFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_fiscal_position> list){
        account_fiscal_positionFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Account_fiscal_position et) {
        Account_fiscal_position rt = account_fiscal_positionFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_fiscal_position> list){
        account_fiscal_positionFeignClient.updateBatch(list) ;
    }

    @Override
    public Account_fiscal_position get(Integer id) {
		Account_fiscal_position et=account_fiscal_positionFeignClient.get(id);
        if(et==null){
            et=new Account_fiscal_position();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Account_fiscal_position getDraft(Account_fiscal_position et) {
        et=account_fiscal_positionFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_fiscal_position> searchDefault(Account_fiscal_positionSearchContext context) {
        Page<Account_fiscal_position> account_fiscal_positions=account_fiscal_positionFeignClient.searchDefault(context);
        return account_fiscal_positions;
    }


}


