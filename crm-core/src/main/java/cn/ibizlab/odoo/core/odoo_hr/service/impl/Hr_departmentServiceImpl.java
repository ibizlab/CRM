package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_department;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_departmentSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_departmentService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_hr.client.hr_departmentFeignClient;

/**
 * 实体[HR 部门] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_departmentServiceImpl implements IHr_departmentService {

    @Autowired
    hr_departmentFeignClient hr_departmentFeignClient;


    @Override
    public Hr_department getDraft(Hr_department et) {
        et=hr_departmentFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Hr_department et) {
        Hr_department rt = hr_departmentFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Hr_department> list){
        hr_departmentFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Hr_department et) {
        Hr_department rt = hr_departmentFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_department> list){
        hr_departmentFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=hr_departmentFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        hr_departmentFeignClient.removeBatch(idList);
    }

    @Override
    public Hr_department get(Integer id) {
		Hr_department et=hr_departmentFeignClient.get(id);
        if(et==null){
            et=new Hr_department();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_department> searchDefault(Hr_departmentSearchContext context) {
        Page<Hr_department> hr_departments=hr_departmentFeignClient.searchDefault(context);
        return hr_departments;
    }


}


