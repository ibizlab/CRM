package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_bank;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_bankSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_partner_bankService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.res_partner_bankFeignClient;

/**
 * 实体[银行账户] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_partner_bankServiceImpl implements IRes_partner_bankService {

    @Autowired
    res_partner_bankFeignClient res_partner_bankFeignClient;


    @Override
    public boolean update(Res_partner_bank et) {
        Res_partner_bank rt = res_partner_bankFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Res_partner_bank> list){
        res_partner_bankFeignClient.updateBatch(list) ;
    }

    @Override
    public Res_partner_bank getDraft(Res_partner_bank et) {
        et=res_partner_bankFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=res_partner_bankFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        res_partner_bankFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Res_partner_bank et) {
        Res_partner_bank rt = res_partner_bankFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_partner_bank> list){
        res_partner_bankFeignClient.createBatch(list) ;
    }

    @Override
    public Res_partner_bank get(Integer id) {
		Res_partner_bank et=res_partner_bankFeignClient.get(id);
        if(et==null){
            et=new Res_partner_bank();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_partner_bank> searchDefault(Res_partner_bankSearchContext context) {
        Page<Res_partner_bank> res_partner_banks=res_partner_bankFeignClient.searchDefault(context);
        return res_partner_banks;
    }


}


