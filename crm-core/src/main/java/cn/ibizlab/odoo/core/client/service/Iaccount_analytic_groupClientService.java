package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_analytic_group;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_analytic_group] 服务对象接口
 */
public interface Iaccount_analytic_groupClientService{

    public Iaccount_analytic_group createModel() ;

    public void update(Iaccount_analytic_group account_analytic_group);

    public Page<Iaccount_analytic_group> fetchDefault(SearchContext context);

    public void remove(Iaccount_analytic_group account_analytic_group);

    public void createBatch(List<Iaccount_analytic_group> account_analytic_groups);

    public void create(Iaccount_analytic_group account_analytic_group);

    public void get(Iaccount_analytic_group account_analytic_group);

    public void updateBatch(List<Iaccount_analytic_group> account_analytic_groups);

    public void removeBatch(List<Iaccount_analytic_group> account_analytic_groups);

    public Page<Iaccount_analytic_group> select(SearchContext context);

    public void getDraft(Iaccount_analytic_group account_analytic_group);

}
