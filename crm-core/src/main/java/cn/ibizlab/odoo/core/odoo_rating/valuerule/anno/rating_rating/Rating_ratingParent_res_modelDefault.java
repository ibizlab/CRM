package cn.ibizlab.odoo.core.odoo_rating.valuerule.anno.rating_rating;

import cn.ibizlab.odoo.core.odoo_rating.valuerule.validator.rating_rating.Rating_ratingParent_res_modelDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Rating_rating
 * 属性：Parent_res_model
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Rating_ratingParent_res_modelDefaultValidator.class})
public @interface Rating_ratingParent_res_modelDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[100]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
