package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_resend_partner;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_resend_partner] 服务对象接口
 */
public interface Imail_resend_partnerClientService{

    public Imail_resend_partner createModel() ;

    public void get(Imail_resend_partner mail_resend_partner);

    public void update(Imail_resend_partner mail_resend_partner);

    public void updateBatch(List<Imail_resend_partner> mail_resend_partners);

    public void removeBatch(List<Imail_resend_partner> mail_resend_partners);

    public Page<Imail_resend_partner> fetchDefault(SearchContext context);

    public void create(Imail_resend_partner mail_resend_partner);

    public void createBatch(List<Imail_resend_partner> mail_resend_partners);

    public void remove(Imail_resend_partner mail_resend_partner);

    public Page<Imail_resend_partner> select(SearchContext context);

    public void getDraft(Imail_resend_partner mail_resend_partner);

}
