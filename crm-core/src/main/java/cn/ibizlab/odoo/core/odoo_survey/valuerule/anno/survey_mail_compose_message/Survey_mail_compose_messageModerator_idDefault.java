package cn.ibizlab.odoo.core.odoo_survey.valuerule.anno.survey_mail_compose_message;

import cn.ibizlab.odoo.core.odoo_survey.valuerule.validator.survey_mail_compose_message.Survey_mail_compose_messageModerator_idDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Survey_mail_compose_message
 * 属性：Moderator_id
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Survey_mail_compose_messageModerator_idDefaultValidator.class})
public @interface Survey_mail_compose_messageModerator_idDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
