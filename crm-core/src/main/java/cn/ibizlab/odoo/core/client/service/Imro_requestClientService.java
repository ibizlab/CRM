package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imro_request;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mro_request] 服务对象接口
 */
public interface Imro_requestClientService{

    public Imro_request createModel() ;

    public void remove(Imro_request mro_request);

    public void create(Imro_request mro_request);

    public void removeBatch(List<Imro_request> mro_requests);

    public void update(Imro_request mro_request);

    public void createBatch(List<Imro_request> mro_requests);

    public void updateBatch(List<Imro_request> mro_requests);

    public Page<Imro_request> fetchDefault(SearchContext context);

    public void get(Imro_request mro_request);

    public Page<Imro_request> select(SearchContext context);

    public void getDraft(Imro_request mro_request);

}
