package cn.ibizlab.odoo.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_m2o;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_m2oSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[base_import_tests_models_m2o] 服务对象接口
 */
@Component
public class base_import_tests_models_m2oFallback implements base_import_tests_models_m2oFeignClient{

    public Base_import_tests_models_m2o create(Base_import_tests_models_m2o base_import_tests_models_m2o){
            return null;
     }
    public Boolean createBatch(List<Base_import_tests_models_m2o> base_import_tests_models_m2os){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Base_import_tests_models_m2o get(Integer id){
            return null;
     }


    public Base_import_tests_models_m2o update(Integer id, Base_import_tests_models_m2o base_import_tests_models_m2o){
            return null;
     }
    public Boolean updateBatch(List<Base_import_tests_models_m2o> base_import_tests_models_m2os){
            return false;
     }




    public Page<Base_import_tests_models_m2o> searchDefault(Base_import_tests_models_m2oSearchContext context){
            return null;
     }


    public Page<Base_import_tests_models_m2o> select(){
            return null;
     }

    public Base_import_tests_models_m2o getDraft(){
            return null;
    }



}
