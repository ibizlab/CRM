package cn.ibizlab.odoo.core.odoo_stock.valuerule.validator.stock_warn_insufficient_qty_repair;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import cn.ibizlab.odoo.util.valuerule.DefaultValueRule;
import cn.ibizlab.odoo.util.valuerule.VRCondition;
import cn.ibizlab.odoo.util.valuerule.condition.*;
import cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_warn_insufficient_qty_repair.Stock_warn_insufficient_qty_repair__last_updateDefault;
import cn.ibizlab.odoo.util.log.IBIZLog;

/**
 * 值规则注解解析类
 * 实体：Stock_warn_insufficient_qty_repair
 * 属性：__last_update
 * 值规则：Default
 * 值规则信息：默认规则
 */
@Slf4j
@IBIZLog
@Component("Stock_warn_insufficient_qty_repair__last_updateDefaultValidator")
public class Stock_warn_insufficient_qty_repair__last_updateDefaultValidator implements ConstraintValidator<Stock_warn_insufficient_qty_repair__last_updateDefault, Timestamp>,Validator {
    private static final String MESSAGE = "值规则校验失败：【默认规则】";

    @Override
    public boolean isValid(Timestamp value, ConstraintValidatorContext context) {
        boolean isValid = doValidate(value);
        if(!isValid) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(MESSAGE)
                    .addConstraintViolation();
        }
        return doValidate(value);
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }

    @Override
    public void validate(Object o, Errors errors) {
        if( o!=null && supports(o.getClass())){
            if (!doValidate((Timestamp) o)){
                errors.reject(MESSAGE);
            }
        }
    }

    public boolean doValidate(Timestamp value) {
        DefaultValueRule<Timestamp> valueRule = new DefaultValueRule<>("默认值规则",MESSAGE,"__last_update",value)
                //字符串长度，重复检查模式，重复值范围，基础值规则，是否递归检查。
                .init(-1,"NONE",null,null,false);
        return valueRule.isValid();
    }
}

