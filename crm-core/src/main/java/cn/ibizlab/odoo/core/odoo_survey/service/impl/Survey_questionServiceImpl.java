package cn.ibizlab.odoo.core.odoo_survey.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_question;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_questionSearchContext;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_questionService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_survey.client.survey_questionFeignClient;

/**
 * 实体[调查问题] 服务对象接口实现
 */
@Slf4j
@Service
public class Survey_questionServiceImpl implements ISurvey_questionService {

    @Autowired
    survey_questionFeignClient survey_questionFeignClient;


    @Override
    public boolean create(Survey_question et) {
        Survey_question rt = survey_questionFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Survey_question> list){
        survey_questionFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Survey_question et) {
        Survey_question rt = survey_questionFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Survey_question> list){
        survey_questionFeignClient.updateBatch(list) ;
    }

    @Override
    public Survey_question getDraft(Survey_question et) {
        et=survey_questionFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=survey_questionFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        survey_questionFeignClient.removeBatch(idList);
    }

    @Override
    public Survey_question get(Integer id) {
		Survey_question et=survey_questionFeignClient.get(id);
        if(et==null){
            et=new Survey_question();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Survey_question> searchDefault(Survey_questionSearchContext context) {
        Page<Survey_question> survey_questions=survey_questionFeignClient.searchDefault(context);
        return survey_questions;
    }


}


