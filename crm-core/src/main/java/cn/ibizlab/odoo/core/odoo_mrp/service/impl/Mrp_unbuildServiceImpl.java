package cn.ibizlab.odoo.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_unbuild;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_unbuildSearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_unbuildService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mrp.client.mrp_unbuildFeignClient;

/**
 * 实体[拆解单] 服务对象接口实现
 */
@Slf4j
@Service
public class Mrp_unbuildServiceImpl implements IMrp_unbuildService {

    @Autowired
    mrp_unbuildFeignClient mrp_unbuildFeignClient;


    @Override
    public boolean update(Mrp_unbuild et) {
        Mrp_unbuild rt = mrp_unbuildFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mrp_unbuild> list){
        mrp_unbuildFeignClient.updateBatch(list) ;
    }

    @Override
    public Mrp_unbuild getDraft(Mrp_unbuild et) {
        et=mrp_unbuildFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mrp_unbuildFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mrp_unbuildFeignClient.removeBatch(idList);
    }

    @Override
    public Mrp_unbuild get(Integer id) {
		Mrp_unbuild et=mrp_unbuildFeignClient.get(id);
        if(et==null){
            et=new Mrp_unbuild();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Mrp_unbuild et) {
        Mrp_unbuild rt = mrp_unbuildFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mrp_unbuild> list){
        mrp_unbuildFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mrp_unbuild> searchDefault(Mrp_unbuildSearchContext context) {
        Page<Mrp_unbuild> mrp_unbuilds=mrp_unbuildFeignClient.searchDefault(context);
        return mrp_unbuilds;
    }


}


