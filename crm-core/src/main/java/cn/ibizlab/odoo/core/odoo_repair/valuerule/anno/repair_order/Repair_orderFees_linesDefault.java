package cn.ibizlab.odoo.core.odoo_repair.valuerule.anno.repair_order;

import cn.ibizlab.odoo.core.odoo_repair.valuerule.validator.repair_order.Repair_orderFees_linesDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Repair_order
 * 属性：Fees_lines
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Repair_orderFees_linesDefaultValidator.class})
public @interface Repair_orderFees_linesDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[1048576]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
