package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_bank;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_bankSearchContext;


/**
 * 实体[Res_partner_bank] 服务对象接口
 */
public interface IRes_partner_bankService{

    boolean update(Res_partner_bank et) ;
    void updateBatch(List<Res_partner_bank> list) ;
    Res_partner_bank getDraft(Res_partner_bank et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Res_partner_bank et) ;
    void createBatch(List<Res_partner_bank> list) ;
    Res_partner_bank get(Integer key) ;
    Page<Res_partner_bank> searchDefault(Res_partner_bankSearchContext context) ;

}



