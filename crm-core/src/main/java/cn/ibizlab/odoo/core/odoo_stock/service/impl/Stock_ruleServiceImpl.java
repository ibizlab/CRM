package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_rule;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_ruleSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_ruleService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_ruleFeignClient;

/**
 * 实体[库存规则] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_ruleServiceImpl implements IStock_ruleService {

    @Autowired
    stock_ruleFeignClient stock_ruleFeignClient;


    @Override
    public Stock_rule get(Integer id) {
		Stock_rule et=stock_ruleFeignClient.get(id);
        if(et==null){
            et=new Stock_rule();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=stock_ruleFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_ruleFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Stock_rule et) {
        Stock_rule rt = stock_ruleFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_rule> list){
        stock_ruleFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Stock_rule et) {
        Stock_rule rt = stock_ruleFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_rule> list){
        stock_ruleFeignClient.createBatch(list) ;
    }

    @Override
    public Stock_rule getDraft(Stock_rule et) {
        et=stock_ruleFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_rule> searchDefault(Stock_ruleSearchContext context) {
        Page<Stock_rule> stock_rules=stock_ruleFeignClient.searchDefault(context);
        return stock_rules;
    }


}


