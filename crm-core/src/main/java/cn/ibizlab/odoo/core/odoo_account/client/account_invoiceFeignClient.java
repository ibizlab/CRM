package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoiceSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_invoice] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-invoice", fallback = account_invoiceFallback.class)
public interface account_invoiceFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/account_invoices/{id}")
    Account_invoice update(@PathVariable("id") Integer id,@RequestBody Account_invoice account_invoice);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_invoices/batch")
    Boolean updateBatch(@RequestBody List<Account_invoice> account_invoices);



    @RequestMapping(method = RequestMethod.POST, value = "/account_invoices/searchdefault")
    Page<Account_invoice> searchDefault(@RequestBody Account_invoiceSearchContext context);




    @RequestMapping(method = RequestMethod.POST, value = "/account_invoices")
    Account_invoice create(@RequestBody Account_invoice account_invoice);

    @RequestMapping(method = RequestMethod.POST, value = "/account_invoices/batch")
    Boolean createBatch(@RequestBody List<Account_invoice> account_invoices);



    @RequestMapping(method = RequestMethod.DELETE, value = "/account_invoices/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_invoices/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/account_invoices/{id}")
    Account_invoice get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/account_invoices/select")
    Page<Account_invoice> select();


    @RequestMapping(method = RequestMethod.POST, value = "/account_invoices/checkkey")
    Boolean checkKey(@RequestBody Account_invoice account_invoice);


    @RequestMapping(method = RequestMethod.GET, value = "/account_invoices/getdraft")
    Account_invoice getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/account_invoices/save")
    Boolean save(@RequestBody Account_invoice account_invoice);

    @RequestMapping(method = RequestMethod.POST, value = "/account_invoices/save")
    Boolean saveBatch(@RequestBody List<Account_invoice> account_invoices);


}
