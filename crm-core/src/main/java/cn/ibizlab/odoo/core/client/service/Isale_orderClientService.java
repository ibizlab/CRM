package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Isale_order;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[sale_order] 服务对象接口
 */
public interface Isale_orderClientService{

    public Isale_order createModel() ;

    public void updateBatch(List<Isale_order> sale_orders);

    public void update(Isale_order sale_order);

    public Page<Isale_order> fetchDefault(SearchContext context);

    public void removeBatch(List<Isale_order> sale_orders);

    public void create(Isale_order sale_order);

    public void createBatch(List<Isale_order> sale_orders);

    public void get(Isale_order sale_order);

    public void remove(Isale_order sale_order);

    public Page<Isale_order> select(SearchContext context);

    public void save(Isale_order sale_order);

    public void checkKey(Isale_order sale_order);

    public void getDraft(Isale_order sale_order);

}
