package cn.ibizlab.odoo.core.odoo_gamification.valuerule.anno.gamification_badge_user_wizard;

import cn.ibizlab.odoo.core.odoo_gamification.valuerule.validator.gamification_badge_user_wizard.Gamification_badge_user_wizardBadge_id_textDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Gamification_badge_user_wizard
 * 属性：Badge_id_text
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Gamification_badge_user_wizardBadge_id_textDefaultValidator.class})
public @interface Gamification_badge_user_wizardBadge_id_textDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
