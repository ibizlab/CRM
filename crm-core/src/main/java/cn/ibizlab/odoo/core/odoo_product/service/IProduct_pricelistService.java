package cn.ibizlab.odoo.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_product.domain.Product_pricelist;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_pricelistSearchContext;


/**
 * 实体[Product_pricelist] 服务对象接口
 */
public interface IProduct_pricelistService{

    Product_pricelist get(Integer key) ;
    boolean save(Product_pricelist et) ;
    void saveBatch(List<Product_pricelist> list) ;
    Product_pricelist getDraft(Product_pricelist et) ;
    boolean update(Product_pricelist et) ;
    void updateBatch(List<Product_pricelist> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean checkKey(Product_pricelist et) ;
    boolean create(Product_pricelist et) ;
    void createBatch(List<Product_pricelist> list) ;
    Page<Product_pricelist> searchDefault(Product_pricelistSearchContext context) ;

}



