package cn.ibizlab.odoo.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_holidays_summary_dept;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_holidays_summary_deptSearchContext;


/**
 * 实体[Hr_holidays_summary_dept] 服务对象接口
 */
public interface IHr_holidays_summary_deptService{

    Hr_holidays_summary_dept getDraft(Hr_holidays_summary_dept et) ;
    boolean create(Hr_holidays_summary_dept et) ;
    void createBatch(List<Hr_holidays_summary_dept> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Hr_holidays_summary_dept et) ;
    void updateBatch(List<Hr_holidays_summary_dept> list) ;
    Hr_holidays_summary_dept get(Integer key) ;
    Page<Hr_holidays_summary_dept> searchDefault(Hr_holidays_summary_deptSearchContext context) ;

}



