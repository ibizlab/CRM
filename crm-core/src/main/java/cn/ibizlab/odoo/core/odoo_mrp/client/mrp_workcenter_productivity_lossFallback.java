package cn.ibizlab.odoo.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenter_productivity_lossSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mrp_workcenter_productivity_loss] 服务对象接口
 */
@Component
public class mrp_workcenter_productivity_lossFallback implements mrp_workcenter_productivity_lossFeignClient{

    public Mrp_workcenter_productivity_loss update(Integer id, Mrp_workcenter_productivity_loss mrp_workcenter_productivity_loss){
            return null;
     }
    public Boolean updateBatch(List<Mrp_workcenter_productivity_loss> mrp_workcenter_productivity_losses){
            return false;
     }


    public Mrp_workcenter_productivity_loss create(Mrp_workcenter_productivity_loss mrp_workcenter_productivity_loss){
            return null;
     }
    public Boolean createBatch(List<Mrp_workcenter_productivity_loss> mrp_workcenter_productivity_losses){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Page<Mrp_workcenter_productivity_loss> searchDefault(Mrp_workcenter_productivity_lossSearchContext context){
            return null;
     }




    public Mrp_workcenter_productivity_loss get(Integer id){
            return null;
     }


    public Page<Mrp_workcenter_productivity_loss> select(){
            return null;
     }

    public Mrp_workcenter_productivity_loss getDraft(){
            return null;
    }



}
