package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailingSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_mass_mailing] 服务对象接口
 */
@FeignClient(value = "odoo-mail", contextId = "mail-mass-mailing", fallback = mail_mass_mailingFallback.class)
public interface mail_mass_mailingFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailings")
    Mail_mass_mailing create(@RequestBody Mail_mass_mailing mail_mass_mailing);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailings/batch")
    Boolean createBatch(@RequestBody List<Mail_mass_mailing> mail_mass_mailings);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailings/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailings/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);




    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailings/searchdefault")
    Page<Mail_mass_mailing> searchDefault(@RequestBody Mail_mass_mailingSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailings/{id}")
    Mail_mass_mailing update(@PathVariable("id") Integer id,@RequestBody Mail_mass_mailing mail_mass_mailing);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailings/batch")
    Boolean updateBatch(@RequestBody List<Mail_mass_mailing> mail_mass_mailings);



    @RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailings/{id}")
    Mail_mass_mailing get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailings/select")
    Page<Mail_mass_mailing> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailings/getdraft")
    Mail_mass_mailing getDraft();


}
