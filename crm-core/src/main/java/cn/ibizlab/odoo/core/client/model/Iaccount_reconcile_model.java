package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [account_reconcile_model] 对象
 */
public interface Iaccount_reconcile_model {

    /**
     * 获取 [科目]
     */
    public void setAccount_id(Integer account_id);
    
    /**
     * 设置 [科目]
     */
    public Integer getAccount_id();

    /**
     * 获取 [科目]脏标记
     */
    public boolean getAccount_idDirtyFlag();
    /**
     * 获取 [科目]
     */
    public void setAccount_id_text(String account_id_text);
    
    /**
     * 设置 [科目]
     */
    public String getAccount_id_text();

    /**
     * 获取 [科目]脏标记
     */
    public boolean getAccount_id_textDirtyFlag();
    /**
     * 获取 [核销金额]
     */
    public void setAmount(Double amount);
    
    /**
     * 设置 [核销金额]
     */
    public Double getAmount();

    /**
     * 获取 [核销金额]脏标记
     */
    public boolean getAmountDirtyFlag();
    /**
     * 获取 [金额类型]
     */
    public void setAmount_type(String amount_type);
    
    /**
     * 设置 [金额类型]
     */
    public String getAmount_type();

    /**
     * 获取 [金额类型]脏标记
     */
    public boolean getAmount_typeDirtyFlag();
    /**
     * 获取 [分析账户]
     */
    public void setAnalytic_account_id(Integer analytic_account_id);
    
    /**
     * 设置 [分析账户]
     */
    public Integer getAnalytic_account_id();

    /**
     * 获取 [分析账户]脏标记
     */
    public boolean getAnalytic_account_idDirtyFlag();
    /**
     * 获取 [分析账户]
     */
    public void setAnalytic_account_id_text(String analytic_account_id_text);
    
    /**
     * 设置 [分析账户]
     */
    public String getAnalytic_account_id_text();

    /**
     * 获取 [分析账户]脏标记
     */
    public boolean getAnalytic_account_id_textDirtyFlag();
    /**
     * 获取 [分析标签]
     */
    public void setAnalytic_tag_ids(String analytic_tag_ids);
    
    /**
     * 设置 [分析标签]
     */
    public String getAnalytic_tag_ids();

    /**
     * 获取 [分析标签]脏标记
     */
    public boolean getAnalytic_tag_idsDirtyFlag();
    /**
     * 获取 [自动验证]
     */
    public void setAuto_reconcile(String auto_reconcile);
    
    /**
     * 设置 [自动验证]
     */
    public String getAuto_reconcile();

    /**
     * 获取 [自动验证]脏标记
     */
    public boolean getAuto_reconcileDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [第二含税价‎]
     */
    public void setForce_second_tax_included(String force_second_tax_included);
    
    /**
     * 设置 [第二含税价‎]
     */
    public String getForce_second_tax_included();

    /**
     * 获取 [第二含税价‎]脏标记
     */
    public boolean getForce_second_tax_includedDirtyFlag();
    /**
     * 获取 [含税价]
     */
    public void setForce_tax_included(String force_tax_included);
    
    /**
     * 设置 [含税价]
     */
    public String getForce_tax_included();

    /**
     * 获取 [含税价]脏标记
     */
    public boolean getForce_tax_includedDirtyFlag();
    /**
     * 获取 [添加第二行]
     */
    public void setHas_second_line(String has_second_line);
    
    /**
     * 设置 [添加第二行]
     */
    public String getHas_second_line();

    /**
     * 获取 [添加第二行]脏标记
     */
    public boolean getHas_second_lineDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [第二含税价‎]
     */
    public void setIs_second_tax_price_included(String is_second_tax_price_included);
    
    /**
     * 设置 [第二含税价‎]
     */
    public String getIs_second_tax_price_included();

    /**
     * 获取 [第二含税价‎]脏标记
     */
    public boolean getIs_second_tax_price_includedDirtyFlag();
    /**
     * 获取 [含税价]
     */
    public void setIs_tax_price_included(String is_tax_price_included);
    
    /**
     * 设置 [含税价]
     */
    public String getIs_tax_price_included();

    /**
     * 获取 [含税价]脏标记
     */
    public boolean getIs_tax_price_includedDirtyFlag();
    /**
     * 获取 [日记账]
     */
    public void setJournal_id(Integer journal_id);
    
    /**
     * 设置 [日记账]
     */
    public Integer getJournal_id();

    /**
     * 获取 [日记账]脏标记
     */
    public boolean getJournal_idDirtyFlag();
    /**
     * 获取 [日记账]
     */
    public void setJournal_id_text(String journal_id_text);
    
    /**
     * 设置 [日记账]
     */
    public String getJournal_id_text();

    /**
     * 获取 [日记账]脏标记
     */
    public boolean getJournal_id_textDirtyFlag();
    /**
     * 获取 [日记账项目标签]
     */
    public void setLabel(String label);
    
    /**
     * 设置 [日记账项目标签]
     */
    public String getLabel();

    /**
     * 获取 [日记账项目标签]脏标记
     */
    public boolean getLabelDirtyFlag();
    /**
     * 获取 [金额]
     */
    public void setMatch_amount(String match_amount);
    
    /**
     * 设置 [金额]
     */
    public String getMatch_amount();

    /**
     * 获取 [金额]脏标记
     */
    public boolean getMatch_amountDirtyFlag();
    /**
     * 获取 [参数最大金额]
     */
    public void setMatch_amount_max(Double match_amount_max);
    
    /**
     * 设置 [参数最大金额]
     */
    public Double getMatch_amount_max();

    /**
     * 获取 [参数最大金额]脏标记
     */
    public boolean getMatch_amount_maxDirtyFlag();
    /**
     * 获取 [参数最小金额]
     */
    public void setMatch_amount_min(Double match_amount_min);
    
    /**
     * 设置 [参数最小金额]
     */
    public Double getMatch_amount_min();

    /**
     * 获取 [参数最小金额]脏标记
     */
    public boolean getMatch_amount_minDirtyFlag();
    /**
     * 获取 [凭证类型]
     */
    public void setMatch_journal_ids(String match_journal_ids);
    
    /**
     * 设置 [凭证类型]
     */
    public String getMatch_journal_ids();

    /**
     * 获取 [凭证类型]脏标记
     */
    public boolean getMatch_journal_idsDirtyFlag();
    /**
     * 获取 [标签]
     */
    public void setMatch_label(String match_label);
    
    /**
     * 设置 [标签]
     */
    public String getMatch_label();

    /**
     * 获取 [标签]脏标记
     */
    public boolean getMatch_labelDirtyFlag();
    /**
     * 获取 [标签参数]
     */
    public void setMatch_label_param(String match_label_param);
    
    /**
     * 设置 [标签参数]
     */
    public String getMatch_label_param();

    /**
     * 获取 [标签参数]脏标记
     */
    public boolean getMatch_label_paramDirtyFlag();
    /**
     * 获取 [数量性质]
     */
    public void setMatch_nature(String match_nature);
    
    /**
     * 设置 [数量性质]
     */
    public String getMatch_nature();

    /**
     * 获取 [数量性质]脏标记
     */
    public boolean getMatch_natureDirtyFlag();
    /**
     * 获取 [已经匹配合作伙伴]
     */
    public void setMatch_partner(String match_partner);
    
    /**
     * 设置 [已经匹配合作伙伴]
     */
    public String getMatch_partner();

    /**
     * 获取 [已经匹配合作伙伴]脏标记
     */
    public boolean getMatch_partnerDirtyFlag();
    /**
     * 获取 [限制合作伙伴类别为]
     */
    public void setMatch_partner_category_ids(String match_partner_category_ids);
    
    /**
     * 设置 [限制合作伙伴类别为]
     */
    public String getMatch_partner_category_ids();

    /**
     * 获取 [限制合作伙伴类别为]脏标记
     */
    public boolean getMatch_partner_category_idsDirtyFlag();
    /**
     * 获取 [限制合作伙伴为]
     */
    public void setMatch_partner_ids(String match_partner_ids);
    
    /**
     * 设置 [限制合作伙伴为]
     */
    public String getMatch_partner_ids();

    /**
     * 获取 [限制合作伙伴为]脏标记
     */
    public boolean getMatch_partner_idsDirtyFlag();
    /**
     * 获取 [同币种匹配]
     */
    public void setMatch_same_currency(String match_same_currency);
    
    /**
     * 设置 [同币种匹配]
     */
    public String getMatch_same_currency();

    /**
     * 获取 [同币种匹配]脏标记
     */
    public boolean getMatch_same_currencyDirtyFlag();
    /**
     * 获取 [会计匹配]
     */
    public void setMatch_total_amount(String match_total_amount);
    
    /**
     * 设置 [会计匹配]
     */
    public String getMatch_total_amount();

    /**
     * 获取 [会计匹配]脏标记
     */
    public boolean getMatch_total_amountDirtyFlag();
    /**
     * 获取 [会计匹配%]
     */
    public void setMatch_total_amount_param(Double match_total_amount_param);
    
    /**
     * 设置 [会计匹配%]
     */
    public Double getMatch_total_amount_param();

    /**
     * 获取 [会计匹配%]脏标记
     */
    public boolean getMatch_total_amount_paramDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [类型]
     */
    public void setRule_type(String rule_type);
    
    /**
     * 设置 [类型]
     */
    public String getRule_type();

    /**
     * 获取 [类型]脏标记
     */
    public boolean getRule_typeDirtyFlag();
    /**
     * 获取 [第二科目]
     */
    public void setSecond_account_id(Integer second_account_id);
    
    /**
     * 设置 [第二科目]
     */
    public Integer getSecond_account_id();

    /**
     * 获取 [第二科目]脏标记
     */
    public boolean getSecond_account_idDirtyFlag();
    /**
     * 获取 [第二科目]
     */
    public void setSecond_account_id_text(String second_account_id_text);
    
    /**
     * 设置 [第二科目]
     */
    public String getSecond_account_id_text();

    /**
     * 获取 [第二科目]脏标记
     */
    public boolean getSecond_account_id_textDirtyFlag();
    /**
     * 获取 [第二核销金额]
     */
    public void setSecond_amount(Double second_amount);
    
    /**
     * 设置 [第二核销金额]
     */
    public Double getSecond_amount();

    /**
     * 获取 [第二核销金额]脏标记
     */
    public boolean getSecond_amountDirtyFlag();
    /**
     * 获取 [第二金额类型]
     */
    public void setSecond_amount_type(String second_amount_type);
    
    /**
     * 设置 [第二金额类型]
     */
    public String getSecond_amount_type();

    /**
     * 获取 [第二金额类型]脏标记
     */
    public boolean getSecond_amount_typeDirtyFlag();
    /**
     * 获取 [第二分析帐户]
     */
    public void setSecond_analytic_account_id(Integer second_analytic_account_id);
    
    /**
     * 设置 [第二分析帐户]
     */
    public Integer getSecond_analytic_account_id();

    /**
     * 获取 [第二分析帐户]脏标记
     */
    public boolean getSecond_analytic_account_idDirtyFlag();
    /**
     * 获取 [第二分析帐户]
     */
    public void setSecond_analytic_account_id_text(String second_analytic_account_id_text);
    
    /**
     * 设置 [第二分析帐户]
     */
    public String getSecond_analytic_account_id_text();

    /**
     * 获取 [第二分析帐户]脏标记
     */
    public boolean getSecond_analytic_account_id_textDirtyFlag();
    /**
     * 获取 [第二分析标签]
     */
    public void setSecond_analytic_tag_ids(String second_analytic_tag_ids);
    
    /**
     * 设置 [第二分析标签]
     */
    public String getSecond_analytic_tag_ids();

    /**
     * 获取 [第二分析标签]脏标记
     */
    public boolean getSecond_analytic_tag_idsDirtyFlag();
    /**
     * 获取 [第二个分录]
     */
    public void setSecond_journal_id(Integer second_journal_id);
    
    /**
     * 设置 [第二个分录]
     */
    public Integer getSecond_journal_id();

    /**
     * 获取 [第二个分录]脏标记
     */
    public boolean getSecond_journal_idDirtyFlag();
    /**
     * 获取 [第二个分录]
     */
    public void setSecond_journal_id_text(String second_journal_id_text);
    
    /**
     * 设置 [第二个分录]
     */
    public String getSecond_journal_id_text();

    /**
     * 获取 [第二个分录]脏标记
     */
    public boolean getSecond_journal_id_textDirtyFlag();
    /**
     * 获取 [第二个分录项目标签]
     */
    public void setSecond_label(String second_label);
    
    /**
     * 设置 [第二个分录项目标签]
     */
    public String getSecond_label();

    /**
     * 获取 [第二个分录项目标签]脏标记
     */
    public boolean getSecond_labelDirtyFlag();
    /**
     * 获取 [第二税率类别]
     */
    public void setSecond_tax_amount_type(String second_tax_amount_type);
    
    /**
     * 设置 [第二税率类别]
     */
    public String getSecond_tax_amount_type();

    /**
     * 获取 [第二税率类别]脏标记
     */
    public boolean getSecond_tax_amount_typeDirtyFlag();
    /**
     * 获取 [第二个税]
     */
    public void setSecond_tax_id(Integer second_tax_id);
    
    /**
     * 设置 [第二个税]
     */
    public Integer getSecond_tax_id();

    /**
     * 获取 [第二个税]脏标记
     */
    public boolean getSecond_tax_idDirtyFlag();
    /**
     * 获取 [第二个税]
     */
    public void setSecond_tax_id_text(String second_tax_id_text);
    
    /**
     * 设置 [第二个税]
     */
    public String getSecond_tax_id_text();

    /**
     * 获取 [第二个税]脏标记
     */
    public boolean getSecond_tax_id_textDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [税率类别]
     */
    public void setTax_amount_type(String tax_amount_type);
    
    /**
     * 设置 [税率类别]
     */
    public String getTax_amount_type();

    /**
     * 获取 [税率类别]脏标记
     */
    public boolean getTax_amount_typeDirtyFlag();
    /**
     * 获取 [税率]
     */
    public void setTax_id(Integer tax_id);
    
    /**
     * 设置 [税率]
     */
    public Integer getTax_id();

    /**
     * 获取 [税率]脏标记
     */
    public boolean getTax_idDirtyFlag();
    /**
     * 获取 [税率]
     */
    public void setTax_id_text(String tax_id_text);
    
    /**
     * 设置 [税率]
     */
    public String getTax_id_text();

    /**
     * 获取 [税率]脏标记
     */
    public boolean getTax_id_textDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
