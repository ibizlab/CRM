package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_wizard_invite;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_wizard_inviteSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_wizard_inviteService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_wizard_inviteFeignClient;

/**
 * 实体[邀请向导] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_wizard_inviteServiceImpl implements IMail_wizard_inviteService {

    @Autowired
    mail_wizard_inviteFeignClient mail_wizard_inviteFeignClient;


    @Override
    public Mail_wizard_invite getDraft(Mail_wizard_invite et) {
        et=mail_wizard_inviteFeignClient.getDraft();
        return et;
    }

    @Override
    public Mail_wizard_invite get(Integer id) {
		Mail_wizard_invite et=mail_wizard_inviteFeignClient.get(id);
        if(et==null){
            et=new Mail_wizard_invite();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Mail_wizard_invite et) {
        Mail_wizard_invite rt = mail_wizard_inviteFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_wizard_invite> list){
        mail_wizard_inviteFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Mail_wizard_invite et) {
        Mail_wizard_invite rt = mail_wizard_inviteFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_wizard_invite> list){
        mail_wizard_inviteFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mail_wizard_inviteFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_wizard_inviteFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_wizard_invite> searchDefault(Mail_wizard_inviteSearchContext context) {
        Page<Mail_wizard_invite> mail_wizard_invites=mail_wizard_inviteFeignClient.searchDefault(context);
        return mail_wizard_invites;
    }


}


