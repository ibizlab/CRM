package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_tax_group;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_tax_groupSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_tax_groupService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_tax_groupFeignClient;

/**
 * 实体[税组] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_tax_groupServiceImpl implements IAccount_tax_groupService {

    @Autowired
    account_tax_groupFeignClient account_tax_groupFeignClient;


    @Override
    public boolean update(Account_tax_group et) {
        Account_tax_group rt = account_tax_groupFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_tax_group> list){
        account_tax_groupFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_tax_groupFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_tax_groupFeignClient.removeBatch(idList);
    }

    @Override
    public Account_tax_group get(Integer id) {
		Account_tax_group et=account_tax_groupFeignClient.get(id);
        if(et==null){
            et=new Account_tax_group();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Account_tax_group getDraft(Account_tax_group et) {
        et=account_tax_groupFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Account_tax_group et) {
        Account_tax_group rt = account_tax_groupFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_tax_group> list){
        account_tax_groupFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_tax_group> searchDefault(Account_tax_groupSearchContext context) {
        Page<Account_tax_group> account_tax_groups=account_tax_groupFeignClient.searchDefault(context);
        return account_tax_groups;
    }


}


