package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statementSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_bank_statement] 服务对象接口
 */
@Component
public class account_bank_statementFallback implements account_bank_statementFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Account_bank_statement> searchDefault(Account_bank_statementSearchContext context){
            return null;
     }



    public Account_bank_statement get(Integer id){
            return null;
     }



    public Account_bank_statement create(Account_bank_statement account_bank_statement){
            return null;
     }
    public Boolean createBatch(List<Account_bank_statement> account_bank_statements){
            return false;
     }

    public Account_bank_statement update(Integer id, Account_bank_statement account_bank_statement){
            return null;
     }
    public Boolean updateBatch(List<Account_bank_statement> account_bank_statements){
            return false;
     }



    public Page<Account_bank_statement> select(){
            return null;
     }

    public Account_bank_statement getDraft(){
            return null;
    }



}
