package cn.ibizlab.odoo.core.odoo_lunch.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_product;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_productSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[lunch_product] 服务对象接口
 */
@FeignClient(value = "odoo-lunch", contextId = "lunch-product", fallback = lunch_productFallback.class)
public interface lunch_productFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/lunch_products")
    Lunch_product create(@RequestBody Lunch_product lunch_product);

    @RequestMapping(method = RequestMethod.POST, value = "/lunch_products/batch")
    Boolean createBatch(@RequestBody List<Lunch_product> lunch_products);


    @RequestMapping(method = RequestMethod.GET, value = "/lunch_products/{id}")
    Lunch_product get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/lunch_products/searchdefault")
    Page<Lunch_product> searchDefault(@RequestBody Lunch_productSearchContext context);





    @RequestMapping(method = RequestMethod.PUT, value = "/lunch_products/{id}")
    Lunch_product update(@PathVariable("id") Integer id,@RequestBody Lunch_product lunch_product);

    @RequestMapping(method = RequestMethod.PUT, value = "/lunch_products/batch")
    Boolean updateBatch(@RequestBody List<Lunch_product> lunch_products);


    @RequestMapping(method = RequestMethod.DELETE, value = "/lunch_products/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/lunch_products/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/lunch_products/select")
    Page<Lunch_product> select();


    @RequestMapping(method = RequestMethod.GET, value = "/lunch_products/getdraft")
    Lunch_product getDraft();


}
