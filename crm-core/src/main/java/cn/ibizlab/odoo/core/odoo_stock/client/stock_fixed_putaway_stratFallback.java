package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_fixed_putaway_strat;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_fixed_putaway_stratSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_fixed_putaway_strat] 服务对象接口
 */
@Component
public class stock_fixed_putaway_stratFallback implements stock_fixed_putaway_stratFeignClient{



    public Stock_fixed_putaway_strat update(Integer id, Stock_fixed_putaway_strat stock_fixed_putaway_strat){
            return null;
     }
    public Boolean updateBatch(List<Stock_fixed_putaway_strat> stock_fixed_putaway_strats){
            return false;
     }


    public Stock_fixed_putaway_strat create(Stock_fixed_putaway_strat stock_fixed_putaway_strat){
            return null;
     }
    public Boolean createBatch(List<Stock_fixed_putaway_strat> stock_fixed_putaway_strats){
            return false;
     }

    public Stock_fixed_putaway_strat get(Integer id){
            return null;
     }



    public Page<Stock_fixed_putaway_strat> searchDefault(Stock_fixed_putaway_stratSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Stock_fixed_putaway_strat> select(){
            return null;
     }

    public Stock_fixed_putaway_strat getDraft(){
            return null;
    }



}
