package cn.ibizlab.odoo.core.odoo_website.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_sale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_sale_payment_acquirer_onboarding_wizardSearchContext;
import cn.ibizlab.odoo.core.odoo_website.service.IWebsite_sale_payment_acquirer_onboarding_wizardService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_website.client.website_sale_payment_acquirer_onboarding_wizardFeignClient;

/**
 * 实体[website.sale.payment.acquirer.onboarding.wizard] 服务对象接口实现
 */
@Slf4j
@Service
public class Website_sale_payment_acquirer_onboarding_wizardServiceImpl implements IWebsite_sale_payment_acquirer_onboarding_wizardService {

    @Autowired
    website_sale_payment_acquirer_onboarding_wizardFeignClient website_sale_payment_acquirer_onboarding_wizardFeignClient;


    @Override
    public Website_sale_payment_acquirer_onboarding_wizard get(Integer id) {
		Website_sale_payment_acquirer_onboarding_wizard et=website_sale_payment_acquirer_onboarding_wizardFeignClient.get(id);
        if(et==null){
            et=new Website_sale_payment_acquirer_onboarding_wizard();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Website_sale_payment_acquirer_onboarding_wizard getDraft(Website_sale_payment_acquirer_onboarding_wizard et) {
        et=website_sale_payment_acquirer_onboarding_wizardFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Website_sale_payment_acquirer_onboarding_wizard et) {
        Website_sale_payment_acquirer_onboarding_wizard rt = website_sale_payment_acquirer_onboarding_wizardFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Website_sale_payment_acquirer_onboarding_wizard> list){
        website_sale_payment_acquirer_onboarding_wizardFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=website_sale_payment_acquirer_onboarding_wizardFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        website_sale_payment_acquirer_onboarding_wizardFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Website_sale_payment_acquirer_onboarding_wizard et) {
        Website_sale_payment_acquirer_onboarding_wizard rt = website_sale_payment_acquirer_onboarding_wizardFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Website_sale_payment_acquirer_onboarding_wizard> list){
        website_sale_payment_acquirer_onboarding_wizardFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Website_sale_payment_acquirer_onboarding_wizard> searchDefault(Website_sale_payment_acquirer_onboarding_wizardSearchContext context) {
        Page<Website_sale_payment_acquirer_onboarding_wizard> website_sale_payment_acquirer_onboarding_wizards=website_sale_payment_acquirer_onboarding_wizardFeignClient.searchDefault(context);
        return website_sale_payment_acquirer_onboarding_wizards;
    }


}


