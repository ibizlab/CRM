package cn.ibizlab.odoo.core.odoo_gamification.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_challenge_line;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_challenge_lineSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[gamification_challenge_line] 服务对象接口
 */
@FeignClient(value = "odoo-gamification", contextId = "gamification-challenge-line", fallback = gamification_challenge_lineFallback.class)
public interface gamification_challenge_lineFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/gamification_challenge_lines")
    Gamification_challenge_line create(@RequestBody Gamification_challenge_line gamification_challenge_line);

    @RequestMapping(method = RequestMethod.POST, value = "/gamification_challenge_lines/batch")
    Boolean createBatch(@RequestBody List<Gamification_challenge_line> gamification_challenge_lines);


    @RequestMapping(method = RequestMethod.PUT, value = "/gamification_challenge_lines/{id}")
    Gamification_challenge_line update(@PathVariable("id") Integer id,@RequestBody Gamification_challenge_line gamification_challenge_line);

    @RequestMapping(method = RequestMethod.PUT, value = "/gamification_challenge_lines/batch")
    Boolean updateBatch(@RequestBody List<Gamification_challenge_line> gamification_challenge_lines);





    @RequestMapping(method = RequestMethod.POST, value = "/gamification_challenge_lines/searchdefault")
    Page<Gamification_challenge_line> searchDefault(@RequestBody Gamification_challenge_lineSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/gamification_challenge_lines/{id}")
    Gamification_challenge_line get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.DELETE, value = "/gamification_challenge_lines/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/gamification_challenge_lines/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/gamification_challenge_lines/select")
    Page<Gamification_challenge_line> select();


    @RequestMapping(method = RequestMethod.GET, value = "/gamification_challenge_lines/getdraft")
    Gamification_challenge_line getDraft();


}
