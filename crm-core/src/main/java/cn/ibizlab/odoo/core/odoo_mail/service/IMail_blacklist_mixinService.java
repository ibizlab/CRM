package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_blacklist_mixin;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_blacklist_mixinSearchContext;


/**
 * 实体[Mail_blacklist_mixin] 服务对象接口
 */
public interface IMail_blacklist_mixinService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Mail_blacklist_mixin et) ;
    void updateBatch(List<Mail_blacklist_mixin> list) ;
    Mail_blacklist_mixin get(Integer key) ;
    boolean create(Mail_blacklist_mixin et) ;
    void createBatch(List<Mail_blacklist_mixin> list) ;
    Mail_blacklist_mixin getDraft(Mail_blacklist_mixin et) ;
    Page<Mail_blacklist_mixin> searchDefault(Mail_blacklist_mixinSearchContext context) ;

}



