package cn.ibizlab.odoo.core.odoo_base_import.valuerule.anno.base_import_tests_models_char;

import cn.ibizlab.odoo.core.odoo_base_import.valuerule.validator.base_import_tests_models_char.Base_import_tests_models_charValueDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Base_import_tests_models_char
 * 属性：Value
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Base_import_tests_models_charValueDefaultValidator.class})
public @interface Base_import_tests_models_charValueDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[100]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
