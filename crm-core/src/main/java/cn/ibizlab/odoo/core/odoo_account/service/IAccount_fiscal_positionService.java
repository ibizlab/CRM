package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_positionSearchContext;


/**
 * 实体[Account_fiscal_position] 服务对象接口
 */
public interface IAccount_fiscal_positionService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Account_fiscal_position et) ;
    void createBatch(List<Account_fiscal_position> list) ;
    boolean update(Account_fiscal_position et) ;
    void updateBatch(List<Account_fiscal_position> list) ;
    Account_fiscal_position get(Integer key) ;
    Account_fiscal_position getDraft(Account_fiscal_position et) ;
    Page<Account_fiscal_position> searchDefault(Account_fiscal_positionSearchContext context) ;

}



