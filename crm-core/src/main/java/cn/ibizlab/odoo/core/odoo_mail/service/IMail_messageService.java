package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_message;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_messageSearchContext;


/**
 * 实体[Mail_message] 服务对象接口
 */
public interface IMail_messageService{

    boolean create(Mail_message et) ;
    void createBatch(List<Mail_message> list) ;
    Mail_message get(Integer key) ;
    boolean checkKey(Mail_message et) ;
    Mail_message getDraft(Mail_message et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Mail_message et) ;
    void updateBatch(List<Mail_message> list) ;
    boolean save(Mail_message et) ;
    void saveBatch(List<Mail_message> list) ;
    Page<Mail_message> searchDefault(Mail_messageSearchContext context) ;

}



