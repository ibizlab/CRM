package cn.ibizlab.odoo.core.odoo_snailmail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_snailmail.domain.Snailmail_letter;
import cn.ibizlab.odoo.core.odoo_snailmail.filter.Snailmail_letterSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[snailmail_letter] 服务对象接口
 */
@FeignClient(value = "odoo-snailmail", contextId = "snailmail-letter", fallback = snailmail_letterFallback.class)
public interface snailmail_letterFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/snailmail_letters")
    Snailmail_letter create(@RequestBody Snailmail_letter snailmail_letter);

    @RequestMapping(method = RequestMethod.POST, value = "/snailmail_letters/batch")
    Boolean createBatch(@RequestBody List<Snailmail_letter> snailmail_letters);


    @RequestMapping(method = RequestMethod.GET, value = "/snailmail_letters/{id}")
    Snailmail_letter get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/snailmail_letters/{id}")
    Snailmail_letter update(@PathVariable("id") Integer id,@RequestBody Snailmail_letter snailmail_letter);

    @RequestMapping(method = RequestMethod.PUT, value = "/snailmail_letters/batch")
    Boolean updateBatch(@RequestBody List<Snailmail_letter> snailmail_letters);


    @RequestMapping(method = RequestMethod.DELETE, value = "/snailmail_letters/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/snailmail_letters/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/snailmail_letters/searchdefault")
    Page<Snailmail_letter> searchDefault(@RequestBody Snailmail_letterSearchContext context);





    @RequestMapping(method = RequestMethod.GET, value = "/snailmail_letters/select")
    Page<Snailmail_letter> select();


    @RequestMapping(method = RequestMethod.GET, value = "/snailmail_letters/getdraft")
    Snailmail_letter getDraft();


}
