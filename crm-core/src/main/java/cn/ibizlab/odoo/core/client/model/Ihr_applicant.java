package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [hr_applicant] 对象
 */
public interface Ihr_applicant {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [下一活动截止日期]
     */
    public void setActivity_date_deadline(Timestamp activity_date_deadline);
    
    /**
     * 设置 [下一活动截止日期]
     */
    public Timestamp getActivity_date_deadline();

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    public boolean getActivity_date_deadlineDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setActivity_ids(String activity_ids);
    
    /**
     * 设置 [活动]
     */
    public String getActivity_ids();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getActivity_idsDirtyFlag();
    /**
     * 获取 [活动状态]
     */
    public void setActivity_state(String activity_state);
    
    /**
     * 设置 [活动状态]
     */
    public String getActivity_state();

    /**
     * 获取 [活动状态]脏标记
     */
    public boolean getActivity_stateDirtyFlag();
    /**
     * 获取 [下一活动摘要]
     */
    public void setActivity_summary(String activity_summary);
    
    /**
     * 设置 [下一活动摘要]
     */
    public String getActivity_summary();

    /**
     * 获取 [下一活动摘要]脏标记
     */
    public boolean getActivity_summaryDirtyFlag();
    /**
     * 获取 [下一活动类型]
     */
    public void setActivity_type_id(Integer activity_type_id);
    
    /**
     * 设置 [下一活动类型]
     */
    public Integer getActivity_type_id();

    /**
     * 获取 [下一活动类型]脏标记
     */
    public boolean getActivity_type_idDirtyFlag();
    /**
     * 获取 [责任用户]
     */
    public void setActivity_user_id(Integer activity_user_id);
    
    /**
     * 设置 [责任用户]
     */
    public Integer getActivity_user_id();

    /**
     * 获取 [责任用户]脏标记
     */
    public boolean getActivity_user_idDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setAttachment_ids(String attachment_ids);
    
    /**
     * 设置 [附件]
     */
    public String getAttachment_ids();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getAttachment_idsDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setAttachment_number(Integer attachment_number);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getAttachment_number();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getAttachment_numberDirtyFlag();
    /**
     * 获取 [可用量]
     */
    public void setAvailability(Timestamp availability);
    
    /**
     * 设置 [可用量]
     */
    public Timestamp getAvailability();

    /**
     * 获取 [可用量]脏标记
     */
    public boolean getAvailabilityDirtyFlag();
    /**
     * 获取 [营销]
     */
    public void setCampaign_id(Integer campaign_id);
    
    /**
     * 设置 [营销]
     */
    public Integer getCampaign_id();

    /**
     * 获取 [营销]脏标记
     */
    public boolean getCampaign_idDirtyFlag();
    /**
     * 获取 [营销]
     */
    public void setCampaign_id_text(String campaign_id_text);
    
    /**
     * 设置 [营销]
     */
    public String getCampaign_id_text();

    /**
     * 获取 [营销]脏标记
     */
    public boolean getCampaign_id_textDirtyFlag();
    /**
     * 获取 [标签]
     */
    public void setCateg_ids(String categ_ids);
    
    /**
     * 设置 [标签]
     */
    public String getCateg_ids();

    /**
     * 获取 [标签]脏标记
     */
    public boolean getCateg_idsDirtyFlag();
    /**
     * 获取 [颜色索引]
     */
    public void setColor(Integer color);
    
    /**
     * 设置 [颜色索引]
     */
    public Integer getColor();

    /**
     * 获取 [颜色索引]脏标记
     */
    public boolean getColorDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建日期]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建日期]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建日期]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [已关闭]
     */
    public void setDate_closed(Timestamp date_closed);
    
    /**
     * 设置 [已关闭]
     */
    public Timestamp getDate_closed();

    /**
     * 获取 [已关闭]脏标记
     */
    public boolean getDate_closedDirtyFlag();
    /**
     * 获取 [最后阶段更新]
     */
    public void setDate_last_stage_update(Timestamp date_last_stage_update);
    
    /**
     * 设置 [最后阶段更新]
     */
    public Timestamp getDate_last_stage_update();

    /**
     * 获取 [最后阶段更新]脏标记
     */
    public boolean getDate_last_stage_updateDirtyFlag();
    /**
     * 获取 [已指派]
     */
    public void setDate_open(Timestamp date_open);
    
    /**
     * 设置 [已指派]
     */
    public Timestamp getDate_open();

    /**
     * 获取 [已指派]脏标记
     */
    public boolean getDate_openDirtyFlag();
    /**
     * 获取 [关闭日期]
     */
    public void setDay_close(Double day_close);
    
    /**
     * 设置 [关闭日期]
     */
    public Double getDay_close();

    /**
     * 获取 [关闭日期]脏标记
     */
    public boolean getDay_closeDirtyFlag();
    /**
     * 获取 [开启天数]
     */
    public void setDay_open(Double day_open);
    
    /**
     * 设置 [开启天数]
     */
    public Double getDay_open();

    /**
     * 获取 [开启天数]脏标记
     */
    public boolean getDay_openDirtyFlag();
    /**
     * 获取 [延迟关闭]
     */
    public void setDelay_close(Double delay_close);
    
    /**
     * 设置 [延迟关闭]
     */
    public Double getDelay_close();

    /**
     * 获取 [延迟关闭]脏标记
     */
    public boolean getDelay_closeDirtyFlag();
    /**
     * 获取 [部门]
     */
    public void setDepartment_id(Integer department_id);
    
    /**
     * 设置 [部门]
     */
    public Integer getDepartment_id();

    /**
     * 获取 [部门]脏标记
     */
    public boolean getDepartment_idDirtyFlag();
    /**
     * 获取 [部门]
     */
    public void setDepartment_id_text(String department_id_text);
    
    /**
     * 设置 [部门]
     */
    public String getDepartment_id_text();

    /**
     * 获取 [部门]脏标记
     */
    public boolean getDepartment_id_textDirtyFlag();
    /**
     * 获取 [说明]
     */
    public void setDescription(String description);
    
    /**
     * 设置 [说明]
     */
    public String getDescription();

    /**
     * 获取 [说明]脏标记
     */
    public boolean getDescriptionDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [关注者的EMail]
     */
    public void setEmail_cc(String email_cc);
    
    /**
     * 设置 [关注者的EMail]
     */
    public String getEmail_cc();

    /**
     * 获取 [关注者的EMail]脏标记
     */
    public boolean getEmail_ccDirtyFlag();
    /**
     * 获取 [EMail]
     */
    public void setEmail_from(String email_from);
    
    /**
     * 设置 [EMail]
     */
    public String getEmail_from();

    /**
     * 获取 [EMail]脏标记
     */
    public boolean getEmail_fromDirtyFlag();
    /**
     * 获取 [员工姓名]
     */
    public void setEmployee_name(String employee_name);
    
    /**
     * 设置 [员工姓名]
     */
    public String getEmployee_name();

    /**
     * 获取 [员工姓名]脏标记
     */
    public boolean getEmployee_nameDirtyFlag();
    /**
     * 获取 [员工]
     */
    public void setEmp_id(Integer emp_id);
    
    /**
     * 设置 [员工]
     */
    public Integer getEmp_id();

    /**
     * 获取 [员工]脏标记
     */
    public boolean getEmp_idDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [申请的职位]
     */
    public void setJob_id(Integer job_id);
    
    /**
     * 设置 [申请的职位]
     */
    public Integer getJob_id();

    /**
     * 获取 [申请的职位]脏标记
     */
    public boolean getJob_idDirtyFlag();
    /**
     * 获取 [申请的职位]
     */
    public void setJob_id_text(String job_id_text);
    
    /**
     * 设置 [申请的职位]
     */
    public String getJob_id_text();

    /**
     * 获取 [申请的职位]脏标记
     */
    public boolean getJob_id_textDirtyFlag();
    /**
     * 获取 [看板状态]
     */
    public void setKanban_state(String kanban_state);
    
    /**
     * 设置 [看板状态]
     */
    public String getKanban_state();

    /**
     * 获取 [看板状态]脏标记
     */
    public boolean getKanban_stateDirtyFlag();
    /**
     * 获取 [最终阶段]
     */
    public void setLast_stage_id(Integer last_stage_id);
    
    /**
     * 设置 [最终阶段]
     */
    public Integer getLast_stage_id();

    /**
     * 获取 [最终阶段]脏标记
     */
    public boolean getLast_stage_idDirtyFlag();
    /**
     * 获取 [最终阶段]
     */
    public void setLast_stage_id_text(String last_stage_id_text);
    
    /**
     * 设置 [最终阶段]
     */
    public String getLast_stage_id_text();

    /**
     * 获取 [最终阶段]脏标记
     */
    public boolean getLast_stage_id_textDirtyFlag();
    /**
     * 获取 [看板阻塞]
     */
    public void setLegend_blocked(String legend_blocked);
    
    /**
     * 设置 [看板阻塞]
     */
    public String getLegend_blocked();

    /**
     * 获取 [看板阻塞]脏标记
     */
    public boolean getLegend_blockedDirtyFlag();
    /**
     * 获取 [看板有效]
     */
    public void setLegend_done(String legend_done);
    
    /**
     * 设置 [看板有效]
     */
    public String getLegend_done();

    /**
     * 获取 [看板有效]脏标记
     */
    public boolean getLegend_doneDirtyFlag();
    /**
     * 获取 [看板进展]
     */
    public void setLegend_normal(String legend_normal);
    
    /**
     * 设置 [看板进展]
     */
    public String getLegend_normal();

    /**
     * 获取 [看板进展]脏标记
     */
    public boolean getLegend_normalDirtyFlag();
    /**
     * 获取 [媒体]
     */
    public void setMedium_id(Integer medium_id);
    
    /**
     * 设置 [媒体]
     */
    public Integer getMedium_id();

    /**
     * 获取 [媒体]脏标记
     */
    public boolean getMedium_idDirtyFlag();
    /**
     * 获取 [媒体]
     */
    public void setMedium_id_text(String medium_id_text);
    
    /**
     * 设置 [媒体]
     */
    public String getMedium_id_text();

    /**
     * 获取 [媒体]脏标记
     */
    public boolean getMedium_id_textDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [信息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [信息]
     */
    public String getMessage_ids();

    /**
     * 获取 [信息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [是关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [是关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [是关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要采取行动]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要采取行动]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要采取行动]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [行动数量]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [行动数量]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [行动数量]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [主题/应用 名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [主题/应用 名称]
     */
    public String getName();

    /**
     * 获取 [主题/应用 名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [联系]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [联系]
     */
    public Integer getPartner_id();

    /**
     * 获取 [联系]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [联系]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [联系]
     */
    public String getPartner_id_text();

    /**
     * 获取 [联系]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [手机]
     */
    public void setPartner_mobile(String partner_mobile);
    
    /**
     * 设置 [手机]
     */
    public String getPartner_mobile();

    /**
     * 获取 [手机]脏标记
     */
    public boolean getPartner_mobileDirtyFlag();
    /**
     * 获取 [申请人姓名]
     */
    public void setPartner_name(String partner_name);
    
    /**
     * 设置 [申请人姓名]
     */
    public String getPartner_name();

    /**
     * 获取 [申请人姓名]脏标记
     */
    public boolean getPartner_nameDirtyFlag();
    /**
     * 获取 [电话]
     */
    public void setPartner_phone(String partner_phone);
    
    /**
     * 设置 [电话]
     */
    public String getPartner_phone();

    /**
     * 获取 [电话]脏标记
     */
    public boolean getPartner_phoneDirtyFlag();
    /**
     * 获取 [评价]
     */
    public void setPriority(String priority);
    
    /**
     * 设置 [评价]
     */
    public String getPriority();

    /**
     * 获取 [评价]脏标记
     */
    public boolean getPriorityDirtyFlag();
    /**
     * 获取 [概率]
     */
    public void setProbability(Double probability);
    
    /**
     * 设置 [概率]
     */
    public Double getProbability();

    /**
     * 获取 [概率]脏标记
     */
    public boolean getProbabilityDirtyFlag();
    /**
     * 获取 [引荐于]
     */
    public void setReference(String reference);
    
    /**
     * 设置 [引荐于]
     */
    public String getReference();

    /**
     * 获取 [引荐于]脏标记
     */
    public boolean getReferenceDirtyFlag();
    /**
     * 获取 [期望的薪酬]
     */
    public void setSalary_expected(Double salary_expected);
    
    /**
     * 设置 [期望的薪酬]
     */
    public Double getSalary_expected();

    /**
     * 获取 [期望的薪酬]脏标记
     */
    public boolean getSalary_expectedDirtyFlag();
    /**
     * 获取 [期望薪资]
     */
    public void setSalary_expected_extra(String salary_expected_extra);
    
    /**
     * 设置 [期望薪资]
     */
    public String getSalary_expected_extra();

    /**
     * 获取 [期望薪资]脏标记
     */
    public boolean getSalary_expected_extraDirtyFlag();
    /**
     * 获取 [提议薪酬]
     */
    public void setSalary_proposed(Double salary_proposed);
    
    /**
     * 设置 [提议薪酬]
     */
    public Double getSalary_proposed();

    /**
     * 获取 [提议薪酬]脏标记
     */
    public boolean getSalary_proposedDirtyFlag();
    /**
     * 获取 [薪酬标准]
     */
    public void setSalary_proposed_extra(String salary_proposed_extra);
    
    /**
     * 设置 [薪酬标准]
     */
    public String getSalary_proposed_extra();

    /**
     * 获取 [薪酬标准]脏标记
     */
    public boolean getSalary_proposed_extraDirtyFlag();
    /**
     * 获取 [来源]
     */
    public void setSource_id(Integer source_id);
    
    /**
     * 设置 [来源]
     */
    public Integer getSource_id();

    /**
     * 获取 [来源]脏标记
     */
    public boolean getSource_idDirtyFlag();
    /**
     * 获取 [来源]
     */
    public void setSource_id_text(String source_id_text);
    
    /**
     * 设置 [来源]
     */
    public String getSource_id_text();

    /**
     * 获取 [来源]脏标记
     */
    public boolean getSource_id_textDirtyFlag();
    /**
     * 获取 [阶段]
     */
    public void setStage_id(Integer stage_id);
    
    /**
     * 设置 [阶段]
     */
    public Integer getStage_id();

    /**
     * 获取 [阶段]脏标记
     */
    public boolean getStage_idDirtyFlag();
    /**
     * 获取 [阶段]
     */
    public void setStage_id_text(String stage_id_text);
    
    /**
     * 设置 [阶段]
     */
    public String getStage_id_text();

    /**
     * 获取 [阶段]脏标记
     */
    public boolean getStage_id_textDirtyFlag();
    /**
     * 获取 [学历]
     */
    public void setType_id(Integer type_id);
    
    /**
     * 设置 [学历]
     */
    public Integer getType_id();

    /**
     * 获取 [学历]脏标记
     */
    public boolean getType_idDirtyFlag();
    /**
     * 获取 [学历]
     */
    public void setType_id_text(String type_id_text);
    
    /**
     * 设置 [学历]
     */
    public String getType_id_text();

    /**
     * 获取 [学历]脏标记
     */
    public boolean getType_id_textDirtyFlag();
    /**
     * 获取 [用户EMail]
     */
    public void setUser_email(String user_email);
    
    /**
     * 设置 [用户EMail]
     */
    public String getUser_email();

    /**
     * 获取 [用户EMail]脏标记
     */
    public boolean getUser_emailDirtyFlag();
    /**
     * 获取 [负责人]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [负责人]
     */
    public Integer getUser_id();

    /**
     * 获取 [负责人]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [负责人]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [负责人]
     */
    public String getUser_id_text();

    /**
     * 获取 [负责人]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [网站消息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站消息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站消息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
