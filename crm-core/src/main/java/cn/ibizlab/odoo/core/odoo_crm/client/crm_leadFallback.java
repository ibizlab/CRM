package cn.ibizlab.odoo.core.odoo_crm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_leadSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[crm_lead] 服务对象接口
 */
@Component
public class crm_leadFallback implements crm_leadFeignClient{

    public Crm_lead update(Integer id, Crm_lead crm_lead){
            return null;
     }
    public Boolean updateBatch(List<Crm_lead> crm_leads){
            return false;
     }



    public Page<Crm_lead> searchDefault(Crm_leadSearchContext context){
            return null;
     }


    public Crm_lead create(Crm_lead crm_lead){
            return null;
     }
    public Boolean createBatch(List<Crm_lead> crm_leads){
            return false;
     }


    public Crm_lead get(Integer id){
            return null;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Crm_lead> select(){
            return null;
     }

    public Crm_lead getDraft(){
            return null;
    }



    public Boolean save(Crm_lead crm_lead){
            return false;
     }
    public Boolean saveBatch(List<Crm_lead> crm_leads){
            return false;
     }

    public Boolean checkKey(Crm_lead crm_lead){
            return false;
     }


}
