package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_production_lot;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_production_lotSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_production_lot] 服务对象接口
 */
@Component
public class stock_production_lotFallback implements stock_production_lotFeignClient{

    public Stock_production_lot update(Integer id, Stock_production_lot stock_production_lot){
            return null;
     }
    public Boolean updateBatch(List<Stock_production_lot> stock_production_lots){
            return false;
     }




    public Page<Stock_production_lot> searchDefault(Stock_production_lotSearchContext context){
            return null;
     }


    public Stock_production_lot get(Integer id){
            return null;
     }



    public Stock_production_lot create(Stock_production_lot stock_production_lot){
            return null;
     }
    public Boolean createBatch(List<Stock_production_lot> stock_production_lots){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Stock_production_lot> select(){
            return null;
     }

    public Stock_production_lot getDraft(){
            return null;
    }



}
