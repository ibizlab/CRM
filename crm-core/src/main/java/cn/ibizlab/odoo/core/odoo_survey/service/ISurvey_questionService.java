package cn.ibizlab.odoo.core.odoo_survey.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_question;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_questionSearchContext;


/**
 * 实体[Survey_question] 服务对象接口
 */
public interface ISurvey_questionService{

    boolean create(Survey_question et) ;
    void createBatch(List<Survey_question> list) ;
    boolean update(Survey_question et) ;
    void updateBatch(List<Survey_question> list) ;
    Survey_question getDraft(Survey_question et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Survey_question get(Integer key) ;
    Page<Survey_question> searchDefault(Survey_questionSearchContext context) ;

}



