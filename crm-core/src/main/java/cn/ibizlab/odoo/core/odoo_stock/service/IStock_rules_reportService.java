package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_rules_report;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_rules_reportSearchContext;


/**
 * 实体[Stock_rules_report] 服务对象接口
 */
public interface IStock_rules_reportService{

    boolean create(Stock_rules_report et) ;
    void createBatch(List<Stock_rules_report> list) ;
    Stock_rules_report get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Stock_rules_report et) ;
    void updateBatch(List<Stock_rules_report> list) ;
    Stock_rules_report getDraft(Stock_rules_report et) ;
    Page<Stock_rules_report> searchDefault(Stock_rules_reportSearchContext context) ;

}



