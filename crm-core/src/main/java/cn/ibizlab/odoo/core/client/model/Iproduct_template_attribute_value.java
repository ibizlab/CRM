package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [product_template_attribute_value] 对象
 */
public interface Iproduct_template_attribute_value {

    /**
     * 获取 [属性]
     */
    public void setAttribute_id(Integer attribute_id);
    
    /**
     * 设置 [属性]
     */
    public Integer getAttribute_id();

    /**
     * 获取 [属性]脏标记
     */
    public boolean getAttribute_idDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [排除]
     */
    public void setExclude_for(String exclude_for);
    
    /**
     * 设置 [排除]
     */
    public String getExclude_for();

    /**
     * 获取 [排除]脏标记
     */
    public boolean getExclude_forDirtyFlag();
    /**
     * 获取 [HTML 颜色索引]
     */
    public void setHtml_color(String html_color);
    
    /**
     * 设置 [HTML 颜色索引]
     */
    public String getHtml_color();

    /**
     * 获取 [HTML 颜色索引]脏标记
     */
    public boolean getHtml_colorDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [是自定义值]
     */
    public void setIs_custom(String is_custom);
    
    /**
     * 设置 [是自定义值]
     */
    public String getIs_custom();

    /**
     * 获取 [是自定义值]脏标记
     */
    public boolean getIs_customDirtyFlag();
    /**
     * 获取 [值]
     */
    public void setName(String name);
    
    /**
     * 设置 [值]
     */
    public String getName();

    /**
     * 获取 [值]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [额外属性价格]
     */
    public void setPrice_extra(Double price_extra);
    
    /**
     * 设置 [额外属性价格]
     */
    public Double getPrice_extra();

    /**
     * 获取 [额外属性价格]脏标记
     */
    public boolean getPrice_extraDirtyFlag();
    /**
     * 获取 [属性值]
     */
    public void setProduct_attribute_value_id(Integer product_attribute_value_id);
    
    /**
     * 设置 [属性值]
     */
    public Integer getProduct_attribute_value_id();

    /**
     * 获取 [属性值]脏标记
     */
    public boolean getProduct_attribute_value_idDirtyFlag();
    /**
     * 获取 [产品模板]
     */
    public void setProduct_tmpl_id(Integer product_tmpl_id);
    
    /**
     * 设置 [产品模板]
     */
    public Integer getProduct_tmpl_id();

    /**
     * 获取 [产品模板]脏标记
     */
    public boolean getProduct_tmpl_idDirtyFlag();
    /**
     * 获取 [产品模板]
     */
    public void setProduct_tmpl_id_text(String product_tmpl_id_text);
    
    /**
     * 设置 [产品模板]
     */
    public String getProduct_tmpl_id_text();

    /**
     * 获取 [产品模板]脏标记
     */
    public boolean getProduct_tmpl_id_textDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
