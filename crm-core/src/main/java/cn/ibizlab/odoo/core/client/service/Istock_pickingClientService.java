package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_picking;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_picking] 服务对象接口
 */
public interface Istock_pickingClientService{

    public Istock_picking createModel() ;

    public void remove(Istock_picking stock_picking);

    public void create(Istock_picking stock_picking);

    public void updateBatch(List<Istock_picking> stock_pickings);

    public void get(Istock_picking stock_picking);

    public void removeBatch(List<Istock_picking> stock_pickings);

    public void update(Istock_picking stock_picking);

    public void createBatch(List<Istock_picking> stock_pickings);

    public Page<Istock_picking> fetchDefault(SearchContext context);

    public Page<Istock_picking> select(SearchContext context);

    public void getDraft(Istock_picking stock_picking);

}
