package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_tag;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_tagSearchContext;


/**
 * 实体[Mail_mass_mailing_tag] 服务对象接口
 */
public interface IMail_mass_mailing_tagService{

    Mail_mass_mailing_tag getDraft(Mail_mass_mailing_tag et) ;
    boolean update(Mail_mass_mailing_tag et) ;
    void updateBatch(List<Mail_mass_mailing_tag> list) ;
    boolean create(Mail_mass_mailing_tag et) ;
    void createBatch(List<Mail_mass_mailing_tag> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Mail_mass_mailing_tag get(Integer key) ;
    Page<Mail_mass_mailing_tag> searchDefault(Mail_mass_mailing_tagSearchContext context) ;

}



