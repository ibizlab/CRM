package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_alias_mixin;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_alias_mixin] 服务对象接口
 */
public interface Imail_alias_mixinClientService{

    public Imail_alias_mixin createModel() ;

    public void updateBatch(List<Imail_alias_mixin> mail_alias_mixins);

    public void remove(Imail_alias_mixin mail_alias_mixin);

    public Page<Imail_alias_mixin> fetchDefault(SearchContext context);

    public void removeBatch(List<Imail_alias_mixin> mail_alias_mixins);

    public void update(Imail_alias_mixin mail_alias_mixin);

    public void create(Imail_alias_mixin mail_alias_mixin);

    public void get(Imail_alias_mixin mail_alias_mixin);

    public void createBatch(List<Imail_alias_mixin> mail_alias_mixins);

    public Page<Imail_alias_mixin> select(SearchContext context);

    public void getDraft(Imail_alias_mixin mail_alias_mixin);

}
