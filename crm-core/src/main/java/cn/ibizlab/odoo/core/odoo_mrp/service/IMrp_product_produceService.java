package cn.ibizlab.odoo.core.odoo_mrp.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_product_produce;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_product_produceSearchContext;


/**
 * 实体[Mrp_product_produce] 服务对象接口
 */
public interface IMrp_product_produceService{

    boolean create(Mrp_product_produce et) ;
    void createBatch(List<Mrp_product_produce> list) ;
    Mrp_product_produce getDraft(Mrp_product_produce et) ;
    boolean update(Mrp_product_produce et) ;
    void updateBatch(List<Mrp_product_produce> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Mrp_product_produce get(Integer key) ;
    Page<Mrp_product_produce> searchDefault(Mrp_product_produceSearchContext context) ;

}



