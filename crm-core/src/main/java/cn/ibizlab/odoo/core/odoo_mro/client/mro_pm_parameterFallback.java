package cn.ibizlab.odoo.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_parameter;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_parameterSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mro_pm_parameter] 服务对象接口
 */
@Component
public class mro_pm_parameterFallback implements mro_pm_parameterFeignClient{

    public Page<Mro_pm_parameter> searchDefault(Mro_pm_parameterSearchContext context){
            return null;
     }



    public Mro_pm_parameter get(Integer id){
            return null;
     }


    public Mro_pm_parameter create(Mro_pm_parameter mro_pm_parameter){
            return null;
     }
    public Boolean createBatch(List<Mro_pm_parameter> mro_pm_parameters){
            return false;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Mro_pm_parameter update(Integer id, Mro_pm_parameter mro_pm_parameter){
            return null;
     }
    public Boolean updateBatch(List<Mro_pm_parameter> mro_pm_parameters){
            return false;
     }


    public Page<Mro_pm_parameter> select(){
            return null;
     }

    public Mro_pm_parameter getDraft(){
            return null;
    }



}
