package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iproduct_putaway;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_putaway] 服务对象接口
 */
public interface Iproduct_putawayClientService{

    public Iproduct_putaway createModel() ;

    public void createBatch(List<Iproduct_putaway> product_putaways);

    public void create(Iproduct_putaway product_putaway);

    public Page<Iproduct_putaway> fetchDefault(SearchContext context);

    public void update(Iproduct_putaway product_putaway);

    public void removeBatch(List<Iproduct_putaway> product_putaways);

    public void remove(Iproduct_putaway product_putaway);

    public void updateBatch(List<Iproduct_putaway> product_putaways);

    public void get(Iproduct_putaway product_putaway);

    public Page<Iproduct_putaway> select(SearchContext context);

    public void getDraft(Iproduct_putaway product_putaway);

}
