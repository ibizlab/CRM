package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_automation_lead_test;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_automation_lead_testSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_automation_lead_testService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.base_automation_lead_testFeignClient;

/**
 * 实体[自动化规则测试] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_automation_lead_testServiceImpl implements IBase_automation_lead_testService {

    @Autowired
    base_automation_lead_testFeignClient base_automation_lead_testFeignClient;


    @Override
    public Base_automation_lead_test getDraft(Base_automation_lead_test et) {
        et=base_automation_lead_testFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=base_automation_lead_testFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        base_automation_lead_testFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Base_automation_lead_test et) {
        Base_automation_lead_test rt = base_automation_lead_testFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_automation_lead_test> list){
        base_automation_lead_testFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Base_automation_lead_test et) {
        Base_automation_lead_test rt = base_automation_lead_testFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Base_automation_lead_test> list){
        base_automation_lead_testFeignClient.updateBatch(list) ;
    }

    @Override
    public Base_automation_lead_test get(Integer id) {
		Base_automation_lead_test et=base_automation_lead_testFeignClient.get(id);
        if(et==null){
            et=new Base_automation_lead_test();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_automation_lead_test> searchDefault(Base_automation_lead_testSearchContext context) {
        Page<Base_automation_lead_test> base_automation_lead_tests=base_automation_lead_testFeignClient.searchDefault(context);
        return base_automation_lead_tests;
    }


}


