package cn.ibizlab.odoo.core.odoo_portal.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_mixin;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_mixinSearchContext;


/**
 * 实体[Portal_mixin] 服务对象接口
 */
public interface IPortal_mixinService{

    Portal_mixin get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Portal_mixin et) ;
    void updateBatch(List<Portal_mixin> list) ;
    Portal_mixin getDraft(Portal_mixin et) ;
    boolean create(Portal_mixin et) ;
    void createBatch(List<Portal_mixin> list) ;
    Page<Portal_mixin> searchDefault(Portal_mixinSearchContext context) ;

}



