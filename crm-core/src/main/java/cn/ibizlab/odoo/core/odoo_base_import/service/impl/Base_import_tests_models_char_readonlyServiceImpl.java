package cn.ibizlab.odoo.core.odoo_base_import.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_char_readonly;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_char_readonlySearchContext;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_tests_models_char_readonlyService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base_import.client.base_import_tests_models_char_readonlyFeignClient;

/**
 * 实体[测试:基本导入模型，字符只读] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_import_tests_models_char_readonlyServiceImpl implements IBase_import_tests_models_char_readonlyService {

    @Autowired
    base_import_tests_models_char_readonlyFeignClient base_import_tests_models_char_readonlyFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=base_import_tests_models_char_readonlyFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        base_import_tests_models_char_readonlyFeignClient.removeBatch(idList);
    }

    @Override
    public Base_import_tests_models_char_readonly getDraft(Base_import_tests_models_char_readonly et) {
        et=base_import_tests_models_char_readonlyFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Base_import_tests_models_char_readonly et) {
        Base_import_tests_models_char_readonly rt = base_import_tests_models_char_readonlyFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Base_import_tests_models_char_readonly> list){
        base_import_tests_models_char_readonlyFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Base_import_tests_models_char_readonly et) {
        Base_import_tests_models_char_readonly rt = base_import_tests_models_char_readonlyFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_import_tests_models_char_readonly> list){
        base_import_tests_models_char_readonlyFeignClient.createBatch(list) ;
    }

    @Override
    public Base_import_tests_models_char_readonly get(Integer id) {
		Base_import_tests_models_char_readonly et=base_import_tests_models_char_readonlyFeignClient.get(id);
        if(et==null){
            et=new Base_import_tests_models_char_readonly();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_import_tests_models_char_readonly> searchDefault(Base_import_tests_models_char_readonlySearchContext context) {
        Page<Base_import_tests_models_char_readonly> base_import_tests_models_char_readonlys=base_import_tests_models_char_readonlyFeignClient.searchDefault(context);
        return base_import_tests_models_char_readonlys;
    }


}


