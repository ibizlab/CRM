package cn.ibizlab.odoo.core.odoo_crm.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_partner_binding;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_partner_bindingSearchContext;


/**
 * 实体[Crm_partner_binding] 服务对象接口
 */
public interface ICrm_partner_bindingService{

    Crm_partner_binding getDraft(Crm_partner_binding et) ;
    Crm_partner_binding get(Integer key) ;
    boolean update(Crm_partner_binding et) ;
    void updateBatch(List<Crm_partner_binding> list) ;
    boolean create(Crm_partner_binding et) ;
    void createBatch(List<Crm_partner_binding> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Crm_partner_binding> searchDefault(Crm_partner_bindingSearchContext context) ;

}



