package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Isale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[sale_payment_acquirer_onboarding_wizard] 服务对象接口
 */
public interface Isale_payment_acquirer_onboarding_wizardClientService{

    public Isale_payment_acquirer_onboarding_wizard createModel() ;

    public void update(Isale_payment_acquirer_onboarding_wizard sale_payment_acquirer_onboarding_wizard);

    public void createBatch(List<Isale_payment_acquirer_onboarding_wizard> sale_payment_acquirer_onboarding_wizards);

    public void remove(Isale_payment_acquirer_onboarding_wizard sale_payment_acquirer_onboarding_wizard);

    public void get(Isale_payment_acquirer_onboarding_wizard sale_payment_acquirer_onboarding_wizard);

    public Page<Isale_payment_acquirer_onboarding_wizard> fetchDefault(SearchContext context);

    public void removeBatch(List<Isale_payment_acquirer_onboarding_wizard> sale_payment_acquirer_onboarding_wizards);

    public void create(Isale_payment_acquirer_onboarding_wizard sale_payment_acquirer_onboarding_wizard);

    public void updateBatch(List<Isale_payment_acquirer_onboarding_wizard> sale_payment_acquirer_onboarding_wizards);

    public Page<Isale_payment_acquirer_onboarding_wizard> select(SearchContext context);

    public void getDraft(Isale_payment_acquirer_onboarding_wizard sale_payment_acquirer_onboarding_wizard);

}
