package cn.ibizlab.odoo.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave_type;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leave_typeSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[hr_leave_type] 服务对象接口
 */
@Component
public class hr_leave_typeFallback implements hr_leave_typeFeignClient{


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Hr_leave_type create(Hr_leave_type hr_leave_type){
            return null;
     }
    public Boolean createBatch(List<Hr_leave_type> hr_leave_types){
            return false;
     }

    public Page<Hr_leave_type> searchDefault(Hr_leave_typeSearchContext context){
            return null;
     }


    public Hr_leave_type get(Integer id){
            return null;
     }


    public Hr_leave_type update(Integer id, Hr_leave_type hr_leave_type){
            return null;
     }
    public Boolean updateBatch(List<Hr_leave_type> hr_leave_types){
            return false;
     }




    public Page<Hr_leave_type> select(){
            return null;
     }

    public Hr_leave_type getDraft(){
            return null;
    }



}
