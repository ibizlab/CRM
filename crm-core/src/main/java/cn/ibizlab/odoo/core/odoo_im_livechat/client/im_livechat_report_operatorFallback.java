package cn.ibizlab.odoo.core.odoo_im_livechat.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_report_operator;
import cn.ibizlab.odoo.core.odoo_im_livechat.filter.Im_livechat_report_operatorSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[im_livechat_report_operator] 服务对象接口
 */
@Component
public class im_livechat_report_operatorFallback implements im_livechat_report_operatorFeignClient{

    public Page<Im_livechat_report_operator> searchDefault(Im_livechat_report_operatorSearchContext context){
            return null;
     }





    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Im_livechat_report_operator create(Im_livechat_report_operator im_livechat_report_operator){
            return null;
     }
    public Boolean createBatch(List<Im_livechat_report_operator> im_livechat_report_operators){
            return false;
     }

    public Im_livechat_report_operator update(Integer id, Im_livechat_report_operator im_livechat_report_operator){
            return null;
     }
    public Boolean updateBatch(List<Im_livechat_report_operator> im_livechat_report_operators){
            return false;
     }


    public Im_livechat_report_operator get(Integer id){
            return null;
     }


    public Page<Im_livechat_report_operator> select(){
            return null;
     }

    public Im_livechat_report_operator getDraft(){
            return null;
    }



}
