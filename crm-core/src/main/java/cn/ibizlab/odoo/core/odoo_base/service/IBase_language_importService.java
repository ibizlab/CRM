package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Base_language_import;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_language_importSearchContext;


/**
 * 实体[Base_language_import] 服务对象接口
 */
public interface IBase_language_importService{

    boolean create(Base_language_import et) ;
    void createBatch(List<Base_language_import> list) ;
    Base_language_import getDraft(Base_language_import et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Base_language_import et) ;
    void updateBatch(List<Base_language_import> list) ;
    Base_language_import get(Integer key) ;
    Page<Base_language_import> searchDefault(Base_language_importSearchContext context) ;

}



