package cn.ibizlab.odoo.core.odoo_calendar.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_event_type;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_event_typeSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[calendar_event_type] 服务对象接口
 */
@FeignClient(value = "odoo-calendar", contextId = "calendar-event-type", fallback = calendar_event_typeFallback.class)
public interface calendar_event_typeFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/calendar_event_types/searchdefault")
    Page<Calendar_event_type> searchDefault(@RequestBody Calendar_event_typeSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/calendar_event_types/{id}")
    Calendar_event_type update(@PathVariable("id") Integer id,@RequestBody Calendar_event_type calendar_event_type);

    @RequestMapping(method = RequestMethod.PUT, value = "/calendar_event_types/batch")
    Boolean updateBatch(@RequestBody List<Calendar_event_type> calendar_event_types);


    @RequestMapping(method = RequestMethod.GET, value = "/calendar_event_types/{id}")
    Calendar_event_type get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/calendar_event_types")
    Calendar_event_type create(@RequestBody Calendar_event_type calendar_event_type);

    @RequestMapping(method = RequestMethod.POST, value = "/calendar_event_types/batch")
    Boolean createBatch(@RequestBody List<Calendar_event_type> calendar_event_types);


    @RequestMapping(method = RequestMethod.DELETE, value = "/calendar_event_types/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/calendar_event_types/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);




    @RequestMapping(method = RequestMethod.GET, value = "/calendar_event_types/select")
    Page<Calendar_event_type> select();


    @RequestMapping(method = RequestMethod.GET, value = "/calendar_event_types/getdraft")
    Calendar_event_type getDraft();


}
