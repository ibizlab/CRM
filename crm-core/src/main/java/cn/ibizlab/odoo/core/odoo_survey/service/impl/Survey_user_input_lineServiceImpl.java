package cn.ibizlab.odoo.core.odoo_survey.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_user_input_line;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_user_input_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_user_input_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_survey.client.survey_user_input_lineFeignClient;

/**
 * 实体[调查用户输入明细] 服务对象接口实现
 */
@Slf4j
@Service
public class Survey_user_input_lineServiceImpl implements ISurvey_user_input_lineService {

    @Autowired
    survey_user_input_lineFeignClient survey_user_input_lineFeignClient;


    @Override
    public boolean update(Survey_user_input_line et) {
        Survey_user_input_line rt = survey_user_input_lineFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Survey_user_input_line> list){
        survey_user_input_lineFeignClient.updateBatch(list) ;
    }

    @Override
    public Survey_user_input_line get(Integer id) {
		Survey_user_input_line et=survey_user_input_lineFeignClient.get(id);
        if(et==null){
            et=new Survey_user_input_line();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Survey_user_input_line getDraft(Survey_user_input_line et) {
        et=survey_user_input_lineFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Survey_user_input_line et) {
        Survey_user_input_line rt = survey_user_input_lineFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Survey_user_input_line> list){
        survey_user_input_lineFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=survey_user_input_lineFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        survey_user_input_lineFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Survey_user_input_line> searchDefault(Survey_user_input_lineSearchContext context) {
        Page<Survey_user_input_line> survey_user_input_lines=survey_user_input_lineFeignClient.searchDefault(context);
        return survey_user_input_lines;
    }


}


