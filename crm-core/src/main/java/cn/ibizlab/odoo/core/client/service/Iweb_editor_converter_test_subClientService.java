package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iweb_editor_converter_test_sub;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[web_editor_converter_test_sub] 服务对象接口
 */
public interface Iweb_editor_converter_test_subClientService{

    public Iweb_editor_converter_test_sub createModel() ;

    public void remove(Iweb_editor_converter_test_sub web_editor_converter_test_sub);

    public void create(Iweb_editor_converter_test_sub web_editor_converter_test_sub);

    public Page<Iweb_editor_converter_test_sub> fetchDefault(SearchContext context);

    public void updateBatch(List<Iweb_editor_converter_test_sub> web_editor_converter_test_subs);

    public void createBatch(List<Iweb_editor_converter_test_sub> web_editor_converter_test_subs);

    public void update(Iweb_editor_converter_test_sub web_editor_converter_test_sub);

    public void removeBatch(List<Iweb_editor_converter_test_sub> web_editor_converter_test_subs);

    public void get(Iweb_editor_converter_test_sub web_editor_converter_test_sub);

    public Page<Iweb_editor_converter_test_sub> select(SearchContext context);

    public void getDraft(Iweb_editor_converter_test_sub web_editor_converter_test_sub);

}
