package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking_type;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_picking_typeSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_picking_typeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_picking_typeFeignClient;

/**
 * 实体[拣货类型] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_picking_typeServiceImpl implements IStock_picking_typeService {

    @Autowired
    stock_picking_typeFeignClient stock_picking_typeFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=stock_picking_typeFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_picking_typeFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Stock_picking_type et) {
        Stock_picking_type rt = stock_picking_typeFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_picking_type> list){
        stock_picking_typeFeignClient.updateBatch(list) ;
    }

    @Override
    public Stock_picking_type getDraft(Stock_picking_type et) {
        et=stock_picking_typeFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Stock_picking_type et) {
        Stock_picking_type rt = stock_picking_typeFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_picking_type> list){
        stock_picking_typeFeignClient.createBatch(list) ;
    }

    @Override
    public Stock_picking_type get(Integer id) {
		Stock_picking_type et=stock_picking_typeFeignClient.get(id);
        if(et==null){
            et=new Stock_picking_type();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_picking_type> searchDefault(Stock_picking_typeSearchContext context) {
        Page<Stock_picking_type> stock_picking_types=stock_picking_typeFeignClient.searchDefault(context);
        return stock_picking_types;
    }


}


