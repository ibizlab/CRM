package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_list_contact_rel;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_list_contact_relSearchContext;


/**
 * 实体[Mail_mass_mailing_list_contact_rel] 服务对象接口
 */
public interface IMail_mass_mailing_list_contact_relService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Mail_mass_mailing_list_contact_rel et) ;
    void createBatch(List<Mail_mass_mailing_list_contact_rel> list) ;
    Mail_mass_mailing_list_contact_rel getDraft(Mail_mass_mailing_list_contact_rel et) ;
    Mail_mass_mailing_list_contact_rel get(Integer key) ;
    boolean update(Mail_mass_mailing_list_contact_rel et) ;
    void updateBatch(List<Mail_mass_mailing_list_contact_rel> list) ;
    Page<Mail_mass_mailing_list_contact_rel> searchDefault(Mail_mass_mailing_list_contact_relSearchContext context) ;

}



