package cn.ibizlab.odoo.core.odoo_website.valuerule.anno.website_published_multi_mixin;

import cn.ibizlab.odoo.core.odoo_website.valuerule.validator.website_published_multi_mixin.Website_published_multi_mixinWebsite_publishedDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Website_published_multi_mixin
 * 属性：Website_published
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Website_published_multi_mixinWebsite_publishedDefaultValidator.class})
public @interface Website_published_multi_mixinWebsite_publishedDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
