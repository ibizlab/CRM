package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_bank_statement;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_bank_statement] 服务对象接口
 */
public interface Iaccount_bank_statementClientService{

    public Iaccount_bank_statement createModel() ;

    public void remove(Iaccount_bank_statement account_bank_statement);

    public Page<Iaccount_bank_statement> fetchDefault(SearchContext context);

    public void createBatch(List<Iaccount_bank_statement> account_bank_statements);

    public void get(Iaccount_bank_statement account_bank_statement);

    public void removeBatch(List<Iaccount_bank_statement> account_bank_statements);

    public void create(Iaccount_bank_statement account_bank_statement);

    public void update(Iaccount_bank_statement account_bank_statement);

    public void updateBatch(List<Iaccount_bank_statement> account_bank_statements);

    public Page<Iaccount_bank_statement> select(SearchContext context);

    public void getDraft(Iaccount_bank_statement account_bank_statement);

}
