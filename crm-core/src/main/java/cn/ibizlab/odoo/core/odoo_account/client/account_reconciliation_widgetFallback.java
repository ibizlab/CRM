package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_reconciliation_widget;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_reconciliation_widgetSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_reconciliation_widget] 服务对象接口
 */
@Component
public class account_reconciliation_widgetFallback implements account_reconciliation_widgetFeignClient{


    public Account_reconciliation_widget update(Integer id, Account_reconciliation_widget account_reconciliation_widget){
            return null;
     }
    public Boolean updateBatch(List<Account_reconciliation_widget> account_reconciliation_widgets){
            return false;
     }



    public Account_reconciliation_widget get(Integer id){
            return null;
     }


    public Page<Account_reconciliation_widget> searchDefault(Account_reconciliation_widgetSearchContext context){
            return null;
     }


    public Account_reconciliation_widget create(Account_reconciliation_widget account_reconciliation_widget){
            return null;
     }
    public Boolean createBatch(List<Account_reconciliation_widget> account_reconciliation_widgets){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Page<Account_reconciliation_widget> select(){
            return null;
     }

    public Account_reconciliation_widget getDraft(){
            return null;
    }



}
