package cn.ibizlab.odoo.core.odoo_lunch.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_product_category;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_product_categorySearchContext;
import cn.ibizlab.odoo.core.odoo_lunch.service.ILunch_product_categoryService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_lunch.client.lunch_product_categoryFeignClient;

/**
 * 实体[工作餐产品类别] 服务对象接口实现
 */
@Slf4j
@Service
public class Lunch_product_categoryServiceImpl implements ILunch_product_categoryService {

    @Autowired
    lunch_product_categoryFeignClient lunch_product_categoryFeignClient;


    @Override
    public Lunch_product_category get(Integer id) {
		Lunch_product_category et=lunch_product_categoryFeignClient.get(id);
        if(et==null){
            et=new Lunch_product_category();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Lunch_product_category et) {
        Lunch_product_category rt = lunch_product_categoryFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Lunch_product_category> list){
        lunch_product_categoryFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=lunch_product_categoryFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        lunch_product_categoryFeignClient.removeBatch(idList);
    }

    @Override
    public Lunch_product_category getDraft(Lunch_product_category et) {
        et=lunch_product_categoryFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Lunch_product_category et) {
        Lunch_product_category rt = lunch_product_categoryFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Lunch_product_category> list){
        lunch_product_categoryFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Lunch_product_category> searchDefault(Lunch_product_categorySearchContext context) {
        Page<Lunch_product_category> lunch_product_categorys=lunch_product_categoryFeignClient.searchDefault(context);
        return lunch_product_categorys;
    }


}


