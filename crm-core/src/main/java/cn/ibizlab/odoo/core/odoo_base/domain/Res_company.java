package cn.ibizlab.odoo.core.odoo_base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [公司] 对象
 */
@Data
public class Res_company extends EntityClient implements Serializable {

    /**
     * 公司口号
     */
    @DEField(name = "report_header")
    @JSONField(name = "report_header")
    @JsonProperty("report_header")
    private String reportHeader;

    /**
     * 国家/地区
     */
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    private Integer countryId;

    /**
     * 正进行销售面板的状态
     */
    @DEField(name = "sale_quotation_onboarding_state")
    @JSONField(name = "sale_quotation_onboarding_state")
    @JsonProperty("sale_quotation_onboarding_state")
    private String saleQuotationOnboardingState;

    /**
     * 默认报价有效期（日）
     */
    @DEField(name = "quotation_validity_days")
    @JSONField(name = "quotation_validity_days")
    @JsonProperty("quotation_validity_days")
    private Integer quotationValidityDays;

    /**
     * 有待被确认的发票步骤的状态
     */
    @DEField(name = "account_onboarding_invoice_layout_state")
    @JSONField(name = "account_onboarding_invoice_layout_state")
    @JsonProperty("account_onboarding_invoice_layout_state")
    private String accountOnboardingInvoiceLayoutState;

    /**
     * 科目号码
     */
    @DEField(name = "account_no")
    @JSONField(name = "account_no")
    @JsonProperty("account_no")
    private String accountNo;

    /**
     * 银行科目的前缀
     */
    @DEField(name = "bank_account_code_prefix")
    @JSONField(name = "bank_account_code_prefix")
    @JsonProperty("bank_account_code_prefix")
    private String bankAccountCodePrefix;

    /**
     * 银行日记账
     */
    @JSONField(name = "bank_journal_ids")
    @JsonProperty("bank_journal_ids")
    private String bankJournalIds;

    /**
     * 邮政编码
     */
    @JSONField(name = "zip")
    @JsonProperty("zip")
    private String zip;

    /**
     * 非顾问的锁定日期
     */
    @DEField(name = "period_lock_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "period_lock_date" , format="yyyy-MM-dd")
    @JsonProperty("period_lock_date")
    private Timestamp periodLockDate;

    /**
     * 状态
     */
    @JSONField(name = "state_id")
    @JsonProperty("state_id")
    private Integer stateId;

    /**
     * 使用现金收付制
     */
    @DEField(name = "tax_exigibility")
    @JSONField(name = "tax_exigibility")
    @JsonProperty("tax_exigibility")
    private String taxExigibility;

    /**
     * 工作时间
     */
    @JSONField(name = "resource_calendar_ids")
    @JsonProperty("resource_calendar_ids")
    private String resourceCalendarIds;

    /**
     * 银行账户
     */
    @JSONField(name = "bank_ids")
    @JsonProperty("bank_ids")
    private String bankIds;

    /**
     * 处于会计面板的状态
     */
    @DEField(name = "account_dashboard_onboarding_state")
    @JSONField(name = "account_dashboard_onboarding_state")
    @JsonProperty("account_dashboard_onboarding_state")
    private String accountDashboardOnboardingState;

    /**
     * 连接在一起的库存移动的日期变化传播的最小差值。
     */
    @DEField(name = "propagation_minimum_delta")
    @JSONField(name = "propagation_minimum_delta")
    @JsonProperty("propagation_minimum_delta")
    private Integer propagationMinimumDelta;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 颜色
     */
    @DEField(name = "snailmail_color")
    @JSONField(name = "snailmail_color")
    @JsonProperty("snailmail_color")
    private String snailmailColor;

    /**
     * 逾期追款消息
     */
    @DEField(name = "overdue_msg")
    @JSONField(name = "overdue_msg")
    @JsonProperty("overdue_msg")
    private String overdueMsg;

    /**
     * 城市
     */
    @JSONField(name = "city")
    @JsonProperty("city")
    private String city;

    /**
     * 科目状态
     */
    @DEField(name = "account_setup_coa_state")
    @JSONField(name = "account_setup_coa_state")
    @JsonProperty("account_setup_coa_state")
    private String accountSetupCoaState;

    /**
     * 默认信息类型
     */
    @DEField(name = "invoice_reference_type")
    @JSONField(name = "invoice_reference_type")
    @JsonProperty("invoice_reference_type")
    private String invoiceReferenceType;

    /**
     * 预设邮件
     */
    @JSONField(name = "catchall")
    @JsonProperty("catchall")
    private String catchall;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 使用anglo-saxon会计
     */
    @DEField(name = "anglo_saxon_accounting")
    @JSONField(name = "anglo_saxon_accounting")
    @JsonProperty("anglo_saxon_accounting")
    private String angloSaxonAccounting;

    /**
     * 双面
     */
    @DEField(name = "snailmail_duplex")
    @JSONField(name = "snailmail_duplex")
    @JsonProperty("snailmail_duplex")
    private String snailmailDuplex;

    /**
     * GitHub账户
     */
    @DEField(name = "social_github")
    @JSONField(name = "social_github")
    @JsonProperty("social_github")
    private String socialGithub;

    /**
     * 有待被确认的银行数据步骤的状态
     */
    @DEField(name = "account_setup_bank_data_state")
    @JSONField(name = "account_setup_bank_data_state")
    @JsonProperty("account_setup_bank_data_state")
    private String accountSetupBankDataState;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 街道 2
     */
    @JSONField(name = "street2")
    @JsonProperty("street2")
    private String street2;

    /**
     * 预计会计科目表
     */
    @DEField(name = "expects_chart_of_accounts")
    @JSONField(name = "expects_chart_of_accounts")
    @JsonProperty("expects_chart_of_accounts")
    private String expectsChartOfAccounts;

    /**
     * 转账帐户的前缀
     */
    @DEField(name = "transfer_account_code_prefix")
    @JSONField(name = "transfer_account_code_prefix")
    @JsonProperty("transfer_account_code_prefix")
    private String transferAccountCodePrefix;

    /**
     * 会计年度的最后一天
     */
    @DEField(name = "fiscalyear_last_day")
    @JSONField(name = "fiscalyear_last_day")
    @JsonProperty("fiscalyear_last_day")
    private Integer fiscalyearLastDay;

    /**
     * 接受的用户
     */
    @JSONField(name = "user_ids")
    @JsonProperty("user_ids")
    private String userIds;

    /**
     * 银行核销阈值
     */
    @DEField(name = "account_bank_reconciliation_start")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "account_bank_reconciliation_start" , format="yyyy-MM-dd")
    @JsonProperty("account_bank_reconciliation_start")
    private Timestamp accountBankReconciliationStart;

    /**
     * 在线支付
     */
    @DEField(name = "portal_confirmation_pay")
    @JSONField(name = "portal_confirmation_pay")
    @JsonProperty("portal_confirmation_pay")
    private String portalConfirmationPay;

    /**
     * 显示SEPA QR码
     */
    @DEField(name = "qr_code")
    @JSONField(name = "qr_code")
    @JsonProperty("qr_code")
    private String qrCode;

    /**
     * 街道
     */
    @JSONField(name = "street")
    @JsonProperty("street")
    private String street;

    /**
     * 处于会计发票面板的状态
     */
    @DEField(name = "account_invoice_onboarding_state")
    @JSONField(name = "account_invoice_onboarding_state")
    @JsonProperty("account_invoice_onboarding_state")
    private String accountInvoiceOnboardingState;

    /**
     * 下级公司
     */
    @JSONField(name = "child_ids")
    @JsonProperty("child_ids")
    private String childIds;

    /**
     * 序号
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 命名规则
     */
    @DEField(name = "nomenclature_id")
    @JSONField(name = "nomenclature_id")
    @JsonProperty("nomenclature_id")
    private Integer nomenclatureId;

    /**
     * Google+账户
     */
    @DEField(name = "social_googleplus")
    @JSONField(name = "social_googleplus")
    @JsonProperty("social_googleplus")
    private String socialGoogleplus;

    /**
     * 入职支付收单机构的状态
     */
    @DEField(name = "payment_acquirer_onboarding_state")
    @JSONField(name = "payment_acquirer_onboarding_state")
    @JsonProperty("payment_acquirer_onboarding_state")
    private String paymentAcquirerOnboardingState;

    /**
     * 报表页脚
     */
    @DEField(name = "report_footer")
    @JSONField(name = "report_footer")
    @JsonProperty("report_footer")
    private String reportFooter;

    /**
     * 选择付款方式
     */
    @DEField(name = "payment_onboarding_payment_method")
    @JSONField(name = "payment_onboarding_payment_method")
    @JsonProperty("payment_onboarding_payment_method")
    private String paymentOnboardingPaymentMethod;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 批准等级
     */
    @DEField(name = "po_double_validation")
    @JSONField(name = "po_double_validation")
    @JsonProperty("po_double_validation")
    private String poDoubleValidation;

    /**
     * 采购提前时间
     */
    @DEField(name = "po_lead")
    @JSONField(name = "po_lead")
    @JsonProperty("po_lead")
    private Double poLead;

    /**
     * 有待被确认的样品报价单步骤的状态
     */
    @DEField(name = "sale_onboarding_sample_quotation_state")
    @JSONField(name = "sale_onboarding_sample_quotation_state")
    @JsonProperty("sale_onboarding_sample_quotation_state")
    private String saleOnboardingSampleQuotationState;

    /**
     * 有待被确认的订单步骤的状态
     */
    @DEField(name = "sale_onboarding_order_confirmation_state")
    @JSONField(name = "sale_onboarding_order_confirmation_state")
    @JsonProperty("sale_onboarding_order_confirmation_state")
    private String saleOnboardingOrderConfirmationState;

    /**
     * 文档模板
     */
    @DEField(name = "external_report_layout_id")
    @JSONField(name = "external_report_layout_id")
    @JsonProperty("external_report_layout_id")
    private Integer externalReportLayoutId;

    /**
     * 请选择付款方式
     */
    @DEField(name = "sale_onboarding_payment_method")
    @JSONField(name = "sale_onboarding_payment_method")
    @JsonProperty("sale_onboarding_payment_method")
    private String saleOnboardingPaymentMethod;

    /**
     * 有待被确认的样品报价单步骤的状态
     */
    @DEField(name = "account_onboarding_sample_invoice_state")
    @JSONField(name = "account_onboarding_sample_invoice_state")
    @JsonProperty("account_onboarding_sample_invoice_state")
    private String accountOnboardingSampleInvoiceState;

    /**
     * 公司状态
     */
    @DEField(name = "base_onboarding_company_state")
    @JSONField(name = "base_onboarding_company_state")
    @JsonProperty("base_onboarding_company_state")
    private String baseOnboardingCompanyState;

    /**
     * 领英账号
     */
    @DEField(name = "social_linkedin")
    @JSONField(name = "social_linkedin")
    @JsonProperty("social_linkedin")
    private String socialLinkedin;

    /**
     * 制造提前期(日)
     */
    @DEField(name = "manufacturing_lead")
    @JSONField(name = "manufacturing_lead")
    @JsonProperty("manufacturing_lead")
    private Double manufacturingLead;

    /**
     * 默认条款和条件
     */
    @DEField(name = "sale_note")
    @JSONField(name = "sale_note")
    @JsonProperty("sale_note")
    private String saleNote;

    /**
     * 再次验证金额
     */
    @DEField(name = "po_double_validation_amount")
    @JSONField(name = "po_double_validation_amount")
    @JsonProperty("po_double_validation_amount")
    private Double poDoubleValidationAmount;

    /**
     * 销售订单修改
     */
    @DEField(name = "po_lock")
    @JSONField(name = "po_lock")
    @JsonProperty("po_lock")
    private String poLock;

    /**
     * Twitter账号
     */
    @DEField(name = "social_twitter")
    @JSONField(name = "social_twitter")
    @JsonProperty("social_twitter")
    private String socialTwitter;

    /**
     * Instagram 账号
     */
    @DEField(name = "social_instagram")
    @JSONField(name = "social_instagram")
    @JsonProperty("social_instagram")
    private String socialInstagram;

    /**
     * 有待被确认的会计年度步骤的状态
     */
    @DEField(name = "account_setup_fy_data_state")
    @JSONField(name = "account_setup_fy_data_state")
    @JsonProperty("account_setup_fy_data_state")
    private String accountSetupFyDataState;

    /**
     * 税率计算的舍入方法
     */
    @DEField(name = "tax_calculation_rounding_method")
    @JSONField(name = "tax_calculation_rounding_method")
    @JsonProperty("tax_calculation_rounding_method")
    private String taxCalculationRoundingMethod;

    /**
     * 现金科目的前缀
     */
    @DEField(name = "cash_account_code_prefix")
    @JSONField(name = "cash_account_code_prefix")
    @JsonProperty("cash_account_code_prefix")
    private String cashAccountCodePrefix;

    /**
     * 有待被确认的报价单步骤的状态
     */
    @DEField(name = "account_onboarding_sale_tax_state")
    @JSONField(name = "account_onboarding_sale_tax_state")
    @JsonProperty("account_onboarding_sale_tax_state")
    private String accountOnboardingSaleTaxState;

    /**
     * 销售安全天数
     */
    @DEField(name = "security_lead")
    @JSONField(name = "security_lead")
    @JsonProperty("security_lead")
    private Double securityLead;

    /**
     * 招聘网站主题一步完成
     */
    @JSONField(name = "website_theme_onboarding_done")
    @JsonProperty("website_theme_onboarding_done")
    private String websiteThemeOnboardingDone;

    /**
     * 通过默认值打印
     */
    @DEField(name = "invoice_is_print")
    @JSONField(name = "invoice_is_print")
    @JsonProperty("invoice_is_print")
    private String invoiceIsPrint;

    /**
     * 公司注册
     */
    @DEField(name = "company_registry")
    @JSONField(name = "company_registry")
    @JsonProperty("company_registry")
    private String companyRegistry;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 网页徽标
     */
    @JSONField(name = "logo_web")
    @JsonProperty("logo_web")
    private byte[] logoWeb;

    /**
     * 锁定日期
     */
    @DEField(name = "fiscalyear_lock_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "fiscalyear_lock_date" , format="yyyy-MM-dd")
    @JsonProperty("fiscalyear_lock_date")
    private Timestamp fiscalyearLockDate;

    /**
     * 默认以信件发送
     */
    @DEField(name = "invoice_is_snailmail")
    @JSONField(name = "invoice_is_snailmail")
    @JsonProperty("invoice_is_snailmail")
    private String invoiceIsSnailmail;

    /**
     * 网站销售状态入职付款收单机构步骤
     */
    @DEField(name = "website_sale_onboarding_payment_acquirer_state")
    @JSONField(name = "website_sale_onboarding_payment_acquirer_state")
    @JsonProperty("website_sale_onboarding_payment_acquirer_state")
    private String websiteSaleOnboardingPaymentAcquirerState;

    /**
     * 脸书账号
     */
    @DEField(name = "social_facebook")
    @JSONField(name = "social_facebook")
    @JsonProperty("social_facebook")
    private String socialFacebook;

    /**
     * 在线签名
     */
    @DEField(name = "portal_confirmation_sign")
    @JSONField(name = "portal_confirmation_sign")
    @JsonProperty("portal_confirmation_sign")
    private String portalConfirmationSign;

    /**
     * 纸张格式
     */
    @DEField(name = "paperformat_id")
    @JSONField(name = "paperformat_id")
    @JsonProperty("paperformat_id")
    private Integer paperformatId;

    /**
     * 会计年度的最后一个月
     */
    @DEField(name = "fiscalyear_last_month")
    @JSONField(name = "fiscalyear_last_month")
    @JsonProperty("fiscalyear_last_month")
    private String fiscalyearLastMonth;

    /**
     * 默认邮件
     */
    @DEField(name = "invoice_is_email")
    @JSONField(name = "invoice_is_email")
    @JsonProperty("invoice_is_email")
    private String invoiceIsEmail;

    /**
     * Youtube账号
     */
    @DEField(name = "social_youtube")
    @JSONField(name = "social_youtube")
    @JsonProperty("social_youtube")
    private String socialYoutube;

    /**
     * 汇率损失科目
     */
    @JSONField(name = "expense_currency_exchange_account_id")
    @JsonProperty("expense_currency_exchange_account_id")
    private Integer expenseCurrencyExchangeAccountId;

    /**
     * 公司数据库ID
     */
    @JSONField(name = "partner_gid")
    @JsonProperty("partner_gid")
    private Integer partnerGid;

    /**
     * 电话
     */
    @JSONField(name = "phone")
    @JsonProperty("phone")
    private String phone;

    /**
     * 公司 Logo
     */
    @JSONField(name = "logo")
    @JsonProperty("logo")
    private byte[] logo;

    /**
     * 库存计价的入库科目
     */
    @JSONField(name = "property_stock_account_input_categ_id_text")
    @JsonProperty("property_stock_account_input_categ_id_text")
    private String propertyStockAccountInputCategIdText;

    /**
     * 默认进项税
     */
    @JSONField(name = "account_purchase_tax_id_text")
    @JsonProperty("account_purchase_tax_id_text")
    private String accountPurchaseTaxIdText;

    /**
     * 公司名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 默认国际贸易术语
     */
    @JSONField(name = "incoterm_id_text")
    @JsonProperty("incoterm_id_text")
    private String incotermIdText;

    /**
     * 期初日记账
     */
    @JSONField(name = "account_opening_journal_id")
    @JsonProperty("account_opening_journal_id")
    private Integer accountOpeningJournalId;

    /**
     * 银行间转账科目
     */
    @JSONField(name = "transfer_account_id_text")
    @JsonProperty("transfer_account_id_text")
    private String transferAccountIdText;

    /**
     * 汇率增益科目
     */
    @JSONField(name = "income_currency_exchange_account_id")
    @JsonProperty("income_currency_exchange_account_id")
    private Integer incomeCurrencyExchangeAccountId;

    /**
     * 币种
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;

    /**
     * 期初日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "account_opening_date" , format="yyyy-MM-dd")
    @JsonProperty("account_opening_date")
    private Timestamp accountOpeningDate;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 表模板
     */
    @JSONField(name = "chart_template_id_text")
    @JsonProperty("chart_template_id_text")
    private String chartTemplateIdText;

    /**
     * 默认销售税
     */
    @JSONField(name = "account_sale_tax_id_text")
    @JsonProperty("account_sale_tax_id_text")
    private String accountSaleTaxIdText;

    /**
     * 期初日记账分录
     */
    @JSONField(name = "account_opening_move_id_text")
    @JsonProperty("account_opening_move_id_text")
    private String accountOpeningMoveIdText;

    /**
     * EMail
     */
    @JSONField(name = "email")
    @JsonProperty("email")
    private String email;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 库存计价的出货科目
     */
    @JSONField(name = "property_stock_account_output_categ_id_text")
    @JsonProperty("property_stock_account_output_categ_id_text")
    private String propertyStockAccountOutputCategIdText;

    /**
     * 库存计价的科目模板
     */
    @JSONField(name = "property_stock_valuation_account_id_text")
    @JsonProperty("property_stock_valuation_account_id_text")
    private String propertyStockValuationAccountIdText;

    /**
     * 上级公司
     */
    @JSONField(name = "parent_id_text")
    @JsonProperty("parent_id_text")
    private String parentIdText;

    /**
     * 现金收付制日记账
     */
    @JSONField(name = "tax_cash_basis_journal_id_text")
    @JsonProperty("tax_cash_basis_journal_id_text")
    private String taxCashBasisJournalIdText;

    /**
     * 内部中转位置
     */
    @JSONField(name = "internal_transit_location_id_text")
    @JsonProperty("internal_transit_location_id_text")
    private String internalTransitLocationIdText;

    /**
     * 网站
     */
    @JSONField(name = "website")
    @JsonProperty("website")
    private String website;

    /**
     * 税号
     */
    @JSONField(name = "vat")
    @JsonProperty("vat")
    private String vat;

    /**
     * 默认工作时间
     */
    @JSONField(name = "resource_calendar_id_text")
    @JsonProperty("resource_calendar_id_text")
    private String resourceCalendarIdText;

    /**
     * 汇兑损益
     */
    @JSONField(name = "currency_exchange_journal_id_text")
    @JsonProperty("currency_exchange_journal_id_text")
    private String currencyExchangeJournalIdText;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 上级公司
     */
    @DEField(name = "parent_id")
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    private Integer parentId;

    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 库存计价的出货科目
     */
    @DEField(name = "property_stock_account_output_categ_id")
    @JSONField(name = "property_stock_account_output_categ_id")
    @JsonProperty("property_stock_account_output_categ_id")
    private Integer propertyStockAccountOutputCategId;

    /**
     * 库存计价的科目模板
     */
    @DEField(name = "property_stock_valuation_account_id")
    @JSONField(name = "property_stock_valuation_account_id")
    @JsonProperty("property_stock_valuation_account_id")
    private Integer propertyStockValuationAccountId;

    /**
     * 期初日记账分录
     */
    @DEField(name = "account_opening_move_id")
    @JSONField(name = "account_opening_move_id")
    @JsonProperty("account_opening_move_id")
    private Integer accountOpeningMoveId;

    /**
     * 内部中转位置
     */
    @DEField(name = "internal_transit_location_id")
    @JSONField(name = "internal_transit_location_id")
    @JsonProperty("internal_transit_location_id")
    private Integer internalTransitLocationId;

    /**
     * 默认进项税
     */
    @DEField(name = "account_purchase_tax_id")
    @JSONField(name = "account_purchase_tax_id")
    @JsonProperty("account_purchase_tax_id")
    private Integer accountPurchaseTaxId;

    /**
     * 表模板
     */
    @DEField(name = "chart_template_id")
    @JSONField(name = "chart_template_id")
    @JsonProperty("chart_template_id")
    private Integer chartTemplateId;

    /**
     * 默认销售税
     */
    @DEField(name = "account_sale_tax_id")
    @JSONField(name = "account_sale_tax_id")
    @JsonProperty("account_sale_tax_id")
    private Integer accountSaleTaxId;

    /**
     * 现金收付制日记账
     */
    @DEField(name = "tax_cash_basis_journal_id")
    @JSONField(name = "tax_cash_basis_journal_id")
    @JsonProperty("tax_cash_basis_journal_id")
    private Integer taxCashBasisJournalId;

    /**
     * 库存计价的入库科目
     */
    @DEField(name = "property_stock_account_input_categ_id")
    @JSONField(name = "property_stock_account_input_categ_id")
    @JsonProperty("property_stock_account_input_categ_id")
    private Integer propertyStockAccountInputCategId;

    /**
     * 业务伙伴
     */
    @DEField(name = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 默认国际贸易术语
     */
    @DEField(name = "incoterm_id")
    @JSONField(name = "incoterm_id")
    @JsonProperty("incoterm_id")
    private Integer incotermId;

    /**
     * 默认工作时间
     */
    @DEField(name = "resource_calendar_id")
    @JSONField(name = "resource_calendar_id")
    @JsonProperty("resource_calendar_id")
    private Integer resourceCalendarId;

    /**
     * 银行间转账科目
     */
    @DEField(name = "transfer_account_id")
    @JSONField(name = "transfer_account_id")
    @JsonProperty("transfer_account_id")
    private Integer transferAccountId;

    /**
     * 汇兑损益
     */
    @DEField(name = "currency_exchange_journal_id")
    @JSONField(name = "currency_exchange_journal_id")
    @JsonProperty("currency_exchange_journal_id")
    private Integer currencyExchangeJournalId;


    /**
     * 
     */
    @JSONField(name = "odoopropertystockaccountinputcateg")
    @JsonProperty("odoopropertystockaccountinputcateg")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account odooPropertyStockAccountInputCateg;

    /**
     * 
     */
    @JSONField(name = "odoopropertystockaccountoutputcateg")
    @JsonProperty("odoopropertystockaccountoutputcateg")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account odooPropertyStockAccountOutputCateg;

    /**
     * 
     */
    @JSONField(name = "odoopropertystockvaluationaccount")
    @JsonProperty("odoopropertystockvaluationaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account odooPropertyStockValuationAccount;

    /**
     * 
     */
    @JSONField(name = "odootransferaccount")
    @JsonProperty("odootransferaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account odooTransferAccount;

    /**
     * 
     */
    @JSONField(name = "odoocharttemplate")
    @JsonProperty("odoocharttemplate")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_chart_template odooChartTemplate;

    /**
     * 
     */
    @JSONField(name = "odooincoterm")
    @JsonProperty("odooincoterm")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_incoterms odooIncoterm;

    /**
     * 
     */
    @JSONField(name = "odoocurrencyexchangejournal")
    @JsonProperty("odoocurrencyexchangejournal")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_journal odooCurrencyExchangeJournal;

    /**
     * 
     */
    @JSONField(name = "odootaxcashbasisjournal")
    @JsonProperty("odootaxcashbasisjournal")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_journal odooTaxCashBasisJournal;

    /**
     * 
     */
    @JSONField(name = "odooaccountopeningmove")
    @JsonProperty("odooaccountopeningmove")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_move odooAccountOpeningMove;

    /**
     * 
     */
    @JSONField(name = "odooaccountpurchasetax")
    @JsonProperty("odooaccountpurchasetax")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_tax odooAccountPurchaseTax;

    /**
     * 
     */
    @JSONField(name = "odooaccountsaletax")
    @JsonProperty("odooaccountsaletax")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_tax odooAccountSaleTax;

    /**
     * 
     */
    @JSONField(name = "odooresourcecalendar")
    @JsonProperty("odooresourcecalendar")
    private cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar odooResourceCalendar;

    /**
     * 
     */
    @JSONField(name = "odooparent")
    @JsonProperty("odooparent")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooParent;

    /**
     * 
     */
    @JSONField(name = "odoocurrency")
    @JsonProperty("odoocurrency")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JSONField(name = "odoopartner")
    @JsonProperty("odoopartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoointernaltransitlocation")
    @JsonProperty("odoointernaltransitlocation")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location odooInternalTransitLocation;


    /**
     * 销售订单行
     */
    @JSONField(name = "sale_order_lines")
    @JsonProperty("sale_order_lines")
    private List<cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_line> saleOrderLine;



    /**
     * 设置 [公司口号]
     */
    public void setReportHeader(String reportHeader){
        this.reportHeader = reportHeader ;
        this.modify("report_header",reportHeader);
    }
    /**
     * 设置 [正进行销售面板的状态]
     */
    public void setSaleQuotationOnboardingState(String saleQuotationOnboardingState){
        this.saleQuotationOnboardingState = saleQuotationOnboardingState ;
        this.modify("sale_quotation_onboarding_state",saleQuotationOnboardingState);
    }
    /**
     * 设置 [默认报价有效期（日）]
     */
    public void setQuotationValidityDays(Integer quotationValidityDays){
        this.quotationValidityDays = quotationValidityDays ;
        this.modify("quotation_validity_days",quotationValidityDays);
    }
    /**
     * 设置 [有待被确认的发票步骤的状态]
     */
    public void setAccountOnboardingInvoiceLayoutState(String accountOnboardingInvoiceLayoutState){
        this.accountOnboardingInvoiceLayoutState = accountOnboardingInvoiceLayoutState ;
        this.modify("account_onboarding_invoice_layout_state",accountOnboardingInvoiceLayoutState);
    }
    /**
     * 设置 [科目号码]
     */
    public void setAccountNo(String accountNo){
        this.accountNo = accountNo ;
        this.modify("account_no",accountNo);
    }
    /**
     * 设置 [银行科目的前缀]
     */
    public void setBankAccountCodePrefix(String bankAccountCodePrefix){
        this.bankAccountCodePrefix = bankAccountCodePrefix ;
        this.modify("bank_account_code_prefix",bankAccountCodePrefix);
    }
    /**
     * 设置 [非顾问的锁定日期]
     */
    public void setPeriodLockDate(Timestamp periodLockDate){
        this.periodLockDate = periodLockDate ;
        this.modify("period_lock_date",periodLockDate);
    }
    /**
     * 设置 [使用现金收付制]
     */
    public void setTaxExigibility(String taxExigibility){
        this.taxExigibility = taxExigibility ;
        this.modify("tax_exigibility",taxExigibility);
    }
    /**
     * 设置 [处于会计面板的状态]
     */
    public void setAccountDashboardOnboardingState(String accountDashboardOnboardingState){
        this.accountDashboardOnboardingState = accountDashboardOnboardingState ;
        this.modify("account_dashboard_onboarding_state",accountDashboardOnboardingState);
    }
    /**
     * 设置 [连接在一起的库存移动的日期变化传播的最小差值。]
     */
    public void setPropagationMinimumDelta(Integer propagationMinimumDelta){
        this.propagationMinimumDelta = propagationMinimumDelta ;
        this.modify("propagation_minimum_delta",propagationMinimumDelta);
    }
    /**
     * 设置 [颜色]
     */
    public void setSnailmailColor(String snailmailColor){
        this.snailmailColor = snailmailColor ;
        this.modify("snailmail_color",snailmailColor);
    }
    /**
     * 设置 [逾期追款消息]
     */
    public void setOverdueMsg(String overdueMsg){
        this.overdueMsg = overdueMsg ;
        this.modify("overdue_msg",overdueMsg);
    }
    /**
     * 设置 [科目状态]
     */
    public void setAccountSetupCoaState(String accountSetupCoaState){
        this.accountSetupCoaState = accountSetupCoaState ;
        this.modify("account_setup_coa_state",accountSetupCoaState);
    }
    /**
     * 设置 [默认信息类型]
     */
    public void setInvoiceReferenceType(String invoiceReferenceType){
        this.invoiceReferenceType = invoiceReferenceType ;
        this.modify("invoice_reference_type",invoiceReferenceType);
    }
    /**
     * 设置 [使用anglo-saxon会计]
     */
    public void setAngloSaxonAccounting(String angloSaxonAccounting){
        this.angloSaxonAccounting = angloSaxonAccounting ;
        this.modify("anglo_saxon_accounting",angloSaxonAccounting);
    }
    /**
     * 设置 [双面]
     */
    public void setSnailmailDuplex(String snailmailDuplex){
        this.snailmailDuplex = snailmailDuplex ;
        this.modify("snailmail_duplex",snailmailDuplex);
    }
    /**
     * 设置 [GitHub账户]
     */
    public void setSocialGithub(String socialGithub){
        this.socialGithub = socialGithub ;
        this.modify("social_github",socialGithub);
    }
    /**
     * 设置 [有待被确认的银行数据步骤的状态]
     */
    public void setAccountSetupBankDataState(String accountSetupBankDataState){
        this.accountSetupBankDataState = accountSetupBankDataState ;
        this.modify("account_setup_bank_data_state",accountSetupBankDataState);
    }
    /**
     * 设置 [预计会计科目表]
     */
    public void setExpectsChartOfAccounts(String expectsChartOfAccounts){
        this.expectsChartOfAccounts = expectsChartOfAccounts ;
        this.modify("expects_chart_of_accounts",expectsChartOfAccounts);
    }
    /**
     * 设置 [转账帐户的前缀]
     */
    public void setTransferAccountCodePrefix(String transferAccountCodePrefix){
        this.transferAccountCodePrefix = transferAccountCodePrefix ;
        this.modify("transfer_account_code_prefix",transferAccountCodePrefix);
    }
    /**
     * 设置 [会计年度的最后一天]
     */
    public void setFiscalyearLastDay(Integer fiscalyearLastDay){
        this.fiscalyearLastDay = fiscalyearLastDay ;
        this.modify("fiscalyear_last_day",fiscalyearLastDay);
    }
    /**
     * 设置 [银行核销阈值]
     */
    public void setAccountBankReconciliationStart(Timestamp accountBankReconciliationStart){
        this.accountBankReconciliationStart = accountBankReconciliationStart ;
        this.modify("account_bank_reconciliation_start",accountBankReconciliationStart);
    }
    /**
     * 设置 [在线支付]
     */
    public void setPortalConfirmationPay(String portalConfirmationPay){
        this.portalConfirmationPay = portalConfirmationPay ;
        this.modify("portal_confirmation_pay",portalConfirmationPay);
    }
    /**
     * 设置 [显示SEPA QR码]
     */
    public void setQrCode(String qrCode){
        this.qrCode = qrCode ;
        this.modify("qr_code",qrCode);
    }
    /**
     * 设置 [处于会计发票面板的状态]
     */
    public void setAccountInvoiceOnboardingState(String accountInvoiceOnboardingState){
        this.accountInvoiceOnboardingState = accountInvoiceOnboardingState ;
        this.modify("account_invoice_onboarding_state",accountInvoiceOnboardingState);
    }
    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }
    /**
     * 设置 [命名规则]
     */
    public void setNomenclatureId(Integer nomenclatureId){
        this.nomenclatureId = nomenclatureId ;
        this.modify("nomenclature_id",nomenclatureId);
    }
    /**
     * 设置 [Google+账户]
     */
    public void setSocialGoogleplus(String socialGoogleplus){
        this.socialGoogleplus = socialGoogleplus ;
        this.modify("social_googleplus",socialGoogleplus);
    }
    /**
     * 设置 [入职支付收单机构的状态]
     */
    public void setPaymentAcquirerOnboardingState(String paymentAcquirerOnboardingState){
        this.paymentAcquirerOnboardingState = paymentAcquirerOnboardingState ;
        this.modify("payment_acquirer_onboarding_state",paymentAcquirerOnboardingState);
    }
    /**
     * 设置 [报表页脚]
     */
    public void setReportFooter(String reportFooter){
        this.reportFooter = reportFooter ;
        this.modify("report_footer",reportFooter);
    }
    /**
     * 设置 [选择付款方式]
     */
    public void setPaymentOnboardingPaymentMethod(String paymentOnboardingPaymentMethod){
        this.paymentOnboardingPaymentMethod = paymentOnboardingPaymentMethod ;
        this.modify("payment_onboarding_payment_method",paymentOnboardingPaymentMethod);
    }
    /**
     * 设置 [批准等级]
     */
    public void setPoDoubleValidation(String poDoubleValidation){
        this.poDoubleValidation = poDoubleValidation ;
        this.modify("po_double_validation",poDoubleValidation);
    }
    /**
     * 设置 [采购提前时间]
     */
    public void setPoLead(Double poLead){
        this.poLead = poLead ;
        this.modify("po_lead",poLead);
    }
    /**
     * 设置 [有待被确认的样品报价单步骤的状态]
     */
    public void setSaleOnboardingSampleQuotationState(String saleOnboardingSampleQuotationState){
        this.saleOnboardingSampleQuotationState = saleOnboardingSampleQuotationState ;
        this.modify("sale_onboarding_sample_quotation_state",saleOnboardingSampleQuotationState);
    }
    /**
     * 设置 [有待被确认的订单步骤的状态]
     */
    public void setSaleOnboardingOrderConfirmationState(String saleOnboardingOrderConfirmationState){
        this.saleOnboardingOrderConfirmationState = saleOnboardingOrderConfirmationState ;
        this.modify("sale_onboarding_order_confirmation_state",saleOnboardingOrderConfirmationState);
    }
    /**
     * 设置 [文档模板]
     */
    public void setExternalReportLayoutId(Integer externalReportLayoutId){
        this.externalReportLayoutId = externalReportLayoutId ;
        this.modify("external_report_layout_id",externalReportLayoutId);
    }
    /**
     * 设置 [请选择付款方式]
     */
    public void setSaleOnboardingPaymentMethod(String saleOnboardingPaymentMethod){
        this.saleOnboardingPaymentMethod = saleOnboardingPaymentMethod ;
        this.modify("sale_onboarding_payment_method",saleOnboardingPaymentMethod);
    }
    /**
     * 设置 [有待被确认的样品报价单步骤的状态]
     */
    public void setAccountOnboardingSampleInvoiceState(String accountOnboardingSampleInvoiceState){
        this.accountOnboardingSampleInvoiceState = accountOnboardingSampleInvoiceState ;
        this.modify("account_onboarding_sample_invoice_state",accountOnboardingSampleInvoiceState);
    }
    /**
     * 设置 [公司状态]
     */
    public void setBaseOnboardingCompanyState(String baseOnboardingCompanyState){
        this.baseOnboardingCompanyState = baseOnboardingCompanyState ;
        this.modify("base_onboarding_company_state",baseOnboardingCompanyState);
    }
    /**
     * 设置 [领英账号]
     */
    public void setSocialLinkedin(String socialLinkedin){
        this.socialLinkedin = socialLinkedin ;
        this.modify("social_linkedin",socialLinkedin);
    }
    /**
     * 设置 [制造提前期(日)]
     */
    public void setManufacturingLead(Double manufacturingLead){
        this.manufacturingLead = manufacturingLead ;
        this.modify("manufacturing_lead",manufacturingLead);
    }
    /**
     * 设置 [默认条款和条件]
     */
    public void setSaleNote(String saleNote){
        this.saleNote = saleNote ;
        this.modify("sale_note",saleNote);
    }
    /**
     * 设置 [再次验证金额]
     */
    public void setPoDoubleValidationAmount(Double poDoubleValidationAmount){
        this.poDoubleValidationAmount = poDoubleValidationAmount ;
        this.modify("po_double_validation_amount",poDoubleValidationAmount);
    }
    /**
     * 设置 [销售订单修改]
     */
    public void setPoLock(String poLock){
        this.poLock = poLock ;
        this.modify("po_lock",poLock);
    }
    /**
     * 设置 [Twitter账号]
     */
    public void setSocialTwitter(String socialTwitter){
        this.socialTwitter = socialTwitter ;
        this.modify("social_twitter",socialTwitter);
    }
    /**
     * 设置 [Instagram 账号]
     */
    public void setSocialInstagram(String socialInstagram){
        this.socialInstagram = socialInstagram ;
        this.modify("social_instagram",socialInstagram);
    }
    /**
     * 设置 [有待被确认的会计年度步骤的状态]
     */
    public void setAccountSetupFyDataState(String accountSetupFyDataState){
        this.accountSetupFyDataState = accountSetupFyDataState ;
        this.modify("account_setup_fy_data_state",accountSetupFyDataState);
    }
    /**
     * 设置 [税率计算的舍入方法]
     */
    public void setTaxCalculationRoundingMethod(String taxCalculationRoundingMethod){
        this.taxCalculationRoundingMethod = taxCalculationRoundingMethod ;
        this.modify("tax_calculation_rounding_method",taxCalculationRoundingMethod);
    }
    /**
     * 设置 [现金科目的前缀]
     */
    public void setCashAccountCodePrefix(String cashAccountCodePrefix){
        this.cashAccountCodePrefix = cashAccountCodePrefix ;
        this.modify("cash_account_code_prefix",cashAccountCodePrefix);
    }
    /**
     * 设置 [有待被确认的报价单步骤的状态]
     */
    public void setAccountOnboardingSaleTaxState(String accountOnboardingSaleTaxState){
        this.accountOnboardingSaleTaxState = accountOnboardingSaleTaxState ;
        this.modify("account_onboarding_sale_tax_state",accountOnboardingSaleTaxState);
    }
    /**
     * 设置 [销售安全天数]
     */
    public void setSecurityLead(Double securityLead){
        this.securityLead = securityLead ;
        this.modify("security_lead",securityLead);
    }
    /**
     * 设置 [通过默认值打印]
     */
    public void setInvoiceIsPrint(String invoiceIsPrint){
        this.invoiceIsPrint = invoiceIsPrint ;
        this.modify("invoice_is_print",invoiceIsPrint);
    }
    /**
     * 设置 [公司注册]
     */
    public void setCompanyRegistry(String companyRegistry){
        this.companyRegistry = companyRegistry ;
        this.modify("company_registry",companyRegistry);
    }
    /**
     * 设置 [锁定日期]
     */
    public void setFiscalyearLockDate(Timestamp fiscalyearLockDate){
        this.fiscalyearLockDate = fiscalyearLockDate ;
        this.modify("fiscalyear_lock_date",fiscalyearLockDate);
    }
    /**
     * 设置 [默认以信件发送]
     */
    public void setInvoiceIsSnailmail(String invoiceIsSnailmail){
        this.invoiceIsSnailmail = invoiceIsSnailmail ;
        this.modify("invoice_is_snailmail",invoiceIsSnailmail);
    }
    /**
     * 设置 [网站销售状态入职付款收单机构步骤]
     */
    public void setWebsiteSaleOnboardingPaymentAcquirerState(String websiteSaleOnboardingPaymentAcquirerState){
        this.websiteSaleOnboardingPaymentAcquirerState = websiteSaleOnboardingPaymentAcquirerState ;
        this.modify("website_sale_onboarding_payment_acquirer_state",websiteSaleOnboardingPaymentAcquirerState);
    }
    /**
     * 设置 [脸书账号]
     */
    public void setSocialFacebook(String socialFacebook){
        this.socialFacebook = socialFacebook ;
        this.modify("social_facebook",socialFacebook);
    }
    /**
     * 设置 [在线签名]
     */
    public void setPortalConfirmationSign(String portalConfirmationSign){
        this.portalConfirmationSign = portalConfirmationSign ;
        this.modify("portal_confirmation_sign",portalConfirmationSign);
    }
    /**
     * 设置 [纸张格式]
     */
    public void setPaperformatId(Integer paperformatId){
        this.paperformatId = paperformatId ;
        this.modify("paperformat_id",paperformatId);
    }
    /**
     * 设置 [会计年度的最后一个月]
     */
    public void setFiscalyearLastMonth(String fiscalyearLastMonth){
        this.fiscalyearLastMonth = fiscalyearLastMonth ;
        this.modify("fiscalyear_last_month",fiscalyearLastMonth);
    }
    /**
     * 设置 [默认邮件]
     */
    public void setInvoiceIsEmail(String invoiceIsEmail){
        this.invoiceIsEmail = invoiceIsEmail ;
        this.modify("invoice_is_email",invoiceIsEmail);
    }
    /**
     * 设置 [Youtube账号]
     */
    public void setSocialYoutube(String socialYoutube){
        this.socialYoutube = socialYoutube ;
        this.modify("social_youtube",socialYoutube);
    }
    /**
     * 设置 [上级公司]
     */
    public void setParentId(Integer parentId){
        this.parentId = parentId ;
        this.modify("parent_id",parentId);
    }
    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Integer currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }
    /**
     * 设置 [库存计价的出货科目]
     */
    public void setPropertyStockAccountOutputCategId(Integer propertyStockAccountOutputCategId){
        this.propertyStockAccountOutputCategId = propertyStockAccountOutputCategId ;
        this.modify("property_stock_account_output_categ_id",propertyStockAccountOutputCategId);
    }
    /**
     * 设置 [库存计价的科目模板]
     */
    public void setPropertyStockValuationAccountId(Integer propertyStockValuationAccountId){
        this.propertyStockValuationAccountId = propertyStockValuationAccountId ;
        this.modify("property_stock_valuation_account_id",propertyStockValuationAccountId);
    }
    /**
     * 设置 [期初日记账分录]
     */
    public void setAccountOpeningMoveId(Integer accountOpeningMoveId){
        this.accountOpeningMoveId = accountOpeningMoveId ;
        this.modify("account_opening_move_id",accountOpeningMoveId);
    }
    /**
     * 设置 [内部中转位置]
     */
    public void setInternalTransitLocationId(Integer internalTransitLocationId){
        this.internalTransitLocationId = internalTransitLocationId ;
        this.modify("internal_transit_location_id",internalTransitLocationId);
    }
    /**
     * 设置 [默认进项税]
     */
    public void setAccountPurchaseTaxId(Integer accountPurchaseTaxId){
        this.accountPurchaseTaxId = accountPurchaseTaxId ;
        this.modify("account_purchase_tax_id",accountPurchaseTaxId);
    }
    /**
     * 设置 [表模板]
     */
    public void setChartTemplateId(Integer chartTemplateId){
        this.chartTemplateId = chartTemplateId ;
        this.modify("chart_template_id",chartTemplateId);
    }
    /**
     * 设置 [默认销售税]
     */
    public void setAccountSaleTaxId(Integer accountSaleTaxId){
        this.accountSaleTaxId = accountSaleTaxId ;
        this.modify("account_sale_tax_id",accountSaleTaxId);
    }
    /**
     * 设置 [现金收付制日记账]
     */
    public void setTaxCashBasisJournalId(Integer taxCashBasisJournalId){
        this.taxCashBasisJournalId = taxCashBasisJournalId ;
        this.modify("tax_cash_basis_journal_id",taxCashBasisJournalId);
    }
    /**
     * 设置 [库存计价的入库科目]
     */
    public void setPropertyStockAccountInputCategId(Integer propertyStockAccountInputCategId){
        this.propertyStockAccountInputCategId = propertyStockAccountInputCategId ;
        this.modify("property_stock_account_input_categ_id",propertyStockAccountInputCategId);
    }
    /**
     * 设置 [业务伙伴]
     */
    public void setPartnerId(Integer partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }
    /**
     * 设置 [默认国际贸易术语]
     */
    public void setIncotermId(Integer incotermId){
        this.incotermId = incotermId ;
        this.modify("incoterm_id",incotermId);
    }
    /**
     * 设置 [默认工作时间]
     */
    public void setResourceCalendarId(Integer resourceCalendarId){
        this.resourceCalendarId = resourceCalendarId ;
        this.modify("resource_calendar_id",resourceCalendarId);
    }
    /**
     * 设置 [银行间转账科目]
     */
    public void setTransferAccountId(Integer transferAccountId){
        this.transferAccountId = transferAccountId ;
        this.modify("transfer_account_id",transferAccountId);
    }
    /**
     * 设置 [汇兑损益]
     */
    public void setCurrencyExchangeJournalId(Integer currencyExchangeJournalId){
        this.currencyExchangeJournalId = currencyExchangeJournalId ;
        this.modify("currency_exchange_journal_id",currencyExchangeJournalId);
    }

}


