package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warn_insufficient_qtySearchContext;


/**
 * 实体[Stock_warn_insufficient_qty] 服务对象接口
 */
public interface IStock_warn_insufficient_qtyService{

    Stock_warn_insufficient_qty getDraft(Stock_warn_insufficient_qty et) ;
    boolean update(Stock_warn_insufficient_qty et) ;
    void updateBatch(List<Stock_warn_insufficient_qty> list) ;
    Stock_warn_insufficient_qty get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Stock_warn_insufficient_qty et) ;
    void createBatch(List<Stock_warn_insufficient_qty> list) ;
    Page<Stock_warn_insufficient_qty> searchDefault(Stock_warn_insufficient_qtySearchContext context) ;

}



