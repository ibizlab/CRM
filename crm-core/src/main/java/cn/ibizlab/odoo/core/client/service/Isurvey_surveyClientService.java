package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Isurvey_survey;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[survey_survey] 服务对象接口
 */
public interface Isurvey_surveyClientService{

    public Isurvey_survey createModel() ;

    public void create(Isurvey_survey survey_survey);

    public void remove(Isurvey_survey survey_survey);

    public void createBatch(List<Isurvey_survey> survey_surveys);

    public void update(Isurvey_survey survey_survey);

    public void updateBatch(List<Isurvey_survey> survey_surveys);

    public void removeBatch(List<Isurvey_survey> survey_surveys);

    public void get(Isurvey_survey survey_survey);

    public Page<Isurvey_survey> fetchDefault(SearchContext context);

    public Page<Isurvey_survey> select(SearchContext context);

    public void getDraft(Isurvey_survey survey_survey);

}
