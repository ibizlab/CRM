package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_track_line;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_track_lineSearchContext;


/**
 * 实体[Stock_track_line] 服务对象接口
 */
public interface IStock_track_lineService{

    Stock_track_line get(Integer key) ;
    boolean create(Stock_track_line et) ;
    void createBatch(List<Stock_track_line> list) ;
    Stock_track_line getDraft(Stock_track_line et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Stock_track_line et) ;
    void updateBatch(List<Stock_track_line> list) ;
    Page<Stock_track_line> searchDefault(Stock_track_lineSearchContext context) ;

}



