package cn.ibizlab.odoo.core.odoo_gamification.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_badge;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_badgeSearchContext;


/**
 * 实体[Gamification_badge] 服务对象接口
 */
public interface IGamification_badgeService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Gamification_badge et) ;
    void updateBatch(List<Gamification_badge> list) ;
    boolean create(Gamification_badge et) ;
    void createBatch(List<Gamification_badge> list) ;
    Gamification_badge get(Integer key) ;
    Gamification_badge getDraft(Gamification_badge et) ;
    Page<Gamification_badge> searchDefault(Gamification_badgeSearchContext context) ;

}



