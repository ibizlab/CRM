package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ibase_language_install;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_language_install] 服务对象接口
 */
public interface Ibase_language_installClientService{

    public Ibase_language_install createModel() ;

    public void updateBatch(List<Ibase_language_install> base_language_installs);

    public void update(Ibase_language_install base_language_install);

    public Page<Ibase_language_install> fetchDefault(SearchContext context);

    public void removeBatch(List<Ibase_language_install> base_language_installs);

    public void createBatch(List<Ibase_language_install> base_language_installs);

    public void create(Ibase_language_install base_language_install);

    public void remove(Ibase_language_install base_language_install);

    public void get(Ibase_language_install base_language_install);

    public Page<Ibase_language_install> select(SearchContext context);

    public void getDraft(Ibase_language_install base_language_install);

}
