package cn.ibizlab.odoo.core.odoo_repair.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_order_make_invoice;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_order_make_invoiceSearchContext;
import cn.ibizlab.odoo.core.odoo_repair.service.IRepair_order_make_invoiceService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_repair.client.repair_order_make_invoiceFeignClient;

/**
 * 实体[创建批量发票 (修理)] 服务对象接口实现
 */
@Slf4j
@Service
public class Repair_order_make_invoiceServiceImpl implements IRepair_order_make_invoiceService {

    @Autowired
    repair_order_make_invoiceFeignClient repair_order_make_invoiceFeignClient;


    @Override
    public boolean update(Repair_order_make_invoice et) {
        Repair_order_make_invoice rt = repair_order_make_invoiceFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Repair_order_make_invoice> list){
        repair_order_make_invoiceFeignClient.updateBatch(list) ;
    }

    @Override
    public Repair_order_make_invoice get(Integer id) {
		Repair_order_make_invoice et=repair_order_make_invoiceFeignClient.get(id);
        if(et==null){
            et=new Repair_order_make_invoice();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Repair_order_make_invoice getDraft(Repair_order_make_invoice et) {
        et=repair_order_make_invoiceFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=repair_order_make_invoiceFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        repair_order_make_invoiceFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Repair_order_make_invoice et) {
        Repair_order_make_invoice rt = repair_order_make_invoiceFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Repair_order_make_invoice> list){
        repair_order_make_invoiceFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Repair_order_make_invoice> searchDefault(Repair_order_make_invoiceSearchContext context) {
        Page<Repair_order_make_invoice> repair_order_make_invoices=repair_order_make_invoiceFeignClient.searchDefault(context);
        return repair_order_make_invoices;
    }


}


