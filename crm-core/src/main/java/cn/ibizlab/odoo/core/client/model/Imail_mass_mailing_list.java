package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [mail_mass_mailing_list] 对象
 */
public interface Imail_mass_mailing_list {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [邮件列表]
     */
    public void setContact_ids(String contact_ids);
    
    /**
     * 设置 [邮件列表]
     */
    public String getContact_ids();

    /**
     * 获取 [邮件列表]脏标记
     */
    public boolean getContact_idsDirtyFlag();
    /**
     * 获取 [联系人人数]
     */
    public void setContact_nbr(Integer contact_nbr);
    
    /**
     * 设置 [联系人人数]
     */
    public Integer getContact_nbr();

    /**
     * 获取 [联系人人数]脏标记
     */
    public boolean getContact_nbrDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [公开]
     */
    public void setIs_public(String is_public);
    
    /**
     * 设置 [公开]
     */
    public String getIs_public();

    /**
     * 获取 [公开]脏标记
     */
    public boolean getIs_publicDirtyFlag();
    /**
     * 获取 [邮件列表]
     */
    public void setName(String name);
    
    /**
     * 设置 [邮件列表]
     */
    public String getName();

    /**
     * 获取 [邮件列表]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [网站弹出内容]
     */
    public void setPopup_content(String popup_content);
    
    /**
     * 设置 [网站弹出内容]
     */
    public String getPopup_content();

    /**
     * 获取 [网站弹出内容]脏标记
     */
    public boolean getPopup_contentDirtyFlag();
    /**
     * 获取 [网站弹出重新定向网址]
     */
    public void setPopup_redirect_url(String popup_redirect_url);
    
    /**
     * 设置 [网站弹出重新定向网址]
     */
    public String getPopup_redirect_url();

    /**
     * 获取 [网站弹出重新定向网址]脏标记
     */
    public boolean getPopup_redirect_urlDirtyFlag();
    /**
     * 获取 [订阅信息]
     */
    public void setSubscription_contact_ids(String subscription_contact_ids);
    
    /**
     * 设置 [订阅信息]
     */
    public String getSubscription_contact_ids();

    /**
     * 获取 [订阅信息]脏标记
     */
    public boolean getSubscription_contact_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
