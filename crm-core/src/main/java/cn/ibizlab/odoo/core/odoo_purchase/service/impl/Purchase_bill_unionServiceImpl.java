package cn.ibizlab.odoo.core.odoo_purchase.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_bill_union;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_bill_unionSearchContext;
import cn.ibizlab.odoo.core.odoo_purchase.service.IPurchase_bill_unionService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_purchase.client.purchase_bill_unionFeignClient;

/**
 * 实体[采购 & 账单] 服务对象接口实现
 */
@Slf4j
@Service
public class Purchase_bill_unionServiceImpl implements IPurchase_bill_unionService {

    @Autowired
    purchase_bill_unionFeignClient purchase_bill_unionFeignClient;


    @Override
    public Purchase_bill_union getDraft(Purchase_bill_union et) {
        et=purchase_bill_unionFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Purchase_bill_union et) {
        Purchase_bill_union rt = purchase_bill_unionFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Purchase_bill_union> list){
        purchase_bill_unionFeignClient.createBatch(list) ;
    }

    @Override
    public Purchase_bill_union get(Integer id) {
		Purchase_bill_union et=purchase_bill_unionFeignClient.get(id);
        if(et==null){
            et=new Purchase_bill_union();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Purchase_bill_union et) {
        Purchase_bill_union rt = purchase_bill_unionFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Purchase_bill_union> list){
        purchase_bill_unionFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=purchase_bill_unionFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        purchase_bill_unionFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Purchase_bill_union> searchDefault(Purchase_bill_unionSearchContext context) {
        Page<Purchase_bill_union> purchase_bill_unions=purchase_bill_unionFeignClient.searchDefault(context);
        return purchase_bill_unions;
    }


}


