package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quant_package;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_quant_packageSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_quant_package] 服务对象接口
 */
@Component
public class stock_quant_packageFallback implements stock_quant_packageFeignClient{



    public Stock_quant_package update(Integer id, Stock_quant_package stock_quant_package){
            return null;
     }
    public Boolean updateBatch(List<Stock_quant_package> stock_quant_packages){
            return false;
     }


    public Stock_quant_package get(Integer id){
            return null;
     }


    public Page<Stock_quant_package> searchDefault(Stock_quant_packageSearchContext context){
            return null;
     }


    public Stock_quant_package create(Stock_quant_package stock_quant_package){
            return null;
     }
    public Boolean createBatch(List<Stock_quant_package> stock_quant_packages){
            return false;
     }

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Page<Stock_quant_package> select(){
            return null;
     }

    public Stock_quant_package getDraft(){
            return null;
    }



}
