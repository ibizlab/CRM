package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_closebalance;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_closebalanceSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_bank_statement_closebalance] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-bank-statement-closebalance", fallback = account_bank_statement_closebalanceFallback.class)
public interface account_bank_statement_closebalanceFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_closebalances/searchdefault")
    Page<Account_bank_statement_closebalance> searchDefault(@RequestBody Account_bank_statement_closebalanceSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statement_closebalances/{id}")
    Account_bank_statement_closebalance update(@PathVariable("id") Integer id,@RequestBody Account_bank_statement_closebalance account_bank_statement_closebalance);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statement_closebalances/batch")
    Boolean updateBatch(@RequestBody List<Account_bank_statement_closebalance> account_bank_statement_closebalances);


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statement_closebalances/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statement_closebalances/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_closebalances/{id}")
    Account_bank_statement_closebalance get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_closebalances")
    Account_bank_statement_closebalance create(@RequestBody Account_bank_statement_closebalance account_bank_statement_closebalance);

    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_closebalances/batch")
    Boolean createBatch(@RequestBody List<Account_bank_statement_closebalance> account_bank_statement_closebalances);





    @RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_closebalances/select")
    Page<Account_bank_statement_closebalance> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_closebalances/getdraft")
    Account_bank_statement_closebalance getDraft();


}
