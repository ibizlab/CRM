package cn.ibizlab.odoo.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workorder;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workorderSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mrp_workorder] 服务对象接口
 */
@FeignClient(value = "odoo-mrp", contextId = "mrp-workorder", fallback = mrp_workorderFallback.class)
public interface mrp_workorderFeignClient {




    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workorders/searchdefault")
    Page<Mrp_workorder> searchDefault(@RequestBody Mrp_workorderSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workorders")
    Mrp_workorder create(@RequestBody Mrp_workorder mrp_workorder);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workorders/batch")
    Boolean createBatch(@RequestBody List<Mrp_workorder> mrp_workorders);



    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workorders/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workorders/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_workorders/{id}")
    Mrp_workorder update(@PathVariable("id") Integer id,@RequestBody Mrp_workorder mrp_workorder);

    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_workorders/batch")
    Boolean updateBatch(@RequestBody List<Mrp_workorder> mrp_workorders);


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_workorders/{id}")
    Mrp_workorder get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_workorders/select")
    Page<Mrp_workorder> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_workorders/getdraft")
    Mrp_workorder getDraft();


}
