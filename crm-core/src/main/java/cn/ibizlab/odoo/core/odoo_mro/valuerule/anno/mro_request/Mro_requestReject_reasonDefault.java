package cn.ibizlab.odoo.core.odoo_mro.valuerule.anno.mro_request;

import cn.ibizlab.odoo.core.odoo_mro.valuerule.validator.mro_request.Mro_requestReject_reasonDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Mro_request
 * 属性：Reject_reason
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Mro_requestReject_reasonDefaultValidator.class})
public @interface Mro_requestReject_reasonDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[1048576]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
