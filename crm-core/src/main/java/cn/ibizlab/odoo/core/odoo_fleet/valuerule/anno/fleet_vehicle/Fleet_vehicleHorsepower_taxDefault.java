package cn.ibizlab.odoo.core.odoo_fleet.valuerule.anno.fleet_vehicle;

import cn.ibizlab.odoo.core.odoo_fleet.valuerule.validator.fleet_vehicle.Fleet_vehicleHorsepower_taxDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Fleet_vehicle
 * 属性：Horsepower_tax
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Fleet_vehicleHorsepower_taxDefaultValidator.class})
public @interface Fleet_vehicleHorsepower_taxDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
