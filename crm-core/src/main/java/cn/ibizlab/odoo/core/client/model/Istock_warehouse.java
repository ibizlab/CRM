package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [stock_warehouse] 对象
 */
public interface Istock_warehouse {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [购买规则]
     */
    public void setBuy_pull_id(Integer buy_pull_id);
    
    /**
     * 设置 [购买规则]
     */
    public Integer getBuy_pull_id();

    /**
     * 获取 [购买规则]脏标记
     */
    public boolean getBuy_pull_idDirtyFlag();
    /**
     * 获取 [购买规则]
     */
    public void setBuy_pull_id_text(String buy_pull_id_text);
    
    /**
     * 设置 [购买规则]
     */
    public String getBuy_pull_id_text();

    /**
     * 获取 [购买规则]脏标记
     */
    public boolean getBuy_pull_id_textDirtyFlag();
    /**
     * 获取 [购买补给]
     */
    public void setBuy_to_resupply(String buy_to_resupply);
    
    /**
     * 设置 [购买补给]
     */
    public String getBuy_to_resupply();

    /**
     * 获取 [购买补给]脏标记
     */
    public boolean getBuy_to_resupplyDirtyFlag();
    /**
     * 获取 [缩写]
     */
    public void setCode(String code);
    
    /**
     * 设置 [缩写]
     */
    public String getCode();

    /**
     * 获取 [缩写]脏标记
     */
    public boolean getCodeDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [越库路线]
     */
    public void setCrossdock_route_id(Integer crossdock_route_id);
    
    /**
     * 设置 [越库路线]
     */
    public Integer getCrossdock_route_id();

    /**
     * 获取 [越库路线]脏标记
     */
    public boolean getCrossdock_route_idDirtyFlag();
    /**
     * 获取 [越库路线]
     */
    public void setCrossdock_route_id_text(String crossdock_route_id_text);
    
    /**
     * 设置 [越库路线]
     */
    public String getCrossdock_route_id_text();

    /**
     * 获取 [越库路线]脏标记
     */
    public boolean getCrossdock_route_id_textDirtyFlag();
    /**
     * 获取 [交货路线]
     */
    public void setDelivery_route_id(Integer delivery_route_id);
    
    /**
     * 设置 [交货路线]
     */
    public Integer getDelivery_route_id();

    /**
     * 获取 [交货路线]脏标记
     */
    public boolean getDelivery_route_idDirtyFlag();
    /**
     * 获取 [交货路线]
     */
    public void setDelivery_route_id_text(String delivery_route_id_text);
    
    /**
     * 设置 [交货路线]
     */
    public String getDelivery_route_id_text();

    /**
     * 获取 [交货路线]脏标记
     */
    public boolean getDelivery_route_id_textDirtyFlag();
    /**
     * 获取 [出向运输]
     */
    public void setDelivery_steps(String delivery_steps);
    
    /**
     * 设置 [出向运输]
     */
    public String getDelivery_steps();

    /**
     * 获取 [出向运输]脏标记
     */
    public boolean getDelivery_stepsDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [内部类型]
     */
    public void setInt_type_id(Integer int_type_id);
    
    /**
     * 设置 [内部类型]
     */
    public Integer getInt_type_id();

    /**
     * 获取 [内部类型]脏标记
     */
    public boolean getInt_type_idDirtyFlag();
    /**
     * 获取 [内部类型]
     */
    public void setInt_type_id_text(String int_type_id_text);
    
    /**
     * 设置 [内部类型]
     */
    public String getInt_type_id_text();

    /**
     * 获取 [内部类型]脏标记
     */
    public boolean getInt_type_id_textDirtyFlag();
    /**
     * 获取 [入库类型]
     */
    public void setIn_type_id(Integer in_type_id);
    
    /**
     * 设置 [入库类型]
     */
    public Integer getIn_type_id();

    /**
     * 获取 [入库类型]脏标记
     */
    public boolean getIn_type_idDirtyFlag();
    /**
     * 获取 [入库类型]
     */
    public void setIn_type_id_text(String in_type_id_text);
    
    /**
     * 设置 [入库类型]
     */
    public String getIn_type_id_text();

    /**
     * 获取 [入库类型]脏标记
     */
    public boolean getIn_type_id_textDirtyFlag();
    /**
     * 获取 [库存位置]
     */
    public void setLot_stock_id(Integer lot_stock_id);
    
    /**
     * 设置 [库存位置]
     */
    public Integer getLot_stock_id();

    /**
     * 获取 [库存位置]脏标记
     */
    public boolean getLot_stock_idDirtyFlag();
    /**
     * 获取 [库存位置]
     */
    public void setLot_stock_id_text(String lot_stock_id_text);
    
    /**
     * 设置 [库存位置]
     */
    public String getLot_stock_id_text();

    /**
     * 获取 [库存位置]脏标记
     */
    public boolean getLot_stock_id_textDirtyFlag();
    /**
     * 获取 [制造规则]
     */
    public void setManufacture_pull_id(Integer manufacture_pull_id);
    
    /**
     * 设置 [制造规则]
     */
    public Integer getManufacture_pull_id();

    /**
     * 获取 [制造规则]脏标记
     */
    public boolean getManufacture_pull_idDirtyFlag();
    /**
     * 获取 [制造规则]
     */
    public void setManufacture_pull_id_text(String manufacture_pull_id_text);
    
    /**
     * 设置 [制造规则]
     */
    public String getManufacture_pull_id_text();

    /**
     * 获取 [制造规则]脏标记
     */
    public boolean getManufacture_pull_id_textDirtyFlag();
    /**
     * 获取 [制造]
     */
    public void setManufacture_steps(String manufacture_steps);
    
    /**
     * 设置 [制造]
     */
    public String getManufacture_steps();

    /**
     * 获取 [制造]脏标记
     */
    public boolean getManufacture_stepsDirtyFlag();
    /**
     * 获取 [制造补给]
     */
    public void setManufacture_to_resupply(String manufacture_to_resupply);
    
    /**
     * 设置 [制造补给]
     */
    public String getManufacture_to_resupply();

    /**
     * 获取 [制造补给]脏标记
     */
    public boolean getManufacture_to_resupplyDirtyFlag();
    /**
     * 获取 [生产操作类型]
     */
    public void setManu_type_id(Integer manu_type_id);
    
    /**
     * 设置 [生产操作类型]
     */
    public Integer getManu_type_id();

    /**
     * 获取 [生产操作类型]脏标记
     */
    public boolean getManu_type_idDirtyFlag();
    /**
     * 获取 [生产操作类型]
     */
    public void setManu_type_id_text(String manu_type_id_text);
    
    /**
     * 设置 [生产操作类型]
     */
    public String getManu_type_id_text();

    /**
     * 获取 [生产操作类型]脏标记
     */
    public boolean getManu_type_id_textDirtyFlag();
    /**
     * 获取 [MTO规则]
     */
    public void setMto_pull_id(Integer mto_pull_id);
    
    /**
     * 设置 [MTO规则]
     */
    public Integer getMto_pull_id();

    /**
     * 获取 [MTO规则]脏标记
     */
    public boolean getMto_pull_idDirtyFlag();
    /**
     * 获取 [MTO规则]
     */
    public void setMto_pull_id_text(String mto_pull_id_text);
    
    /**
     * 设置 [MTO规则]
     */
    public String getMto_pull_id_text();

    /**
     * 获取 [MTO规则]脏标记
     */
    public boolean getMto_pull_id_textDirtyFlag();
    /**
     * 获取 [仓库]
     */
    public void setName(String name);
    
    /**
     * 设置 [仓库]
     */
    public String getName();

    /**
     * 获取 [仓库]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [出库类型]
     */
    public void setOut_type_id(Integer out_type_id);
    
    /**
     * 设置 [出库类型]
     */
    public Integer getOut_type_id();

    /**
     * 获取 [出库类型]脏标记
     */
    public boolean getOut_type_idDirtyFlag();
    /**
     * 获取 [出库类型]
     */
    public void setOut_type_id_text(String out_type_id_text);
    
    /**
     * 设置 [出库类型]
     */
    public String getOut_type_id_text();

    /**
     * 获取 [出库类型]脏标记
     */
    public boolean getOut_type_id_textDirtyFlag();
    /**
     * 获取 [包裹类型]
     */
    public void setPack_type_id(Integer pack_type_id);
    
    /**
     * 设置 [包裹类型]
     */
    public Integer getPack_type_id();

    /**
     * 获取 [包裹类型]脏标记
     */
    public boolean getPack_type_idDirtyFlag();
    /**
     * 获取 [包裹类型]
     */
    public void setPack_type_id_text(String pack_type_id_text);
    
    /**
     * 设置 [包裹类型]
     */
    public String getPack_type_id_text();

    /**
     * 获取 [包裹类型]脏标记
     */
    public boolean getPack_type_id_textDirtyFlag();
    /**
     * 获取 [地址]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [地址]
     */
    public Integer getPartner_id();

    /**
     * 获取 [地址]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [地址]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [地址]
     */
    public String getPartner_id_text();

    /**
     * 获取 [地址]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [在制造位置前拣货]
     */
    public void setPbm_loc_id(Integer pbm_loc_id);
    
    /**
     * 设置 [在制造位置前拣货]
     */
    public Integer getPbm_loc_id();

    /**
     * 获取 [在制造位置前拣货]脏标记
     */
    public boolean getPbm_loc_idDirtyFlag();
    /**
     * 获取 [在制造位置前拣货]
     */
    public void setPbm_loc_id_text(String pbm_loc_id_text);
    
    /**
     * 设置 [在制造位置前拣货]
     */
    public String getPbm_loc_id_text();

    /**
     * 获取 [在制造位置前拣货]脏标记
     */
    public boolean getPbm_loc_id_textDirtyFlag();
    /**
     * 获取 [在制造（按订单补货）MTO规则之前拣货]
     */
    public void setPbm_mto_pull_id(Integer pbm_mto_pull_id);
    
    /**
     * 设置 [在制造（按订单补货）MTO规则之前拣货]
     */
    public Integer getPbm_mto_pull_id();

    /**
     * 获取 [在制造（按订单补货）MTO规则之前拣货]脏标记
     */
    public boolean getPbm_mto_pull_idDirtyFlag();
    /**
     * 获取 [在制造（按订单补货）MTO规则之前拣货]
     */
    public void setPbm_mto_pull_id_text(String pbm_mto_pull_id_text);
    
    /**
     * 设置 [在制造（按订单补货）MTO规则之前拣货]
     */
    public String getPbm_mto_pull_id_text();

    /**
     * 获取 [在制造（按订单补货）MTO规则之前拣货]脏标记
     */
    public boolean getPbm_mto_pull_id_textDirtyFlag();
    /**
     * 获取 [在制造路线前拣货]
     */
    public void setPbm_route_id(Integer pbm_route_id);
    
    /**
     * 设置 [在制造路线前拣货]
     */
    public Integer getPbm_route_id();

    /**
     * 获取 [在制造路线前拣货]脏标记
     */
    public boolean getPbm_route_idDirtyFlag();
    /**
     * 获取 [在制造路线前拣货]
     */
    public void setPbm_route_id_text(String pbm_route_id_text);
    
    /**
     * 设置 [在制造路线前拣货]
     */
    public String getPbm_route_id_text();

    /**
     * 获取 [在制造路线前拣货]脏标记
     */
    public boolean getPbm_route_id_textDirtyFlag();
    /**
     * 获取 [在制造作业类型前拣货]
     */
    public void setPbm_type_id(Integer pbm_type_id);
    
    /**
     * 设置 [在制造作业类型前拣货]
     */
    public Integer getPbm_type_id();

    /**
     * 获取 [在制造作业类型前拣货]脏标记
     */
    public boolean getPbm_type_idDirtyFlag();
    /**
     * 获取 [在制造作业类型前拣货]
     */
    public void setPbm_type_id_text(String pbm_type_id_text);
    
    /**
     * 设置 [在制造作业类型前拣货]
     */
    public String getPbm_type_id_text();

    /**
     * 获取 [在制造作业类型前拣货]脏标记
     */
    public boolean getPbm_type_id_textDirtyFlag();
    /**
     * 获取 [分拣类型]
     */
    public void setPick_type_id(Integer pick_type_id);
    
    /**
     * 设置 [分拣类型]
     */
    public Integer getPick_type_id();

    /**
     * 获取 [分拣类型]脏标记
     */
    public boolean getPick_type_idDirtyFlag();
    /**
     * 获取 [分拣类型]
     */
    public void setPick_type_id_text(String pick_type_id_text);
    
    /**
     * 设置 [分拣类型]
     */
    public String getPick_type_id_text();

    /**
     * 获取 [分拣类型]脏标记
     */
    public boolean getPick_type_id_textDirtyFlag();
    /**
     * 获取 [收货路线]
     */
    public void setReception_route_id(Integer reception_route_id);
    
    /**
     * 设置 [收货路线]
     */
    public Integer getReception_route_id();

    /**
     * 获取 [收货路线]脏标记
     */
    public boolean getReception_route_idDirtyFlag();
    /**
     * 获取 [收货路线]
     */
    public void setReception_route_id_text(String reception_route_id_text);
    
    /**
     * 设置 [收货路线]
     */
    public String getReception_route_id_text();

    /**
     * 获取 [收货路线]脏标记
     */
    public boolean getReception_route_id_textDirtyFlag();
    /**
     * 获取 [入库]
     */
    public void setReception_steps(String reception_steps);
    
    /**
     * 设置 [入库]
     */
    public String getReception_steps();

    /**
     * 获取 [入库]脏标记
     */
    public boolean getReception_stepsDirtyFlag();
    /**
     * 获取 [补充路线]
     */
    public void setResupply_route_ids(String resupply_route_ids);
    
    /**
     * 设置 [补充路线]
     */
    public String getResupply_route_ids();

    /**
     * 获取 [补充路线]脏标记
     */
    public boolean getResupply_route_idsDirtyFlag();
    /**
     * 获取 [补给 自]
     */
    public void setResupply_wh_ids(String resupply_wh_ids);
    
    /**
     * 设置 [补给 自]
     */
    public String getResupply_wh_ids();

    /**
     * 获取 [补给 自]脏标记
     */
    public boolean getResupply_wh_idsDirtyFlag();
    /**
     * 获取 [路线]
     */
    public void setRoute_ids(String route_ids);
    
    /**
     * 设置 [路线]
     */
    public String getRoute_ids();

    /**
     * 获取 [路线]脏标记
     */
    public boolean getRoute_idsDirtyFlag();
    /**
     * 获取 [制造地点后的库存]
     */
    public void setSam_loc_id(Integer sam_loc_id);
    
    /**
     * 设置 [制造地点后的库存]
     */
    public Integer getSam_loc_id();

    /**
     * 获取 [制造地点后的库存]脏标记
     */
    public boolean getSam_loc_idDirtyFlag();
    /**
     * 获取 [制造地点后的库存]
     */
    public void setSam_loc_id_text(String sam_loc_id_text);
    
    /**
     * 设置 [制造地点后的库存]
     */
    public String getSam_loc_id_text();

    /**
     * 获取 [制造地点后的库存]脏标记
     */
    public boolean getSam_loc_id_textDirtyFlag();
    /**
     * 获取 [制造规则后的库存]
     */
    public void setSam_rule_id(Integer sam_rule_id);
    
    /**
     * 设置 [制造规则后的库存]
     */
    public Integer getSam_rule_id();

    /**
     * 获取 [制造规则后的库存]脏标记
     */
    public boolean getSam_rule_idDirtyFlag();
    /**
     * 获取 [制造规则后的库存]
     */
    public void setSam_rule_id_text(String sam_rule_id_text);
    
    /**
     * 设置 [制造规则后的库存]
     */
    public String getSam_rule_id_text();

    /**
     * 获取 [制造规则后的库存]脏标记
     */
    public boolean getSam_rule_id_textDirtyFlag();
    /**
     * 获取 [制造运营类型后的库存]
     */
    public void setSam_type_id(Integer sam_type_id);
    
    /**
     * 设置 [制造运营类型后的库存]
     */
    public Integer getSam_type_id();

    /**
     * 获取 [制造运营类型后的库存]脏标记
     */
    public boolean getSam_type_idDirtyFlag();
    /**
     * 获取 [制造运营类型后的库存]
     */
    public void setSam_type_id_text(String sam_type_id_text);
    
    /**
     * 设置 [制造运营类型后的库存]
     */
    public String getSam_type_id_text();

    /**
     * 获取 [制造运营类型后的库存]脏标记
     */
    public boolean getSam_type_id_textDirtyFlag();
    /**
     * 获取 [显示补给]
     */
    public void setShow_resupply(String show_resupply);
    
    /**
     * 设置 [显示补给]
     */
    public String getShow_resupply();

    /**
     * 获取 [显示补给]脏标记
     */
    public boolean getShow_resupplyDirtyFlag();
    /**
     * 获取 [视图位置]
     */
    public void setView_location_id(Integer view_location_id);
    
    /**
     * 设置 [视图位置]
     */
    public Integer getView_location_id();

    /**
     * 获取 [视图位置]脏标记
     */
    public boolean getView_location_idDirtyFlag();
    /**
     * 获取 [视图位置]
     */
    public void setView_location_id_text(String view_location_id_text);
    
    /**
     * 设置 [视图位置]
     */
    public String getView_location_id_text();

    /**
     * 获取 [视图位置]脏标记
     */
    public boolean getView_location_id_textDirtyFlag();
    /**
     * 获取 [仓库个数]
     */
    public void setWarehouse_count(Integer warehouse_count);
    
    /**
     * 设置 [仓库个数]
     */
    public Integer getWarehouse_count();

    /**
     * 获取 [仓库个数]脏标记
     */
    public boolean getWarehouse_countDirtyFlag();
    /**
     * 获取 [进货位置]
     */
    public void setWh_input_stock_loc_id(Integer wh_input_stock_loc_id);
    
    /**
     * 设置 [进货位置]
     */
    public Integer getWh_input_stock_loc_id();

    /**
     * 获取 [进货位置]脏标记
     */
    public boolean getWh_input_stock_loc_idDirtyFlag();
    /**
     * 获取 [进货位置]
     */
    public void setWh_input_stock_loc_id_text(String wh_input_stock_loc_id_text);
    
    /**
     * 设置 [进货位置]
     */
    public String getWh_input_stock_loc_id_text();

    /**
     * 获取 [进货位置]脏标记
     */
    public boolean getWh_input_stock_loc_id_textDirtyFlag();
    /**
     * 获取 [出货位置]
     */
    public void setWh_output_stock_loc_id(Integer wh_output_stock_loc_id);
    
    /**
     * 设置 [出货位置]
     */
    public Integer getWh_output_stock_loc_id();

    /**
     * 获取 [出货位置]脏标记
     */
    public boolean getWh_output_stock_loc_idDirtyFlag();
    /**
     * 获取 [出货位置]
     */
    public void setWh_output_stock_loc_id_text(String wh_output_stock_loc_id_text);
    
    /**
     * 设置 [出货位置]
     */
    public String getWh_output_stock_loc_id_text();

    /**
     * 获取 [出货位置]脏标记
     */
    public boolean getWh_output_stock_loc_id_textDirtyFlag();
    /**
     * 获取 [打包位置]
     */
    public void setWh_pack_stock_loc_id(Integer wh_pack_stock_loc_id);
    
    /**
     * 设置 [打包位置]
     */
    public Integer getWh_pack_stock_loc_id();

    /**
     * 获取 [打包位置]脏标记
     */
    public boolean getWh_pack_stock_loc_idDirtyFlag();
    /**
     * 获取 [打包位置]
     */
    public void setWh_pack_stock_loc_id_text(String wh_pack_stock_loc_id_text);
    
    /**
     * 设置 [打包位置]
     */
    public String getWh_pack_stock_loc_id_text();

    /**
     * 获取 [打包位置]脏标记
     */
    public boolean getWh_pack_stock_loc_id_textDirtyFlag();
    /**
     * 获取 [质量管理位置]
     */
    public void setWh_qc_stock_loc_id(Integer wh_qc_stock_loc_id);
    
    /**
     * 设置 [质量管理位置]
     */
    public Integer getWh_qc_stock_loc_id();

    /**
     * 获取 [质量管理位置]脏标记
     */
    public boolean getWh_qc_stock_loc_idDirtyFlag();
    /**
     * 获取 [质量管理位置]
     */
    public void setWh_qc_stock_loc_id_text(String wh_qc_stock_loc_id_text);
    
    /**
     * 设置 [质量管理位置]
     */
    public String getWh_qc_stock_loc_id_text();

    /**
     * 获取 [质量管理位置]脏标记
     */
    public boolean getWh_qc_stock_loc_id_textDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
