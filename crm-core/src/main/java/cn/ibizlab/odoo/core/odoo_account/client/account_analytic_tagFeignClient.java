package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_tag;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_tagSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_analytic_tag] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-analytic-tag", fallback = account_analytic_tagFallback.class)
public interface account_analytic_tagFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/account_analytic_tags/searchdefault")
    Page<Account_analytic_tag> searchDefault(@RequestBody Account_analytic_tagSearchContext context);



    @RequestMapping(method = RequestMethod.DELETE, value = "/account_analytic_tags/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_analytic_tags/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_analytic_tags/{id}")
    Account_analytic_tag update(@PathVariable("id") Integer id,@RequestBody Account_analytic_tag account_analytic_tag);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_analytic_tags/batch")
    Boolean updateBatch(@RequestBody List<Account_analytic_tag> account_analytic_tags);


    @RequestMapping(method = RequestMethod.GET, value = "/account_analytic_tags/{id}")
    Account_analytic_tag get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/account_analytic_tags")
    Account_analytic_tag create(@RequestBody Account_analytic_tag account_analytic_tag);

    @RequestMapping(method = RequestMethod.POST, value = "/account_analytic_tags/batch")
    Boolean createBatch(@RequestBody List<Account_analytic_tag> account_analytic_tags);



    @RequestMapping(method = RequestMethod.GET, value = "/account_analytic_tags/select")
    Page<Account_analytic_tag> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_analytic_tags/getdraft")
    Account_analytic_tag getDraft();


}
