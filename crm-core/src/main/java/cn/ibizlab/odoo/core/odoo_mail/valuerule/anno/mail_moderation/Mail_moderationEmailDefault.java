package cn.ibizlab.odoo.core.odoo_mail.valuerule.anno.mail_moderation;

import cn.ibizlab.odoo.core.odoo_mail.valuerule.validator.mail_moderation.Mail_moderationEmailDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Mail_moderation
 * 属性：Email
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Mail_moderationEmailDefaultValidator.class})
public @interface Mail_moderationEmailDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[100]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
