package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_statistics_report;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_statistics_reportSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_statistics_report] 服务对象接口
 */
@FeignClient(value = "odoo-mail", contextId = "mail-statistics-report", fallback = mail_statistics_reportFallback.class)
public interface mail_statistics_reportFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/mail_statistics_reports/{id}")
    Mail_statistics_report get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_statistics_reports/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_statistics_reports/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);




    @RequestMapping(method = RequestMethod.POST, value = "/mail_statistics_reports")
    Mail_statistics_report create(@RequestBody Mail_statistics_report mail_statistics_report);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_statistics_reports/batch")
    Boolean createBatch(@RequestBody List<Mail_statistics_report> mail_statistics_reports);



    @RequestMapping(method = RequestMethod.PUT, value = "/mail_statistics_reports/{id}")
    Mail_statistics_report update(@PathVariable("id") Integer id,@RequestBody Mail_statistics_report mail_statistics_report);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_statistics_reports/batch")
    Boolean updateBatch(@RequestBody List<Mail_statistics_report> mail_statistics_reports);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_statistics_reports/searchdefault")
    Page<Mail_statistics_report> searchDefault(@RequestBody Mail_statistics_reportSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_statistics_reports/select")
    Page<Mail_statistics_report> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_statistics_reports/getdraft")
    Mail_statistics_report getDraft();


}
