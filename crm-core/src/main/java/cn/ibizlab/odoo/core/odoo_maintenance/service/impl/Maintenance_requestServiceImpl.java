package cn.ibizlab.odoo.core.odoo_maintenance.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_request;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_requestSearchContext;
import cn.ibizlab.odoo.core.odoo_maintenance.service.IMaintenance_requestService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_maintenance.client.maintenance_requestFeignClient;

/**
 * 实体[保养请求] 服务对象接口实现
 */
@Slf4j
@Service
public class Maintenance_requestServiceImpl implements IMaintenance_requestService {

    @Autowired
    maintenance_requestFeignClient maintenance_requestFeignClient;


    @Override
    public Maintenance_request getDraft(Maintenance_request et) {
        et=maintenance_requestFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=maintenance_requestFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        maintenance_requestFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Maintenance_request et) {
        Maintenance_request rt = maintenance_requestFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Maintenance_request> list){
        maintenance_requestFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Maintenance_request et) {
        Maintenance_request rt = maintenance_requestFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Maintenance_request> list){
        maintenance_requestFeignClient.updateBatch(list) ;
    }

    @Override
    public Maintenance_request get(Integer id) {
		Maintenance_request et=maintenance_requestFeignClient.get(id);
        if(et==null){
            et=new Maintenance_request();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Maintenance_request> searchDefault(Maintenance_requestSearchContext context) {
        Page<Maintenance_request> maintenance_requests=maintenance_requestFeignClient.searchDefault(context);
        return maintenance_requests;
    }


}


