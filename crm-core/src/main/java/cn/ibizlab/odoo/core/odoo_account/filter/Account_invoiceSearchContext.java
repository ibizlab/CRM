package cn.ibizlab.odoo.core.odoo_account.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.odoo.util.filter.SearchContextBase;

/**
 * ServiceApi数据实体[Account_invoice] 查询条件对象
 */
@Slf4j
@Data
public class Account_invoiceSearchContext extends SearchContextBase {
	private String n_activity_state_eq;//[活动状态]

	private String n_state_eq;//[状态]

	private String n_name_like;//[参考/说明]

	private String n_type_eq;//[类型]

	private String n_journal_id_text_eq;//[日记账]

	private String n_journal_id_text_like;//[日记账]

	private String n_purchase_id_text_eq;//[添加采购订单]

	private String n_purchase_id_text_like;//[添加采购订单]

	private String n_vendor_bill_purchase_id_text_eq;//[自动完成]

	private String n_vendor_bill_purchase_id_text_like;//[自动完成]

	private String n_source_id_text_eq;//[来源]

	private String n_source_id_text_like;//[来源]

	private String n_campaign_id_text_eq;//[营销]

	private String n_campaign_id_text_like;//[营销]

	private String n_partner_shipping_id_text_eq;//[送货地址]

	private String n_partner_shipping_id_text_like;//[送货地址]

	private String n_currency_id_text_eq;//[币种]

	private String n_currency_id_text_like;//[币种]

	private String n_write_uid_text_eq;//[最后更新人]

	private String n_write_uid_text_like;//[最后更新人]

	private String n_incoterms_id_text_eq;//[贸易条款]

	private String n_incoterms_id_text_like;//[贸易条款]

	private String n_account_id_text_eq;//[科目]

	private String n_account_id_text_like;//[科目]

	private String n_incoterm_id_text_eq;//[国际贸易术语]

	private String n_incoterm_id_text_like;//[国际贸易术语]

	private String n_partner_id_text_eq;//[业务伙伴]

	private String n_partner_id_text_like;//[业务伙伴]

	private String n_medium_id_text_eq;//[媒体]

	private String n_medium_id_text_like;//[媒体]

	private String n_create_uid_text_eq;//[创建人]

	private String n_create_uid_text_like;//[创建人]

	private String n_company_id_text_eq;//[公司]

	private String n_company_id_text_like;//[公司]

	private String n_user_id_text_eq;//[销售员]

	private String n_user_id_text_like;//[销售员]

	private String n_cash_rounding_id_text_eq;//[现金舍入方式]

	private String n_cash_rounding_id_text_like;//[现金舍入方式]

	private String n_vendor_bill_id_text_eq;//[供应商账单]

	private String n_vendor_bill_id_text_like;//[供应商账单]

	private String n_refund_invoice_id_text_eq;//[此发票为信用票的发票]

	private String n_refund_invoice_id_text_like;//[此发票为信用票的发票]

	private String n_fiscal_position_id_text_eq;//[税科目调整]

	private String n_fiscal_position_id_text_like;//[税科目调整]

	private String n_number_eq;//[号码]

	private String n_number_like;//[号码]

	private String n_commercial_partner_id_text_eq;//[商业实体]

	private String n_commercial_partner_id_text_like;//[商业实体]

	private String n_team_id_text_eq;//[销售团队]

	private String n_team_id_text_like;//[销售团队]

	private String n_payment_term_id_text_eq;//[付款条款]

	private String n_payment_term_id_text_like;//[付款条款]

	private Integer n_user_id_eq;//[销售员]

	private Integer n_team_id_eq;//[销售团队]

	private Integer n_create_uid_eq;//[创建人]

	private Integer n_account_id_eq;//[科目]

	private Integer n_medium_id_eq;//[媒体]

	private Integer n_company_id_eq;//[公司]

	private Integer n_move_id_eq;//[日记账分录]

	private Integer n_payment_term_id_eq;//[付款条款]

	private Integer n_journal_id_eq;//[日记账]

	private Integer n_incoterm_id_eq;//[国际贸易术语]

	private Integer n_purchase_id_eq;//[添加采购订单]

	private Integer n_fiscal_position_id_eq;//[税科目调整]

	private Integer n_refund_invoice_id_eq;//[此发票为信用票的发票]

	private Integer n_currency_id_eq;//[币种]

	private Integer n_partner_id_eq;//[业务伙伴]

	private Integer n_partner_bank_id_eq;//[银行账户]

	private Integer n_cash_rounding_id_eq;//[现金舍入方式]

	private Integer n_commercial_partner_id_eq;//[商业实体]

	private Integer n_partner_shipping_id_eq;//[送货地址]

	private Integer n_incoterms_id_eq;//[贸易条款]

	private Integer n_source_id_eq;//[来源]

	private Integer n_vendor_bill_id_eq;//[供应商账单]

	private Integer n_write_uid_eq;//[最后更新人]

	private Integer n_vendor_bill_purchase_id_eq;//[自动完成]

	private Integer n_campaign_id_eq;//[营销]

}



