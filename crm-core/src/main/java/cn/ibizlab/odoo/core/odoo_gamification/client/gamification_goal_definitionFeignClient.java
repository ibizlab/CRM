package cn.ibizlab.odoo.core.odoo_gamification.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal_definition;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_goal_definitionSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[gamification_goal_definition] 服务对象接口
 */
@FeignClient(value = "odoo-gamification", contextId = "gamification-goal-definition", fallback = gamification_goal_definitionFallback.class)
public interface gamification_goal_definitionFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/gamification_goal_definitions/searchdefault")
    Page<Gamification_goal_definition> searchDefault(@RequestBody Gamification_goal_definitionSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/gamification_goal_definitions/{id}")
    Gamification_goal_definition update(@PathVariable("id") Integer id,@RequestBody Gamification_goal_definition gamification_goal_definition);

    @RequestMapping(method = RequestMethod.PUT, value = "/gamification_goal_definitions/batch")
    Boolean updateBatch(@RequestBody List<Gamification_goal_definition> gamification_goal_definitions);


    @RequestMapping(method = RequestMethod.GET, value = "/gamification_goal_definitions/{id}")
    Gamification_goal_definition get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.POST, value = "/gamification_goal_definitions")
    Gamification_goal_definition create(@RequestBody Gamification_goal_definition gamification_goal_definition);

    @RequestMapping(method = RequestMethod.POST, value = "/gamification_goal_definitions/batch")
    Boolean createBatch(@RequestBody List<Gamification_goal_definition> gamification_goal_definitions);


    @RequestMapping(method = RequestMethod.DELETE, value = "/gamification_goal_definitions/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/gamification_goal_definitions/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/gamification_goal_definitions/select")
    Page<Gamification_goal_definition> select();


    @RequestMapping(method = RequestMethod.GET, value = "/gamification_goal_definitions/getdraft")
    Gamification_goal_definition getDraft();


}
