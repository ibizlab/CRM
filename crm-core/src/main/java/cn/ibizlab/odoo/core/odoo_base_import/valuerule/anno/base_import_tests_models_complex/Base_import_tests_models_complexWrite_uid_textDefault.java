package cn.ibizlab.odoo.core.odoo_base_import.valuerule.anno.base_import_tests_models_complex;

import cn.ibizlab.odoo.core.odoo_base_import.valuerule.validator.base_import_tests_models_complex.Base_import_tests_models_complexWrite_uid_textDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Base_import_tests_models_complex
 * 属性：Write_uid_text
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Base_import_tests_models_complexWrite_uid_textDefaultValidator.class})
public @interface Base_import_tests_models_complexWrite_uid_textDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
