package cn.ibizlab.odoo.core.odoo_rating.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_rating.domain.Rating_rating;
import cn.ibizlab.odoo.core.odoo_rating.filter.Rating_ratingSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[rating_rating] 服务对象接口
 */
@FeignClient(value = "odoo-rating", contextId = "rating-rating", fallback = rating_ratingFallback.class)
public interface rating_ratingFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/rating_ratings/{id}")
    Rating_rating get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/rating_ratings/{id}")
    Rating_rating update(@PathVariable("id") Integer id,@RequestBody Rating_rating rating_rating);

    @RequestMapping(method = RequestMethod.PUT, value = "/rating_ratings/batch")
    Boolean updateBatch(@RequestBody List<Rating_rating> rating_ratings);


    @RequestMapping(method = RequestMethod.POST, value = "/rating_ratings")
    Rating_rating create(@RequestBody Rating_rating rating_rating);

    @RequestMapping(method = RequestMethod.POST, value = "/rating_ratings/batch")
    Boolean createBatch(@RequestBody List<Rating_rating> rating_ratings);


    @RequestMapping(method = RequestMethod.DELETE, value = "/rating_ratings/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/rating_ratings/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);





    @RequestMapping(method = RequestMethod.POST, value = "/rating_ratings/searchdefault")
    Page<Rating_rating> searchDefault(@RequestBody Rating_ratingSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/rating_ratings/select")
    Page<Rating_rating> select();


    @RequestMapping(method = RequestMethod.GET, value = "/rating_ratings/getdraft")
    Rating_rating getDraft();


}
