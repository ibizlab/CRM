package cn.ibizlab.odoo.core.odoo_fleet.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_state;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_stateSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[fleet_vehicle_state] 服务对象接口
 */
@FeignClient(value = "odoo-fleet", contextId = "fleet-vehicle-state", fallback = fleet_vehicle_stateFallback.class)
public interface fleet_vehicle_stateFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_states")
    Fleet_vehicle_state create(@RequestBody Fleet_vehicle_state fleet_vehicle_state);

    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_states/batch")
    Boolean createBatch(@RequestBody List<Fleet_vehicle_state> fleet_vehicle_states);




    @RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_states/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_states/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_states/searchdefault")
    Page<Fleet_vehicle_state> searchDefault(@RequestBody Fleet_vehicle_stateSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_states/{id}")
    Fleet_vehicle_state update(@PathVariable("id") Integer id,@RequestBody Fleet_vehicle_state fleet_vehicle_state);

    @RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_states/batch")
    Boolean updateBatch(@RequestBody List<Fleet_vehicle_state> fleet_vehicle_states);


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_states/{id}")
    Fleet_vehicle_state get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_states/select")
    Page<Fleet_vehicle_state> select();


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_states/getdraft")
    Fleet_vehicle_state getDraft();


}
