package cn.ibizlab.odoo.core.odoo_resource.valuerule.anno.resource_resource;

import cn.ibizlab.odoo.core.odoo_resource.valuerule.validator.resource_resource.Resource_resourceActiveDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Resource_resource
 * 属性：Active
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Resource_resourceActiveDefaultValidator.class})
public @interface Resource_resourceActiveDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
