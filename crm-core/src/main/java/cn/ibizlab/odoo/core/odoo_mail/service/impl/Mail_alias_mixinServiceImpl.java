package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_alias_mixin;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_alias_mixinSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_alias_mixinService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mail.client.mail_alias_mixinFeignClient;

/**
 * 实体[EMail别名 Mixin] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_alias_mixinServiceImpl implements IMail_alias_mixinService {

    @Autowired
    mail_alias_mixinFeignClient mail_alias_mixinFeignClient;


    @Override
    public boolean create(Mail_alias_mixin et) {
        Mail_alias_mixin rt = mail_alias_mixinFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_alias_mixin> list){
        mail_alias_mixinFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mail_alias_mixinFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mail_alias_mixinFeignClient.removeBatch(idList);
    }

    @Override
    public Mail_alias_mixin getDraft(Mail_alias_mixin et) {
        et=mail_alias_mixinFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Mail_alias_mixin et) {
        Mail_alias_mixin rt = mail_alias_mixinFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mail_alias_mixin> list){
        mail_alias_mixinFeignClient.updateBatch(list) ;
    }

    @Override
    public Mail_alias_mixin get(Integer id) {
		Mail_alias_mixin et=mail_alias_mixinFeignClient.get(id);
        if(et==null){
            et=new Mail_alias_mixin();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_alias_mixin> searchDefault(Mail_alias_mixinSearchContext context) {
        Page<Mail_alias_mixin> mail_alias_mixins=mail_alias_mixinFeignClient.searchDefault(context);
        return mail_alias_mixins;
    }


}


