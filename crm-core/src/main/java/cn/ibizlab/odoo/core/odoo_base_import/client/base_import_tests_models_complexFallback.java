package cn.ibizlab.odoo.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_complex;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_complexSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[base_import_tests_models_complex] 服务对象接口
 */
@Component
public class base_import_tests_models_complexFallback implements base_import_tests_models_complexFeignClient{

    public Base_import_tests_models_complex update(Integer id, Base_import_tests_models_complex base_import_tests_models_complex){
            return null;
     }
    public Boolean updateBatch(List<Base_import_tests_models_complex> base_import_tests_models_complices){
            return false;
     }


    public Page<Base_import_tests_models_complex> searchDefault(Base_import_tests_models_complexSearchContext context){
            return null;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Base_import_tests_models_complex get(Integer id){
            return null;
     }


    public Base_import_tests_models_complex create(Base_import_tests_models_complex base_import_tests_models_complex){
            return null;
     }
    public Boolean createBatch(List<Base_import_tests_models_complex> base_import_tests_models_complices){
            return false;
     }



    public Page<Base_import_tests_models_complex> select(){
            return null;
     }

    public Base_import_tests_models_complex getDraft(){
            return null;
    }



}
