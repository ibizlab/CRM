package cn.ibizlab.odoo.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_template;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_templateSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[product_template] 服务对象接口
 */
@Component
public class product_templateFallback implements product_templateFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Product_template> searchDefault(Product_templateSearchContext context){
            return null;
     }


    public Product_template create(Product_template product_template){
            return null;
     }
    public Boolean createBatch(List<Product_template> product_templates){
            return false;
     }

    public Product_template update(Integer id, Product_template product_template){
            return null;
     }
    public Boolean updateBatch(List<Product_template> product_templates){
            return false;
     }




    public Product_template get(Integer id){
            return null;
     }



    public Page<Product_template> select(){
            return null;
     }

    public Product_template getDraft(){
            return null;
    }



}
