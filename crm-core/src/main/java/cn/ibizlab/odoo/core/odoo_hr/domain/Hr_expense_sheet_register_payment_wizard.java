package cn.ibizlab.odoo.core.odoo_hr.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [费用登记付款向导] 对象
 */
@Data
public class Hr_expense_sheet_register_payment_wizard extends EntityClient implements Serializable {

    /**
     * 付款金额
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 备忘
     */
    @JSONField(name = "communication")
    @JsonProperty("communication")
    private String communication;

    /**
     * 隐藏付款方式
     */
    @JSONField(name = "hide_payment_method")
    @JsonProperty("hide_payment_method")
    private String hidePaymentMethod;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 显示合作伙伴银行账户
     */
    @JSONField(name = "show_partner_bank_account")
    @JsonProperty("show_partner_bank_account")
    private String showPartnerBankAccount;

    /**
     * 付款日期
     */
    @DEField(name = "payment_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "payment_date" , format="yyyy-MM-dd")
    @JsonProperty("payment_date")
    private Timestamp paymentDate;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 付款方法
     */
    @JSONField(name = "journal_id_text")
    @JsonProperty("journal_id_text")
    private String journalIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 业务伙伴
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 付款类型
     */
    @JSONField(name = "payment_method_id_text")
    @JsonProperty("payment_method_id_text")
    private String paymentMethodIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 币种
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;

    /**
     * 公司
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 业务伙伴
     */
    @DEField(name = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 付款类型
     */
    @DEField(name = "payment_method_id")
    @JSONField(name = "payment_method_id")
    @JsonProperty("payment_method_id")
    private Integer paymentMethodId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 收款银行账号
     */
    @DEField(name = "partner_bank_account_id")
    @JSONField(name = "partner_bank_account_id")
    @JsonProperty("partner_bank_account_id")
    private Integer partnerBankAccountId;

    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 付款方法
     */
    @DEField(name = "journal_id")
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    private Integer journalId;


    /**
     * 
     */
    @JSONField(name = "odoojournal")
    @JsonProperty("odoojournal")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_journal odooJournal;

    /**
     * 
     */
    @JSONField(name = "odoopaymentmethod")
    @JsonProperty("odoopaymentmethod")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_method odooPaymentMethod;

    /**
     * 
     */
    @JSONField(name = "odoocurrency")
    @JsonProperty("odoocurrency")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JSONField(name = "odoopartnerbankaccount")
    @JsonProperty("odoopartnerbankaccount")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_bank odooPartnerBankAccount;

    /**
     * 
     */
    @JSONField(name = "odoopartner")
    @JsonProperty("odoopartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [付款金额]
     */
    public void setAmount(Double amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }
    /**
     * 设置 [备忘]
     */
    public void setCommunication(String communication){
        this.communication = communication ;
        this.modify("communication",communication);
    }
    /**
     * 设置 [付款日期]
     */
    public void setPaymentDate(Timestamp paymentDate){
        this.paymentDate = paymentDate ;
        this.modify("payment_date",paymentDate);
    }
    /**
     * 设置 [业务伙伴]
     */
    public void setPartnerId(Integer partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }
    /**
     * 设置 [付款类型]
     */
    public void setPaymentMethodId(Integer paymentMethodId){
        this.paymentMethodId = paymentMethodId ;
        this.modify("payment_method_id",paymentMethodId);
    }
    /**
     * 设置 [收款银行账号]
     */
    public void setPartnerBankAccountId(Integer partnerBankAccountId){
        this.partnerBankAccountId = partnerBankAccountId ;
        this.modify("partner_bank_account_id",partnerBankAccountId);
    }
    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Integer currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }
    /**
     * 设置 [付款方法]
     */
    public void setJournalId(Integer journalId){
        this.journalId = journalId ;
        this.modify("journal_id",journalId);
    }

}


