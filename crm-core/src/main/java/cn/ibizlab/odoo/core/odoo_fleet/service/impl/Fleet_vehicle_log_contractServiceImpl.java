package cn.ibizlab.odoo.core.odoo_fleet.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_contract;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_log_contractSearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_log_contractService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_fleet.client.fleet_vehicle_log_contractFeignClient;

/**
 * 实体[车辆合同信息] 服务对象接口实现
 */
@Slf4j
@Service
public class Fleet_vehicle_log_contractServiceImpl implements IFleet_vehicle_log_contractService {

    @Autowired
    fleet_vehicle_log_contractFeignClient fleet_vehicle_log_contractFeignClient;


    @Override
    public Fleet_vehicle_log_contract getDraft(Fleet_vehicle_log_contract et) {
        et=fleet_vehicle_log_contractFeignClient.getDraft();
        return et;
    }

    @Override
    public Fleet_vehicle_log_contract get(Integer id) {
		Fleet_vehicle_log_contract et=fleet_vehicle_log_contractFeignClient.get(id);
        if(et==null){
            et=new Fleet_vehicle_log_contract();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Fleet_vehicle_log_contract et) {
        Fleet_vehicle_log_contract rt = fleet_vehicle_log_contractFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Fleet_vehicle_log_contract> list){
        fleet_vehicle_log_contractFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=fleet_vehicle_log_contractFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        fleet_vehicle_log_contractFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Fleet_vehicle_log_contract et) {
        Fleet_vehicle_log_contract rt = fleet_vehicle_log_contractFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Fleet_vehicle_log_contract> list){
        fleet_vehicle_log_contractFeignClient.createBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Fleet_vehicle_log_contract> searchDefault(Fleet_vehicle_log_contractSearchContext context) {
        Page<Fleet_vehicle_log_contract> fleet_vehicle_log_contracts=fleet_vehicle_log_contractFeignClient.searchDefault(context);
        return fleet_vehicle_log_contracts;
    }


}


