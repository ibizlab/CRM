package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_incoterms;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_incoterms] 服务对象接口
 */
public interface Iaccount_incotermsClientService{

    public Iaccount_incoterms createModel() ;

    public void removeBatch(List<Iaccount_incoterms> account_incoterms);

    public Page<Iaccount_incoterms> fetchDefault(SearchContext context);

    public void update(Iaccount_incoterms account_incoterms);

    public void create(Iaccount_incoterms account_incoterms);

    public void remove(Iaccount_incoterms account_incoterms);

    public void get(Iaccount_incoterms account_incoterms);

    public void createBatch(List<Iaccount_incoterms> account_incoterms);

    public void updateBatch(List<Iaccount_incoterms> account_incoterms);

    public Page<Iaccount_incoterms> select(SearchContext context);

    public void getDraft(Iaccount_incoterms account_incoterms);

}
