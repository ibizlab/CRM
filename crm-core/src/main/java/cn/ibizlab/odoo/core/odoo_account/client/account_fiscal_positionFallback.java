package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_positionSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_fiscal_position] 服务对象接口
 */
@Component
public class account_fiscal_positionFallback implements account_fiscal_positionFeignClient{

    public Account_fiscal_position update(Integer id, Account_fiscal_position account_fiscal_position){
            return null;
     }
    public Boolean updateBatch(List<Account_fiscal_position> account_fiscal_positions){
            return false;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Account_fiscal_position get(Integer id){
            return null;
     }


    public Page<Account_fiscal_position> searchDefault(Account_fiscal_positionSearchContext context){
            return null;
     }



    public Account_fiscal_position create(Account_fiscal_position account_fiscal_position){
            return null;
     }
    public Boolean createBatch(List<Account_fiscal_position> account_fiscal_positions){
            return false;
     }


    public Page<Account_fiscal_position> select(){
            return null;
     }

    public Account_fiscal_position getDraft(){
            return null;
    }



}
