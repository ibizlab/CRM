package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imro_pm_rule;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mro_pm_rule] 服务对象接口
 */
public interface Imro_pm_ruleClientService{

    public Imro_pm_rule createModel() ;

    public void create(Imro_pm_rule mro_pm_rule);

    public void get(Imro_pm_rule mro_pm_rule);

    public void removeBatch(List<Imro_pm_rule> mro_pm_rules);

    public void createBatch(List<Imro_pm_rule> mro_pm_rules);

    public void remove(Imro_pm_rule mro_pm_rule);

    public void update(Imro_pm_rule mro_pm_rule);

    public Page<Imro_pm_rule> fetchDefault(SearchContext context);

    public void updateBatch(List<Imro_pm_rule> mro_pm_rules);

    public Page<Imro_pm_rule> select(SearchContext context);

    public void getDraft(Imro_pm_rule mro_pm_rule);

}
