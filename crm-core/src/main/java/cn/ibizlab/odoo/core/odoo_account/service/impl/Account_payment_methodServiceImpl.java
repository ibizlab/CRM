package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_method;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_payment_methodSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_payment_methodService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_payment_methodFeignClient;

/**
 * 实体[付款方法] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_payment_methodServiceImpl implements IAccount_payment_methodService {

    @Autowired
    account_payment_methodFeignClient account_payment_methodFeignClient;


    @Override
    public Account_payment_method getDraft(Account_payment_method et) {
        et=account_payment_methodFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_payment_methodFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_payment_methodFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Account_payment_method et) {
        Account_payment_method rt = account_payment_methodFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_payment_method> list){
        account_payment_methodFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Account_payment_method et) {
        Account_payment_method rt = account_payment_methodFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_payment_method> list){
        account_payment_methodFeignClient.createBatch(list) ;
    }

    @Override
    public Account_payment_method get(Integer id) {
		Account_payment_method et=account_payment_methodFeignClient.get(id);
        if(et==null){
            et=new Account_payment_method();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_payment_method> searchDefault(Account_payment_methodSearchContext context) {
        Page<Account_payment_method> account_payment_methods=account_payment_methodFeignClient.searchDefault(context);
        return account_payment_methods;
    }


}


