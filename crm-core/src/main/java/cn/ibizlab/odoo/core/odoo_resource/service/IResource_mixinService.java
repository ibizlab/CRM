package cn.ibizlab.odoo.core.odoo_resource.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_mixin;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_mixinSearchContext;


/**
 * 实体[Resource_mixin] 服务对象接口
 */
public interface IResource_mixinService{

    boolean update(Resource_mixin et) ;
    void updateBatch(List<Resource_mixin> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Resource_mixin getDraft(Resource_mixin et) ;
    Resource_mixin get(Integer key) ;
    boolean create(Resource_mixin et) ;
    void createBatch(List<Resource_mixin> list) ;
    Page<Resource_mixin> searchDefault(Resource_mixinSearchContext context) ;

}



