package cn.ibizlab.odoo.core.odoo_purchase.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_order_line;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_order_lineSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[purchase_order_line] 服务对象接口
 */
@Component
public class purchase_order_lineFallback implements purchase_order_lineFeignClient{



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Purchase_order_line> searchDefault(Purchase_order_lineSearchContext context){
            return null;
     }



    public Purchase_order_line get(Integer id){
            return null;
     }


    public Purchase_order_line create(Purchase_order_line purchase_order_line){
            return null;
     }
    public Boolean createBatch(List<Purchase_order_line> purchase_order_lines){
            return false;
     }

    public Purchase_order_line update(Integer id, Purchase_order_line purchase_order_line){
            return null;
     }
    public Boolean updateBatch(List<Purchase_order_line> purchase_order_lines){
            return false;
     }


    public Page<Purchase_order_line> select(){
            return null;
     }

    public Purchase_order_line getDraft(){
            return null;
    }



}
