package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ires_partner_industry;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_partner_industry] 服务对象接口
 */
public interface Ires_partner_industryClientService{

    public Ires_partner_industry createModel() ;

    public void create(Ires_partner_industry res_partner_industry);

    public void updateBatch(List<Ires_partner_industry> res_partner_industries);

    public void update(Ires_partner_industry res_partner_industry);

    public void createBatch(List<Ires_partner_industry> res_partner_industries);

    public void removeBatch(List<Ires_partner_industry> res_partner_industries);

    public void remove(Ires_partner_industry res_partner_industry);

    public Page<Ires_partner_industry> fetchDefault(SearchContext context);

    public void get(Ires_partner_industry res_partner_industry);

    public Page<Ires_partner_industry> select(SearchContext context);

    public void getDraft(Ires_partner_industry res_partner_industry);

}
