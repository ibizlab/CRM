package cn.ibizlab.odoo.core.odoo_web_tour.valuerule.anno.web_tour_tour;

import cn.ibizlab.odoo.core.odoo_web_tour.valuerule.validator.web_tour_tour.Web_tour_tourIdDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Web_tour_tour
 * 属性：Id
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Web_tour_tourIdDefaultValidator.class})
public @interface Web_tour_tourIdDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
