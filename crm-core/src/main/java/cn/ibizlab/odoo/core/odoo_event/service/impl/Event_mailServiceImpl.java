package cn.ibizlab.odoo.core.odoo_event.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_mail;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_mailSearchContext;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_mailService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_event.client.event_mailFeignClient;

/**
 * 实体[自动发邮件] 服务对象接口实现
 */
@Slf4j
@Service
public class Event_mailServiceImpl implements IEvent_mailService {

    @Autowired
    event_mailFeignClient event_mailFeignClient;


    @Override
    public boolean create(Event_mail et) {
        Event_mail rt = event_mailFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Event_mail> list){
        event_mailFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=event_mailFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        event_mailFeignClient.removeBatch(idList);
    }

    @Override
    public Event_mail getDraft(Event_mail et) {
        et=event_mailFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Event_mail et) {
        Event_mail rt = event_mailFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Event_mail> list){
        event_mailFeignClient.updateBatch(list) ;
    }

    @Override
    public Event_mail get(Integer id) {
		Event_mail et=event_mailFeignClient.get(id);
        if(et==null){
            et=new Event_mail();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Event_mail> searchDefault(Event_mailSearchContext context) {
        Page<Event_mail> event_mails=event_mailFeignClient.searchDefault(context);
        return event_mails;
    }


}


