package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Irating_mixin;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[rating_mixin] 服务对象接口
 */
public interface Irating_mixinClientService{

    public Irating_mixin createModel() ;

    public void updateBatch(List<Irating_mixin> rating_mixins);

    public void create(Irating_mixin rating_mixin);

    public void get(Irating_mixin rating_mixin);

    public void removeBatch(List<Irating_mixin> rating_mixins);

    public void update(Irating_mixin rating_mixin);

    public void createBatch(List<Irating_mixin> rating_mixins);

    public Page<Irating_mixin> fetchDefault(SearchContext context);

    public void remove(Irating_mixin rating_mixin);

    public Page<Irating_mixin> select(SearchContext context);

    public void getDraft(Irating_mixin rating_mixin);

}
