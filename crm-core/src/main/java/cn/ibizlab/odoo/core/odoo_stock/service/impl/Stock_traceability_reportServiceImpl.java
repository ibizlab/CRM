package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_traceability_report;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_traceability_reportSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_traceability_reportService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_stock.client.stock_traceability_reportFeignClient;

/**
 * 实体[追溯报告] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_traceability_reportServiceImpl implements IStock_traceability_reportService {

    @Autowired
    stock_traceability_reportFeignClient stock_traceability_reportFeignClient;


    @Override
    public boolean create(Stock_traceability_report et) {
        Stock_traceability_report rt = stock_traceability_reportFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_traceability_report> list){
        stock_traceability_reportFeignClient.createBatch(list) ;
    }

    @Override
    public Stock_traceability_report getDraft(Stock_traceability_report et) {
        et=stock_traceability_reportFeignClient.getDraft();
        return et;
    }

    @Override
    public Stock_traceability_report get(Integer id) {
		Stock_traceability_report et=stock_traceability_reportFeignClient.get(id);
        if(et==null){
            et=new Stock_traceability_report();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Stock_traceability_report et) {
        Stock_traceability_report rt = stock_traceability_reportFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Stock_traceability_report> list){
        stock_traceability_reportFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=stock_traceability_reportFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        stock_traceability_reportFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_traceability_report> searchDefault(Stock_traceability_reportSearchContext context) {
        Page<Stock_traceability_report> stock_traceability_reports=stock_traceability_reportFeignClient.searchDefault(context);
        return stock_traceability_reports;
    }


}


