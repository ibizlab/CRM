package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_unreconcile;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_unreconcile] 服务对象接口
 */
public interface Iaccount_unreconcileClientService{

    public Iaccount_unreconcile createModel() ;

    public Page<Iaccount_unreconcile> fetchDefault(SearchContext context);

    public void get(Iaccount_unreconcile account_unreconcile);

    public void createBatch(List<Iaccount_unreconcile> account_unreconciles);

    public void removeBatch(List<Iaccount_unreconcile> account_unreconciles);

    public void create(Iaccount_unreconcile account_unreconcile);

    public void updateBatch(List<Iaccount_unreconcile> account_unreconciles);

    public void update(Iaccount_unreconcile account_unreconcile);

    public void remove(Iaccount_unreconcile account_unreconcile);

    public Page<Iaccount_unreconcile> select(SearchContext context);

    public void getDraft(Iaccount_unreconcile account_unreconcile);

}
