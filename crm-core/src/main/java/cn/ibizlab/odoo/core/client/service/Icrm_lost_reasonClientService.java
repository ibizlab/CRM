package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Icrm_lost_reason;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[crm_lost_reason] 服务对象接口
 */
public interface Icrm_lost_reasonClientService{

    public Icrm_lost_reason createModel() ;

    public void update(Icrm_lost_reason crm_lost_reason);

    public void removeBatch(List<Icrm_lost_reason> crm_lost_reasons);

    public void updateBatch(List<Icrm_lost_reason> crm_lost_reasons);

    public void remove(Icrm_lost_reason crm_lost_reason);

    public void create(Icrm_lost_reason crm_lost_reason);

    public Page<Icrm_lost_reason> fetchDefault(SearchContext context);

    public void get(Icrm_lost_reason crm_lost_reason);

    public void createBatch(List<Icrm_lost_reason> crm_lost_reasons);

    public Page<Icrm_lost_reason> select(SearchContext context);

    public void getDraft(Icrm_lost_reason crm_lost_reason);

}
