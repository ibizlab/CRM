package cn.ibizlab.odoo.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_pickingSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_picking] 服务对象接口
 */
@Component
public class stock_pickingFallback implements stock_pickingFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Stock_picking create(Stock_picking stock_picking){
            return null;
     }
    public Boolean createBatch(List<Stock_picking> stock_pickings){
            return false;
     }


    public Stock_picking get(Integer id){
            return null;
     }



    public Stock_picking update(Integer id, Stock_picking stock_picking){
            return null;
     }
    public Boolean updateBatch(List<Stock_picking> stock_pickings){
            return false;
     }



    public Page<Stock_picking> searchDefault(Stock_pickingSearchContext context){
            return null;
     }


    public Page<Stock_picking> select(){
            return null;
     }

    public Stock_picking getDraft(){
            return null;
    }



}
