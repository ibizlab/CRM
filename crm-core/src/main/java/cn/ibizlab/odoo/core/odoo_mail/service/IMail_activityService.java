package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_activitySearchContext;


/**
 * 实体[Mail_activity] 服务对象接口
 */
public interface IMail_activityService{

    Mail_activity getDraft(Mail_activity et) ;
    boolean update(Mail_activity et) ;
    void updateBatch(List<Mail_activity> list) ;
    boolean checkKey(Mail_activity et) ;
    boolean save(Mail_activity et) ;
    void saveBatch(List<Mail_activity> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Mail_activity get(Integer key) ;
    boolean create(Mail_activity et) ;
    void createBatch(List<Mail_activity> list) ;
    Page<Mail_activity> searchDefault(Mail_activitySearchContext context) ;

}



