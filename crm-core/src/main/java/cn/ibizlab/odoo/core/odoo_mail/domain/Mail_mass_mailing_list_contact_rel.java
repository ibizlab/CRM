package cn.ibizlab.odoo.core.odoo_mail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [群发邮件订阅信息] 对象
 */
@Data
public class Mail_mass_mailing_list_contact_rel extends EntityClient implements Serializable {

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 取消订阅日期
     */
    @DEField(name = "unsubscription_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "unsubscription_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("unsubscription_date")
    private Timestamp unsubscriptionDate;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 退出
     */
    @DEField(name = "opt_out")
    @JSONField(name = "opt_out")
    @JsonProperty("opt_out")
    private String optOut;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 被退回
     */
    @JSONField(name = "message_bounce")
    @JsonProperty("message_bounce")
    private Integer messageBounce;

    /**
     * 黑名单
     */
    @JSONField(name = "is_blacklisted")
    @JsonProperty("is_blacklisted")
    private String isBlacklisted;

    /**
     * 邮件列表
     */
    @JSONField(name = "list_id_text")
    @JsonProperty("list_id_text")
    private String listIdText;

    /**
     * 联系
     */
    @JSONField(name = "contact_id_text")
    @JsonProperty("contact_id_text")
    private String contactIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 联系人人数
     */
    @JSONField(name = "contact_count")
    @JsonProperty("contact_count")
    private Integer contactCount;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 联系
     */
    @DEField(name = "contact_id")
    @JSONField(name = "contact_id")
    @JsonProperty("contact_id")
    private Integer contactId;

    /**
     * 邮件列表
     */
    @DEField(name = "list_id")
    @JSONField(name = "list_id")
    @JsonProperty("list_id")
    private Integer listId;


    /**
     * 
     */
    @JSONField(name = "odoocontact")
    @JsonProperty("odoocontact")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_contact odooContact;

    /**
     * 
     */
    @JSONField(name = "odoolist")
    @JsonProperty("odoolist")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_list odooList;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [取消订阅日期]
     */
    public void setUnsubscriptionDate(Timestamp unsubscriptionDate){
        this.unsubscriptionDate = unsubscriptionDate ;
        this.modify("unsubscription_date",unsubscriptionDate);
    }
    /**
     * 设置 [退出]
     */
    public void setOptOut(String optOut){
        this.optOut = optOut ;
        this.modify("opt_out",optOut);
    }
    /**
     * 设置 [联系]
     */
    public void setContactId(Integer contactId){
        this.contactId = contactId ;
        this.modify("contact_id",contactId);
    }
    /**
     * 设置 [邮件列表]
     */
    public void setListId(Integer listId){
        this.listId = listId ;
        this.modify("list_id",listId);
    }

}


