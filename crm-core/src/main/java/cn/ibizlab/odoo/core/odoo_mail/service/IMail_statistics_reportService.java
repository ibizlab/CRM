package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_statistics_report;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_statistics_reportSearchContext;


/**
 * 实体[Mail_statistics_report] 服务对象接口
 */
public interface IMail_statistics_reportService{

    boolean update(Mail_statistics_report et) ;
    void updateBatch(List<Mail_statistics_report> list) ;
    Mail_statistics_report getDraft(Mail_statistics_report et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Mail_statistics_report get(Integer key) ;
    boolean create(Mail_statistics_report et) ;
    void createBatch(List<Mail_statistics_report> list) ;
    Page<Mail_statistics_report> searchDefault(Mail_statistics_reportSearchContext context) ;

}



