package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_blacklist;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_blacklistSearchContext;


/**
 * 实体[Mail_blacklist] 服务对象接口
 */
public interface IMail_blacklistService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Mail_blacklist get(Integer key) ;
    boolean update(Mail_blacklist et) ;
    void updateBatch(List<Mail_blacklist> list) ;
    boolean create(Mail_blacklist et) ;
    void createBatch(List<Mail_blacklist> list) ;
    Mail_blacklist getDraft(Mail_blacklist et) ;
    Page<Mail_blacklist> searchDefault(Mail_blacklistSearchContext context) ;

}



