package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_cashbox;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_cashboxSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_bank_statement_cashbox] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-bank-statement-cashbox", fallback = account_bank_statement_cashboxFallback.class)
public interface account_bank_statement_cashboxFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statement_cashboxes/{id}")
    Account_bank_statement_cashbox update(@PathVariable("id") Integer id,@RequestBody Account_bank_statement_cashbox account_bank_statement_cashbox);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statement_cashboxes/batch")
    Boolean updateBatch(@RequestBody List<Account_bank_statement_cashbox> account_bank_statement_cashboxes);


    @RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_cashboxes/{id}")
    Account_bank_statement_cashbox get(@PathVariable("id") Integer id);




    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_cashboxes/searchdefault")
    Page<Account_bank_statement_cashbox> searchDefault(@RequestBody Account_bank_statement_cashboxSearchContext context);



    @RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statement_cashboxes/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statement_cashboxes/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_cashboxes")
    Account_bank_statement_cashbox create(@RequestBody Account_bank_statement_cashbox account_bank_statement_cashbox);

    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_cashboxes/batch")
    Boolean createBatch(@RequestBody List<Account_bank_statement_cashbox> account_bank_statement_cashboxes);


    @RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_cashboxes/select")
    Page<Account_bank_statement_cashbox> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_cashboxes/getdraft")
    Account_bank_statement_cashbox getDraft();


}
