package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_track_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_track_line] 服务对象接口
 */
public interface Istock_track_lineClientService{

    public Istock_track_line createModel() ;

    public void updateBatch(List<Istock_track_line> stock_track_lines);

    public void get(Istock_track_line stock_track_line);

    public void create(Istock_track_line stock_track_line);

    public void removeBatch(List<Istock_track_line> stock_track_lines);

    public Page<Istock_track_line> fetchDefault(SearchContext context);

    public void update(Istock_track_line stock_track_line);

    public void remove(Istock_track_line stock_track_line);

    public void createBatch(List<Istock_track_line> stock_track_lines);

    public Page<Istock_track_line> select(SearchContext context);

    public void getDraft(Istock_track_line stock_track_line);

}
