package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Igamification_goal_wizard;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[gamification_goal_wizard] 服务对象接口
 */
public interface Igamification_goal_wizardClientService{

    public Igamification_goal_wizard createModel() ;

    public void removeBatch(List<Igamification_goal_wizard> gamification_goal_wizards);

    public void update(Igamification_goal_wizard gamification_goal_wizard);

    public Page<Igamification_goal_wizard> fetchDefault(SearchContext context);

    public void create(Igamification_goal_wizard gamification_goal_wizard);

    public void get(Igamification_goal_wizard gamification_goal_wizard);

    public void createBatch(List<Igamification_goal_wizard> gamification_goal_wizards);

    public void updateBatch(List<Igamification_goal_wizard> gamification_goal_wizards);

    public void remove(Igamification_goal_wizard gamification_goal_wizard);

    public Page<Igamification_goal_wizard> select(SearchContext context);

    public void getDraft(Igamification_goal_wizard gamification_goal_wizard);

}
