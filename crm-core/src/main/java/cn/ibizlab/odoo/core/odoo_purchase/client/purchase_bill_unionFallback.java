package cn.ibizlab.odoo.core.odoo_purchase.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_bill_union;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_bill_unionSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[purchase_bill_union] 服务对象接口
 */
@Component
public class purchase_bill_unionFallback implements purchase_bill_unionFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Purchase_bill_union create(Purchase_bill_union purchase_bill_union){
            return null;
     }
    public Boolean createBatch(List<Purchase_bill_union> purchase_bill_unions){
            return false;
     }

    public Page<Purchase_bill_union> searchDefault(Purchase_bill_unionSearchContext context){
            return null;
     }


    public Purchase_bill_union get(Integer id){
            return null;
     }





    public Purchase_bill_union update(Integer id, Purchase_bill_union purchase_bill_union){
            return null;
     }
    public Boolean updateBatch(List<Purchase_bill_union> purchase_bill_unions){
            return false;
     }


    public Page<Purchase_bill_union> select(){
            return null;
     }

    public Purchase_bill_union getDraft(){
            return null;
    }



}
