package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_partner_merge_line;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_partner_merge_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_partner_merge_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.base_partner_merge_lineFeignClient;

/**
 * 实体[合并合作伙伴明细行] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_partner_merge_lineServiceImpl implements IBase_partner_merge_lineService {

    @Autowired
    base_partner_merge_lineFeignClient base_partner_merge_lineFeignClient;


    @Override
    public Base_partner_merge_line getDraft(Base_partner_merge_line et) {
        et=base_partner_merge_lineFeignClient.getDraft();
        return et;
    }

    @Override
    public Base_partner_merge_line get(Integer id) {
		Base_partner_merge_line et=base_partner_merge_lineFeignClient.get(id);
        if(et==null){
            et=new Base_partner_merge_line();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Base_partner_merge_line et) {
        Base_partner_merge_line rt = base_partner_merge_lineFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Base_partner_merge_line> list){
        base_partner_merge_lineFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Base_partner_merge_line et) {
        Base_partner_merge_line rt = base_partner_merge_lineFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_partner_merge_line> list){
        base_partner_merge_lineFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=base_partner_merge_lineFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        base_partner_merge_lineFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_partner_merge_line> searchDefault(Base_partner_merge_lineSearchContext context) {
        Page<Base_partner_merge_line> base_partner_merge_lines=base_partner_merge_lineFeignClient.searchDefault(context);
        return base_partner_merge_lines;
    }


}


