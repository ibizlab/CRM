package cn.ibizlab.odoo.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave_type;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leave_typeSearchContext;


/**
 * 实体[Hr_leave_type] 服务对象接口
 */
public interface IHr_leave_typeService{

    boolean create(Hr_leave_type et) ;
    void createBatch(List<Hr_leave_type> list) ;
    Hr_leave_type getDraft(Hr_leave_type et) ;
    boolean update(Hr_leave_type et) ;
    void updateBatch(List<Hr_leave_type> list) ;
    Hr_leave_type get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Hr_leave_type> searchDefault(Hr_leave_typeSearchContext context) ;

}



