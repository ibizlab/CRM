package cn.ibizlab.odoo.core.odoo_calendar.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.odoo.util.filter.SearchContextBase;

/**
 * ServiceApi数据实体[Calendar_event] 查询条件对象
 */
@Slf4j
@Data
public class Calendar_eventSearchContext extends SearchContextBase {
	private String n_month_by_eq;//[选项]

	private String n_week_list_eq;//[工作日]

	private String n_end_type_eq;//[重复终止]

	private String n_attendee_status_eq;//[出席者状态]

	private String n_rrule_type_eq;//[重新提起]

	private String n_privacy_eq;//[隐私]

	private String n_state_eq;//[状态]

	private String n_show_as_eq;//[显示时间为]

	private String n_byday_eq;//[按 天]

	private String n_name_like;//[会议主题]

	private String n_opportunity_id_text_eq;//[商机]

	private String n_opportunity_id_text_like;//[商机]

	private String n_create_uid_text_eq;//[创建人]

	private String n_create_uid_text_like;//[创建人]

	private String n_write_uid_text_eq;//[最后更新人]

	private String n_write_uid_text_like;//[最后更新人]

	private String n_user_id_text_eq;//[所有者]

	private String n_user_id_text_like;//[所有者]

	private String n_applicant_id_text_eq;//[申请人]

	private String n_applicant_id_text_like;//[申请人]

	private Integer n_create_uid_eq;//[创建人]

	private Integer n_applicant_id_eq;//[申请人]

	private Integer n_write_uid_eq;//[最后更新人]

	private Integer n_user_id_eq;//[所有者]

	private Integer n_opportunity_id_eq;//[商机]

}



