package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [account_bank_statement_import_journal_creation] 对象
 */
public interface Iaccount_bank_statement_import_journal_creation {

    /**
     * 获取 [允许的科目]
     */
    public void setAccount_control_ids(String account_control_ids);
    
    /**
     * 设置 [允许的科目]
     */
    public String getAccount_control_ids();

    /**
     * 获取 [允许的科目]脏标记
     */
    public boolean getAccount_control_idsDirtyFlag();
    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [别名域]
     */
    public void setAlias_domain(String alias_domain);
    
    /**
     * 设置 [别名域]
     */
    public String getAlias_domain();

    /**
     * 获取 [别名域]脏标记
     */
    public boolean getAlias_domainDirtyFlag();
    /**
     * 获取 [别名]
     */
    public void setAlias_id(Integer alias_id);
    
    /**
     * 设置 [别名]
     */
    public Integer getAlias_id();

    /**
     * 获取 [别名]脏标记
     */
    public boolean getAlias_idDirtyFlag();
    /**
     * 获取 [供应商账单的别名]
     */
    public void setAlias_name(String alias_name);
    
    /**
     * 设置 [供应商账单的别名]
     */
    public String getAlias_name();

    /**
     * 获取 [供应商账单的别名]脏标记
     */
    public boolean getAlias_nameDirtyFlag();
    /**
     * 获取 [至少一个转入]
     */
    public void setAt_least_one_inbound(String at_least_one_inbound);
    
    /**
     * 设置 [至少一个转入]
     */
    public String getAt_least_one_inbound();

    /**
     * 获取 [至少一个转入]脏标记
     */
    public boolean getAt_least_one_inboundDirtyFlag();
    /**
     * 获取 [至少一个转出]
     */
    public void setAt_least_one_outbound(String at_least_one_outbound);
    
    /**
     * 设置 [至少一个转出]
     */
    public String getAt_least_one_outbound();

    /**
     * 获取 [至少一个转出]脏标记
     */
    public boolean getAt_least_one_outboundDirtyFlag();
    /**
     * 获取 [银行账户]
     */
    public void setBank_account_id(Integer bank_account_id);
    
    /**
     * 设置 [银行账户]
     */
    public Integer getBank_account_id();

    /**
     * 获取 [银行账户]脏标记
     */
    public boolean getBank_account_idDirtyFlag();
    /**
     * 获取 [账户号码]
     */
    public void setBank_acc_number(String bank_acc_number);
    
    /**
     * 设置 [账户号码]
     */
    public String getBank_acc_number();

    /**
     * 获取 [账户号码]脏标记
     */
    public boolean getBank_acc_numberDirtyFlag();
    /**
     * 获取 [银行]
     */
    public void setBank_id(Integer bank_id);
    
    /**
     * 设置 [银行]
     */
    public Integer getBank_id();

    /**
     * 获取 [银行]脏标记
     */
    public boolean getBank_idDirtyFlag();
    /**
     * 获取 [银行费用]
     */
    public void setBank_statements_source(String bank_statements_source);
    
    /**
     * 设置 [银行费用]
     */
    public String getBank_statements_source();

    /**
     * 获取 [银行费用]脏标记
     */
    public boolean getBank_statements_sourceDirtyFlag();
    /**
     * 获取 [属于用户的当前公司]
     */
    public void setBelongs_to_company(String belongs_to_company);
    
    /**
     * 设置 [属于用户的当前公司]
     */
    public String getBelongs_to_company();

    /**
     * 获取 [属于用户的当前公司]脏标记
     */
    public boolean getBelongs_to_companyDirtyFlag();
    /**
     * 获取 [简码]
     */
    public void setCode(String code);
    
    /**
     * 设置 [简码]
     */
    public String getCode();

    /**
     * 获取 [简码]脏标记
     */
    public boolean getCodeDirtyFlag();
    /**
     * 获取 [颜色索引]
     */
    public void setColor(Integer color);
    
    /**
     * 设置 [颜色索引]
     */
    public Integer getColor();

    /**
     * 获取 [颜色索引]脏标记
     */
    public boolean getColorDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [账户持有人]
     */
    public void setCompany_partner_id(Integer company_partner_id);
    
    /**
     * 设置 [账户持有人]
     */
    public Integer getCompany_partner_id();

    /**
     * 获取 [账户持有人]脏标记
     */
    public boolean getCompany_partner_idDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [默认贷方科目]
     */
    public void setDefault_credit_account_id(Integer default_credit_account_id);
    
    /**
     * 设置 [默认贷方科目]
     */
    public Integer getDefault_credit_account_id();

    /**
     * 获取 [默认贷方科目]脏标记
     */
    public boolean getDefault_credit_account_idDirtyFlag();
    /**
     * 获取 [默认借方科目]
     */
    public void setDefault_debit_account_id(Integer default_debit_account_id);
    
    /**
     * 设置 [默认借方科目]
     */
    public Integer getDefault_debit_account_id();

    /**
     * 获取 [默认借方科目]脏标记
     */
    public boolean getDefault_debit_account_idDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [分组发票明细行]
     */
    public void setGroup_invoice_lines(String group_invoice_lines);
    
    /**
     * 设置 [分组发票明细行]
     */
    public String getGroup_invoice_lines();

    /**
     * 获取 [分组发票明细行]脏标记
     */
    public boolean getGroup_invoice_linesDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [为收款]
     */
    public void setInbound_payment_method_ids(String inbound_payment_method_ids);
    
    /**
     * 设置 [为收款]
     */
    public String getInbound_payment_method_ids();

    /**
     * 获取 [为收款]脏标记
     */
    public boolean getInbound_payment_method_idsDirtyFlag();
    /**
     * 获取 [日记账]
     */
    public void setJournal_id(Integer journal_id);
    
    /**
     * 设置 [日记账]
     */
    public Integer getJournal_id();

    /**
     * 获取 [日记账]脏标记
     */
    public boolean getJournal_idDirtyFlag();
    /**
     * 获取 [看板仪表板]
     */
    public void setKanban_dashboard(String kanban_dashboard);
    
    /**
     * 设置 [看板仪表板]
     */
    public String getKanban_dashboard();

    /**
     * 获取 [看板仪表板]脏标记
     */
    public boolean getKanban_dashboardDirtyFlag();
    /**
     * 获取 [看板仪表板图表]
     */
    public void setKanban_dashboard_graph(String kanban_dashboard_graph);
    
    /**
     * 设置 [看板仪表板图表]
     */
    public String getKanban_dashboard_graph();

    /**
     * 获取 [看板仪表板图表]脏标记
     */
    public boolean getKanban_dashboard_graphDirtyFlag();
    /**
     * 获取 [损失科目]
     */
    public void setLoss_account_id(Integer loss_account_id);
    
    /**
     * 设置 [损失科目]
     */
    public Integer getLoss_account_id();

    /**
     * 获取 [损失科目]脏标记
     */
    public boolean getLoss_account_idDirtyFlag();
    /**
     * 获取 [日记账名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [日记账名称]
     */
    public String getName();

    /**
     * 获取 [日记账名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [为付款]
     */
    public void setOutbound_payment_method_ids(String outbound_payment_method_ids);
    
    /**
     * 设置 [为付款]
     */
    public String getOutbound_payment_method_ids();

    /**
     * 获取 [为付款]脏标记
     */
    public boolean getOutbound_payment_method_idsDirtyFlag();
    /**
     * 获取 [银行核销时过账]
     */
    public void setPost_at_bank_rec(String post_at_bank_rec);
    
    /**
     * 设置 [银行核销时过账]
     */
    public String getPost_at_bank_rec();

    /**
     * 获取 [银行核销时过账]脏标记
     */
    public boolean getPost_at_bank_recDirtyFlag();
    /**
     * 获取 [利润科目]
     */
    public void setProfit_account_id(Integer profit_account_id);
    
    /**
     * 设置 [利润科目]
     */
    public Integer getProfit_account_id();

    /**
     * 获取 [利润科目]脏标记
     */
    public boolean getProfit_account_idDirtyFlag();
    /**
     * 获取 [专用的信用票序列]
     */
    public void setRefund_sequence(String refund_sequence);
    
    /**
     * 设置 [专用的信用票序列]
     */
    public String getRefund_sequence();

    /**
     * 获取 [专用的信用票序列]脏标记
     */
    public boolean getRefund_sequenceDirtyFlag();
    /**
     * 获取 [信用票分录序列]
     */
    public void setRefund_sequence_id(Integer refund_sequence_id);
    
    /**
     * 设置 [信用票分录序列]
     */
    public Integer getRefund_sequence_id();

    /**
     * 获取 [信用票分录序列]脏标记
     */
    public boolean getRefund_sequence_idDirtyFlag();
    /**
     * 获取 [信用票：下一号码]
     */
    public void setRefund_sequence_number_next(Integer refund_sequence_number_next);
    
    /**
     * 设置 [信用票：下一号码]
     */
    public Integer getRefund_sequence_number_next();

    /**
     * 获取 [信用票：下一号码]脏标记
     */
    public boolean getRefund_sequence_number_nextDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [分录序列]
     */
    public void setSequence_id(Integer sequence_id);
    
    /**
     * 设置 [分录序列]
     */
    public Integer getSequence_id();

    /**
     * 获取 [分录序列]脏标记
     */
    public boolean getSequence_idDirtyFlag();
    /**
     * 获取 [下一号码]
     */
    public void setSequence_number_next(Integer sequence_number_next);
    
    /**
     * 设置 [下一号码]
     */
    public Integer getSequence_number_next();

    /**
     * 获取 [下一号码]脏标记
     */
    public boolean getSequence_number_nextDirtyFlag();
    /**
     * 获取 [在仪表板显示日记账]
     */
    public void setShow_on_dashboard(String show_on_dashboard);
    
    /**
     * 设置 [在仪表板显示日记账]
     */
    public String getShow_on_dashboard();

    /**
     * 获取 [在仪表板显示日记账]脏标记
     */
    public boolean getShow_on_dashboardDirtyFlag();
    /**
     * 获取 [类型]
     */
    public void setType(String type);
    
    /**
     * 设置 [类型]
     */
    public String getType();

    /**
     * 获取 [类型]脏标记
     */
    public boolean getTypeDirtyFlag();
    /**
     * 获取 [允许的科目类型]
     */
    public void setType_control_ids(String type_control_ids);
    
    /**
     * 设置 [允许的科目类型]
     */
    public String getType_control_ids();

    /**
     * 获取 [允许的科目类型]脏标记
     */
    public boolean getType_control_idsDirtyFlag();
    /**
     * 获取 [允许取消分录]
     */
    public void setUpdate_posted(String update_posted);
    
    /**
     * 设置 [允许取消分录]
     */
    public String getUpdate_posted();

    /**
     * 获取 [允许取消分录]脏标记
     */
    public boolean getUpdate_postedDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
