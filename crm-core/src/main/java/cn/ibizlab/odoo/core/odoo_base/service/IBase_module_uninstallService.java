package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Base_module_uninstall;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_module_uninstallSearchContext;


/**
 * 实体[Base_module_uninstall] 服务对象接口
 */
public interface IBase_module_uninstallService{

    boolean update(Base_module_uninstall et) ;
    void updateBatch(List<Base_module_uninstall> list) ;
    Base_module_uninstall get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Base_module_uninstall getDraft(Base_module_uninstall et) ;
    boolean create(Base_module_uninstall et) ;
    void createBatch(List<Base_module_uninstall> list) ;
    Page<Base_module_uninstall> searchDefault(Base_module_uninstallSearchContext context) ;

}



