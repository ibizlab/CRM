package cn.ibizlab.odoo.core.odoo_note.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_note.domain.Note_note;
import cn.ibizlab.odoo.core.odoo_note.filter.Note_noteSearchContext;
import cn.ibizlab.odoo.core.odoo_note.service.INote_noteService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_note.client.note_noteFeignClient;

/**
 * 实体[便签] 服务对象接口实现
 */
@Slf4j
@Service
public class Note_noteServiceImpl implements INote_noteService {

    @Autowired
    note_noteFeignClient note_noteFeignClient;


    @Override
    public Note_note get(Integer id) {
		Note_note et=note_noteFeignClient.get(id);
        if(et==null){
            et=new Note_note();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Note_note getDraft(Note_note et) {
        et=note_noteFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=note_noteFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        note_noteFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Note_note et) {
        Note_note rt = note_noteFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Note_note> list){
        note_noteFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Note_note et) {
        Note_note rt = note_noteFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Note_note> list){
        note_noteFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Note_note> searchDefault(Note_noteSearchContext context) {
        Page<Note_note> note_notes=note_noteFeignClient.searchDefault(context);
        return note_notes;
    }


}


