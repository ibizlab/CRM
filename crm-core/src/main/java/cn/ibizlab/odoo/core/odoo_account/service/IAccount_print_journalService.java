package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_print_journal;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_print_journalSearchContext;


/**
 * 实体[Account_print_journal] 服务对象接口
 */
public interface IAccount_print_journalService{

    Account_print_journal getDraft(Account_print_journal et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Account_print_journal et) ;
    void updateBatch(List<Account_print_journal> list) ;
    Account_print_journal get(Integer key) ;
    boolean create(Account_print_journal et) ;
    void createBatch(List<Account_print_journal> list) ;
    Page<Account_print_journal> searchDefault(Account_print_journalSearchContext context) ;

}



