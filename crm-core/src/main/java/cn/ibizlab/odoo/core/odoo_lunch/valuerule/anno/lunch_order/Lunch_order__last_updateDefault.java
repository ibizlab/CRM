package cn.ibizlab.odoo.core.odoo_lunch.valuerule.anno.lunch_order;

import cn.ibizlab.odoo.core.odoo_lunch.valuerule.validator.lunch_order.Lunch_order__last_updateDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Lunch_order
 * 属性：__last_update
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Lunch_order__last_updateDefaultValidator.class})
public @interface Lunch_order__last_updateDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
