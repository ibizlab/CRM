package cn.ibizlab.odoo.core.odoo_payment.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_transaction;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_transactionSearchContext;


/**
 * 实体[Payment_transaction] 服务对象接口
 */
public interface IPayment_transactionService{

    boolean create(Payment_transaction et) ;
    void createBatch(List<Payment_transaction> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Payment_transaction get(Integer key) ;
    Payment_transaction getDraft(Payment_transaction et) ;
    boolean update(Payment_transaction et) ;
    void updateBatch(List<Payment_transaction> list) ;
    Page<Payment_transaction> searchDefault(Payment_transactionSearchContext context) ;

}



