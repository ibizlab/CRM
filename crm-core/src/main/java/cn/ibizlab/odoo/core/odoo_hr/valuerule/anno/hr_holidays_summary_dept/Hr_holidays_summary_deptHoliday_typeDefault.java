package cn.ibizlab.odoo.core.odoo_hr.valuerule.anno.hr_holidays_summary_dept;

import cn.ibizlab.odoo.core.odoo_hr.valuerule.validator.hr_holidays_summary_dept.Hr_holidays_summary_deptHoliday_typeDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Hr_holidays_summary_dept
 * 属性：Holiday_type
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Hr_holidays_summary_deptHoliday_typeDefaultValidator.class})
public @interface Hr_holidays_summary_deptHoliday_typeDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
