package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_unreconcile;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_unreconcileSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_unreconcileService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_unreconcileFeignClient;

/**
 * 实体[会计取消核销] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_unreconcileServiceImpl implements IAccount_unreconcileService {

    @Autowired
    account_unreconcileFeignClient account_unreconcileFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=account_unreconcileFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_unreconcileFeignClient.removeBatch(idList);
    }

    @Override
    public Account_unreconcile get(Integer id) {
		Account_unreconcile et=account_unreconcileFeignClient.get(id);
        if(et==null){
            et=new Account_unreconcile();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Account_unreconcile et) {
        Account_unreconcile rt = account_unreconcileFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_unreconcile> list){
        account_unreconcileFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Account_unreconcile et) {
        Account_unreconcile rt = account_unreconcileFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_unreconcile> list){
        account_unreconcileFeignClient.createBatch(list) ;
    }

    @Override
    public Account_unreconcile getDraft(Account_unreconcile et) {
        et=account_unreconcileFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_unreconcile> searchDefault(Account_unreconcileSearchContext context) {
        Page<Account_unreconcile> account_unreconciles=account_unreconcileFeignClient.searchDefault(context);
        return account_unreconciles;
    }


}


