package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [lunch_order_line] 对象
 */
public interface Ilunch_order_line {

    /**
     * 获取 [现金划拨]
     */
    public void setCashmove(String cashmove);
    
    /**
     * 设置 [现金划拨]
     */
    public String getCashmove();

    /**
     * 获取 [现金划拨]脏标记
     */
    public boolean getCashmoveDirtyFlag();
    /**
     * 获取 [产品种类]
     */
    public void setCategory_id(Integer category_id);
    
    /**
     * 设置 [产品种类]
     */
    public Integer getCategory_id();

    /**
     * 获取 [产品种类]脏标记
     */
    public boolean getCategory_idDirtyFlag();
    /**
     * 获取 [产品种类]
     */
    public void setCategory_id_text(String category_id_text);
    
    /**
     * 设置 [产品种类]
     */
    public String getCategory_id_text();

    /**
     * 获取 [产品种类]脏标记
     */
    public boolean getCategory_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [日期]
     */
    public void setDate(Timestamp date);
    
    /**
     * 设置 [日期]
     */
    public Timestamp getDate();

    /**
     * 获取 [日期]脏标记
     */
    public boolean getDateDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [产品名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [产品名称]
     */
    public String getName();

    /**
     * 获取 [产品名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [笔记]
     */
    public void setNote(String note);
    
    /**
     * 设置 [笔记]
     */
    public String getNote();

    /**
     * 获取 [笔记]脏标记
     */
    public boolean getNoteDirtyFlag();
    /**
     * 获取 [订单]
     */
    public void setOrder_id(Integer order_id);
    
    /**
     * 设置 [订单]
     */
    public Integer getOrder_id();

    /**
     * 获取 [订单]脏标记
     */
    public boolean getOrder_idDirtyFlag();
    /**
     * 获取 [价格]
     */
    public void setPrice(Double price);
    
    /**
     * 设置 [价格]
     */
    public Double getPrice();

    /**
     * 获取 [价格]脏标记
     */
    public boolean getPriceDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id(Integer product_id);
    
    /**
     * 设置 [产品]
     */
    public Integer getProduct_id();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_idDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [供应商]
     */
    public void setSupplier(Integer supplier);
    
    /**
     * 设置 [供应商]
     */
    public Integer getSupplier();

    /**
     * 获取 [供应商]脏标记
     */
    public boolean getSupplierDirtyFlag();
    /**
     * 获取 [供应商]
     */
    public void setSupplier_text(String supplier_text);
    
    /**
     * 设置 [供应商]
     */
    public String getSupplier_text();

    /**
     * 获取 [供应商]脏标记
     */
    public boolean getSupplier_textDirtyFlag();
    /**
     * 获取 [用户]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [用户]
     */
    public Integer getUser_id();

    /**
     * 获取 [用户]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [用户]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [用户]
     */
    public String getUser_id_text();

    /**
     * 获取 [用户]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
