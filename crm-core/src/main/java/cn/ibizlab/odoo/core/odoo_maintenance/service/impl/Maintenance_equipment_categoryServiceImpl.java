package cn.ibizlab.odoo.core.odoo_maintenance.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_equipment_category;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_equipment_categorySearchContext;
import cn.ibizlab.odoo.core.odoo_maintenance.service.IMaintenance_equipment_categoryService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_maintenance.client.maintenance_equipment_categoryFeignClient;

/**
 * 实体[维护设备类别] 服务对象接口实现
 */
@Slf4j
@Service
public class Maintenance_equipment_categoryServiceImpl implements IMaintenance_equipment_categoryService {

    @Autowired
    maintenance_equipment_categoryFeignClient maintenance_equipment_categoryFeignClient;


    @Override
    public Maintenance_equipment_category getDraft(Maintenance_equipment_category et) {
        et=maintenance_equipment_categoryFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Maintenance_equipment_category et) {
        Maintenance_equipment_category rt = maintenance_equipment_categoryFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Maintenance_equipment_category> list){
        maintenance_equipment_categoryFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Maintenance_equipment_category et) {
        Maintenance_equipment_category rt = maintenance_equipment_categoryFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Maintenance_equipment_category> list){
        maintenance_equipment_categoryFeignClient.createBatch(list) ;
    }

    @Override
    public Maintenance_equipment_category get(Integer id) {
		Maintenance_equipment_category et=maintenance_equipment_categoryFeignClient.get(id);
        if(et==null){
            et=new Maintenance_equipment_category();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=maintenance_equipment_categoryFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        maintenance_equipment_categoryFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Maintenance_equipment_category> searchDefault(Maintenance_equipment_categorySearchContext context) {
        Page<Maintenance_equipment_category> maintenance_equipment_categorys=maintenance_equipment_categoryFeignClient.searchDefault(context);
        return maintenance_equipment_categorys;
    }


}


