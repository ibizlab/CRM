package cn.ibizlab.odoo.core.odoo_maintenance.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_stage;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_stageSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[maintenance_stage] 服务对象接口
 */
@FeignClient(value = "odoo-maintenance", contextId = "maintenance-stage", fallback = maintenance_stageFallback.class)
public interface maintenance_stageFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/maintenance_stages/searchdefault")
    Page<Maintenance_stage> searchDefault(@RequestBody Maintenance_stageSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/maintenance_stages/{id}")
    Maintenance_stage get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/maintenance_stages")
    Maintenance_stage create(@RequestBody Maintenance_stage maintenance_stage);

    @RequestMapping(method = RequestMethod.POST, value = "/maintenance_stages/batch")
    Boolean createBatch(@RequestBody List<Maintenance_stage> maintenance_stages);


    @RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_stages/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_stages/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/maintenance_stages/{id}")
    Maintenance_stage update(@PathVariable("id") Integer id,@RequestBody Maintenance_stage maintenance_stage);

    @RequestMapping(method = RequestMethod.PUT, value = "/maintenance_stages/batch")
    Boolean updateBatch(@RequestBody List<Maintenance_stage> maintenance_stages);





    @RequestMapping(method = RequestMethod.GET, value = "/maintenance_stages/select")
    Page<Maintenance_stage> select();


    @RequestMapping(method = RequestMethod.GET, value = "/maintenance_stages/getdraft")
    Maintenance_stage getDraft();


}
