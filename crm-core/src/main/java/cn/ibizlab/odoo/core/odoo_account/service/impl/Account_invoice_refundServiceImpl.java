package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_refund;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_refundSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoice_refundService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_invoice_refundFeignClient;

/**
 * 实体[信用票] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_invoice_refundServiceImpl implements IAccount_invoice_refundService {

    @Autowired
    account_invoice_refundFeignClient account_invoice_refundFeignClient;


    @Override
    public Account_invoice_refund get(Integer id) {
		Account_invoice_refund et=account_invoice_refundFeignClient.get(id);
        if(et==null){
            et=new Account_invoice_refund();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Account_invoice_refund getDraft(Account_invoice_refund et) {
        et=account_invoice_refundFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean create(Account_invoice_refund et) {
        Account_invoice_refund rt = account_invoice_refundFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_invoice_refund> list){
        account_invoice_refundFeignClient.createBatch(list) ;
    }

    @Override
    public boolean update(Account_invoice_refund et) {
        Account_invoice_refund rt = account_invoice_refundFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_invoice_refund> list){
        account_invoice_refundFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_invoice_refundFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_invoice_refundFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_invoice_refund> searchDefault(Account_invoice_refundSearchContext context) {
        Page<Account_invoice_refund> account_invoice_refunds=account_invoice_refundFeignClient.searchDefault(context);
        return account_invoice_refunds;
    }


}


