package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ires_users_log;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_users_log] 服务对象接口
 */
public interface Ires_users_logClientService{

    public Ires_users_log createModel() ;

    public void removeBatch(List<Ires_users_log> res_users_logs);

    public void remove(Ires_users_log res_users_log);

    public void create(Ires_users_log res_users_log);

    public Page<Ires_users_log> fetchDefault(SearchContext context);

    public void update(Ires_users_log res_users_log);

    public void get(Ires_users_log res_users_log);

    public void updateBatch(List<Ires_users_log> res_users_logs);

    public void createBatch(List<Ires_users_log> res_users_logs);

    public Page<Ires_users_log> select(SearchContext context);

    public void getDraft(Ires_users_log res_users_log);

}
