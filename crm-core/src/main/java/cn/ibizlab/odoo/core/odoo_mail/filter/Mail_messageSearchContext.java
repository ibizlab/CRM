package cn.ibizlab.odoo.core.odoo_mail.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.odoo.util.filter.SearchContextBase;

/**
 * ServiceApi数据实体[Mail_message] 查询条件对象
 */
@Slf4j
@Data
public class Mail_messageSearchContext extends SearchContextBase {
	private String n_moderation_status_eq;//[管理状态]

	private String n_message_type_eq;//[类型]

	private String n_author_id_text_eq;//[作者]

	private String n_author_id_text_like;//[作者]

	private String n_create_uid_text_eq;//[创建人]

	private String n_create_uid_text_like;//[创建人]

	private String n_mail_activity_type_id_text_eq;//[邮件活动类型]

	private String n_mail_activity_type_id_text_like;//[邮件活动类型]

	private String n_moderator_id_text_eq;//[管理员]

	private String n_moderator_id_text_like;//[管理员]

	private String n_write_uid_text_eq;//[最后更新者]

	private String n_write_uid_text_like;//[最后更新者]

	private String n_subtype_id_text_eq;//[子类型]

	private String n_subtype_id_text_like;//[子类型]

	private Integer n_write_uid_eq;//[最后更新者]

	private Integer n_mail_activity_type_id_eq;//[邮件活动类型]

	private Integer n_moderator_id_eq;//[管理员]

	private Integer n_create_uid_eq;//[创建人]

	private Integer n_author_id_eq;//[作者]

	private Integer n_parent_id_eq;//[上级消息]

	private Integer n_subtype_id_eq;//[子类型]

}



