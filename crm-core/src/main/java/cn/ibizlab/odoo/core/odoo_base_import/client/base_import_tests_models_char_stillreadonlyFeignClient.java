package cn.ibizlab.odoo.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_char_stillreadonly;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_char_stillreadonlySearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_import_tests_models_char_stillreadonly] 服务对象接口
 */
@FeignClient(value = "odoo-base-import", contextId = "base-import-tests-models-char-stillreadonly", fallback = base_import_tests_models_char_stillreadonlyFallback.class)
public interface base_import_tests_models_char_stillreadonlyFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_char_stillreadonlies/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_char_stillreadonlies/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_char_stillreadonlies/{id}")
    Base_import_tests_models_char_stillreadonly get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_stillreadonlies")
    Base_import_tests_models_char_stillreadonly create(@RequestBody Base_import_tests_models_char_stillreadonly base_import_tests_models_char_stillreadonly);

    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_stillreadonlies/batch")
    Boolean createBatch(@RequestBody List<Base_import_tests_models_char_stillreadonly> base_import_tests_models_char_stillreadonlies);





    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_stillreadonlies/searchdefault")
    Page<Base_import_tests_models_char_stillreadonly> searchDefault(@RequestBody Base_import_tests_models_char_stillreadonlySearchContext context);



    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_char_stillreadonlies/{id}")
    Base_import_tests_models_char_stillreadonly update(@PathVariable("id") Integer id,@RequestBody Base_import_tests_models_char_stillreadonly base_import_tests_models_char_stillreadonly);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_char_stillreadonlies/batch")
    Boolean updateBatch(@RequestBody List<Base_import_tests_models_char_stillreadonly> base_import_tests_models_char_stillreadonlies);


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_char_stillreadonlies/select")
    Page<Base_import_tests_models_char_stillreadonly> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_char_stillreadonlies/getdraft")
    Base_import_tests_models_char_stillreadonly getDraft();


}
