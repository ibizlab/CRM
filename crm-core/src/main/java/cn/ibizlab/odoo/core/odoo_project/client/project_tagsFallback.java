package cn.ibizlab.odoo.core.odoo_project.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_project.domain.Project_tags;
import cn.ibizlab.odoo.core.odoo_project.filter.Project_tagsSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[project_tags] 服务对象接口
 */
@Component
public class project_tagsFallback implements project_tagsFeignClient{

    public Page<Project_tags> searchDefault(Project_tagsSearchContext context){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Project_tags update(Integer id, Project_tags project_tags){
            return null;
     }
    public Boolean updateBatch(List<Project_tags> project_tags){
            return false;
     }



    public Project_tags create(Project_tags project_tags){
            return null;
     }
    public Boolean createBatch(List<Project_tags> project_tags){
            return false;
     }


    public Project_tags get(Integer id){
            return null;
     }



    public Page<Project_tags> select(){
            return null;
     }

    public Project_tags getDraft(){
            return null;
    }



}
