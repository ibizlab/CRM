package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_cashbox;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_cashboxSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_bank_statement_cashboxService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_bank_statement_cashboxFeignClient;

/**
 * 实体[银行资金调节表] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_bank_statement_cashboxServiceImpl implements IAccount_bank_statement_cashboxService {

    @Autowired
    account_bank_statement_cashboxFeignClient account_bank_statement_cashboxFeignClient;


    @Override
    public Account_bank_statement_cashbox getDraft(Account_bank_statement_cashbox et) {
        et=account_bank_statement_cashboxFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_bank_statement_cashboxFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_bank_statement_cashboxFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Account_bank_statement_cashbox et) {
        Account_bank_statement_cashbox rt = account_bank_statement_cashboxFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_bank_statement_cashbox> list){
        account_bank_statement_cashboxFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Account_bank_statement_cashbox et) {
        Account_bank_statement_cashbox rt = account_bank_statement_cashboxFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_bank_statement_cashbox> list){
        account_bank_statement_cashboxFeignClient.createBatch(list) ;
    }

    @Override
    public Account_bank_statement_cashbox get(Integer id) {
		Account_bank_statement_cashbox et=account_bank_statement_cashboxFeignClient.get(id);
        if(et==null){
            et=new Account_bank_statement_cashbox();
            et.setId(id);
        }
        else{
        }
        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_bank_statement_cashbox> searchDefault(Account_bank_statement_cashboxSearchContext context) {
        Page<Account_bank_statement_cashbox> account_bank_statement_cashboxs=account_bank_statement_cashboxFeignClient.searchDefault(context);
        return account_bank_statement_cashboxs;
    }


}


