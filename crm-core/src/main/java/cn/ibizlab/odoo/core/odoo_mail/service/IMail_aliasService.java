package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_alias;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_aliasSearchContext;


/**
 * 实体[Mail_alias] 服务对象接口
 */
public interface IMail_aliasService{

    Mail_alias getDraft(Mail_alias et) ;
    boolean create(Mail_alias et) ;
    void createBatch(List<Mail_alias> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Mail_alias et) ;
    void updateBatch(List<Mail_alias> list) ;
    Mail_alias get(Integer key) ;
    Page<Mail_alias> searchDefault(Mail_aliasSearchContext context) ;

}



