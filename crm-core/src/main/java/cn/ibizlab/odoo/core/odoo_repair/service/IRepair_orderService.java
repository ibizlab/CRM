package cn.ibizlab.odoo.core.odoo_repair.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_order;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_orderSearchContext;


/**
 * 实体[Repair_order] 服务对象接口
 */
public interface IRepair_orderService{

    Repair_order get(Integer key) ;
    Repair_order getDraft(Repair_order et) ;
    boolean update(Repair_order et) ;
    void updateBatch(List<Repair_order> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Repair_order et) ;
    void createBatch(List<Repair_order> list) ;
    Page<Repair_order> searchDefault(Repair_orderSearchContext context) ;

}



