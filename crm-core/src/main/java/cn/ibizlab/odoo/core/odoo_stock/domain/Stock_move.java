package cn.ibizlab.odoo.core.odoo_stock.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [库存移动] 对象
 */
@Data
public class Stock_move extends EntityClient implements Serializable {

    /**
     * 全部退回移动
     */
    @JSONField(name = "returned_move_ids")
    @JsonProperty("returned_move_ids")
    private String returnedMoveIds;

    /**
     * 在分拣确认后是否添加了移动
     */
    @JSONField(name = "additional")
    @JsonProperty("additional")
    private String additional;

    /**
     * 单价
     */
    @DEField(name = "price_unit")
    @JSONField(name = "price_unit")
    @JsonProperty("price_unit")
    private Double priceUnit;

    /**
     * 补货组
     */
    @DEField(name = "group_id")
    @JSONField(name = "group_id")
    @JsonProperty("group_id")
    private Integer groupId;

    /**
     * 报废
     */
    @JSONField(name = "scrap_ids")
    @JsonProperty("scrap_ids")
    private String scrapIds;

    /**
     * 会计凭证
     */
    @JSONField(name = "account_move_ids")
    @JsonProperty("account_move_ids")
    private String accountMoveIds;

    /**
     * 目的地移动
     */
    @JSONField(name = "move_dest_ids")
    @JsonProperty("move_dest_ids")
    private String moveDestIds;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 退款 (更新 SO/PO)
     */
    @DEField(name = "to_refund")
    @JSONField(name = "to_refund")
    @JsonProperty("to_refund")
    private String toRefund;

    /**
     * 单位因子
     */
    @DEField(name = "unit_factor")
    @JSONField(name = "unit_factor")
    @JsonProperty("unit_factor")
    private Double unitFactor;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 预测数量
     */
    @JSONField(name = "availability")
    @JsonProperty("availability")
    private Double availability;

    /**
     * 剩余数量
     */
    @DEField(name = "remaining_qty")
    @JSONField(name = "remaining_qty")
    @JsonProperty("remaining_qty")
    private Double remainingQty;

    /**
     * 传播取消以及拆分
     */
    @JSONField(name = "propagate")
    @JsonProperty("propagate")
    private String propagate;

    /**
     * 编号
     */
    @JSONField(name = "reference")
    @JsonProperty("reference")
    private String reference;

    /**
     * 批次
     */
    @JSONField(name = "active_move_line_ids")
    @JsonProperty("active_move_line_ids")
    private String activeMoveLineIds;

    /**
     * 移动行无建议
     */
    @JSONField(name = "move_line_nosuggest_ids")
    @JsonProperty("move_line_nosuggest_ids")
    private String moveLineNosuggestIds;

    /**
     * 详情可见
     */
    @JSONField(name = "show_details_visible")
    @JsonProperty("show_details_visible")
    private String showDetailsVisible;

    /**
     * 实际数量
     */
    @DEField(name = "product_qty")
    @JSONField(name = "product_qty")
    @JsonProperty("product_qty")
    private Double productQty;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 源文档
     */
    @JSONField(name = "origin")
    @JsonProperty("origin")
    private String origin;

    /**
     * 初始需求
     */
    @DEField(name = "product_uom_qty")
    @JSONField(name = "product_uom_qty")
    @JsonProperty("product_uom_qty")
    private Double productUomQty;

    /**
     * 追踪
     */
    @JSONField(name = "needs_lots")
    @JsonProperty("needs_lots")
    private String needsLots;

    /**
     * 优先级
     */
    @JSONField(name = "priority")
    @JsonProperty("priority")
    private String priority;

    /**
     * 日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 剩余价值
     */
    @DEField(name = "remaining_value")
    @JSONField(name = "remaining_value")
    @JsonProperty("remaining_value")
    private Double remainingValue;

    /**
     * 订单完成批次
     */
    @JSONField(name = "order_finished_lot_ids")
    @JsonProperty("order_finished_lot_ids")
    private String orderFinishedLotIds;

    /**
     * 序号
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 创建日期
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 原始移动
     */
    @JSONField(name = "move_orig_ids")
    @JsonProperty("move_orig_ids")
    private String moveOrigIds;

    /**
     * 凭证明细
     */
    @JSONField(name = "move_line_ids")
    @JsonProperty("move_line_ids")
    private String moveLineIds;

    /**
     * 备注
     */
    @JSONField(name = "note")
    @JsonProperty("note")
    private String note;

    /**
     * 已预留数量
     */
    @JSONField(name = "reserved_availability")
    @JsonProperty("reserved_availability")
    private Double reservedAvailability;

    /**
     * 初始需求是否可以编辑
     */
    @JSONField(name = "is_initial_demand_editable")
    @JsonProperty("is_initial_demand_editable")
    private String isInitialDemandEditable;

    /**
     * 完成数量
     */
    @JSONField(name = "quantity_done")
    @JsonProperty("quantity_done")
    private Double quantityDone;

    /**
     * 是锁定
     */
    @JSONField(name = "is_locked")
    @JsonProperty("is_locked")
    private String isLocked;

    /**
     * 可用量
     */
    @JSONField(name = "string_availability_info")
    @JsonProperty("string_availability_info")
    private String stringAvailabilityInfo;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 从供应商
     */
    @JSONField(name = "show_reserved_availability")
    @JsonProperty("show_reserved_availability")
    private String showReservedAvailability;

    /**
     * 目的路线
     */
    @JSONField(name = "route_ids")
    @JsonProperty("route_ids")
    private String routeIds;

    /**
     * 状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 说明
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 值
     */
    @JSONField(name = "value")
    @JsonProperty("value")
    private Double value;

    /**
     * 完工批次已存在
     */
    @JSONField(name = "finished_lots_exist")
    @JsonProperty("finished_lots_exist")
    private String finishedLotsExist;

    /**
     * 完成
     */
    @DEField(name = "is_done")
    @JSONField(name = "is_done")
    @JsonProperty("is_done")
    private String isDone;

    /**
     * 完成数量是否可以编辑
     */
    @JSONField(name = "is_quantity_done_editable")
    @JsonProperty("is_quantity_done_editable")
    private String isQuantityDoneEditable;

    /**
     * 有移动行
     */
    @JSONField(name = "has_move_lines")
    @JsonProperty("has_move_lines")
    private String hasMoveLines;

    /**
     * 预计日期
     */
    @DEField(name = "date_expected")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_expected" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_expected")
    private Timestamp dateExpected;

    /**
     * 供应方法
     */
    @DEField(name = "procure_method")
    @JSONField(name = "procure_method")
    @JsonProperty("procure_method")
    private String procureMethod;

    /**
     * 产品类型
     */
    @JSONField(name = "product_type")
    @JsonProperty("product_type")
    private String productType;

    /**
     * 拆卸顺序
     */
    @JSONField(name = "unbuild_id_text")
    @JsonProperty("unbuild_id_text")
    private String unbuildIdText;

    /**
     * 已报废
     */
    @JSONField(name = "scrapped")
    @JsonProperty("scrapped")
    private String scrapped;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 成品的生产订单
     */
    @JSONField(name = "production_id_text")
    @JsonProperty("production_id_text")
    private String productionIdText;

    /**
     * 仓库
     */
    @JSONField(name = "warehouse_id_text")
    @JsonProperty("warehouse_id_text")
    private String warehouseIdText;

    /**
     * 显示详细作业
     */
    @JSONField(name = "show_operations")
    @JsonProperty("show_operations")
    private String showOperations;

    /**
     * 创建生产订单
     */
    @JSONField(name = "created_production_id_text")
    @JsonProperty("created_production_id_text")
    private String createdProductionIdText;

    /**
     * 创建采购订单行
     */
    @JSONField(name = "created_purchase_line_id_text")
    @JsonProperty("created_purchase_line_id_text")
    private String createdPurchaseLineIdText;

    /**
     * 目的位置
     */
    @JSONField(name = "location_dest_id_text")
    @JsonProperty("location_dest_id_text")
    private String locationDestIdText;

    /**
     * 使用追踪的产品
     */
    @JSONField(name = "has_tracking")
    @JsonProperty("has_tracking")
    private String hasTracking;

    /**
     * 作业类型
     */
    @JSONField(name = "picking_type_id_text")
    @JsonProperty("picking_type_id_text")
    private String pickingTypeIdText;

    /**
     * 销售明细行
     */
    @JSONField(name = "sale_line_id_text")
    @JsonProperty("sale_line_id_text")
    private String saleLineIdText;

    /**
     * 目的地地址
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 调拨目的地地址
     */
    @JSONField(name = "picking_partner_id")
    @JsonProperty("picking_partner_id")
    private Integer pickingPartnerId;

    /**
     * 原材料的生产订单
     */
    @JSONField(name = "raw_material_production_id_text")
    @JsonProperty("raw_material_production_id_text")
    private String rawMaterialProductionIdText;

    /**
     * 原始退回移动
     */
    @JSONField(name = "origin_returned_move_id_text")
    @JsonProperty("origin_returned_move_id_text")
    private String originReturnedMoveIdText;

    /**
     * 维修
     */
    @JSONField(name = "repair_id_text")
    @JsonProperty("repair_id_text")
    private String repairIdText;

    /**
     * 欠单
     */
    @JSONField(name = "backorder_id")
    @JsonProperty("backorder_id")
    private Integer backorderId;

    /**
     * 采购订单行
     */
    @JSONField(name = "purchase_line_id_text")
    @JsonProperty("purchase_line_id_text")
    private String purchaseLineIdText;

    /**
     * 调拨参照
     */
    @JSONField(name = "picking_id_text")
    @JsonProperty("picking_id_text")
    private String pickingIdText;

    /**
     * 产品
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;

    /**
     * 源位置
     */
    @JSONField(name = "location_id_text")
    @JsonProperty("location_id_text")
    private String locationIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 单位
     */
    @JSONField(name = "product_uom_text")
    @JsonProperty("product_uom_text")
    private String productUomText;

    /**
     * 待加工的作业
     */
    @JSONField(name = "operation_id_text")
    @JsonProperty("operation_id_text")
    private String operationIdText;

    /**
     * 待消耗的工单
     */
    @JSONField(name = "workorder_id_text")
    @JsonProperty("workorder_id_text")
    private String workorderIdText;

    /**
     * 拆卸单
     */
    @JSONField(name = "consume_unbuild_id_text")
    @JsonProperty("consume_unbuild_id_text")
    private String consumeUnbuildIdText;

    /**
     * 首选包装
     */
    @JSONField(name = "product_packaging_text")
    @JsonProperty("product_packaging_text")
    private String productPackagingText;

    /**
     * 所有者
     */
    @JSONField(name = "restrict_partner_id_text")
    @JsonProperty("restrict_partner_id_text")
    private String restrictPartnerIdText;

    /**
     * 库存
     */
    @JSONField(name = "inventory_id_text")
    @JsonProperty("inventory_id_text")
    private String inventoryIdText;

    /**
     * 产品模板
     */
    @JSONField(name = "product_tmpl_id")
    @JsonProperty("product_tmpl_id")
    private Integer productTmplId;

    /**
     * 库存规则
     */
    @JSONField(name = "rule_id_text")
    @JsonProperty("rule_id_text")
    private String ruleIdText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 作业的类型
     */
    @JSONField(name = "picking_code")
    @JsonProperty("picking_code")
    private String pickingCode;

    /**
     * 移动整个包裹
     */
    @JSONField(name = "picking_type_entire_packs")
    @JsonProperty("picking_type_entire_packs")
    private String pickingTypeEntirePacks;

    /**
     * 创建生产订单
     */
    @DEField(name = "created_production_id")
    @JSONField(name = "created_production_id")
    @JsonProperty("created_production_id")
    private Integer createdProductionId;

    /**
     * 拆卸单
     */
    @DEField(name = "consume_unbuild_id")
    @JSONField(name = "consume_unbuild_id")
    @JsonProperty("consume_unbuild_id")
    private Integer consumeUnbuildId;

    /**
     * 销售明细行
     */
    @DEField(name = "sale_line_id")
    @JSONField(name = "sale_line_id")
    @JsonProperty("sale_line_id")
    private Integer saleLineId;

    /**
     * 原始退回移动
     */
    @DEField(name = "origin_returned_move_id")
    @JSONField(name = "origin_returned_move_id")
    @JsonProperty("origin_returned_move_id")
    private Integer originReturnedMoveId;

    /**
     * 作业类型
     */
    @DEField(name = "picking_type_id")
    @JSONField(name = "picking_type_id")
    @JsonProperty("picking_type_id")
    private Integer pickingTypeId;

    /**
     * 待加工的作业
     */
    @DEField(name = "operation_id")
    @JSONField(name = "operation_id")
    @JsonProperty("operation_id")
    private Integer operationId;

    /**
     * BOM行
     */
    @DEField(name = "bom_line_id")
    @JSONField(name = "bom_line_id")
    @JsonProperty("bom_line_id")
    private Integer bomLineId;

    /**
     * 维修
     */
    @DEField(name = "repair_id")
    @JSONField(name = "repair_id")
    @JsonProperty("repair_id")
    private Integer repairId;

    /**
     * 产品
     */
    @DEField(name = "product_id")
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Integer productId;

    /**
     * 拆卸顺序
     */
    @DEField(name = "unbuild_id")
    @JSONField(name = "unbuild_id")
    @JsonProperty("unbuild_id")
    private Integer unbuildId;

    /**
     * 成品的生产订单
     */
    @DEField(name = "production_id")
    @JSONField(name = "production_id")
    @JsonProperty("production_id")
    private Integer productionId;

    /**
     * 创建采购订单行
     */
    @DEField(name = "created_purchase_line_id")
    @JSONField(name = "created_purchase_line_id")
    @JsonProperty("created_purchase_line_id")
    private Integer createdPurchaseLineId;

    /**
     * 待消耗的工单
     */
    @DEField(name = "workorder_id")
    @JSONField(name = "workorder_id")
    @JsonProperty("workorder_id")
    private Integer workorderId;

    /**
     * 单位
     */
    @DEField(name = "product_uom")
    @JSONField(name = "product_uom")
    @JsonProperty("product_uom")
    private Integer productUom;

    /**
     * 调拨参照
     */
    @DEField(name = "picking_id")
    @JSONField(name = "picking_id")
    @JsonProperty("picking_id")
    private Integer pickingId;

    /**
     * 目的地地址
     */
    @DEField(name = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 包裹层级
     */
    @DEField(name = "package_level_id")
    @JSONField(name = "package_level_id")
    @JsonProperty("package_level_id")
    private Integer packageLevelId;

    /**
     * 首选包装
     */
    @DEField(name = "product_packaging")
    @JSONField(name = "product_packaging")
    @JsonProperty("product_packaging")
    private Integer productPackaging;

    /**
     * 所有者
     */
    @DEField(name = "restrict_partner_id")
    @JSONField(name = "restrict_partner_id")
    @JsonProperty("restrict_partner_id")
    private Integer restrictPartnerId;

    /**
     * 库存规则
     */
    @DEField(name = "rule_id")
    @JSONField(name = "rule_id")
    @JsonProperty("rule_id")
    private Integer ruleId;

    /**
     * 仓库
     */
    @DEField(name = "warehouse_id")
    @JSONField(name = "warehouse_id")
    @JsonProperty("warehouse_id")
    private Integer warehouseId;

    /**
     * 目的位置
     */
    @DEField(name = "location_dest_id")
    @JSONField(name = "location_dest_id")
    @JsonProperty("location_dest_id")
    private Integer locationDestId;

    /**
     * 原材料的生产订单
     */
    @DEField(name = "raw_material_production_id")
    @JSONField(name = "raw_material_production_id")
    @JsonProperty("raw_material_production_id")
    private Integer rawMaterialProductionId;

    /**
     * 库存
     */
    @DEField(name = "inventory_id")
    @JSONField(name = "inventory_id")
    @JsonProperty("inventory_id")
    private Integer inventoryId;

    /**
     * 采购订单行
     */
    @DEField(name = "purchase_line_id")
    @JSONField(name = "purchase_line_id")
    @JsonProperty("purchase_line_id")
    private Integer purchaseLineId;

    /**
     * 源位置
     */
    @DEField(name = "location_id")
    @JSONField(name = "location_id")
    @JsonProperty("location_id")
    private Integer locationId;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odoobomline")
    @JsonProperty("odoobomline")
    private cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_bom_line odooBomLine;

    /**
     * 
     */
    @JSONField(name = "odoocreatedproduction")
    @JsonProperty("odoocreatedproduction")
    private cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_production odooCreatedProduction;

    /**
     * 
     */
    @JSONField(name = "odooproduction")
    @JsonProperty("odooproduction")
    private cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_production odooProduction;

    /**
     * 
     */
    @JSONField(name = "odoorawmaterialproduction")
    @JsonProperty("odoorawmaterialproduction")
    private cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_production odooRawMaterialProduction;

    /**
     * 
     */
    @JSONField(name = "odoooperation")
    @JsonProperty("odoooperation")
    private cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_routing_workcenter odooOperation;

    /**
     * 
     */
    @JSONField(name = "odooconsumeunbuild")
    @JsonProperty("odooconsumeunbuild")
    private cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_unbuild odooConsumeUnbuild;

    /**
     * 
     */
    @JSONField(name = "odoounbuild")
    @JsonProperty("odoounbuild")
    private cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_unbuild odooUnbuild;

    /**
     * 
     */
    @JSONField(name = "odooworkorder")
    @JsonProperty("odooworkorder")
    private cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workorder odooWorkorder;

    /**
     * 
     */
    @JSONField(name = "odooproductpackaging")
    @JsonProperty("odooproductpackaging")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_packaging odooProductPackaging;

    /**
     * 
     */
    @JSONField(name = "odooproduct")
    @JsonProperty("odooproduct")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_product odooProduct;

    /**
     * 
     */
    @JSONField(name = "odoocreatedpurchaseline")
    @JsonProperty("odoocreatedpurchaseline")
    private cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_order_line odooCreatedPurchaseLine;

    /**
     * 
     */
    @JSONField(name = "odoopurchaseline")
    @JsonProperty("odoopurchaseline")
    private cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_order_line odooPurchaseLine;

    /**
     * 
     */
    @JSONField(name = "odoorepair")
    @JsonProperty("odoorepair")
    private cn.ibizlab.odoo.core.odoo_repair.domain.Repair_order odooRepair;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoopartner")
    @JsonProperty("odoopartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JSONField(name = "odoorestrictpartner")
    @JsonProperty("odoorestrictpartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooRestrictPartner;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoosaleline")
    @JsonProperty("odoosaleline")
    private cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_line odooSaleLine;

    /**
     * 
     */
    @JSONField(name = "odooinventory")
    @JsonProperty("odooinventory")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_inventory odooInventory;

    /**
     * 
     */
    @JSONField(name = "odoolocationdest")
    @JsonProperty("odoolocationdest")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location odooLocationDest;

    /**
     * 
     */
    @JSONField(name = "odoolocation")
    @JsonProperty("odoolocation")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location odooLocation;

    /**
     * 
     */
    @JSONField(name = "odoooriginreturnedmove")
    @JsonProperty("odoooriginreturnedmove")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_move odooOriginReturnedMove;

    /**
     * 
     */
    @JSONField(name = "odoopackagelevel")
    @JsonProperty("odoopackagelevel")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_package_level odooPackageLevel;

    /**
     * 
     */
    @JSONField(name = "odoopickingtype")
    @JsonProperty("odoopickingtype")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking_type odooPickingType;

    /**
     * 
     */
    @JSONField(name = "odoopicking")
    @JsonProperty("odoopicking")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking odooPicking;

    /**
     * 
     */
    @JSONField(name = "odoorule")
    @JsonProperty("odoorule")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_rule odooRule;

    /**
     * 
     */
    @JSONField(name = "odoowarehouse")
    @JsonProperty("odoowarehouse")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warehouse odooWarehouse;

    /**
     * 
     */
    @JSONField(name = "odooproductuom")
    @JsonProperty("odooproductuom")
    private cn.ibizlab.odoo.core.odoo_uom.domain.Uom_uom odooProductUom;




    /**
     * 设置 [在分拣确认后是否添加了移动]
     */
    public void setAdditional(String additional){
        this.additional = additional ;
        this.modify("additional",additional);
    }
    /**
     * 设置 [单价]
     */
    public void setPriceUnit(Double priceUnit){
        this.priceUnit = priceUnit ;
        this.modify("price_unit",priceUnit);
    }
    /**
     * 设置 [补货组]
     */
    public void setGroupId(Integer groupId){
        this.groupId = groupId ;
        this.modify("group_id",groupId);
    }
    /**
     * 设置 [退款 (更新 SO/PO)]
     */
    public void setToRefund(String toRefund){
        this.toRefund = toRefund ;
        this.modify("to_refund",toRefund);
    }
    /**
     * 设置 [单位因子]
     */
    public void setUnitFactor(Double unitFactor){
        this.unitFactor = unitFactor ;
        this.modify("unit_factor",unitFactor);
    }
    /**
     * 设置 [剩余数量]
     */
    public void setRemainingQty(Double remainingQty){
        this.remainingQty = remainingQty ;
        this.modify("remaining_qty",remainingQty);
    }
    /**
     * 设置 [传播取消以及拆分]
     */
    public void setPropagate(String propagate){
        this.propagate = propagate ;
        this.modify("propagate",propagate);
    }
    /**
     * 设置 [编号]
     */
    public void setReference(String reference){
        this.reference = reference ;
        this.modify("reference",reference);
    }
    /**
     * 设置 [实际数量]
     */
    public void setProductQty(Double productQty){
        this.productQty = productQty ;
        this.modify("product_qty",productQty);
    }
    /**
     * 设置 [源文档]
     */
    public void setOrigin(String origin){
        this.origin = origin ;
        this.modify("origin",origin);
    }
    /**
     * 设置 [初始需求]
     */
    public void setProductUomQty(Double productUomQty){
        this.productUomQty = productUomQty ;
        this.modify("product_uom_qty",productUomQty);
    }
    /**
     * 设置 [优先级]
     */
    public void setPriority(String priority){
        this.priority = priority ;
        this.modify("priority",priority);
    }
    /**
     * 设置 [日期]
     */
    public void setDate(Timestamp date){
        this.date = date ;
        this.modify("date",date);
    }
    /**
     * 设置 [剩余价值]
     */
    public void setRemainingValue(Double remainingValue){
        this.remainingValue = remainingValue ;
        this.modify("remaining_value",remainingValue);
    }
    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }
    /**
     * 设置 [备注]
     */
    public void setNote(String note){
        this.note = note ;
        this.modify("note",note);
    }
    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }
    /**
     * 设置 [说明]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [值]
     */
    public void setValue(Double value){
        this.value = value ;
        this.modify("value",value);
    }
    /**
     * 设置 [完成]
     */
    public void setIsDone(String isDone){
        this.isDone = isDone ;
        this.modify("is_done",isDone);
    }
    /**
     * 设置 [预计日期]
     */
    public void setDateExpected(Timestamp dateExpected){
        this.dateExpected = dateExpected ;
        this.modify("date_expected",dateExpected);
    }
    /**
     * 设置 [供应方法]
     */
    public void setProcureMethod(String procureMethod){
        this.procureMethod = procureMethod ;
        this.modify("procure_method",procureMethod);
    }
    /**
     * 设置 [创建生产订单]
     */
    public void setCreatedProductionId(Integer createdProductionId){
        this.createdProductionId = createdProductionId ;
        this.modify("created_production_id",createdProductionId);
    }
    /**
     * 设置 [拆卸单]
     */
    public void setConsumeUnbuildId(Integer consumeUnbuildId){
        this.consumeUnbuildId = consumeUnbuildId ;
        this.modify("consume_unbuild_id",consumeUnbuildId);
    }
    /**
     * 设置 [销售明细行]
     */
    public void setSaleLineId(Integer saleLineId){
        this.saleLineId = saleLineId ;
        this.modify("sale_line_id",saleLineId);
    }
    /**
     * 设置 [原始退回移动]
     */
    public void setOriginReturnedMoveId(Integer originReturnedMoveId){
        this.originReturnedMoveId = originReturnedMoveId ;
        this.modify("origin_returned_move_id",originReturnedMoveId);
    }
    /**
     * 设置 [作业类型]
     */
    public void setPickingTypeId(Integer pickingTypeId){
        this.pickingTypeId = pickingTypeId ;
        this.modify("picking_type_id",pickingTypeId);
    }
    /**
     * 设置 [待加工的作业]
     */
    public void setOperationId(Integer operationId){
        this.operationId = operationId ;
        this.modify("operation_id",operationId);
    }
    /**
     * 设置 [BOM行]
     */
    public void setBomLineId(Integer bomLineId){
        this.bomLineId = bomLineId ;
        this.modify("bom_line_id",bomLineId);
    }
    /**
     * 设置 [维修]
     */
    public void setRepairId(Integer repairId){
        this.repairId = repairId ;
        this.modify("repair_id",repairId);
    }
    /**
     * 设置 [产品]
     */
    public void setProductId(Integer productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }
    /**
     * 设置 [拆卸顺序]
     */
    public void setUnbuildId(Integer unbuildId){
        this.unbuildId = unbuildId ;
        this.modify("unbuild_id",unbuildId);
    }
    /**
     * 设置 [成品的生产订单]
     */
    public void setProductionId(Integer productionId){
        this.productionId = productionId ;
        this.modify("production_id",productionId);
    }
    /**
     * 设置 [创建采购订单行]
     */
    public void setCreatedPurchaseLineId(Integer createdPurchaseLineId){
        this.createdPurchaseLineId = createdPurchaseLineId ;
        this.modify("created_purchase_line_id",createdPurchaseLineId);
    }
    /**
     * 设置 [待消耗的工单]
     */
    public void setWorkorderId(Integer workorderId){
        this.workorderId = workorderId ;
        this.modify("workorder_id",workorderId);
    }
    /**
     * 设置 [单位]
     */
    public void setProductUom(Integer productUom){
        this.productUom = productUom ;
        this.modify("product_uom",productUom);
    }
    /**
     * 设置 [调拨参照]
     */
    public void setPickingId(Integer pickingId){
        this.pickingId = pickingId ;
        this.modify("picking_id",pickingId);
    }
    /**
     * 设置 [目的地地址]
     */
    public void setPartnerId(Integer partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }
    /**
     * 设置 [包裹层级]
     */
    public void setPackageLevelId(Integer packageLevelId){
        this.packageLevelId = packageLevelId ;
        this.modify("package_level_id",packageLevelId);
    }
    /**
     * 设置 [首选包装]
     */
    public void setProductPackaging(Integer productPackaging){
        this.productPackaging = productPackaging ;
        this.modify("product_packaging",productPackaging);
    }
    /**
     * 设置 [所有者]
     */
    public void setRestrictPartnerId(Integer restrictPartnerId){
        this.restrictPartnerId = restrictPartnerId ;
        this.modify("restrict_partner_id",restrictPartnerId);
    }
    /**
     * 设置 [库存规则]
     */
    public void setRuleId(Integer ruleId){
        this.ruleId = ruleId ;
        this.modify("rule_id",ruleId);
    }
    /**
     * 设置 [仓库]
     */
    public void setWarehouseId(Integer warehouseId){
        this.warehouseId = warehouseId ;
        this.modify("warehouse_id",warehouseId);
    }
    /**
     * 设置 [目的位置]
     */
    public void setLocationDestId(Integer locationDestId){
        this.locationDestId = locationDestId ;
        this.modify("location_dest_id",locationDestId);
    }
    /**
     * 设置 [原材料的生产订单]
     */
    public void setRawMaterialProductionId(Integer rawMaterialProductionId){
        this.rawMaterialProductionId = rawMaterialProductionId ;
        this.modify("raw_material_production_id",rawMaterialProductionId);
    }
    /**
     * 设置 [库存]
     */
    public void setInventoryId(Integer inventoryId){
        this.inventoryId = inventoryId ;
        this.modify("inventory_id",inventoryId);
    }
    /**
     * 设置 [采购订单行]
     */
    public void setPurchaseLineId(Integer purchaseLineId){
        this.purchaseLineId = purchaseLineId ;
        this.modify("purchase_line_id",purchaseLineId);
    }
    /**
     * 设置 [源位置]
     */
    public void setLocationId(Integer locationId){
        this.locationId = locationId ;
        this.modify("location_id",locationId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

}


