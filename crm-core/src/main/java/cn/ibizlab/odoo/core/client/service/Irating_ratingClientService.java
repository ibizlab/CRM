package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Irating_rating;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[rating_rating] 服务对象接口
 */
public interface Irating_ratingClientService{

    public Irating_rating createModel() ;

    public void get(Irating_rating rating_rating);

    public void update(Irating_rating rating_rating);

    public void create(Irating_rating rating_rating);

    public void remove(Irating_rating rating_rating);

    public void updateBatch(List<Irating_rating> rating_ratings);

    public void removeBatch(List<Irating_rating> rating_ratings);

    public Page<Irating_rating> fetchDefault(SearchContext context);

    public void createBatch(List<Irating_rating> rating_ratings);

    public Page<Irating_rating> select(SearchContext context);

    public void getDraft(Irating_rating rating_rating);

}
