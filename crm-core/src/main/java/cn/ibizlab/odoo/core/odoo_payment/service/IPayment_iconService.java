package cn.ibizlab.odoo.core.odoo_payment.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_icon;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_iconSearchContext;


/**
 * 实体[Payment_icon] 服务对象接口
 */
public interface IPayment_iconService{

    boolean update(Payment_icon et) ;
    void updateBatch(List<Payment_icon> list) ;
    Payment_icon getDraft(Payment_icon et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Payment_icon get(Integer key) ;
    boolean create(Payment_icon et) ;
    void createBatch(List<Payment_icon> list) ;
    Page<Payment_icon> searchDefault(Payment_iconSearchContext context) ;

}



