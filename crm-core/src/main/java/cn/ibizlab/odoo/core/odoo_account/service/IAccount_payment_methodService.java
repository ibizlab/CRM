package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_method;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_payment_methodSearchContext;


/**
 * 实体[Account_payment_method] 服务对象接口
 */
public interface IAccount_payment_methodService{

    Account_payment_method getDraft(Account_payment_method et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Account_payment_method et) ;
    void updateBatch(List<Account_payment_method> list) ;
    boolean create(Account_payment_method et) ;
    void createBatch(List<Account_payment_method> list) ;
    Account_payment_method get(Integer key) ;
    Page<Account_payment_method> searchDefault(Account_payment_methodSearchContext context) ;

}



