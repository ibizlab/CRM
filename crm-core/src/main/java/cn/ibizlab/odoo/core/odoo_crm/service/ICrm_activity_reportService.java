package cn.ibizlab.odoo.core.odoo_crm.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_activity_report;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_activity_reportSearchContext;


/**
 * 实体[Crm_activity_report] 服务对象接口
 */
public interface ICrm_activity_reportService{

    boolean create(Crm_activity_report et) ;
    void createBatch(List<Crm_activity_report> list) ;
    boolean update(Crm_activity_report et) ;
    void updateBatch(List<Crm_activity_report> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Crm_activity_report get(Integer key) ;
    Crm_activity_report getDraft(Crm_activity_report et) ;
    Page<Crm_activity_report> searchDefault(Crm_activity_reportSearchContext context) ;

}



