package cn.ibizlab.odoo.core.odoo_base.valuerule.anno.res_users;

import cn.ibizlab.odoo.core.odoo_base.valuerule.validator.res_users.Res_usersPurchase_warn_msgDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Res_users
 * 属性：Purchase_warn_msg
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Res_usersPurchase_warn_msgDefaultValidator.class})
public @interface Res_usersPurchase_warn_msgDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[1048576]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
