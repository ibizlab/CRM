package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_tax_group;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_tax_groupSearchContext;


/**
 * 实体[Account_tax_group] 服务对象接口
 */
public interface IAccount_tax_groupService{

    boolean update(Account_tax_group et) ;
    void updateBatch(List<Account_tax_group> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Account_tax_group get(Integer key) ;
    Account_tax_group getDraft(Account_tax_group et) ;
    boolean create(Account_tax_group et) ;
    void createBatch(List<Account_tax_group> list) ;
    Page<Account_tax_group> searchDefault(Account_tax_groupSearchContext context) ;

}



