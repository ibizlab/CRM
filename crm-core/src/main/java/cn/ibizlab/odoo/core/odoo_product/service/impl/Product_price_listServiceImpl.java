package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_price_list;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_price_listSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_price_listService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_product.client.product_price_listFeignClient;

/**
 * 实体[基于价格列表版本的单位产品价格] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_price_listServiceImpl implements IProduct_price_listService {

    @Autowired
    product_price_listFeignClient product_price_listFeignClient;


    @Override
    public boolean remove(Integer id) {
        boolean result=product_price_listFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        product_price_listFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Product_price_list et) {
        Product_price_list rt = product_price_listFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Product_price_list> list){
        product_price_listFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Product_price_list et) {
        Product_price_list rt = product_price_listFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_price_list> list){
        product_price_listFeignClient.createBatch(list) ;
    }

    @Override
    public Product_price_list get(Integer id) {
		Product_price_list et=product_price_listFeignClient.get(id);
        if(et==null){
            et=new Product_price_list();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public Product_price_list getDraft(Product_price_list et) {
        et=product_price_listFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_price_list> searchDefault(Product_price_listSearchContext context) {
        Page<Product_price_list> product_price_lists=product_price_listFeignClient.searchDefault(context);
        return product_price_lists;
    }


}


