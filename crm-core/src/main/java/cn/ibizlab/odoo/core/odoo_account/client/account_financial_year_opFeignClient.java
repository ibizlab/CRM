package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_financial_year_op;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_financial_year_opSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_financial_year_op] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-financial-year-op", fallback = account_financial_year_opFallback.class)
public interface account_financial_year_opFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_financial_year_ops/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_financial_year_ops/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);






    @RequestMapping(method = RequestMethod.POST, value = "/account_financial_year_ops/searchdefault")
    Page<Account_financial_year_op> searchDefault(@RequestBody Account_financial_year_opSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/account_financial_year_ops/{id}")
    Account_financial_year_op get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_financial_year_ops/{id}")
    Account_financial_year_op update(@PathVariable("id") Integer id,@RequestBody Account_financial_year_op account_financial_year_op);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_financial_year_ops/batch")
    Boolean updateBatch(@RequestBody List<Account_financial_year_op> account_financial_year_ops);


    @RequestMapping(method = RequestMethod.POST, value = "/account_financial_year_ops")
    Account_financial_year_op create(@RequestBody Account_financial_year_op account_financial_year_op);

    @RequestMapping(method = RequestMethod.POST, value = "/account_financial_year_ops/batch")
    Boolean createBatch(@RequestBody List<Account_financial_year_op> account_financial_year_ops);


    @RequestMapping(method = RequestMethod.GET, value = "/account_financial_year_ops/select")
    Page<Account_financial_year_op> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_financial_year_ops/getdraft")
    Account_financial_year_op getDraft();


}
