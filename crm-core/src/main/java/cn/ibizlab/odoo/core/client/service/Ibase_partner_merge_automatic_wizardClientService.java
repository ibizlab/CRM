package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ibase_partner_merge_automatic_wizard;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_partner_merge_automatic_wizard] 服务对象接口
 */
public interface Ibase_partner_merge_automatic_wizardClientService{

    public Ibase_partner_merge_automatic_wizard createModel() ;

    public void remove(Ibase_partner_merge_automatic_wizard base_partner_merge_automatic_wizard);

    public Page<Ibase_partner_merge_automatic_wizard> fetchDefault(SearchContext context);

    public void update(Ibase_partner_merge_automatic_wizard base_partner_merge_automatic_wizard);

    public void createBatch(List<Ibase_partner_merge_automatic_wizard> base_partner_merge_automatic_wizards);

    public void updateBatch(List<Ibase_partner_merge_automatic_wizard> base_partner_merge_automatic_wizards);

    public void get(Ibase_partner_merge_automatic_wizard base_partner_merge_automatic_wizard);

    public void create(Ibase_partner_merge_automatic_wizard base_partner_merge_automatic_wizard);

    public void removeBatch(List<Ibase_partner_merge_automatic_wizard> base_partner_merge_automatic_wizards);

    public Page<Ibase_partner_merge_automatic_wizard> select(SearchContext context);

    public void getDraft(Ibase_partner_merge_automatic_wizard base_partner_merge_automatic_wizard);

}
