package cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_rule;

import cn.ibizlab.odoo.core.odoo_stock.valuerule.validator.stock_rule.Stock_ruleWarehouse_idDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Stock_rule
 * 属性：Warehouse_id
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Stock_ruleWarehouse_idDefaultValidator.class})
public @interface Stock_ruleWarehouse_idDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
