package cn.ibizlab.odoo.core.odoo_website.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_website.domain.Website_seo_metadata;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_seo_metadataSearchContext;


/**
 * 实体[Website_seo_metadata] 服务对象接口
 */
public interface IWebsite_seo_metadataService{

    boolean create(Website_seo_metadata et) ;
    void createBatch(List<Website_seo_metadata> list) ;
    boolean update(Website_seo_metadata et) ;
    void updateBatch(List<Website_seo_metadata> list) ;
    Website_seo_metadata get(Integer key) ;
    Website_seo_metadata getDraft(Website_seo_metadata et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Website_seo_metadata> searchDefault(Website_seo_metadataSearchContext context) ;

}



