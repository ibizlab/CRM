package cn.ibizlab.odoo.core.odoo_sms.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_sms.domain.Sms_api;
import cn.ibizlab.odoo.core.odoo_sms.filter.Sms_apiSearchContext;


/**
 * 实体[Sms_api] 服务对象接口
 */
public interface ISms_apiService{

    Sms_api get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Sms_api getDraft(Sms_api et) ;
    boolean create(Sms_api et) ;
    void createBatch(List<Sms_api> list) ;
    boolean update(Sms_api et) ;
    void updateBatch(List<Sms_api> list) ;
    Page<Sms_api> searchDefault(Sms_apiSearchContext context) ;

}



