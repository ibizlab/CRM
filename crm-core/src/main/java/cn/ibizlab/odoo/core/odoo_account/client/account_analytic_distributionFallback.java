package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_distribution;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_distributionSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_analytic_distribution] 服务对象接口
 */
@Component
public class account_analytic_distributionFallback implements account_analytic_distributionFeignClient{



    public Page<Account_analytic_distribution> searchDefault(Account_analytic_distributionSearchContext context){
            return null;
     }


    public Account_analytic_distribution create(Account_analytic_distribution account_analytic_distribution){
            return null;
     }
    public Boolean createBatch(List<Account_analytic_distribution> account_analytic_distributions){
            return false;
     }

    public Account_analytic_distribution update(Integer id, Account_analytic_distribution account_analytic_distribution){
            return null;
     }
    public Boolean updateBatch(List<Account_analytic_distribution> account_analytic_distributions){
            return false;
     }



    public Account_analytic_distribution get(Integer id){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Account_analytic_distribution> select(){
            return null;
     }

    public Account_analytic_distribution getDraft(){
            return null;
    }



}
