package cn.ibizlab.odoo.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_attribute;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_attributeSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[product_attribute] 服务对象接口
 */
@FeignClient(value = "odoo-product", contextId = "product-attribute", fallback = product_attributeFallback.class)
public interface product_attributeFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/product_attributes/{id}")
    Product_attribute get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.DELETE, value = "/product_attributes/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/product_attributes/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/product_attributes/searchdefault")
    Page<Product_attribute> searchDefault(@RequestBody Product_attributeSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/product_attributes/{id}")
    Product_attribute update(@PathVariable("id") Integer id,@RequestBody Product_attribute product_attribute);

    @RequestMapping(method = RequestMethod.PUT, value = "/product_attributes/batch")
    Boolean updateBatch(@RequestBody List<Product_attribute> product_attributes);



    @RequestMapping(method = RequestMethod.POST, value = "/product_attributes")
    Product_attribute create(@RequestBody Product_attribute product_attribute);

    @RequestMapping(method = RequestMethod.POST, value = "/product_attributes/batch")
    Boolean createBatch(@RequestBody List<Product_attribute> product_attributes);



    @RequestMapping(method = RequestMethod.GET, value = "/product_attributes/select")
    Page<Product_attribute> select();


    @RequestMapping(method = RequestMethod.GET, value = "/product_attributes/getdraft")
    Product_attribute getDraft();


}
