package cn.ibizlab.odoo.core.odoo_hr.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [Contract] 对象
 */
@Data
public class Hr_contract extends EntityClient implements Serializable {

    /**
     * 下一活动截止日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 结束日期
     */
    @DEField(name = "date_end")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_end" , format="yyyy-MM-dd")
    @JsonProperty("date_end")
    private Timestamp dateEnd;

    /**
     * 补贴
     */
    @JSONField(name = "advantages")
    @JsonProperty("advantages")
    private String advantages;

    /**
     * 行动数量
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 备注
     */
    @JSONField(name = "notes")
    @JsonProperty("notes")
    private String notes;

    /**
     * 关注者
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 下一活动摘要
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;

    /**
     * 关注者(渠道)
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 未读消息
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 合同参考
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 工资
     */
    @JSONField(name = "wage")
    @JsonProperty("wage")
    private Double wage;

    /**
     * 社交管理中心
     */
    @DEField(name = "reported_to_secretariat")
    @JSONField(name = "reported_to_secretariat")
    @JsonProperty("reported_to_secretariat")
    private String reportedToSecretariat;

    /**
     * 开始日期
     */
    @DEField(name = "date_start")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_start" , format="yyyy-MM-dd")
    @JsonProperty("date_start")
    private Timestamp dateStart;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 活动状态
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;

    /**
     * 信息
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 网站消息
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 附件数量
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 试用期结束
     */
    @DEField(name = "trial_date_end")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "trial_date_end" , format="yyyy-MM-dd")
    @JsonProperty("trial_date_end")
    private Timestamp trialDateEnd;

    /**
     * 是关注者
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 活动
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;

    /**
     * 关注者(业务伙伴)
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 消息递送错误
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 未读消息计数器
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 责任用户
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 需要采取行动
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 错误数
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 下一活动类型
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 工作许可编号
     */
    @JSONField(name = "permit_no")
    @JsonProperty("permit_no")
    private String permitNo;

    /**
     * 工作安排
     */
    @JSONField(name = "resource_calendar_id_text")
    @JsonProperty("resource_calendar_id_text")
    private String resourceCalendarIdText;

    /**
     * 部门
     */
    @JSONField(name = "department_id_text")
    @JsonProperty("department_id_text")
    private String departmentIdText;

    /**
     * 工作岗位
     */
    @JSONField(name = "job_id_text")
    @JsonProperty("job_id_text")
    private String jobIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 币种
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 员工
     */
    @JSONField(name = "employee_id_text")
    @JsonProperty("employee_id_text")
    private String employeeIdText;

    /**
     * 员工类别
     */
    @JSONField(name = "type_id_text")
    @JsonProperty("type_id_text")
    private String typeIdText;

    /**
     * 签证到期日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "visa_expire" , format="yyyy-MM-dd")
    @JsonProperty("visa_expire")
    private Timestamp visaExpire;

    /**
     * 签证号
     */
    @JSONField(name = "visa_no")
    @JsonProperty("visa_no")
    private String visaNo;

    /**
     * 工作安排
     */
    @DEField(name = "resource_calendar_id")
    @JSONField(name = "resource_calendar_id")
    @JsonProperty("resource_calendar_id")
    private Integer resourceCalendarId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 员工
     */
    @DEField(name = "employee_id")
    @JSONField(name = "employee_id")
    @JsonProperty("employee_id")
    private Integer employeeId;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 工作岗位
     */
    @DEField(name = "job_id")
    @JSONField(name = "job_id")
    @JsonProperty("job_id")
    private Integer jobId;

    /**
     * 部门
     */
    @DEField(name = "department_id")
    @JSONField(name = "department_id")
    @JsonProperty("department_id")
    private Integer departmentId;

    /**
     * 员工类别
     */
    @DEField(name = "type_id")
    @JSONField(name = "type_id")
    @JsonProperty("type_id")
    private Integer typeId;


    /**
     * 
     */
    @JSONField(name = "odootype")
    @JsonProperty("odootype")
    private cn.ibizlab.odoo.core.odoo_hr.domain.Hr_contract_type odooType;

    /**
     * 
     */
    @JSONField(name = "odoodepartment")
    @JsonProperty("odoodepartment")
    private cn.ibizlab.odoo.core.odoo_hr.domain.Hr_department odooDepartment;

    /**
     * 
     */
    @JSONField(name = "odooemployee")
    @JsonProperty("odooemployee")
    private cn.ibizlab.odoo.core.odoo_hr.domain.Hr_employee odooEmployee;

    /**
     * 
     */
    @JSONField(name = "odoojob")
    @JsonProperty("odoojob")
    private cn.ibizlab.odoo.core.odoo_hr.domain.Hr_job odooJob;

    /**
     * 
     */
    @JSONField(name = "odooresourcecalendar")
    @JsonProperty("odooresourcecalendar")
    private cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar odooResourceCalendar;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [结束日期]
     */
    public void setDateEnd(Timestamp dateEnd){
        this.dateEnd = dateEnd ;
        this.modify("date_end",dateEnd);
    }
    /**
     * 设置 [补贴]
     */
    public void setAdvantages(String advantages){
        this.advantages = advantages ;
        this.modify("advantages",advantages);
    }
    /**
     * 设置 [备注]
     */
    public void setNotes(String notes){
        this.notes = notes ;
        this.modify("notes",notes);
    }
    /**
     * 设置 [合同参考]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [工资]
     */
    public void setWage(Double wage){
        this.wage = wage ;
        this.modify("wage",wage);
    }
    /**
     * 设置 [社交管理中心]
     */
    public void setReportedToSecretariat(String reportedToSecretariat){
        this.reportedToSecretariat = reportedToSecretariat ;
        this.modify("reported_to_secretariat",reportedToSecretariat);
    }
    /**
     * 设置 [开始日期]
     */
    public void setDateStart(Timestamp dateStart){
        this.dateStart = dateStart ;
        this.modify("date_start",dateStart);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }
    /**
     * 设置 [试用期结束]
     */
    public void setTrialDateEnd(Timestamp trialDateEnd){
        this.trialDateEnd = trialDateEnd ;
        this.modify("trial_date_end",trialDateEnd);
    }
    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }
    /**
     * 设置 [工作安排]
     */
    public void setResourceCalendarId(Integer resourceCalendarId){
        this.resourceCalendarId = resourceCalendarId ;
        this.modify("resource_calendar_id",resourceCalendarId);
    }
    /**
     * 设置 [员工]
     */
    public void setEmployeeId(Integer employeeId){
        this.employeeId = employeeId ;
        this.modify("employee_id",employeeId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [工作岗位]
     */
    public void setJobId(Integer jobId){
        this.jobId = jobId ;
        this.modify("job_id",jobId);
    }
    /**
     * 设置 [部门]
     */
    public void setDepartmentId(Integer departmentId){
        this.departmentId = departmentId ;
        this.modify("department_id",departmentId);
    }
    /**
     * 设置 [员工类别]
     */
    public void setTypeId(Integer typeId){
        this.typeId = typeId ;
        this.modify("type_id",typeId);
    }

}


