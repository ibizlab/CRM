package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_replenish;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_replenishSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_replenishService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_product.client.product_replenishFeignClient;

/**
 * 实体[补料] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_replenishServiceImpl implements IProduct_replenishService {

    @Autowired
    product_replenishFeignClient product_replenishFeignClient;


    @Override
    public Product_replenish get(Integer id) {
		Product_replenish et=product_replenishFeignClient.get(id);
        if(et==null){
            et=new Product_replenish();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=product_replenishFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        product_replenishFeignClient.removeBatch(idList);
    }

    @Override
    public boolean create(Product_replenish et) {
        Product_replenish rt = product_replenishFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_replenish> list){
        product_replenishFeignClient.createBatch(list) ;
    }

    @Override
    public Product_replenish getDraft(Product_replenish et) {
        et=product_replenishFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Product_replenish et) {
        Product_replenish rt = product_replenishFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Product_replenish> list){
        product_replenishFeignClient.updateBatch(list) ;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_replenish> searchDefault(Product_replenishSearchContext context) {
        Page<Product_replenish> product_replenishs=product_replenishFeignClient.searchDefault(context);
        return product_replenishs;
    }


}


