package cn.ibizlab.odoo.core.odoo_mail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [群发邮件] 对象
 */
@Data
public class Mail_mass_mailing extends EntityClient implements Serializable {

    /**
     * 发送日期
     */
    @DEField(name = "sent_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "sent_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("sent_date")
    private Timestamp sentDate;

    /**
     * 报价个数
     */
    @JSONField(name = "sale_quotation_count")
    @JsonProperty("sale_quotation_count")
    private Integer saleQuotationCount;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 域
     */
    @DEField(name = "mailing_domain")
    @JSONField(name = "mailing_domain")
    @JsonProperty("mailing_domain")
    private String mailingDomain;

    /**
     * 已开启
     */
    @JSONField(name = "opened")
    @JsonProperty("opened")
    private Integer opened;

    /**
     * 回复
     */
    @DEField(name = "reply_to")
    @JSONField(name = "reply_to")
    @JsonProperty("reply_to")
    private String replyTo;

    /**
     * 已回复
     */
    @JSONField(name = "replied")
    @JsonProperty("replied")
    private Integer replied;

    /**
     * 线索总数
     */
    @JSONField(name = "crm_lead_count")
    @JsonProperty("crm_lead_count")
    private Integer crmLeadCount;

    /**
     * 收件人实物模型
     */
    @JSONField(name = "mailing_model_real")
    @JsonProperty("mailing_model_real")
    private String mailingModelReal;

    /**
     * 点击数
     */
    @JSONField(name = "clicks_ratio")
    @JsonProperty("clicks_ratio")
    private Integer clicksRatio;

    /**
     * 已送货
     */
    @JSONField(name = "delivered")
    @JsonProperty("delivered")
    private Integer delivered;

    /**
     * 收件人模型
     */
    @DEField(name = "mailing_model_id")
    @JSONField(name = "mailing_model_id")
    @JsonProperty("mailing_model_id")
    private Integer mailingModelId;

    /**
     * 回复模式
     */
    @DEField(name = "reply_to_mode")
    @JSONField(name = "reply_to_mode")
    @JsonProperty("reply_to_mode")
    private String replyToMode;

    /**
     * 在将来计划
     */
    @DEField(name = "schedule_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "schedule_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("schedule_date")
    private Timestamp scheduleDate;

    /**
     * 从
     */
    @DEField(name = "email_from")
    @JSONField(name = "email_from")
    @JsonProperty("email_from")
    private String emailFrom;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 开票金额
     */
    @JSONField(name = "sale_invoiced_amount")
    @JsonProperty("sale_invoiced_amount")
    private Integer saleInvoicedAmount;

    /**
     * 附件
     */
    @JSONField(name = "attachment_ids")
    @JsonProperty("attachment_ids")
    private String attachmentIds;

    /**
     * 总计
     */
    @JSONField(name = "total")
    @JsonProperty("total")
    private Integer total;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 被退回的比率
     */
    @JSONField(name = "bounced_ratio")
    @JsonProperty("bounced_ratio")
    private Integer bouncedRatio;

    /**
     * 安排的日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "next_departure" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("next_departure")
    private Timestamp nextDeparture;

    /**
     * 邮件服务器
     */
    @DEField(name = "mail_server_id")
    @JSONField(name = "mail_server_id")
    @JsonProperty("mail_server_id")
    private Integer mailServerId;

    /**
     * 忽略
     */
    @JSONField(name = "ignored")
    @JsonProperty("ignored")
    private Integer ignored;

    /**
     * 安排
     */
    @JSONField(name = "scheduled")
    @JsonProperty("scheduled")
    private Integer scheduled;

    /**
     * 使用线索
     */
    @JSONField(name = "crm_lead_activated")
    @JsonProperty("crm_lead_activated")
    private String crmLeadActivated;

    /**
     * 点击率
     */
    @JSONField(name = "clicked")
    @JsonProperty("clicked")
    private Integer clicked;

    /**
     * 回复比例
     */
    @JSONField(name = "replied_ratio")
    @JsonProperty("replied_ratio")
    private Integer repliedRatio;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 邮件统计
     */
    @JSONField(name = "statistics_ids")
    @JsonProperty("statistics_ids")
    private String statisticsIds;

    /**
     * 打开比例
     */
    @JSONField(name = "opened_ratio")
    @JsonProperty("opened_ratio")
    private Integer openedRatio;

    /**
     * 正文
     */
    @DEField(name = "body_html")
    @JSONField(name = "body_html")
    @JsonProperty("body_html")
    private String bodyHtml;

    /**
     * 被退回
     */
    @JSONField(name = "bounced")
    @JsonProperty("bounced")
    private Integer bounced;

    /**
     * 颜色索引
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 收件人模型
     */
    @JSONField(name = "mailing_model_name")
    @JsonProperty("mailing_model_name")
    private String mailingModelName;

    /**
     * 邮件列表
     */
    @JSONField(name = "contact_list_ids")
    @JsonProperty("contact_list_ids")
    private String contactListIds;

    /**
     * 预期
     */
    @JSONField(name = "expected")
    @JsonProperty("expected")
    private Integer expected;

    /**
     * 失败的
     */
    @JSONField(name = "failed")
    @JsonProperty("failed")
    private Integer failed;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 已接收比例
     */
    @JSONField(name = "received_ratio")
    @JsonProperty("received_ratio")
    private Integer receivedRatio;

    /**
     * 商机个数
     */
    @JSONField(name = "crm_opportunities_count")
    @JsonProperty("crm_opportunities_count")
    private Integer crmOpportunitiesCount;

    /**
     * 归档
     */
    @DEField(name = "keep_archives")
    @JSONField(name = "keep_archives")
    @JsonProperty("keep_archives")
    private String keepArchives;

    /**
     * A/B 测试百分比
     */
    @DEField(name = "contact_ab_pc")
    @JSONField(name = "contact_ab_pc")
    @JsonProperty("contact_ab_pc")
    private Integer contactAbPc;

    /**
     * 已汇
     */
    @JSONField(name = "sent")
    @JsonProperty("sent")
    private Integer sent;

    /**
     * 营销
     */
    @JSONField(name = "campaign_id_text")
    @JsonProperty("campaign_id_text")
    private String campaignIdText;

    /**
     * 邮件管理器
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 来源名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 群发邮件营销
     */
    @JSONField(name = "mass_mailing_campaign_id_text")
    @JsonProperty("mass_mailing_campaign_id_text")
    private String massMailingCampaignIdText;

    /**
     * 媒体
     */
    @JSONField(name = "medium_id_text")
    @JsonProperty("medium_id_text")
    private String mediumIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 邮件管理器
     */
    @DEField(name = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;

    /**
     * 群发邮件营销
     */
    @DEField(name = "mass_mailing_campaign_id")
    @JSONField(name = "mass_mailing_campaign_id")
    @JsonProperty("mass_mailing_campaign_id")
    private Integer massMailingCampaignId;

    /**
     * 营销
     */
    @DEField(name = "campaign_id")
    @JSONField(name = "campaign_id")
    @JsonProperty("campaign_id")
    private Integer campaignId;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 媒体
     */
    @DEField(name = "medium_id")
    @JSONField(name = "medium_id")
    @JsonProperty("medium_id")
    private Integer mediumId;

    /**
     * 主题
     */
    @DEField(name = "source_id")
    @JSONField(name = "source_id")
    @JsonProperty("source_id")
    private Integer sourceId;


    /**
     * 
     */
    @JSONField(name = "odoomassmailingcampaign")
    @JsonProperty("odoomassmailingcampaign")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_campaign odooMassMailingCampaign;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoouser")
    @JsonProperty("odoouser")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoocampaign")
    @JsonProperty("odoocampaign")
    private cn.ibizlab.odoo.core.odoo_utm.domain.Utm_campaign odooCampaign;

    /**
     * 
     */
    @JSONField(name = "odoomedium")
    @JsonProperty("odoomedium")
    private cn.ibizlab.odoo.core.odoo_utm.domain.Utm_medium odooMedium;

    /**
     * 
     */
    @JSONField(name = "odoosource")
    @JsonProperty("odoosource")
    private cn.ibizlab.odoo.core.odoo_utm.domain.Utm_source odooSource;




    /**
     * 设置 [发送日期]
     */
    public void setSentDate(Timestamp sentDate){
        this.sentDate = sentDate ;
        this.modify("sent_date",sentDate);
    }
    /**
     * 设置 [域]
     */
    public void setMailingDomain(String mailingDomain){
        this.mailingDomain = mailingDomain ;
        this.modify("mailing_domain",mailingDomain);
    }
    /**
     * 设置 [回复]
     */
    public void setReplyTo(String replyTo){
        this.replyTo = replyTo ;
        this.modify("reply_to",replyTo);
    }
    /**
     * 设置 [收件人模型]
     */
    public void setMailingModelId(Integer mailingModelId){
        this.mailingModelId = mailingModelId ;
        this.modify("mailing_model_id",mailingModelId);
    }
    /**
     * 设置 [回复模式]
     */
    public void setReplyToMode(String replyToMode){
        this.replyToMode = replyToMode ;
        this.modify("reply_to_mode",replyToMode);
    }
    /**
     * 设置 [在将来计划]
     */
    public void setScheduleDate(Timestamp scheduleDate){
        this.scheduleDate = scheduleDate ;
        this.modify("schedule_date",scheduleDate);
    }
    /**
     * 设置 [从]
     */
    public void setEmailFrom(String emailFrom){
        this.emailFrom = emailFrom ;
        this.modify("email_from",emailFrom);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [邮件服务器]
     */
    public void setMailServerId(Integer mailServerId){
        this.mailServerId = mailServerId ;
        this.modify("mail_server_id",mailServerId);
    }
    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }
    /**
     * 设置 [正文]
     */
    public void setBodyHtml(String bodyHtml){
        this.bodyHtml = bodyHtml ;
        this.modify("body_html",bodyHtml);
    }
    /**
     * 设置 [颜色索引]
     */
    public void setColor(Integer color){
        this.color = color ;
        this.modify("color",color);
    }
    /**
     * 设置 [归档]
     */
    public void setKeepArchives(String keepArchives){
        this.keepArchives = keepArchives ;
        this.modify("keep_archives",keepArchives);
    }
    /**
     * 设置 [A/B 测试百分比]
     */
    public void setContactAbPc(Integer contactAbPc){
        this.contactAbPc = contactAbPc ;
        this.modify("contact_ab_pc",contactAbPc);
    }
    /**
     * 设置 [邮件管理器]
     */
    public void setUserId(Integer userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }
    /**
     * 设置 [群发邮件营销]
     */
    public void setMassMailingCampaignId(Integer massMailingCampaignId){
        this.massMailingCampaignId = massMailingCampaignId ;
        this.modify("mass_mailing_campaign_id",massMailingCampaignId);
    }
    /**
     * 设置 [营销]
     */
    public void setCampaignId(Integer campaignId){
        this.campaignId = campaignId ;
        this.modify("campaign_id",campaignId);
    }
    /**
     * 设置 [媒体]
     */
    public void setMediumId(Integer mediumId){
        this.mediumId = mediumId ;
        this.modify("medium_id",mediumId);
    }
    /**
     * 设置 [主题]
     */
    public void setSourceId(Integer sourceId){
        this.sourceId = sourceId ;
        this.modify("source_id",sourceId);
    }

}


