package cn.ibizlab.odoo.core.odoo_portal.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_share;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_shareSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[portal_share] 服务对象接口
 */
@Component
public class portal_shareFallback implements portal_shareFeignClient{

    public Portal_share create(Portal_share portal_share){
            return null;
     }
    public Boolean createBatch(List<Portal_share> portal_shares){
            return false;
     }

    public Portal_share get(Integer id){
            return null;
     }





    public Portal_share update(Integer id, Portal_share portal_share){
            return null;
     }
    public Boolean updateBatch(List<Portal_share> portal_shares){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Portal_share> searchDefault(Portal_shareSearchContext context){
            return null;
     }


    public Page<Portal_share> select(){
            return null;
     }

    public Portal_share getDraft(){
            return null;
    }



}
