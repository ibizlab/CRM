package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_list;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_listSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_mass_mailing_list] 服务对象接口
 */
@Component
public class mail_mass_mailing_listFallback implements mail_mass_mailing_listFeignClient{

    public Mail_mass_mailing_list update(Integer id, Mail_mass_mailing_list mail_mass_mailing_list){
            return null;
     }
    public Boolean updateBatch(List<Mail_mass_mailing_list> mail_mass_mailing_lists){
            return false;
     }


    public Mail_mass_mailing_list create(Mail_mass_mailing_list mail_mass_mailing_list){
            return null;
     }
    public Boolean createBatch(List<Mail_mass_mailing_list> mail_mass_mailing_lists){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Mail_mass_mailing_list> searchDefault(Mail_mass_mailing_listSearchContext context){
            return null;
     }




    public Mail_mass_mailing_list get(Integer id){
            return null;
     }


    public Page<Mail_mass_mailing_list> select(){
            return null;
     }

    public Mail_mass_mailing_list getDraft(){
            return null;
    }



}
