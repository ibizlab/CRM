package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_group;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_groupSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_groupService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_account.client.account_groupFeignClient;

/**
 * 实体[科目组] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_groupServiceImpl implements IAccount_groupService {

    @Autowired
    account_groupFeignClient account_groupFeignClient;


    @Override
    public Account_group get(Integer id) {
		Account_group et=account_groupFeignClient.get(id);
        if(et==null){
            et=new Account_group();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=account_groupFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        account_groupFeignClient.removeBatch(idList);
    }

    @Override
    public boolean update(Account_group et) {
        Account_group rt = account_groupFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Account_group> list){
        account_groupFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean create(Account_group et) {
        Account_group rt = account_groupFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_group> list){
        account_groupFeignClient.createBatch(list) ;
    }

    @Override
    public Account_group getDraft(Account_group et) {
        et=account_groupFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_group> searchDefault(Account_groupSearchContext context) {
        Page<Account_group> account_groups=account_groupFeignClient.searchDefault(context);
        return account_groups;
    }


}


