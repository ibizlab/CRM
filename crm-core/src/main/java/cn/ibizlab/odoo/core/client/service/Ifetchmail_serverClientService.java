package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ifetchmail_server;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[fetchmail_server] 服务对象接口
 */
public interface Ifetchmail_serverClientService{

    public Ifetchmail_server createModel() ;

    public void remove(Ifetchmail_server fetchmail_server);

    public void createBatch(List<Ifetchmail_server> fetchmail_servers);

    public void create(Ifetchmail_server fetchmail_server);

    public void update(Ifetchmail_server fetchmail_server);

    public Page<Ifetchmail_server> fetchDefault(SearchContext context);

    public void removeBatch(List<Ifetchmail_server> fetchmail_servers);

    public void updateBatch(List<Ifetchmail_server> fetchmail_servers);

    public void get(Ifetchmail_server fetchmail_server);

    public Page<Ifetchmail_server> select(SearchContext context);

    public void getDraft(Ifetchmail_server fetchmail_server);

}
