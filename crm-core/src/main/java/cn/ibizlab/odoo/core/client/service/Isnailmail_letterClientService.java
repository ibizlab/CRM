package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Isnailmail_letter;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[snailmail_letter] 服务对象接口
 */
public interface Isnailmail_letterClientService{

    public Isnailmail_letter createModel() ;

    public void create(Isnailmail_letter snailmail_letter);

    public void get(Isnailmail_letter snailmail_letter);

    public void update(Isnailmail_letter snailmail_letter);

    public void remove(Isnailmail_letter snailmail_letter);

    public Page<Isnailmail_letter> fetchDefault(SearchContext context);

    public void removeBatch(List<Isnailmail_letter> snailmail_letters);

    public void createBatch(List<Isnailmail_letter> snailmail_letters);

    public void updateBatch(List<Isnailmail_letter> snailmail_letters);

    public Page<Isnailmail_letter> select(SearchContext context);

    public void getDraft(Isnailmail_letter snailmail_letter);

}
