package cn.ibizlab.odoo.core.odoo_event.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_event.domain.Event_mail_registration;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_mail_registrationSearchContext;


/**
 * 实体[Event_mail_registration] 服务对象接口
 */
public interface IEvent_mail_registrationService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Event_mail_registration et) ;
    void updateBatch(List<Event_mail_registration> list) ;
    boolean create(Event_mail_registration et) ;
    void createBatch(List<Event_mail_registration> list) ;
    Event_mail_registration getDraft(Event_mail_registration et) ;
    Event_mail_registration get(Integer key) ;
    Page<Event_mail_registration> searchDefault(Event_mail_registrationSearchContext context) ;

}



