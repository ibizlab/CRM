package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_package_destination;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_package_destinationSearchContext;


/**
 * 实体[Stock_package_destination] 服务对象接口
 */
public interface IStock_package_destinationService{

    Stock_package_destination getDraft(Stock_package_destination et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Stock_package_destination et) ;
    void updateBatch(List<Stock_package_destination> list) ;
    Stock_package_destination get(Integer key) ;
    boolean create(Stock_package_destination et) ;
    void createBatch(List<Stock_package_destination> list) ;
    Page<Stock_package_destination> searchDefault(Stock_package_destinationSearchContext context) ;

}



