package cn.ibizlab.odoo.core.odoo_repair.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_order;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_orderSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[repair_order] 服务对象接口
 */
@Component
public class repair_orderFallback implements repair_orderFeignClient{


    public Repair_order update(Integer id, Repair_order repair_order){
            return null;
     }
    public Boolean updateBatch(List<Repair_order> repair_orders){
            return false;
     }




    public Repair_order create(Repair_order repair_order){
            return null;
     }
    public Boolean createBatch(List<Repair_order> repair_orders){
            return false;
     }

    public Page<Repair_order> searchDefault(Repair_orderSearchContext context){
            return null;
     }


    public Repair_order get(Integer id){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Repair_order> select(){
            return null;
     }

    public Repair_order getDraft(){
            return null;
    }



}
