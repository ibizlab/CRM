package cn.ibizlab.odoo.core.odoo_crm.valuerule.anno.crm_lead;

import cn.ibizlab.odoo.core.odoo_crm.valuerule.validator.crm_lead.Crm_leadMessage_bounceDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Crm_lead
 * 属性：Message_bounce
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Crm_leadMessage_bounceDefaultValidator.class})
public @interface Crm_leadMessage_bounceDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
