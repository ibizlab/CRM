package cn.ibizlab.odoo.core.odoo_web_tour.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_web_tour.domain.Web_tour_tour;
import cn.ibizlab.odoo.core.odoo_web_tour.filter.Web_tour_tourSearchContext;
import cn.ibizlab.odoo.core.odoo_web_tour.service.IWeb_tour_tourService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_web_tour.client.web_tour_tourFeignClient;

/**
 * 实体[向导] 服务对象接口实现
 */
@Slf4j
@Service
public class Web_tour_tourServiceImpl implements IWeb_tour_tourService {

    @Autowired
    web_tour_tourFeignClient web_tour_tourFeignClient;


    @Override
    public boolean update(Web_tour_tour et) {
        Web_tour_tour rt = web_tour_tourFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Web_tour_tour> list){
        web_tour_tourFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=web_tour_tourFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        web_tour_tourFeignClient.removeBatch(idList);
    }

    @Override
    public Web_tour_tour get(Integer id) {
		Web_tour_tour et=web_tour_tourFeignClient.get(id);
        if(et==null){
            et=new Web_tour_tour();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Web_tour_tour et) {
        Web_tour_tour rt = web_tour_tourFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Web_tour_tour> list){
        web_tour_tourFeignClient.createBatch(list) ;
    }

    @Override
    public Web_tour_tour getDraft(Web_tour_tour et) {
        et=web_tour_tourFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Web_tour_tour> searchDefault(Web_tour_tourSearchContext context) {
        Page<Web_tour_tour> web_tour_tours=web_tour_tourFeignClient.searchDefault(context);
        return web_tour_tours;
    }


}


