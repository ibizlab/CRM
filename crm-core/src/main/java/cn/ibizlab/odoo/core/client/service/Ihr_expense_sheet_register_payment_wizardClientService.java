package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ihr_expense_sheet_register_payment_wizard;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_expense_sheet_register_payment_wizard] 服务对象接口
 */
public interface Ihr_expense_sheet_register_payment_wizardClientService{

    public Ihr_expense_sheet_register_payment_wizard createModel() ;

    public void updateBatch(List<Ihr_expense_sheet_register_payment_wizard> hr_expense_sheet_register_payment_wizards);

    public void update(Ihr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard);

    public void removeBatch(List<Ihr_expense_sheet_register_payment_wizard> hr_expense_sheet_register_payment_wizards);

    public void create(Ihr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard);

    public Page<Ihr_expense_sheet_register_payment_wizard> fetchDefault(SearchContext context);

    public void remove(Ihr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard);

    public void createBatch(List<Ihr_expense_sheet_register_payment_wizard> hr_expense_sheet_register_payment_wizards);

    public void get(Ihr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard);

    public Page<Ihr_expense_sheet_register_payment_wizard> select(SearchContext context);

    public void getDraft(Ihr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard);

}
