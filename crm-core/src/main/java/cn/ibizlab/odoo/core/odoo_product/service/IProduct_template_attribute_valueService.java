package cn.ibizlab.odoo.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_product.domain.Product_template_attribute_value;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_template_attribute_valueSearchContext;


/**
 * 实体[Product_template_attribute_value] 服务对象接口
 */
public interface IProduct_template_attribute_valueService{

    boolean update(Product_template_attribute_value et) ;
    void updateBatch(List<Product_template_attribute_value> list) ;
    boolean create(Product_template_attribute_value et) ;
    void createBatch(List<Product_template_attribute_value> list) ;
    Product_template_attribute_value getDraft(Product_template_attribute_value et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Product_template_attribute_value get(Integer key) ;
    Page<Product_template_attribute_value> searchDefault(Product_template_attribute_valueSearchContext context) ;

}



