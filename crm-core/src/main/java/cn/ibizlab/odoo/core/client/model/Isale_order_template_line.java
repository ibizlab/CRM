package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [sale_order_template_line] 对象
 */
public interface Isale_order_template_line {

    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [折扣(%)]
     */
    public void setDiscount(Double discount);
    
    /**
     * 设置 [折扣(%)]
     */
    public Double getDiscount();

    /**
     * 获取 [折扣(%)]脏标记
     */
    public boolean getDiscountDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [显示类型]
     */
    public void setDisplay_type(String display_type);
    
    /**
     * 设置 [显示类型]
     */
    public String getDisplay_type();

    /**
     * 获取 [显示类型]脏标记
     */
    public boolean getDisplay_typeDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [说明]
     */
    public void setName(String name);
    
    /**
     * 设置 [说明]
     */
    public String getName();

    /**
     * 获取 [说明]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [单价]
     */
    public void setPrice_unit(Double price_unit);
    
    /**
     * 设置 [单价]
     */
    public Double getPrice_unit();

    /**
     * 获取 [单价]脏标记
     */
    public boolean getPrice_unitDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id(Integer product_id);
    
    /**
     * 设置 [产品]
     */
    public Integer getProduct_id();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_idDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id_text(String product_id_text);
    
    /**
     * 设置 [产品]
     */
    public String getProduct_id_text();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_id_textDirtyFlag();
    /**
     * 获取 [计量单位]
     */
    public void setProduct_uom_id(Integer product_uom_id);
    
    /**
     * 设置 [计量单位]
     */
    public Integer getProduct_uom_id();

    /**
     * 获取 [计量单位]脏标记
     */
    public boolean getProduct_uom_idDirtyFlag();
    /**
     * 获取 [计量单位]
     */
    public void setProduct_uom_id_text(String product_uom_id_text);
    
    /**
     * 设置 [计量单位]
     */
    public String getProduct_uom_id_text();

    /**
     * 获取 [计量单位]脏标记
     */
    public boolean getProduct_uom_id_textDirtyFlag();
    /**
     * 获取 [数量]
     */
    public void setProduct_uom_qty(Double product_uom_qty);
    
    /**
     * 设置 [数量]
     */
    public Double getProduct_uom_qty();

    /**
     * 获取 [数量]脏标记
     */
    public boolean getProduct_uom_qtyDirtyFlag();
    /**
     * 获取 [报价单模板参考]
     */
    public void setSale_order_template_id(Integer sale_order_template_id);
    
    /**
     * 设置 [报价单模板参考]
     */
    public Integer getSale_order_template_id();

    /**
     * 获取 [报价单模板参考]脏标记
     */
    public boolean getSale_order_template_idDirtyFlag();
    /**
     * 获取 [报价单模板参考]
     */
    public void setSale_order_template_id_text(String sale_order_template_id_text);
    
    /**
     * 设置 [报价单模板参考]
     */
    public String getSale_order_template_id_text();

    /**
     * 获取 [报价单模板参考]脏标记
     */
    public boolean getSale_order_template_id_textDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
