package cn.ibizlab.odoo.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [登记付款] 对象
 */
@Data
public class Account_register_payments extends EntityClient implements Serializable {

    /**
     * 显示评论字段
     */
    @JSONField(name = "show_communication_field")
    @JsonProperty("show_communication_field")
    private String showCommunicationField;

    /**
     * 付款类型
     */
    @DEField(name = "payment_type")
    @JSONField(name = "payment_type")
    @JsonProperty("payment_type")
    private String paymentType;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 业务伙伴类型
     */
    @DEField(name = "partner_type")
    @JSONField(name = "partner_type")
    @JsonProperty("partner_type")
    private String partnerType;

    /**
     * 备忘
     */
    @JSONField(name = "communication")
    @JsonProperty("communication")
    private String communication;

    /**
     * 付款差异
     */
    @JSONField(name = "payment_difference")
    @JsonProperty("payment_difference")
    private Double paymentDifference;

    /**
     * 显示合作伙伴银行账户
     */
    @JSONField(name = "show_partner_bank_account")
    @JsonProperty("show_partner_bank_account")
    private String showPartnerBankAccount;

    /**
     * 发票分组
     */
    @DEField(name = "group_invoices")
    @JSONField(name = "group_invoices")
    @JsonProperty("group_invoices")
    private String groupInvoices;

    /**
     * 隐藏付款方式
     */
    @JSONField(name = "hide_payment_method")
    @JsonProperty("hide_payment_method")
    private String hidePaymentMethod;

    /**
     * 发票
     */
    @JSONField(name = "invoice_ids")
    @JsonProperty("invoice_ids")
    private String invoiceIds;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 付款日期
     */
    @DEField(name = "payment_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "payment_date" , format="yyyy-MM-dd")
    @JsonProperty("payment_date")
    private Timestamp paymentDate;

    /**
     * 日记账项目标签
     */
    @DEField(name = "writeoff_label")
    @JSONField(name = "writeoff_label")
    @JsonProperty("writeoff_label")
    private String writeoffLabel;

    /**
     * 多
     */
    @JSONField(name = "multi")
    @JsonProperty("multi")
    private String multi;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 付款差异处理
     */
    @DEField(name = "payment_difference_handling")
    @JSONField(name = "payment_difference_handling")
    @JsonProperty("payment_difference_handling")
    private String paymentDifferenceHandling;

    /**
     * 付款金额
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;

    /**
     * 币种
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 代码
     */
    @JSONField(name = "payment_method_code")
    @JsonProperty("payment_method_code")
    private String paymentMethodCode;

    /**
     * 付款方法类型
     */
    @JSONField(name = "payment_method_id_text")
    @JsonProperty("payment_method_id_text")
    private String paymentMethodIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 付款日记账
     */
    @JSONField(name = "journal_id_text")
    @JsonProperty("journal_id_text")
    private String journalIdText;

    /**
     * 业务伙伴
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 差异科目
     */
    @JSONField(name = "writeoff_account_id_text")
    @JsonProperty("writeoff_account_id_text")
    private String writeoffAccountIdText;

    /**
     * 差异科目
     */
    @DEField(name = "writeoff_account_id")
    @JSONField(name = "writeoff_account_id")
    @JsonProperty("writeoff_account_id")
    private Integer writeoffAccountId;

    /**
     * 收款银行账号
     */
    @DEField(name = "partner_bank_account_id")
    @JSONField(name = "partner_bank_account_id")
    @JsonProperty("partner_bank_account_id")
    private Integer partnerBankAccountId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 业务伙伴
     */
    @DEField(name = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 付款日记账
     */
    @DEField(name = "journal_id")
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    private Integer journalId;

    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 付款方法类型
     */
    @DEField(name = "payment_method_id")
    @JSONField(name = "payment_method_id")
    @JsonProperty("payment_method_id")
    private Integer paymentMethodId;


    /**
     * 
     */
    @JSONField(name = "odoowriteoffaccount")
    @JsonProperty("odoowriteoffaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account odooWriteoffAccount;

    /**
     * 
     */
    @JSONField(name = "odoojournal")
    @JsonProperty("odoojournal")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_journal odooJournal;

    /**
     * 
     */
    @JSONField(name = "odoopaymentmethod")
    @JsonProperty("odoopaymentmethod")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_method odooPaymentMethod;

    /**
     * 
     */
    @JSONField(name = "odoocurrency")
    @JsonProperty("odoocurrency")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JSONField(name = "odoopartnerbankaccount")
    @JsonProperty("odoopartnerbankaccount")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_bank odooPartnerBankAccount;

    /**
     * 
     */
    @JSONField(name = "odoopartner")
    @JsonProperty("odoopartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [付款类型]
     */
    public void setPaymentType(String paymentType){
        this.paymentType = paymentType ;
        this.modify("payment_type",paymentType);
    }
    /**
     * 设置 [业务伙伴类型]
     */
    public void setPartnerType(String partnerType){
        this.partnerType = partnerType ;
        this.modify("partner_type",partnerType);
    }
    /**
     * 设置 [备忘]
     */
    public void setCommunication(String communication){
        this.communication = communication ;
        this.modify("communication",communication);
    }
    /**
     * 设置 [发票分组]
     */
    public void setGroupInvoices(String groupInvoices){
        this.groupInvoices = groupInvoices ;
        this.modify("group_invoices",groupInvoices);
    }
    /**
     * 设置 [付款日期]
     */
    public void setPaymentDate(Timestamp paymentDate){
        this.paymentDate = paymentDate ;
        this.modify("payment_date",paymentDate);
    }
    /**
     * 设置 [日记账项目标签]
     */
    public void setWriteoffLabel(String writeoffLabel){
        this.writeoffLabel = writeoffLabel ;
        this.modify("writeoff_label",writeoffLabel);
    }
    /**
     * 设置 [多]
     */
    public void setMulti(String multi){
        this.multi = multi ;
        this.modify("multi",multi);
    }
    /**
     * 设置 [付款差异处理]
     */
    public void setPaymentDifferenceHandling(String paymentDifferenceHandling){
        this.paymentDifferenceHandling = paymentDifferenceHandling ;
        this.modify("payment_difference_handling",paymentDifferenceHandling);
    }
    /**
     * 设置 [付款金额]
     */
    public void setAmount(Double amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }
    /**
     * 设置 [差异科目]
     */
    public void setWriteoffAccountId(Integer writeoffAccountId){
        this.writeoffAccountId = writeoffAccountId ;
        this.modify("writeoff_account_id",writeoffAccountId);
    }
    /**
     * 设置 [收款银行账号]
     */
    public void setPartnerBankAccountId(Integer partnerBankAccountId){
        this.partnerBankAccountId = partnerBankAccountId ;
        this.modify("partner_bank_account_id",partnerBankAccountId);
    }
    /**
     * 设置 [业务伙伴]
     */
    public void setPartnerId(Integer partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }
    /**
     * 设置 [付款日记账]
     */
    public void setJournalId(Integer journalId){
        this.journalId = journalId ;
        this.modify("journal_id",journalId);
    }
    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Integer currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }
    /**
     * 设置 [付款方法类型]
     */
    public void setPaymentMethodId(Integer paymentMethodId){
        this.paymentMethodId = paymentMethodId ;
        this.modify("payment_method_id",paymentMethodId);
    }

}


