package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_notification;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_notification] 服务对象接口
 */
public interface Imail_notificationClientService{

    public Imail_notification createModel() ;

    public void createBatch(List<Imail_notification> mail_notifications);

    public void updateBatch(List<Imail_notification> mail_notifications);

    public void create(Imail_notification mail_notification);

    public Page<Imail_notification> fetchDefault(SearchContext context);

    public void remove(Imail_notification mail_notification);

    public void get(Imail_notification mail_notification);

    public void update(Imail_notification mail_notification);

    public void removeBatch(List<Imail_notification> mail_notifications);

    public Page<Imail_notification> select(SearchContext context);

    public void getDraft(Imail_notification mail_notification);

}
