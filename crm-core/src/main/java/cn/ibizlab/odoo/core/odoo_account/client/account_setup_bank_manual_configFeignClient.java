package cn.ibizlab.odoo.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_setup_bank_manual_config;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_setup_bank_manual_configSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_setup_bank_manual_config] 服务对象接口
 */
@FeignClient(value = "odoo-account", contextId = "account-setup-bank-manual-config", fallback = account_setup_bank_manual_configFallback.class)
public interface account_setup_bank_manual_configFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/account_setup_bank_manual_configs/{id}")
    Account_setup_bank_manual_config update(@PathVariable("id") Integer id,@RequestBody Account_setup_bank_manual_config account_setup_bank_manual_config);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_setup_bank_manual_configs/batch")
    Boolean updateBatch(@RequestBody List<Account_setup_bank_manual_config> account_setup_bank_manual_configs);


    @RequestMapping(method = RequestMethod.POST, value = "/account_setup_bank_manual_configs")
    Account_setup_bank_manual_config create(@RequestBody Account_setup_bank_manual_config account_setup_bank_manual_config);

    @RequestMapping(method = RequestMethod.POST, value = "/account_setup_bank_manual_configs/batch")
    Boolean createBatch(@RequestBody List<Account_setup_bank_manual_config> account_setup_bank_manual_configs);


    @RequestMapping(method = RequestMethod.GET, value = "/account_setup_bank_manual_configs/{id}")
    Account_setup_bank_manual_config get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.DELETE, value = "/account_setup_bank_manual_configs/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_setup_bank_manual_configs/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);





    @RequestMapping(method = RequestMethod.POST, value = "/account_setup_bank_manual_configs/searchdefault")
    Page<Account_setup_bank_manual_config> searchDefault(@RequestBody Account_setup_bank_manual_configSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/account_setup_bank_manual_configs/select")
    Page<Account_setup_bank_manual_config> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_setup_bank_manual_configs/getdraft")
    Account_setup_bank_manual_config getDraft();


}
