package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_mass_mailing] 服务对象接口
 */
public interface Imail_mass_mailingClientService{

    public Imail_mass_mailing createModel() ;

    public void removeBatch(List<Imail_mass_mailing> mail_mass_mailings);

    public void create(Imail_mass_mailing mail_mass_mailing);

    public void remove(Imail_mass_mailing mail_mass_mailing);

    public void updateBatch(List<Imail_mass_mailing> mail_mass_mailings);

    public Page<Imail_mass_mailing> fetchDefault(SearchContext context);

    public void update(Imail_mass_mailing mail_mass_mailing);

    public void createBatch(List<Imail_mass_mailing> mail_mass_mailings);

    public void get(Imail_mass_mailing mail_mass_mailing);

    public Page<Imail_mass_mailing> select(SearchContext context);

    public void getDraft(Imail_mass_mailing mail_mass_mailing);

}
