package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [account_payment_term_line] 对象
 */
public interface Iaccount_payment_term_line {

    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [天数]
     */
    public void setDays(Integer days);
    
    /**
     * 设置 [天数]
     */
    public Integer getDays();

    /**
     * 获取 [天数]脏标记
     */
    public boolean getDaysDirtyFlag();
    /**
     * 获取 [日期]
     */
    public void setDay_of_the_month(Integer day_of_the_month);
    
    /**
     * 设置 [日期]
     */
    public Integer getDay_of_the_month();

    /**
     * 获取 [日期]脏标记
     */
    public boolean getDay_of_the_monthDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [选项]
     */
    public void setOption(String option);
    
    /**
     * 设置 [选项]
     */
    public String getOption();

    /**
     * 获取 [选项]脏标记
     */
    public boolean getOptionDirtyFlag();
    /**
     * 获取 [付款条款]
     */
    public void setPayment_id(Integer payment_id);
    
    /**
     * 设置 [付款条款]
     */
    public Integer getPayment_id();

    /**
     * 获取 [付款条款]脏标记
     */
    public boolean getPayment_idDirtyFlag();
    /**
     * 获取 [付款条款]
     */
    public void setPayment_id_text(String payment_id_text);
    
    /**
     * 设置 [付款条款]
     */
    public String getPayment_id_text();

    /**
     * 获取 [付款条款]脏标记
     */
    public boolean getPayment_id_textDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [类型]
     */
    public void setValue(String value);
    
    /**
     * 设置 [类型]
     */
    public String getValue();

    /**
     * 获取 [类型]脏标记
     */
    public boolean getValueDirtyFlag();
    /**
     * 获取 [值]
     */
    public void setValue_amount(Double value_amount);
    
    /**
     * 设置 [值]
     */
    public Double getValue_amount();

    /**
     * 获取 [值]脏标记
     */
    public boolean getValue_amountDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
