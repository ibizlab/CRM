package cn.ibizlab.odoo.core.odoo_sale.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_orderSearchContext;


/**
 * 实体[Sale_order] 服务对象接口
 */
public interface ISale_orderService{

    boolean update(Sale_order et) ;
    void updateBatch(List<Sale_order> list) ;
    Sale_order get(Integer key) ;
    boolean save(Sale_order et) ;
    void saveBatch(List<Sale_order> list) ;
    boolean checkKey(Sale_order et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Sale_order getDraft(Sale_order et) ;
    boolean create(Sale_order et) ;
    void createBatch(List<Sale_order> list) ;
    Page<Sale_order> searchDefault(Sale_orderSearchContext context) ;

}



