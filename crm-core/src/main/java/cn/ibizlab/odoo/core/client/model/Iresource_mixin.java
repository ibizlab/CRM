package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [resource_mixin] 对象
 */
public interface Iresource_mixin {

    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [工作时间]
     */
    public void setResource_calendar_id(Integer resource_calendar_id);
    
    /**
     * 设置 [工作时间]
     */
    public Integer getResource_calendar_id();

    /**
     * 获取 [工作时间]脏标记
     */
    public boolean getResource_calendar_idDirtyFlag();
    /**
     * 获取 [工作时间]
     */
    public void setResource_calendar_id_text(String resource_calendar_id_text);
    
    /**
     * 设置 [工作时间]
     */
    public String getResource_calendar_id_text();

    /**
     * 获取 [工作时间]脏标记
     */
    public boolean getResource_calendar_id_textDirtyFlag();
    /**
     * 获取 [资源]
     */
    public void setResource_id(Integer resource_id);
    
    /**
     * 设置 [资源]
     */
    public Integer getResource_id();

    /**
     * 获取 [资源]脏标记
     */
    public boolean getResource_idDirtyFlag();
    /**
     * 获取 [资源]
     */
    public void setResource_id_text(String resource_id_text);
    
    /**
     * 设置 [资源]
     */
    public String getResource_id_text();

    /**
     * 获取 [资源]脏标记
     */
    public boolean getResource_id_textDirtyFlag();
    /**
     * 获取 [时区]
     */
    public void setTz(String tz);
    
    /**
     * 设置 [时区]
     */
    public String getTz();

    /**
     * 获取 [时区]脏标记
     */
    public boolean getTzDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
