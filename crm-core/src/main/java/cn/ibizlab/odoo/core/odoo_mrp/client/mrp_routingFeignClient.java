package cn.ibizlab.odoo.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_routing;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_routingSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mrp_routing] 服务对象接口
 */
@FeignClient(value = "odoo-mrp", contextId = "mrp-routing", fallback = mrp_routingFallback.class)
public interface mrp_routingFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_routings/{id}")
    Mrp_routing update(@PathVariable("id") Integer id,@RequestBody Mrp_routing mrp_routing);

    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_routings/batch")
    Boolean updateBatch(@RequestBody List<Mrp_routing> mrp_routings);


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_routings/{id}")
    Mrp_routing get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_routings")
    Mrp_routing create(@RequestBody Mrp_routing mrp_routing);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_routings/batch")
    Boolean createBatch(@RequestBody List<Mrp_routing> mrp_routings);



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_routings/searchdefault")
    Page<Mrp_routing> searchDefault(@RequestBody Mrp_routingSearchContext context);




    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_routings/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_routings/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/mrp_routings/select")
    Page<Mrp_routing> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_routings/getdraft")
    Mrp_routing getDraft();


}
