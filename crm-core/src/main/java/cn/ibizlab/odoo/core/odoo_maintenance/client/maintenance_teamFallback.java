package cn.ibizlab.odoo.core.odoo_maintenance.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_team;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_teamSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[maintenance_team] 服务对象接口
 */
@Component
public class maintenance_teamFallback implements maintenance_teamFeignClient{

    public Maintenance_team get(Integer id){
            return null;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Maintenance_team> searchDefault(Maintenance_teamSearchContext context){
            return null;
     }


    public Maintenance_team update(Integer id, Maintenance_team maintenance_team){
            return null;
     }
    public Boolean updateBatch(List<Maintenance_team> maintenance_teams){
            return false;
     }




    public Maintenance_team create(Maintenance_team maintenance_team){
            return null;
     }
    public Boolean createBatch(List<Maintenance_team> maintenance_teams){
            return false;
     }


    public Page<Maintenance_team> select(){
            return null;
     }

    public Maintenance_team getDraft(){
            return null;
    }



}
