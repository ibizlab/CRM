package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_users_log;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_users_logSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_users_logService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_base.client.res_users_logFeignClient;

/**
 * 实体[用户日志] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_users_logServiceImpl implements IRes_users_logService {

    @Autowired
    res_users_logFeignClient res_users_logFeignClient;


    @Override
    public Res_users_log getDraft(Res_users_log et) {
        et=res_users_logFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Res_users_log et) {
        Res_users_log rt = res_users_logFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Res_users_log> list){
        res_users_logFeignClient.updateBatch(list) ;
    }

    @Override
    public Res_users_log get(Integer id) {
		Res_users_log et=res_users_logFeignClient.get(id);
        if(et==null){
            et=new Res_users_log();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Res_users_log et) {
        Res_users_log rt = res_users_logFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_users_log> list){
        res_users_logFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=res_users_logFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        res_users_logFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_users_log> searchDefault(Res_users_logSearchContext context) {
        Page<Res_users_log> res_users_logs=res_users_logFeignClient.searchDefault(context);
        return res_users_logs;
    }


}


