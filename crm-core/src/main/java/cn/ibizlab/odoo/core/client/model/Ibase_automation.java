package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [base_automation] 对象
 */
public interface Ibase_automation {

    /**
     * 获取 [服务器动作]
     */
    public void setAction_server_id(Integer action_server_id);
    
    /**
     * 设置 [服务器动作]
     */
    public Integer getAction_server_id();

    /**
     * 获取 [服务器动作]脏标记
     */
    public boolean getAction_server_idDirtyFlag();
    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [截止日期至]
     */
    public void setActivity_date_deadline_range(Integer activity_date_deadline_range);
    
    /**
     * 设置 [截止日期至]
     */
    public Integer getActivity_date_deadline_range();

    /**
     * 获取 [截止日期至]脏标记
     */
    public boolean getActivity_date_deadline_rangeDirtyFlag();
    /**
     * 获取 [到期类型]
     */
    public void setActivity_date_deadline_range_type(String activity_date_deadline_range_type);
    
    /**
     * 设置 [到期类型]
     */
    public String getActivity_date_deadline_range_type();

    /**
     * 获取 [到期类型]脏标记
     */
    public boolean getActivity_date_deadline_range_typeDirtyFlag();
    /**
     * 获取 [备注]
     */
    public void setActivity_note(String activity_note);
    
    /**
     * 设置 [备注]
     */
    public String getActivity_note();

    /**
     * 获取 [备注]脏标记
     */
    public boolean getActivity_noteDirtyFlag();
    /**
     * 获取 [摘要]
     */
    public void setActivity_summary(String activity_summary);
    
    /**
     * 设置 [摘要]
     */
    public String getActivity_summary();

    /**
     * 获取 [摘要]脏标记
     */
    public boolean getActivity_summaryDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setActivity_type_id(Integer activity_type_id);
    
    /**
     * 设置 [活动]
     */
    public Integer getActivity_type_id();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getActivity_type_idDirtyFlag();
    /**
     * 获取 [用户字段名字]
     */
    public void setActivity_user_field_name(String activity_user_field_name);
    
    /**
     * 设置 [用户字段名字]
     */
    public String getActivity_user_field_name();

    /**
     * 获取 [用户字段名字]脏标记
     */
    public boolean getActivity_user_field_nameDirtyFlag();
    /**
     * 获取 [负责人]
     */
    public void setActivity_user_id(Integer activity_user_id);
    
    /**
     * 设置 [负责人]
     */
    public Integer getActivity_user_id();

    /**
     * 获取 [负责人]脏标记
     */
    public boolean getActivity_user_idDirtyFlag();
    /**
     * 获取 [活动用户类型]
     */
    public void setActivity_user_type(String activity_user_type);
    
    /**
     * 设置 [活动用户类型]
     */
    public String getActivity_user_type();

    /**
     * 获取 [活动用户类型]脏标记
     */
    public boolean getActivity_user_typeDirtyFlag();
    /**
     * 获取 [绑定模型]
     */
    public void setBinding_model_id(Integer binding_model_id);
    
    /**
     * 设置 [绑定模型]
     */
    public Integer getBinding_model_id();

    /**
     * 获取 [绑定模型]脏标记
     */
    public boolean getBinding_model_idDirtyFlag();
    /**
     * 获取 [绑定类型]
     */
    public void setBinding_type(String binding_type);
    
    /**
     * 设置 [绑定类型]
     */
    public String getBinding_type();

    /**
     * 获取 [绑定类型]脏标记
     */
    public boolean getBinding_typeDirtyFlag();
    /**
     * 获取 [添加频道]
     */
    public void setChannel_ids(String channel_ids);
    
    /**
     * 设置 [添加频道]
     */
    public String getChannel_ids();

    /**
     * 获取 [添加频道]脏标记
     */
    public boolean getChannel_idsDirtyFlag();
    /**
     * 获取 [下级动作]
     */
    public void setChild_ids(String child_ids);
    
    /**
     * 设置 [下级动作]
     */
    public String getChild_ids();

    /**
     * 获取 [下级动作]脏标记
     */
    public boolean getChild_idsDirtyFlag();
    /**
     * 获取 [Python 代码]
     */
    public void setCode(String code);
    
    /**
     * 设置 [Python 代码]
     */
    public String getCode();

    /**
     * 获取 [Python 代码]脏标记
     */
    public boolean getCodeDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [创建/写目标模型]
     */
    public void setCrud_model_id(Integer crud_model_id);
    
    /**
     * 设置 [创建/写目标模型]
     */
    public Integer getCrud_model_id();

    /**
     * 获取 [创建/写目标模型]脏标记
     */
    public boolean getCrud_model_idDirtyFlag();
    /**
     * 获取 [目标模型]
     */
    public void setCrud_model_name(String crud_model_name);
    
    /**
     * 设置 [目标模型]
     */
    public String getCrud_model_name();

    /**
     * 获取 [目标模型]脏标记
     */
    public boolean getCrud_model_nameDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [映射的值]
     */
    public void setFields_lines(String fields_lines);
    
    /**
     * 设置 [映射的值]
     */
    public String getFields_lines();

    /**
     * 获取 [映射的值]脏标记
     */
    public boolean getFields_linesDirtyFlag();
    /**
     * 获取 [应用于]
     */
    public void setFilter_domain(String filter_domain);
    
    /**
     * 设置 [应用于]
     */
    public String getFilter_domain();

    /**
     * 获取 [应用于]脏标记
     */
    public boolean getFilter_domainDirtyFlag();
    /**
     * 获取 [更新前域表达式]
     */
    public void setFilter_pre_domain(String filter_pre_domain);
    
    /**
     * 设置 [更新前域表达式]
     */
    public String getFilter_pre_domain();

    /**
     * 获取 [更新前域表达式]脏标记
     */
    public boolean getFilter_pre_domainDirtyFlag();
    /**
     * 获取 [动作说明]
     */
    public void setHelp(String help);
    
    /**
     * 设置 [动作说明]
     */
    public String getHelp();

    /**
     * 获取 [动作说明]脏标记
     */
    public boolean getHelpDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [最后运行]
     */
    public void setLast_run(Timestamp last_run);
    
    /**
     * 设置 [最后运行]
     */
    public Timestamp getLast_run();

    /**
     * 获取 [最后运行]脏标记
     */
    public boolean getLast_runDirtyFlag();
    /**
     * 获取 [链接使用字段]
     */
    public void setLink_field_id(Integer link_field_id);
    
    /**
     * 设置 [链接使用字段]
     */
    public Integer getLink_field_id();

    /**
     * 获取 [链接使用字段]脏标记
     */
    public boolean getLink_field_idDirtyFlag();
    /**
     * 获取 [模型]
     */
    public void setModel_id(Integer model_id);
    
    /**
     * 设置 [模型]
     */
    public Integer getModel_id();

    /**
     * 获取 [模型]脏标记
     */
    public boolean getModel_idDirtyFlag();
    /**
     * 获取 [模型名称]
     */
    public void setModel_name(String model_name);
    
    /**
     * 设置 [模型名称]
     */
    public String getModel_name();

    /**
     * 获取 [模型名称]脏标记
     */
    public boolean getModel_nameDirtyFlag();
    /**
     * 获取 [动作名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [动作名称]
     */
    public String getName();

    /**
     * 获取 [动作名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [变化字段的触发器]
     */
    public void setOn_change_fields(String on_change_fields);
    
    /**
     * 设置 [变化字段的触发器]
     */
    public String getOn_change_fields();

    /**
     * 获取 [变化字段的触发器]脏标记
     */
    public boolean getOn_change_fieldsDirtyFlag();
    /**
     * 获取 [添加关注者]
     */
    public void setPartner_ids(String partner_ids);
    
    /**
     * 设置 [添加关注者]
     */
    public String getPartner_ids();

    /**
     * 获取 [添加关注者]脏标记
     */
    public boolean getPartner_idsDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [待办的行动]
     */
    public void setState(String state);
    
    /**
     * 设置 [待办的行动]
     */
    public String getState();

    /**
     * 获取 [待办的行动]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [EMail模板]
     */
    public void setTemplate_id(Integer template_id);
    
    /**
     * 设置 [EMail模板]
     */
    public Integer getTemplate_id();

    /**
     * 获取 [EMail模板]脏标记
     */
    public boolean getTemplate_idDirtyFlag();
    /**
     * 获取 [使用日历]
     */
    public void setTrg_date_calendar_id(Integer trg_date_calendar_id);
    
    /**
     * 设置 [使用日历]
     */
    public Integer getTrg_date_calendar_id();

    /**
     * 获取 [使用日历]脏标记
     */
    public boolean getTrg_date_calendar_idDirtyFlag();
    /**
     * 获取 [使用日历]
     */
    public void setTrg_date_calendar_id_text(String trg_date_calendar_id_text);
    
    /**
     * 设置 [使用日历]
     */
    public String getTrg_date_calendar_id_text();

    /**
     * 获取 [使用日历]脏标记
     */
    public boolean getTrg_date_calendar_id_textDirtyFlag();
    /**
     * 获取 [触发日期]
     */
    public void setTrg_date_id(Integer trg_date_id);
    
    /**
     * 设置 [触发日期]
     */
    public Integer getTrg_date_id();

    /**
     * 获取 [触发日期]脏标记
     */
    public boolean getTrg_date_idDirtyFlag();
    /**
     * 获取 [触发日期后的延迟]
     */
    public void setTrg_date_range(Integer trg_date_range);
    
    /**
     * 设置 [触发日期后的延迟]
     */
    public Integer getTrg_date_range();

    /**
     * 获取 [触发日期后的延迟]脏标记
     */
    public boolean getTrg_date_rangeDirtyFlag();
    /**
     * 获取 [延迟类型]
     */
    public void setTrg_date_range_type(String trg_date_range_type);
    
    /**
     * 设置 [延迟类型]
     */
    public String getTrg_date_range_type();

    /**
     * 获取 [延迟类型]脏标记
     */
    public boolean getTrg_date_range_typeDirtyFlag();
    /**
     * 获取 [触发条件]
     */
    public void setTrigger(String trigger);
    
    /**
     * 设置 [触发条件]
     */
    public String getTrigger();

    /**
     * 获取 [触发条件]脏标记
     */
    public boolean getTriggerDirtyFlag();
    /**
     * 获取 [动作类型]
     */
    public void setType(String type);
    
    /**
     * 设置 [动作类型]
     */
    public String getType();

    /**
     * 获取 [动作类型]脏标记
     */
    public boolean getTypeDirtyFlag();
    /**
     * 获取 [用途]
     */
    public void setUsage(String usage);
    
    /**
     * 设置 [用途]
     */
    public String getUsage();

    /**
     * 获取 [用途]脏标记
     */
    public boolean getUsageDirtyFlag();
    /**
     * 获取 [网站路径]
     */
    public void setWebsite_path(String website_path);
    
    /**
     * 设置 [网站路径]
     */
    public String getWebsite_path();

    /**
     * 获取 [网站路径]脏标记
     */
    public boolean getWebsite_pathDirtyFlag();
    /**
     * 获取 [可用于网站]
     */
    public void setWebsite_published(String website_published);
    
    /**
     * 设置 [可用于网站]
     */
    public String getWebsite_published();

    /**
     * 获取 [可用于网站]脏标记
     */
    public boolean getWebsite_publishedDirtyFlag();
    /**
     * 获取 [网站网址]
     */
    public void setWebsite_url(String website_url);
    
    /**
     * 设置 [网站网址]
     */
    public String getWebsite_url();

    /**
     * 获取 [网站网址]脏标记
     */
    public boolean getWebsite_urlDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [外部 ID]
     */
    public void setXml_id(String xml_id);
    
    /**
     * 设置 [外部 ID]
     */
    public String getXml_id();

    /**
     * 获取 [外部 ID]脏标记
     */
    public boolean getXml_idDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
