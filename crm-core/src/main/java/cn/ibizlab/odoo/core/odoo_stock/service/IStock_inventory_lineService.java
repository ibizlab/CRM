package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_inventory_line;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_inventory_lineSearchContext;


/**
 * 实体[Stock_inventory_line] 服务对象接口
 */
public interface IStock_inventory_lineService{

    boolean create(Stock_inventory_line et) ;
    void createBatch(List<Stock_inventory_line> list) ;
    Stock_inventory_line get(Integer key) ;
    boolean update(Stock_inventory_line et) ;
    void updateBatch(List<Stock_inventory_line> list) ;
    Stock_inventory_line getDraft(Stock_inventory_line et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Stock_inventory_line> searchDefault(Stock_inventory_lineSearchContext context) ;

}



