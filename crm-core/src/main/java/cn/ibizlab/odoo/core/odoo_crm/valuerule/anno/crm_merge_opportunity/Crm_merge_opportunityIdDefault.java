package cn.ibizlab.odoo.core.odoo_crm.valuerule.anno.crm_merge_opportunity;

import cn.ibizlab.odoo.core.odoo_crm.valuerule.validator.crm_merge_opportunity.Crm_merge_opportunityIdDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Crm_merge_opportunity
 * 属性：Id
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Crm_merge_opportunityIdDefaultValidator.class})
public @interface Crm_merge_opportunityIdDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
