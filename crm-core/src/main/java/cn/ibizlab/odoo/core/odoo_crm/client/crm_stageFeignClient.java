package cn.ibizlab.odoo.core.odoo_crm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_stage;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_stageSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[crm_stage] 服务对象接口
 */
@FeignClient(value = "odoo-crm", contextId = "crm-stage", fallback = crm_stageFallback.class)
public interface crm_stageFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/crm_stages/{id}")
    Crm_stage get(@PathVariable("id") Integer id);



    @RequestMapping(method = RequestMethod.PUT, value = "/crm_stages/{id}")
    Crm_stage update(@PathVariable("id") Integer id,@RequestBody Crm_stage crm_stage);

    @RequestMapping(method = RequestMethod.PUT, value = "/crm_stages/batch")
    Boolean updateBatch(@RequestBody List<Crm_stage> crm_stages);


    @RequestMapping(method = RequestMethod.DELETE, value = "/crm_stages/{id}")
    Boolean remove(@PathVariable("id") Integer id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/crm_stages/batch}")
    Boolean removeBatch(@RequestBody Collection<Integer> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/crm_stages")
    Crm_stage create(@RequestBody Crm_stage crm_stage);

    @RequestMapping(method = RequestMethod.POST, value = "/crm_stages/batch")
    Boolean createBatch(@RequestBody List<Crm_stage> crm_stages);



    @RequestMapping(method = RequestMethod.POST, value = "/crm_stages/searchdefault")
    Page<Crm_stage> searchDefault(@RequestBody Crm_stageSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/crm_stages/select")
    Page<Crm_stage> select();


    @RequestMapping(method = RequestMethod.GET, value = "/crm_stages/getdraft")
    Crm_stage getDraft();


}
