package cn.ibizlab.odoo.core.odoo_bus.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_bus.domain.Bus_presence;
import cn.ibizlab.odoo.core.odoo_bus.filter.Bus_presenceSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[bus_presence] 服务对象接口
 */
@Component
public class bus_presenceFallback implements bus_presenceFeignClient{


    public Bus_presence get(Integer id){
            return null;
     }


    public Page<Bus_presence> searchDefault(Bus_presenceSearchContext context){
            return null;
     }


    public Bus_presence create(Bus_presence bus_presence){
            return null;
     }
    public Boolean createBatch(List<Bus_presence> bus_presences){
            return false;
     }



    public Bus_presence update(Integer id, Bus_presence bus_presence){
            return null;
     }
    public Boolean updateBatch(List<Bus_presence> bus_presences){
            return false;
     }


    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Bus_presence> select(){
            return null;
     }

    public Bus_presence getDraft(){
            return null;
    }



}
