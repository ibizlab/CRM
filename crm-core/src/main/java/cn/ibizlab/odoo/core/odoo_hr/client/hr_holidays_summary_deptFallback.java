package cn.ibizlab.odoo.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_holidays_summary_dept;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_holidays_summary_deptSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[hr_holidays_summary_dept] 服务对象接口
 */
@Component
public class hr_holidays_summary_deptFallback implements hr_holidays_summary_deptFeignClient{

    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }

    public Page<Hr_holidays_summary_dept> searchDefault(Hr_holidays_summary_deptSearchContext context){
            return null;
     }


    public Hr_holidays_summary_dept get(Integer id){
            return null;
     }



    public Hr_holidays_summary_dept create(Hr_holidays_summary_dept hr_holidays_summary_dept){
            return null;
     }
    public Boolean createBatch(List<Hr_holidays_summary_dept> hr_holidays_summary_depts){
            return false;
     }


    public Hr_holidays_summary_dept update(Integer id, Hr_holidays_summary_dept hr_holidays_summary_dept){
            return null;
     }
    public Boolean updateBatch(List<Hr_holidays_summary_dept> hr_holidays_summary_depts){
            return false;
     }



    public Page<Hr_holidays_summary_dept> select(){
            return null;
     }

    public Hr_holidays_summary_dept getDraft(){
            return null;
    }



}
