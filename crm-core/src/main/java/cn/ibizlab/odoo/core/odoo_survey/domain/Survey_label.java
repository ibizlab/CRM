package cn.ibizlab.odoo.core.odoo_survey.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [调查标签] 对象
 */
@Data
public class Survey_label extends EntityClient implements Serializable {

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 标签序列顺序
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 这个选项的分数
     */
    @DEField(name = "quizz_mark")
    @JSONField(name = "quizz_mark")
    @JsonProperty("quizz_mark")
    private Double quizzMark;

    /**
     * 建议值
     */
    @JSONField(name = "value")
    @JsonProperty("value")
    private String value;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 第二个问题
     */
    @DEField(name = "question_id_2")
    @JSONField(name = "question_id_2")
    @JsonProperty("question_id_2")
    private Integer questionId2;

    /**
     * 疑问
     */
    @DEField(name = "question_id")
    @JSONField(name = "question_id")
    @JsonProperty("question_id")
    private Integer questionId;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odooquestion")
    @JsonProperty("odooquestion")
    private cn.ibizlab.odoo.core.odoo_survey.domain.Survey_question odooQuestion;

    /**
     * 
     */
    @JSONField(name = "odooquestionid")
    @JsonProperty("odooquestionid")
    private cn.ibizlab.odoo.core.odoo_survey.domain.Survey_question odooQuestionId;




    /**
     * 设置 [标签序列顺序]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }
    /**
     * 设置 [这个选项的分数]
     */
    public void setQuizzMark(Double quizzMark){
        this.quizzMark = quizzMark ;
        this.modify("quizz_mark",quizzMark);
    }
    /**
     * 设置 [建议值]
     */
    public void setValue(String value){
        this.value = value ;
        this.modify("value",value);
    }
    /**
     * 设置 [第二个问题]
     */
    public void setQuestionId2(Integer questionId2){
        this.questionId2 = questionId2 ;
        this.modify("question_id_2",questionId2);
    }
    /**
     * 设置 [疑问]
     */
    public void setQuestionId(Integer questionId){
        this.questionId = questionId ;
        this.modify("question_id",questionId);
    }

}


