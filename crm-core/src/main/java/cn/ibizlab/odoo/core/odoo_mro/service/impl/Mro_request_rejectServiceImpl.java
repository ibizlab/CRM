package cn.ibizlab.odoo.core.odoo_mro.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_request_reject;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_request_rejectSearchContext;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_request_rejectService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_mro.client.mro_request_rejectFeignClient;

/**
 * 实体[Reject Request] 服务对象接口实现
 */
@Slf4j
@Service
public class Mro_request_rejectServiceImpl implements IMro_request_rejectService {

    @Autowired
    mro_request_rejectFeignClient mro_request_rejectFeignClient;


    @Override
    public boolean create(Mro_request_reject et) {
        Mro_request_reject rt = mro_request_rejectFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mro_request_reject> list){
        mro_request_rejectFeignClient.createBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=mro_request_rejectFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        mro_request_rejectFeignClient.removeBatch(idList);
    }

    @Override
    public Mro_request_reject get(Integer id) {
		Mro_request_reject et=mro_request_rejectFeignClient.get(id);
        if(et==null){
            et=new Mro_request_reject();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean update(Mro_request_reject et) {
        Mro_request_reject rt = mro_request_rejectFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Mro_request_reject> list){
        mro_request_rejectFeignClient.updateBatch(list) ;
    }

    @Override
    public Mro_request_reject getDraft(Mro_request_reject et) {
        et=mro_request_rejectFeignClient.getDraft();
        return et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mro_request_reject> searchDefault(Mro_request_rejectSearchContext context) {
        Page<Mro_request_reject> mro_request_rejects=mro_request_rejectFeignClient.searchDefault(context);
        return mro_request_rejects;
    }


}


