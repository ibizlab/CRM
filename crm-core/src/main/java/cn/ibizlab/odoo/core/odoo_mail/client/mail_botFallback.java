package cn.ibizlab.odoo.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_bot;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_botSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_bot] 服务对象接口
 */
@Component
public class mail_botFallback implements mail_botFeignClient{

    public Mail_bot update(Integer id, Mail_bot mail_bot){
            return null;
     }
    public Boolean updateBatch(List<Mail_bot> mail_bots){
            return false;
     }


    public Mail_bot create(Mail_bot mail_bot){
            return null;
     }
    public Boolean createBatch(List<Mail_bot> mail_bots){
            return false;
     }


    public Page<Mail_bot> searchDefault(Mail_botSearchContext context){
            return null;
     }


    public Mail_bot get(Integer id){
            return null;
     }



    public Boolean remove(Integer id){
            return false;
     }
    public Boolean removeBatch(Collection<Integer> idList){
            return false;
     }


    public Page<Mail_bot> select(){
            return null;
     }

    public Mail_bot getDraft(){
            return null;
    }



}
