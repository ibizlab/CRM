package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_reconcile_model;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_reconcile_modelSearchContext;


/**
 * 实体[Account_reconcile_model] 服务对象接口
 */
public interface IAccount_reconcile_modelService{

    Account_reconcile_model get(Integer key) ;
    Account_reconcile_model getDraft(Account_reconcile_model et) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Account_reconcile_model et) ;
    void createBatch(List<Account_reconcile_model> list) ;
    boolean update(Account_reconcile_model et) ;
    void updateBatch(List<Account_reconcile_model> list) ;
    Page<Account_reconcile_model> searchDefault(Account_reconcile_modelSearchContext context) ;

}



