package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iportal_wizard_user;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[portal_wizard_user] 服务对象接口
 */
public interface Iportal_wizard_userClientService{

    public Iportal_wizard_user createModel() ;

    public void get(Iportal_wizard_user portal_wizard_user);

    public void create(Iportal_wizard_user portal_wizard_user);

    public void createBatch(List<Iportal_wizard_user> portal_wizard_users);

    public void updateBatch(List<Iportal_wizard_user> portal_wizard_users);

    public void removeBatch(List<Iportal_wizard_user> portal_wizard_users);

    public Page<Iportal_wizard_user> fetchDefault(SearchContext context);

    public void remove(Iportal_wizard_user portal_wizard_user);

    public void update(Iportal_wizard_user portal_wizard_user);

    public Page<Iportal_wizard_user> select(SearchContext context);

    public void getDraft(Iportal_wizard_user portal_wizard_user);

}
