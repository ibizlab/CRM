package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_template;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_templateSearchContext;


/**
 * 实体[Mail_template] 服务对象接口
 */
public interface IMail_templateService{

    Mail_template get(Integer key) ;
    Mail_template getDraft(Mail_template et) ;
    boolean update(Mail_template et) ;
    void updateBatch(List<Mail_template> list) ;
    boolean create(Mail_template et) ;
    void createBatch(List<Mail_template> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Mail_template> searchDefault(Mail_templateSearchContext context) ;

}



