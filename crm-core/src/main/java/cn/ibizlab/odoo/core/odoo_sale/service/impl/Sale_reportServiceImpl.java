package cn.ibizlab.odoo.core.odoo_sale.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_report;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_reportSearchContext;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_reportService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;


import cn.ibizlab.odoo.core.odoo_sale.client.sale_reportFeignClient;

/**
 * 实体[销售分析报告] 服务对象接口实现
 */
@Slf4j
@Service
public class Sale_reportServiceImpl implements ISale_reportService {

    @Autowired
    sale_reportFeignClient sale_reportFeignClient;


    @Override
    public Sale_report get(Integer id) {
		Sale_report et=sale_reportFeignClient.get(id);
        if(et==null){
            et=new Sale_report();
            et.setId(id);
        }
        else{
        }
        return  et;
    }

    @Override
    public boolean create(Sale_report et) {
        Sale_report rt = sale_reportFeignClient.create(et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Sale_report> list){
        sale_reportFeignClient.createBatch(list) ;
    }

    @Override
    public Sale_report getDraft(Sale_report et) {
        et=sale_reportFeignClient.getDraft();
        return et;
    }

    @Override
    public boolean update(Sale_report et) {
        Sale_report rt = sale_reportFeignClient.update(et.getId(),et);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }

    public void updateBatch(List<Sale_report> list){
        sale_reportFeignClient.updateBatch(list) ;
    }

    @Override
    public boolean remove(Integer id) {
        boolean result=sale_reportFeignClient.remove(id) ;
        return result;
    }

    public void removeBatch(Collection<Integer> idList){
        sale_reportFeignClient.removeBatch(idList);
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Sale_report> searchDefault(Sale_reportSearchContext context) {
        Page<Sale_report> sale_reports=sale_reportFeignClient.searchDefault(context);
        return sale_reports;
    }


}


