package cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_abstract_payment;

import cn.ibizlab.odoo.core.odoo_account.valuerule.validator.account_abstract_payment.Account_abstract_paymentPayment_differenceDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Account_abstract_payment
 * 属性：Payment_difference
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Account_abstract_paymentPayment_differenceDefaultValidator.class})
public @interface Account_abstract_paymentPayment_differenceDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
