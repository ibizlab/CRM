package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [base_automation_lead_test] 对象
 */
public interface Ibase_automation_lead_test {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [客户]
     */
    public void setCustomer(String customer);
    
    /**
     * 设置 [客户]
     */
    public String getCustomer();

    /**
     * 获取 [客户]脏标记
     */
    public boolean getCustomerDirtyFlag();
    /**
     * 获取 [最近行动]
     */
    public void setDate_action_last(Timestamp date_action_last);
    
    /**
     * 设置 [最近行动]
     */
    public Timestamp getDate_action_last();

    /**
     * 获取 [最近行动]脏标记
     */
    public boolean getDate_action_lastDirtyFlag();
    /**
     * 获取 [截止日期]
     */
    public void setDeadline(String deadline);
    
    /**
     * 设置 [截止日期]
     */
    public String getDeadline();

    /**
     * 获取 [截止日期]脏标记
     */
    public boolean getDeadlineDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [指派给管理用户]
     */
    public void setIs_assigned_to_admin(String is_assigned_to_admin);
    
    /**
     * 设置 [指派给管理用户]
     */
    public String getIs_assigned_to_admin();

    /**
     * 获取 [指派给管理用户]脏标记
     */
    public boolean getIs_assigned_to_adminDirtyFlag();
    /**
     * 获取 [行]
     */
    public void setLine_ids(String line_ids);
    
    /**
     * 设置 [行]
     */
    public String getLine_ids();

    /**
     * 获取 [行]脏标记
     */
    public boolean getLine_idsDirtyFlag();
    /**
     * 获取 [主题]
     */
    public void setName(String name);
    
    /**
     * 设置 [主题]
     */
    public String getName();

    /**
     * 获取 [主题]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [业务伙伴]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [业务伙伴]
     */
    public Integer getPartner_id();

    /**
     * 获取 [业务伙伴]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [业务伙伴]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [业务伙伴]
     */
    public String getPartner_id_text();

    /**
     * 获取 [业务伙伴]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [优先级]
     */
    public void setPriority(String priority);
    
    /**
     * 设置 [优先级]
     */
    public String getPriority();

    /**
     * 获取 [优先级]脏标记
     */
    public boolean getPriorityDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [负责人]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [负责人]
     */
    public Integer getUser_id();

    /**
     * 获取 [负责人]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [负责人]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [负责人]
     */
    public String getUser_id_text();

    /**
     * 获取 [负责人]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();
}
