package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Icrm_team;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[crm_team] 服务对象接口
 */
public interface Icrm_teamClientService{

    public Icrm_team createModel() ;

    public Page<Icrm_team> fetchDefault(SearchContext context);

    public void create(Icrm_team crm_team);

    public void updateBatch(List<Icrm_team> crm_teams);

    public void removeBatch(List<Icrm_team> crm_teams);

    public void createBatch(List<Icrm_team> crm_teams);

    public void get(Icrm_team crm_team);

    public void remove(Icrm_team crm_team);

    public void update(Icrm_team crm_team);

    public Page<Icrm_team> select(SearchContext context);

    public void checkKey(Icrm_team crm_team);

    public void getDraft(Icrm_team crm_team);

    public void save(Icrm_team crm_team);

}
