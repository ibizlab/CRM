package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ievent_mail_registration;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[event_mail_registration] 服务对象接口
 */
public interface Ievent_mail_registrationClientService{

    public Ievent_mail_registration createModel() ;

    public void update(Ievent_mail_registration event_mail_registration);

    public void remove(Ievent_mail_registration event_mail_registration);

    public void createBatch(List<Ievent_mail_registration> event_mail_registrations);

    public void removeBatch(List<Ievent_mail_registration> event_mail_registrations);

    public void updateBatch(List<Ievent_mail_registration> event_mail_registrations);

    public void create(Ievent_mail_registration event_mail_registration);

    public Page<Ievent_mail_registration> fetchDefault(SearchContext context);

    public void get(Ievent_mail_registration event_mail_registration);

    public Page<Ievent_mail_registration> select(SearchContext context);

    public void getDraft(Ievent_mail_registration event_mail_registration);

}
