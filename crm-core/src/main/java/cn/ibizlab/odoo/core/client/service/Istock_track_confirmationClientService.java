package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_track_confirmation;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_track_confirmation] 服务对象接口
 */
public interface Istock_track_confirmationClientService{

    public Istock_track_confirmation createModel() ;

    public void updateBatch(List<Istock_track_confirmation> stock_track_confirmations);

    public void createBatch(List<Istock_track_confirmation> stock_track_confirmations);

    public void removeBatch(List<Istock_track_confirmation> stock_track_confirmations);

    public Page<Istock_track_confirmation> fetchDefault(SearchContext context);

    public void create(Istock_track_confirmation stock_track_confirmation);

    public void update(Istock_track_confirmation stock_track_confirmation);

    public void get(Istock_track_confirmation stock_track_confirmation);

    public void remove(Istock_track_confirmation stock_track_confirmation);

    public Page<Istock_track_confirmation> select(SearchContext context);

    public void getDraft(Istock_track_confirmation stock_track_confirmation);

}
