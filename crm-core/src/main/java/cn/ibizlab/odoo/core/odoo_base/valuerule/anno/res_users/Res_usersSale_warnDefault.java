package cn.ibizlab.odoo.core.odoo_base.valuerule.anno.res_users;

import cn.ibizlab.odoo.core.odoo_base.valuerule.validator.res_users.Res_usersSale_warnDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Res_users
 * 属性：Sale_warn
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Res_usersSale_warnDefaultValidator.class})
public @interface Res_usersSale_warnDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
