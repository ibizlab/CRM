package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_mail_statistics;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_mail_statistics] 服务对象接口
 */
public interface Imail_mail_statisticsClientService{

    public Imail_mail_statistics createModel() ;

    public void remove(Imail_mail_statistics mail_mail_statistics);

    public Page<Imail_mail_statistics> fetchDefault(SearchContext context);

    public void updateBatch(List<Imail_mail_statistics> mail_mail_statistics);

    public void create(Imail_mail_statistics mail_mail_statistics);

    public void removeBatch(List<Imail_mail_statistics> mail_mail_statistics);

    public void createBatch(List<Imail_mail_statistics> mail_mail_statistics);

    public void update(Imail_mail_statistics mail_mail_statistics);

    public void get(Imail_mail_statistics mail_mail_statistics);

    public Page<Imail_mail_statistics> select(SearchContext context);

    public void getDraft(Imail_mail_statistics mail_mail_statistics);

}
