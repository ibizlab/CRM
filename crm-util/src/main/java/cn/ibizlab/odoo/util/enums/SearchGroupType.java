package cn.ibizlab.odoo.util.enums;

public enum SearchGroupType {
     /**
     * 与
     */
    AND,

    /**
     * 或
     */
    OR
}