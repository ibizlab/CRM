package cn.ibizlab.odoo.util.service;

import cn.ibizlab.odoo.util.domain.IBZUSER;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[IBZUSER] 服务对象接口实现
 */
public  interface IBZUSERService extends IService<IBZUSER> {

}