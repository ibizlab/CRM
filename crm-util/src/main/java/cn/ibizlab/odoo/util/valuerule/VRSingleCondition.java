package cn.ibizlab.odoo.util.valuerule;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 属性值规则单项条件（VRSingleCondition）
 *
 * @param <T>
 */
@Slf4j
@Data
public abstract class VRSingleCondition<T> extends VRCondition<T> {
    public VRSingleCondition(String name, Boolean isNotMode, String ruleInfo, T value) {
        super(name, isNotMode, ruleInfo, value);
    }
}
