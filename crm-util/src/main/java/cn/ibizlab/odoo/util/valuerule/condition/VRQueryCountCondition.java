package cn.ibizlab.odoo.util.valuerule.condition;

import lombok.Data;

import cn.ibizlab.odoo.util.valuerule.VRSingleCondition;
import cn.ibizlab.odoo.util.log.IBIZLog;

import lombok.extern.slf4j.Slf4j;

/**
 * 查询计数条件。
 * 暂未开放
 *
 * @param <T>
 */
@Slf4j
@Data
@IBIZLog
public class VRQueryCountCondition<T> extends VRSingleCondition<T> {
    public VRQueryCountCondition(String name, Boolean isNotMode, String ruleInfo, T value) {
        super(name, isNotMode, ruleInfo, value);
    }

    public VRQueryCountCondition<T> init(){
        return this;
    }
    @Override
    public boolean validate() {
        return true;
    }
}
