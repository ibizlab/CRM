package cn.ibizlab.odoo.util;

import java.util.List;

import lombok.Data;

@Data
public class SearchResult<T> {

    /**
	 * 每页数量
	 */
	int pageSize ;
	
    /**
     * 当前页数
     */
    int currentPage ;
    
    /**
     * 排序方向
     */
    String[] sortDir ;
    
    /**
     * 排序字段
     */
    String[] sort ;
    
    /**
     * 记录总数
     */
    private long sizeCount;
    
    /**
     * 页码总数
     */
    private int pageCount;
    
    /**
     * 数据
     */
    private List<T> records;

}
