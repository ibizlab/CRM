package cn.ibizlab.odoo.util.valuerule;

import lombok.Data;
import org.springframework.util.StringUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public abstract class VRCondition<T> {
    protected String name;

    protected String ruleInfo;
//    //属性名
//    protected String field;
    //值
    protected T value;
    protected Boolean isNotMode;

    public VRCondition(String name, Boolean isNotMode, String ruleInfo, T value) {
        this.name = name;
        this.ruleInfo = ruleInfo;
        this.value = value;
        this.isNotMode = isNotMode;
    }

    /**
     * 单条件逻辑判断。（包括逻辑取反）
     *
     * @return
     */

    public boolean isValid() {
        boolean isValid = validate();

        //如果是逻辑取反
        return isNotMode ? (!isValid) : isValid;
    }

    /**
     * 基础 值规则单项条件判断
     *
     * @return
     */
    public abstract boolean validate();


    public String getMessage() {
        String msg = isNotMode ? "[逻辑取反]" : "";
        if (StringUtils.isEmpty(ruleInfo)) {
            msg += name;
        } else {
            msg += ruleInfo;

        }
        return "值规则校验失败:【" + msg + "】";
    }
}
