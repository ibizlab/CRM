package cn.ibizlab.odoo.util.web;

public interface SessionConstants {

	public final static String SESSIONPREFIX = "SRFSESSION-";

	public final static String PERSONID = SESSIONPREFIX + "SRFPERSONID";

	public final static String USERNAME = SESSIONPREFIX + "SRFUSERNAME";
	
	public final static String USERMODE = SESSIONPREFIX + "SRFUSERMODE";

	public final static String ORGUSERID = SESSIONPREFIX + "SRFORGUSERID";

	public final static String ORGUSERNAME = SESSIONPREFIX + "SRFORGUSERNAME";

	public final static String ORGID = SESSIONPREFIX + "SRFORGID";

	public final static String ORGNAME = SESSIONPREFIX + "SRFORGNAME";
	
	public final static String ORGSECTORID = SESSIONPREFIX + "SRFORGSECTORID";

	public final static String ORGSECTORNAME = SESSIONPREFIX + "SRFORGSECTORNAME";

}

