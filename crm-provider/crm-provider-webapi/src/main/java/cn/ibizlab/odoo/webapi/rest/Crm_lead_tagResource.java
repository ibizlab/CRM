package cn.ibizlab.odoo.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.webapi.dto.*;
import cn.ibizlab.odoo.webapi.mapping.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead_tag;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_lead_tagService;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lead_tagSearchContext;




@Slf4j
@Api(tags = {"Crm_lead_tag" })
@RestController("WebApi-crm_lead_tag")
@RequestMapping("")
public class Crm_lead_tagResource {

    @Autowired
    private ICrm_lead_tagService crm_lead_tagService;

    @Autowired
    @Lazy
    private Crm_lead_tagMapping crm_lead_tagMapping;







    @ApiOperation(value = "获取草稿数据", tags = {"Crm_lead_tag" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_lead_tags/getdraft")
    public ResponseEntity<Crm_lead_tagDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead_tagMapping.toDto(crm_lead_tagService.getDraft(new Crm_lead_tag())));
    }










    @PreAuthorize("hasPermission('Remove',{#crm_lead_tag_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Crm_lead_tag" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead_tags/{crm_lead_tag_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("crm_lead_tag_id") Integer crm_lead_tag_id) {
         return ResponseEntity.status(HttpStatus.OK).body(crm_lead_tagService.remove(crm_lead_tag_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Crm_lead_tag" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead_tags/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        crm_lead_tagService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#crm_lead_tag_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Crm_lead_tag" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_lead_tags/{crm_lead_tag_id}")

    public ResponseEntity<Crm_lead_tagDTO> update(@PathVariable("crm_lead_tag_id") Integer crm_lead_tag_id, @RequestBody Crm_lead_tagDTO crm_lead_tagdto) {
		Crm_lead_tag domain = crm_lead_tagMapping.toDomain(crm_lead_tagdto);
        domain.setId(crm_lead_tag_id);
		crm_lead_tagService.update(domain);
		Crm_lead_tagDTO dto = crm_lead_tagMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#crm_lead_tag_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Crm_lead_tag" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_lead_tags/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_lead_tagDTO> crm_lead_tagdtos) {
        crm_lead_tagService.updateBatch(crm_lead_tagMapping.toDomain(crm_lead_tagdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#crm_lead_tag_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Crm_lead_tag" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_lead_tags/{crm_lead_tag_id}")
    public ResponseEntity<Crm_lead_tagDTO> get(@PathVariable("crm_lead_tag_id") Integer crm_lead_tag_id) {
        Crm_lead_tag domain = crm_lead_tagService.get(crm_lead_tag_id);
        Crm_lead_tagDTO dto = crm_lead_tagMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Crm_lead_tag" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead_tags")

    public ResponseEntity<Crm_lead_tagDTO> create(@RequestBody Crm_lead_tagDTO crm_lead_tagdto) {
        Crm_lead_tag domain = crm_lead_tagMapping.toDomain(crm_lead_tagdto);
		crm_lead_tagService.create(domain);
        Crm_lead_tagDTO dto = crm_lead_tagMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }
    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Crm_lead_tag" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead_tags/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Crm_lead_tagDTO> crm_lead_tagdtos) {
        crm_lead_tagService.createBatch(crm_lead_tagMapping.toDomain(crm_lead_tagdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Crm_lead_tag-Default-all')")
	@ApiOperation(value = "fetch默认查询", tags = {"Crm_lead_tag" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/crm_lead_tags/fetchdefault")
	public ResponseEntity<List<Crm_lead_tagDTO>> fetchDefault(Crm_lead_tagSearchContext context) {
        Page<Crm_lead_tag> domains = crm_lead_tagService.searchDefault(context) ;
        List<Crm_lead_tagDTO> list = crm_lead_tagMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Crm_lead_tag-Default-all')")
	@ApiOperation(value = "search默认查询", tags = {"Crm_lead_tag" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.POST , value="/crm_lead_tags/searchdefault")
	public ResponseEntity<Page<Crm_lead_tagDTO>> searchDefault(@RequestBody Crm_lead_tagSearchContext context) {
        Page<Crm_lead_tag> domains = crm_lead_tagService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(crm_lead_tagMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Crm_lead_tag getEntity(){
        return new Crm_lead_tag();
    }

}
