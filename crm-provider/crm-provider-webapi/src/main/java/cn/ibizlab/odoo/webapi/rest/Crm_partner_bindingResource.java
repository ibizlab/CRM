package cn.ibizlab.odoo.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.webapi.dto.*;
import cn.ibizlab.odoo.webapi.mapping.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_partner_binding;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_partner_bindingService;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_partner_bindingSearchContext;




@Slf4j
@Api(tags = {"Crm_partner_binding" })
@RestController("WebApi-crm_partner_binding")
@RequestMapping("")
public class Crm_partner_bindingResource {

    @Autowired
    private ICrm_partner_bindingService crm_partner_bindingService;

    @Autowired
    @Lazy
    private Crm_partner_bindingMapping crm_partner_bindingMapping;







    @ApiOperation(value = "获取草稿数据", tags = {"Crm_partner_binding" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_partner_bindings/getdraft")
    public ResponseEntity<Crm_partner_bindingDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(crm_partner_bindingMapping.toDto(crm_partner_bindingService.getDraft(new Crm_partner_binding())));
    }




    @PreAuthorize("hasPermission(#crm_partner_binding_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Crm_partner_binding" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_partner_bindings/{crm_partner_binding_id}")
    public ResponseEntity<Crm_partner_bindingDTO> get(@PathVariable("crm_partner_binding_id") Integer crm_partner_binding_id) {
        Crm_partner_binding domain = crm_partner_bindingService.get(crm_partner_binding_id);
        Crm_partner_bindingDTO dto = crm_partner_bindingMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @PreAuthorize("hasPermission(#crm_partner_binding_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Crm_partner_binding" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_partner_bindings/{crm_partner_binding_id}")

    public ResponseEntity<Crm_partner_bindingDTO> update(@PathVariable("crm_partner_binding_id") Integer crm_partner_binding_id, @RequestBody Crm_partner_bindingDTO crm_partner_bindingdto) {
		Crm_partner_binding domain = crm_partner_bindingMapping.toDomain(crm_partner_bindingdto);
        domain.setId(crm_partner_binding_id);
		crm_partner_bindingService.update(domain);
		Crm_partner_bindingDTO dto = crm_partner_bindingMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#crm_partner_binding_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Crm_partner_binding" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_partner_bindings/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_partner_bindingDTO> crm_partner_bindingdtos) {
        crm_partner_bindingService.updateBatch(crm_partner_bindingMapping.toDomain(crm_partner_bindingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Crm_partner_binding" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_partner_bindings")

    public ResponseEntity<Crm_partner_bindingDTO> create(@RequestBody Crm_partner_bindingDTO crm_partner_bindingdto) {
        Crm_partner_binding domain = crm_partner_bindingMapping.toDomain(crm_partner_bindingdto);
		crm_partner_bindingService.create(domain);
        Crm_partner_bindingDTO dto = crm_partner_bindingMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }
    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Crm_partner_binding" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_partner_bindings/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Crm_partner_bindingDTO> crm_partner_bindingdtos) {
        crm_partner_bindingService.createBatch(crm_partner_bindingMapping.toDomain(crm_partner_bindingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#crm_partner_binding_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Crm_partner_binding" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_partner_bindings/{crm_partner_binding_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("crm_partner_binding_id") Integer crm_partner_binding_id) {
         return ResponseEntity.status(HttpStatus.OK).body(crm_partner_bindingService.remove(crm_partner_binding_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Crm_partner_binding" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_partner_bindings/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        crm_partner_bindingService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Crm_partner_binding-Default-all')")
	@ApiOperation(value = "fetch默认查询", tags = {"Crm_partner_binding" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/crm_partner_bindings/fetchdefault")
	public ResponseEntity<List<Crm_partner_bindingDTO>> fetchDefault(Crm_partner_bindingSearchContext context) {
        Page<Crm_partner_binding> domains = crm_partner_bindingService.searchDefault(context) ;
        List<Crm_partner_bindingDTO> list = crm_partner_bindingMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Crm_partner_binding-Default-all')")
	@ApiOperation(value = "search默认查询", tags = {"Crm_partner_binding" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.POST , value="/crm_partner_bindings/searchdefault")
	public ResponseEntity<Page<Crm_partner_bindingDTO>> searchDefault(@RequestBody Crm_partner_bindingSearchContext context) {
        Page<Crm_partner_binding> domains = crm_partner_bindingService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(crm_partner_bindingMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Crm_partner_binding getEntity(){
        return new Crm_partner_binding();
    }

}
