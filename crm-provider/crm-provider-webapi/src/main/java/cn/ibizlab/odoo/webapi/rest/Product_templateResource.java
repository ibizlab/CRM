package cn.ibizlab.odoo.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.webapi.dto.*;
import cn.ibizlab.odoo.webapi.mapping.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_template;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_templateService;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_templateSearchContext;




@Slf4j
@Api(tags = {"Product_template" })
@RestController("WebApi-product_template")
@RequestMapping("")
public class Product_templateResource {

    @Autowired
    private IProduct_templateService product_templateService;

    @Autowired
    @Lazy
    private Product_templateMapping product_templateMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Product_template" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates")

    public ResponseEntity<Product_templateDTO> create(@RequestBody Product_templateDTO product_templatedto) {
        Product_template domain = product_templateMapping.toDomain(product_templatedto);
		product_templateService.create(domain);
        Product_templateDTO dto = product_templateMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }
    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Product_template" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_templateDTO> product_templatedtos) {
        product_templateService.createBatch(product_templateMapping.toDomain(product_templatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#product_template_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Product_template" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_templates/{product_template_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_template_id") Integer product_template_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_templateService.remove(product_template_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Product_template" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_templates/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        product_templateService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#product_template_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Product_template" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/product_templates/{product_template_id}")
    public ResponseEntity<Product_templateDTO> get(@PathVariable("product_template_id") Integer product_template_id) {
        Product_template domain = product_templateService.get(product_template_id);
        Product_templateDTO dto = product_templateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @ApiOperation(value = "获取草稿数据", tags = {"Product_template" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/product_templates/getdraft")
    public ResponseEntity<Product_templateDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(product_templateMapping.toDto(product_templateService.getDraft(new Product_template())));
    }







    @PreAuthorize("hasPermission(#product_template_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Product_template" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_templates/{product_template_id}")

    public ResponseEntity<Product_templateDTO> update(@PathVariable("product_template_id") Integer product_template_id, @RequestBody Product_templateDTO product_templatedto) {
		Product_template domain = product_templateMapping.toDomain(product_templatedto);
        domain.setId(product_template_id);
		product_templateService.update(domain);
		Product_templateDTO dto = product_templateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#product_template_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Product_template" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_templates/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_templateDTO> product_templatedtos) {
        product_templateService.updateBatch(product_templateMapping.toDomain(product_templatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Product_template-Default-all')")
	@ApiOperation(value = "fetch默认查询", tags = {"Product_template" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_templates/fetchdefault")
	public ResponseEntity<List<Product_templateDTO>> fetchDefault(Product_templateSearchContext context) {
        Page<Product_template> domains = product_templateService.searchDefault(context) ;
        List<Product_templateDTO> list = product_templateMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Product_template-Default-all')")
	@ApiOperation(value = "search默认查询", tags = {"Product_template" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.POST , value="/product_templates/searchdefault")
	public ResponseEntity<Page<Product_templateDTO>> searchDefault(@RequestBody Product_templateSearchContext context) {
        Page<Product_template> domains = product_templateService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_templateMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Product_template getEntity(){
        return new Product_template();
    }

}
