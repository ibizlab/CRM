package cn.ibizlab.odoo.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Sale_order_lineDTO]
 */
@Data
public class Sale_order_lineDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [IS_EXPENSE]
     *
     */
    @JSONField(name = "is_expense")
    @JsonProperty("is_expense")
    private String isExpense;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [QTY_TO_INVOICE]
     *
     */
    @JSONField(name = "qty_to_invoice")
    @JsonProperty("qty_to_invoice")
    private Double qtyToInvoice;

    /**
     * 属性 [ANALYTIC_TAG_IDS]
     *
     */
    @JSONField(name = "analytic_tag_ids")
    @JsonProperty("analytic_tag_ids")
    private String analyticTagIds;

    /**
     * 属性 [WARNING_STOCK]
     *
     */
    @JSONField(name = "warning_stock")
    @JsonProperty("warning_stock")
    private String warningStock;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [PRICE_REDUCE_TAXEXCL]
     *
     */
    @JSONField(name = "price_reduce_taxexcl")
    @JsonProperty("price_reduce_taxexcl")
    private Double priceReduceTaxexcl;

    /**
     * 属性 [PRODUCT_CUSTOM_ATTRIBUTE_VALUE_IDS]
     *
     */
    @JSONField(name = "product_custom_attribute_value_ids")
    @JsonProperty("product_custom_attribute_value_ids")
    private String productCustomAttributeValueIds;

    /**
     * 属性 [QTY_DELIVERED_MANUAL]
     *
     */
    @JSONField(name = "qty_delivered_manual")
    @JsonProperty("qty_delivered_manual")
    private Double qtyDeliveredManual;

    /**
     * 属性 [PRICE_TAX]
     *
     */
    @JSONField(name = "price_tax")
    @JsonProperty("price_tax")
    private Double priceTax;

    /**
     * 属性 [QTY_DELIVERED_METHOD]
     *
     */
    @JSONField(name = "qty_delivered_method")
    @JsonProperty("qty_delivered_method")
    private String qtyDeliveredMethod;

    /**
     * 属性 [INVOICE_STATUS]
     *
     */
    @JSONField(name = "invoice_status")
    @JsonProperty("invoice_status")
    private String invoiceStatus;

    /**
     * 属性 [DISPLAY_TYPE]
     *
     */
    @JSONField(name = "display_type")
    @JsonProperty("display_type")
    private String displayType;

    /**
     * 属性 [MOVE_IDS]
     *
     */
    @JSONField(name = "move_ids")
    @JsonProperty("move_ids")
    private String moveIds;

    /**
     * 属性 [UNTAXED_AMOUNT_INVOICED]
     *
     */
    @JSONField(name = "untaxed_amount_invoiced")
    @JsonProperty("untaxed_amount_invoiced")
    private Double untaxedAmountInvoiced;

    /**
     * 属性 [PRODUCT_NO_VARIANT_ATTRIBUTE_VALUE_IDS]
     *
     */
    @JSONField(name = "product_no_variant_attribute_value_ids")
    @JsonProperty("product_no_variant_attribute_value_ids")
    private String productNoVariantAttributeValueIds;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [IS_DOWNPAYMENT]
     *
     */
    @JSONField(name = "is_downpayment")
    @JsonProperty("is_downpayment")
    private String isDownpayment;

    /**
     * 属性 [ANALYTIC_LINE_IDS]
     *
     */
    @JSONField(name = "analytic_line_ids")
    @JsonProperty("analytic_line_ids")
    private String analyticLineIds;

    /**
     * 属性 [PRICE_REDUCE_TAXINC]
     *
     */
    @JSONField(name = "price_reduce_taxinc")
    @JsonProperty("price_reduce_taxinc")
    private Double priceReduceTaxinc;

    /**
     * 属性 [QTY_DELIVERED]
     *
     */
    @JSONField(name = "qty_delivered")
    @JsonProperty("qty_delivered")
    private Double qtyDelivered;

    /**
     * 属性 [DISCOUNT]
     *
     */
    @JSONField(name = "discount")
    @JsonProperty("discount")
    private Double discount;

    /**
     * 属性 [PRODUCT_UOM_QTY]
     *
     */
    @JSONField(name = "product_uom_qty")
    @JsonProperty("product_uom_qty")
    private Double productUomQty;

    /**
     * 属性 [PRICE_REDUCE]
     *
     */
    @JSONField(name = "price_reduce")
    @JsonProperty("price_reduce")
    private Double priceReduce;

    /**
     * 属性 [CUSTOMER_LEAD]
     *
     */
    @JSONField(name = "customer_lead")
    @JsonProperty("customer_lead")
    private Double customerLead;

    /**
     * 属性 [PRODUCT_UPDATABLE]
     *
     */
    @JSONField(name = "product_updatable")
    @JsonProperty("product_updatable")
    private String productUpdatable;

    /**
     * 属性 [QTY_INVOICED]
     *
     */
    @JSONField(name = "qty_invoiced")
    @JsonProperty("qty_invoiced")
    private Double qtyInvoiced;

    /**
     * 属性 [UNTAXED_AMOUNT_TO_INVOICE]
     *
     */
    @JSONField(name = "untaxed_amount_to_invoice")
    @JsonProperty("untaxed_amount_to_invoice")
    private Double untaxedAmountToInvoice;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [PRICE_UNIT]
     *
     */
    @JSONField(name = "price_unit")
    @JsonProperty("price_unit")
    private Double priceUnit;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [PURCHASE_LINE_COUNT]
     *
     */
    @JSONField(name = "purchase_line_count")
    @JsonProperty("purchase_line_count")
    private Integer purchaseLineCount;

    /**
     * 属性 [NAME_SHORT]
     *
     */
    @JSONField(name = "name_short")
    @JsonProperty("name_short")
    private String nameShort;

    /**
     * 属性 [OPTION_LINE_IDS]
     *
     */
    @JSONField(name = "option_line_ids")
    @JsonProperty("option_line_ids")
    private String optionLineIds;

    /**
     * 属性 [TAX_ID]
     *
     */
    @JSONField(name = "tax_id")
    @JsonProperty("tax_id")
    private String taxId;

    /**
     * 属性 [SALE_ORDER_OPTION_IDS]
     *
     */
    @JSONField(name = "sale_order_option_ids")
    @JsonProperty("sale_order_option_ids")
    private String saleOrderOptionIds;

    /**
     * 属性 [PRICE_SUBTOTAL]
     *
     */
    @JSONField(name = "price_subtotal")
    @JsonProperty("price_subtotal")
    private Double priceSubtotal;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [PRICE_TOTAL]
     *
     */
    @JSONField(name = "price_total")
    @JsonProperty("price_total")
    private Double priceTotal;

    /**
     * 属性 [INVOICE_LINES]
     *
     */
    @JSONField(name = "invoice_lines")
    @JsonProperty("invoice_lines")
    private String invoiceLines;

    /**
     * 属性 [PURCHASE_LINE_IDS]
     *
     */
    @JSONField(name = "purchase_line_ids")
    @JsonProperty("purchase_line_ids")
    private String purchaseLineIds;

    /**
     * 属性 [EVENT_OK]
     *
     */
    @JSONField(name = "event_ok")
    @JsonProperty("event_ok")
    private String eventOk;

    /**
     * 属性 [EVENT_TICKET_ID_TEXT]
     *
     */
    @JSONField(name = "event_ticket_id_text")
    @JsonProperty("event_ticket_id_text")
    private String eventTicketIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [LINKED_LINE_ID_TEXT]
     *
     */
    @JSONField(name = "linked_line_id_text")
    @JsonProperty("linked_line_id_text")
    private String linkedLineIdText;

    /**
     * 属性 [ORDER_PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "order_partner_id_text")
    @JsonProperty("order_partner_id_text")
    private String orderPartnerIdText;

    /**
     * 属性 [PRODUCT_IMAGE]
     *
     */
    @JSONField(name = "product_image")
    @JsonProperty("product_image")
    private byte[] productImage;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;

    /**
     * 属性 [PRODUCT_PACKAGING_TEXT]
     *
     */
    @JSONField(name = "product_packaging_text")
    @JsonProperty("product_packaging_text")
    private String productPackagingText;

    /**
     * 属性 [ORDER_ID_TEXT]
     *
     */
    @JSONField(name = "order_id_text")
    @JsonProperty("order_id_text")
    private String orderIdText;

    /**
     * 属性 [EVENT_ID_TEXT]
     *
     */
    @JSONField(name = "event_id_text")
    @JsonProperty("event_id_text")
    private String eventIdText;

    /**
     * 属性 [SALESMAN_ID_TEXT]
     *
     */
    @JSONField(name = "salesman_id_text")
    @JsonProperty("salesman_id_text")
    private String salesmanIdText;

    /**
     * 属性 [PRODUCT_UOM_TEXT]
     *
     */
    @JSONField(name = "product_uom_text")
    @JsonProperty("product_uom_text")
    private String productUomText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 属性 [ROUTE_ID_TEXT]
     *
     */
    @JSONField(name = "route_id_text")
    @JsonProperty("route_id_text")
    private String routeIdText;

    /**
     * 属性 [SALESMAN_ID]
     *
     */
    @JSONField(name = "salesman_id")
    @JsonProperty("salesman_id")
    private Integer salesmanId;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 属性 [ORDER_ID]
     *
     */
    @JSONField(name = "order_id")
    @JsonProperty("order_id")
    private Integer orderId;

    /**
     * 属性 [EVENT_ID]
     *
     */
    @JSONField(name = "event_id")
    @JsonProperty("event_id")
    private Integer eventId;

    /**
     * 属性 [LINKED_LINE_ID]
     *
     */
    @JSONField(name = "linked_line_id")
    @JsonProperty("linked_line_id")
    private Integer linkedLineId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [ROUTE_ID]
     *
     */
    @JSONField(name = "route_id")
    @JsonProperty("route_id")
    private Integer routeId;

    /**
     * 属性 [ORDER_PARTNER_ID]
     *
     */
    @JSONField(name = "order_partner_id")
    @JsonProperty("order_partner_id")
    private Integer orderPartnerId;

    /**
     * 属性 [PRODUCT_UOM]
     *
     */
    @JSONField(name = "product_uom")
    @JsonProperty("product_uom")
    private Integer productUom;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Integer productId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 属性 [PRODUCT_PACKAGING]
     *
     */
    @JSONField(name = "product_packaging")
    @JsonProperty("product_packaging")
    private Integer productPackaging;

    /**
     * 属性 [EVENT_TICKET_ID]
     *
     */
    @JSONField(name = "event_ticket_id")
    @JsonProperty("event_ticket_id")
    private Integer eventTicketId;


    /**
     * 设置 [IS_EXPENSE]
     */
    public void setIsExpense(String  isExpense){
        this.isExpense = isExpense ;
        this.modify("is_expense",isExpense);
    }

    /**
     * 设置 [QTY_TO_INVOICE]
     */
    public void setQtyToInvoice(Double  qtyToInvoice){
        this.qtyToInvoice = qtyToInvoice ;
        this.modify("qty_to_invoice",qtyToInvoice);
    }

    /**
     * 设置 [WARNING_STOCK]
     */
    public void setWarningStock(String  warningStock){
        this.warningStock = warningStock ;
        this.modify("warning_stock",warningStock);
    }

    /**
     * 设置 [PRICE_REDUCE_TAXEXCL]
     */
    public void setPriceReduceTaxexcl(Double  priceReduceTaxexcl){
        this.priceReduceTaxexcl = priceReduceTaxexcl ;
        this.modify("price_reduce_taxexcl",priceReduceTaxexcl);
    }

    /**
     * 设置 [QTY_DELIVERED_MANUAL]
     */
    public void setQtyDeliveredManual(Double  qtyDeliveredManual){
        this.qtyDeliveredManual = qtyDeliveredManual ;
        this.modify("qty_delivered_manual",qtyDeliveredManual);
    }

    /**
     * 设置 [PRICE_TAX]
     */
    public void setPriceTax(Double  priceTax){
        this.priceTax = priceTax ;
        this.modify("price_tax",priceTax);
    }

    /**
     * 设置 [QTY_DELIVERED_METHOD]
     */
    public void setQtyDeliveredMethod(String  qtyDeliveredMethod){
        this.qtyDeliveredMethod = qtyDeliveredMethod ;
        this.modify("qty_delivered_method",qtyDeliveredMethod);
    }

    /**
     * 设置 [INVOICE_STATUS]
     */
    public void setInvoiceStatus(String  invoiceStatus){
        this.invoiceStatus = invoiceStatus ;
        this.modify("invoice_status",invoiceStatus);
    }

    /**
     * 设置 [DISPLAY_TYPE]
     */
    public void setDisplayType(String  displayType){
        this.displayType = displayType ;
        this.modify("display_type",displayType);
    }

    /**
     * 设置 [UNTAXED_AMOUNT_INVOICED]
     */
    public void setUntaxedAmountInvoiced(Double  untaxedAmountInvoiced){
        this.untaxedAmountInvoiced = untaxedAmountInvoiced ;
        this.modify("untaxed_amount_invoiced",untaxedAmountInvoiced);
    }

    /**
     * 设置 [IS_DOWNPAYMENT]
     */
    public void setIsDownpayment(String  isDownpayment){
        this.isDownpayment = isDownpayment ;
        this.modify("is_downpayment",isDownpayment);
    }

    /**
     * 设置 [PRICE_REDUCE_TAXINC]
     */
    public void setPriceReduceTaxinc(Double  priceReduceTaxinc){
        this.priceReduceTaxinc = priceReduceTaxinc ;
        this.modify("price_reduce_taxinc",priceReduceTaxinc);
    }

    /**
     * 设置 [QTY_DELIVERED]
     */
    public void setQtyDelivered(Double  qtyDelivered){
        this.qtyDelivered = qtyDelivered ;
        this.modify("qty_delivered",qtyDelivered);
    }

    /**
     * 设置 [DISCOUNT]
     */
    public void setDiscount(Double  discount){
        this.discount = discount ;
        this.modify("discount",discount);
    }

    /**
     * 设置 [PRODUCT_UOM_QTY]
     */
    public void setProductUomQty(Double  productUomQty){
        this.productUomQty = productUomQty ;
        this.modify("product_uom_qty",productUomQty);
    }

    /**
     * 设置 [PRICE_REDUCE]
     */
    public void setPriceReduce(Double  priceReduce){
        this.priceReduce = priceReduce ;
        this.modify("price_reduce",priceReduce);
    }

    /**
     * 设置 [CUSTOMER_LEAD]
     */
    public void setCustomerLead(Double  customerLead){
        this.customerLead = customerLead ;
        this.modify("customer_lead",customerLead);
    }

    /**
     * 设置 [QTY_INVOICED]
     */
    public void setQtyInvoiced(Double  qtyInvoiced){
        this.qtyInvoiced = qtyInvoiced ;
        this.modify("qty_invoiced",qtyInvoiced);
    }

    /**
     * 设置 [UNTAXED_AMOUNT_TO_INVOICE]
     */
    public void setUntaxedAmountToInvoice(Double  untaxedAmountToInvoice){
        this.untaxedAmountToInvoice = untaxedAmountToInvoice ;
        this.modify("untaxed_amount_to_invoice",untaxedAmountToInvoice);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [PRICE_UNIT]
     */
    public void setPriceUnit(Double  priceUnit){
        this.priceUnit = priceUnit ;
        this.modify("price_unit",priceUnit);
    }

    /**
     * 设置 [PRICE_SUBTOTAL]
     */
    public void setPriceSubtotal(Double  priceSubtotal){
        this.priceSubtotal = priceSubtotal ;
        this.modify("price_subtotal",priceSubtotal);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [PRICE_TOTAL]
     */
    public void setPriceTotal(Double  priceTotal){
        this.priceTotal = priceTotal ;
        this.modify("price_total",priceTotal);
    }

    /**
     * 设置 [SALESMAN_ID]
     */
    public void setSalesmanId(Integer  salesmanId){
        this.salesmanId = salesmanId ;
        this.modify("salesman_id",salesmanId);
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    public void setCurrencyId(Integer  currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [ORDER_ID]
     */
    public void setOrderId(Integer  orderId){
        this.orderId = orderId ;
        this.modify("order_id",orderId);
    }

    /**
     * 设置 [EVENT_ID]
     */
    public void setEventId(Integer  eventId){
        this.eventId = eventId ;
        this.modify("event_id",eventId);
    }

    /**
     * 设置 [LINKED_LINE_ID]
     */
    public void setLinkedLineId(Integer  linkedLineId){
        this.linkedLineId = linkedLineId ;
        this.modify("linked_line_id",linkedLineId);
    }

    /**
     * 设置 [ROUTE_ID]
     */
    public void setRouteId(Integer  routeId){
        this.routeId = routeId ;
        this.modify("route_id",routeId);
    }

    /**
     * 设置 [ORDER_PARTNER_ID]
     */
    public void setOrderPartnerId(Integer  orderPartnerId){
        this.orderPartnerId = orderPartnerId ;
        this.modify("order_partner_id",orderPartnerId);
    }

    /**
     * 设置 [PRODUCT_UOM]
     */
    public void setProductUom(Integer  productUom){
        this.productUom = productUom ;
        this.modify("product_uom",productUom);
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    public void setProductId(Integer  productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Integer  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [PRODUCT_PACKAGING]
     */
    public void setProductPackaging(Integer  productPackaging){
        this.productPackaging = productPackaging ;
        this.modify("product_packaging",productPackaging);
    }

    /**
     * 设置 [EVENT_TICKET_ID]
     */
    public void setEventTicketId(Integer  eventTicketId){
        this.eventTicketId = eventTicketId ;
        this.modify("event_ticket_id",eventTicketId);
    }


}

