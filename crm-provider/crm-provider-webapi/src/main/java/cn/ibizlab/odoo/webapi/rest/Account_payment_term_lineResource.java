package cn.ibizlab.odoo.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.webapi.dto.*;
import cn.ibizlab.odoo.webapi.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_term_line;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_payment_term_lineService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_payment_term_lineSearchContext;




@Slf4j
@Api(tags = {"Account_payment_term_line" })
@RestController("WebApi-account_payment_term_line")
@RequestMapping("")
public class Account_payment_term_lineResource {

    @Autowired
    private IAccount_payment_term_lineService account_payment_term_lineService;

    @Autowired
    @Lazy
    private Account_payment_term_lineMapping account_payment_term_lineMapping;




    @PreAuthorize("hasPermission(#account_payment_term_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_payment_term_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_payment_term_lines/{account_payment_term_line_id}")

    public ResponseEntity<Account_payment_term_lineDTO> update(@PathVariable("account_payment_term_line_id") Integer account_payment_term_line_id, @RequestBody Account_payment_term_lineDTO account_payment_term_linedto) {
		Account_payment_term_line domain = account_payment_term_lineMapping.toDomain(account_payment_term_linedto);
        domain.setId(account_payment_term_line_id);
		account_payment_term_lineService.update(domain);
		Account_payment_term_lineDTO dto = account_payment_term_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_payment_term_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_payment_term_line" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_payment_term_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_payment_term_lineDTO> account_payment_term_linedtos) {
        account_payment_term_lineService.updateBatch(account_payment_term_lineMapping.toDomain(account_payment_term_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_payment_term_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_payment_term_lines")

    public ResponseEntity<Account_payment_term_lineDTO> create(@RequestBody Account_payment_term_lineDTO account_payment_term_linedto) {
        Account_payment_term_line domain = account_payment_term_lineMapping.toDomain(account_payment_term_linedto);
		account_payment_term_lineService.create(domain);
        Account_payment_term_lineDTO dto = account_payment_term_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }
    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_payment_term_line" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_payment_term_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_payment_term_lineDTO> account_payment_term_linedtos) {
        account_payment_term_lineService.createBatch(account_payment_term_lineMapping.toDomain(account_payment_term_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#account_payment_term_line_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_payment_term_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_payment_term_lines/{account_payment_term_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_payment_term_line_id") Integer account_payment_term_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_payment_term_lineService.remove(account_payment_term_line_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_payment_term_line" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_payment_term_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_payment_term_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#account_payment_term_line_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_payment_term_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_payment_term_lines/{account_payment_term_line_id}")
    public ResponseEntity<Account_payment_term_lineDTO> get(@PathVariable("account_payment_term_line_id") Integer account_payment_term_line_id) {
        Account_payment_term_line domain = account_payment_term_lineService.get(account_payment_term_line_id);
        Account_payment_term_lineDTO dto = account_payment_term_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @ApiOperation(value = "获取草稿数据", tags = {"Account_payment_term_line" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_payment_term_lines/getdraft")
    public ResponseEntity<Account_payment_term_lineDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_payment_term_lineMapping.toDto(account_payment_term_lineService.getDraft(new Account_payment_term_line())));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Account_payment_term_line-Default-all')")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_payment_term_line" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_payment_term_lines/fetchdefault")
	public ResponseEntity<List<Account_payment_term_lineDTO>> fetchDefault(Account_payment_term_lineSearchContext context) {
        Page<Account_payment_term_line> domains = account_payment_term_lineService.searchDefault(context) ;
        List<Account_payment_term_lineDTO> list = account_payment_term_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Account_payment_term_line-Default-all')")
	@ApiOperation(value = "search默认查询", tags = {"Account_payment_term_line" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.POST , value="/account_payment_term_lines/searchdefault")
	public ResponseEntity<Page<Account_payment_term_lineDTO>> searchDefault(@RequestBody Account_payment_term_lineSearchContext context) {
        Page<Account_payment_term_line> domains = account_payment_term_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_payment_term_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_payment_term_line getEntity(){
        return new Account_payment_term_line();
    }

}
