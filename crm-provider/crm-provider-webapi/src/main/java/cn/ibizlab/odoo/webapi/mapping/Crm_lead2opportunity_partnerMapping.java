package cn.ibizlab.odoo.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead2opportunity_partner;
import cn.ibizlab.odoo.webapi.dto.Crm_lead2opportunity_partnerDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Crm_lead2opportunity_partnerMapping extends MappingBase<Crm_lead2opportunity_partnerDTO, Crm_lead2opportunity_partner> {


}

