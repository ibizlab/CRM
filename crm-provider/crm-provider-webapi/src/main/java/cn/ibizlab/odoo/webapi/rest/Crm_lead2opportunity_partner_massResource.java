package cn.ibizlab.odoo.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.webapi.dto.*;
import cn.ibizlab.odoo.webapi.mapping.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead2opportunity_partner_mass;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_lead2opportunity_partner_massService;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lead2opportunity_partner_massSearchContext;




@Slf4j
@Api(tags = {"Crm_lead2opportunity_partner_mass" })
@RestController("WebApi-crm_lead2opportunity_partner_mass")
@RequestMapping("")
public class Crm_lead2opportunity_partner_massResource {

    @Autowired
    private ICrm_lead2opportunity_partner_massService crm_lead2opportunity_partner_massService;

    @Autowired
    @Lazy
    private Crm_lead2opportunity_partner_massMapping crm_lead2opportunity_partner_massMapping;




    @ApiOperation(value = "获取草稿数据", tags = {"Crm_lead2opportunity_partner_mass" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_lead2opportunity_partner_masses/getdraft")
    public ResponseEntity<Crm_lead2opportunity_partner_massDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2opportunity_partner_massMapping.toDto(crm_lead2opportunity_partner_massService.getDraft(new Crm_lead2opportunity_partner_mass())));
    }










    @PreAuthorize("hasPermission(#crm_lead2opportunity_partner_mass_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Crm_lead2opportunity_partner_mass" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_lead2opportunity_partner_masses/{crm_lead2opportunity_partner_mass_id}")
    public ResponseEntity<Crm_lead2opportunity_partner_massDTO> get(@PathVariable("crm_lead2opportunity_partner_mass_id") Integer crm_lead2opportunity_partner_mass_id) {
        Crm_lead2opportunity_partner_mass domain = crm_lead2opportunity_partner_massService.get(crm_lead2opportunity_partner_mass_id);
        Crm_lead2opportunity_partner_massDTO dto = crm_lead2opportunity_partner_massMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Crm_lead2opportunity_partner_mass" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead2opportunity_partner_masses")

    public ResponseEntity<Crm_lead2opportunity_partner_massDTO> create(@RequestBody Crm_lead2opportunity_partner_massDTO crm_lead2opportunity_partner_massdto) {
        Crm_lead2opportunity_partner_mass domain = crm_lead2opportunity_partner_massMapping.toDomain(crm_lead2opportunity_partner_massdto);
		crm_lead2opportunity_partner_massService.create(domain);
        Crm_lead2opportunity_partner_massDTO dto = crm_lead2opportunity_partner_massMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }
    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Crm_lead2opportunity_partner_mass" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead2opportunity_partner_masses/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Crm_lead2opportunity_partner_massDTO> crm_lead2opportunity_partner_massdtos) {
        crm_lead2opportunity_partner_massService.createBatch(crm_lead2opportunity_partner_massMapping.toDomain(crm_lead2opportunity_partner_massdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#crm_lead2opportunity_partner_mass_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Crm_lead2opportunity_partner_mass" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead2opportunity_partner_masses/{crm_lead2opportunity_partner_mass_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("crm_lead2opportunity_partner_mass_id") Integer crm_lead2opportunity_partner_mass_id) {
         return ResponseEntity.status(HttpStatus.OK).body(crm_lead2opportunity_partner_massService.remove(crm_lead2opportunity_partner_mass_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Crm_lead2opportunity_partner_mass" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead2opportunity_partner_masses/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        crm_lead2opportunity_partner_massService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#crm_lead2opportunity_partner_mass_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Crm_lead2opportunity_partner_mass" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_lead2opportunity_partner_masses/{crm_lead2opportunity_partner_mass_id}")

    public ResponseEntity<Crm_lead2opportunity_partner_massDTO> update(@PathVariable("crm_lead2opportunity_partner_mass_id") Integer crm_lead2opportunity_partner_mass_id, @RequestBody Crm_lead2opportunity_partner_massDTO crm_lead2opportunity_partner_massdto) {
		Crm_lead2opportunity_partner_mass domain = crm_lead2opportunity_partner_massMapping.toDomain(crm_lead2opportunity_partner_massdto);
        domain.setId(crm_lead2opportunity_partner_mass_id);
		crm_lead2opportunity_partner_massService.update(domain);
		Crm_lead2opportunity_partner_massDTO dto = crm_lead2opportunity_partner_massMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#crm_lead2opportunity_partner_mass_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Crm_lead2opportunity_partner_mass" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_lead2opportunity_partner_masses/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_lead2opportunity_partner_massDTO> crm_lead2opportunity_partner_massdtos) {
        crm_lead2opportunity_partner_massService.updateBatch(crm_lead2opportunity_partner_massMapping.toDomain(crm_lead2opportunity_partner_massdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Crm_lead2opportunity_partner_mass-Default-all')")
	@ApiOperation(value = "fetch默认查询", tags = {"Crm_lead2opportunity_partner_mass" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/crm_lead2opportunity_partner_masses/fetchdefault")
	public ResponseEntity<List<Crm_lead2opportunity_partner_massDTO>> fetchDefault(Crm_lead2opportunity_partner_massSearchContext context) {
        Page<Crm_lead2opportunity_partner_mass> domains = crm_lead2opportunity_partner_massService.searchDefault(context) ;
        List<Crm_lead2opportunity_partner_massDTO> list = crm_lead2opportunity_partner_massMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Crm_lead2opportunity_partner_mass-Default-all')")
	@ApiOperation(value = "search默认查询", tags = {"Crm_lead2opportunity_partner_mass" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.POST , value="/crm_lead2opportunity_partner_masses/searchdefault")
	public ResponseEntity<Page<Crm_lead2opportunity_partner_massDTO>> searchDefault(@RequestBody Crm_lead2opportunity_partner_massSearchContext context) {
        Page<Crm_lead2opportunity_partner_mass> domains = crm_lead2opportunity_partner_massService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(crm_lead2opportunity_partner_massMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Crm_lead2opportunity_partner_mass getEntity(){
        return new Crm_lead2opportunity_partner_mass();
    }

}
