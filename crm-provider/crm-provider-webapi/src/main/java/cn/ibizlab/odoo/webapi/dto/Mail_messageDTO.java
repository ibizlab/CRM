package cn.ibizlab.odoo.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Mail_messageDTO]
 */
@Data
public class Mail_messageDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [NEEDACTION]
     *
     */
    @JSONField(name = "needaction")
    @JsonProperty("needaction")
    private String needaction;

    /**
     * 属性 [LAYOUT]
     *
     */
    @JSONField(name = "layout")
    @JsonProperty("layout")
    private String layout;

    /**
     * 属性 [MODEL]
     *
     */
    @JSONField(name = "model")
    @JsonProperty("model")
    private String model;

    /**
     * 属性 [NO_AUTO_THREAD]
     *
     */
    @JSONField(name = "no_auto_thread")
    @JsonProperty("no_auto_thread")
    private String noAutoThread;

    /**
     * 属性 [PARTNER_IDS]
     *
     */
    @JSONField(name = "partner_ids")
    @JsonProperty("partner_ids")
    private String partnerIds;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [RECORD_NAME]
     *
     */
    @JSONField(name = "record_name")
    @JsonProperty("record_name")
    private String recordName;

    /**
     * 属性 [MESSAGE_ID]
     *
     */
    @JSONField(name = "message_id")
    @JsonProperty("message_id")
    private String messageId;

    /**
     * 属性 [TRACKING_VALUE_IDS]
     *
     */
    @JSONField(name = "tracking_value_ids")
    @JsonProperty("tracking_value_ids")
    private String trackingValueIds;

    /**
     * 属性 [MODERATION_STATUS]
     *
     */
    @JSONField(name = "moderation_status")
    @JsonProperty("moderation_status")
    private String moderationStatus;

    /**
     * 属性 [NOTIFICATION_IDS]
     *
     */
    @JSONField(name = "notification_ids")
    @JsonProperty("notification_ids")
    private String notificationIds;

    /**
     * 属性 [EMAIL_FROM]
     *
     */
    @JSONField(name = "email_from")
    @JsonProperty("email_from")
    private String emailFrom;

    /**
     * 属性 [CHANNEL_IDS]
     *
     */
    @JSONField(name = "channel_ids")
    @JsonProperty("channel_ids")
    private String channelIds;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [STARRED]
     *
     */
    @JSONField(name = "starred")
    @JsonProperty("starred")
    private String starred;

    /**
     * 属性 [RATING_IDS]
     *
     */
    @JSONField(name = "rating_ids")
    @JsonProperty("rating_ids")
    private String ratingIds;

    /**
     * 属性 [NEEDACTION_PARTNER_IDS]
     *
     */
    @JSONField(name = "needaction_partner_ids")
    @JsonProperty("needaction_partner_ids")
    private String needactionPartnerIds;

    /**
     * 属性 [REPLY_TO]
     *
     */
    @JSONField(name = "reply_to")
    @JsonProperty("reply_to")
    private String replyTo;

    /**
     * 属性 [MAIL_SERVER_ID]
     *
     */
    @JSONField(name = "mail_server_id")
    @JsonProperty("mail_server_id")
    private Integer mailServerId;

    /**
     * 属性 [RES_ID]
     *
     */
    @JSONField(name = "res_id")
    @JsonProperty("res_id")
    private Integer resId;

    /**
     * 属性 [NEED_MODERATION]
     *
     */
    @JSONField(name = "need_moderation")
    @JsonProperty("need_moderation")
    private String needModeration;

    /**
     * 属性 [SUBJECT]
     *
     */
    @JSONField(name = "subject")
    @JsonProperty("subject")
    private String subject;

    /**
     * 属性 [BODY]
     *
     */
    @JSONField(name = "body")
    @JsonProperty("body")
    private String body;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [WEBSITE_PUBLISHED]
     *
     */
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private String websitePublished;

    /**
     * 属性 [ATTACHMENT_IDS]
     *
     */
    @JSONField(name = "attachment_ids")
    @JsonProperty("attachment_ids")
    private String attachmentIds;

    /**
     * 属性 [CHILD_IDS]
     *
     */
    @JSONField(name = "child_ids")
    @JsonProperty("child_ids")
    private String childIds;

    /**
     * 属性 [RATING_VALUE]
     *
     */
    @JSONField(name = "rating_value")
    @JsonProperty("rating_value")
    private Double ratingValue;

    /**
     * 属性 [ADD_SIGN]
     *
     */
    @JSONField(name = "add_sign")
    @JsonProperty("add_sign")
    private String addSign;

    /**
     * 属性 [STARRED_PARTNER_IDS]
     *
     */
    @JSONField(name = "starred_partner_ids")
    @JsonProperty("starred_partner_ids")
    private String starredPartnerIds;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 属性 [HAS_ERROR]
     *
     */
    @JSONField(name = "has_error")
    @JsonProperty("has_error")
    private String hasError;

    /**
     * 属性 [MESSAGE_TYPE]
     *
     */
    @JSONField(name = "message_type")
    @JsonProperty("message_type")
    private String messageType;

    /**
     * 属性 [AUTHOR_ID_TEXT]
     *
     */
    @JSONField(name = "author_id_text")
    @JsonProperty("author_id_text")
    private String authorIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [MAIL_ACTIVITY_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "mail_activity_type_id_text")
    @JsonProperty("mail_activity_type_id_text")
    private String mailActivityTypeIdText;

    /**
     * 属性 [MODERATOR_ID_TEXT]
     *
     */
    @JSONField(name = "moderator_id_text")
    @JsonProperty("moderator_id_text")
    private String moderatorIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [SUBTYPE_ID_TEXT]
     *
     */
    @JSONField(name = "subtype_id_text")
    @JsonProperty("subtype_id_text")
    private String subtypeIdText;

    /**
     * 属性 [AUTHOR_AVATAR]
     *
     */
    @JSONField(name = "author_avatar")
    @JsonProperty("author_avatar")
    private byte[] authorAvatar;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [MAIL_ACTIVITY_TYPE_ID]
     *
     */
    @JSONField(name = "mail_activity_type_id")
    @JsonProperty("mail_activity_type_id")
    private Integer mailActivityTypeId;

    /**
     * 属性 [MODERATOR_ID]
     *
     */
    @JSONField(name = "moderator_id")
    @JsonProperty("moderator_id")
    private Integer moderatorId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [AUTHOR_ID]
     *
     */
    @JSONField(name = "author_id")
    @JsonProperty("author_id")
    private Integer authorId;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    private Integer parentId;

    /**
     * 属性 [SUBTYPE_ID]
     *
     */
    @JSONField(name = "subtype_id")
    @JsonProperty("subtype_id")
    private Integer subtypeId;


    /**
     * 设置 [LAYOUT]
     */
    public void setLayout(String  layout){
        this.layout = layout ;
        this.modify("layout",layout);
    }

    /**
     * 设置 [MODEL]
     */
    public void setModel(String  model){
        this.model = model ;
        this.modify("model",model);
    }

    /**
     * 设置 [NO_AUTO_THREAD]
     */
    public void setNoAutoThread(String  noAutoThread){
        this.noAutoThread = noAutoThread ;
        this.modify("no_auto_thread",noAutoThread);
    }

    /**
     * 设置 [RECORD_NAME]
     */
    public void setRecordName(String  recordName){
        this.recordName = recordName ;
        this.modify("record_name",recordName);
    }

    /**
     * 设置 [MESSAGE_ID]
     */
    public void setMessageId(String  messageId){
        this.messageId = messageId ;
        this.modify("message_id",messageId);
    }

    /**
     * 设置 [MODERATION_STATUS]
     */
    public void setModerationStatus(String  moderationStatus){
        this.moderationStatus = moderationStatus ;
        this.modify("moderation_status",moderationStatus);
    }

    /**
     * 设置 [EMAIL_FROM]
     */
    public void setEmailFrom(String  emailFrom){
        this.emailFrom = emailFrom ;
        this.modify("email_from",emailFrom);
    }

    /**
     * 设置 [REPLY_TO]
     */
    public void setReplyTo(String  replyTo){
        this.replyTo = replyTo ;
        this.modify("reply_to",replyTo);
    }

    /**
     * 设置 [MAIL_SERVER_ID]
     */
    public void setMailServerId(Integer  mailServerId){
        this.mailServerId = mailServerId ;
        this.modify("mail_server_id",mailServerId);
    }

    /**
     * 设置 [RES_ID]
     */
    public void setResId(Integer  resId){
        this.resId = resId ;
        this.modify("res_id",resId);
    }

    /**
     * 设置 [SUBJECT]
     */
    public void setSubject(String  subject){
        this.subject = subject ;
        this.modify("subject",subject);
    }

    /**
     * 设置 [BODY]
     */
    public void setBody(String  body){
        this.body = body ;
        this.modify("body",body);
    }

    /**
     * 设置 [WEBSITE_PUBLISHED]
     */
    public void setWebsitePublished(String  websitePublished){
        this.websitePublished = websitePublished ;
        this.modify("website_published",websitePublished);
    }

    /**
     * 设置 [ADD_SIGN]
     */
    public void setAddSign(String  addSign){
        this.addSign = addSign ;
        this.modify("add_sign",addSign);
    }

    /**
     * 设置 [DATE]
     */
    public void setDate(Timestamp  date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 设置 [MESSAGE_TYPE]
     */
    public void setMessageType(String  messageType){
        this.messageType = messageType ;
        this.modify("message_type",messageType);
    }

    /**
     * 设置 [MAIL_ACTIVITY_TYPE_ID]
     */
    public void setMailActivityTypeId(Integer  mailActivityTypeId){
        this.mailActivityTypeId = mailActivityTypeId ;
        this.modify("mail_activity_type_id",mailActivityTypeId);
    }

    /**
     * 设置 [MODERATOR_ID]
     */
    public void setModeratorId(Integer  moderatorId){
        this.moderatorId = moderatorId ;
        this.modify("moderator_id",moderatorId);
    }

    /**
     * 设置 [AUTHOR_ID]
     */
    public void setAuthorId(Integer  authorId){
        this.authorId = authorId ;
        this.modify("author_id",authorId);
    }

    /**
     * 设置 [PARENT_ID]
     */
    public void setParentId(Integer  parentId){
        this.parentId = parentId ;
        this.modify("parent_id",parentId);
    }

    /**
     * 设置 [SUBTYPE_ID]
     */
    public void setSubtypeId(Integer  subtypeId){
        this.subtypeId = subtypeId ;
        this.modify("subtype_id",subtypeId);
    }


}

