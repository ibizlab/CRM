package cn.ibizlab.odoo.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead;
import cn.ibizlab.odoo.webapi.dto.Crm_leadDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Crm_leadMapping extends MappingBase<Crm_leadDTO, Crm_lead> {


}

