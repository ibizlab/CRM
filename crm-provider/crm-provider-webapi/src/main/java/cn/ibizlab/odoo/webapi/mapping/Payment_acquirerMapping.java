package cn.ibizlab.odoo.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_acquirer;
import cn.ibizlab.odoo.webapi.dto.Payment_acquirerDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Payment_acquirerMapping extends MappingBase<Payment_acquirerDTO, Payment_acquirer> {


}

