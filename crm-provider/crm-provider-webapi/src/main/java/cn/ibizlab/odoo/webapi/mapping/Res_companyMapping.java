package cn.ibizlab.odoo.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_company;
import cn.ibizlab.odoo.webapi.dto.Res_companyDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Res_companyMapping extends MappingBase<Res_companyDTO, Res_company> {


}

