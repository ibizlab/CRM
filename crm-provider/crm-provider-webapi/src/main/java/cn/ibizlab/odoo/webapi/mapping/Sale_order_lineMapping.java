package cn.ibizlab.odoo.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_line;
import cn.ibizlab.odoo.webapi.dto.Sale_order_lineDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Sale_order_lineMapping extends MappingBase<Sale_order_lineDTO, Sale_order_line> {


}

