package cn.ibizlab.odoo.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_activity_report;
import cn.ibizlab.odoo.webapi.dto.Crm_activity_reportDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Crm_activity_reportMapping extends MappingBase<Crm_activity_reportDTO, Crm_activity_report> {


}

