package cn.ibizlab.odoo.webapi.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.webapi")
public class WebApiRestConfiguration {

}
