package cn.ibizlab.odoo.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Res_partner_pDTO]
 */
@Data
public class Res_partner_pDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [IMAGE]
     *
     */
    @JSONField(name = "image")
    @JsonProperty("image")
    private byte[] image;

    /**
     * 属性 [TYPE]
     *
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;

    /**
     * 属性 [COLOR]
     *
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 属性 [PAYMENT_TOKEN_IDS]
     *
     */
    @JSONField(name = "payment_token_ids")
    @JsonProperty("payment_token_ids")
    private String paymentTokenIds;

    /**
     * 属性 [INVOICE_IDS]
     *
     */
    @JSONField(name = "invoice_ids")
    @JsonProperty("invoice_ids")
    private String invoiceIds;

    /**
     * 属性 [MEETING_COUNT]
     *
     */
    @JSONField(name = "meeting_count")
    @JsonProperty("meeting_count")
    private Integer meetingCount;

    /**
     * 属性 [SUPPLIER_INVOICE_COUNT]
     *
     */
    @JSONField(name = "supplier_invoice_count")
    @JsonProperty("supplier_invoice_count")
    private Integer supplierInvoiceCount;

    /**
     * 属性 [COMPANY_NAME]
     *
     */
    @JSONField(name = "company_name")
    @JsonProperty("company_name")
    private String companyName;

    /**
     * 属性 [WEBSITE_PUBLISHED]
     *
     */
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private String websitePublished;

    /**
     * 属性 [LAST_TIME_ENTRIES_CHECKED]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "last_time_entries_checked" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("last_time_entries_checked")
    private Timestamp lastTimeEntriesChecked;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 属性 [TRUST]
     *
     */
    @JSONField(name = "trust")
    @JsonProperty("trust")
    private String trust;

    /**
     * 属性 [IBIZFUNCTION]
     *
     */
    @JSONField(name = "ibizfunction")
    @JsonProperty("ibizfunction")
    private String ibizfunction;

    /**
     * 属性 [TOTAL_INVOICED]
     *
     */
    @JSONField(name = "total_invoiced")
    @JsonProperty("total_invoiced")
    private Double totalInvoiced;

    /**
     * 属性 [POS_ORDER_COUNT]
     *
     */
    @JSONField(name = "pos_order_count")
    @JsonProperty("pos_order_count")
    private Integer posOrderCount;

    /**
     * 属性 [CONTACT_ADDRESS]
     *
     */
    @JSONField(name = "contact_address")
    @JsonProperty("contact_address")
    private String contactAddress;

    /**
     * 属性 [INVOICE_WARN]
     *
     */
    @JSONField(name = "invoice_warn")
    @JsonProperty("invoice_warn")
    private String invoiceWarn;

    /**
     * 属性 [BANK_IDS]
     *
     */
    @JSONField(name = "bank_ids")
    @JsonProperty("bank_ids")
    private String bankIds;

    /**
     * 属性 [SIGNUP_EXPIRATION]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "signup_expiration" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("signup_expiration")
    private Timestamp signupExpiration;

    /**
     * 属性 [PURCHASE_ORDER_COUNT]
     *
     */
    @JSONField(name = "purchase_order_count")
    @JsonProperty("purchase_order_count")
    private Integer purchaseOrderCount;

    /**
     * 属性 [HAS_UNRECONCILED_ENTRIES]
     *
     */
    @JSONField(name = "has_unreconciled_entries")
    @JsonProperty("has_unreconciled_entries")
    private String hasUnreconciledEntries;

    /**
     * 属性 [CATEGORY_ID]
     *
     */
    @JSONField(name = "category_id")
    @JsonProperty("category_id")
    private String categoryId;

    /**
     * 属性 [WEBSITE_DESCRIPTION]
     *
     */
    @JSONField(name = "website_description")
    @JsonProperty("website_description")
    private String websiteDescription;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [MEETING_IDS]
     *
     */
    @JSONField(name = "meeting_ids")
    @JsonProperty("meeting_ids")
    private String meetingIds;

    /**
     * 属性 [EMPLOYEE]
     *
     */
    @JSONField(name = "employee")
    @JsonProperty("employee")
    private String employee;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [CHILD_IDS]
     *
     */
    @JSONField(name = "child_ids")
    @JsonProperty("child_ids")
    private String childIds;

    /**
     * 属性 [WEBSITE_META_DESCRIPTION]
     *
     */
    @JSONField(name = "website_meta_description")
    @JsonProperty("website_meta_description")
    private String websiteMetaDescription;

    /**
     * 属性 [IS_BLACKLISTED]
     *
     */
    @JSONField(name = "is_blacklisted")
    @JsonProperty("is_blacklisted")
    private String isBlacklisted;

    /**
     * 属性 [PROPERTY_PRODUCT_PRICELIST]
     *
     */
    @JSONField(name = "property_product_pricelist")
    @JsonProperty("property_product_pricelist")
    private Integer propertyProductPricelist;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 属性 [SIGNUP_TOKEN]
     *
     */
    @JSONField(name = "signup_token")
    @JsonProperty("signup_token")
    private String signupToken;

    /**
     * 属性 [REF_COMPANY_IDS]
     *
     */
    @JSONField(name = "ref_company_ids")
    @JsonProperty("ref_company_ids")
    private String refCompanyIds;

    /**
     * 属性 [IS_COMPANY]
     *
     */
    @JSONField(name = "is_company")
    @JsonProperty("is_company")
    private String isCompany;

    /**
     * 属性 [PHONE]
     *
     */
    @JSONField(name = "phone")
    @JsonProperty("phone")
    private String phone;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [TZ]
     *
     */
    @JSONField(name = "tz")
    @JsonProperty("tz")
    private String tz;

    /**
     * 属性 [EVENT_COUNT]
     *
     */
    @JSONField(name = "event_count")
    @JsonProperty("event_count")
    private Integer eventCount;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 属性 [CALENDAR_LAST_NOTIF_ACK]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "calendar_last_notif_ack" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("calendar_last_notif_ack")
    private Timestamp calendarLastNotifAck;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 属性 [SIGNUP_TYPE]
     *
     */
    @JSONField(name = "signup_type")
    @JsonProperty("signup_type")
    private String signupType;

    /**
     * 属性 [EMAIL_FORMATTED]
     *
     */
    @JSONField(name = "email_formatted")
    @JsonProperty("email_formatted")
    private String emailFormatted;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 属性 [PARTNER_SHARE]
     *
     */
    @JSONField(name = "partner_share")
    @JsonProperty("partner_share")
    private String partnerShare;

    /**
     * 属性 [STREET2]
     *
     */
    @JSONField(name = "street2")
    @JsonProperty("street2")
    private String street2;

    /**
     * 属性 [DEBIT]
     *
     */
    @JSONField(name = "debit")
    @JsonProperty("debit")
    private Double debit;

    /**
     * 属性 [PAYMENT_TOKEN_COUNT]
     *
     */
    @JSONField(name = "payment_token_count")
    @JsonProperty("payment_token_count")
    private Integer paymentTokenCount;

    /**
     * 属性 [REF]
     *
     */
    @JSONField(name = "ref")
    @JsonProperty("ref")
    private String ref;

    /**
     * 属性 [PARTNER_GID]
     *
     */
    @JSONField(name = "partner_gid")
    @JsonProperty("partner_gid")
    private Integer partnerGid;

    /**
     * 属性 [SIGNUP_VALID]
     *
     */
    @JSONField(name = "signup_valid")
    @JsonProperty("signup_valid")
    private String signupValid;

    /**
     * 属性 [WEBSITE_META_OG_IMG]
     *
     */
    @JSONField(name = "website_meta_og_img")
    @JsonProperty("website_meta_og_img")
    private String websiteMetaOgImg;

    /**
     * 属性 [IMAGE_SMALL]
     *
     */
    @JSONField(name = "image_small")
    @JsonProperty("image_small")
    private byte[] imageSmall;

    /**
     * 属性 [BANK_ACCOUNT_COUNT]
     *
     */
    @JSONField(name = "bank_account_count")
    @JsonProperty("bank_account_count")
    private Integer bankAccountCount;

    /**
     * 属性 [STREET]
     *
     */
    @JSONField(name = "street")
    @JsonProperty("street")
    private String street;

    /**
     * 属性 [SALE_WARN]
     *
     */
    @JSONField(name = "sale_warn")
    @JsonProperty("sale_warn")
    private String saleWarn;

    /**
     * 属性 [MESSAGE_BOUNCE]
     *
     */
    @JSONField(name = "message_bounce")
    @JsonProperty("message_bounce")
    private Integer messageBounce;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 属性 [OPPORTUNITY_COUNT]
     *
     */
    @JSONField(name = "opportunity_count")
    @JsonProperty("opportunity_count")
    private Integer opportunityCount;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 属性 [SELF]
     *
     */
    @JSONField(name = "self")
    @JsonProperty("self")
    private Integer self;

    /**
     * 属性 [IM_STATUS]
     *
     */
    @JSONField(name = "im_status")
    @JsonProperty("im_status")
    private String imStatus;

    /**
     * 属性 [CUSTOMER]
     *
     */
    @JSONField(name = "customer")
    @JsonProperty("customer")
    private String customer;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [INVOICE_WARN_MSG]
     *
     */
    @JSONField(name = "invoice_warn_msg")
    @JsonProperty("invoice_warn_msg")
    private String invoiceWarnMsg;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 属性 [PICKING_WARN]
     *
     */
    @JSONField(name = "picking_warn")
    @JsonProperty("picking_warn")
    private String pickingWarn;

    /**
     * 属性 [CONTRACT_IDS]
     *
     */
    @JSONField(name = "contract_ids")
    @JsonProperty("contract_ids")
    private String contractIds;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 属性 [WEBSITE]
     *
     */
    @JSONField(name = "website")
    @JsonProperty("website")
    private String website;

    /**
     * 属性 [MOBILE]
     *
     */
    @JSONField(name = "mobile")
    @JsonProperty("mobile")
    private String mobile;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [CITY]
     *
     */
    @JSONField(name = "city")
    @JsonProperty("city")
    private String city;

    /**
     * 属性 [PROPERTY_PAYMENT_TERM_ID]
     *
     */
    @JSONField(name = "property_payment_term_id")
    @JsonProperty("property_payment_term_id")
    private Integer propertyPaymentTermId;

    /**
     * 属性 [USER_IDS]
     *
     */
    @JSONField(name = "user_ids")
    @JsonProperty("user_ids")
    private String userIds;

    /**
     * 属性 [WEBSITE_META_KEYWORDS]
     *
     */
    @JSONField(name = "website_meta_keywords")
    @JsonProperty("website_meta_keywords")
    private String websiteMetaKeywords;

    /**
     * 属性 [CHANNEL_IDS]
     *
     */
    @JSONField(name = "channel_ids")
    @JsonProperty("channel_ids")
    private String channelIds;

    /**
     * 属性 [PURCHASE_WARN]
     *
     */
    @JSONField(name = "purchase_warn")
    @JsonProperty("purchase_warn")
    private String purchaseWarn;

    /**
     * 属性 [JOURNAL_ITEM_COUNT]
     *
     */
    @JSONField(name = "journal_item_count")
    @JsonProperty("journal_item_count")
    private Integer journalItemCount;

    /**
     * 属性 [SUPPLIER]
     *
     */
    @JSONField(name = "supplier")
    @JsonProperty("supplier")
    private String supplier;

    /**
     * 属性 [PROPERTY_STOCK_SUPPLIER]
     *
     */
    @JSONField(name = "property_stock_supplier")
    @JsonProperty("property_stock_supplier")
    private Integer propertyStockSupplier;

    /**
     * 属性 [PROPERTY_ACCOUNT_PAYABLE_ID]
     *
     */
    @JSONField(name = "property_account_payable_id")
    @JsonProperty("property_account_payable_id")
    private Integer propertyAccountPayableId;

    /**
     * 属性 [WEBSITE_SHORT_DESCRIPTION]
     *
     */
    @JSONField(name = "website_short_description")
    @JsonProperty("website_short_description")
    private String websiteShortDescription;

    /**
     * 属性 [SALE_WARN_MSG]
     *
     */
    @JSONField(name = "sale_warn_msg")
    @JsonProperty("sale_warn_msg")
    private String saleWarnMsg;

    /**
     * 属性 [CREDIT]
     *
     */
    @JSONField(name = "credit")
    @JsonProperty("credit")
    private Double credit;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [VAT]
     *
     */
    @JSONField(name = "vat")
    @JsonProperty("vat")
    private String vat;

    /**
     * 属性 [PROPERTY_SUPPLIER_PAYMENT_TERM_ID]
     *
     */
    @JSONField(name = "property_supplier_payment_term_id")
    @JsonProperty("property_supplier_payment_term_id")
    private Integer propertySupplierPaymentTermId;

    /**
     * 属性 [PROPERTY_STOCK_CUSTOMER]
     *
     */
    @JSONField(name = "property_stock_customer")
    @JsonProperty("property_stock_customer")
    private Integer propertyStockCustomer;

    /**
     * 属性 [COMMENT]
     *
     */
    @JSONField(name = "comment")
    @JsonProperty("comment")
    private String comment;

    /**
     * 属性 [TASK_IDS]
     *
     */
    @JSONField(name = "task_ids")
    @JsonProperty("task_ids")
    private String taskIds;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [EMAIL]
     *
     */
    @JSONField(name = "email")
    @JsonProperty("email")
    private String email;

    /**
     * 属性 [PURCHASE_WARN_MSG]
     *
     */
    @JSONField(name = "purchase_warn_msg")
    @JsonProperty("purchase_warn_msg")
    private String purchaseWarnMsg;

    /**
     * 属性 [WEBSITE_META_TITLE]
     *
     */
    @JSONField(name = "website_meta_title")
    @JsonProperty("website_meta_title")
    private String websiteMetaTitle;

    /**
     * 属性 [ZIP]
     *
     */
    @JSONField(name = "zip")
    @JsonProperty("zip")
    private String zip;

    /**
     * 属性 [TZ_OFFSET]
     *
     */
    @JSONField(name = "tz_offset")
    @JsonProperty("tz_offset")
    private String tzOffset;

    /**
     * 属性 [COMPANY_TYPE]
     *
     */
    @JSONField(name = "company_type")
    @JsonProperty("company_type")
    private String companyType;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;

    /**
     * 属性 [TASK_COUNT]
     *
     */
    @JSONField(name = "task_count")
    @JsonProperty("task_count")
    private Integer taskCount;

    /**
     * 属性 [CREDIT_LIMIT]
     *
     */
    @JSONField(name = "credit_limit")
    @JsonProperty("credit_limit")
    private Double creditLimit;

    /**
     * 属性 [PROPERTY_ACCOUNT_RECEIVABLE_ID]
     *
     */
    @JSONField(name = "property_account_receivable_id")
    @JsonProperty("property_account_receivable_id")
    private Integer propertyAccountReceivableId;

    /**
     * 属性 [PROPERTY_PURCHASE_CURRENCY_ID]
     *
     */
    @JSONField(name = "property_purchase_currency_id")
    @JsonProperty("property_purchase_currency_id")
    private Integer propertyPurchaseCurrencyId;

    /**
     * 属性 [PICKING_WARN_MSG]
     *
     */
    @JSONField(name = "picking_warn_msg")
    @JsonProperty("picking_warn_msg")
    private String pickingWarnMsg;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [SIGNUP_URL]
     *
     */
    @JSONField(name = "signup_url")
    @JsonProperty("signup_url")
    private String signupUrl;

    /**
     * 属性 [LANG]
     *
     */
    @JSONField(name = "lang")
    @JsonProperty("lang")
    private String lang;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 属性 [PROPERTY_ACCOUNT_POSITION_ID]
     *
     */
    @JSONField(name = "property_account_position_id")
    @JsonProperty("property_account_position_id")
    private Integer propertyAccountPositionId;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 属性 [BARCODE]
     *
     */
    @JSONField(name = "barcode")
    @JsonProperty("barcode")
    private String barcode;

    /**
     * 属性 [IS_PUBLISHED]
     *
     */
    @JSONField(name = "is_published")
    @JsonProperty("is_published")
    private String isPublished;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 属性 [SALE_ORDER_COUNT]
     *
     */
    @JSONField(name = "sale_order_count")
    @JsonProperty("sale_order_count")
    private Integer saleOrderCount;

    /**
     * 属性 [IMAGE_MEDIUM]
     *
     */
    @JSONField(name = "image_medium")
    @JsonProperty("image_medium")
    private byte[] imageMedium;

    /**
     * 属性 [ADDITIONAL_INFO]
     *
     */
    @JSONField(name = "additional_info")
    @JsonProperty("additional_info")
    private String additionalInfo;

    /**
     * 属性 [OPPORTUNITY_IDS]
     *
     */
    @JSONField(name = "opportunity_ids")
    @JsonProperty("opportunity_ids")
    private String opportunityIds;

    /**
     * 属性 [CONTRACTS_COUNT]
     *
     */
    @JSONField(name = "contracts_count")
    @JsonProperty("contracts_count")
    private Integer contractsCount;

    /**
     * 属性 [DEBIT_LIMIT]
     *
     */
    @JSONField(name = "debit_limit")
    @JsonProperty("debit_limit")
    private Double debitLimit;

    /**
     * 属性 [WEBSITE_URL]
     *
     */
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    private String websiteUrl;

    /**
     * 属性 [SALE_ORDER_IDS]
     *
     */
    @JSONField(name = "sale_order_ids")
    @JsonProperty("sale_order_ids")
    private String saleOrderIds;

    /**
     * 属性 [LAST_WEBSITE_SO_ID]
     *
     */
    @JSONField(name = "last_website_so_id")
    @JsonProperty("last_website_so_id")
    private Integer lastWebsiteSoId;

    /**
     * 属性 [IS_SEO_OPTIMIZED]
     *
     */
    @JSONField(name = "is_seo_optimized")
    @JsonProperty("is_seo_optimized")
    private String isSeoOptimized;

    /**
     * 属性 [COMMERCIAL_COMPANY_NAME]
     *
     */
    @JSONField(name = "commercial_company_name")
    @JsonProperty("commercial_company_name")
    private String commercialCompanyName;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [TITLE_TEXT]
     *
     */
    @JSONField(name = "title_text")
    @JsonProperty("title_text")
    private String titleText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 属性 [COUNTRY_ID_TEXT]
     *
     */
    @JSONField(name = "country_id_text")
    @JsonProperty("country_id_text")
    private String countryIdText;

    /**
     * 属性 [STATE_ID_TEXT]
     *
     */
    @JSONField(name = "state_id_text")
    @JsonProperty("state_id_text")
    private String stateIdText;

    /**
     * 属性 [COMMERCIAL_PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "commercial_partner_id_text")
    @JsonProperty("commercial_partner_id_text")
    private String commercialPartnerIdText;

    /**
     * 属性 [PARENT_NAME]
     *
     */
    @JSONField(name = "parent_name")
    @JsonProperty("parent_name")
    private String parentName;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [INDUSTRY_ID_TEXT]
     *
     */
    @JSONField(name = "industry_id_text")
    @JsonProperty("industry_id_text")
    private String industryIdText;

    /**
     * 属性 [TEAM_ID_TEXT]
     *
     */
    @JSONField(name = "team_id_text")
    @JsonProperty("team_id_text")
    private String teamIdText;

    /**
     * 属性 [TEAM_ID]
     *
     */
    @JSONField(name = "team_id")
    @JsonProperty("team_id")
    private Integer teamId;

    /**
     * 属性 [STATE_ID]
     *
     */
    @JSONField(name = "state_id")
    @JsonProperty("state_id")
    private Integer stateId;

    /**
     * 属性 [USER_ID]
     *
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    private Integer parentId;

    /**
     * 属性 [TITLE]
     *
     */
    @JSONField(name = "title")
    @JsonProperty("title")
    private Integer title;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [COMMERCIAL_PARTNER_ID]
     *
     */
    @JSONField(name = "commercial_partner_id")
    @JsonProperty("commercial_partner_id")
    private Integer commercialPartnerId;

    /**
     * 属性 [INDUSTRY_ID]
     *
     */
    @JSONField(name = "industry_id")
    @JsonProperty("industry_id")
    private Integer industryId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 属性 [COUNTRY_ID]
     *
     */
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    private Integer countryId;


    /**
     * 设置 [TYPE]
     */
    public void setType(String  type){
        this.type = type ;
        this.modify("type",type);
    }

    /**
     * 设置 [COLOR]
     */
    public void setColor(Integer  color){
        this.color = color ;
        this.modify("color",color);
    }

    /**
     * 设置 [COMPANY_NAME]
     */
    public void setCompanyName(String  companyName){
        this.companyName = companyName ;
        this.modify("company_name",companyName);
    }

    /**
     * 设置 [LAST_TIME_ENTRIES_CHECKED]
     */
    public void setLastTimeEntriesChecked(Timestamp  lastTimeEntriesChecked){
        this.lastTimeEntriesChecked = lastTimeEntriesChecked ;
        this.modify("last_time_entries_checked",lastTimeEntriesChecked);
    }

    /**
     * 设置 [IBIZFUNCTION]
     */
    public void setIbizfunction(String  ibizfunction){
        this.ibizfunction = ibizfunction ;
        this.modify("ibizfunction",ibizfunction);
    }

    /**
     * 设置 [INVOICE_WARN]
     */
    public void setInvoiceWarn(String  invoiceWarn){
        this.invoiceWarn = invoiceWarn ;
        this.modify("invoice_warn",invoiceWarn);
    }

    /**
     * 设置 [SIGNUP_EXPIRATION]
     */
    public void setSignupExpiration(Timestamp  signupExpiration){
        this.signupExpiration = signupExpiration ;
        this.modify("signup_expiration",signupExpiration);
    }

    /**
     * 设置 [WEBSITE_DESCRIPTION]
     */
    public void setWebsiteDescription(String  websiteDescription){
        this.websiteDescription = websiteDescription ;
        this.modify("website_description",websiteDescription);
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    public void setMessageMainAttachmentId(Integer  messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [EMPLOYEE]
     */
    public void setEmployee(String  employee){
        this.employee = employee ;
        this.modify("employee",employee);
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    public void setDisplayName(String  displayName){
        this.displayName = displayName ;
        this.modify("display_name",displayName);
    }

    /**
     * 设置 [WEBSITE_META_DESCRIPTION]
     */
    public void setWebsiteMetaDescription(String  websiteMetaDescription){
        this.websiteMetaDescription = websiteMetaDescription ;
        this.modify("website_meta_description",websiteMetaDescription);
    }

    /**
     * 设置 [SIGNUP_TOKEN]
     */
    public void setSignupToken(String  signupToken){
        this.signupToken = signupToken ;
        this.modify("signup_token",signupToken);
    }

    /**
     * 设置 [IS_COMPANY]
     */
    public void setIsCompany(String  isCompany){
        this.isCompany = isCompany ;
        this.modify("is_company",isCompany);
    }

    /**
     * 设置 [PHONE]
     */
    public void setPhone(String  phone){
        this.phone = phone ;
        this.modify("phone",phone);
    }

    /**
     * 设置 [TZ]
     */
    public void setTz(String  tz){
        this.tz = tz ;
        this.modify("tz",tz);
    }

    /**
     * 设置 [CALENDAR_LAST_NOTIF_ACK]
     */
    public void setCalendarLastNotifAck(Timestamp  calendarLastNotifAck){
        this.calendarLastNotifAck = calendarLastNotifAck ;
        this.modify("calendar_last_notif_ack",calendarLastNotifAck);
    }

    /**
     * 设置 [SIGNUP_TYPE]
     */
    public void setSignupType(String  signupType){
        this.signupType = signupType ;
        this.modify("signup_type",signupType);
    }

    /**
     * 设置 [PARTNER_SHARE]
     */
    public void setPartnerShare(String  partnerShare){
        this.partnerShare = partnerShare ;
        this.modify("partner_share",partnerShare);
    }

    /**
     * 设置 [STREET2]
     */
    public void setStreet2(String  street2){
        this.street2 = street2 ;
        this.modify("street2",street2);
    }

    /**
     * 设置 [REF]
     */
    public void setRef(String  ref){
        this.ref = ref ;
        this.modify("ref",ref);
    }

    /**
     * 设置 [PARTNER_GID]
     */
    public void setPartnerGid(Integer  partnerGid){
        this.partnerGid = partnerGid ;
        this.modify("partner_gid",partnerGid);
    }

    /**
     * 设置 [WEBSITE_META_OG_IMG]
     */
    public void setWebsiteMetaOgImg(String  websiteMetaOgImg){
        this.websiteMetaOgImg = websiteMetaOgImg ;
        this.modify("website_meta_og_img",websiteMetaOgImg);
    }

    /**
     * 设置 [STREET]
     */
    public void setStreet(String  street){
        this.street = street ;
        this.modify("street",street);
    }

    /**
     * 设置 [SALE_WARN]
     */
    public void setSaleWarn(String  saleWarn){
        this.saleWarn = saleWarn ;
        this.modify("sale_warn",saleWarn);
    }

    /**
     * 设置 [MESSAGE_BOUNCE]
     */
    public void setMessageBounce(Integer  messageBounce){
        this.messageBounce = messageBounce ;
        this.modify("message_bounce",messageBounce);
    }

    /**
     * 设置 [DATE]
     */
    public void setDate(Timestamp  date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 设置 [CUSTOMER]
     */
    public void setCustomer(String  customer){
        this.customer = customer ;
        this.modify("customer",customer);
    }

    /**
     * 设置 [INVOICE_WARN_MSG]
     */
    public void setInvoiceWarnMsg(String  invoiceWarnMsg){
        this.invoiceWarnMsg = invoiceWarnMsg ;
        this.modify("invoice_warn_msg",invoiceWarnMsg);
    }

    /**
     * 设置 [PICKING_WARN]
     */
    public void setPickingWarn(String  pickingWarn){
        this.pickingWarn = pickingWarn ;
        this.modify("picking_warn",pickingWarn);
    }

    /**
     * 设置 [WEBSITE]
     */
    public void setWebsite(String  website){
        this.website = website ;
        this.modify("website",website);
    }

    /**
     * 设置 [MOBILE]
     */
    public void setMobile(String  mobile){
        this.mobile = mobile ;
        this.modify("mobile",mobile);
    }

    /**
     * 设置 [CITY]
     */
    public void setCity(String  city){
        this.city = city ;
        this.modify("city",city);
    }

    /**
     * 设置 [WEBSITE_META_KEYWORDS]
     */
    public void setWebsiteMetaKeywords(String  websiteMetaKeywords){
        this.websiteMetaKeywords = websiteMetaKeywords ;
        this.modify("website_meta_keywords",websiteMetaKeywords);
    }

    /**
     * 设置 [PURCHASE_WARN]
     */
    public void setPurchaseWarn(String  purchaseWarn){
        this.purchaseWarn = purchaseWarn ;
        this.modify("purchase_warn",purchaseWarn);
    }

    /**
     * 设置 [SUPPLIER]
     */
    public void setSupplier(String  supplier){
        this.supplier = supplier ;
        this.modify("supplier",supplier);
    }

    /**
     * 设置 [WEBSITE_SHORT_DESCRIPTION]
     */
    public void setWebsiteShortDescription(String  websiteShortDescription){
        this.websiteShortDescription = websiteShortDescription ;
        this.modify("website_short_description",websiteShortDescription);
    }

    /**
     * 设置 [SALE_WARN_MSG]
     */
    public void setSaleWarnMsg(String  saleWarnMsg){
        this.saleWarnMsg = saleWarnMsg ;
        this.modify("sale_warn_msg",saleWarnMsg);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [VAT]
     */
    public void setVat(String  vat){
        this.vat = vat ;
        this.modify("vat",vat);
    }

    /**
     * 设置 [COMMENT]
     */
    public void setComment(String  comment){
        this.comment = comment ;
        this.modify("comment",comment);
    }

    /**
     * 设置 [EMAIL]
     */
    public void setEmail(String  email){
        this.email = email ;
        this.modify("email",email);
    }

    /**
     * 设置 [PURCHASE_WARN_MSG]
     */
    public void setPurchaseWarnMsg(String  purchaseWarnMsg){
        this.purchaseWarnMsg = purchaseWarnMsg ;
        this.modify("purchase_warn_msg",purchaseWarnMsg);
    }

    /**
     * 设置 [WEBSITE_META_TITLE]
     */
    public void setWebsiteMetaTitle(String  websiteMetaTitle){
        this.websiteMetaTitle = websiteMetaTitle ;
        this.modify("website_meta_title",websiteMetaTitle);
    }

    /**
     * 设置 [ZIP]
     */
    public void setZip(String  zip){
        this.zip = zip ;
        this.modify("zip",zip);
    }

    /**
     * 设置 [CREDIT_LIMIT]
     */
    public void setCreditLimit(Double  creditLimit){
        this.creditLimit = creditLimit ;
        this.modify("credit_limit",creditLimit);
    }

    /**
     * 设置 [PICKING_WARN_MSG]
     */
    public void setPickingWarnMsg(String  pickingWarnMsg){
        this.pickingWarnMsg = pickingWarnMsg ;
        this.modify("picking_warn_msg",pickingWarnMsg);
    }

    /**
     * 设置 [LANG]
     */
    public void setLang(String  lang){
        this.lang = lang ;
        this.modify("lang",lang);
    }

    /**
     * 设置 [WEBSITE_ID]
     */
    public void setWebsiteId(Integer  websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(String  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [BARCODE]
     */
    public void setBarcode(String  barcode){
        this.barcode = barcode ;
        this.modify("barcode",barcode);
    }

    /**
     * 设置 [IS_PUBLISHED]
     */
    public void setIsPublished(String  isPublished){
        this.isPublished = isPublished ;
        this.modify("is_published",isPublished);
    }

    /**
     * 设置 [ADDITIONAL_INFO]
     */
    public void setAdditionalInfo(String  additionalInfo){
        this.additionalInfo = additionalInfo ;
        this.modify("additional_info",additionalInfo);
    }

    /**
     * 设置 [DEBIT_LIMIT]
     */
    public void setDebitLimit(Double  debitLimit){
        this.debitLimit = debitLimit ;
        this.modify("debit_limit",debitLimit);
    }

    /**
     * 设置 [COMMERCIAL_COMPANY_NAME]
     */
    public void setCommercialCompanyName(String  commercialCompanyName){
        this.commercialCompanyName = commercialCompanyName ;
        this.modify("commercial_company_name",commercialCompanyName);
    }

    /**
     * 设置 [TEAM_ID]
     */
    public void setTeamId(Integer  teamId){
        this.teamId = teamId ;
        this.modify("team_id",teamId);
    }

    /**
     * 设置 [STATE_ID]
     */
    public void setStateId(Integer  stateId){
        this.stateId = stateId ;
        this.modify("state_id",stateId);
    }

    /**
     * 设置 [USER_ID]
     */
    public void setUserId(Integer  userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [PARENT_ID]
     */
    public void setParentId(Integer  parentId){
        this.parentId = parentId ;
        this.modify("parent_id",parentId);
    }

    /**
     * 设置 [TITLE]
     */
    public void setTitle(Integer  title){
        this.title = title ;
        this.modify("title",title);
    }

    /**
     * 设置 [COMMERCIAL_PARTNER_ID]
     */
    public void setCommercialPartnerId(Integer  commercialPartnerId){
        this.commercialPartnerId = commercialPartnerId ;
        this.modify("commercial_partner_id",commercialPartnerId);
    }

    /**
     * 设置 [INDUSTRY_ID]
     */
    public void setIndustryId(Integer  industryId){
        this.industryId = industryId ;
        this.modify("industry_id",industryId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Integer  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [COUNTRY_ID]
     */
    public void setCountryId(Integer  countryId){
        this.countryId = countryId ;
        this.modify("country_id",countryId);
    }


}

