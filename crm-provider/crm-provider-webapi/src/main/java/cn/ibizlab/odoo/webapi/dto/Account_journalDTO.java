package cn.ibizlab.odoo.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Account_journalDTO]
 */
@Data
public class Account_journalDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [REFUND_SEQUENCE_NUMBER_NEXT]
     *
     */
    @JSONField(name = "refund_sequence_number_next")
    @JsonProperty("refund_sequence_number_next")
    private Integer refundSequenceNumberNext;

    /**
     * 属性 [TYPE_CONTROL_IDS]
     *
     */
    @JSONField(name = "type_control_ids")
    @JsonProperty("type_control_ids")
    private String typeControlIds;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [AT_LEAST_ONE_OUTBOUND]
     *
     */
    @JSONField(name = "at_least_one_outbound")
    @JsonProperty("at_least_one_outbound")
    private String atLeastOneOutbound;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [SEQUENCE_ID]
     *
     */
    @JSONField(name = "sequence_id")
    @JsonProperty("sequence_id")
    private Integer sequenceId;

    /**
     * 属性 [SHOW_ON_DASHBOARD]
     *
     */
    @JSONField(name = "show_on_dashboard")
    @JsonProperty("show_on_dashboard")
    private String showOnDashboard;

    /**
     * 属性 [JOURNAL_USER]
     *
     */
    @JSONField(name = "journal_user")
    @JsonProperty("journal_user")
    private String journalUser;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [BANK_STATEMENTS_SOURCE]
     *
     */
    @JSONField(name = "bank_statements_source")
    @JsonProperty("bank_statements_source")
    private String bankStatementsSource;

    /**
     * 属性 [AT_LEAST_ONE_INBOUND]
     *
     */
    @JSONField(name = "at_least_one_inbound")
    @JsonProperty("at_least_one_inbound")
    private String atLeastOneInbound;

    /**
     * 属性 [POST_AT_BANK_REC]
     *
     */
    @JSONField(name = "post_at_bank_rec")
    @JsonProperty("post_at_bank_rec")
    private String postAtBankRec;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [SEQUENCE_NUMBER_NEXT]
     *
     */
    @JSONField(name = "sequence_number_next")
    @JsonProperty("sequence_number_next")
    private Integer sequenceNumberNext;

    /**
     * 属性 [BELONGS_TO_COMPANY]
     *
     */
    @JSONField(name = "belongs_to_company")
    @JsonProperty("belongs_to_company")
    private String belongsToCompany;

    /**
     * 属性 [CODE]
     *
     */
    @JSONField(name = "code")
    @JsonProperty("code")
    private String code;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [INBOUND_PAYMENT_METHOD_IDS]
     *
     */
    @JSONField(name = "inbound_payment_method_ids")
    @JsonProperty("inbound_payment_method_ids")
    private String inboundPaymentMethodIds;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [GROUP_INVOICE_LINES]
     *
     */
    @JSONField(name = "group_invoice_lines")
    @JsonProperty("group_invoice_lines")
    private String groupInvoiceLines;

    /**
     * 属性 [REFUND_SEQUENCE_ID]
     *
     */
    @JSONField(name = "refund_sequence_id")
    @JsonProperty("refund_sequence_id")
    private Integer refundSequenceId;

    /**
     * 属性 [ALIAS_DOMAIN]
     *
     */
    @JSONField(name = "alias_domain")
    @JsonProperty("alias_domain")
    private String aliasDomain;

    /**
     * 属性 [ACCOUNT_CONTROL_IDS]
     *
     */
    @JSONField(name = "account_control_ids")
    @JsonProperty("account_control_ids")
    private String accountControlIds;

    /**
     * 属性 [UPDATE_POSTED]
     *
     */
    @JSONField(name = "update_posted")
    @JsonProperty("update_posted")
    private String updatePosted;

    /**
     * 属性 [AMOUNT_AUTHORIZED_DIFF]
     *
     */
    @JSONField(name = "amount_authorized_diff")
    @JsonProperty("amount_authorized_diff")
    private Double amountAuthorizedDiff;

    /**
     * 属性 [KANBAN_DASHBOARD_GRAPH]
     *
     */
    @JSONField(name = "kanban_dashboard_graph")
    @JsonProperty("kanban_dashboard_graph")
    private String kanbanDashboardGraph;

    /**
     * 属性 [OUTBOUND_PAYMENT_METHOD_IDS]
     *
     */
    @JSONField(name = "outbound_payment_method_ids")
    @JsonProperty("outbound_payment_method_ids")
    private String outboundPaymentMethodIds;

    /**
     * 属性 [TYPE]
     *
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 属性 [KANBAN_DASHBOARD]
     *
     */
    @JSONField(name = "kanban_dashboard")
    @JsonProperty("kanban_dashboard")
    private String kanbanDashboard;

    /**
     * 属性 [REFUND_SEQUENCE]
     *
     */
    @JSONField(name = "refund_sequence")
    @JsonProperty("refund_sequence")
    private String refundSequence;

    /**
     * 属性 [COLOR]
     *
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [PROFIT_ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "profit_account_id_text")
    @JsonProperty("profit_account_id_text")
    private String profitAccountIdText;

    /**
     * 属性 [ALIAS_NAME]
     *
     */
    @JSONField(name = "alias_name")
    @JsonProperty("alias_name")
    private String aliasName;

    /**
     * 属性 [BANK_ACC_NUMBER]
     *
     */
    @JSONField(name = "bank_acc_number")
    @JsonProperty("bank_acc_number")
    private String bankAccNumber;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [BANK_ID]
     *
     */
    @JSONField(name = "bank_id")
    @JsonProperty("bank_id")
    private Integer bankId;

    /**
     * 属性 [LOSS_ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "loss_account_id_text")
    @JsonProperty("loss_account_id_text")
    private String lossAccountIdText;

    /**
     * 属性 [DEFAULT_CREDIT_ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "default_credit_account_id_text")
    @JsonProperty("default_credit_account_id_text")
    private String defaultCreditAccountIdText;

    /**
     * 属性 [COMPANY_PARTNER_ID]
     *
     */
    @JSONField(name = "company_partner_id")
    @JsonProperty("company_partner_id")
    private Integer companyPartnerId;

    /**
     * 属性 [DEFAULT_DEBIT_ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "default_debit_account_id_text")
    @JsonProperty("default_debit_account_id_text")
    private String defaultDebitAccountIdText;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 属性 [BANK_ACCOUNT_ID]
     *
     */
    @JSONField(name = "bank_account_id")
    @JsonProperty("bank_account_id")
    private Integer bankAccountId;

    /**
     * 属性 [LOSS_ACCOUNT_ID]
     *
     */
    @JSONField(name = "loss_account_id")
    @JsonProperty("loss_account_id")
    private Integer lossAccountId;

    /**
     * 属性 [ALIAS_ID]
     *
     */
    @JSONField(name = "alias_id")
    @JsonProperty("alias_id")
    private Integer aliasId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 属性 [DEFAULT_CREDIT_ACCOUNT_ID]
     *
     */
    @JSONField(name = "default_credit_account_id")
    @JsonProperty("default_credit_account_id")
    private Integer defaultCreditAccountId;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [DEFAULT_DEBIT_ACCOUNT_ID]
     *
     */
    @JSONField(name = "default_debit_account_id")
    @JsonProperty("default_debit_account_id")
    private Integer defaultDebitAccountId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [PROFIT_ACCOUNT_ID]
     *
     */
    @JSONField(name = "profit_account_id")
    @JsonProperty("profit_account_id")
    private Integer profitAccountId;


    /**
     * 设置 [AT_LEAST_ONE_OUTBOUND]
     */
    public void setAtLeastOneOutbound(String  atLeastOneOutbound){
        this.atLeastOneOutbound = atLeastOneOutbound ;
        this.modify("at_least_one_outbound",atLeastOneOutbound);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [SEQUENCE_ID]
     */
    public void setSequenceId(Integer  sequenceId){
        this.sequenceId = sequenceId ;
        this.modify("sequence_id",sequenceId);
    }

    /**
     * 设置 [SHOW_ON_DASHBOARD]
     */
    public void setShowOnDashboard(String  showOnDashboard){
        this.showOnDashboard = showOnDashboard ;
        this.modify("show_on_dashboard",showOnDashboard);
    }

    /**
     * 设置 [JOURNAL_USER]
     */
    public void setJournalUser(String  journalUser){
        this.journalUser = journalUser ;
        this.modify("journal_user",journalUser);
    }

    /**
     * 设置 [BANK_STATEMENTS_SOURCE]
     */
    public void setBankStatementsSource(String  bankStatementsSource){
        this.bankStatementsSource = bankStatementsSource ;
        this.modify("bank_statements_source",bankStatementsSource);
    }

    /**
     * 设置 [AT_LEAST_ONE_INBOUND]
     */
    public void setAtLeastOneInbound(String  atLeastOneInbound){
        this.atLeastOneInbound = atLeastOneInbound ;
        this.modify("at_least_one_inbound",atLeastOneInbound);
    }

    /**
     * 设置 [POST_AT_BANK_REC]
     */
    public void setPostAtBankRec(String  postAtBankRec){
        this.postAtBankRec = postAtBankRec ;
        this.modify("post_at_bank_rec",postAtBankRec);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [CODE]
     */
    public void setCode(String  code){
        this.code = code ;
        this.modify("code",code);
    }

    /**
     * 设置 [GROUP_INVOICE_LINES]
     */
    public void setGroupInvoiceLines(String  groupInvoiceLines){
        this.groupInvoiceLines = groupInvoiceLines ;
        this.modify("group_invoice_lines",groupInvoiceLines);
    }

    /**
     * 设置 [REFUND_SEQUENCE_ID]
     */
    public void setRefundSequenceId(Integer  refundSequenceId){
        this.refundSequenceId = refundSequenceId ;
        this.modify("refund_sequence_id",refundSequenceId);
    }

    /**
     * 设置 [UPDATE_POSTED]
     */
    public void setUpdatePosted(String  updatePosted){
        this.updatePosted = updatePosted ;
        this.modify("update_posted",updatePosted);
    }

    /**
     * 设置 [AMOUNT_AUTHORIZED_DIFF]
     */
    public void setAmountAuthorizedDiff(Double  amountAuthorizedDiff){
        this.amountAuthorizedDiff = amountAuthorizedDiff ;
        this.modify("amount_authorized_diff",amountAuthorizedDiff);
    }

    /**
     * 设置 [TYPE]
     */
    public void setType(String  type){
        this.type = type ;
        this.modify("type",type);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(String  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [REFUND_SEQUENCE]
     */
    public void setRefundSequence(String  refundSequence){
        this.refundSequence = refundSequence ;
        this.modify("refund_sequence",refundSequence);
    }

    /**
     * 设置 [COLOR]
     */
    public void setColor(Integer  color){
        this.color = color ;
        this.modify("color",color);
    }

    /**
     * 设置 [BANK_ACCOUNT_ID]
     */
    public void setBankAccountId(Integer  bankAccountId){
        this.bankAccountId = bankAccountId ;
        this.modify("bank_account_id",bankAccountId);
    }

    /**
     * 设置 [LOSS_ACCOUNT_ID]
     */
    public void setLossAccountId(Integer  lossAccountId){
        this.lossAccountId = lossAccountId ;
        this.modify("loss_account_id",lossAccountId);
    }

    /**
     * 设置 [ALIAS_ID]
     */
    public void setAliasId(Integer  aliasId){
        this.aliasId = aliasId ;
        this.modify("alias_id",aliasId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Integer  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [DEFAULT_CREDIT_ACCOUNT_ID]
     */
    public void setDefaultCreditAccountId(Integer  defaultCreditAccountId){
        this.defaultCreditAccountId = defaultCreditAccountId ;
        this.modify("default_credit_account_id",defaultCreditAccountId);
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    public void setCurrencyId(Integer  currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [DEFAULT_DEBIT_ACCOUNT_ID]
     */
    public void setDefaultDebitAccountId(Integer  defaultDebitAccountId){
        this.defaultDebitAccountId = defaultDebitAccountId ;
        this.modify("default_debit_account_id",defaultDebitAccountId);
    }

    /**
     * 设置 [PROFIT_ACCOUNT_ID]
     */
    public void setProfitAccountId(Integer  profitAccountId){
        this.profitAccountId = profitAccountId ;
        this.modify("profit_account_id",profitAccountId);
    }


}

