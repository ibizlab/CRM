package cn.ibizlab.odoo.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.webapi.dto.*;
import cn.ibizlab.odoo.webapi.mapping.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_pricelist;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_pricelistService;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_pricelistSearchContext;




@Slf4j
@Api(tags = {"Product_pricelist" })
@RestController("WebApi-product_pricelist")
@RequestMapping("")
public class Product_pricelistResource {

    @Autowired
    private IProduct_pricelistService product_pricelistService;

    @Autowired
    @Lazy
    private Product_pricelistMapping product_pricelistMapping;




    @PreAuthorize("hasPermission(#product_pricelist_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Product_pricelist" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/product_pricelists/{product_pricelist_id}")
    public ResponseEntity<Product_pricelistDTO> get(@PathVariable("product_pricelist_id") Integer product_pricelist_id) {
        Product_pricelist domain = product_pricelistService.get(product_pricelist_id);
        Product_pricelistDTO dto = product_pricelistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @ApiOperation(value = "Save", tags = {"Product_pricelist" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/product_pricelists/save")
    public ResponseEntity<Boolean> save(@RequestBody Product_pricelistDTO product_pricelistdto) {
        return ResponseEntity.status(HttpStatus.OK).body(product_pricelistService.save(product_pricelistMapping.toDomain(product_pricelistdto)));
    }

    @ApiOperation(value = "SaveBatch", tags = {"Product_pricelist" },  notes = "SaveBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/product_pricelists/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Product_pricelistDTO> product_pricelistdtos) {
        product_pricelistService.saveBatch(product_pricelistMapping.toDomain(product_pricelistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @ApiOperation(value = "获取草稿数据", tags = {"Product_pricelist" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/product_pricelists/getdraft")
    public ResponseEntity<Product_pricelistDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(product_pricelistMapping.toDto(product_pricelistService.getDraft(new Product_pricelist())));
    }




    @PreAuthorize("hasPermission(#product_pricelist_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Product_pricelist" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_pricelists/{product_pricelist_id}")

    public ResponseEntity<Product_pricelistDTO> update(@PathVariable("product_pricelist_id") Integer product_pricelist_id, @RequestBody Product_pricelistDTO product_pricelistdto) {
		Product_pricelist domain = product_pricelistMapping.toDomain(product_pricelistdto);
        domain.setId(product_pricelist_id);
		product_pricelistService.update(domain);
		Product_pricelistDTO dto = product_pricelistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#product_pricelist_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Product_pricelist" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_pricelists/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_pricelistDTO> product_pricelistdtos) {
        product_pricelistService.updateBatch(product_pricelistMapping.toDomain(product_pricelistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#product_pricelist_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Product_pricelist" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_pricelists/{product_pricelist_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_pricelist_id") Integer product_pricelist_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_pricelistService.remove(product_pricelist_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Product_pricelist" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_pricelists/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        product_pricelistService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @ApiOperation(value = "CheckKey", tags = {"Product_pricelist" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/product_pricelists/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_pricelistDTO product_pricelistdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(product_pricelistService.checkKey(product_pricelistMapping.toDomain(product_pricelistdto)));
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Product_pricelist" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/product_pricelists")

    public ResponseEntity<Product_pricelistDTO> create(@RequestBody Product_pricelistDTO product_pricelistdto) {
        Product_pricelist domain = product_pricelistMapping.toDomain(product_pricelistdto);
		product_pricelistService.create(domain);
        Product_pricelistDTO dto = product_pricelistMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }
    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Product_pricelist" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/product_pricelists/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_pricelistDTO> product_pricelistdtos) {
        product_pricelistService.createBatch(product_pricelistMapping.toDomain(product_pricelistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Product_pricelist-Default-all')")
	@ApiOperation(value = "fetch默认查询", tags = {"Product_pricelist" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_pricelists/fetchdefault")
	public ResponseEntity<List<Product_pricelistDTO>> fetchDefault(Product_pricelistSearchContext context) {
        Page<Product_pricelist> domains = product_pricelistService.searchDefault(context) ;
        List<Product_pricelistDTO> list = product_pricelistMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Product_pricelist-Default-all')")
	@ApiOperation(value = "search默认查询", tags = {"Product_pricelist" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.POST , value="/product_pricelists/searchdefault")
	public ResponseEntity<Page<Product_pricelistDTO>> searchDefault(@RequestBody Product_pricelistSearchContext context) {
        Page<Product_pricelist> domains = product_pricelistService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_pricelistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Product_pricelist getEntity(){
        return new Product_pricelist();
    }

}
