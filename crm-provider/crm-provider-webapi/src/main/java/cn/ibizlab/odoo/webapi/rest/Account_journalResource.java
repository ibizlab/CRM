package cn.ibizlab.odoo.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.webapi.dto.*;
import cn.ibizlab.odoo.webapi.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_journal;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_journalService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_journalSearchContext;




@Slf4j
@Api(tags = {"Account_journal" })
@RestController("WebApi-account_journal")
@RequestMapping("")
public class Account_journalResource {

    @Autowired
    private IAccount_journalService account_journalService;

    @Autowired
    @Lazy
    private Account_journalMapping account_journalMapping;







    @PreAuthorize("hasPermission(#account_journal_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_journal" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_journals/{account_journal_id}")
    public ResponseEntity<Account_journalDTO> get(@PathVariable("account_journal_id") Integer account_journal_id) {
        Account_journal domain = account_journalService.get(account_journal_id);
        Account_journalDTO dto = account_journalMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @ApiOperation(value = "获取草稿数据", tags = {"Account_journal" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_journals/getdraft")
    public ResponseEntity<Account_journalDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_journalMapping.toDto(account_journalService.getDraft(new Account_journal())));
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_journal" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_journals")

    public ResponseEntity<Account_journalDTO> create(@RequestBody Account_journalDTO account_journaldto) {
        Account_journal domain = account_journalMapping.toDomain(account_journaldto);
		account_journalService.create(domain);
        Account_journalDTO dto = account_journalMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }
    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_journal" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_journals/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_journalDTO> account_journaldtos) {
        account_journalService.createBatch(account_journalMapping.toDomain(account_journaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#account_journal_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_journal" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_journals/{account_journal_id}")

    public ResponseEntity<Account_journalDTO> update(@PathVariable("account_journal_id") Integer account_journal_id, @RequestBody Account_journalDTO account_journaldto) {
		Account_journal domain = account_journalMapping.toDomain(account_journaldto);
        domain.setId(account_journal_id);
		account_journalService.update(domain);
		Account_journalDTO dto = account_journalMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_journal_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_journal" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_journals/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_journalDTO> account_journaldtos) {
        account_journalService.updateBatch(account_journalMapping.toDomain(account_journaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#account_journal_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_journal" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_journals/{account_journal_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_journal_id") Integer account_journal_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_journalService.remove(account_journal_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_journal" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_journals/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_journalService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Account_journal-Default-all')")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_journal" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_journals/fetchdefault")
	public ResponseEntity<List<Account_journalDTO>> fetchDefault(Account_journalSearchContext context) {
        Page<Account_journal> domains = account_journalService.searchDefault(context) ;
        List<Account_journalDTO> list = account_journalMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Account_journal-Default-all')")
	@ApiOperation(value = "search默认查询", tags = {"Account_journal" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.POST , value="/account_journals/searchdefault")
	public ResponseEntity<Page<Account_journalDTO>> searchDefault(@RequestBody Account_journalSearchContext context) {
        Page<Account_journal> domains = account_journalService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_journalMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_journal getEntity(){
        return new Account_journal();
    }

}
