package cn.ibizlab.odoo.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.webapi.dto.*;
import cn.ibizlab.odoo.webapi.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_partnerService;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partnerSearchContext;




@Slf4j
@Api(tags = {"Res_partner" })
@RestController("WebApi-res_partner")
@RequestMapping("")
public class Res_partnerResource {

    @Autowired
    private IRes_partnerService res_partnerService;

    @Autowired
    @Lazy
    private Res_partnerMapping res_partnerMapping;




    @PreAuthorize("hasPermission('Remove',{#res_partner_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Res_partner" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partners/{res_partner_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_partner_id") Integer res_partner_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_partnerService.remove(res_partner_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Res_partner" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partners/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        res_partnerService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#res_partner_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Res_partner" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/res_partners/{res_partner_id}")
    public ResponseEntity<Res_partnerDTO> get(@PathVariable("res_partner_id") Integer res_partner_id) {
        Res_partner domain = res_partnerService.get(res_partner_id);
        Res_partnerDTO dto = res_partnerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @ApiOperation(value = "CheckKey", tags = {"Res_partner" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_partnerDTO res_partnerdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_partnerService.checkKey(res_partnerMapping.toDomain(res_partnerdto)));
    }




    @ApiOperation(value = "Save", tags = {"Res_partner" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/save")
    public ResponseEntity<Boolean> save(@RequestBody Res_partnerDTO res_partnerdto) {
        return ResponseEntity.status(HttpStatus.OK).body(res_partnerService.save(res_partnerMapping.toDomain(res_partnerdto)));
    }

    @ApiOperation(value = "SaveBatch", tags = {"Res_partner" },  notes = "SaveBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Res_partnerDTO> res_partnerdtos) {
        res_partnerService.saveBatch(res_partnerMapping.toDomain(res_partnerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#res_partner_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Res_partner" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partners/{res_partner_id}")

    public ResponseEntity<Res_partnerDTO> update(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Res_partnerDTO res_partnerdto) {
		Res_partner domain = res_partnerMapping.toDomain(res_partnerdto);
        domain.setId(res_partner_id);
		res_partnerService.update(domain);
		Res_partnerDTO dto = res_partnerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#res_partner_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Res_partner" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partners/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_partnerDTO> res_partnerdtos) {
        res_partnerService.updateBatch(res_partnerMapping.toDomain(res_partnerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @ApiOperation(value = "获取草稿数据", tags = {"Res_partner" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/res_partners/getdraft")
    public ResponseEntity<Res_partnerDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(res_partnerMapping.toDto(res_partnerService.getDraft(new Res_partner())));
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Res_partner" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners")

    public ResponseEntity<Res_partnerDTO> create(@RequestBody Res_partnerDTO res_partnerdto) {
        Res_partner domain = res_partnerMapping.toDomain(res_partnerdto);
		res_partnerService.create(domain);
        Res_partnerDTO dto = res_partnerMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }
    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Res_partner" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_partnerDTO> res_partnerdtos) {
        res_partnerService.createBatch(res_partnerMapping.toDomain(res_partnerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Res_partner-Contacts-all')")
	@ApiOperation(value = "fetch联系人（人）", tags = {"Res_partner" } ,notes = "fetch联系人（人）")
    @RequestMapping(method= RequestMethod.GET , value="/res_partners/fetchcontacts")
	public ResponseEntity<List<Res_partnerDTO>> fetchContacts(Res_partnerSearchContext context) {
        Page<Res_partner> domains = res_partnerService.searchContacts(context) ;
        List<Res_partnerDTO> list = res_partnerMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Res_partner-Contacts-all')")
	@ApiOperation(value = "search联系人（人）", tags = {"Res_partner" } ,notes = "search联系人（人）")
    @RequestMapping(method= RequestMethod.POST , value="/res_partners/searchcontacts")
	public ResponseEntity<Page<Res_partnerDTO>> searchContacts(@RequestBody Res_partnerSearchContext context) {
        Page<Res_partner> domains = res_partnerService.searchContacts(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_partnerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Res_partner-Company-all')")
	@ApiOperation(value = "fetch联系人（公司）", tags = {"Res_partner" } ,notes = "fetch联系人（公司）")
    @RequestMapping(method= RequestMethod.GET , value="/res_partners/fetchcompany")
	public ResponseEntity<List<Res_partnerDTO>> fetchCompany(Res_partnerSearchContext context) {
        Page<Res_partner> domains = res_partnerService.searchCompany(context) ;
        List<Res_partnerDTO> list = res_partnerMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Res_partner-Company-all')")
	@ApiOperation(value = "search联系人（公司）", tags = {"Res_partner" } ,notes = "search联系人（公司）")
    @RequestMapping(method= RequestMethod.POST , value="/res_partners/searchcompany")
	public ResponseEntity<Page<Res_partnerDTO>> searchCompany(@RequestBody Res_partnerSearchContext context) {
        Page<Res_partner> domains = res_partnerService.searchCompany(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_partnerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Res_partner-Default-all')")
	@ApiOperation(value = "fetch默认查询", tags = {"Res_partner" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_partners/fetchdefault")
	public ResponseEntity<List<Res_partnerDTO>> fetchDefault(Res_partnerSearchContext context) {
        Page<Res_partner> domains = res_partnerService.searchDefault(context) ;
        List<Res_partnerDTO> list = res_partnerMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Res_partner-Default-all')")
	@ApiOperation(value = "search默认查询", tags = {"Res_partner" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.POST , value="/res_partners/searchdefault")
	public ResponseEntity<Page<Res_partnerDTO>> searchDefault(@RequestBody Res_partnerSearchContext context) {
        Page<Res_partner> domains = res_partnerService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_partnerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Res_partner getEntity(){
        return new Res_partner();
    }

}
