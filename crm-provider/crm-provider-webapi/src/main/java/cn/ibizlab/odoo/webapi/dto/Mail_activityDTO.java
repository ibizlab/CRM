package cn.ibizlab.odoo.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Mail_activityDTO]
 */
@Data
public class Mail_activityDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [MAIL_TEMPLATE_IDS]
     *
     */
    @JSONField(name = "mail_template_ids")
    @JsonProperty("mail_template_ids")
    private String mailTemplateIds;

    /**
     * 属性 [AUTOMATED]
     *
     */
    @JSONField(name = "automated")
    @JsonProperty("automated")
    private String automated;

    /**
     * 属性 [RES_NAME]
     *
     */
    @JSONField(name = "res_name")
    @JsonProperty("res_name")
    private String resName;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [DATE_DEADLINE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("date_deadline")
    private Timestamp dateDeadline;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [SUMMARY]
     *
     */
    @JSONField(name = "summary")
    @JsonProperty("summary")
    private String summary;

    /**
     * 属性 [HAS_RECOMMENDED_ACTIVITIES]
     *
     */
    @JSONField(name = "has_recommended_activities")
    @JsonProperty("has_recommended_activities")
    private String hasRecommendedActivities;

    /**
     * 属性 [RES_ID]
     *
     */
    @JSONField(name = "res_id")
    @JsonProperty("res_id")
    private Integer resId;

    /**
     * 属性 [NOTE]
     *
     */
    @JSONField(name = "note")
    @JsonProperty("note")
    private String note;

    /**
     * 属性 [FEEDBACK]
     *
     */
    @JSONField(name = "feedback")
    @JsonProperty("feedback")
    private String feedback;

    /**
     * 属性 [RES_MODEL_ID]
     *
     */
    @JSONField(name = "res_model_id")
    @JsonProperty("res_model_id")
    private Integer resModelId;

    /**
     * 属性 [RES_MODEL]
     *
     */
    @JSONField(name = "res_model")
    @JsonProperty("res_model")
    private String resModel;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 属性 [ACTIVITY_CATEGORY]
     *
     */
    @JSONField(name = "activity_category")
    @JsonProperty("activity_category")
    private String activityCategory;

    /**
     * 属性 [ACTIVITY_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "activity_type_id_text")
    @JsonProperty("activity_type_id_text")
    private String activityTypeIdText;

    /**
     * 属性 [PREVIOUS_ACTIVITY_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "previous_activity_type_id_text")
    @JsonProperty("previous_activity_type_id_text")
    private String previousActivityTypeIdText;

    /**
     * 属性 [NOTE_ID_TEXT]
     *
     */
    @JSONField(name = "note_id_text")
    @JsonProperty("note_id_text")
    private String noteIdText;

    /**
     * 属性 [CREATE_USER_ID_TEXT]
     *
     */
    @JSONField(name = "create_user_id_text")
    @JsonProperty("create_user_id_text")
    private String createUserIdText;

    /**
     * 属性 [ICON]
     *
     */
    @JSONField(name = "icon")
    @JsonProperty("icon")
    private String icon;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [ACTIVITY_DECORATION]
     *
     */
    @JSONField(name = "activity_decoration")
    @JsonProperty("activity_decoration")
    private String activityDecoration;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [FORCE_NEXT]
     *
     */
    @JSONField(name = "force_next")
    @JsonProperty("force_next")
    private String forceNext;

    /**
     * 属性 [RECOMMENDED_ACTIVITY_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "recommended_activity_type_id_text")
    @JsonProperty("recommended_activity_type_id_text")
    private String recommendedActivityTypeIdText;

    /**
     * 属性 [CALENDAR_EVENT_ID_TEXT]
     *
     */
    @JSONField(name = "calendar_event_id_text")
    @JsonProperty("calendar_event_id_text")
    private String calendarEventIdText;

    /**
     * 属性 [RECOMMENDED_ACTIVITY_TYPE_ID]
     *
     */
    @JSONField(name = "recommended_activity_type_id")
    @JsonProperty("recommended_activity_type_id")
    private Integer recommendedActivityTypeId;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 属性 [CREATE_USER_ID]
     *
     */
    @JSONField(name = "create_user_id")
    @JsonProperty("create_user_id")
    private Integer createUserId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [USER_ID]
     *
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;

    /**
     * 属性 [NOTE_ID]
     *
     */
    @JSONField(name = "note_id")
    @JsonProperty("note_id")
    private Integer noteId;

    /**
     * 属性 [PREVIOUS_ACTIVITY_TYPE_ID]
     *
     */
    @JSONField(name = "previous_activity_type_id")
    @JsonProperty("previous_activity_type_id")
    private Integer previousActivityTypeId;

    /**
     * 属性 [CALENDAR_EVENT_ID]
     *
     */
    @JSONField(name = "calendar_event_id")
    @JsonProperty("calendar_event_id")
    private Integer calendarEventId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 设置 [AUTOMATED]
     */
    public void setAutomated(String  automated){
        this.automated = automated ;
        this.modify("automated",automated);
    }

    /**
     * 设置 [RES_NAME]
     */
    public void setResName(String  resName){
        this.resName = resName ;
        this.modify("res_name",resName);
    }

    /**
     * 设置 [DATE_DEADLINE]
     */
    public void setDateDeadline(Timestamp  dateDeadline){
        this.dateDeadline = dateDeadline ;
        this.modify("date_deadline",dateDeadline);
    }

    /**
     * 设置 [SUMMARY]
     */
    public void setSummary(String  summary){
        this.summary = summary ;
        this.modify("summary",summary);
    }

    /**
     * 设置 [RES_ID]
     */
    public void setResId(Integer  resId){
        this.resId = resId ;
        this.modify("res_id",resId);
    }

    /**
     * 设置 [NOTE]
     */
    public void setNote(String  note){
        this.note = note ;
        this.modify("note",note);
    }

    /**
     * 设置 [FEEDBACK]
     */
    public void setFeedback(String  feedback){
        this.feedback = feedback ;
        this.modify("feedback",feedback);
    }

    /**
     * 设置 [RES_MODEL_ID]
     */
    public void setResModelId(Integer  resModelId){
        this.resModelId = resModelId ;
        this.modify("res_model_id",resModelId);
    }

    /**
     * 设置 [RES_MODEL]
     */
    public void setResModel(String  resModel){
        this.resModel = resModel ;
        this.modify("res_model",resModel);
    }

    /**
     * 设置 [RECOMMENDED_ACTIVITY_TYPE_ID]
     */
    public void setRecommendedActivityTypeId(Integer  recommendedActivityTypeId){
        this.recommendedActivityTypeId = recommendedActivityTypeId ;
        this.modify("recommended_activity_type_id",recommendedActivityTypeId);
    }

    /**
     * 设置 [ACTIVITY_TYPE_ID]
     */
    public void setActivityTypeId(Integer  activityTypeId){
        this.activityTypeId = activityTypeId ;
        this.modify("activity_type_id",activityTypeId);
    }

    /**
     * 设置 [CREATE_USER_ID]
     */
    public void setCreateUserId(Integer  createUserId){
        this.createUserId = createUserId ;
        this.modify("create_user_id",createUserId);
    }

    /**
     * 设置 [USER_ID]
     */
    public void setUserId(Integer  userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [NOTE_ID]
     */
    public void setNoteId(Integer  noteId){
        this.noteId = noteId ;
        this.modify("note_id",noteId);
    }

    /**
     * 设置 [PREVIOUS_ACTIVITY_TYPE_ID]
     */
    public void setPreviousActivityTypeId(Integer  previousActivityTypeId){
        this.previousActivityTypeId = previousActivityTypeId ;
        this.modify("previous_activity_type_id",previousActivityTypeId);
    }

    /**
     * 设置 [CALENDAR_EVENT_ID]
     */
    public void setCalendarEventId(Integer  calendarEventId){
        this.calendarEventId = calendarEventId ;
        this.modify("calendar_event_id",calendarEventId);
    }


}

