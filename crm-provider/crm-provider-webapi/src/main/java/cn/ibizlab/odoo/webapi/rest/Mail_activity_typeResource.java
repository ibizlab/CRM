package cn.ibizlab.odoo.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.webapi.dto.*;
import cn.ibizlab.odoo.webapi.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity_type;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_activity_typeService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_activity_typeSearchContext;




@Slf4j
@Api(tags = {"Mail_activity_type" })
@RestController("WebApi-mail_activity_type")
@RequestMapping("")
public class Mail_activity_typeResource {

    @Autowired
    private IMail_activity_typeService mail_activity_typeService;

    @Autowired
    @Lazy
    private Mail_activity_typeMapping mail_activity_typeMapping;










    @PreAuthorize("hasPermission('Remove',{#mail_activity_type_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_activity_type" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_activity_types/{mail_activity_type_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_activity_type_id") Integer mail_activity_type_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_activity_typeService.remove(mail_activity_type_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_activity_type" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_activity_types/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_activity_typeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mail_activity_type_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_activity_type" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_activity_types/{mail_activity_type_id}")
    public ResponseEntity<Mail_activity_typeDTO> get(@PathVariable("mail_activity_type_id") Integer mail_activity_type_id) {
        Mail_activity_type domain = mail_activity_typeService.get(mail_activity_type_id);
        Mail_activity_typeDTO dto = mail_activity_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @ApiOperation(value = "获取草稿数据", tags = {"Mail_activity_type" },  notes = "获取草稿数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_activity_types/getdraft")
    public ResponseEntity<Mail_activity_typeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_activity_typeMapping.toDto(mail_activity_typeService.getDraft(new Mail_activity_type())));
    }




    @ApiOperation(value = "CheckKey", tags = {"Mail_activity_type" },  notes = "CheckKey")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activity_types/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_activity_typeDTO mail_activity_typedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_activity_typeService.checkKey(mail_activity_typeMapping.toDomain(mail_activity_typedto)));
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_activity_type" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activity_types")

    public ResponseEntity<Mail_activity_typeDTO> create(@RequestBody Mail_activity_typeDTO mail_activity_typedto) {
        Mail_activity_type domain = mail_activity_typeMapping.toDomain(mail_activity_typedto);
		mail_activity_typeService.create(domain);
        Mail_activity_typeDTO dto = mail_activity_typeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }
    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_activity_type" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activity_types/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_activity_typeDTO> mail_activity_typedtos) {
        mail_activity_typeService.createBatch(mail_activity_typeMapping.toDomain(mail_activity_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#mail_activity_type_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_activity_type" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_activity_types/{mail_activity_type_id}")

    public ResponseEntity<Mail_activity_typeDTO> update(@PathVariable("mail_activity_type_id") Integer mail_activity_type_id, @RequestBody Mail_activity_typeDTO mail_activity_typedto) {
		Mail_activity_type domain = mail_activity_typeMapping.toDomain(mail_activity_typedto);
        domain.setId(mail_activity_type_id);
		mail_activity_typeService.update(domain);
		Mail_activity_typeDTO dto = mail_activity_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_activity_type_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_activity_type" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_activity_types/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_activity_typeDTO> mail_activity_typedtos) {
        mail_activity_typeService.updateBatch(mail_activity_typeMapping.toDomain(mail_activity_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @ApiOperation(value = "Save", tags = {"Mail_activity_type" },  notes = "Save")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activity_types/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_activity_typeDTO mail_activity_typedto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_activity_typeService.save(mail_activity_typeMapping.toDomain(mail_activity_typedto)));
    }

    @ApiOperation(value = "SaveBatch", tags = {"Mail_activity_type" },  notes = "SaveBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activity_types/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_activity_typeDTO> mail_activity_typedtos) {
        mail_activity_typeService.saveBatch(mail_activity_typeMapping.toDomain(mail_activity_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Mail_activity_type-Default-all')")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_activity_type" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_activity_types/fetchdefault")
	public ResponseEntity<List<Mail_activity_typeDTO>> fetchDefault(Mail_activity_typeSearchContext context) {
        Page<Mail_activity_type> domains = mail_activity_typeService.searchDefault(context) ;
        List<Mail_activity_typeDTO> list = mail_activity_typeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','crm-Mail_activity_type-Default-all')")
	@ApiOperation(value = "search默认查询", tags = {"Mail_activity_type" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.POST , value="/mail_activity_types/searchdefault")
	public ResponseEntity<Page<Mail_activity_typeDTO>> searchDefault(@RequestBody Mail_activity_typeSearchContext context) {
        Page<Mail_activity_type> domains = mail_activity_typeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_activity_typeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_activity_type getEntity(){
        return new Mail_activity_type();
    }

}
